# Rev6 OA Config

The Configurator’s purpose is to generate a Bill of Materials (BOM). It can be run as a standalone app and will display all jobs, or by using Job Entry module in Epicor to select a specific job, and then clicking “Get OAU Details” button on the KCC tab.

## Project Structure

### OA_Config_Rev6
This project is a .NET Framework 4 windows application project.

## Data Access
All data access to the KCC database is done via ADO.NET and stored procedures, as well as a Visual Studio dataset. The following stored procedures are used:

* LineChange_GetOprDataByPlantLineAndOpr
* LineChange_GetProductionLineByJobNum
* LineChange_GetJobSchedulingDataByJobNum
* LineChange_GetLineScheduleByDate
* R6_OA_GetOA_ConfigMainList
* R6_OA_GetOA_JobHeadDataByJobNum
* R6_OA_GetTandemCompElecData
* R6_OA_GetProdCalDay
* R6_OA_GetPartDetail
* R6_OA_GetPartBOM
* R6_OA_GetJobNumsByPartSearch
* R6_OA_GetOrderDetail
* R6_OA_GetLaborDetailDataByJobNum
* R6_OA_GetOA_ScheduleData
* R6_OA_GetRefrigerationComponentData
* R6_OA_GetPartRulesByRuleHeadID_AndRuleBatchID
* VKG_GetPartRulesByPartNumAndRuleBatchID
* R5_ROA_GetPartRulesByRuleHeadID_AndRuleBatchID
* R6_OA_GetLineSizeList
* MonitorGetOrderDetails
* MonitorGetModelNoDigitDesc
* R6_OA_GetPartOprAndOpDtlData
* R6_OA_GetOA_JobOperations
* R6_OA_GetJobOpDtlByJobNumAsmSeqOprSeq
* R6_OA_GetOA_JobDataByJobNum
* R6_ROA_GetTandemCompAssemblyMtls
* R6_OA_UpdateOA_SchedulePartsAdded
* R6_OA_UpdateOA_SchedulePartsOrdered
* R6_OA_GetOA_SchedulePartsOrdered
* GetOAU_PartListByJobNum
* R6_OA_GetOAU_BOMsData
* R6_OA_InsertOAU_BOMsData
* R6_OA_UpdateOAU_BOMsData
* R6_OA_DeleteOAU_BOMsData
* R6_OA_GetModelNumDesc
* VKG_GetModelNumDesc
* MSP_GetModelNumDesc
* R6_OA_GetRev5ModelNoDigitDesc
* GetOAU_ModelNoValues
* MonitorGetModelNoValues
* MSP_GetModelNoValues
* R6_OA_GetTandemAsmFromJobAsmbl
* VKG_GetPartListByModelNo_StatLoc
* R6_OA_GetPartListByModelNo_StatLoc
* R5_ROA_GetPartListByModelNo_StatLoc
* MonitorGetPartListByModelNo
* MSP_GetPartListByModelNo
* VKG_GetExistingBOMPartListByJobNum
* R6_OA_GetExistingBOMPartListByJobNum_Statloc
* MonitorGetExistingBOMPartListByJobNum
* R5_ROA_GetExistingBOMPartListByJobNum_StatLoc
* R6_OA_GetRev5ExistingBOMPartListByJobNum
* R6_OA_GetCompressorDataDtl
* R5_ROA_GetCompressorDetailData
* R6_OA_GetCompressorChargeData
* R6_OA_GetFurnaceDataDTL
* R6_OA_GetMotorDataDTL
* R5_ROA_GetMotorDataDTL
* MonitorGetCompAndMotorData
* R6_OA_GetPartRevisionNum
* R6_OA_GetPanelAsmBOM
* R6_OA_GetPartMtlBOM
* R6_OA_GetOA_JobAssemblys
* R6_OA_GetPartOpDtl
* R6_ROA_GetPartOperations
* R6_OA_GetResourceGroupData
* R6_OA_GetAssemblyOperations
* R6_OA_GetOrderHedData
* R6_OA_GetOAU_PartNumbers
* R6_OA_GetParentPartNums
* R6_ROA_GetPartCategoryHead
* R6_OA_GetPartCategoryHead
* R6_ROA_GetPartNumbersByCategory
* R6_OA_GetMetalPartNumbers
* R6_OA_GetPartAsmCost
* R6_ROA_GetMotorData
* R6_ROA_GetPartConditionsByRuleHeadID
* R6_ROA_GetAssemblyQtyAndUOM
* R6_ROA_GetPartRulesByRuleHeadIdAndRuleBatchId
* R6_ROA_GetPartRulesByDigit
* R6_OA_GetOAU_BreakerByModelNo
* R5_ROA_GetOAU_BreakerByModelNo
* MSP_GetBreakerByModelNo
* R6_OA_GetPartRulesHead
* R6_OA_GetSheetMetalReleaseByJobNum
* R6_OA_GetSheetMetalReleases
* R6_OA_GetSheetMetalReleaseMetal
* R6_OA_GetJobMtlPartDetail
* R6_OA_GetSheetMetalQtyByPart
* R6_OA_UpdateJobMtlPartQty
* R6_OA_UpdateSheetMetalRelease
* R6_ROA_InsertJobOpDtl
* R6_ROA_InsertJobOper
* R6_OA_Insert_MaterialJobMtl
* R6_OA_Insert_SinglePartJobMtl
* R6_OA_InsertPartJobMtl
* R6_OA_UpdateJobMtl
* R6_OA_InsertSheetMetalRelease
* R6_ROA_InsertJobAssembly
* R6_OA_UpdateJobAsmbl
* R6_OA_DeleteJobOpDtl
* R6_OA_CopyJobMtlToHistory
* R6_OA_DeleteJobMtl
* R6_OA_DeletePartNumFromJobMtl
* R6_OA_DeleteJobOper
* R6_OA_UpdateRemoveQtysPartQtyPartWhse
* R6_ROA_DeleteSecondaryAssemblySeqFromJobAsmbl
* MonitorGetEtlData
* R6_OA_GetEtlData
* R6_OA_GetEtlJobHeadData
* R6_OA_DeleteEtlJobHeadData
* R6_OA_DeleteEtlOAUData
* MonitorDeleteEtlData
* R6_OA_InsertEtlJobHeadData
* R6_OA_InsertETLOau
* MonitorInsertEtl
* MSP_InsertEtl
* R6_OA_GetModelNumDesc
* VKG_GetModelNumDesc
* R6_OA_GetCompCircuitChargeData
* R6_OA_GetRev5CompCircuitChargeData
* MonitorGetMotorDataDTL
* DeleteFromOA_PremliminaryBOM
* GetOAU_JobsByDateRange
* GetCritCompParts_InsertOA_PremBOM
* Insert_OAU_PickListHistory

## Reports
The following Crystal Reports are used (reports not stored on epicor905app server are used for debugging purposes):

* \\KCCWVTEPIC9APP2\Apps\OAU\OAU.Configurator\crystalreports\CriticalCompBOM.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompBOM.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\OAU.Configurator\crystalreports\CriticalCompBOM.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompBOM.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\OAU.Configurator\crystalreports\MonitorCriticalCompBOM.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\MonitorCriticalCompBOM.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_RefrigerationComp.v2.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\OAU.Configurator\crystalreports\R6_OA_JobTraveler.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_JobTraveler.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_NoJobNumJobTraveler.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\JobNumBarCode.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompPartsByDateRange.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\PurchasePartsByPartNum.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\SheetMetalNeededByJobNum.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\SheetMetalNeededByDateRange.rpt
* \\KCCWVTEPICSQL01.kccinternational.local\Apps\OAU\OAU.Configurator\crystalreports\TandemAsmByJobNumber.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\TandemAsmByJobNumber.rpt
* \\KCCWVTEPICSQL01.kccinternational.local\Apps\OAU\OAU.Configurator\crystalreports\JobMtlHistoryByJobNum.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\JobMtlHistoryByJobNum.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\JobGoodToGo.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\RefrigerationComponent_TestVersion.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\RefrigerationComponent.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\CrystalReports\VME_and_RFGASM_PartsMultiJob.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\VME_and_RFGASM_PartsMultiJob.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_ModelNoConfiguration.rpt
* \\DEVSVR01\Apps\OAU\OAU.Configurator\crystalreports\R6_OA_ModelNoConfiguration.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\OAU.Configurator\crystalreports\R6_OA_ModelNoConfiguration.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\VKG_ModelNoConfiguration.rpt
* \\DEVSVR01\Apps\OAU\OAU.Configurator\crystalreports\VKG_ModelNoConfiguration.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\OAU.Configurator\crystalreports\VKG_ModelNoConfiguration.rpt
* C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OA_Config_Rev6\OA_Config_Rev6\Reports\MonitorSubmittalRpt.v1.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\MonitorSubmittalRpt.v1.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\MSP_ModelNoConfiguration.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\MSP_ModelNoConfiguration.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\OA_OldModelNoConfiguration.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\PartDemandByDateRange.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\OAUPickList\OAUPickListv5.rpt
* \\EPICOR905APP\Apps\OAU\OAUPickList\OAUPickListv5.rpt
* \\KCCWVTEPIC9APP2\Apps\OAU\OAUPickList\OAU_PickListByStation.rpt
* \\EPICOR905APP\Apps\OAU\OAUPickList\OAU_PickListByStation.rpt
* C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OAUConfig\OAUPrePicklist.rpt
* \\EPICOR905APP\Apps\OAU\OAUPickList\OAUPrePicklist.rpt
* rassdk://\\\\EPICOR905APP\\Apps\\OAU\\OAUConfiguratorcrystalreports\\R6_OA_NoJobNumJobTraveler.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompPartsByJobNum.rpt
* \\DEVSVR01\Apps\OAU\OAU.Configurator\crystalreports\OAU_SheetMetalBendReport.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\OAU_SheetMetalBendReport.rpt
* \\DEVSVR01\Apps\OAU\OAU.Configurator\crystalreports\SheetMetalSuperMarketPicklistByJobNum.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\SheetMetalSuperMarketPicklistByJobNum.rpt
* C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OA_Config_Rev6\OA_Config_Rev6\Reports\SubAssemblyLine.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\SubAssemblyLine.rpt
* C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OAUConfig\TandemAsmJobs.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\TandemAsmJobs.rpt

## How to run locally
Assuming a VPN connection, make sure solution is ready to run in Debug configuration. Change the connection strings in frmMain.cs #if Debug sections to point to the appropriate local or dev KCC database.
Make sure Crystal Reports for Visual Studio is installed, and fix all broken references to point to the installed Crystal Reports .dlls. Disable manifest signing. Build the solution and run it. 

## How to deploy
Manually copy to Epicor:

Navigate to the Epicor folder, create a folder with name of "backup" followed by current date, and copy GetDetailsLoader.application, .exe, .exe.config, .pdb, and .vshost.exe to that folder. 
![alt text](https://bitbucket.org/barndog324/rev6-oa-config/downloads/BackupFiles.png)  
  

Build solution, go to output directory and copy OA_Config_Rev6.application, .exe, .exe.config, .pdb, and .vshost.exe:
![alt text](https://bitbucket.org/barndog324/rev6-oa-config/downloads/Step1.png)  


Paste the above files into the Epicor folder and rename them to GetDetailsLoader:
![alt text](https://bitbucket.org/barndog324/rev6-oa-config/downloads/Step2.png)


## Author/Devs
Tony Thoman