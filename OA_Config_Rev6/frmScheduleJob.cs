﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmScheduleJob : Form
    {
        private string ToLine = "Empty";
        private string ToLoc = "Empty";
        private string SelectedSlot = "Empty";

        private string Slot_StartTime = "";
        private string Slot1_StartTime = "";
        private string Slot2_StartTime = "";
        private string Slot3_StartTime = "";
        private string Slot4_StartTime = "";
        private string Slot5_StartTime = "";
        private string Slot1_EndTime = "";
        private string Slot2_EndTime = "";
        private string Slot3_EndTime = "";
        private string Slot4_EndTime = "";
        private string Slot5_EndTime = "";
        private DateTime StartDate = DateTime.Now;

        LineTransferBO objTran = new LineTransferBO();
        MainBO objMain = new MainBO();

        public frmScheduleJob()
        {
            InitializeComponent();
        }       
     
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string slotStartTime = "";
            string jobNum = lbJobNum.Text;

            double prodHrs = 0;

            if (Slot_StartTime == "")
            {
                MessageBox.Show("ERROR -- No slot time has been selected!");
            }
            else
            {
                DialogResult result1 = MessageBox.Show("You are attempting to Schedule Job# " + jobNum + "to " + ToLoc + " " + ToLine + ".\nIf you wish to proceed, then press 'Yes' to Continue otherwise press 'No' to Cancel.",
                                                           "ATTENTION ATTENTION ATTENTION",
                                                           MessageBoxButtons.YesNo,
                                                           MessageBoxIcon.Exclamation);
                if (result1 == DialogResult.Yes)
                {
                    string epicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
                    string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;
                    Console.WriteLine(epicorServer);

                    TransferJob tj = new TransferJob(epicorServer, epicorSqlConnectionString);
                    tj.ScheduleJobToOpenSlot(jobNum, ToLoc, ToLine, Slot_StartTime, StartDate);
                }
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpToLineDate_ValueChanged(object sender, EventArgs e)
        {            
            if (ToLine != "Empty")
            {
                populateSlotTimes(ToLine);
            }
            else
            {
                MessageBox.Show("ERROR - No To Line has been selected!");
            }
        }

        private void rbGrassToLine1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine1.Checked == true)
            {
                ToLine = "Line1";
                ToLoc = "Grassland";
                populateSlotTimes(ToLine);
            }
        }

        private void rbGrassToLine2_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine2.Checked == true)
            {
                ToLine = "Line2";
                ToLoc = "Grassland";
                populateSlotTimes(ToLine);
            }
        }

        private void rbGrassToLine3_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine3.Checked == true)
            {
                ToLine = "Line3";
                ToLoc = "Grassland";
                populateSlotTimes(ToLine);
            }
        }

        private void rbGrassToLine4_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine4.Checked == true)
            {
                ToLine = "Line4";
                ToLoc = "Grassland";
                populateSlotTimes(ToLine);
            }
        }

        private void rbTechToLineA_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineA.Checked == true)
            {
                ToLine = "LineA";
                ToLoc = "Technology";
                populateSlotTimes(ToLine);
            }
        }

        private void rbTechToLineB_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineB.Checked == true)
            {
                ToLine = "LineB";
                ToLoc = "Technology";
                populateSlotTimes(ToLine);
            }
        }

        private void rbTechToLineC_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineC.Checked == true)
            {
                ToLine = "LineC";
                ToLoc = "Technology";
                populateSlotTimes(ToLine);
            }
        }

        private void rbTechToLineD_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineD.Checked == true)
            {
                ToLine = "LineD";
                ToLoc = "Technology";
                populateSlotTimes(ToLine);
            }
        }

        private void rbSlot1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSlot1.Checked == true)
            {               
                if (gbSlot1.Text == "Open Slot")
                {
                    SelectedSlot = "Slot1";
                    Slot_StartTime = "05:00";
                    btnSubmit.Enabled = true;
                }
                else
                {
                    MessageBox.Show("WARNING -- This Slot already has a job scheduled in it. You must first unschedule this job in order to proceed.");
                    rbSlot1.Checked = false;
                }
            }
        }

        private void rbSlot2_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSlot2.Checked == true)
            {
                if (gbSlot2.Text == "Open Slot")
                {
                    SelectedSlot = "Slot2";
                    Slot_StartTime = "07:00";
                    btnSubmit.Enabled = true;
                }
                else
                {
                    MessageBox.Show("WARNING -- This Slot already has a job scheduled in it. You must first unschedule this job in order to proceed.");
                    rbSlot2.Checked = false;
                }
            }
        }

        private void rbSlot3_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSlot3.Checked == true)
            {
                if (gbSlot3.Text == "Open Slot")
                {
                    SelectedSlot = "Slot3";
                    Slot_StartTime = "09:00";
                    btnSubmit.Enabled = true;
                }
                else
                {
                    MessageBox.Show("WARNING -- This Slot already has a job scheduled in it. You must first unschedule this job in order to proceed.");
                    rbSlot3.Checked = false;
                }
            }
        }

        private void rbSlot4_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSlot4.Checked == true)
            {
                if (gbSlot4.Text == "Open Slot")
                {
                    SelectedSlot = "Slot4";
                    Slot_StartTime = "11:00";
                    btnSubmit.Enabled = true;
                }
                else
                {
                    MessageBox.Show("WARNING -- This Slot already has a job scheduled in it. You must first unschedule this job in order to proceed.");
                    rbSlot4.Checked = false;
                }
            }
        }

        private void gbSlot5_Enter(object sender, EventArgs e)
        {
            if (rbSlot5.Checked == true)
            {
                if (gbSlot5.Text == "Open Slot")
                {
                    SelectedSlot = "Slot5";
                    Slot_StartTime = "13:00";
                    btnSubmit.Enabled = true;
                }
                else
                {
                    MessageBox.Show("WARNING -- This Slot already has a job scheduled in it. You must first unschedule this job in order to proceed.");
                    rbSlot5.Checked = false;
                }
            }
        }

        #region OtherMethods
        private void populateSlotTimes(string toLine)
        {
            string jobNum = "";
            string startHour = "";
            string dueHour = "";
            string startDate = "";

            int rowIdx = 1;

            StartDate = dtpToLineDate.Value;

            startDate = StartDate.ToShortDateString();

            gbLineSchd.Text = ToLine + " Schedule for " + startDate;

            DataTable dt = objTran.GetLineScheduleByDate(ToLine, StartDate);

            if (dt.Rows.Count == 0)
            {
                gbSlot1.Text = "Open Slot";
                lbSlot1StartHour.Text = "05:00";
                lbSlot1DueHour.Text = "??:??";
                gbSlot2.Text = "Open Slot";
                lbSlot2StartHour.Text = "07:00";
                lbSlot2DueHour.Text = "??:??";
                gbSlot3.Text = "Open Slot";
                lbSlot3StartHour.Text = "09:00";
                lbSlot3DueHour.Text = "??:??";
                gbSlot4.Text = "Open Slot";
                lbSlot4StartHour.Text = "11:00";
                lbSlot4DueHour.Text = "??:??";
                gbSlot5.Text = "Open Slot";
                lbSlot5StartHour.Text = "13:00";
                lbSlot5DueHour.Text = "??:??";
            }
            else
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["JobNum"] != null)
                    {
                        jobNum = row["JobNum"].ToString();
                    }
                    else
                    {
                        jobNum = "Open Slot";
                    }

                    if (row["StartHour"] != null)
                    {
                        startHour = row["StartHour"].ToString();
                    }
                    if (row["DueHour"] != null)
                    {
                        dueHour = row["DueHour"].ToString();
                    }

                    if (rowIdx == 1)
                    {
                        gbSlot1.Text = jobNum;
                        if (jobNum != "EmptySlot")
                        {
                            gbSlot1.Text = jobNum;
                            lbSlot1StartHour.Text = startHour;
                            Slot1_StartTime = startHour;
                            lbSlot1DueHour.Text = dueHour;
                            Slot1_EndTime = dueHour;
                        }
                    }
                    else if (rowIdx == 2)
                    {
                        gbSlot2.Text = jobNum;
                        if (jobNum != "EmptySlot")
                        {
                            gbSlot2.Text = jobNum;
                            lbSlot2StartHour.Text = startHour;
                            Slot2_StartTime = startHour;
                            lbSlot2DueHour.Text = dueHour;
                            Slot2_EndTime = dueHour;
                        }
                    }
                    else if (rowIdx == 3)
                    {
                        gbSlot3.Text = jobNum;

                        if (jobNum != "EmptySlot")
                        {
                            gbSlot3.Text = jobNum;
                            lbSlot3StartHour.Text = startHour;
                            Slot3_StartTime = startHour;
                            lbSlot3DueHour.Text = dueHour;
                            Slot3_EndTime = dueHour;
                        }

                    }
                    else if (rowIdx == 4)
                    {
                        if (jobNum != "EmptySlot")
                        {
                            gbSlot4.Text = jobNum;
                            lbSlot4StartHour.Text = startHour;
                            Slot4_StartTime = startHour;
                            lbSlot4DueHour.Text = dueHour;
                            Slot4_EndTime = dueHour;
                        }
                    }
                    else if (rowIdx == 5)
                    {
                        if (jobNum != "EmptySlot")
                        {
                            gbSlot5.Text = jobNum;
                            lbSlot5StartHour.Text = startHour;
                            Slot5_StartTime = startHour;
                            lbSlot5DueHour.Text = dueHour;
                            Slot5_EndTime = dueHour;
                        }
                    }
                    ++rowIdx;
                }                               
            }
            if (ToLoc == "Technology")
            {
                gbSlot4.Visible = true;
                gbSlot5.Visible = true;
            }
            else
            {
                gbSlot4.Visible = false;
                gbSlot5.Visible = false;
            }
                       
        }

        private double calcProdHrs(DataTable dtJob)
        {
            string prodHrs = "0";

            int asmSeq = 0;

            double totProdHrs = 0;            

            foreach (DataRow dr in dtJob.Rows)  // Add all of the productions hours
            {
                asmSeq = Int32.Parse(dr["AssemblySeq"].ToString());

                if (asmSeq <= 2)
                {
                    prodHrs = dr["ProdStandard"].ToString();
                    totProdHrs += double.Parse(prodHrs);
                }
            }

            totProdHrs += 8; // This 8 hours is to account for the extra day of travel time given for the Electron control panel.
            return totProdHrs;
        }
        
        #endregion        
    }
}
