﻿namespace OA_Config_Rev6
{
    partial class frmSelectCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbWheels = new System.Windows.Forms.CheckBox();
            this.cbPreHeat = new System.Windows.Forms.CheckBox();
            this.cbMotor = new System.Windows.Forms.CheckBox();
            this.cbFurnaceHeater = new System.Windows.Forms.CheckBox();
            this.cbExtHeater = new System.Windows.Forms.CheckBox();
            this.cbDampers = new System.Windows.Forms.CheckBox();
            this.cbCompressor = new System.Windows.Forms.CheckBox();
            this.cbCoil = new System.Windows.Forms.CheckBox();
            this.lbJobNumListHidden = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbWheels);
            this.groupBox1.Controls.Add(this.cbPreHeat);
            this.groupBox1.Controls.Add(this.cbMotor);
            this.groupBox1.Controls.Add(this.cbFurnaceHeater);
            this.groupBox1.Controls.Add(this.cbExtHeater);
            this.groupBox1.Controls.Add(this.cbDampers);
            this.groupBox1.Controls.Add(this.cbCompressor);
            this.groupBox1.Controls.Add(this.cbCoil);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.groupBox1.Location = new System.Drawing.Point(13, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(418, 222);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Check all Part Categories you wish to appear in this report.";
            // 
            // cbWheels
            // 
            this.cbWheels.Location = new System.Drawing.Point(7, 191);
            this.cbWheels.Name = "cbWheels";
            this.cbWheels.Size = new System.Drawing.Size(150, 20);
            this.cbWheels.TabIndex = 9;
            this.cbWheels.Text = "Wheels";
            this.cbWheels.UseVisualStyleBackColor = true;
            // 
            // cbPreHeat
            // 
            this.cbPreHeat.Location = new System.Drawing.Point(7, 167);
            this.cbPreHeat.Name = "cbPreHeat";
            this.cbPreHeat.Size = new System.Drawing.Size(150, 20);
            this.cbPreHeat.TabIndex = 8;
            this.cbPreHeat.Text = "Pre-Heat";
            this.cbPreHeat.UseVisualStyleBackColor = true;
            // 
            // cbMotor
            // 
            this.cbMotor.Location = new System.Drawing.Point(7, 143);
            this.cbMotor.Name = "cbMotor";
            this.cbMotor.Size = new System.Drawing.Size(150, 20);
            this.cbMotor.TabIndex = 7;
            this.cbMotor.Text = "Motor";
            this.cbMotor.UseVisualStyleBackColor = true;
            // 
            // cbFurnaceHeater
            // 
            this.cbFurnaceHeater.Location = new System.Drawing.Point(7, 119);
            this.cbFurnaceHeater.Name = "cbFurnaceHeater";
            this.cbFurnaceHeater.Size = new System.Drawing.Size(150, 20);
            this.cbFurnaceHeater.TabIndex = 6;
            this.cbFurnaceHeater.Text = "Furnace/Heater";
            this.cbFurnaceHeater.UseVisualStyleBackColor = true;
            // 
            // cbExtHeater
            // 
            this.cbExtHeater.Location = new System.Drawing.Point(7, 95);
            this.cbExtHeater.Name = "cbExtHeater";
            this.cbExtHeater.Size = new System.Drawing.Size(150, 20);
            this.cbExtHeater.TabIndex = 5;
            this.cbExtHeater.Text = "External Heater";
            this.cbExtHeater.UseVisualStyleBackColor = true;
            // 
            // cbDampers
            // 
            this.cbDampers.Location = new System.Drawing.Point(7, 71);
            this.cbDampers.Name = "cbDampers";
            this.cbDampers.Size = new System.Drawing.Size(150, 20);
            this.cbDampers.TabIndex = 4;
            this.cbDampers.Text = "Dampers";
            this.cbDampers.UseVisualStyleBackColor = true;
            // 
            // cbCompressor
            // 
            this.cbCompressor.Location = new System.Drawing.Point(7, 47);
            this.cbCompressor.Name = "cbCompressor";
            this.cbCompressor.Size = new System.Drawing.Size(405, 20);
            this.cbCompressor.TabIndex = 2;
            this.cbCompressor.Text = "Compressors (Scroll, Digital Scroll, & Digital Scroll Tandem)";
            this.cbCompressor.UseVisualStyleBackColor = true;
            // 
            // cbCoil
            // 
            this.cbCoil.Location = new System.Drawing.Point(7, 23);
            this.cbCoil.Name = "cbCoil";
            this.cbCoil.Size = new System.Drawing.Size(150, 20);
            this.cbCoil.TabIndex = 0;
            this.cbCoil.Text = "Coil";
            this.cbCoil.UseVisualStyleBackColor = true;
            // 
            // lbJobNumListHidden
            // 
            this.lbJobNumListHidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNumListHidden.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbJobNumListHidden.Location = new System.Drawing.Point(154, 5);
            this.lbJobNumListHidden.Name = "lbJobNumListHidden";
            this.lbJobNumListHidden.Size = new System.Drawing.Size(100, 20);
            this.lbJobNumListHidden.TabIndex = 12;
            this.lbJobNumListHidden.Text = "Job Number List";
            this.lbJobNumListHidden.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbJobNumListHidden.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(276, 287);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(356, 287);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnDisplay.TabIndex = 10;
            this.btnDisplay.Text = "Display Rpt";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // frmSelectCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 315);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbJobNumListHidden);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDisplay);
            this.Name = "frmSelectCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Categories For Report";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbWheels;
        private System.Windows.Forms.CheckBox cbPreHeat;
        private System.Windows.Forms.CheckBox cbMotor;
        private System.Windows.Forms.CheckBox cbFurnaceHeater;
        private System.Windows.Forms.CheckBox cbExtHeater;
        private System.Windows.Forms.CheckBox cbDampers;
        private System.Windows.Forms.CheckBox cbCompressor;
        private System.Windows.Forms.CheckBox cbCoil;
        public System.Windows.Forms.Label lbJobNumListHidden;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDisplay;
    }
}