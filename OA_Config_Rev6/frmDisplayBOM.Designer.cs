﻿namespace OA_Config_Rev6
{
    partial class frmDisplayBOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDisplayBOM));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbBOM = new System.Windows.Forms.GroupBox();
            this.gbSaveBomProgress = new System.Windows.Forms.GroupBox();
            this.lbSaveMsg = new System.Windows.Forms.Label();
            this.gbBOMPrintOptions = new System.Windows.Forms.GroupBox();
            this.buttonPrintOpCancel = new System.Windows.Forms.Button();
            this.buttonPrintOpOK = new System.Windows.Forms.Button();
            this.gbBOMRptType = new System.Windows.Forms.GroupBox();
            this.rbRfgCompRpt = new System.Windows.Forms.RadioButton();
            this.rbJobNumSticker = new System.Windows.Forms.RadioButton();
            this.rbCriticalCompBOM = new System.Windows.Forms.RadioButton();
            this.rbEngBOM = new System.Windows.Forms.RadioButton();
            this.gbModPartSearchResults = new System.Windows.Forms.GroupBox();
            this.dgvPartSearchResults = new System.Windows.Forms.DataGridView();
            this.btnSearchResultsOK = new System.Windows.Forms.Button();
            this.btnSearchResultsCancel = new System.Windows.Forms.Button();
            this.buttonPartSearchOK = new System.Windows.Forms.Button();
            this.buttonPartSearchCancel = new System.Windows.Forms.Button();
            this.btnAddPart = new System.Windows.Forms.Button();
            this.gbAddModDelPart = new System.Windows.Forms.GroupBox();
            this.lbModParentPartNum = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtModAsmSeq = new System.Windows.Forms.TextBox();
            this.lbModCostMethod = new System.Windows.Forms.Label();
            this.lbModPartStatus = new System.Windows.Forms.Label();
            this.lbModJobMtlProgressRecID = new System.Windows.Forms.Label();
            this.lbModRevNum = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtModStatus = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtModUnitCost = new System.Windows.Forms.TextBox();
            this.cbModMotorType = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtModUOM = new System.Windows.Forms.TextBox();
            this.txtModPartCategory = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBoxSearchIcon = new System.Windows.Forms.PictureBox();
            this.btnModUpdate = new System.Windows.Forms.Button();
            this.btnModDelete = new System.Windows.Forms.Button();
            this.btnModExit = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtModRelOP = new System.Windows.Forms.TextBox();
            this.txtModWhseLoc = new System.Windows.Forms.TextBox();
            this.txtModPartQty = new System.Windows.Forms.TextBox();
            this.txtModPartDes = new System.Windows.Forms.TextBox();
            this.txtModSeqNo = new System.Windows.Forms.TextBox();
            this.txtModPartNum = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvBOM = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.lbLastUpdateDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbBOMCreationDate = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbOrderDate = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lbCompleteDate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbProdStartDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbShipDate = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lbMsg = new System.Windows.Forms.Label();
            this.msDisplayBOM_Menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recalculateMCAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.lbMCA = new System.Windows.Forms.Label();
            this.lbMOP = new System.Windows.Forms.Label();
            this.lbCustName = new System.Windows.Forms.Label();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbRawMOP = new System.Windows.Forms.Label();
            this.lbFlowRate = new System.Windows.Forms.Label();
            this.lbEnteringTemp = new System.Windows.Forms.Label();
            this.lbVoltage = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbBOM_CreateBy = new System.Windows.Forms.Label();
            this.lbLastUpdateBy = new System.Windows.Forms.Label();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.lbCir1Label = new System.Windows.Forms.Label();
            this.lbCir2Label = new System.Windows.Forms.Label();
            this.lbCir1Chrg = new System.Windows.Forms.Label();
            this.lbCir2Chrg = new System.Windows.Forms.Label();
            this.lbUnitType = new System.Windows.Forms.Label();
            this.gbBOM.SuspendLayout();
            this.gbSaveBomProgress.SuspendLayout();
            this.gbBOMPrintOptions.SuspendLayout();
            this.gbBOMRptType.SuspendLayout();
            this.gbModPartSearchResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartSearchResults)).BeginInit();
            this.gbAddModDelPart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSearchIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBOM)).BeginInit();
            this.msDisplayBOM_Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbBOM
            // 
            this.gbBOM.Controls.Add(this.gbSaveBomProgress);
            this.gbBOM.Controls.Add(this.gbBOMPrintOptions);
            this.gbBOM.Controls.Add(this.gbModPartSearchResults);
            this.gbBOM.Controls.Add(this.btnAddPart);
            this.gbBOM.Controls.Add(this.gbAddModDelPart);
            this.gbBOM.Controls.Add(this.label7);
            this.gbBOM.Controls.Add(this.label8);
            this.gbBOM.Controls.Add(this.label5);
            this.gbBOM.Controls.Add(this.label6);
            this.gbBOM.Controls.Add(this.dgvBOM);
            this.gbBOM.Controls.Add(this.btnExit);
            this.gbBOM.Controls.Add(this.btnSave);
            this.gbBOM.Controls.Add(this.btnPrint);
            this.gbBOM.ForeColor = System.Drawing.Color.Teal;
            this.gbBOM.Location = new System.Drawing.Point(13, 117);
            this.gbBOM.Name = "gbBOM";
            this.gbBOM.Size = new System.Drawing.Size(1466, 619);
            this.gbBOM.TabIndex = 0;
            this.gbBOM.TabStop = false;
            this.gbBOM.Text = "Materials";
            // 
            // gbSaveBomProgress
            // 
            this.gbSaveBomProgress.BackColor = System.Drawing.Color.PaleGreen;
            this.gbSaveBomProgress.Controls.Add(this.lbSaveMsg);
            this.gbSaveBomProgress.Location = new System.Drawing.Point(17, 398);
            this.gbSaveBomProgress.Name = "gbSaveBomProgress";
            this.gbSaveBomProgress.Size = new System.Drawing.Size(226, 87);
            this.gbSaveBomProgress.TabIndex = 263;
            this.gbSaveBomProgress.TabStop = false;
            this.gbSaveBomProgress.Text = "Save BOM Percentage";
            this.gbSaveBomProgress.Visible = false;
            // 
            // lbSaveMsg
            // 
            this.lbSaveMsg.BackColor = System.Drawing.Color.LightGray;
            this.lbSaveMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSaveMsg.ForeColor = System.Drawing.Color.Navy;
            this.lbSaveMsg.Location = new System.Drawing.Point(39, 34);
            this.lbSaveMsg.Name = "lbSaveMsg";
            this.lbSaveMsg.Size = new System.Drawing.Size(146, 23);
            this.lbSaveMsg.TabIndex = 0;
            this.lbSaveMsg.Text = "Saving BOM";
            this.lbSaveMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbBOMPrintOptions
            // 
            this.gbBOMPrintOptions.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gbBOMPrintOptions.Controls.Add(this.buttonPrintOpCancel);
            this.gbBOMPrintOptions.Controls.Add(this.buttonPrintOpOK);
            this.gbBOMPrintOptions.Controls.Add(this.gbBOMRptType);
            this.gbBOMPrintOptions.Location = new System.Drawing.Point(17, 179);
            this.gbBOMPrintOptions.Name = "gbBOMPrintOptions";
            this.gbBOMPrintOptions.Size = new System.Drawing.Size(226, 213);
            this.gbBOMPrintOptions.TabIndex = 260;
            this.gbBOMPrintOptions.TabStop = false;
            this.gbBOMPrintOptions.Text = "Report Options";
            this.gbBOMPrintOptions.Visible = false;
            // 
            // buttonPrintOpCancel
            // 
            this.buttonPrintOpCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrintOpCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonPrintOpCancel.Location = new System.Drawing.Point(119, 179);
            this.buttonPrintOpCancel.Name = "buttonPrintOpCancel";
            this.buttonPrintOpCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintOpCancel.TabIndex = 3;
            this.buttonPrintOpCancel.Text = "Cancel";
            this.buttonPrintOpCancel.UseVisualStyleBackColor = true;
            this.buttonPrintOpCancel.Click += new System.EventHandler(this.buttonPrintOpCancel_Click);
            // 
            // buttonPrintOpOK
            // 
            this.buttonPrintOpOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrintOpOK.ForeColor = System.Drawing.Color.Teal;
            this.buttonPrintOpOK.Location = new System.Drawing.Point(29, 179);
            this.buttonPrintOpOK.Name = "buttonPrintOpOK";
            this.buttonPrintOpOK.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintOpOK.TabIndex = 2;
            this.buttonPrintOpOK.Text = "Display";
            this.buttonPrintOpOK.UseVisualStyleBackColor = true;
            this.buttonPrintOpOK.Click += new System.EventHandler(this.buttonPrintOpOK_Click);
            // 
            // gbBOMRptType
            // 
            this.gbBOMRptType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gbBOMRptType.Controls.Add(this.rbRfgCompRpt);
            this.gbBOMRptType.Controls.Add(this.rbJobNumSticker);
            this.gbBOMRptType.Controls.Add(this.rbCriticalCompBOM);
            this.gbBOMRptType.Controls.Add(this.rbEngBOM);
            this.gbBOMRptType.Location = new System.Drawing.Point(10, 24);
            this.gbBOMRptType.Name = "gbBOMRptType";
            this.gbBOMRptType.Size = new System.Drawing.Size(206, 135);
            this.gbBOMRptType.TabIndex = 0;
            this.gbBOMRptType.TabStop = false;
            this.gbBOMRptType.Text = "Reports";
            // 
            // rbRfgCompRpt
            // 
            this.rbRfgCompRpt.AutoSize = true;
            this.rbRfgCompRpt.Location = new System.Drawing.Point(19, 100);
            this.rbRfgCompRpt.Name = "rbRfgCompRpt";
            this.rbRfgCompRpt.Size = new System.Drawing.Size(177, 17);
            this.rbRfgCompRpt.TabIndex = 10;
            this.rbRfgCompRpt.Text = "Refrigeration Component Report";
            this.rbRfgCompRpt.UseVisualStyleBackColor = true;
            // 
            // rbJobNumSticker
            // 
            this.rbJobNumSticker.AutoSize = true;
            this.rbJobNumSticker.Location = new System.Drawing.Point(19, 77);
            this.rbJobNumSticker.Name = "rbJobNumSticker";
            this.rbJobNumSticker.Size = new System.Drawing.Size(161, 17);
            this.rbJobNumSticker.TabIndex = 9;
            this.rbJobNumSticker.Text = "Job Number Barcode Sticker";
            this.rbJobNumSticker.UseVisualStyleBackColor = true;
            // 
            // rbCriticalCompBOM
            // 
            this.rbCriticalCompBOM.AutoSize = true;
            this.rbCriticalCompBOM.Location = new System.Drawing.Point(19, 54);
            this.rbCriticalCompBOM.Name = "rbCriticalCompBOM";
            this.rbCriticalCompBOM.Size = new System.Drawing.Size(140, 17);
            this.rbCriticalCompBOM.TabIndex = 8;
            this.rbCriticalCompBOM.Text = "Critical Component BOM";
            this.rbCriticalCompBOM.UseVisualStyleBackColor = true;
            // 
            // rbEngBOM
            // 
            this.rbEngBOM.AutoSize = true;
            this.rbEngBOM.Checked = true;
            this.rbEngBOM.Location = new System.Drawing.Point(19, 31);
            this.rbEngBOM.Name = "rbEngBOM";
            this.rbEngBOM.Size = new System.Drawing.Size(108, 17);
            this.rbEngBOM.TabIndex = 4;
            this.rbEngBOM.TabStop = true;
            this.rbEngBOM.Text = "Engineering BOM";
            this.rbEngBOM.UseVisualStyleBackColor = true;
            // 
            // gbModPartSearchResults
            // 
            this.gbModPartSearchResults.BackColor = System.Drawing.Color.LightSalmon;
            this.gbModPartSearchResults.Controls.Add(this.dgvPartSearchResults);
            this.gbModPartSearchResults.Controls.Add(this.btnSearchResultsOK);
            this.gbModPartSearchResults.Controls.Add(this.btnSearchResultsCancel);
            this.gbModPartSearchResults.Controls.Add(this.buttonPartSearchOK);
            this.gbModPartSearchResults.Controls.Add(this.buttonPartSearchCancel);
            this.gbModPartSearchResults.Location = new System.Drawing.Point(251, 179);
            this.gbModPartSearchResults.Name = "gbModPartSearchResults";
            this.gbModPartSearchResults.Size = new System.Drawing.Size(777, 423);
            this.gbModPartSearchResults.TabIndex = 259;
            this.gbModPartSearchResults.TabStop = false;
            this.gbModPartSearchResults.Text = "Part Number Search Results";
            this.gbModPartSearchResults.Visible = false;
            // 
            // dgvPartSearchResults
            // 
            this.dgvPartSearchResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartSearchResults.Location = new System.Drawing.Point(15, 20);
            this.dgvPartSearchResults.MultiSelect = false;
            this.dgvPartSearchResults.Name = "dgvPartSearchResults";
            this.dgvPartSearchResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartSearchResults.Size = new System.Drawing.Size(747, 365);
            this.dgvPartSearchResults.TabIndex = 5;
            this.dgvPartSearchResults.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartSearchResults_CellMouseDoubleClick);
            // 
            // btnSearchResultsOK
            // 
            this.btnSearchResultsOK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchResultsOK.ForeColor = System.Drawing.Color.Blue;
            this.btnSearchResultsOK.Location = new System.Drawing.Point(587, 391);
            this.btnSearchResultsOK.Name = "btnSearchResultsOK";
            this.btnSearchResultsOK.Size = new System.Drawing.Size(75, 23);
            this.btnSearchResultsOK.TabIndex = 27;
            this.btnSearchResultsOK.Text = "OK";
            this.btnSearchResultsOK.UseVisualStyleBackColor = true;
            this.btnSearchResultsOK.Click += new System.EventHandler(this.btnSearchResultsOK_Click);
            // 
            // btnSearchResultsCancel
            // 
            this.btnSearchResultsCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchResultsCancel.ForeColor = System.Drawing.Color.Red;
            this.btnSearchResultsCancel.Location = new System.Drawing.Point(687, 391);
            this.btnSearchResultsCancel.Name = "btnSearchResultsCancel";
            this.btnSearchResultsCancel.Size = new System.Drawing.Size(75, 23);
            this.btnSearchResultsCancel.TabIndex = 26;
            this.btnSearchResultsCancel.Text = "Cancel";
            this.btnSearchResultsCancel.UseVisualStyleBackColor = true;
            this.btnSearchResultsCancel.Click += new System.EventHandler(this.btnSearchResultsCancel_Click);
            // 
            // buttonPartSearchOK
            // 
            this.buttonPartSearchOK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPartSearchOK.ForeColor = System.Drawing.Color.Blue;
            this.buttonPartSearchOK.Location = new System.Drawing.Point(378, 547);
            this.buttonPartSearchOK.Name = "buttonPartSearchOK";
            this.buttonPartSearchOK.Size = new System.Drawing.Size(75, 24);
            this.buttonPartSearchOK.TabIndex = 4;
            this.buttonPartSearchOK.Text = "OK";
            this.buttonPartSearchOK.UseVisualStyleBackColor = true;
            // 
            // buttonPartSearchCancel
            // 
            this.buttonPartSearchCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPartSearchCancel.ForeColor = System.Drawing.Color.Red;
            this.buttonPartSearchCancel.Location = new System.Drawing.Point(472, 547);
            this.buttonPartSearchCancel.Name = "buttonPartSearchCancel";
            this.buttonPartSearchCancel.Size = new System.Drawing.Size(75, 24);
            this.buttonPartSearchCancel.TabIndex = 3;
            this.buttonPartSearchCancel.Text = "Cancel";
            this.buttonPartSearchCancel.UseVisualStyleBackColor = true;
            // 
            // btnAddPart
            // 
            this.btnAddPart.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPart.ForeColor = System.Drawing.Color.Blue;
            this.btnAddPart.Location = new System.Drawing.Point(46, 16);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(85, 25);
            this.btnAddPart.TabIndex = 262;
            this.btnAddPart.Text = "Add Part";
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click);
            // 
            // gbAddModDelPart
            // 
            this.gbAddModDelPart.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gbAddModDelPart.Controls.Add(this.lbModParentPartNum);
            this.gbAddModDelPart.Controls.Add(this.label21);
            this.gbAddModDelPart.Controls.Add(this.txtModAsmSeq);
            this.gbAddModDelPart.Controls.Add(this.lbModCostMethod);
            this.gbAddModDelPart.Controls.Add(this.lbModPartStatus);
            this.gbAddModDelPart.Controls.Add(this.lbModJobMtlProgressRecID);
            this.gbAddModDelPart.Controls.Add(this.lbModRevNum);
            this.gbAddModDelPart.Controls.Add(this.label20);
            this.gbAddModDelPart.Controls.Add(this.txtModStatus);
            this.gbAddModDelPart.Controls.Add(this.label19);
            this.gbAddModDelPart.Controls.Add(this.txtModUnitCost);
            this.gbAddModDelPart.Controls.Add(this.cbModMotorType);
            this.gbAddModDelPart.Controls.Add(this.label18);
            this.gbAddModDelPart.Controls.Add(this.txtModUOM);
            this.gbAddModDelPart.Controls.Add(this.txtModPartCategory);
            this.gbAddModDelPart.Controls.Add(this.label10);
            this.gbAddModDelPart.Controls.Add(this.pictureBoxSearchIcon);
            this.gbAddModDelPart.Controls.Add(this.btnModUpdate);
            this.gbAddModDelPart.Controls.Add(this.btnModDelete);
            this.gbAddModDelPart.Controls.Add(this.btnModExit);
            this.gbAddModDelPart.Controls.Add(this.label11);
            this.gbAddModDelPart.Controls.Add(this.label12);
            this.gbAddModDelPart.Controls.Add(this.label13);
            this.gbAddModDelPart.Controls.Add(this.label14);
            this.gbAddModDelPart.Controls.Add(this.label15);
            this.gbAddModDelPart.Controls.Add(this.label16);
            this.gbAddModDelPart.Controls.Add(this.label17);
            this.gbAddModDelPart.Controls.Add(this.txtModRelOP);
            this.gbAddModDelPart.Controls.Add(this.txtModWhseLoc);
            this.gbAddModDelPart.Controls.Add(this.txtModPartQty);
            this.gbAddModDelPart.Controls.Add(this.txtModPartDes);
            this.gbAddModDelPart.Controls.Add(this.txtModSeqNo);
            this.gbAddModDelPart.Controls.Add(this.txtModPartNum);
            this.gbAddModDelPart.Location = new System.Drawing.Point(46, 72);
            this.gbAddModDelPart.Name = "gbAddModDelPart";
            this.gbAddModDelPart.Size = new System.Drawing.Size(1289, 101);
            this.gbAddModDelPart.TabIndex = 258;
            this.gbAddModDelPart.TabStop = false;
            this.gbAddModDelPart.Text = "Add/Modify/Delete";
            this.gbAddModDelPart.Visible = false;
            // 
            // lbModParentPartNum
            // 
            this.lbModParentPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModParentPartNum.Location = new System.Drawing.Point(749, 74);
            this.lbModParentPartNum.Name = "lbModParentPartNum";
            this.lbModParentPartNum.Size = new System.Drawing.Size(165, 15);
            this.lbModParentPartNum.TabIndex = 47;
            this.lbModParentPartNum.Text = "ParentPartNum";
            this.lbModParentPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbModParentPartNum.Visible = false;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Teal;
            this.label21.Location = new System.Drawing.Point(19, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 15);
            this.label21.TabIndex = 46;
            this.label21.Text = "Asm Seq";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtModAsmSeq
            // 
            this.txtModAsmSeq.BackColor = System.Drawing.Color.White;
            this.txtModAsmSeq.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModAsmSeq.ForeColor = System.Drawing.Color.Blue;
            this.txtModAsmSeq.Location = new System.Drawing.Point(19, 35);
            this.txtModAsmSeq.Name = "txtModAsmSeq";
            this.txtModAsmSeq.Size = new System.Drawing.Size(74, 22);
            this.txtModAsmSeq.TabIndex = 1;
            // 
            // lbModCostMethod
            // 
            this.lbModCostMethod.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModCostMethod.Location = new System.Drawing.Point(600, 74);
            this.lbModCostMethod.Name = "lbModCostMethod";
            this.lbModCostMethod.Size = new System.Drawing.Size(165, 15);
            this.lbModCostMethod.TabIndex = 44;
            this.lbModCostMethod.Text = "CostMethod";
            this.lbModCostMethod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbModCostMethod.Visible = false;
            // 
            // lbModPartStatus
            // 
            this.lbModPartStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModPartStatus.Location = new System.Drawing.Point(452, 74);
            this.lbModPartStatus.Name = "lbModPartStatus";
            this.lbModPartStatus.Size = new System.Drawing.Size(165, 15);
            this.lbModPartStatus.TabIndex = 43;
            this.lbModPartStatus.Text = "PartStatus";
            this.lbModPartStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbModPartStatus.Visible = false;
            // 
            // lbModJobMtlProgressRecID
            // 
            this.lbModJobMtlProgressRecID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModJobMtlProgressRecID.Location = new System.Drawing.Point(296, 74);
            this.lbModJobMtlProgressRecID.Name = "lbModJobMtlProgressRecID";
            this.lbModJobMtlProgressRecID.Size = new System.Drawing.Size(165, 15);
            this.lbModJobMtlProgressRecID.TabIndex = 42;
            this.lbModJobMtlProgressRecID.Text = "PROGRESS_RECID";
            this.lbModJobMtlProgressRecID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbModJobMtlProgressRecID.Visible = false;
            // 
            // lbModRevNum
            // 
            this.lbModRevNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModRevNum.Location = new System.Drawing.Point(103, 74);
            this.lbModRevNum.Name = "lbModRevNum";
            this.lbModRevNum.Size = new System.Drawing.Size(165, 15);
            this.lbModRevNum.TabIndex = 41;
            this.lbModRevNum.Text = "RevisionNumber";
            this.lbModRevNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbModRevNum.Visible = false;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(1175, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 15);
            this.label20.TabIndex = 40;
            this.label20.Text = "Status";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtModStatus
            // 
            this.txtModStatus.BackColor = System.Drawing.Color.White;
            this.txtModStatus.Enabled = false;
            this.txtModStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModStatus.ForeColor = System.Drawing.Color.Blue;
            this.txtModStatus.Location = new System.Drawing.Point(1175, 35);
            this.txtModStatus.Name = "txtModStatus";
            this.txtModStatus.Size = new System.Drawing.Size(99, 22);
            this.txtModStatus.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(1110, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 15);
            this.label19.TabIndex = 38;
            this.label19.Text = "Unit Cost";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtModUnitCost
            // 
            this.txtModUnitCost.BackColor = System.Drawing.Color.White;
            this.txtModUnitCost.Enabled = false;
            this.txtModUnitCost.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModUnitCost.ForeColor = System.Drawing.Color.Blue;
            this.txtModUnitCost.Location = new System.Drawing.Point(1110, 35);
            this.txtModUnitCost.Name = "txtModUnitCost";
            this.txtModUnitCost.Size = new System.Drawing.Size(60, 22);
            this.txtModUnitCost.TabIndex = 11;
            // 
            // cbModMotorType
            // 
            this.cbModMotorType.Enabled = false;
            this.cbModMotorType.FormattingEnabled = true;
            this.cbModMotorType.ItemHeight = 13;
            this.cbModMotorType.Items.AddRange(new object[] {
            "Please Select Motor Type",
            "Condensor",
            "ERV",
            "Indoor FAN",
            "Powered Exhaust"});
            this.cbModMotorType.Location = new System.Drawing.Point(718, 35);
            this.cbModMotorType.Name = "cbModMotorType";
            this.cbModMotorType.Size = new System.Drawing.Size(121, 21);
            this.cbModMotorType.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(730, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 15);
            this.label18.TabIndex = 35;
            this.label18.Text = "Motor Type";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtModUOM
            // 
            this.txtModUOM.BackColor = System.Drawing.Color.White;
            this.txtModUOM.Enabled = false;
            this.txtModUOM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModUOM.ForeColor = System.Drawing.Color.Blue;
            this.txtModUOM.Location = new System.Drawing.Point(930, 35);
            this.txtModUOM.Name = "txtModUOM";
            this.txtModUOM.Size = new System.Drawing.Size(45, 22);
            this.txtModUOM.TabIndex = 8;
            // 
            // txtModPartCategory
            // 
            this.txtModPartCategory.BackColor = System.Drawing.Color.White;
            this.txtModPartCategory.Enabled = false;
            this.txtModPartCategory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModPartCategory.ForeColor = System.Drawing.Color.Blue;
            this.txtModPartCategory.Location = new System.Drawing.Point(603, 35);
            this.txtModPartCategory.Name = "txtModPartCategory";
            this.txtModPartCategory.Size = new System.Drawing.Size(110, 22);
            this.txtModPartCategory.TabIndex = 5;
            this.txtModPartCategory.TextChanged += new System.EventHandler(this.txtModPartCategory_TextChanged);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(611, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 15);
            this.label10.TabIndex = 32;
            this.label10.Text = "Part Category";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxSearchIcon
            // 
            this.pictureBoxSearchIcon.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSearchIcon.Image")));
            this.pictureBoxSearchIcon.Location = new System.Drawing.Point(342, 37);
            this.pictureBoxSearchIcon.Name = "pictureBoxSearchIcon";
            this.pictureBoxSearchIcon.Size = new System.Drawing.Size(22, 20);
            this.pictureBoxSearchIcon.TabIndex = 29;
            this.pictureBoxSearchIcon.TabStop = false;
            this.pictureBoxSearchIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxSearchIcon_MouseClick);
            // 
            // btnModUpdate
            // 
            this.btnModUpdate.Enabled = false;
            this.btnModUpdate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModUpdate.ForeColor = System.Drawing.Color.Blue;
            this.btnModUpdate.Location = new System.Drawing.Point(1008, 66);
            this.btnModUpdate.Name = "btnModUpdate";
            this.btnModUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnModUpdate.TabIndex = 13;
            this.btnModUpdate.Text = "Update";
            this.btnModUpdate.UseVisualStyleBackColor = true;
            this.btnModUpdate.Click += new System.EventHandler(this.btnModUpdate_Click);
            // 
            // btnModDelete
            // 
            this.btnModDelete.Enabled = false;
            this.btnModDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModDelete.ForeColor = System.Drawing.Color.Black;
            this.btnModDelete.Location = new System.Drawing.Point(1103, 66);
            this.btnModDelete.Name = "btnModDelete";
            this.btnModDelete.Size = new System.Drawing.Size(75, 23);
            this.btnModDelete.TabIndex = 14;
            this.btnModDelete.Text = "Delete";
            this.btnModDelete.UseVisualStyleBackColor = true;
            this.btnModDelete.Click += new System.EventHandler(this.btnModDelete_Click);
            // 
            // btnModExit
            // 
            this.btnModExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModExit.ForeColor = System.Drawing.Color.Red;
            this.btnModExit.Location = new System.Drawing.Point(1199, 66);
            this.btnModExit.Name = "btnModExit";
            this.btnModExit.Size = new System.Drawing.Size(75, 23);
            this.btnModExit.TabIndex = 15;
            this.btnModExit.Text = "Exit";
            this.btnModExit.UseVisualStyleBackColor = true;
            this.btnModExit.Click += new System.EventHandler(this.btnModExit_Click);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(1044, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 15);
            this.label11.TabIndex = 23;
            this.label11.Text = "RelOP";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(978, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 15);
            this.label12.TabIndex = 22;
            this.label12.Text = "WHSE";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(931, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 15);
            this.label13.TabIndex = 21;
            this.label13.Text = "UOM";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(843, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 15);
            this.label14.TabIndex = 20;
            this.label14.Text = "Req Qty";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(372, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(228, 15);
            this.label15.TabIndex = 19;
            this.label15.Text = "Part Description";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Teal;
            this.label16.Location = new System.Drawing.Point(98, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 15);
            this.label16.TabIndex = 18;
            this.label16.Text = "Seq No";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(180, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(165, 15);
            this.label17.TabIndex = 17;
            this.label17.Text = "Part Number";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtModRelOP
            // 
            this.txtModRelOP.BackColor = System.Drawing.Color.White;
            this.txtModRelOP.Enabled = false;
            this.txtModRelOP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModRelOP.ForeColor = System.Drawing.Color.Blue;
            this.txtModRelOP.Location = new System.Drawing.Point(1045, 35);
            this.txtModRelOP.Name = "txtModRelOP";
            this.txtModRelOP.Size = new System.Drawing.Size(60, 22);
            this.txtModRelOP.TabIndex = 10;
            // 
            // txtModWhseLoc
            // 
            this.txtModWhseLoc.BackColor = System.Drawing.Color.White;
            this.txtModWhseLoc.Enabled = false;
            this.txtModWhseLoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModWhseLoc.ForeColor = System.Drawing.Color.Blue;
            this.txtModWhseLoc.Location = new System.Drawing.Point(980, 35);
            this.txtModWhseLoc.Name = "txtModWhseLoc";
            this.txtModWhseLoc.Size = new System.Drawing.Size(60, 22);
            this.txtModWhseLoc.TabIndex = 9;
            // 
            // txtModPartQty
            // 
            this.txtModPartQty.BackColor = System.Drawing.Color.White;
            this.txtModPartQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModPartQty.ForeColor = System.Drawing.Color.Blue;
            this.txtModPartQty.Location = new System.Drawing.Point(844, 35);
            this.txtModPartQty.Name = "txtModPartQty";
            this.txtModPartQty.Size = new System.Drawing.Size(81, 22);
            this.txtModPartQty.TabIndex = 7;
            // 
            // txtModPartDes
            // 
            this.txtModPartDes.BackColor = System.Drawing.Color.White;
            this.txtModPartDes.Enabled = false;
            this.txtModPartDes.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModPartDes.ForeColor = System.Drawing.Color.Blue;
            this.txtModPartDes.Location = new System.Drawing.Point(370, 35);
            this.txtModPartDes.Name = "txtModPartDes";
            this.txtModPartDes.Size = new System.Drawing.Size(228, 22);
            this.txtModPartDes.TabIndex = 4;
            // 
            // txtModSeqNo
            // 
            this.txtModSeqNo.BackColor = System.Drawing.Color.White;
            this.txtModSeqNo.Enabled = false;
            this.txtModSeqNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModSeqNo.ForeColor = System.Drawing.Color.Blue;
            this.txtModSeqNo.Location = new System.Drawing.Point(98, 35);
            this.txtModSeqNo.Name = "txtModSeqNo";
            this.txtModSeqNo.Size = new System.Drawing.Size(74, 22);
            this.txtModSeqNo.TabIndex = 2;
            // 
            // txtModPartNum
            // 
            this.txtModPartNum.AcceptsReturn = true;
            this.txtModPartNum.BackColor = System.Drawing.Color.White;
            this.txtModPartNum.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModPartNum.ForeColor = System.Drawing.Color.Blue;
            this.txtModPartNum.Location = new System.Drawing.Point(177, 35);
            this.txtModPartNum.Name = "txtModPartNum";
            this.txtModPartNum.Size = new System.Drawing.Size(165, 22);
            this.txtModPartNum.TabIndex = 3;
            this.txtModPartNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtModPartNum_KeyDown);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Location = new System.Drawing.Point(737, -21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 20);
            this.label7.TabIndex = 257;
            this.label7.Text = "MCA:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Teal;
            this.label8.Location = new System.Drawing.Point(678, -21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 20);
            this.label8.TabIndex = 256;
            this.label8.Text = "MCA:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(732, -21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 20);
            this.label5.TabIndex = 257;
            this.label5.Text = "MCA:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(673, -21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.TabIndex = 256;
            this.label6.Text = "MCA:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvBOM
            // 
            this.dgvBOM.AllowUserToAddRows = false;
            this.dgvBOM.AllowUserToDeleteRows = false;
            this.dgvBOM.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvBOM.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBOM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBOM.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvBOM.Location = new System.Drawing.Point(7, 50);
            this.dgvBOM.MultiSelect = false;
            this.dgvBOM.Name = "dgvBOM";
            this.dgvBOM.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBOM.Size = new System.Drawing.Size(1453, 562);
            this.dgvBOM.TabIndex = 0;
            this.dgvBOM.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvBOM_CellMouseDoubleClick);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(357, 16);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(85, 25);
            this.btnExit.TabIndex = 249;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Teal;
            this.btnSave.Location = new System.Drawing.Point(253, 16);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 25);
            this.btnSave.TabIndex = 251;
            this.btnSave.Text = "Save BOM";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Blue;
            this.btnPrint.Location = new System.Drawing.Point(149, 16);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(85, 25);
            this.btnPrint.TabIndex = 250;
            this.btnPrint.Text = "Print BOM";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // lbLastUpdateDate
            // 
            this.lbLastUpdateDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateDate.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateDate.Location = new System.Drawing.Point(1312, 51);
            this.lbLastUpdateDate.Name = "lbLastUpdateDate";
            this.lbLastUpdateDate.Size = new System.Drawing.Size(100, 20);
            this.lbLastUpdateDate.TabIndex = 238;
            this.lbLastUpdateDate.Text = "??/??/????";
            this.lbLastUpdateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(1173, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 237;
            this.label3.Text = "Last Updated Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbBOMCreationDate
            // 
            this.lbBOMCreationDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBOMCreationDate.ForeColor = System.Drawing.Color.Black;
            this.lbBOMCreationDate.Location = new System.Drawing.Point(1312, 30);
            this.lbBOMCreationDate.Name = "lbBOMCreationDate";
            this.lbBOMCreationDate.Size = new System.Drawing.Size(100, 20);
            this.lbBOMCreationDate.TabIndex = 236;
            this.lbBOMCreationDate.Text = "??/??/????";
            this.lbBOMCreationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Teal;
            this.label27.Location = new System.Drawing.Point(1173, 30);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(140, 20);
            this.label27.TabIndex = 235;
            this.label27.Text = "BOM Creation Date:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbOrderDate
            // 
            this.lbOrderDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderDate.ForeColor = System.Drawing.Color.Black;
            this.lbOrderDate.Location = new System.Drawing.Point(144, 30);
            this.lbOrderDate.Name = "lbOrderDate";
            this.lbOrderDate.Size = new System.Drawing.Size(100, 20);
            this.lbOrderDate.TabIndex = 234;
            this.lbOrderDate.Text = "??/??/????";
            this.lbOrderDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Teal;
            this.label26.Location = new System.Drawing.Point(21, 30);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(120, 20);
            this.label26.TabIndex = 233;
            this.label26.Text = "Order Date:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbCompleteDate
            // 
            this.lbCompleteDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCompleteDate.ForeColor = System.Drawing.Color.Black;
            this.lbCompleteDate.Location = new System.Drawing.Point(145, 91);
            this.lbCompleteDate.Name = "lbCompleteDate";
            this.lbCompleteDate.Size = new System.Drawing.Size(94, 20);
            this.lbCompleteDate.TabIndex = 244;
            this.lbCompleteDate.Text = "??/??/????";
            this.lbCompleteDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(21, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 243;
            this.label4.Text = "Complete Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbProdStartDate
            // 
            this.lbProdStartDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProdStartDate.ForeColor = System.Drawing.Color.Black;
            this.lbProdStartDate.Location = new System.Drawing.Point(145, 71);
            this.lbProdStartDate.Name = "lbProdStartDate";
            this.lbProdStartDate.Size = new System.Drawing.Size(94, 20);
            this.lbProdStartDate.TabIndex = 242;
            this.lbProdStartDate.Text = "??/??/????";
            this.lbProdStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(21, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 241;
            this.label2.Text = "Prod Start Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbShipDate
            // 
            this.lbShipDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShipDate.ForeColor = System.Drawing.Color.Black;
            this.lbShipDate.Location = new System.Drawing.Point(144, 51);
            this.lbShipDate.Name = "lbShipDate";
            this.lbShipDate.Size = new System.Drawing.Size(94, 20);
            this.lbShipDate.TabIndex = 240;
            this.lbShipDate.Text = "??/??/????";
            this.lbShipDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Teal;
            this.label32.Location = new System.Drawing.Point(21, 51);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(120, 20);
            this.label32.TabIndex = 239;
            this.label32.Text = "Ship Date:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbMsg
            // 
            this.lbMsg.BackColor = System.Drawing.Color.LightSalmon;
            this.lbMsg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMsg.Location = new System.Drawing.Point(17, 739);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(1456, 23);
            this.lbMsg.TabIndex = 252;
            this.lbMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // msDisplayBOM_Menu
            // 
            this.msDisplayBOM_Menu.BackColor = System.Drawing.Color.LightSalmon;
            this.msDisplayBOM_Menu.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msDisplayBOM_Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.msDisplayBOM_Menu.Location = new System.Drawing.Point(0, 0);
            this.msDisplayBOM_Menu.Name = "msDisplayBOM_Menu";
            this.msDisplayBOM_Menu.Size = new System.Drawing.Size(1370, 24);
            this.msDisplayBOM_Menu.TabIndex = 253;
            this.msDisplayBOM_Menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.recalculateMCAToolStripMenuItem,
            this.saveBOMToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // recalculateMCAToolStripMenuItem
            // 
            this.recalculateMCAToolStripMenuItem.Name = "recalculateMCAToolStripMenuItem";
            this.recalculateMCAToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.recalculateMCAToolStripMenuItem.Text = "RecalculateMCA";
            this.recalculateMCAToolStripMenuItem.Click += new System.EventHandler(this.recalculateMCAToolStripMenuItem_Click);
            // 
            // saveBOMToolStripMenuItem
            // 
            this.saveBOMToolStripMenuItem.Name = "saveBOMToolStripMenuItem";
            this.saveBOMToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveBOMToolStripMenuItem.Text = "SaveBOM";
            this.saveBOMToolStripMenuItem.Click += new System.EventHandler(this.saveBOMToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(1177, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 20);
            this.label1.TabIndex = 254;
            this.label1.Text = "MCA:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMCA
            // 
            this.lbMCA.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMCA.ForeColor = System.Drawing.Color.Red;
            this.lbMCA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbMCA.Location = new System.Drawing.Point(1225, 73);
            this.lbMCA.Name = "lbMCA";
            this.lbMCA.Size = new System.Drawing.Size(73, 20);
            this.lbMCA.TabIndex = 255;
            this.lbMCA.Text = "MCA";
            this.lbMCA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMOP
            // 
            this.lbMOP.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMOP.ForeColor = System.Drawing.Color.Red;
            this.lbMOP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbMOP.Location = new System.Drawing.Point(1365, 73);
            this.lbMOP.Name = "lbMOP";
            this.lbMOP.Size = new System.Drawing.Size(73, 20);
            this.lbMOP.TabIndex = 257;
            this.lbMOP.Text = "MOP";
            this.lbMOP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Black;
            this.lbCustName.Location = new System.Drawing.Point(375, 75);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(720, 20);
            this.lbCustName.TabIndex = 258;
            this.lbCustName.Text = "Customer Name";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Black;
            this.lbModelNo.Location = new System.Drawing.Point(375, 99);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(720, 20);
            this.lbModelNo.TabIndex = 259;
            this.lbModelNo.Text = "123456789-123456789-123456789-123456789-123456789-1234567890";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbRawMOP
            // 
            this.lbRawMOP.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRawMOP.ForeColor = System.Drawing.Color.Blue;
            this.lbRawMOP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbRawMOP.Location = new System.Drawing.Point(1078, 81);
            this.lbRawMOP.Name = "lbRawMOP";
            this.lbRawMOP.Size = new System.Drawing.Size(73, 20);
            this.lbRawMOP.TabIndex = 260;
            this.lbRawMOP.Text = "RawMOP";
            this.lbRawMOP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbRawMOP.Visible = false;
            // 
            // lbFlowRate
            // 
            this.lbFlowRate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFlowRate.ForeColor = System.Drawing.Color.Blue;
            this.lbFlowRate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbFlowRate.Location = new System.Drawing.Point(1078, 43);
            this.lbFlowRate.Name = "lbFlowRate";
            this.lbFlowRate.Size = new System.Drawing.Size(73, 20);
            this.lbFlowRate.TabIndex = 261;
            this.lbFlowRate.Text = "FlowRate";
            this.lbFlowRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbFlowRate.Visible = false;
            // 
            // lbEnteringTemp
            // 
            this.lbEnteringTemp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnteringTemp.ForeColor = System.Drawing.Color.Blue;
            this.lbEnteringTemp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbEnteringTemp.Location = new System.Drawing.Point(1078, 26);
            this.lbEnteringTemp.Name = "lbEnteringTemp";
            this.lbEnteringTemp.Size = new System.Drawing.Size(73, 20);
            this.lbEnteringTemp.TabIndex = 261;
            this.lbEnteringTemp.Text = "EnteringTemp";
            this.lbEnteringTemp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbEnteringTemp.Visible = false;
            // 
            // lbVoltage
            // 
            this.lbVoltage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVoltage.ForeColor = System.Drawing.Color.Blue;
            this.lbVoltage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbVoltage.Location = new System.Drawing.Point(1082, 101);
            this.lbVoltage.Name = "lbVoltage";
            this.lbVoltage.Size = new System.Drawing.Size(73, 18);
            this.lbVoltage.TabIndex = 263;
            this.lbVoltage.Text = "Voltage";
            this.lbVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbVoltage.Visible = false;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Teal;
            this.label9.Location = new System.Drawing.Point(1317, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 20);
            this.label9.TabIndex = 264;
            this.label9.Text = "MOP:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbBOM_CreateBy
            // 
            this.lbBOM_CreateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBOM_CreateBy.ForeColor = System.Drawing.Color.Black;
            this.lbBOM_CreateBy.Location = new System.Drawing.Point(1394, 30);
            this.lbBOM_CreateBy.Name = "lbBOM_CreateBy";
            this.lbBOM_CreateBy.Size = new System.Drawing.Size(90, 20);
            this.lbBOM_CreateBy.TabIndex = 265;
            this.lbBOM_CreateBy.Text = "??/??/????";
            this.lbBOM_CreateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLastUpdateBy
            // 
            this.lbLastUpdateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateBy.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateBy.Location = new System.Drawing.Point(1394, 51);
            this.lbLastUpdateBy.Name = "lbLastUpdateBy";
            this.lbLastUpdateBy.Size = new System.Drawing.Size(90, 20);
            this.lbLastUpdateBy.TabIndex = 266;
            this.lbLastUpdateBy.Text = "??/??/????";
            this.lbLastUpdateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Engravers MT", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.Blue;
            this.lbEnvironment.Location = new System.Drawing.Point(434, 24);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(592, 25);
            this.lbEnvironment.TabIndex = 267;
            this.lbEnvironment.Text = "Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(633, 52);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 20);
            this.label25.TabIndex = 269;
            this.label25.Text = "Job #:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Black;
            this.lbJobNum.Location = new System.Drawing.Point(712, 52);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(247, 20);
            this.lbJobNum.TabIndex = 268;
            this.lbJobNum.Text = "??????-??-??";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCir1Label
            // 
            this.lbCir1Label.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCir1Label.ForeColor = System.Drawing.Color.Teal;
            this.lbCir1Label.Location = new System.Drawing.Point(1177, 93);
            this.lbCir1Label.Name = "lbCir1Label";
            this.lbCir1Label.Size = new System.Drawing.Size(65, 20);
            this.lbCir1Label.TabIndex = 270;
            this.lbCir1Label.Text = "Circuit1:";
            this.lbCir1Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCir2Label
            // 
            this.lbCir2Label.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCir2Label.ForeColor = System.Drawing.Color.Teal;
            this.lbCir2Label.Location = new System.Drawing.Point(1320, 93);
            this.lbCir2Label.Name = "lbCir2Label";
            this.lbCir2Label.Size = new System.Drawing.Size(65, 20);
            this.lbCir2Label.TabIndex = 271;
            this.lbCir2Label.Text = "Circuit2:";
            this.lbCir2Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCir1Chrg
            // 
            this.lbCir1Chrg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCir1Chrg.ForeColor = System.Drawing.Color.Blue;
            this.lbCir1Chrg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbCir1Chrg.Location = new System.Drawing.Point(1237, 93);
            this.lbCir1Chrg.Name = "lbCir1Chrg";
            this.lbCir1Chrg.Size = new System.Drawing.Size(73, 20);
            this.lbCir1Chrg.TabIndex = 272;
            this.lbCir1Chrg.Text = "Circuit 1";
            this.lbCir1Chrg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCir2Chrg
            // 
            this.lbCir2Chrg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCir2Chrg.ForeColor = System.Drawing.Color.Blue;
            this.lbCir2Chrg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbCir2Chrg.Location = new System.Drawing.Point(1383, 93);
            this.lbCir2Chrg.Name = "lbCir2Chrg";
            this.lbCir2Chrg.Size = new System.Drawing.Size(73, 20);
            this.lbCir2Chrg.TabIndex = 273;
            this.lbCir2Chrg.Text = "Circuit 2";
            this.lbCir2Chrg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbUnitType
            // 
            this.lbUnitType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUnitType.ForeColor = System.Drawing.Color.Blue;
            this.lbUnitType.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbUnitType.Location = new System.Drawing.Point(1078, 64);
            this.lbUnitType.Name = "lbUnitType";
            this.lbUnitType.Size = new System.Drawing.Size(73, 20);
            this.lbUnitType.TabIndex = 274;
            this.lbUnitType.Text = "UnitType";
            this.lbUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbUnitType.Visible = false;
            // 
            // frmDisplayBOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.lbUnitType);
            this.Controls.Add(this.lbCir2Chrg);
            this.Controls.Add(this.lbCir1Chrg);
            this.Controls.Add(this.lbCir2Label);
            this.Controls.Add(this.lbCir1Label);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbJobNum);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.lbLastUpdateBy);
            this.Controls.Add(this.lbShipDate);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.lbOrderDate);
            this.Controls.Add(this.lbBOM_CreateBy);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.lbVoltage);
            this.Controls.Add(this.lbEnteringTemp);
            this.Controls.Add(this.lbFlowRate);
            this.Controls.Add(this.lbRawMOP);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lbBOMCreationDate);
            this.Controls.Add(this.lbMOP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.msDisplayBOM_Menu);
            this.Controls.Add(this.lbLastUpdateDate);
            this.Controls.Add(this.lbMsg);
            this.Controls.Add(this.lbMCA);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbCompleteDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbProdStartDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gbBOM);
            this.Name = "frmDisplayBOM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "\\";
            this.Load += new System.EventHandler(this.frmDisplayBOM_Load);
            this.gbBOM.ResumeLayout(false);
            this.gbSaveBomProgress.ResumeLayout(false);
            this.gbBOMPrintOptions.ResumeLayout(false);
            this.gbBOMRptType.ResumeLayout(false);
            this.gbBOMRptType.PerformLayout();
            this.gbModPartSearchResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartSearchResults)).EndInit();
            this.gbAddModDelPart.ResumeLayout(false);
            this.gbAddModDelPart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSearchIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBOM)).EndInit();
            this.msDisplayBOM_Menu.ResumeLayout(false);
            this.msDisplayBOM_Menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbBOM;
        public System.Windows.Forms.DataGridView dgvBOM;
        public System.Windows.Forms.Label lbLastUpdateDate;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label lbBOMCreationDate;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label lbOrderDate;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label lbCompleteDate;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lbProdStartDate;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lbShipDate;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.Label lbMsg;
        private System.Windows.Forms.MenuStrip msDisplayBOM_Menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recalculateMCAToolStripMenuItem;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lbMCA;
        public System.Windows.Forms.Label lbMOP;
        public System.Windows.Forms.Label lbCustName;
        public System.Windows.Forms.Label lbModelNo;
        public System.Windows.Forms.Label lbRawMOP;
        public System.Windows.Forms.Label lbFlowRate;
        public System.Windows.Forms.Label lbEnteringTemp;
        public System.Windows.Forms.GroupBox gbAddModDelPart;
        private System.Windows.Forms.PictureBox pictureBoxSearchIcon;
        private System.Windows.Forms.Button btnModUpdate;
        private System.Windows.Forms.Button btnModDelete;
        private System.Windows.Forms.Button btnModExit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtModRelOP;
        public System.Windows.Forms.TextBox txtModWhseLoc;
        public System.Windows.Forms.TextBox txtModPartQty;
        public System.Windows.Forms.TextBox txtModPartDes;
        public System.Windows.Forms.TextBox txtModSeqNo;
        public System.Windows.Forms.TextBox txtModPartNum;
        public System.Windows.Forms.Button btnAddPart;
        public System.Windows.Forms.TextBox txtModPartCategory;
        private System.Windows.Forms.ComboBox cbModMotorType;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtModUOM;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox txtModStatus;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtModUnitCost;
        private System.Windows.Forms.Label lbModRevNum;
        public System.Windows.Forms.GroupBox gbModPartSearchResults;
        private System.Windows.Forms.DataGridView dgvPartSearchResults;
        private System.Windows.Forms.Button buttonPartSearchOK;
        private System.Windows.Forms.Button buttonPartSearchCancel;
        private System.Windows.Forms.Button btnSearchResultsCancel;
        private System.Windows.Forms.Label lbModJobMtlProgressRecID;
        private System.Windows.Forms.Label lbModPartStatus;
        private System.Windows.Forms.Label lbModCostMethod;
        private System.Windows.Forms.Button btnSearchResultsOK;
        private System.Windows.Forms.GroupBox gbBOMPrintOptions;
        private System.Windows.Forms.Button buttonPrintOpCancel;
        private System.Windows.Forms.Button buttonPrintOpOK;
        private System.Windows.Forms.GroupBox gbBOMRptType;
        private System.Windows.Forms.RadioButton rbCriticalCompBOM;
        private System.Windows.Forms.RadioButton rbEngBOM;
        public System.Windows.Forms.Label lbVoltage;
        public System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton rbJobNumSticker;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox txtModAsmSeq;
        public System.Windows.Forms.Label lbBOM_CreateBy;
        public System.Windows.Forms.Label lbLastUpdateBy;
        public System.Windows.Forms.Label lbEnvironment;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbJobNum;
        private System.Windows.Forms.ToolStripMenuItem saveBOMToolStripMenuItem;
        public System.Windows.Forms.Label lbCir1Label;
        public System.Windows.Forms.Label lbCir2Label;
        public System.Windows.Forms.Label lbCir1Chrg;
        public System.Windows.Forms.Label lbCir2Chrg;
        private System.Windows.Forms.RadioButton rbRfgCompRpt;
        public System.Windows.Forms.Label lbUnitType;
        private System.Windows.Forms.Label lbModParentPartNum;
        public System.Windows.Forms.GroupBox gbSaveBomProgress;
        private System.Windows.Forms.Label lbSaveMsg;

    }
}