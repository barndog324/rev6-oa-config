﻿//using Ice;
//using Erp.Adapters;
//using Erp.BO;
//using Erp.Contracts;
//using Ice.Core.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace OA_Config_Rev6
{   
    class KineticBuild_BOM
    {
        //private Ice.Core.Session epiSession;
        //private Erp.ErpContext dataContext;
        private string epicorServer;
        private string epicorSqlConnectionString;

        MainBO objMain = new MainBO();

        public KineticBuild_BOM(string epicorServer, string epicorSqlConnectionString)
        {
            this.epicorServer = epicorServer;
            this.epicorSqlConnectionString = epicorSqlConnectionString;
        }

        public string SaveBOM(string jobNumStr, DataTable dtBOM)
        {
            string errorMsg = "";
            string unitTypeStr = "";
            string unitPartNum = "";
            string curAsmPartNum = ""; ;
            string asmPartNum = "";
            string asmRevNum = "";
            string revisionNum = "";
            string ipPartNum = "";
            string stationLoc = "";
            string rowStationLoc = "";
            string subAsmPart = "";
            string subAsmMtlPart = "";
            string runOutStr = "";
            string rev6LogicalOpr = "41,51,72,85";
            string prodLine = "";
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

            int assemblySeq = 0;
            int oprSeq = 0;
            int relOp = 0;
            int jaIdx = 0;
            int jmIdx = 0;
            int parentAsm = 0;
            int curAsmSeq = 0;
            int priorAsmSeq = 0;

            byte buyToOrder = 0;
            byte runOut = 0;

            decimal qtyPerDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool firstError = true;
            bool firstInactive = true;
            bool inactivePartsFound = false;
            bool logicalOperation = false;
            //MessageBox.Show("MadeIt");      


            //if (envStr.Contains("kccwvpkntcapp01") == true)
            //{
            //    string path = @"\\kccwvpkntcapp01\default.sysconfig";
            //    epiSession = new Ice.Core.Session("epiagent", "epiagent", Ice.Core.Session.LicenseType.Default, path);

            //    ////this.epiSession = new Session("epiagent", "epiagent1", "HTTP://KCCWVPKNTCAPP01/KineticTest");    // Kinetic production
            //    ////this.epiSession = new Session("epiagent", "epiagent1", "HTTP://net.tcp.KCCWVPKNTCAPP01.kccinternational.local/KineticTest");
            //}
            //else
            //{
            //    //this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);   // Connect to Test2 epicor
            //}

            //DataTable dtJobOpr = objMain.GetOA_JobOperations(jobNumStr);

            //if (dtJobOpr.Rows.Count > 0)
            //{

            //    //JobEntry je = new JobEntry(ConnectionPool);

            //    using (Erp.Contracts.JobEntrySvcContract hJob = Ice.Assemblies.ServiceRenderer.GetService<Erp.Contracts.JobEntrySvcContract>(Db, true))
            //    {
            //        hQuote.GetByID(jobNumStr);
            //    } je = new JobEntryAdapter(epiSession);  

            //    try
            //    {
            //        JobEntryDataSet jeds = je.GetByID(jobNumStr);
            //        DataRow[] jhdr = jeds.JobHead.Select();

            //        if (jhdr[0]["JobEngineered"].ToString() == "True")
            //        {
            //            jobEngineered = true;
            //        }

            //        if (jhdr[0]["JobReleased"].ToString() == "True")
            //        {
            //            jobReleased = true;
            //        }

            //        if (jobEngineered == true || jobReleased == true) // If JobEngineered = True then un-engineer the job.
            //        {
            //            try
            //            {
            //                je.ValidateJobNum(jobNumStr);
            //            }
            //            catch (Exception ex)
            //            {
            //                errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
            //            }
            //            if (errorMsg.Length == 0)
            //            {
            //                try
            //                {
            //                    jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
            //                    je.ChangeJobHeadJobEngineered(jeds);
            //                }
            //                catch (Exception ex)
            //                {
            //                    errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
            //                }

            //                if (errorMsg.Length == 0)
            //                {
            //                    try
            //                    {
            //                        je.Update(jeds); //JobEntry dataset
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
            //                    }
            //                }
            //            }
            //        }

            //        if (errorMsg.Length == 0)
            //        {
            //            unitPartNum = jhdr[0]["PartNum"].ToString();
            //            unitTypeStr = unitPartNum.Substring(0, 4);

            //            var bomResult = dtBOM.AsEnumerable().Distinct();
            //            //var bomResult = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

            //            foreach (DataRow dRow in bomResult)
            //            {
            //                ipPartNum = dRow["PartNum"].ToString();

            //                try
            //                {
            //                    runOutStr = dRow["RunOut"].ToString();
            //                    runOut = byte.Parse(runOutStr);
            //                }
            //                catch
            //                {
            //                    MessageBox.Show("ERROR - PartNum => " + ipPartNum + " has a runout value problem");
            //                }

            //                if (runOut == 1)
            //                {
            //                    if (firstError)
            //                    {
            //                        firstError = false;
            //                        errorMsg = "WARNING - Part Numbers shown as a RunOut part. This is a potential issue.";
            //                    }
            //                    errorMsg += "\nPartNum = " + ipPartNum;
            //                }

            //                if (dRow["PartStatus"].ToString() == "InActive")
            //                {
            //                    if (firstInactive)
            //                    {
            //                        firstInactive = false;
            //                        inactivePartsFound = true;
            //                        errorMsg = "ERROR - Part Numbers shown as an InActive part. \nThis needs to be resolved before you can move on!";
            //                    }
            //                    errorMsg += "\nPartNum = " + ipPartNum;
            //                }
            //            }

            //            if (inactivePartsFound == false)
            //            {
            //                if (errorMsg.Length > 0)
            //                {
            //                    DialogResult result1 = MessageBox.Show(errorMsg +
            //                                                           " Press 'Yes' to continue saving BOM & 'No' to cancel!",
            //                                                           "WARNING WARNING WARNING",
            //                                                           MessageBoxButtons.YesNo,
            //                                                           MessageBoxIcon.Exclamation);
            //                    if (result1 == DialogResult.No)
            //                    {
            //                        return (errorMsg);
            //                    }
            //                }

            //                errorMsg = "";

            //                var bomResult2 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

            //                foreach (DataRow dRow in bomResult2)
            //                {
            //                    ipPartNum = dRow["PartNum"].ToString();
            //                    relOp = (int)dRow["RelOp"];

            //                    if (unitTypeStr.Contains("OA") == true && relOp < 40)
            //                    {
            //                        if (firstError)
            //                        {
            //                            firstError = false;
            //                            errorMsg = "ERROR - Related operation must point to a main line operation!";
            //                        }
            //                        errorMsg += "\nPartNum = " + ipPartNum + " - RelatedOp = " + relOp.ToString();
            //                    }
            //                }
            //            }
            //            //else
            //            //{
            //            //    MessageBox.Show("ERROR - Inactive parts");                      
            //            //}
            //        }

            //        if (errorMsg.Length == 0)
            //        {
            //            foreach (DataRow prow in dtJobOpr.Rows)
            //            {
            //                oprSeq = (int)prow["OprSeq"];
            //                prodLine = prow["OprSeq"].ToString();

            //                logicalOperation = false;

            //                if ((unitPartNum.StartsWith("OANG") == true) && (rev6LogicalOpr.Contains(oprSeq.ToString()) == true))
            //                {
            //                    logicalOperation = true;
            //                }

            //                if (logicalOperation == false)
            //                {
            //                    stationLoc = objMain.findStationLoc(oprSeq, unitTypeStr, prodLine);

            //                    //Process main assembly parts
            //                    var results = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("StationLoc") == stationLoc && dr.Field<string>("ConfigurablePart") == "No"));

            //                    int resCount = results.Count();

            //                    if (resCount > 0)
            //                    {
            //                        foreach (DataRow row in results)
            //                        {
            //                            ipPartNum = row["PartNum"].ToString();
            //                            //assemblySeq = (int)row["AssemblySeq"];
            //                            assemblySeq = 0;
            //                            qtyPerDec = (decimal)row["ReqQty"];
            //                            rowStationLoc = row["StationLoc"].ToString();
            //                            buyToOrder = (byte)row["BuyToOrder"];

            //                            if (jmIdx == 142)
            //                            {
            //                                string taco = "bell";
            //                            }

            //                            errorMsg = insertPartMtl(je, jobNumStr, jeds, ipPartNum, row, assemblySeq, oprSeq, jmIdx, buyToOrder);

            //                            if (errorMsg.Length == 0)
            //                            {
            //                                ++jmIdx;
            //                            }
            //                            else
            //                            {
            //                                //break;
            //                                return errorMsg;
            //                            }
            //                        }
            //                    }
            //                }
            //            }

            //            //if (errorMsg.Length == 0)
            //            //{
            //            //    var results2 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "No" && dr.Field<int>("AssemblySeq") > 0));

            //            //    foreach (DataRow row in results2)
            //            //    {
            //            //        ipPartNum = row["PartNum"].ToString();
            //            //        revisionNum = row["RevisionNum"].ToString();

            //            //        asmPartNum = ipPartNum;
            //            //        assemblySeq = (int)row["AssemblySeq"];
            //            //        subAsmPart = row["SubAssemblyPart"].ToString();
            //            //        subAsmMtlPart = row["SubAsmMtlPart"].ToString();

            //            //        if (asmPartNum != curAsmPartNum)
            //            //        {
            //            //            ++jaIdx;
            //            //            curAsmSeq = jaIdx;
            //            //            parentAsm = 0;
            //            //            priorAsmSeq = 0;
            //            //            oprSeq = 40;
            //            //            qtyPerDec = 1;

            //            //            DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
            //            //            if (dtRev.Rows.Count > 0)
            //            //            {
            //            //                DataRow drRev = dtRev.Rows[0];
            //            //                asmRevNum = drRev["RevisionNum"].ToString();
            //            //            }

            //            //            jaIdx = createSubAssembly(je, oprSeq, false, jobNumStr, jeds, asmPartNum, asmRevNum, 1,
            //            //                                     jaIdx, parentAsm, curAsmSeq, priorAsmSeq, out errorMsg);
            //            //            curAsmSeq = jaIdx;
            //            //            curAsmPartNum = asmPartNum;

            //            //            if (errorMsg.Length != 0)
            //            //            {
            //            //                break;
            //            //            }
            //            //        }
            //            //    }
            //            //}

            //            //Process the Configurable sub-assemblies
            //            if (errorMsg.Length == 0)
            //            {
            //                var results3 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

            //                foreach (DataRow row in results3)
            //                {
            //                    ipPartNum = row["PartNum"].ToString();
            //                    revisionNum = row["RevisionNum"].ToString();

            //                    asmPartNum = row["ParentPartNum"].ToString();
            //                    assemblySeq = (int)row["AssemblySeq"];
            //                    subAsmPart = row["SubAssemblyPart"].ToString();
            //                    subAsmMtlPart = row["SubAsmMtlPart"].ToString();
            //                    buyToOrder = (byte)row["BuyToOrder"];

            //                    if (asmPartNum != curAsmPartNum)
            //                    {
            //                        ++jaIdx;
            //                        curAsmSeq = jaIdx;
            //                        parentAsm = 0;
            //                        priorAsmSeq = 0;
            //                        //oprSeq = 40;

            //                        relOp = (int)row["RelOp"];
            //                        qtyPerDec = 1;

            //                        DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
            //                        if (dtRev.Rows.Count > 0)
            //                        {
            //                            DataRow drRev = dtRev.Rows[0];
            //                            asmRevNum = drRev["RevisionNum"].ToString();
            //                        }

            //                        jaIdx = createSubAssembly(je, relOp, true, jobNumStr, jeds, asmPartNum, asmRevNum, 1,
            //                                                jaIdx, parentAsm, curAsmSeq, priorAsmSeq, out errorMsg);
            //                        curAsmSeq = jaIdx;
            //                        curAsmPartNum = asmPartNum;
            //                    }

            //                    if (errorMsg.Length == 0)
            //                    {
            //                        ipPartNum = row["PartNum"].ToString();
            //                        assemblySeq = (int)row["AssemblySeq"];
            //                        qtyPerDec = (decimal)row["ReqQty"];
            //                        rowStationLoc = row["StationLoc"].ToString();
            //                        oprSeq = 10;

            //                        errorMsg = insertPartMtl(je, jobNumStr, jeds, ipPartNum, row, curAsmSeq, oprSeq, jmIdx, buyToOrder);
            //                    }
            //                    else
            //                    {
            //                        break;
            //                    }

            //                    if (errorMsg.Length == 0)
            //                    {
            //                        ++jmIdx;
            //                    }
            //                    else
            //                    {
            //                        break;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        errorMsg = ("ERROR - GetByID - Job# " + jobNumStr + " " + ex);
            //    }
            //}
            //else
            //{
            //    errorMsg = ("ERROR - This job has No operations setup in Epicor. Please contact Business Technology!");
            //}

            return errorMsg;
        }

        //public void AccessJob()
        //{

        //    using (JobEntrySvcContract hJob = Ice.Assemblies.ServiceRenderer.GetService<JobEntrySvcContract>(dataContext, true))
        //    {

        //        hJob.GetByID("JobNum");

        //    }

        //}

    }
}
