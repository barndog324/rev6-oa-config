﻿namespace OA_Config_Rev6
{
    partial class frmMSP_ModelNoConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbUnitType = new System.Windows.Forms.Label();
            this.lbLastUpdateBy = new System.Windows.Forms.Label();
            this.lbBomCreateBy = new System.Windows.Forms.Label();
            this.lbCustName = new System.Windows.Forms.Label();
            this.gbModelNoConfig = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbMSP_CoolingCoilFluidType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFilterOptions = new System.Windows.Forms.ComboBox();
            this.label194 = new System.Windows.Forms.Label();
            this.cbSupplyFanMotorSize = new System.Windows.Forms.ComboBox();
            this.label193 = new System.Windows.Forms.Label();
            this.cbSupplyFanMotorType = new System.Windows.Forms.ComboBox();
            this.label192 = new System.Windows.Forms.Label();
            this.cbSupplyFanWheel = new System.Windows.Forms.ComboBox();
            this.label191 = new System.Windows.Forms.Label();
            this.cbBypassDamper = new System.Windows.Forms.ComboBox();
            this.cbCoilCorrisionPackage = new System.Windows.Forms.ComboBox();
            this.cbCabinetDesign = new System.Windows.Forms.ComboBox();
            this.label161 = new System.Windows.Forms.Label();
            this.cbVoltage = new System.Windows.Forms.ComboBox();
            this.label162 = new System.Windows.Forms.Label();
            this.cbWarranty = new System.Windows.Forms.ComboBox();
            this.label166 = new System.Windows.Forms.Label();
            this.cbServiceLights = new System.Windows.Forms.ComboBox();
            this.label167 = new System.Windows.Forms.Label();
            this.cbElectricalOptions = new System.Windows.Forms.ComboBox();
            this.label168 = new System.Windows.Forms.Label();
            this.cbControlsDisplay = new System.Windows.Forms.ComboBox();
            this.label169 = new System.Windows.Forms.Label();
            this.cbControls = new System.Windows.Forms.ComboBox();
            this.label170 = new System.Windows.Forms.Label();
            this.cbCondensateOverflowSwitch = new System.Windows.Forms.ComboBox();
            this.label171 = new System.Windows.Forms.Label();
            this.cbSmokeDetector = new System.Windows.Forms.ComboBox();
            this.label172 = new System.Windows.Forms.Label();
            this.cbAccessories = new System.Windows.Forms.ComboBox();
            this.label173 = new System.Windows.Forms.Label();
            this.cbAirflowMonitoring = new System.Windows.Forms.ComboBox();
            this.label174 = new System.Windows.Forms.Label();
            this.cbAltitude = new System.Windows.Forms.ComboBox();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.labelCoilcorrisionPackage = new System.Windows.Forms.Label();
            this.cbCabinetConstructOutdoor = new System.Windows.Forms.ComboBox();
            this.label177 = new System.Windows.Forms.Label();
            this.cbHeatExchangerType = new System.Windows.Forms.ComboBox();
            this.label178 = new System.Windows.Forms.Label();
            this.cbHeatingCoilFluidType = new System.Windows.Forms.ComboBox();
            this.label179 = new System.Windows.Forms.Label();
            this.cbPostHeatingCoilSize = new System.Windows.Forms.ComboBox();
            this.label180 = new System.Windows.Forms.Label();
            this.cbPostHeatingCoilType = new System.Windows.Forms.ComboBox();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.cbCoilType = new System.Windows.Forms.ComboBox();
            this.cbModelSize = new System.Windows.Forms.ComboBox();
            this.label183 = new System.Windows.Forms.Label();
            this.cbConvenienceOutlet = new System.Windows.Forms.ComboBox();
            this.label184 = new System.Windows.Forms.Label();
            this.cbPostCoolingCoilSize = new System.Windows.Forms.ComboBox();
            this.label185 = new System.Windows.Forms.Label();
            this.cbCoilSize = new System.Windows.Forms.ComboBox();
            this.label186 = new System.Windows.Forms.Label();
            this.cbPostCoolingCoilType = new System.Windows.Forms.ComboBox();
            this.cbCabinetConstuctIndoor = new System.Windows.Forms.ComboBox();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.cbMajorDesignSeq = new System.Windows.Forms.ComboBox();
            this.label189 = new System.Windows.Forms.Label();
            this.cbUnitType = new System.Windows.Forms.ComboBox();
            this.label190 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.lbLastUpdateDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbCompleteDate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbProdStartDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbBOMCreationDate = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbShipDate = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lbOrderDate = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.btnParseModelNo = new System.Windows.Forms.Button();
            this.lbEnterModelNo = new System.Windows.Forms.Label();
            this.txtModelNo = new System.Windows.Forms.TextBox();
            this.btnCreateBOM = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label102 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this.label219 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this.label235 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.msConfigMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.gbModelNoConfig.SuspendLayout();
            this.msConfigMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbUnitType
            // 
            this.lbUnitType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUnitType.ForeColor = System.Drawing.Color.Black;
            this.lbUnitType.Location = new System.Drawing.Point(1336, 123);
            this.lbUnitType.Name = "lbUnitType";
            this.lbUnitType.Size = new System.Drawing.Size(100, 20);
            this.lbUnitType.TabIndex = 525;
            this.lbUnitType.Text = "UnitType";
            this.lbUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbUnitType.Visible = false;
            // 
            // lbLastUpdateBy
            // 
            this.lbLastUpdateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateBy.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateBy.Location = new System.Drawing.Point(236, 75);
            this.lbLastUpdateBy.Name = "lbLastUpdateBy";
            this.lbLastUpdateBy.Size = new System.Drawing.Size(165, 20);
            this.lbLastUpdateBy.TabIndex = 524;
            this.lbLastUpdateBy.Text = "??/??/????";
            this.lbLastUpdateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbBomCreateBy
            // 
            this.lbBomCreateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBomCreateBy.ForeColor = System.Drawing.Color.Black;
            this.lbBomCreateBy.Location = new System.Drawing.Point(236, 55);
            this.lbBomCreateBy.Name = "lbBomCreateBy";
            this.lbBomCreateBy.Size = new System.Drawing.Size(222, 20);
            this.lbBomCreateBy.TabIndex = 523;
            this.lbBomCreateBy.Text = "??/??/????";
            this.lbBomCreateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Black;
            this.lbCustName.Location = new System.Drawing.Point(449, 83);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(537, 25);
            this.lbCustName.TabIndex = 495;
            this.lbCustName.Text = "CustomerName";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbModelNoConfig
            // 
            this.gbModelNoConfig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gbModelNoConfig.Controls.Add(this.label8);
            this.gbModelNoConfig.Controls.Add(this.label9);
            this.gbModelNoConfig.Controls.Add(this.label7);
            this.gbModelNoConfig.Controls.Add(this.label6);
            this.gbModelNoConfig.Controls.Add(this.label5);
            this.gbModelNoConfig.Controls.Add(this.cbMSP_CoolingCoilFluidType);
            this.gbModelNoConfig.Controls.Add(this.label1);
            this.gbModelNoConfig.Controls.Add(this.cbFilterOptions);
            this.gbModelNoConfig.Controls.Add(this.label194);
            this.gbModelNoConfig.Controls.Add(this.cbSupplyFanMotorSize);
            this.gbModelNoConfig.Controls.Add(this.label193);
            this.gbModelNoConfig.Controls.Add(this.cbSupplyFanMotorType);
            this.gbModelNoConfig.Controls.Add(this.label192);
            this.gbModelNoConfig.Controls.Add(this.cbSupplyFanWheel);
            this.gbModelNoConfig.Controls.Add(this.label191);
            this.gbModelNoConfig.Controls.Add(this.cbBypassDamper);
            this.gbModelNoConfig.Controls.Add(this.cbCoilCorrisionPackage);
            this.gbModelNoConfig.Controls.Add(this.cbCabinetDesign);
            this.gbModelNoConfig.Controls.Add(this.label161);
            this.gbModelNoConfig.Controls.Add(this.cbVoltage);
            this.gbModelNoConfig.Controls.Add(this.label162);
            this.gbModelNoConfig.Controls.Add(this.cbWarranty);
            this.gbModelNoConfig.Controls.Add(this.label166);
            this.gbModelNoConfig.Controls.Add(this.cbServiceLights);
            this.gbModelNoConfig.Controls.Add(this.label167);
            this.gbModelNoConfig.Controls.Add(this.cbElectricalOptions);
            this.gbModelNoConfig.Controls.Add(this.label168);
            this.gbModelNoConfig.Controls.Add(this.cbControlsDisplay);
            this.gbModelNoConfig.Controls.Add(this.label169);
            this.gbModelNoConfig.Controls.Add(this.cbControls);
            this.gbModelNoConfig.Controls.Add(this.label170);
            this.gbModelNoConfig.Controls.Add(this.cbCondensateOverflowSwitch);
            this.gbModelNoConfig.Controls.Add(this.label171);
            this.gbModelNoConfig.Controls.Add(this.cbSmokeDetector);
            this.gbModelNoConfig.Controls.Add(this.label172);
            this.gbModelNoConfig.Controls.Add(this.cbAccessories);
            this.gbModelNoConfig.Controls.Add(this.label173);
            this.gbModelNoConfig.Controls.Add(this.cbAirflowMonitoring);
            this.gbModelNoConfig.Controls.Add(this.label174);
            this.gbModelNoConfig.Controls.Add(this.cbAltitude);
            this.gbModelNoConfig.Controls.Add(this.label175);
            this.gbModelNoConfig.Controls.Add(this.label176);
            this.gbModelNoConfig.Controls.Add(this.labelCoilcorrisionPackage);
            this.gbModelNoConfig.Controls.Add(this.cbCabinetConstructOutdoor);
            this.gbModelNoConfig.Controls.Add(this.label177);
            this.gbModelNoConfig.Controls.Add(this.cbHeatExchangerType);
            this.gbModelNoConfig.Controls.Add(this.label178);
            this.gbModelNoConfig.Controls.Add(this.cbHeatingCoilFluidType);
            this.gbModelNoConfig.Controls.Add(this.label179);
            this.gbModelNoConfig.Controls.Add(this.cbPostHeatingCoilSize);
            this.gbModelNoConfig.Controls.Add(this.label180);
            this.gbModelNoConfig.Controls.Add(this.cbPostHeatingCoilType);
            this.gbModelNoConfig.Controls.Add(this.label181);
            this.gbModelNoConfig.Controls.Add(this.label182);
            this.gbModelNoConfig.Controls.Add(this.cbCoilType);
            this.gbModelNoConfig.Controls.Add(this.cbModelSize);
            this.gbModelNoConfig.Controls.Add(this.label183);
            this.gbModelNoConfig.Controls.Add(this.cbConvenienceOutlet);
            this.gbModelNoConfig.Controls.Add(this.label184);
            this.gbModelNoConfig.Controls.Add(this.cbPostCoolingCoilSize);
            this.gbModelNoConfig.Controls.Add(this.label185);
            this.gbModelNoConfig.Controls.Add(this.cbCoilSize);
            this.gbModelNoConfig.Controls.Add(this.label186);
            this.gbModelNoConfig.Controls.Add(this.cbPostCoolingCoilType);
            this.gbModelNoConfig.Controls.Add(this.cbCabinetConstuctIndoor);
            this.gbModelNoConfig.Controls.Add(this.label187);
            this.gbModelNoConfig.Controls.Add(this.label188);
            this.gbModelNoConfig.Controls.Add(this.cbMajorDesignSeq);
            this.gbModelNoConfig.Controls.Add(this.label189);
            this.gbModelNoConfig.Controls.Add(this.cbUnitType);
            this.gbModelNoConfig.Controls.Add(this.label190);
            this.gbModelNoConfig.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbModelNoConfig.Location = new System.Drawing.Point(15, 187);
            this.gbModelNoConfig.Name = "gbModelNoConfig";
            this.gbModelNoConfig.Size = new System.Drawing.Size(1424, 600);
            this.gbModelNoConfig.TabIndex = 494;
            this.gbModelNoConfig.TabStop = false;
            this.gbModelNoConfig.Text = "Inputs";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.IndianRed;
            this.label8.Location = new System.Drawing.Point(746, 445);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(214, 20);
            this.label8.TabIndex = 96;
            this.label8.Text = "Digit 61-69 Future (Default to 0)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.IndianRed;
            this.label9.Location = new System.Drawing.Point(746, 420);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(214, 20);
            this.label9.TabIndex = 95;
            this.label9.Text = "Digit 60 Not Used";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.IndianRed;
            this.label7.Location = new System.Drawing.Point(746, 394);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(214, 20);
            this.label7.TabIndex = 94;
            this.label7.Text = "Digit 51-59 Future (Default to 0)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.IndianRed;
            this.label6.Location = new System.Drawing.Point(746, 369);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(214, 20);
            this.label6.TabIndex = 93;
            this.label6.Text = "Digit 50 Not Used";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.IndianRed;
            this.label5.Location = new System.Drawing.Point(746, 344);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(214, 20);
            this.label5.TabIndex = 92;
            this.label5.Text = "Digit 42-49 Future (Default to 0)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbMSP_CoolingCoilFluidType
            // 
            this.cbMSP_CoolingCoilFluidType.BackColor = System.Drawing.Color.White;
            this.cbMSP_CoolingCoilFluidType.DropDownWidth = 300;
            this.cbMSP_CoolingCoilFluidType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMSP_CoolingCoilFluidType.ForeColor = System.Drawing.Color.Black;
            this.cbMSP_CoolingCoilFluidType.FormattingEnabled = true;
            this.cbMSP_CoolingCoilFluidType.Items.AddRange(new object[] {
            "No Compressor",
            "Scroll Compressor",
            "Digital Scroll-1st Circuit Only",
            "Digital Scroll-1st Circuit & 2nd Circuit",
            "Variable Speed Scroll"});
            this.cbMSP_CoolingCoilFluidType.Location = new System.Drawing.Point(269, 201);
            this.cbMSP_CoolingCoilFluidType.Name = "cbMSP_CoolingCoilFluidType";
            this.cbMSP_CoolingCoilFluidType.Size = new System.Drawing.Size(210, 21);
            this.cbMSP_CoolingCoilFluidType.TabIndex = 91;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(7, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 20);
            this.label1.TabIndex = 90;
            this.label1.Text = "MSP/Cooling Coil Fluid Type (13)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbFilterOptions
            // 
            this.cbFilterOptions.BackColor = System.Drawing.Color.White;
            this.cbFilterOptions.DropDownWidth = 300;
            this.cbFilterOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFilterOptions.ForeColor = System.Drawing.Color.Black;
            this.cbFilterOptions.FormattingEnabled = true;
            this.cbFilterOptions.Location = new System.Drawing.Point(269, 544);
            this.cbFilterOptions.Name = "cbFilterOptions";
            this.cbFilterOptions.Size = new System.Drawing.Size(210, 21);
            this.cbFilterOptions.TabIndex = 89;
            // 
            // label194
            // 
            this.label194.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label194.ForeColor = System.Drawing.Color.Teal;
            this.label194.Location = new System.Drawing.Point(7, 544);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(253, 23);
            this.label194.TabIndex = 88;
            this.label194.Text = "Filter Options (27)";
            this.label194.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSupplyFanMotorSize
            // 
            this.cbSupplyFanMotorSize.BackColor = System.Drawing.Color.White;
            this.cbSupplyFanMotorSize.DropDownWidth = 300;
            this.cbSupplyFanMotorSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyFanMotorSize.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyFanMotorSize.FormattingEnabled = true;
            this.cbSupplyFanMotorSize.Location = new System.Drawing.Point(269, 517);
            this.cbSupplyFanMotorSize.Name = "cbSupplyFanMotorSize";
            this.cbSupplyFanMotorSize.Size = new System.Drawing.Size(210, 21);
            this.cbSupplyFanMotorSize.TabIndex = 87;
            // 
            // label193
            // 
            this.label193.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label193.ForeColor = System.Drawing.Color.Teal;
            this.label193.Location = new System.Drawing.Point(7, 517);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(253, 23);
            this.label193.TabIndex = 86;
            this.label193.Text = "Supply Fan Motor Size (26)";
            this.label193.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSupplyFanMotorType
            // 
            this.cbSupplyFanMotorType.BackColor = System.Drawing.Color.White;
            this.cbSupplyFanMotorType.DropDownWidth = 300;
            this.cbSupplyFanMotorType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyFanMotorType.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyFanMotorType.FormattingEnabled = true;
            this.cbSupplyFanMotorType.Location = new System.Drawing.Point(269, 490);
            this.cbSupplyFanMotorType.Name = "cbSupplyFanMotorType";
            this.cbSupplyFanMotorType.Size = new System.Drawing.Size(210, 21);
            this.cbSupplyFanMotorType.TabIndex = 85;
            // 
            // label192
            // 
            this.label192.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label192.ForeColor = System.Drawing.Color.Teal;
            this.label192.Location = new System.Drawing.Point(7, 490);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(253, 23);
            this.label192.TabIndex = 84;
            this.label192.Text = "Supply Fan Motor Type (25)";
            this.label192.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSupplyFanWheel
            // 
            this.cbSupplyFanWheel.BackColor = System.Drawing.Color.White;
            this.cbSupplyFanWheel.DropDownWidth = 300;
            this.cbSupplyFanWheel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyFanWheel.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyFanWheel.FormattingEnabled = true;
            this.cbSupplyFanWheel.Location = new System.Drawing.Point(269, 463);
            this.cbSupplyFanWheel.Name = "cbSupplyFanWheel";
            this.cbSupplyFanWheel.Size = new System.Drawing.Size(210, 21);
            this.cbSupplyFanWheel.TabIndex = 83;
            // 
            // label191
            // 
            this.label191.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label191.ForeColor = System.Drawing.Color.Teal;
            this.label191.Location = new System.Drawing.Point(7, 463);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(253, 23);
            this.label191.TabIndex = 82;
            this.label191.Text = "Supply Fan Wheels (24)";
            this.label191.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbBypassDamper
            // 
            this.cbBypassDamper.BackColor = System.Drawing.Color.White;
            this.cbBypassDamper.DropDownWidth = 300;
            this.cbBypassDamper.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBypassDamper.ForeColor = System.Drawing.Color.Black;
            this.cbBypassDamper.FormattingEnabled = true;
            this.cbBypassDamper.Location = new System.Drawing.Point(746, 19);
            this.cbBypassDamper.Name = "cbBypassDamper";
            this.cbBypassDamper.Size = new System.Drawing.Size(213, 21);
            this.cbBypassDamper.TabIndex = 81;
            // 
            // cbCoilCorrisionPackage
            // 
            this.cbCoilCorrisionPackage.BackColor = System.Drawing.Color.White;
            this.cbCoilCorrisionPackage.DropDownWidth = 300;
            this.cbCoilCorrisionPackage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCoilCorrisionPackage.ForeColor = System.Drawing.Color.Black;
            this.cbCoilCorrisionPackage.FormattingEnabled = true;
            this.cbCoilCorrisionPackage.Location = new System.Drawing.Point(269, 436);
            this.cbCoilCorrisionPackage.Name = "cbCoilCorrisionPackage";
            this.cbCoilCorrisionPackage.Size = new System.Drawing.Size(210, 21);
            this.cbCoilCorrisionPackage.TabIndex = 80;
            // 
            // cbCabinetDesign
            // 
            this.cbCabinetDesign.BackColor = System.Drawing.Color.White;
            this.cbCabinetDesign.DropDownWidth = 300;
            this.cbCabinetDesign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCabinetDesign.ForeColor = System.Drawing.Color.Black;
            this.cbCabinetDesign.FormattingEnabled = true;
            this.cbCabinetDesign.Items.AddRange(new object[] {
            "Rev4",
            "Rev5",
            "Heat Pump"});
            this.cbCabinetDesign.Location = new System.Drawing.Point(269, 45);
            this.cbCabinetDesign.Name = "cbCabinetDesign";
            this.cbCabinetDesign.Size = new System.Drawing.Size(210, 21);
            this.cbCabinetDesign.TabIndex = 78;
            // 
            // label161
            // 
            this.label161.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.ForeColor = System.Drawing.Color.Teal;
            this.label161.Location = new System.Drawing.Point(7, 45);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(253, 20);
            this.label161.TabIndex = 77;
            this.label161.Text = "Cabinet Design (2)";
            this.label161.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbVoltage
            // 
            this.cbVoltage.BackColor = System.Drawing.Color.White;
            this.cbVoltage.DropDownWidth = 300;
            this.cbVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVoltage.ForeColor = System.Drawing.Color.Black;
            this.cbVoltage.FormattingEnabled = true;
            this.cbVoltage.Items.AddRange(new object[] {
            "Vertical Discharge/Vertical Return",
            "Vertical Discharge/ Horizontal Return",
            "Horizontal Discharge/ Vertical Return",
            "Horizontal Discharge/ Horizontal Return"});
            this.cbVoltage.Location = new System.Drawing.Point(269, 97);
            this.cbVoltage.Name = "cbVoltage";
            this.cbVoltage.Size = new System.Drawing.Size(210, 21);
            this.cbVoltage.TabIndex = 76;
            // 
            // label162
            // 
            this.label162.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.ForeColor = System.Drawing.Color.Teal;
            this.label162.Location = new System.Drawing.Point(7, 97);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(253, 20);
            this.label162.TabIndex = 75;
            this.label162.Text = "Voltage (7)";
            this.label162.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbWarranty
            // 
            this.cbWarranty.BackColor = System.Drawing.Color.White;
            this.cbWarranty.DropDownWidth = 300;
            this.cbWarranty.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbWarranty.ForeColor = System.Drawing.Color.Black;
            this.cbWarranty.FormattingEnabled = true;
            this.cbWarranty.Items.AddRange(new object[] {
            "Non-Fused Disconnect",
            "Fused Disconnect Switch",
            "Non-Fused Disconnect w/Convenience Outlet",
            "Fused Disconnect Switch w/Convenience Outlet",
            "Dual Point Power"});
            this.cbWarranty.Location = new System.Drawing.Point(746, 305);
            this.cbWarranty.Name = "cbWarranty";
            this.cbWarranty.Size = new System.Drawing.Size(214, 21);
            this.cbWarranty.TabIndex = 68;
            // 
            // label166
            // 
            this.label166.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.ForeColor = System.Drawing.Color.Teal;
            this.label166.Location = new System.Drawing.Point(487, 305);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(253, 20);
            this.label166.TabIndex = 67;
            this.label166.Text = "Warranty (41)";
            this.label166.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbServiceLights
            // 
            this.cbServiceLights.BackColor = System.Drawing.Color.White;
            this.cbServiceLights.DropDownWidth = 300;
            this.cbServiceLights.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbServiceLights.ForeColor = System.Drawing.Color.Black;
            this.cbServiceLights.FormattingEnabled = true;
            this.cbServiceLights.Items.AddRange(new object[] {
            "No Smoke Detector",
            "Supply Smoke Detector",
            "Return Smoke Detector",
            "Supply & Return Detector"});
            this.cbServiceLights.Location = new System.Drawing.Point(746, 279);
            this.cbServiceLights.Name = "cbServiceLights";
            this.cbServiceLights.Size = new System.Drawing.Size(214, 21);
            this.cbServiceLights.TabIndex = 66;
            // 
            // label167
            // 
            this.label167.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.ForeColor = System.Drawing.Color.Teal;
            this.label167.Location = new System.Drawing.Point(487, 279);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(253, 20);
            this.label167.TabIndex = 65;
            this.label167.Text = "Service Lights (39)";
            this.label167.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbElectricalOptions
            // 
            this.cbElectricalOptions.BackColor = System.Drawing.Color.White;
            this.cbElectricalOptions.DropDownWidth = 300;
            this.cbElectricalOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbElectricalOptions.ForeColor = System.Drawing.Color.Black;
            this.cbElectricalOptions.FormattingEnabled = true;
            this.cbElectricalOptions.Items.AddRange(new object[] {
            "100 % OA 2-Position Damper",
            "100 % OA 2-Position Damper w/RA 2-Position Damper",
            "Modulating Mixed Air Damper"});
            this.cbElectricalOptions.Location = new System.Drawing.Point(746, 227);
            this.cbElectricalOptions.Name = "cbElectricalOptions";
            this.cbElectricalOptions.Size = new System.Drawing.Size(214, 21);
            this.cbElectricalOptions.TabIndex = 64;
            // 
            // label168
            // 
            this.label168.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.ForeColor = System.Drawing.Color.Teal;
            this.label168.Location = new System.Drawing.Point(487, 227);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(253, 20);
            this.label168.TabIndex = 63;
            this.label168.Text = "Electrical Options (37)";
            this.label168.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbControlsDisplay
            // 
            this.cbControlsDisplay.BackColor = System.Drawing.Color.White;
            this.cbControlsDisplay.DropDownWidth = 300;
            this.cbControlsDisplay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbControlsDisplay.ForeColor = System.Drawing.Color.Black;
            this.cbControlsDisplay.FormattingEnabled = true;
            this.cbControlsDisplay.Items.AddRange(new object[] {
            "No ERV",
            "3014C",
            "3622C",
            "4136C",
            "4634C",
            "5856C",
            "6488C",
            "6876C",
            "74122C"});
            this.cbControlsDisplay.Location = new System.Drawing.Point(746, 201);
            this.cbControlsDisplay.Name = "cbControlsDisplay";
            this.cbControlsDisplay.Size = new System.Drawing.Size(214, 21);
            this.cbControlsDisplay.TabIndex = 62;
            // 
            // label169
            // 
            this.label169.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.ForeColor = System.Drawing.Color.Teal;
            this.label169.Location = new System.Drawing.Point(487, 201);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(253, 20);
            this.label169.TabIndex = 61;
            this.label169.Text = "Controls Display (36)";
            this.label169.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbControls
            // 
            this.cbControls.BackColor = System.Drawing.Color.White;
            this.cbControls.DropDownWidth = 300;
            this.cbControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbControls.ForeColor = System.Drawing.Color.Black;
            this.cbControls.FormattingEnabled = true;
            this.cbControls.Items.AddRange(new object[] {
            "No ERV",
            "ERV-Composite Contruction",
            "ERV-Composite Contruction with Frost Protection",
            "ERV-Composite Contruction with Bypass",
            "ERV-Composite Contruction with Frost Protection & Bypass",
            "ERV-Aluminum Contruction",
            "ERV-Aluminum Contruction with Frost Protection",
            "ERV-Aluminum Contruction with Bypass",
            "ERV-Aluminum Contruction with Frost Protection & Bypass"});
            this.cbControls.Location = new System.Drawing.Point(746, 175);
            this.cbControls.Name = "cbControls";
            this.cbControls.Size = new System.Drawing.Size(214, 21);
            this.cbControls.TabIndex = 60;
            // 
            // label170
            // 
            this.label170.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.ForeColor = System.Drawing.Color.Teal;
            this.label170.Location = new System.Drawing.Point(487, 175);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(253, 20);
            this.label170.TabIndex = 59;
            this.label170.Text = "Controls (35)";
            this.label170.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCondensateOverflowSwitch
            // 
            this.cbCondensateOverflowSwitch.BackColor = System.Drawing.Color.White;
            this.cbCondensateOverflowSwitch.DropDownWidth = 300;
            this.cbCondensateOverflowSwitch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCondensateOverflowSwitch.ForeColor = System.Drawing.Color.Black;
            this.cbCondensateOverflowSwitch.FormattingEnabled = true;
            this.cbCondensateOverflowSwitch.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbCondensateOverflowSwitch.Location = new System.Drawing.Point(746, 149);
            this.cbCondensateOverflowSwitch.Name = "cbCondensateOverflowSwitch";
            this.cbCondensateOverflowSwitch.Size = new System.Drawing.Size(214, 21);
            this.cbCondensateOverflowSwitch.TabIndex = 58;
            // 
            // label171
            // 
            this.label171.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.Teal;
            this.label171.Location = new System.Drawing.Point(487, 149);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(253, 20);
            this.label171.TabIndex = 57;
            this.label171.Text = "Condensate Overflow Switch (34)";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSmokeDetector
            // 
            this.cbSmokeDetector.BackColor = System.Drawing.Color.White;
            this.cbSmokeDetector.DropDownWidth = 300;
            this.cbSmokeDetector.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSmokeDetector.ForeColor = System.Drawing.Color.Black;
            this.cbSmokeDetector.FormattingEnabled = true;
            this.cbSmokeDetector.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbSmokeDetector.Location = new System.Drawing.Point(746, 123);
            this.cbSmokeDetector.Name = "cbSmokeDetector";
            this.cbSmokeDetector.Size = new System.Drawing.Size(214, 21);
            this.cbSmokeDetector.TabIndex = 56;
            // 
            // label172
            // 
            this.label172.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.ForeColor = System.Drawing.Color.Teal;
            this.label172.Location = new System.Drawing.Point(487, 123);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(253, 20);
            this.label172.TabIndex = 55;
            this.label172.Text = "Smoke Detector (33)";
            this.label172.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAccessories
            // 
            this.cbAccessories.BackColor = System.Drawing.Color.White;
            this.cbAccessories.DropDownWidth = 300;
            this.cbAccessories.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAccessories.ForeColor = System.Drawing.Color.Black;
            this.cbAccessories.FormattingEnabled = true;
            this.cbAccessories.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbAccessories.Location = new System.Drawing.Point(746, 97);
            this.cbAccessories.Name = "cbAccessories";
            this.cbAccessories.Size = new System.Drawing.Size(214, 21);
            this.cbAccessories.TabIndex = 54;
            // 
            // label173
            // 
            this.label173.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.ForeColor = System.Drawing.Color.Teal;
            this.label173.Location = new System.Drawing.Point(487, 97);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(253, 20);
            this.label173.TabIndex = 53;
            this.label173.Text = "Accessories (32)";
            this.label173.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAirflowMonitoring
            // 
            this.cbAirflowMonitoring.BackColor = System.Drawing.Color.White;
            this.cbAirflowMonitoring.DropDownWidth = 300;
            this.cbAirflowMonitoring.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAirflowMonitoring.ForeColor = System.Drawing.Color.Black;
            this.cbAirflowMonitoring.FormattingEnabled = true;
            this.cbAirflowMonitoring.Items.AddRange(new object[] {
            "Non DDC - Electromechanical",
            "Trane - Outdoor Air Control w/LON Read-Write w/Display",
            "Trane - Space Control w/LON Read-Write w/Display",
            "Trane - Outdoor Air Control w/BACNET (No Display)",
            "Trane - Space Control w/BACNET  (No Display)",
            "Trane - Discharge Air Control w/BACNET  (No Display)",
            "Trane - Outdoor Air Control w/BACNET  w/Display",
            "Trane - Space Control w/BACNET w/Display",
            "Trane - Discharge Air Control w/BACNET w/Display"});
            this.cbAirflowMonitoring.Location = new System.Drawing.Point(746, 71);
            this.cbAirflowMonitoring.Name = "cbAirflowMonitoring";
            this.cbAirflowMonitoring.Size = new System.Drawing.Size(214, 21);
            this.cbAirflowMonitoring.TabIndex = 52;
            // 
            // label174
            // 
            this.label174.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.ForeColor = System.Drawing.Color.Teal;
            this.label174.Location = new System.Drawing.Point(487, 71);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(253, 20);
            this.label174.TabIndex = 51;
            this.label174.Text = "Airflow Monitoring (31)";
            this.label174.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAltitude
            // 
            this.cbAltitude.BackColor = System.Drawing.Color.White;
            this.cbAltitude.DropDownWidth = 300;
            this.cbAltitude.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAltitude.ForeColor = System.Drawing.Color.Black;
            this.cbAltitude.FormattingEnabled = true;
            this.cbAltitude.Items.AddRange(new object[] {
            "No Corrosive Package",
            "S/S Cabinet, Basepan, Eco Coated Coils",
            "S/S Cabinet, Basepan",
            "S/S Basepan, Eco Coated Coils",
            "S/S Coil Casing",
            "S/S Interior Casing",
            "Eco Coated Coils"});
            this.cbAltitude.Location = new System.Drawing.Point(746, 45);
            this.cbAltitude.Name = "cbAltitude";
            this.cbAltitude.Size = new System.Drawing.Size(214, 21);
            this.cbAltitude.TabIndex = 50;
            // 
            // label175
            // 
            this.label175.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.ForeColor = System.Drawing.Color.Teal;
            this.label175.Location = new System.Drawing.Point(487, 45);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(253, 20);
            this.label175.TabIndex = 49;
            this.label175.Text = "Altitude (29)";
            this.label175.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label176
            // 
            this.label176.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.ForeColor = System.Drawing.Color.Teal;
            this.label176.Location = new System.Drawing.Point(487, 19);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(253, 23);
            this.label176.TabIndex = 44;
            this.label176.Text = "Bypass Damper (28)";
            this.label176.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCoilcorrisionPackage
            // 
            this.labelCoilcorrisionPackage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCoilcorrisionPackage.ForeColor = System.Drawing.Color.Teal;
            this.labelCoilcorrisionPackage.Location = new System.Drawing.Point(7, 436);
            this.labelCoilcorrisionPackage.Name = "labelCoilcorrisionPackage";
            this.labelCoilcorrisionPackage.Size = new System.Drawing.Size(253, 23);
            this.labelCoilcorrisionPackage.TabIndex = 40;
            this.labelCoilcorrisionPackage.Text = "Coil Corrision Package (23)";
            this.labelCoilcorrisionPackage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCabinetConstructOutdoor
            // 
            this.cbCabinetConstructOutdoor.BackColor = System.Drawing.Color.White;
            this.cbCabinetConstructOutdoor.DropDownWidth = 300;
            this.cbCabinetConstructOutdoor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCabinetConstructOutdoor.ForeColor = System.Drawing.Color.Black;
            this.cbCabinetConstructOutdoor.FormattingEnabled = true;
            this.cbCabinetConstructOutdoor.Items.AddRange(new object[] {
            "No Heat",
            "Natural Gas",
            "Propane",
            "Electric - Open Coil",
            "Electric - Sheathed Coil",
            "Hot Water",
            "Steam"});
            this.cbCabinetConstructOutdoor.Location = new System.Drawing.Point(269, 409);
            this.cbCabinetConstructOutdoor.Name = "cbCabinetConstructOutdoor";
            this.cbCabinetConstructOutdoor.Size = new System.Drawing.Size(210, 21);
            this.cbCabinetConstructOutdoor.TabIndex = 39;
            // 
            // label177
            // 
            this.label177.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.ForeColor = System.Drawing.Color.Teal;
            this.label177.Location = new System.Drawing.Point(7, 409);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(253, 23);
            this.label177.TabIndex = 38;
            this.label177.Text = "Cabinet Construction Outdoor (22)";
            this.label177.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHeatExchangerType
            // 
            this.cbHeatExchangerType.BackColor = System.Drawing.Color.White;
            this.cbHeatExchangerType.DropDownWidth = 300;
            this.cbHeatExchangerType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatExchangerType.ForeColor = System.Drawing.Color.Black;
            this.cbHeatExchangerType.FormattingEnabled = true;
            this.cbHeatExchangerType.Items.AddRange(new object[] {
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbHeatExchangerType.Location = new System.Drawing.Point(269, 357);
            this.cbHeatExchangerType.Name = "cbHeatExchangerType";
            this.cbHeatExchangerType.Size = new System.Drawing.Size(210, 21);
            this.cbHeatExchangerType.TabIndex = 37;
            // 
            // label178
            // 
            this.label178.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.ForeColor = System.Drawing.Color.Teal;
            this.label178.Location = new System.Drawing.Point(7, 357);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(253, 20);
            this.label178.TabIndex = 36;
            this.label178.Text = "Heat Exchanger Type (19)";
            this.label178.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHeatingCoilFluidType
            // 
            this.cbHeatingCoilFluidType.BackColor = System.Drawing.Color.White;
            this.cbHeatingCoilFluidType.DropDownWidth = 300;
            this.cbHeatingCoilFluidType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatingCoilFluidType.ForeColor = System.Drawing.Color.Black;
            this.cbHeatingCoilFluidType.FormattingEnabled = true;
            this.cbHeatingCoilFluidType.Items.AddRange(new object[] {
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbHeatingCoilFluidType.Location = new System.Drawing.Point(269, 331);
            this.cbHeatingCoilFluidType.Name = "cbHeatingCoilFluidType";
            this.cbHeatingCoilFluidType.Size = new System.Drawing.Size(210, 21);
            this.cbHeatingCoilFluidType.TabIndex = 35;
            // 
            // label179
            // 
            this.label179.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.ForeColor = System.Drawing.Color.Teal;
            this.label179.Location = new System.Drawing.Point(7, 331);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(253, 20);
            this.label179.TabIndex = 34;
            this.label179.Text = "Heating Coil Fluid Type (18)";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPostHeatingCoilSize
            // 
            this.cbPostHeatingCoilSize.BackColor = System.Drawing.Color.White;
            this.cbPostHeatingCoilSize.DropDownWidth = 300;
            this.cbPostHeatingCoilSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPostHeatingCoilSize.ForeColor = System.Drawing.Color.Black;
            this.cbPostHeatingCoilSize.FormattingEnabled = true;
            this.cbPostHeatingCoilSize.Items.AddRange(new object[] {
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbPostHeatingCoilSize.Location = new System.Drawing.Point(269, 305);
            this.cbPostHeatingCoilSize.Name = "cbPostHeatingCoilSize";
            this.cbPostHeatingCoilSize.Size = new System.Drawing.Size(210, 21);
            this.cbPostHeatingCoilSize.TabIndex = 33;
            // 
            // label180
            // 
            this.label180.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.ForeColor = System.Drawing.Color.Teal;
            this.label180.Location = new System.Drawing.Point(7, 305);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(253, 20);
            this.label180.TabIndex = 32;
            this.label180.Text = "Post Heating Coil Size (17)";
            this.label180.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPostHeatingCoilType
            // 
            this.cbPostHeatingCoilType.BackColor = System.Drawing.Color.White;
            this.cbPostHeatingCoilType.DropDownWidth = 300;
            this.cbPostHeatingCoilType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPostHeatingCoilType.ForeColor = System.Drawing.Color.Black;
            this.cbPostHeatingCoilType.FormattingEnabled = true;
            this.cbPostHeatingCoilType.Items.AddRange(new object[] {
            "No RCC Valve",
            "RCC Valve on 1st Circuit",
            "RCC Valve on 1st Circuit & 2nd Circuit",
            "ERCC Valve on 1st Circuit",
            "ERCC Valve on 1st Circuit & 2nd Circuit",
            "HGBP Valve on 1st Circuit",
            "HGBP Valve on 1st Circuit & 2nd Circuit"});
            this.cbPostHeatingCoilType.Location = new System.Drawing.Point(269, 279);
            this.cbPostHeatingCoilType.Name = "cbPostHeatingCoilType";
            this.cbPostHeatingCoilType.Size = new System.Drawing.Size(210, 21);
            this.cbPostHeatingCoilType.TabIndex = 31;
            // 
            // label181
            // 
            this.label181.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label181.ForeColor = System.Drawing.Color.Teal;
            this.label181.Location = new System.Drawing.Point(7, 279);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(253, 20);
            this.label181.TabIndex = 30;
            this.label181.Text = "Post Heating Coil Type (16)";
            this.label181.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label182
            // 
            this.label182.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label182.ForeColor = System.Drawing.Color.Teal;
            this.label182.Location = new System.Drawing.Point(7, 149);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(253, 20);
            this.label182.TabIndex = 29;
            this.label182.Text = "MSP Cooling Coil Type (11)";
            this.label182.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCoilType
            // 
            this.cbCoilType.BackColor = System.Drawing.Color.White;
            this.cbCoilType.DropDownWidth = 300;
            this.cbCoilType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCoilType.ForeColor = System.Drawing.Color.Black;
            this.cbCoilType.FormattingEnabled = true;
            this.cbCoilType.Items.AddRange(new object[] {
            "No Cooling",
            "DX 3-Row",
            "DX 4-Row",
            "DX 4-Row Interlaced",
            "DX 6-Row Interlaced",
            "DX 8-Row",
            "Glycol/Chilled Water Coil"});
            this.cbCoilType.Location = new System.Drawing.Point(269, 149);
            this.cbCoilType.Name = "cbCoilType";
            this.cbCoilType.Size = new System.Drawing.Size(210, 21);
            this.cbCoilType.TabIndex = 28;
            // 
            // cbModelSize
            // 
            this.cbModelSize.BackColor = System.Drawing.Color.White;
            this.cbModelSize.DropDownWidth = 300;
            this.cbModelSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModelSize.ForeColor = System.Drawing.Color.Black;
            this.cbModelSize.FormattingEnabled = true;
            this.cbModelSize.Items.AddRange(new object[] {
            "No Cooling",
            "5 Tons High Efficiency",
            "6 Tons High Efficiency",
            "7 Tons High Efficiency",
            "8 Tons High Efficiency",
            "10 Tons High Efficiency",
            "12 Tons High Efficiency",
            "15 Tons High Efficiency",
            "17 Tons High Efficiency",
            "20 Tons High Efficiency",
            "22 Tons High Efficiency",
            "25 Tons High Efficiency",
            "30 Tons High Efficiency",
            "35 Tons High Efficiency",
            "40 Tons High Efficiency",
            "45 Tons High Efficiency",
            "50 Tons High Efficiency",
            "54 Tons High Efficiency"});
            this.cbModelSize.Location = new System.Drawing.Point(269, 71);
            this.cbModelSize.Name = "cbModelSize";
            this.cbModelSize.Size = new System.Drawing.Size(210, 21);
            this.cbModelSize.TabIndex = 26;
            // 
            // label183
            // 
            this.label183.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label183.ForeColor = System.Drawing.Color.Teal;
            this.label183.Location = new System.Drawing.Point(7, 71);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(253, 20);
            this.label183.TabIndex = 25;
            this.label183.Text = "Model Size (3456)";
            this.label183.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbConvenienceOutlet
            // 
            this.cbConvenienceOutlet.BackColor = System.Drawing.Color.White;
            this.cbConvenienceOutlet.DropDownWidth = 300;
            this.cbConvenienceOutlet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConvenienceOutlet.ForeColor = System.Drawing.Color.Black;
            this.cbConvenienceOutlet.FormattingEnabled = true;
            this.cbConvenienceOutlet.Items.AddRange(new object[] {
            "Aluminum Mesh Intake Filters (ALM)",
            "MERV-8, 30%, and ALM",
            "MERV-13, 80%, and ALM",
            "MERV-14, 95%, and ALM",
            "Aluminum Mesh Intake Filters (ALM), w/UVC",
            "MERV-8, 30%, and ALM, w/UVC",
            "MERV-13, 80%, and ALM, w/UVC",
            "MERV-14, 95%, and ALM, w/UVC",
            "Aluminum Mesh Intake Filters (ALM), w/UVC & Electrostatic Filters",
            "MERV-8, 30%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-13, 80%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-14, 95%, and ALM, w/UVC & Electrostatic Filters",
            "Aluminum Mesh Intake Filters (ALM) & Electrostatic Filters",
            "MERV-8, 30%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM & Electrostatic Filters",
            "MERV-14, 95%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM w/TCACS",
            "MERV-14, 95%, and ALM w/TCACS",
            "Special Filter Options"});
            this.cbConvenienceOutlet.Location = new System.Drawing.Point(746, 253);
            this.cbConvenienceOutlet.Name = "cbConvenienceOutlet";
            this.cbConvenienceOutlet.Size = new System.Drawing.Size(214, 21);
            this.cbConvenienceOutlet.TabIndex = 22;
            // 
            // label184
            // 
            this.label184.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label184.ForeColor = System.Drawing.Color.Teal;
            this.label184.Location = new System.Drawing.Point(487, 252);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(253, 20);
            this.label184.TabIndex = 21;
            this.label184.Text = "Convenience Outlet (38)";
            this.label184.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPostCoolingCoilSize
            // 
            this.cbPostCoolingCoilSize.BackColor = System.Drawing.Color.White;
            this.cbPostCoolingCoilSize.DropDownWidth = 300;
            this.cbPostCoolingCoilSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPostCoolingCoilSize.ForeColor = System.Drawing.Color.Black;
            this.cbPostCoolingCoilSize.FormattingEnabled = true;
            this.cbPostCoolingCoilSize.Items.AddRange(new object[] {
            "No Condenser",
            "Air Cooled",
            "Air Cooled w/Head Pressure on/off control",
            "Water Source Heat Pump",
            "Air Cooled Fin & Tube w/ Head Pressure Variable Speed",
            "Air Cooled Micro Channel",
            "Air Cooled Micro Channel w/Head Pressure on/off control",
            "Air Cooled Micro Channel Variable Speed",
            "Water Cooled DX Condenser Copper/Nickel"});
            this.cbPostCoolingCoilSize.Location = new System.Drawing.Point(269, 253);
            this.cbPostCoolingCoilSize.Name = "cbPostCoolingCoilSize";
            this.cbPostCoolingCoilSize.Size = new System.Drawing.Size(210, 21);
            this.cbPostCoolingCoilSize.TabIndex = 18;
            // 
            // label185
            // 
            this.label185.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label185.ForeColor = System.Drawing.Color.Teal;
            this.label185.Location = new System.Drawing.Point(7, 253);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(253, 20);
            this.label185.TabIndex = 17;
            this.label185.Text = "Post Cooling Coil Size (15)";
            this.label185.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCoilSize
            // 
            this.cbCoilSize.BackColor = System.Drawing.Color.White;
            this.cbCoilSize.DropDownWidth = 300;
            this.cbCoilSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCoilSize.ForeColor = System.Drawing.Color.Black;
            this.cbCoilSize.FormattingEnabled = true;
            this.cbCoilSize.Items.AddRange(new object[] {
            "No HGRH",
            "Modulating",
            "On/Off"});
            this.cbCoilSize.Location = new System.Drawing.Point(269, 175);
            this.cbCoilSize.Name = "cbCoilSize";
            this.cbCoilSize.Size = new System.Drawing.Size(210, 21);
            this.cbCoilSize.TabIndex = 16;
            // 
            // label186
            // 
            this.label186.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label186.ForeColor = System.Drawing.Color.Teal;
            this.label186.Location = new System.Drawing.Point(7, 175);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(253, 20);
            this.label186.TabIndex = 15;
            this.label186.Text = "MSP Coil Size (12)";
            this.label186.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPostCoolingCoilType
            // 
            this.cbPostCoolingCoilType.BackColor = System.Drawing.Color.White;
            this.cbPostCoolingCoilType.DropDownWidth = 300;
            this.cbPostCoolingCoilType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPostCoolingCoilType.ForeColor = System.Drawing.Color.Black;
            this.cbPostCoolingCoilType.FormattingEnabled = true;
            this.cbPostCoolingCoilType.Items.AddRange(new object[] {
            "No Compressor",
            "Scroll Compressor",
            "Digital Scroll-1st Circuit Only",
            "Digital Scroll-1st Circuit & 2nd Circuit",
            "Variable Speed Scroll"});
            this.cbPostCoolingCoilType.Location = new System.Drawing.Point(269, 227);
            this.cbPostCoolingCoilType.Name = "cbPostCoolingCoilType";
            this.cbPostCoolingCoilType.Size = new System.Drawing.Size(210, 21);
            this.cbPostCoolingCoilType.TabIndex = 14;
            // 
            // cbCabinetConstuctIndoor
            // 
            this.cbCabinetConstuctIndoor.BackColor = System.Drawing.Color.White;
            this.cbCabinetConstuctIndoor.DropDownWidth = 300;
            this.cbCabinetConstuctIndoor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCabinetConstuctIndoor.ForeColor = System.Drawing.Color.Black;
            this.cbCabinetConstuctIndoor.FormattingEnabled = true;
            this.cbCabinetConstuctIndoor.Items.AddRange(new object[] {
            "No Heat",
            "Indirect - Fired (IF)",
            "Direct - Fired (DF)",
            "Electric - 2 Stage",
            "Electric - SCR Modulating",
            "Dual Fuel (PRI-DF/SEC-IF)",
            "Dual Fuel (PRI-DF/SEC-ELEC)",
            "Dual Fuel (PRI-IF/SEC-ELEC)",
            "Dual Fuel (PRI-ELEC/SEC-ELEC)",
            "Hot Water",
            "Steam"});
            this.cbCabinetConstuctIndoor.Location = new System.Drawing.Point(269, 383);
            this.cbCabinetConstuctIndoor.Name = "cbCabinetConstuctIndoor";
            this.cbCabinetConstuctIndoor.Size = new System.Drawing.Size(210, 21);
            this.cbCabinetConstuctIndoor.TabIndex = 14;
            // 
            // label187
            // 
            this.label187.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label187.ForeColor = System.Drawing.Color.Teal;
            this.label187.Location = new System.Drawing.Point(7, 227);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(253, 20);
            this.label187.TabIndex = 13;
            this.label187.Text = "Post Cooling Coil Type (14)";
            this.label187.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label188
            // 
            this.label188.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label188.ForeColor = System.Drawing.Color.Teal;
            this.label188.Location = new System.Drawing.Point(7, 383);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(253, 23);
            this.label188.TabIndex = 13;
            this.label188.Text = "Cabinet Construction Indoor (21)";
            this.label188.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbMajorDesignSeq
            // 
            this.cbMajorDesignSeq.BackColor = System.Drawing.Color.White;
            this.cbMajorDesignSeq.DropDownWidth = 300;
            this.cbMajorDesignSeq.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMajorDesignSeq.ForeColor = System.Drawing.Color.Black;
            this.cbMajorDesignSeq.FormattingEnabled = true;
            this.cbMajorDesignSeq.Items.AddRange(new object[] {
            "115/60/1",
            "208-230/60/1",
            "208-230/60/3",
            "460/60/3",
            "575/60/3"});
            this.cbMajorDesignSeq.Location = new System.Drawing.Point(269, 123);
            this.cbMajorDesignSeq.Name = "cbMajorDesignSeq";
            this.cbMajorDesignSeq.Size = new System.Drawing.Size(210, 21);
            this.cbMajorDesignSeq.TabIndex = 12;
            // 
            // label189
            // 
            this.label189.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label189.ForeColor = System.Drawing.Color.Teal;
            this.label189.Location = new System.Drawing.Point(7, 123);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(253, 20);
            this.label189.TabIndex = 11;
            this.label189.Text = "Major Design Seq (8)";
            this.label189.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitType
            // 
            this.cbUnitType.BackColor = System.Drawing.Color.White;
            this.cbUnitType.DropDownWidth = 300;
            this.cbUnitType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUnitType.ForeColor = System.Drawing.Color.Black;
            this.cbUnitType.FormattingEnabled = true;
            this.cbUnitType.Items.AddRange(new object[] {
            "625 - 3,600 cfm",
            "1,500 - 9,000 cfm",
            "3,500 - 13,500 cfm"});
            this.cbUnitType.Location = new System.Drawing.Point(269, 19);
            this.cbUnitType.Name = "cbUnitType";
            this.cbUnitType.Size = new System.Drawing.Size(210, 21);
            this.cbUnitType.TabIndex = 1;
            // 
            // label190
            // 
            this.label190.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label190.ForeColor = System.Drawing.Color.Teal;
            this.label190.Location = new System.Drawing.Point(7, 19);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(253, 20);
            this.label190.TabIndex = 0;
            this.label190.Text = "Unit Type (1)";
            this.label190.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(812, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 15);
            this.label14.TabIndex = 456;
            this.label14.Text = "1";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(812, 147);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 15);
            this.label22.TabIndex = 448;
            this.label22.Text = "4";
            // 
            // label133
            // 
            this.label133.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.Black;
            this.label133.Location = new System.Drawing.Point(813, 139);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(13, 15);
            this.label133.TabIndex = 440;
            this.label133.Text = "^";
            // 
            // lbLastUpdateDate
            // 
            this.lbLastUpdateDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateDate.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateDate.Location = new System.Drawing.Point(154, 75);
            this.lbLastUpdateDate.Name = "lbLastUpdateDate";
            this.lbLastUpdateDate.Size = new System.Drawing.Size(100, 20);
            this.lbLastUpdateDate.TabIndex = 439;
            this.lbLastUpdateDate.Text = "??/??/????";
            this.lbLastUpdateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(15, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 438;
            this.label3.Text = "Last Updated Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label129
            // 
            this.label129.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Black;
            this.label129.Location = new System.Drawing.Point(781, 162);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(13, 15);
            this.label129.TabIndex = 437;
            this.label129.Text = "9";
            // 
            // label130
            // 
            this.label130.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Black;
            this.label130.Location = new System.Drawing.Point(781, 147);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(13, 15);
            this.label130.TabIndex = 436;
            this.label130.Text = "3";
            // 
            // label131
            // 
            this.label131.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Black;
            this.label131.Location = new System.Drawing.Point(781, 139);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(13, 15);
            this.label131.TabIndex = 435;
            this.label131.Text = "^";
            // 
            // label127
            // 
            this.label127.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Black;
            this.label127.Location = new System.Drawing.Point(621, 162);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(13, 15);
            this.label127.TabIndex = 434;
            this.label127.Text = "9";
            // 
            // label128
            // 
            this.label128.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Black;
            this.label128.Location = new System.Drawing.Point(606, 162);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(13, 15);
            this.label128.TabIndex = 433;
            this.label128.Text = "8";
            // 
            // label119
            // 
            this.label119.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(765, 162);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(13, 15);
            this.label119.TabIndex = 432;
            this.label119.Text = "8";
            // 
            // label120
            // 
            this.label120.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(749, 162);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(13, 15);
            this.label120.TabIndex = 431;
            this.label120.Text = "7";
            // 
            // label121
            // 
            this.label121.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(733, 162);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(13, 15);
            this.label121.TabIndex = 430;
            this.label121.Text = "6";
            // 
            // label122
            // 
            this.label122.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Black;
            this.label122.Location = new System.Drawing.Point(718, 162);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(13, 15);
            this.label122.TabIndex = 429;
            this.label122.Text = "5";
            // 
            // label123
            // 
            this.label123.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Black;
            this.label123.Location = new System.Drawing.Point(702, 162);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(13, 15);
            this.label123.TabIndex = 428;
            this.label123.Text = "4";
            // 
            // label124
            // 
            this.label124.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.Black;
            this.label124.Location = new System.Drawing.Point(685, 162);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(13, 15);
            this.label124.TabIndex = 427;
            this.label124.Text = "3";
            // 
            // label125
            // 
            this.label125.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.Black;
            this.label125.Location = new System.Drawing.Point(668, 162);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(13, 15);
            this.label125.TabIndex = 426;
            this.label125.Text = "2";
            // 
            // label126
            // 
            this.label126.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Black;
            this.label126.Location = new System.Drawing.Point(653, 162);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(13, 15);
            this.label126.TabIndex = 425;
            this.label126.Text = "1";
            // 
            // label111
            // 
            this.label111.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(590, 162);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(13, 15);
            this.label111.TabIndex = 424;
            this.label111.Text = "7";
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Black;
            this.label112.Location = new System.Drawing.Point(575, 162);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(13, 15);
            this.label112.TabIndex = 423;
            this.label112.Text = "6";
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(558, 162);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(13, 15);
            this.label113.TabIndex = 422;
            this.label113.Text = "5";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(542, 162);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(13, 15);
            this.label114.TabIndex = 421;
            this.label114.Text = "4";
            // 
            // label115
            // 
            this.label115.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(526, 162);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(13, 15);
            this.label115.TabIndex = 420;
            this.label115.Text = "3";
            // 
            // label116
            // 
            this.label116.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Black;
            this.label116.Location = new System.Drawing.Point(509, 162);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(13, 15);
            this.label116.TabIndex = 419;
            this.label116.Text = "2";
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(492, 162);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(13, 15);
            this.label117.TabIndex = 418;
            this.label117.Text = "1";
            // 
            // label118
            // 
            this.label118.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(461, 162);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(13, 15);
            this.label118.TabIndex = 417;
            this.label118.Text = "9";
            // 
            // label103
            // 
            this.label103.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(445, 162);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(13, 15);
            this.label103.TabIndex = 416;
            this.label103.Text = "8";
            // 
            // label104
            // 
            this.label104.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(430, 162);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(13, 15);
            this.label104.TabIndex = 415;
            this.label104.Text = "7";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(413, 162);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(13, 15);
            this.label105.TabIndex = 414;
            this.label105.Text = "6";
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(397, 162);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(13, 15);
            this.label106.TabIndex = 413;
            this.label106.Text = "5";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(380, 162);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(13, 15);
            this.label107.TabIndex = 412;
            this.label107.Text = "4";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(364, 162);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(13, 15);
            this.label108.TabIndex = 411;
            this.label108.Text = "3";
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Black;
            this.label109.Location = new System.Drawing.Point(347, 162);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(13, 15);
            this.label109.TabIndex = 410;
            this.label109.Text = "2";
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(332, 162);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(13, 15);
            this.label110.TabIndex = 409;
            this.label110.Text = "1";
            // 
            // label100
            // 
            this.label100.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(765, 147);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(13, 15);
            this.label100.TabIndex = 407;
            this.label100.Text = "3";
            // 
            // label101
            // 
            this.label101.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(749, 147);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(13, 15);
            this.label101.TabIndex = 406;
            this.label101.Text = "3";
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(733, 147);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(13, 15);
            this.label96.TabIndex = 405;
            this.label96.Text = "3";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(718, 147);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(13, 15);
            this.label97.TabIndex = 404;
            this.label97.Text = "3";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(702, 147);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(13, 15);
            this.label98.TabIndex = 403;
            this.label98.Text = "3";
            // 
            // label99
            // 
            this.label99.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(685, 147);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(13, 15);
            this.label99.TabIndex = 402;
            this.label99.Text = "3";
            // 
            // label92
            // 
            this.label92.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(668, 147);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(13, 15);
            this.label92.TabIndex = 401;
            this.label92.Text = "3";
            // 
            // label93
            // 
            this.label93.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(653, 147);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(13, 15);
            this.label93.TabIndex = 400;
            this.label93.Text = "3";
            // 
            // label94
            // 
            this.label94.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(621, 147);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(13, 15);
            this.label94.TabIndex = 399;
            this.label94.Text = "2";
            // 
            // label95
            // 
            this.label95.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(606, 147);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(13, 15);
            this.label95.TabIndex = 398;
            this.label95.Text = "2";
            // 
            // label88
            // 
            this.label88.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(590, 147);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(13, 15);
            this.label88.TabIndex = 397;
            this.label88.Text = "2";
            // 
            // label89
            // 
            this.label89.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(575, 147);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(13, 15);
            this.label89.TabIndex = 396;
            this.label89.Text = "2";
            // 
            // label90
            // 
            this.label90.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(558, 147);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(13, 15);
            this.label90.TabIndex = 395;
            this.label90.Text = "2";
            // 
            // label91
            // 
            this.label91.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(542, 147);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(13, 15);
            this.label91.TabIndex = 394;
            this.label91.Text = "2";
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(526, 147);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(13, 15);
            this.label84.TabIndex = 393;
            this.label84.Text = "2";
            // 
            // label85
            // 
            this.label85.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(509, 147);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(13, 15);
            this.label85.TabIndex = 392;
            this.label85.Text = "2";
            // 
            // label86
            // 
            this.label86.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(492, 147);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(13, 15);
            this.label86.TabIndex = 391;
            this.label86.Text = "2";
            // 
            // label87
            // 
            this.label87.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(461, 147);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(13, 15);
            this.label87.TabIndex = 390;
            this.label87.Text = "1";
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(445, 147);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(13, 15);
            this.label80.TabIndex = 389;
            this.label80.Text = "1";
            // 
            // label81
            // 
            this.label81.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(430, 147);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(13, 15);
            this.label81.TabIndex = 388;
            this.label81.Text = "1";
            // 
            // label82
            // 
            this.label82.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(413, 147);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(13, 15);
            this.label82.TabIndex = 387;
            this.label82.Text = "1";
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(397, 147);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(13, 15);
            this.label83.TabIndex = 386;
            this.label83.Text = "1";
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(380, 147);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(13, 15);
            this.label76.TabIndex = 385;
            this.label76.Text = "1";
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(364, 147);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(13, 15);
            this.label77.TabIndex = 384;
            this.label77.Text = "1";
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(347, 147);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(13, 15);
            this.label78.TabIndex = 383;
            this.label78.Text = "1";
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(332, 147);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(13, 15);
            this.label79.TabIndex = 382;
            this.label79.Text = "1";
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(285, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(13, 15);
            this.label72.TabIndex = 381;
            this.label72.Text = "8";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(269, 147);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(13, 15);
            this.label73.TabIndex = 380;
            this.label73.Text = "7";
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(253, 147);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(13, 15);
            this.label74.TabIndex = 379;
            this.label74.Text = "6";
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(235, 147);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(13, 15);
            this.label75.TabIndex = 378;
            this.label75.Text = "5";
            // 
            // label70
            // 
            this.label70.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(220, 147);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(13, 15);
            this.label70.TabIndex = 377;
            this.label70.Text = "4";
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(204, 147);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(13, 15);
            this.label71.TabIndex = 376;
            this.label71.Text = "3";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(190, 147);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(13, 15);
            this.label69.TabIndex = 375;
            this.label69.Text = "2";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(175, 147);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(13, 15);
            this.label68.TabIndex = 374;
            this.label68.Text = "1";
            // 
            // label66
            // 
            this.label66.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(622, 139);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(13, 15);
            this.label66.TabIndex = 372;
            this.label66.Text = "^";
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(606, 139);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(13, 15);
            this.label65.TabIndex = 371;
            this.label65.Text = "^";
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(765, 139);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(13, 15);
            this.label61.TabIndex = 370;
            this.label61.Text = "^";
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(749, 139);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(13, 15);
            this.label62.TabIndex = 369;
            this.label62.Text = "^";
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(733, 139);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(13, 15);
            this.label63.TabIndex = 368;
            this.label63.Text = "^";
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(718, 139);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(13, 15);
            this.label64.TabIndex = 367;
            this.label64.Text = "^";
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(702, 139);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(13, 15);
            this.label57.TabIndex = 366;
            this.label57.Text = "^";
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(685, 139);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(13, 15);
            this.label58.TabIndex = 365;
            this.label58.Text = "^";
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(669, 139);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(13, 15);
            this.label59.TabIndex = 364;
            this.label59.Text = "^";
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(654, 139);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(13, 15);
            this.label60.TabIndex = 363;
            this.label60.Text = "^";
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(590, 139);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(13, 15);
            this.label53.TabIndex = 362;
            this.label53.Text = "^";
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(575, 139);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(13, 15);
            this.label54.TabIndex = 361;
            this.label54.Text = "^";
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(558, 139);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(13, 15);
            this.label55.TabIndex = 360;
            this.label55.Text = "^";
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(543, 139);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(13, 15);
            this.label56.TabIndex = 359;
            this.label56.Text = "^";
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(526, 139);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(13, 15);
            this.label49.TabIndex = 358;
            this.label49.Text = "^";
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(509, 139);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(13, 15);
            this.label50.TabIndex = 357;
            this.label50.Text = "^";
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(493, 139);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(13, 15);
            this.label51.TabIndex = 356;
            this.label51.Text = "^";
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(462, 139);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(13, 15);
            this.label52.TabIndex = 355;
            this.label52.Text = "^";
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(445, 139);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(13, 15);
            this.label45.TabIndex = 354;
            this.label45.Text = "^";
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(430, 139);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(13, 15);
            this.label46.TabIndex = 353;
            this.label46.Text = "^";
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(414, 139);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(13, 15);
            this.label47.TabIndex = 352;
            this.label47.Text = "^";
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(398, 139);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 15);
            this.label48.TabIndex = 351;
            this.label48.Text = "^";
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(380, 139);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(13, 15);
            this.label41.TabIndex = 350;
            this.label41.Text = "^";
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(364, 139);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(13, 15);
            this.label42.TabIndex = 349;
            this.label42.Text = "^";
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(348, 139);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(13, 15);
            this.label43.TabIndex = 348;
            this.label43.Text = "^";
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(333, 139);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(13, 15);
            this.label44.TabIndex = 347;
            this.label44.Text = "^";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(286, 139);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(13, 15);
            this.label37.TabIndex = 346;
            this.label37.Text = "^";
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(269, 139);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(13, 15);
            this.label38.TabIndex = 345;
            this.label38.Text = "^";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(254, 139);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 15);
            this.label39.TabIndex = 344;
            this.label39.Text = "^";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(236, 139);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 15);
            this.label40.TabIndex = 343;
            this.label40.Text = "^";
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(220, 139);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(13, 15);
            this.label35.TabIndex = 342;
            this.label35.Text = "^";
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(204, 139);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(13, 15);
            this.label36.TabIndex = 341;
            this.label36.Text = "^";
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(190, 139);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(13, 15);
            this.label34.TabIndex = 340;
            this.label34.Text = "^";
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(175, 139);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(13, 15);
            this.label33.TabIndex = 339;
            this.label33.Text = "^";
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Black;
            this.lbModelNo.Location = new System.Drawing.Point(166, 113);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(1120, 30);
            this.lbModelNo.TabIndex = 338;
            this.lbModelNo.Text = "12345678--123456789-123456789-123456789-123456789-123456789-123456789";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCompleteDate
            // 
            this.lbCompleteDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCompleteDate.ForeColor = System.Drawing.Color.Black;
            this.lbCompleteDate.Location = new System.Drawing.Point(1330, 55);
            this.lbCompleteDate.Name = "lbCompleteDate";
            this.lbCompleteDate.Size = new System.Drawing.Size(94, 20);
            this.lbCompleteDate.TabIndex = 337;
            this.lbCompleteDate.Text = "??/??/????";
            this.lbCompleteDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(1212, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 336;
            this.label4.Text = "Complete Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbProdStartDate
            // 
            this.lbProdStartDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProdStartDate.ForeColor = System.Drawing.Color.Black;
            this.lbProdStartDate.Location = new System.Drawing.Point(1330, 35);
            this.lbProdStartDate.Name = "lbProdStartDate";
            this.lbProdStartDate.Size = new System.Drawing.Size(94, 20);
            this.lbProdStartDate.TabIndex = 335;
            this.lbProdStartDate.Text = "??/??/????";
            this.lbProdStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(1212, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 334;
            this.label2.Text = "Prod Start Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbBOMCreationDate
            // 
            this.lbBOMCreationDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBOMCreationDate.ForeColor = System.Drawing.Color.Black;
            this.lbBOMCreationDate.Location = new System.Drawing.Point(154, 55);
            this.lbBOMCreationDate.Name = "lbBOMCreationDate";
            this.lbBOMCreationDate.Size = new System.Drawing.Size(100, 20);
            this.lbBOMCreationDate.TabIndex = 333;
            this.lbBOMCreationDate.Text = "??/??/????";
            this.lbBOMCreationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Teal;
            this.label27.Location = new System.Drawing.Point(15, 53);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(140, 20);
            this.label27.TabIndex = 332;
            this.label27.Text = "BOM Creation Date:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbShipDate
            // 
            this.lbShipDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShipDate.ForeColor = System.Drawing.Color.Black;
            this.lbShipDate.Location = new System.Drawing.Point(1330, 77);
            this.lbShipDate.Name = "lbShipDate";
            this.lbShipDate.Size = new System.Drawing.Size(94, 20);
            this.lbShipDate.TabIndex = 331;
            this.lbShipDate.Text = "??/??/????";
            this.lbShipDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Teal;
            this.label32.Location = new System.Drawing.Point(1190, 77);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 20);
            this.label32.TabIndex = 330;
            this.label32.Text = "Ship Date:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbOrderDate
            // 
            this.lbOrderDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderDate.ForeColor = System.Drawing.Color.Black;
            this.lbOrderDate.Location = new System.Drawing.Point(154, 35);
            this.lbOrderDate.Name = "lbOrderDate";
            this.lbOrderDate.Size = new System.Drawing.Size(100, 20);
            this.lbOrderDate.TabIndex = 329;
            this.lbOrderDate.Text = "??/??/????";
            this.lbOrderDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Teal;
            this.label26.Location = new System.Drawing.Point(15, 35);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 20);
            this.label26.TabIndex = 328;
            this.label26.Text = "Order Date:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(526, 54);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(140, 25);
            this.label25.TabIndex = 327;
            this.label25.Text = "Job #:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Tahoma", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Black;
            this.lbJobNum.Location = new System.Drawing.Point(665, 54);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(247, 25);
            this.lbJobNum.TabIndex = 326;
            this.lbJobNum.Text = "??????-??-??";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnParseModelNo
            // 
            this.btnParseModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParseModelNo.ForeColor = System.Drawing.Color.Teal;
            this.btnParseModelNo.Location = new System.Drawing.Point(629, 773);
            this.btnParseModelNo.Name = "btnParseModelNo";
            this.btnParseModelNo.Size = new System.Drawing.Size(100, 25);
            this.btnParseModelNo.TabIndex = 531;
            this.btnParseModelNo.Text = "Parse Model#";
            this.btnParseModelNo.UseVisualStyleBackColor = true;
            this.btnParseModelNo.Visible = false;
            this.btnParseModelNo.Click += new System.EventHandler(this.btnParseModelNo_Click);
            // 
            // lbEnterModelNo
            // 
            this.lbEnterModelNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnterModelNo.ForeColor = System.Drawing.Color.Teal;
            this.lbEnterModelNo.Location = new System.Drawing.Point(17, 775);
            this.lbEnterModelNo.Name = "lbEnterModelNo";
            this.lbEnterModelNo.Size = new System.Drawing.Size(101, 20);
            this.lbEnterModelNo.TabIndex = 530;
            this.lbEnterModelNo.Text = "Enter ModelNo:";
            this.lbEnterModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbEnterModelNo.Visible = false;
            // 
            // txtModelNo
            // 
            this.txtModelNo.Location = new System.Drawing.Point(127, 776);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(473, 20);
            this.txtModelNo.TabIndex = 529;
            this.txtModelNo.Visible = false;
            // 
            // btnCreateBOM
            // 
            this.btnCreateBOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateBOM.ForeColor = System.Drawing.Color.Teal;
            this.btnCreateBOM.Location = new System.Drawing.Point(1210, 773);
            this.btnCreateBOM.Name = "btnCreateBOM";
            this.btnCreateBOM.Size = new System.Drawing.Size(100, 25);
            this.btnCreateBOM.TabIndex = 528;
            this.btnCreateBOM.Text = "Create BOM";
            this.btnCreateBOM.UseVisualStyleBackColor = true;
            this.btnCreateBOM.Click += new System.EventHandler(this.btnCreateBOM_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Blue;
            this.btnPrint.Location = new System.Drawing.Point(1082, 773);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 25);
            this.btnPrint.TabIndex = 527;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1336, 772);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 25);
            this.btnExit.TabIndex = 526;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label102
            // 
            this.label102.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(302, 147);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(13, 15);
            this.label102.TabIndex = 533;
            this.label102.Text = "9";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(302, 139);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(13, 15);
            this.label67.TabIndex = 532;
            this.label67.Text = "^";
            // 
            // label215
            // 
            this.label215.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label215.ForeColor = System.Drawing.Color.Black;
            this.label215.Location = new System.Drawing.Point(1261, 162);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(13, 15);
            this.label215.TabIndex = 611;
            this.label215.Text = "9";
            // 
            // label216
            // 
            this.label216.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label216.ForeColor = System.Drawing.Color.Black;
            this.label216.Location = new System.Drawing.Point(1261, 147);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(13, 15);
            this.label216.TabIndex = 610;
            this.label216.Text = "6";
            // 
            // label217
            // 
            this.label217.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label217.ForeColor = System.Drawing.Color.Black;
            this.label217.Location = new System.Drawing.Point(1261, 139);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(13, 15);
            this.label217.TabIndex = 609;
            this.label217.Text = "^";
            // 
            // label218
            // 
            this.label218.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label218.ForeColor = System.Drawing.Color.Black;
            this.label218.Location = new System.Drawing.Point(1245, 162);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(13, 15);
            this.label218.TabIndex = 608;
            this.label218.Text = "8";
            // 
            // label219
            // 
            this.label219.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label219.ForeColor = System.Drawing.Color.Black;
            this.label219.Location = new System.Drawing.Point(1229, 162);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(13, 15);
            this.label219.TabIndex = 607;
            this.label219.Text = "7";
            // 
            // label220
            // 
            this.label220.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label220.ForeColor = System.Drawing.Color.Black;
            this.label220.Location = new System.Drawing.Point(1213, 162);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(13, 15);
            this.label220.TabIndex = 606;
            this.label220.Text = "6";
            // 
            // label221
            // 
            this.label221.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label221.ForeColor = System.Drawing.Color.Black;
            this.label221.Location = new System.Drawing.Point(1197, 162);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(13, 15);
            this.label221.TabIndex = 605;
            this.label221.Text = "5";
            // 
            // label222
            // 
            this.label222.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label222.ForeColor = System.Drawing.Color.Black;
            this.label222.Location = new System.Drawing.Point(1182, 162);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(13, 15);
            this.label222.TabIndex = 604;
            this.label222.Text = "4";
            // 
            // label223
            // 
            this.label223.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label223.ForeColor = System.Drawing.Color.Black;
            this.label223.Location = new System.Drawing.Point(1165, 162);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(13, 15);
            this.label223.TabIndex = 603;
            this.label223.Text = "3";
            // 
            // label224
            // 
            this.label224.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label224.ForeColor = System.Drawing.Color.Black;
            this.label224.Location = new System.Drawing.Point(1148, 162);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(13, 15);
            this.label224.TabIndex = 602;
            this.label224.Text = "2";
            // 
            // label225
            // 
            this.label225.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label225.ForeColor = System.Drawing.Color.Black;
            this.label225.Location = new System.Drawing.Point(1245, 147);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(13, 15);
            this.label225.TabIndex = 601;
            this.label225.Text = "6";
            // 
            // label226
            // 
            this.label226.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label226.ForeColor = System.Drawing.Color.Black;
            this.label226.Location = new System.Drawing.Point(1229, 147);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(13, 15);
            this.label226.TabIndex = 600;
            this.label226.Text = "6";
            // 
            // label227
            // 
            this.label227.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label227.ForeColor = System.Drawing.Color.Black;
            this.label227.Location = new System.Drawing.Point(1213, 147);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(13, 15);
            this.label227.TabIndex = 599;
            this.label227.Text = "6";
            // 
            // label228
            // 
            this.label228.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label228.ForeColor = System.Drawing.Color.Black;
            this.label228.Location = new System.Drawing.Point(1197, 147);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(13, 15);
            this.label228.TabIndex = 598;
            this.label228.Text = "6";
            // 
            // label229
            // 
            this.label229.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label229.ForeColor = System.Drawing.Color.Black;
            this.label229.Location = new System.Drawing.Point(1182, 147);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(13, 15);
            this.label229.TabIndex = 597;
            this.label229.Text = "6";
            // 
            // label230
            // 
            this.label230.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label230.ForeColor = System.Drawing.Color.Black;
            this.label230.Location = new System.Drawing.Point(1165, 147);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(13, 15);
            this.label230.TabIndex = 596;
            this.label230.Text = "6";
            // 
            // label231
            // 
            this.label231.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label231.ForeColor = System.Drawing.Color.Black;
            this.label231.Location = new System.Drawing.Point(1148, 147);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(13, 15);
            this.label231.TabIndex = 595;
            this.label231.Text = "6";
            // 
            // label232
            // 
            this.label232.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label232.ForeColor = System.Drawing.Color.Black;
            this.label232.Location = new System.Drawing.Point(1245, 139);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(13, 15);
            this.label232.TabIndex = 594;
            this.label232.Text = "^";
            // 
            // label233
            // 
            this.label233.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label233.ForeColor = System.Drawing.Color.Black;
            this.label233.Location = new System.Drawing.Point(1229, 139);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(13, 15);
            this.label233.TabIndex = 593;
            this.label233.Text = "^";
            // 
            // label234
            // 
            this.label234.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label234.ForeColor = System.Drawing.Color.Black;
            this.label234.Location = new System.Drawing.Point(1213, 139);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(13, 15);
            this.label234.TabIndex = 592;
            this.label234.Text = "^";
            // 
            // label235
            // 
            this.label235.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label235.ForeColor = System.Drawing.Color.Black;
            this.label235.Location = new System.Drawing.Point(1197, 139);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(13, 15);
            this.label235.TabIndex = 591;
            this.label235.Text = "^";
            // 
            // label236
            // 
            this.label236.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label236.ForeColor = System.Drawing.Color.Black;
            this.label236.Location = new System.Drawing.Point(1182, 139);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(13, 15);
            this.label236.TabIndex = 590;
            this.label236.Text = "^";
            // 
            // label237
            // 
            this.label237.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label237.ForeColor = System.Drawing.Color.Black;
            this.label237.Location = new System.Drawing.Point(1165, 139);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(13, 15);
            this.label237.TabIndex = 589;
            this.label237.Text = "^";
            // 
            // label238
            // 
            this.label238.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label238.ForeColor = System.Drawing.Color.Black;
            this.label238.Location = new System.Drawing.Point(1149, 139);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(13, 15);
            this.label238.TabIndex = 588;
            this.label238.Text = "^";
            // 
            // label212
            // 
            this.label212.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label212.ForeColor = System.Drawing.Color.Black;
            this.label212.Location = new System.Drawing.Point(1132, 162);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(13, 15);
            this.label212.TabIndex = 587;
            this.label212.Text = "1";
            // 
            // label213
            // 
            this.label213.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label213.ForeColor = System.Drawing.Color.Black;
            this.label213.Location = new System.Drawing.Point(1132, 147);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(13, 15);
            this.label213.TabIndex = 586;
            this.label213.Text = "6";
            // 
            // label214
            // 
            this.label214.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label214.ForeColor = System.Drawing.Color.Black;
            this.label214.Location = new System.Drawing.Point(1132, 139);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(13, 15);
            this.label214.TabIndex = 585;
            this.label214.Text = "^";
            // 
            // label134
            // 
            this.label134.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.ForeColor = System.Drawing.Color.Black;
            this.label134.Location = new System.Drawing.Point(1101, 162);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(13, 15);
            this.label134.TabIndex = 584;
            this.label134.Text = "9";
            // 
            // label135
            // 
            this.label135.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.ForeColor = System.Drawing.Color.Black;
            this.label135.Location = new System.Drawing.Point(1101, 147);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(13, 15);
            this.label135.TabIndex = 583;
            this.label135.Text = "5";
            // 
            // label136
            // 
            this.label136.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.Color.Black;
            this.label136.Location = new System.Drawing.Point(1101, 139);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(13, 15);
            this.label136.TabIndex = 582;
            this.label136.Text = "^";
            // 
            // label137
            // 
            this.label137.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.ForeColor = System.Drawing.Color.Black;
            this.label137.Location = new System.Drawing.Point(1085, 162);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(13, 15);
            this.label137.TabIndex = 581;
            this.label137.Text = "8";
            // 
            // label138
            // 
            this.label138.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.ForeColor = System.Drawing.Color.Black;
            this.label138.Location = new System.Drawing.Point(1069, 162);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(13, 15);
            this.label138.TabIndex = 580;
            this.label138.Text = "7";
            // 
            // label139
            // 
            this.label139.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.ForeColor = System.Drawing.Color.Black;
            this.label139.Location = new System.Drawing.Point(1053, 162);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(13, 15);
            this.label139.TabIndex = 579;
            this.label139.Text = "6";
            // 
            // label140
            // 
            this.label140.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.ForeColor = System.Drawing.Color.Black;
            this.label140.Location = new System.Drawing.Point(1037, 162);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(13, 15);
            this.label140.TabIndex = 578;
            this.label140.Text = "5";
            // 
            // label141
            // 
            this.label141.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.ForeColor = System.Drawing.Color.Black;
            this.label141.Location = new System.Drawing.Point(1022, 162);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(13, 15);
            this.label141.TabIndex = 577;
            this.label141.Text = "4";
            // 
            // label142
            // 
            this.label142.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.ForeColor = System.Drawing.Color.Black;
            this.label142.Location = new System.Drawing.Point(1005, 162);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(13, 15);
            this.label142.TabIndex = 576;
            this.label142.Text = "3";
            // 
            // label143
            // 
            this.label143.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.ForeColor = System.Drawing.Color.Black;
            this.label143.Location = new System.Drawing.Point(988, 162);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(13, 15);
            this.label143.TabIndex = 575;
            this.label143.Text = "2";
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.Black;
            this.label144.Location = new System.Drawing.Point(972, 162);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(13, 15);
            this.label144.TabIndex = 574;
            this.label144.Text = "1";
            // 
            // label145
            // 
            this.label145.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.ForeColor = System.Drawing.Color.Black;
            this.label145.Location = new System.Drawing.Point(1085, 147);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(13, 15);
            this.label145.TabIndex = 573;
            this.label145.Text = "5";
            // 
            // label146
            // 
            this.label146.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.Black;
            this.label146.Location = new System.Drawing.Point(1069, 147);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(13, 15);
            this.label146.TabIndex = 572;
            this.label146.Text = "5";
            // 
            // label147
            // 
            this.label147.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.ForeColor = System.Drawing.Color.Black;
            this.label147.Location = new System.Drawing.Point(1053, 147);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(13, 15);
            this.label147.TabIndex = 571;
            this.label147.Text = "5";
            // 
            // label148
            // 
            this.label148.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.Black;
            this.label148.Location = new System.Drawing.Point(1037, 147);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(13, 15);
            this.label148.TabIndex = 570;
            this.label148.Text = "5";
            // 
            // label149
            // 
            this.label149.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.Black;
            this.label149.Location = new System.Drawing.Point(1022, 147);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(13, 15);
            this.label149.TabIndex = 569;
            this.label149.Text = "5";
            // 
            // label150
            // 
            this.label150.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.Black;
            this.label150.Location = new System.Drawing.Point(1005, 147);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(13, 15);
            this.label150.TabIndex = 568;
            this.label150.Text = "5";
            // 
            // label151
            // 
            this.label151.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.ForeColor = System.Drawing.Color.Black;
            this.label151.Location = new System.Drawing.Point(988, 147);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(13, 15);
            this.label151.TabIndex = 567;
            this.label151.Text = "5";
            // 
            // label152
            // 
            this.label152.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.ForeColor = System.Drawing.Color.Black;
            this.label152.Location = new System.Drawing.Point(972, 147);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(13, 15);
            this.label152.TabIndex = 566;
            this.label152.Text = "5";
            // 
            // label153
            // 
            this.label153.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.ForeColor = System.Drawing.Color.Black;
            this.label153.Location = new System.Drawing.Point(1085, 139);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(13, 15);
            this.label153.TabIndex = 565;
            this.label153.Text = "^";
            // 
            // label154
            // 
            this.label154.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.ForeColor = System.Drawing.Color.Black;
            this.label154.Location = new System.Drawing.Point(1069, 139);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(13, 15);
            this.label154.TabIndex = 564;
            this.label154.Text = "^";
            // 
            // label155
            // 
            this.label155.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.ForeColor = System.Drawing.Color.Black;
            this.label155.Location = new System.Drawing.Point(1053, 139);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(13, 15);
            this.label155.TabIndex = 563;
            this.label155.Text = "^";
            // 
            // label156
            // 
            this.label156.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.ForeColor = System.Drawing.Color.Black;
            this.label156.Location = new System.Drawing.Point(1037, 139);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(13, 15);
            this.label156.TabIndex = 562;
            this.label156.Text = "^";
            // 
            // label157
            // 
            this.label157.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.ForeColor = System.Drawing.Color.Black;
            this.label157.Location = new System.Drawing.Point(1022, 139);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(13, 15);
            this.label157.TabIndex = 561;
            this.label157.Text = "^";
            // 
            // label158
            // 
            this.label158.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.ForeColor = System.Drawing.Color.Black;
            this.label158.Location = new System.Drawing.Point(1005, 139);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(13, 15);
            this.label158.TabIndex = 560;
            this.label158.Text = "^";
            // 
            // label159
            // 
            this.label159.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.ForeColor = System.Drawing.Color.Black;
            this.label159.Location = new System.Drawing.Point(989, 139);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(13, 15);
            this.label159.TabIndex = 559;
            this.label159.Text = "^";
            // 
            // label160
            // 
            this.label160.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.ForeColor = System.Drawing.Color.Black;
            this.label160.Location = new System.Drawing.Point(973, 139);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(13, 15);
            this.label160.TabIndex = 558;
            this.label160.Text = "^";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(941, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 15);
            this.label10.TabIndex = 557;
            this.label10.Text = "9";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(941, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 15);
            this.label11.TabIndex = 556;
            this.label11.Text = "4";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(941, 139);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 15);
            this.label12.TabIndex = 555;
            this.label12.Text = "^";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(925, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 15);
            this.label13.TabIndex = 554;
            this.label13.Text = "8";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(909, 162);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 15);
            this.label15.TabIndex = 553;
            this.label15.Text = "7";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(893, 162);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 15);
            this.label16.TabIndex = 552;
            this.label16.Text = "6";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(877, 162);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 15);
            this.label17.TabIndex = 551;
            this.label17.Text = "5";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(862, 162);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 15);
            this.label18.TabIndex = 550;
            this.label18.Text = "4";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(845, 162);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 15);
            this.label19.TabIndex = 549;
            this.label19.Text = "3";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(828, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 15);
            this.label20.TabIndex = 548;
            this.label20.Text = "2";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(925, 147);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 15);
            this.label21.TabIndex = 547;
            this.label21.Text = "4";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(909, 147);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 15);
            this.label23.TabIndex = 546;
            this.label23.Text = "4";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(893, 147);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 15);
            this.label24.TabIndex = 545;
            this.label24.Text = "4";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(877, 147);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 15);
            this.label28.TabIndex = 544;
            this.label28.Text = "4";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(862, 147);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 15);
            this.label29.TabIndex = 543;
            this.label29.Text = "4";
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(845, 147);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 15);
            this.label30.TabIndex = 542;
            this.label30.Text = "4";
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(828, 147);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(13, 15);
            this.label31.TabIndex = 541;
            this.label31.Text = "4";
            // 
            // label132
            // 
            this.label132.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.Black;
            this.label132.Location = new System.Drawing.Point(925, 139);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(13, 15);
            this.label132.TabIndex = 540;
            this.label132.Text = "^";
            // 
            // label163
            // 
            this.label163.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.ForeColor = System.Drawing.Color.Black;
            this.label163.Location = new System.Drawing.Point(909, 139);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(13, 15);
            this.label163.TabIndex = 539;
            this.label163.Text = "^";
            // 
            // label164
            // 
            this.label164.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.ForeColor = System.Drawing.Color.Black;
            this.label164.Location = new System.Drawing.Point(893, 139);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(13, 15);
            this.label164.TabIndex = 538;
            this.label164.Text = "^";
            // 
            // label165
            // 
            this.label165.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.ForeColor = System.Drawing.Color.Black;
            this.label165.Location = new System.Drawing.Point(877, 139);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(13, 15);
            this.label165.TabIndex = 537;
            this.label165.Text = "^";
            // 
            // label195
            // 
            this.label195.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label195.ForeColor = System.Drawing.Color.Black;
            this.label195.Location = new System.Drawing.Point(862, 139);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(13, 15);
            this.label195.TabIndex = 536;
            this.label195.Text = "^";
            // 
            // label196
            // 
            this.label196.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label196.ForeColor = System.Drawing.Color.Black;
            this.label196.Location = new System.Drawing.Point(845, 139);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(13, 15);
            this.label196.TabIndex = 535;
            this.label196.Text = "^";
            // 
            // label197
            // 
            this.label197.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label197.ForeColor = System.Drawing.Color.Black;
            this.label197.Location = new System.Drawing.Point(829, 139);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(13, 15);
            this.label197.TabIndex = 534;
            this.label197.Text = "^";
            // 
            // msConfigMenu
            // 
            this.msConfigMenu.BackColor = System.Drawing.Color.LightSalmon;
            this.msConfigMenu.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msConfigMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.msConfigMenu.Location = new System.Drawing.Point(0, 0);
            this.msConfigMenu.Name = "msConfigMenu";
            this.msConfigMenu.Size = new System.Drawing.Size(1448, 24);
            this.msConfigMenu.TabIndex = 612;
            this.msConfigMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createBOMToolStripMenuItem,
            this.deleteBOMToolStripMenuItem,
            this.printToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createBOMToolStripMenuItem
            // 
            this.createBOMToolStripMenuItem.Name = "createBOMToolStripMenuItem";
            this.createBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.createBOMToolStripMenuItem.Text = "Create BOM";
            this.createBOMToolStripMenuItem.Click += new System.EventHandler(this.createBOMToolStripMenuItem_Click);
            // 
            // deleteBOMToolStripMenuItem
            // 
            this.deleteBOMToolStripMenuItem.Name = "deleteBOMToolStripMenuItem";
            this.deleteBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteBOMToolStripMenuItem.Text = "Delete BOM";
            this.deleteBOMToolStripMenuItem.Click += new System.EventHandler(this.deleteBOMToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Engravers MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.Blue;
            this.lbEnvironment.Location = new System.Drawing.Point(464, 34);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(521, 20);
            this.lbEnvironment.TabIndex = 329;
            this.lbEnvironment.Text = "Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMSP_ModelNoConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1448, 799);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.msConfigMenu);
            this.Controls.Add(this.label215);
            this.Controls.Add(this.label216);
            this.Controls.Add(this.label217);
            this.Controls.Add(this.label218);
            this.Controls.Add(this.label219);
            this.Controls.Add(this.label220);
            this.Controls.Add(this.label221);
            this.Controls.Add(this.label222);
            this.Controls.Add(this.label223);
            this.Controls.Add(this.label224);
            this.Controls.Add(this.label225);
            this.Controls.Add(this.label226);
            this.Controls.Add(this.label227);
            this.Controls.Add(this.label228);
            this.Controls.Add(this.label229);
            this.Controls.Add(this.label230);
            this.Controls.Add(this.label231);
            this.Controls.Add(this.label232);
            this.Controls.Add(this.label233);
            this.Controls.Add(this.label234);
            this.Controls.Add(this.label235);
            this.Controls.Add(this.label236);
            this.Controls.Add(this.label237);
            this.Controls.Add(this.label238);
            this.Controls.Add(this.label212);
            this.Controls.Add(this.label213);
            this.Controls.Add(this.label214);
            this.Controls.Add(this.label134);
            this.Controls.Add(this.label135);
            this.Controls.Add(this.label136);
            this.Controls.Add(this.label137);
            this.Controls.Add(this.label138);
            this.Controls.Add(this.label139);
            this.Controls.Add(this.label140);
            this.Controls.Add(this.label141);
            this.Controls.Add(this.label142);
            this.Controls.Add(this.label143);
            this.Controls.Add(this.label144);
            this.Controls.Add(this.label145);
            this.Controls.Add(this.label146);
            this.Controls.Add(this.label147);
            this.Controls.Add(this.label148);
            this.Controls.Add(this.label149);
            this.Controls.Add(this.label150);
            this.Controls.Add(this.label151);
            this.Controls.Add(this.label152);
            this.Controls.Add(this.label153);
            this.Controls.Add(this.label154);
            this.Controls.Add(this.label155);
            this.Controls.Add(this.label156);
            this.Controls.Add(this.label157);
            this.Controls.Add(this.label158);
            this.Controls.Add(this.label159);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label132);
            this.Controls.Add(this.label163);
            this.Controls.Add(this.label164);
            this.Controls.Add(this.label165);
            this.Controls.Add(this.label195);
            this.Controls.Add(this.label196);
            this.Controls.Add(this.label197);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.btnParseModelNo);
            this.Controls.Add(this.lbEnterModelNo);
            this.Controls.Add(this.txtModelNo);
            this.Controls.Add(this.btnCreateBOM);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lbUnitType);
            this.Controls.Add(this.lbLastUpdateBy);
            this.Controls.Add(this.lbBomCreateBy);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.gbModelNoConfig);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label133);
            this.Controls.Add(this.lbLastUpdateDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label129);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.lbCompleteDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbProdStartDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbBOMCreationDate);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lbShipDate);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.lbOrderDate);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbJobNum);
            this.Name = "frmMSP_ModelNoConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MSP ModelNo Configuration";
            this.gbModelNoConfig.ResumeLayout(false);
            this.msConfigMenu.ResumeLayout(false);
            this.msConfigMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbUnitType;
        public System.Windows.Forms.Label lbLastUpdateBy;
        public System.Windows.Forms.Label lbBomCreateBy;
        public System.Windows.Forms.Label lbCustName;
        public System.Windows.Forms.GroupBox gbModelNoConfig;
        public System.Windows.Forms.ComboBox cbFilterOptions;
        private System.Windows.Forms.Label label194;
        public System.Windows.Forms.ComboBox cbSupplyFanMotorSize;
        private System.Windows.Forms.Label label193;
        public System.Windows.Forms.ComboBox cbSupplyFanMotorType;
        private System.Windows.Forms.Label label192;
        public System.Windows.Forms.ComboBox cbSupplyFanWheel;
        private System.Windows.Forms.Label label191;
        public System.Windows.Forms.ComboBox cbBypassDamper;
        public System.Windows.Forms.ComboBox cbCoilCorrisionPackage;
        public System.Windows.Forms.ComboBox cbCabinetDesign;
        private System.Windows.Forms.Label label161;
        public System.Windows.Forms.ComboBox cbVoltage;
        private System.Windows.Forms.Label label162;
        public System.Windows.Forms.ComboBox cbWarranty;
        private System.Windows.Forms.Label label166;
        public System.Windows.Forms.ComboBox cbServiceLights;
        private System.Windows.Forms.Label label167;
        public System.Windows.Forms.ComboBox cbElectricalOptions;
        private System.Windows.Forms.Label label168;
        public System.Windows.Forms.ComboBox cbControlsDisplay;
        private System.Windows.Forms.Label label169;
        public System.Windows.Forms.ComboBox cbControls;
        private System.Windows.Forms.Label label170;
        public System.Windows.Forms.ComboBox cbCondensateOverflowSwitch;
        private System.Windows.Forms.Label label171;
        public System.Windows.Forms.ComboBox cbSmokeDetector;
        private System.Windows.Forms.Label label172;
        public System.Windows.Forms.ComboBox cbAccessories;
        private System.Windows.Forms.Label label173;
        public System.Windows.Forms.ComboBox cbAirflowMonitoring;
        private System.Windows.Forms.Label label174;
        public System.Windows.Forms.ComboBox cbAltitude;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label labelCoilcorrisionPackage;
        public System.Windows.Forms.ComboBox cbCabinetConstructOutdoor;
        private System.Windows.Forms.Label label177;
        public System.Windows.Forms.ComboBox cbHeatExchangerType;
        private System.Windows.Forms.Label label178;
        public System.Windows.Forms.ComboBox cbHeatingCoilFluidType;
        private System.Windows.Forms.Label label179;
        public System.Windows.Forms.ComboBox cbPostHeatingCoilSize;
        private System.Windows.Forms.Label label180;
        public System.Windows.Forms.ComboBox cbPostHeatingCoilType;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        public System.Windows.Forms.ComboBox cbCoilType;
        public System.Windows.Forms.ComboBox cbModelSize;
        private System.Windows.Forms.Label label183;
        public System.Windows.Forms.ComboBox cbConvenienceOutlet;
        private System.Windows.Forms.Label label184;
        public System.Windows.Forms.ComboBox cbPostCoolingCoilSize;
        private System.Windows.Forms.Label label185;
        public System.Windows.Forms.ComboBox cbCoilSize;
        private System.Windows.Forms.Label label186;
        public System.Windows.Forms.ComboBox cbPostCoolingCoilType;
        public System.Windows.Forms.ComboBox cbCabinetConstuctIndoor;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        public System.Windows.Forms.ComboBox cbMajorDesignSeq;
        private System.Windows.Forms.Label label189;
        public System.Windows.Forms.ComboBox cbUnitType;
        private System.Windows.Forms.Label label190;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label133;
        public System.Windows.Forms.Label lbLastUpdateDate;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label129;
        public System.Windows.Forms.Label label130;
        public System.Windows.Forms.Label label131;
        public System.Windows.Forms.Label label127;
        public System.Windows.Forms.Label label128;
        public System.Windows.Forms.Label label119;
        public System.Windows.Forms.Label label120;
        public System.Windows.Forms.Label label121;
        public System.Windows.Forms.Label label122;
        public System.Windows.Forms.Label label123;
        public System.Windows.Forms.Label label124;
        public System.Windows.Forms.Label label125;
        public System.Windows.Forms.Label label126;
        public System.Windows.Forms.Label label111;
        public System.Windows.Forms.Label label112;
        public System.Windows.Forms.Label label113;
        public System.Windows.Forms.Label label114;
        public System.Windows.Forms.Label label115;
        public System.Windows.Forms.Label label116;
        public System.Windows.Forms.Label label117;
        public System.Windows.Forms.Label label118;
        public System.Windows.Forms.Label label103;
        public System.Windows.Forms.Label label104;
        public System.Windows.Forms.Label label105;
        public System.Windows.Forms.Label label106;
        public System.Windows.Forms.Label label107;
        public System.Windows.Forms.Label label108;
        public System.Windows.Forms.Label label109;
        public System.Windows.Forms.Label label110;
        public System.Windows.Forms.Label label100;
        public System.Windows.Forms.Label label101;
        public System.Windows.Forms.Label label96;
        public System.Windows.Forms.Label label97;
        public System.Windows.Forms.Label label98;
        public System.Windows.Forms.Label label99;
        public System.Windows.Forms.Label label92;
        public System.Windows.Forms.Label label93;
        public System.Windows.Forms.Label label94;
        public System.Windows.Forms.Label label95;
        public System.Windows.Forms.Label label88;
        public System.Windows.Forms.Label label89;
        public System.Windows.Forms.Label label90;
        public System.Windows.Forms.Label label91;
        public System.Windows.Forms.Label label84;
        public System.Windows.Forms.Label label85;
        public System.Windows.Forms.Label label86;
        public System.Windows.Forms.Label label87;
        public System.Windows.Forms.Label label80;
        public System.Windows.Forms.Label label81;
        public System.Windows.Forms.Label label82;
        public System.Windows.Forms.Label label83;
        public System.Windows.Forms.Label label76;
        public System.Windows.Forms.Label label77;
        public System.Windows.Forms.Label label78;
        public System.Windows.Forms.Label label79;
        public System.Windows.Forms.Label label72;
        public System.Windows.Forms.Label label73;
        public System.Windows.Forms.Label label74;
        public System.Windows.Forms.Label label75;
        public System.Windows.Forms.Label label70;
        public System.Windows.Forms.Label label71;
        public System.Windows.Forms.Label label69;
        public System.Windows.Forms.Label label68;
        public System.Windows.Forms.Label label66;
        public System.Windows.Forms.Label label65;
        public System.Windows.Forms.Label label61;
        public System.Windows.Forms.Label label62;
        public System.Windows.Forms.Label label63;
        public System.Windows.Forms.Label label64;
        public System.Windows.Forms.Label label57;
        public System.Windows.Forms.Label label58;
        public System.Windows.Forms.Label label59;
        public System.Windows.Forms.Label label60;
        public System.Windows.Forms.Label label53;
        public System.Windows.Forms.Label label54;
        public System.Windows.Forms.Label label55;
        public System.Windows.Forms.Label label56;
        public System.Windows.Forms.Label label49;
        public System.Windows.Forms.Label label50;
        public System.Windows.Forms.Label label51;
        public System.Windows.Forms.Label label52;
        public System.Windows.Forms.Label label45;
        public System.Windows.Forms.Label label46;
        public System.Windows.Forms.Label label47;
        public System.Windows.Forms.Label label48;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label lbModelNo;
        public System.Windows.Forms.Label lbCompleteDate;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lbProdStartDate;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lbBOMCreationDate;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label lbShipDate;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label lbOrderDate;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.Button btnParseModelNo;
        public System.Windows.Forms.Label lbEnterModelNo;
        public System.Windows.Forms.TextBox txtModelNo;
        private System.Windows.Forms.Button btnCreateBOM;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.ComboBox cbMSP_CoolingCoilFluidType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label102;
        public System.Windows.Forms.Label label67;
        public System.Windows.Forms.Label label215;
        public System.Windows.Forms.Label label216;
        public System.Windows.Forms.Label label217;
        public System.Windows.Forms.Label label218;
        public System.Windows.Forms.Label label219;
        public System.Windows.Forms.Label label220;
        public System.Windows.Forms.Label label221;
        public System.Windows.Forms.Label label222;
        public System.Windows.Forms.Label label223;
        public System.Windows.Forms.Label label224;
        public System.Windows.Forms.Label label225;
        public System.Windows.Forms.Label label226;
        public System.Windows.Forms.Label label227;
        public System.Windows.Forms.Label label228;
        public System.Windows.Forms.Label label229;
        public System.Windows.Forms.Label label230;
        public System.Windows.Forms.Label label231;
        public System.Windows.Forms.Label label232;
        public System.Windows.Forms.Label label233;
        public System.Windows.Forms.Label label234;
        public System.Windows.Forms.Label label235;
        public System.Windows.Forms.Label label236;
        public System.Windows.Forms.Label label237;
        public System.Windows.Forms.Label label238;
        public System.Windows.Forms.Label label212;
        public System.Windows.Forms.Label label213;
        public System.Windows.Forms.Label label214;
        public System.Windows.Forms.Label label134;
        public System.Windows.Forms.Label label135;
        public System.Windows.Forms.Label label136;
        public System.Windows.Forms.Label label137;
        public System.Windows.Forms.Label label138;
        public System.Windows.Forms.Label label139;
        public System.Windows.Forms.Label label140;
        public System.Windows.Forms.Label label141;
        public System.Windows.Forms.Label label142;
        public System.Windows.Forms.Label label143;
        public System.Windows.Forms.Label label144;
        public System.Windows.Forms.Label label145;
        public System.Windows.Forms.Label label146;
        public System.Windows.Forms.Label label147;
        public System.Windows.Forms.Label label148;
        public System.Windows.Forms.Label label149;
        public System.Windows.Forms.Label label150;
        public System.Windows.Forms.Label label151;
        public System.Windows.Forms.Label label152;
        public System.Windows.Forms.Label label153;
        public System.Windows.Forms.Label label154;
        public System.Windows.Forms.Label label155;
        public System.Windows.Forms.Label label156;
        public System.Windows.Forms.Label label157;
        public System.Windows.Forms.Label label158;
        public System.Windows.Forms.Label label159;
        public System.Windows.Forms.Label label160;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label label132;
        public System.Windows.Forms.Label label163;
        public System.Windows.Forms.Label label164;
        public System.Windows.Forms.Label label165;
        public System.Windows.Forms.Label label195;
        public System.Windows.Forms.Label label196;
        public System.Windows.Forms.Label label197;
        private System.Windows.Forms.MenuStrip msConfigMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.Label lbEnvironment;
    }
}