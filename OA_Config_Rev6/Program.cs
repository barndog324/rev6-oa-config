﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            
            if (args.Length == 0)
            {
                GlobalJob.JobNumber = "";
                GlobalJob.ModelNo = "";
                GlobalJob.ProgramMode = "DisplayMode";
                //GlobalJob.JobNumber = "271150-1-1";
                //GlobalJob.ModelNo = "OAND480A4-D1B400LU-A1R00AP7LN-G82B3B3A0";
                //GlobalJob.ProgramMode = "JobEntryMode";      
            }
            else
            {
                //GlobalJob.JobNumber = "264595-2-1";
                //GlobalJob.ModelNo = "OADG01024-D1B11AE43-J1AJG1AA3-11A10C02B-A01A01000-A00000000-000000000";
                GlobalJob.JobNumber = args[0];
                GlobalJob.ModelNo = args[1];
                GlobalJob.ProgramMode = "JobEntryMode";               
            }
           
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

            if (envStr.Contains("Epicor905APP") == true)
            {
                GlobalJob.EnvColor = Color.Snow;
                GlobalJob.EnvironmentStr = "Production Environment";
                GlobalJob.DisplayMode = "ProductionCurrent";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=EPICOR905SQL\\EPICOR905;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
            {
                GlobalJob.EnvColor = Color.Snow;
                GlobalJob.EnvironmentStr = "New Production Environment";
                GlobalJob.DisplayMode = "ProductionNew";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=kccwvpepic9sql;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            else if (envStr.Contains("kccwvpkntcapp01") == true)
            {
                GlobalJob.EnvColor = Color.Snow;
                GlobalJob.EnvironmentStr = "Kinetic Production Environment";
                GlobalJob.DisplayMode = "ProductionKinetic";

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=kccwvpkntcsql01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }

#if DEBUG
            try           // If in the debug environment then set the Database connection string to the test database.
            {
                GlobalJob.EnvColor = Color.LightCoral;
                GlobalJob.EnvironmentStr = "Development Environment";
                GlobalJob.DisplayMode = "Debug";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
            }
#if TEST2
            try           // If in the debug environment then set the Database connection string to the test database.
            {
                GlobalJob.EnvColor = Color.SpringGreen;
                GlobalJob.EnvironmentStr = "Test 2 Environment";
                GlobalJob.DisplayMode = "Test";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
            }
#endif
#endif           

            //MessageBox.Show("Env display mode = " + GlobalJob.DisplayMode);
            //GlobalJob.JobNumber = "";
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());           
            
        }
    }
}
