﻿namespace OA_Config_Rev6
{
    partial class frmDateRange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDatePickOK = new System.Windows.Forms.Button();
            this.btnDatePickExit = new System.Windows.Forms.Button();
            this.dtpDatePickEndDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDatePickStartDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDatePickOK
            // 
            this.btnDatePickOK.Location = new System.Drawing.Point(55, 83);
            this.btnDatePickOK.Name = "btnDatePickOK";
            this.btnDatePickOK.Size = new System.Drawing.Size(75, 23);
            this.btnDatePickOK.TabIndex = 11;
            this.btnDatePickOK.Text = "OK";
            this.btnDatePickOK.UseVisualStyleBackColor = true;
            this.btnDatePickOK.Click += new System.EventHandler(this.btnDatePickOK_Click);
            // 
            // btnDatePickExit
            // 
            this.btnDatePickExit.Location = new System.Drawing.Point(159, 83);
            this.btnDatePickExit.Name = "btnDatePickExit";
            this.btnDatePickExit.Size = new System.Drawing.Size(75, 23);
            this.btnDatePickExit.TabIndex = 10;
            this.btnDatePickExit.Text = "Exit";
            this.btnDatePickExit.UseVisualStyleBackColor = true;
            this.btnDatePickExit.Click += new System.EventHandler(this.btnDatePickExit_Click);
            // 
            // dtpDatePickEndDate
            // 
            this.dtpDatePickEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatePickEndDate.Location = new System.Drawing.Point(133, 47);
            this.dtpDatePickEndDate.Name = "dtpDatePickEndDate";
            this.dtpDatePickEndDate.Size = new System.Drawing.Size(105, 20);
            this.dtpDatePickEndDate.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(52, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "End Date:";
            // 
            // dtpDatePickStartDate
            // 
            this.dtpDatePickStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatePickStartDate.Location = new System.Drawing.Point(133, 21);
            this.dtpDatePickStartDate.Name = "dtpDatePickStartDate";
            this.dtpDatePickStartDate.Size = new System.Drawing.Size(105, 20);
            this.dtpDatePickStartDate.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Start Date:";
            // 
            // frmDateRange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 129);
            this.Controls.Add(this.btnDatePickOK);
            this.Controls.Add(this.btnDatePickExit);
            this.Controls.Add(this.dtpDatePickEndDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpDatePickStartDate);
            this.Controls.Add(this.label1);
            this.Name = "frmDateRange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enter Date Range";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDatePickOK;
        private System.Windows.Forms.Button btnDatePickExit;
        private System.Windows.Forms.DateTimePicker dtpDatePickEndDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDatePickStartDate;
        private System.Windows.Forms.Label label1;
    }
}