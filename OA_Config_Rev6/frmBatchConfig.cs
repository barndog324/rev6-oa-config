﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace OA_Config_Rev6
{
    public partial class frmBatchConfig : Form
    {
        MainBO objMain = new MainBO();

        public frmBatchConfig()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string resultsMsg = "";
            string jobNum = "";
            string modelNo = "";
            string unitType = "";
            string mcaStr = "";
            string mopStr = "";
            string rawMopStr = "";
            string circuit1Charge = "";
            string circuit2Charge = "";

            foreach (DataGridViewRow dgvr in dgvMainUnitList.Rows)
            {
                jobNum = dgvr.Cells["JobNum"].Value.ToString();
                modelNo = dgvr.Cells["ModelNo"].Value.ToString();
                unitType = dgvr.Cells["UnitType"].Value.ToString();
                resultsMsg = "";
                
                DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNum, unitType); // All units the same, therefore only need a single stored proc, not one for each unit type.

                if (dtBOM.Rows.Count == 0) // No Existing BOM found.
                {
                    dtBOM = objMain.GetPartListByModelNo(modelNo, unitType);

                    if (dtBOM.Rows.Count > 0)
                    {
                        try
                        {
                            mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNo, out mopStr, out rawMopStr,
                                                                     unitType, jobNum);
                        }
                        catch
                        {
                            resultsMsg += "JobNum - " + jobNum + "ERROR - Calculating the MCA/MOP electrical values!";
                        }

                        if (resultsMsg.Length == 0)
                        {
                            try
                            {
                                dtBOM = objMain.AddRefrigAndBreakerAndWarranty(dtBOM, modelNo, mcaStr, mopStr,
                                                   unitType, out circuit1Charge, out circuit2Charge);

                                objMain.SaveBOM_StatLoc(dtBOM, jobNum, modelNo, unitType);
                            }
                            catch
                            {
                                resultsMsg += "JobNum - " + jobNum + "ERROR - Error occurred in AddRefrigAndBreakerAndWarranty!";
                            }
                        }

                        if (resultsMsg.Length == 0)
                        {
                            resultsMsg += "JobNum - " + jobNum + " - Configured Successfully!";
                        }
                    }
                }
                else
                {
                    resultsMsg += "JobNum - " + jobNum + " - ERROR - Job is already configured!";
                }

                dgvr.Cells["Results"].Value = resultsMsg;
                dgvMainUnitList.Refresh();
            }

            if (this.chkBoxEmailResults.Checked == true)
            {
                //using (StreamWriter sw = new StreamWriter(@"\\EPICOR905APP\Apps\OAU\Rev6_OAU_Config\BatchConfigureJobsResults.csv"))
                using (StreamWriter sw = new StreamWriter(@"C:\Users\tonyt\Documents\BatchConfigureJobsResults.csv"))
                {
                    sw.WriteLine("JobNum," + "ModelNo," + "Result");

                    foreach (DataGridViewRow dgvr in dgvMainUnitList.Rows)
                    {
                        sw.WriteLine(dgvr.Cells["JobNum"].Value.ToString() + "," + dgvr.Cells["ModelNo"].Value.ToString() + "," +
                                     dgvr.Cells["Results"].Value.ToString());
                    }
                }

                // Send email to the scheduling team.
                MailMessage mail = new MailMessage();
                //mail.To.Add("jschroering@KCCMfg.com");
                //mail.To.Add("lwolz@KCCMfg.com");
                //mail.To.Add("kwiseman@KCCMfg.com");
                //mail.To.Add("dhernandez@KCCMfg.com");
                mail.To.Add("tthoman@KCCMfg.com");                

                mail.Subject = "Batch Configure Jobs - " + DateTime.Now.ToShortDateString();

                mail.Body = "See Attached";

                System.Net.Mail.Attachment attachment;

                //attachment = new System.Net.Mail.Attachment(@"\\EPICOR905APP\Apps\OAU\Rev6_OAU_Config\BatchConfigureJobsResults.csv");
                attachment = new System.Net.Mail.Attachment(@"C:\Users\tonyt\Documents\BatchConfigureJobsResults.csv");
                mail.Attachments.Add(attachment);
                mail.IsBodyHtml = false;

                SmtpClient smtp = new SmtpClient();
                //smtp.Host = "smtp.gmail.com";
                //smtp.Port = 587;

                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("username@domain.com", "password");
                //smtp.EnableSsl = true;              

                smtp.Send(mail);
                this.Refresh();
            }
        }        
    }
}
