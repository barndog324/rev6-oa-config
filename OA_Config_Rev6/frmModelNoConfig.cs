﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmModelNoConfig : Form
    {
        MainBO objMain = new MainBO();

        public string HeatType { get; set; }

        public class SubAssembly
        {
            public string ParentPartNum { get; set; }
            public string PartNum { get; set; }
            public decimal RequiredQty { get; set; }
        }       

        public frmModelNoConfig()
        {
            InitializeComponent();            
        }

        #region Events
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (GlobalJob.ProgramMode == "DisplayMode")
            {
                this.Close();
            }
            else
            {
                Environment.Exit(0);
            }            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (lbModelNo.Text.Substring(0, 2).StartsWith("OA") == true)
            {
                printConfigurationReport("OA");
            }
            else
            {
                printConfigurationReport("VKG");
            }
        }

        private void btnCreateBOM_Click(object sender, EventArgs e)
        {
            if (lbModelNo.Text.StartsWith("OA") == true)
            {
                CreateAllPartsBOM_OA("OAU");
            }
            else if (lbModelNo.Text.StartsWith("HA") == true)
            {
                CreateBOM_VKG("VKG");
            }
            else
            {
                CreateBOM();
            }
        }

        private void createBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lbModelNo.Text.StartsWith("OA") == true)
            {
                CreateAllPartsBOM_OA("OAU");
            }
            else if (lbModelNo.Text.StartsWith("HA") == true)
            {
                CreateBOM_VKG("VKG");
            }
            else
            {
                CreateBOM();
            }
        }

        private void btnParseModelNo_Click(object sender, EventArgs e)
        {
            string digitValStr = "";
            string heatingTypeStr = "NA";
            string modelNoStr = "";
            string unitTypeStr = "OAU";

            modelNoStr = txtModelNo.Text;

            if (modelNoStr.Substring(0,2) != "OA")
            {
                unitTypeStr = "Viking";
            }

            //labelConfigModelNo.Text = modelNoStr;    

            lbJobNum.Text = "";
            lbOrderDate.Text = "";
            lbShipDate.Text = "";
            lbBOMCreationDate.Text = "";
            lbLastUpdateDate.Text = "";
            lbProdStartDate.Text = "";
            lbCompleteDate.Text = "";
          
            lbModelNo.Text = modelNoStr;
            lbCustName.Text = "";

            if (modelNoStr.Length == 69)
            {
                // Digit 3 - Cabinet Size
                digitValStr = modelNoStr.Substring(2, 1);
                DataTable dt = objMain.GetModelNumDesc(3, "NA", null, unitTypeStr);
                cbCabinet.DataSource = dt;
                cbCabinet.DisplayMember = "DigitDescription";
                cbCabinet.ValueMember = "DigitVal";
                cbCabinet.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 4 - Major Design
                digitValStr = modelNoStr.Substring(3, 1);
                dt = objMain.GetModelNumDesc(4, heatingTypeStr, null, unitTypeStr);
                cbMajorDesign.DataSource = dt;
                cbMajorDesign.DisplayMember = "DigitDescription";
                cbMajorDesign.ValueMember = "DigitVal";
                cbMajorDesign.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 567 - Cooling Capacity               
                digitValStr = modelNoStr.Substring(4, 3);
                dt = objMain.GetModelNumDesc(567, "NA", null, unitTypeStr);
                cbCoolingCapacity.DataSource = dt;
                cbCoolingCapacity.DisplayMember = "DigitDescription";
                cbCoolingCapacity.ValueMember = "DigitVal";
                cbCoolingCapacity.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 8 - Airflow Configuration               
                digitValStr = modelNoStr.Substring(7, 1);
                dt = objMain.GetModelNumDesc(8, "NA", null, unitTypeStr);
                cbAirflowConfig.DataSource = dt;
                cbAirflowConfig.DisplayMember = "DigitDescription";
                cbAirflowConfig.ValueMember = "DigitVal";
                cbAirflowConfig.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 9 - Voltage
                digitValStr = modelNoStr.Substring(8, 1);
                dt = objMain.GetModelNumDesc(9, "NA", null, unitTypeStr);
                cbVoltage.DataSource = dt;
                cbVoltage.DisplayMember = "DigitDescription";
                cbVoltage.ValueMember = "DigitVal";
                cbVoltage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 11 - Indoor Coil Type
                digitValStr = modelNoStr.Substring(10, 1);
                dt = objMain.GetModelNumDesc(11, "NA", null, unitTypeStr);
                cbIndoorCoilType.DataSource = dt;
                cbIndoorCoilType.DisplayMember = "DigitDescription";
                cbIndoorCoilType.ValueMember = "DigitVal";
                cbIndoorCoilType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 12 - Hot Gas Reheat
                digitValStr = modelNoStr.Substring(11, 1);
                dt = objMain.GetModelNumDesc(12, "NA", null, unitTypeStr);
                cbHotGasReheat.DataSource = dt;
                cbHotGasReheat.DisplayMember = "DigitDescription";
                cbHotGasReheat.ValueMember = "DigitVal";
                cbHotGasReheat.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 13 - Compressor
                digitValStr = modelNoStr.Substring(12, 1);
                dt = objMain.GetModelNumDesc(13, "NA", null, unitTypeStr);
                cbCompressor.DataSource = dt;
                cbCompressor.DisplayMember = "DigitDescription";
                cbCompressor.ValueMember = "DigitVal";
                cbCompressor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 14 - Outdoor Coil Type
                digitValStr = modelNoStr.Substring(13, 1);
                dt = objMain.GetModelNumDesc(14, "NA", null, unitTypeStr);
                cbOutdoorCoilType.DataSource = dt;
                cbOutdoorCoilType.DisplayMember = "DigitDescription";
                cbOutdoorCoilType.ValueMember = "DigitVal";
                cbOutdoorCoilType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 15 - Refridgerant Capacity Control
                digitValStr = modelNoStr.Substring(14, 1);
                dt = objMain.GetModelNumDesc(15, "NA", null, unitTypeStr);
                cbRefridgerantCapacityControl.DataSource = dt;
                cbRefridgerantCapacityControl.DisplayMember = "DigitDescription";
                cbRefridgerantCapacityControl.ValueMember = "DigitVal";
                cbRefridgerantCapacityControl.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 16 - Heat Type Primary
                digitValStr = modelNoStr.Substring(15, 1);
                dt = objMain.GetModelNumDesc(16, "NA", null, unitTypeStr);
                cbHeatTypePrimary.DataSource = dt;
                cbHeatTypePrimary.DisplayMember = "DigitDescription";
                cbHeatTypePrimary.ValueMember = "DigitVal";
                cbHeatTypePrimary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                if (digitValStr == "A" || digitValStr == "B" || digitValStr == "C" || digitValStr == "D" || digitValStr == "E" || digitValStr == "F")
                {
                    HeatType = "IF";
                }
                else if (digitValStr == "G")
                {
                    HeatType = "HOTWATER";
                }
                else if (digitValStr == "H" || digitValStr == "J")
                {
                    HeatType = "ELEC";
                }
                else
                {
                    HeatType = "IF";
                }

                // Digit 17 - Heat Capacity Primary
                digitValStr = modelNoStr.Substring(16, 1);
                dt = objMain.GetModelNumDesc(17, HeatType, null, unitTypeStr);
                cbHeatCapacityPrimary.DataSource = dt;
                cbHeatCapacityPrimary.DisplayMember = "DigitDescription";
                cbHeatCapacityPrimary.ValueMember = "DigitVal";
                cbHeatCapacityPrimary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 18 - Heat Type Secondary
                digitValStr = modelNoStr.Substring(17, 1);
                dt = objMain.GetModelNumDesc(18, "NA", null, unitTypeStr);
                cbHeatTypeSecondary.DataSource = dt;
                cbHeatTypeSecondary.DisplayMember = "DigitDescription";
                cbHeatTypeSecondary.ValueMember = "DigitVal";
                cbHeatTypeSecondary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                if (digitValStr == "1" || digitValStr == "2")
                {
                    HeatType = "DF";
                }
                else if (digitValStr == "3")
                {
                    HeatType = "HOTWATER";
                }
                else if (digitValStr == "4" || digitValStr == "5")
                {
                    HeatType = "ELEC";
                }    
                else
                {
                    HeatType = "NA";
                }

                // Digit 19 - Heat Capacity Secondary
                digitValStr = modelNoStr.Substring(18, 1);

                if (digitValStr != "0")
                {
                    dt = objMain.GetModelNumDesc(19, HeatType, null, unitTypeStr);
                    cbHeatCapacitySecondary.DataSource = dt;
                    cbHeatCapacitySecondary.DisplayMember = "DigitDescription";
                    cbHeatCapacitySecondary.ValueMember = "DigitVal";
                    cbHeatCapacitySecondary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
                }
                else
                {
                    cbHeatCapacitySecondary.Text = "No Secondary Heat";
                }

                // Digit 21 - Supply Fan Motor
                digitValStr = modelNoStr.Substring(20, 1);
                dt = objMain.GetModelNumDesc(21, "NA", null, unitTypeStr);
                cbSupplyFanMotor.DataSource = dt;
                cbSupplyFanMotor.DisplayMember = "DigitDescription";
                cbSupplyFanMotor.ValueMember = "DigitVal";
                cbSupplyFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 22 - Supply Fan Motor Type
                digitValStr = modelNoStr.Substring(21, 1);
                dt = objMain.GetModelNumDesc(22, "NA", null, unitTypeStr);
                cbSupplyFanMotorType.DataSource = dt;
                cbSupplyFanMotorType.DisplayMember = "DigitDescription";
                cbSupplyFanMotorType.ValueMember = "DigitVal";
                cbSupplyFanMotorType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 23 & 24 - Supply Fan Wheel Diameter
                digitValStr = modelNoStr.Substring(22, 2);
                dt = objMain.GetModelNumDesc(2324, "NA", null, unitTypeStr);
                cbSupplyFanWheelDiameter.DataSource = dt;
                cbSupplyFanWheelDiameter.DisplayMember = "DigitDescription";
                cbSupplyFanWheelDiameter.ValueMember = "DigitVal";
                cbSupplyFanWheelDiameter.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 25 - Exhaust Fan Motor
                digitValStr = modelNoStr.Substring(24, 1);
                dt = objMain.GetModelNumDesc(25, "NA", null, unitTypeStr);
                cbExhaustFanMotor.DataSource = dt;
                cbExhaustFanMotor.DisplayMember = "DigitDescription";
                cbExhaustFanMotor.ValueMember = "DigitVal";
                cbExhaustFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 26 - Exhaust Fan Motor Type
                digitValStr = modelNoStr.Substring(25, 1);
                dt = objMain.GetModelNumDesc(26, "NA", null, unitTypeStr);
                cbExhaustFanMotorType.DataSource = dt;
                cbExhaustFanMotorType.DisplayMember = "DigitDescription";
                cbExhaustFanMotorType.ValueMember = "DigitVal";
                cbExhaustFanMotorType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 27 & 28 - Exhaust Fan Wheel Diameter
                digitValStr = modelNoStr.Substring(26, 2);
                dt = objMain.GetModelNumDesc(2728, "NA", null, unitTypeStr);
                cbExhaustFanWheelDiamater.DataSource = dt;
                cbExhaustFanWheelDiamater.DisplayMember = "DigitDescription";
                cbExhaustFanWheelDiamater.ValueMember = "DigitVal";
                cbExhaustFanWheelDiamater.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 29 - Exhaust Fan Piezo Ring
                digitValStr = modelNoStr.Substring(28, 1);
                dt = objMain.GetModelNumDesc(29, "NA", null, unitTypeStr);
                cbFanPiezoRing.DataSource = dt;
                cbFanPiezoRing.DisplayMember = "DigitDescription";
                cbFanPiezoRing.ValueMember = "DigitVal";
                cbFanPiezoRing.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 31 - Unit Controls
                digitValStr = modelNoStr.Substring(30, 1);
                dt = objMain.GetModelNumDesc(31, "NA", null, unitTypeStr);
                cbUnitControls.DataSource = dt;
                cbUnitControls.DisplayMember = "DigitDescription";
                cbUnitControls.ValueMember = "DigitVal";
                cbUnitControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 32 - Build Interface
                digitValStr = modelNoStr.Substring(31, 1);
                dt = objMain.GetModelNumDesc(32, "NA", null, unitTypeStr);
                cbBuildingInterface.DataSource = dt;
                cbBuildingInterface.DisplayMember = "DigitDescription";
                cbBuildingInterface.ValueMember = "DigitVal";
                cbBuildingInterface.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 33 - Filter Options
                digitValStr = modelNoStr.Substring(32, 1);
                dt = objMain.GetModelNumDesc(33, "NA", null, unitTypeStr);
                cbFilterOptions.DataSource = dt;
                cbFilterOptions.DisplayMember = "DigitDescription";
                cbFilterOptions.ValueMember = "DigitVal";
                cbFilterOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 34 - ERV-Composite Construction with Bypass
                digitValStr = modelNoStr.Substring(33, 1);
                dt = objMain.GetModelNumDesc(34, "NA", null, unitTypeStr);
                cbEnergyRecovery.DataSource = dt;
                cbEnergyRecovery.DisplayMember = "DigitDescription";
                cbEnergyRecovery.ValueMember = "DigitVal";
                cbEnergyRecovery.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 35 - Energy Recovery Wheel Options
                digitValStr = modelNoStr.Substring(34, 1);
                dt = objMain.GetModelNumDesc(35, "NA", null, unitTypeStr);
                cbEnergyRecoveryWheelOptions.DataSource = dt;
                cbEnergyRecoveryWheelOptions.DisplayMember = "DigitDescription";
                cbEnergyRecoveryWheelOptions.ValueMember = "DigitVal";
                cbEnergyRecoveryWheelOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 36 - Energy Recovery Wheel Size
                digitValStr = modelNoStr.Substring(35, 1);
                dt = objMain.GetModelNumDesc(36, "NA", null, unitTypeStr);
                cbEnergyWheelRecoverySize.DataSource = dt;
                cbEnergyWheelRecoverySize.DisplayMember = "DigitDescription";
                cbEnergyWheelRecoverySize.ValueMember = "DigitVal";
                cbEnergyWheelRecoverySize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 37 ERV Rotation Sensor
                digitValStr = modelNoStr.Substring(36, 1);
                dt = objMain.GetModelNumDesc(37, "NA", null, unitTypeStr);
                cbERV_RotationSensor.DataSource = dt;
                cbERV_RotationSensor.DisplayMember = "DigitDescription";
                cbERV_RotationSensor.ValueMember = "DigitVal";
                cbERV_RotationSensor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 38 - Damper Options
                digitValStr = modelNoStr.Substring(37, 1);
                dt = objMain.GetModelNumDesc(38, "NA", null, unitTypeStr);
                cbDamperOptions.DataSource = dt;
                cbDamperOptions.DisplayMember = "DigitDescription";
                cbDamperOptions.ValueMember = "DigitVal";
                cbDamperOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 39 - Exhaust Dampers 
                digitValStr = modelNoStr.Substring(38, 1);
                dt = objMain.GetModelNumDesc(39, "NA", null, unitTypeStr);
                cbExhaustDampers.DataSource = dt;
                cbExhaustDampers.DisplayMember = "DigitDescription";
                cbExhaustDampers.ValueMember = "DigitVal";
                cbExhaustDampers.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 41 - Electrical Options
                digitValStr = modelNoStr.Substring(40, 1);
                dt = objMain.GetModelNumDesc(41, "NA", null, unitTypeStr);
                cbElectricalOptions.DataSource = dt;
                cbElectricalOptions.DisplayMember = "DigitDescription";
                cbElectricalOptions.ValueMember = "DigitVal";
                cbElectricalOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 42 - Corrosive Environment Package
                digitValStr = modelNoStr.Substring(41, 1);
                dt = objMain.GetModelNumDesc(42, "NA", null, unitTypeStr);
                cbCorrisiveEnvironmentPackage.DataSource = dt;
                cbCorrisiveEnvironmentPackage.DisplayMember = "DigitDescription";
                cbCorrisiveEnvironmentPackage.ValueMember = "DigitVal";
                cbCorrisiveEnvironmentPackage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 43 - Outdoor Air Monitoring
                digitValStr = modelNoStr.Substring(42, 1);
                dt = objMain.GetModelNumDesc(43, "NA", null, unitTypeStr);
                cbOutdoorAirMonitoring.DataSource = dt;
                cbOutdoorAirMonitoring.DisplayMember = "DigitDescription";
                cbOutdoorAirMonitoring.ValueMember = "DigitVal";
                cbOutdoorAirMonitoring.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 44 - Condenser Fan Options
                digitValStr = modelNoStr.Substring(43, 1);
                dt = objMain.GetModelNumDesc(44, "NA", null, unitTypeStr);
                cbCondenserFanOptions.DataSource = dt;
                cbCondenserFanOptions.DisplayMember = "DigitDescription";
                cbCondenserFanOptions.ValueMember = "DigitVal";
                cbCondenserFanOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 45 - Compressor Sound Blankets 
                digitValStr = modelNoStr.Substring(44, 1);
                dt = objMain.GetModelNumDesc(45, "NA", null, unitTypeStr);
                cbSoundAttenuationPackage.DataSource = dt;
                cbSoundAttenuationPackage.DisplayMember = "DigitDescription";
                cbSoundAttenuationPackage.ValueMember = "DigitVal";
                cbSoundAttenuationPackage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 46 - Smoke Detector 
                digitValStr = modelNoStr.Substring(45, 1);
                dt = objMain.GetModelNumDesc(46, "NA", null, unitTypeStr);
                cbSmokeDetector.DataSource = dt;
                cbSmokeDetector.DisplayMember = "DigitDescription";
                cbSmokeDetector.ValueMember = "DigitVal";
                cbSmokeDetector.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 47 - Hailguards
                digitValStr = modelNoStr.Substring(46, 1);
                dt = objMain.GetModelNumDesc(47, "NA", null, unitTypeStr);
                cbHailguards.DataSource = dt;
                cbHailguards.DisplayMember = "DigitDescription";
                cbHailguards.ValueMember = "DigitVal";
                cbHailguards.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 48 - Service Lights
                digitValStr = modelNoStr.Substring(47, 1);
                dt = objMain.GetModelNumDesc(48, "NA", null, unitTypeStr);
                cbServiceLights.DataSource = dt;
                cbServiceLights.DisplayMember = "DigitDescription";
                cbServiceLights.ValueMember = "DigitVal";
                cbServiceLights.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 49 - UV Lights 
                digitValStr = modelNoStr.Substring(48, 1);
                dt = objMain.GetModelNumDesc(49, "NA", null, unitTypeStr);
                cbUV_Lights.DataSource = dt;
                cbUV_Lights.DisplayMember = "DigitDescription";
                cbUV_Lights.ValueMember = "DigitVal";
                cbUV_Lights.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 51 - Installation
                digitValStr = modelNoStr.Substring(50, 1);
                dt = objMain.GetModelNumDesc(51, "NA", null, unitTypeStr);
                cbInstallation.DataSource = dt;
                cbInstallation.DisplayMember = "DigitDescription";
                cbInstallation.ValueMember = "DigitVal";
                cbInstallation.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 52 - Convenience Outlet 
                digitValStr = modelNoStr.Substring(51, 1);
                dt = objMain.GetModelNumDesc(52, "NA", null, unitTypeStr);
                cbConvenienceOutlet.DataSource = dt;
                cbConvenienceOutlet.DisplayMember = "DigitDescription";
                cbConvenienceOutlet.ValueMember = "DigitVal";
                cbConvenienceOutlet.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 53 - Controls Display 
                digitValStr = modelNoStr.Substring(52, 1);
                dt = objMain.GetModelNumDesc(53, "NA", null, unitTypeStr);
                cbControlsDisplay.DataSource = dt;
                cbControlsDisplay.DisplayMember = "DigitDescription";
                cbControlsDisplay.ValueMember = "DigitVal";
                cbControlsDisplay.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 54 - Reliatel 
                digitValStr = modelNoStr.Substring(53, 1);
                dt = objMain.GetModelNumDesc(54, "NA", null, unitTypeStr);
                cbCoolingControls.DataSource = dt;
                cbCoolingControls.DisplayMember = "DigitDescription";
                cbCoolingControls.ValueMember = "DigitVal";
                cbCoolingControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 55 - Face and Bypass Evap 
                digitValStr = modelNoStr.Substring(54, 1);
                dt = objMain.GetModelNumDesc(55, "NA", null, unitTypeStr);
                cbFaceAndBypassEvap.DataSource = dt;
                cbFaceAndBypassEvap.DisplayMember = "DigitDescription";
                cbFaceAndBypassEvap.ValueMember = "DigitVal";
                cbFaceAndBypassEvap.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 56 - Thermostat 
                digitValStr = modelNoStr.Substring(55, 1);
                dt = objMain.GetModelNumDesc(56, "NA", null, unitTypeStr);
                cbThermostat.DataSource = dt;
                cbThermostat.DisplayMember = "DigitDescription";
                cbThermostat.ValueMember = "DigitVal";
                cbThermostat.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 57 - Altitude 
                digitValStr = modelNoStr.Substring(56, 1);
                dt = objMain.GetModelNumDesc(57, "NA", null, unitTypeStr);
                cbAltitude.DataSource = dt;
                cbAltitude.DisplayMember = "DigitDescription";
                cbAltitude.ValueMember = "DigitVal";
                cbAltitude.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 58 - Condensate Overflow Switch 
                digitValStr = modelNoStr.Substring(57, 1);
                dt = objMain.GetModelNumDesc(58, "NA", null, unitTypeStr);
                cbCondensateOverflowSwitch.DataSource = dt;
                cbCondensateOverflowSwitch.DisplayMember = "DigitDescription";
                cbCondensateOverflowSwitch.ValueMember = "DigitVal";
                cbCondensateOverflowSwitch.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 59 - Freezstat 
                digitValStr = modelNoStr.Substring(58, 1);
                dt = objMain.GetModelNumDesc(59, "NA", null, unitTypeStr);
                cbFreezstat.DataSource = dt;
                cbFreezstat.DisplayMember = "DigitDescription";
                cbFreezstat.ValueMember = "DigitVal";
                cbFreezstat.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 61 - Chilled Water Type
                digitValStr = modelNoStr.Substring(60, 1);
                dt = objMain.GetModelNumDesc(61, "NA", null, unitTypeStr);
                cbOutdoorCoilFluidType.DataSource = dt;
                cbOutdoorCoilFluidType.DisplayMember = "DigitDescription";
                cbOutdoorCoilFluidType.ValueMember = "DigitVal";
                cbOutdoorCoilFluidType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 62 - Minumum Damper Leakage
                digitValStr = modelNoStr.Substring(61, 1);
                dt = objMain.GetModelNumDesc(62, "NA", null, unitTypeStr);
                cbMinimumDamperLeakage.DataSource = dt;
                cbMinimumDamperLeakage.DisplayMember = "DigitDescription";
                cbMinimumDamperLeakage.ValueMember = "DigitVal";
                cbMinimumDamperLeakage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);                      
            }
            else
            {
                modelNoStr = "OA" + cbCabinet.SelectedValue.ToString();
                modelNoStr += cbMajorDesign.SelectedValue.ToString();
                modelNoStr += cbCoolingCapacity.SelectedValue.ToString();
                modelNoStr += cbAirflowConfig.SelectedValue.ToString();
                modelNoStr += cbVoltage.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbIndoorCoilType.SelectedValue.ToString();
                modelNoStr += cbHotGasReheat.SelectedValue.ToString();
                modelNoStr += cbCompressor.SelectedValue.ToString();
                modelNoStr += cbOutdoorCoilType.SelectedValue.ToString();
                modelNoStr += cbRefridgerantCapacityControl.SelectedValue.ToString();
                modelNoStr += cbHeatTypePrimary.SelectedValue.ToString();
                if (cbHeatCapacityPrimary.Text != "")
                {
                    modelNoStr += cbHeatCapacityPrimary.SelectedValue.ToString();
                }
                else
                {
                    modelNoStr += "0";
                }

                modelNoStr += cbHeatTypeSecondary.SelectedValue.ToString();
                if (cbHeatCapacitySecondary.Text != "")
                {
                    modelNoStr += cbHeatCapacitySecondary.SelectedValue.ToString();
                }
                else
                {
                    modelNoStr += "0";
                }
                modelNoStr += "-";
                modelNoStr += cbSupplyFanMotor.SelectedValue.ToString();
                modelNoStr += cbSupplyFanMotorType.SelectedValue.ToString();
                modelNoStr += cbSupplyFanWheelDiameter.SelectedValue.ToString();
                modelNoStr += cbExhaustFanMotor.SelectedValue.ToString();
                modelNoStr += cbExhaustFanMotorType.SelectedValue.ToString();
                modelNoStr += cbExhaustFanWheelDiamater.SelectedValue.ToString();
                modelNoStr += cbFanPiezoRing.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbUnitControls.SelectedValue.ToString();
                modelNoStr += cbBuildingInterface.SelectedValue.ToString();
                modelNoStr += cbFilterOptions.SelectedValue.ToString();
                modelNoStr += cbEnergyRecovery.SelectedValue.ToString();
                modelNoStr += cbEnergyRecoveryWheelOptions.SelectedValue.ToString();
                modelNoStr += cbEnergyWheelRecoverySize.SelectedValue.ToString();
                modelNoStr += cbERV_RotationSensor.SelectedValue.ToString();
                modelNoStr += cbDamperOptions.SelectedValue.ToString();
                modelNoStr += cbExhaustDampers.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbElectricalOptions.SelectedValue.ToString();
                modelNoStr += cbCorrisiveEnvironmentPackage.SelectedValue.ToString();
                modelNoStr += cbOutdoorAirMonitoring.SelectedValue.ToString();
                modelNoStr += cbCondenserFanOptions.SelectedValue.ToString();
                modelNoStr += cbSoundAttenuationPackage.SelectedValue.ToString();
                modelNoStr += cbSmokeDetector.SelectedValue.ToString();
                modelNoStr += cbHailguards.SelectedValue.ToString();
                modelNoStr += cbServiceLights.SelectedValue.ToString();
                modelNoStr += cbUV_Lights.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbInstallation.SelectedValue.ToString();
                modelNoStr += cbConvenienceOutlet.SelectedValue.ToString();
                modelNoStr += cbControlsDisplay.SelectedValue.ToString();
                modelNoStr += cbCoolingControls.SelectedValue.ToString();
                modelNoStr += cbFaceAndBypassEvap.SelectedValue.ToString();
                modelNoStr += cbThermostat.SelectedValue.ToString();
                modelNoStr += cbAltitude.SelectedValue.ToString();
                modelNoStr += cbCondensateOverflowSwitch.SelectedValue.ToString();
                modelNoStr += cbFreezstat.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbOutdoorCoilFluidType.SelectedValue.ToString();
                modelNoStr += "0";
                modelNoStr += "0";
                modelNoStr += "0";
                modelNoStr += "0";
                modelNoStr += "0";
                modelNoStr += "0";
                modelNoStr += "0";
                modelNoStr += "0";

                txtModelNo.Text = modelNoStr;
                lbModelNo.Text = modelNoStr;

            }           
        }       

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lbModelNo.Text.Substring(0, 2).StartsWith("OA") == true)
            {
                printConfigurationReport("OA");
            }
            else
            {
                printConfigurationReport("VKG");
            };
        }

        private void deleteBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            string jobNumStr = lbJobNum.Text;
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string modelNoStr = lbModelNo.Text;

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            if (modelNoStr.StartsWith("OA") == true || modelNoStr.StartsWith("HA") == true)
            {
                deleteAllPartsOnTheBOM();
            }
            else
            {
                //DataTable dt = objMain.GetOAU_BOMsData(jobNumStr);
                //DataTable dtJobMtl = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "Rev6");

                //if (dtJobMtl.Rows.Count > 0)
                //{
                DialogResult result1 = MessageBox.Show("Are you sure you want to Delete the BOM for Job# " + jobNumStr +
                                                       " Press 'Yes' to delete & 'No' to cancel!",
                                                       "WARNING WARNING WARNING",
                                                       MessageBoxButtons.YesNo,
                                                       MessageBoxIcon.Exclamation);
                if (result1 == DialogResult.Yes)
                {
                    objMain.deleteJobFromDatabase(jobNumStr, modByStr, "OAU");

                    this.lbBOMCreationDate.Text = "";
                    MessageBox.Show("You have successfully deleted the BOM for Job# " + jobNumStr + " from the database");
                }
                //}
            }
        }              

        #endregion
        
        #region Other Methods
        private void deleteAllPartsOnTheBOM()
        {
            string errorMsg = "";
            string jobNumStr = lbJobNum.Text;
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string epicorServer = "";
            string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;

            if (GlobalJob.DisplayMode == "Production")
            {
                epicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            }
            else if (GlobalJob.DisplayMode == "Debug")
            {
                epicorServer = ConfigurationManager.AppSettings["DevEpicorAppServer"];
            }
            else
            {
                epicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            }

            Console.WriteLine(epicorServer);

            DeleteBOM db = new DeleteBOM(epicorServer, epicorSqlConnectionString);

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "REV6"); // All units the same, therefore only need a single stored proc, not one for each unit type.            

            if (dtBOM.Rows.Count > 0)
            {
                DialogResult result1 = MessageBox.Show("Are you sure you want to Delete the BOM for Job# " + jobNumStr +
                                                       " Press 'Yes' to delete & 'No' to cancel!",
                                                       "WARNING WARNING WARNING",
                                                       MessageBoxButtons.YesNo,
                                                       MessageBoxIcon.Exclamation);
                if (result1 == DialogResult.Yes)
                {                                       
                    try
                    {
                        objMain.CopyJobMtlToHistory(jobNumStr, modByStr);
                    }
                    catch (Exception f)
                    {
                        MessageBox.Show("Error copying job# " + jobNumStr + " from jobmtl Table to JobMtlHistory Table - " + f);
                    }

                    this.lbBOMCreationDate.Text = "";

                    try
                    {
                        errorMsg = db.DeleteBOM_FromEpicor(jobNumStr);

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                objMain.DeleteOAU_BOMsData(jobNumStr, modByStr);
                            }
                            catch (Exception f)
                            {
                                MessageBox.Show("Error deleting job# " + jobNumStr + " from OAU_BOMs Table - " + f);
                            }

                            try
                            {
                                objMain.DeleteEtlOAUData(jobNumStr);
                            }
                            catch (Exception f)
                            {
                                MessageBox.Show("Error deleting job# " + jobNumStr + " from R6_OA_EtlOAU Table - " + f);
                            }

                            MessageBox.Show("You have successfully deleted the BOM for Job# " + jobNumStr + " from the database");
                        }
                        else
                        {
                            MessageBox.Show(errorMsg);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR: Something went awry while deleting Job# " + jobNumStr + " from the database");
                    }
                }
            }
            else
            {
                MessageBox.Show("ERROR: BOM does Not existfor this JobNum " + jobNumStr);
            }
        }

        private void CreateBOM()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "";
            string circuit2Charge = "";
            string partNumStr = "";
            string revisionNumStr = "";
            string unitTypeStr = "";

            frmDisplayBOM frmBOM = new frmDisplayBOM();

            frmBOM.lbVoltage.Text = cbVoltage.Text;

            if (lbUnitType.Text == "Viking")
            {
                unitTypeStr = "VKG";
            }
            else
            {
                unitTypeStr = "REV6";
            }

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "REV6"); // All units the same, therefore only need a single stored proc, not one for each unit type.
            if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            {
                dtBOM = objMain.GetPartListByModelNo(modelNoStr, unitTypeStr);

                frmBOM.btnAddPart.Enabled = false;
                string parentPartNum = "";
                string curParentPartNum = "";
                string curAsmSeq = "";
                string asmQty = "0";
                string partQty = "";
                int seqNo = 0;
                int asmSeq = 1;                
                int asmQtyInt = 0;
                decimal partQtyDec = 0;

                var parts = new List<SubAssembly>();

                foreach(DataRow dr in dtBOM.Rows)
                {
                    parentPartNum = dr["ParentPartNum"].ToString();
                    partNumStr = dr["PartNum"].ToString();
                    revisionNumStr = dr["RevisionNum"].ToString();

                    if (parentPartNum.Contains("RFGASM"))                    
                    {
                        if (parentPartNum != curParentPartNum)
                        {
                            curParentPartNum = parentPartNum;
                            asmQty = parentPartNum.Substring((parentPartNum.Length - 2), 1);
                            asmQtyInt = int.Parse(asmQty);
                            curAsmSeq = dr["AssemblySeq"].ToString();
                            seqNo = 10;
                            ++asmSeq;
                        }

                        partQty = dr["ReqQty"].ToString();
                        partQtyDec = decimal.Parse(partQty);

                        dr["ReqQty"] = (partQtyDec * asmQtyInt).ToString();

                        if (asmSeq > 2)
                        {
                            dr["AssemblySeq"] = asmSeq.ToString();
                            dr["SeqNo"] = seqNo;
                            seqNo += 10;
                        }
                    }
                    else if (parentPartNum.Length > 0 && !parentPartNum.StartsWith("OACON"))
                    {
                        if (parentPartNum != curParentPartNum)
                        {
                            curParentPartNum = parentPartNum;                            
                            asmQtyInt = 1;
                            curAsmSeq = dr["AssemblySeq"].ToString();
                            seqNo = 10;
                            ++asmSeq;
                        }

                        partQty = dr["ReqQty"].ToString();
                        partQtyDec = decimal.Parse(partQty);

                        dr["ReqQty"] = (partQtyDec * asmQtyInt).ToString();
                        dr["AssemblySeq"] = asmSeq.ToString();
                        dr["SeqNo"] = seqNo;
                        seqNo += 10;                        
                    }                   
                }
                if (dtBOM.Rows.Count > 0)
                {                   
                    //dtBOM = objMain.AddInnerSubAssemblyParts(dtBOM); //01/27/2020
                    dtBOM = addPanelAssemblyBOM(dtBOM);  
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6", jobNumStr);
                    dtBOM = addRefrigAndBreakerAndWarranty(dtBOM, mcaStr, mopStr, "REV6", out circuit1Charge, out circuit2Charge);
                }
            }
            else
            {
                //mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6");
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = true;
                DataTable dtETL = objMain.GetEtlData(jobNumStr);
                if (dtETL.Rows.Count > 0)
                {
                    DataRow drETL = dtETL.Rows[0];
                    circuit1Charge = drETL["Circuit1Charge"].ToString();
                    circuit2Charge = drETL["Circuit2Charge"].ToString();
                    mcaStr = drETL["MinCKTAmp"].ToString();
                    mopStr = drETL["MFSMCB"].ToString();
                }
                else
                {
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6", jobNumStr);
                }
            }

            if (jobNumStr == "")
            {
                frmBOM.btnSave.Enabled = false;
            }

            //if (dtBOM.Rows.Count > 0)
            //{
            //    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6");               
            //}

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();
            frmBOM.dgvBOM.DataSource = dtBOM;
            frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            frmBOM.dgvBOM.Columns["Description"].Width = 300;
            frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            frmBOM.dgvBOM.Columns["Status"].Width = 75;
            frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
            frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            //frmBOM.dgvBOM.Columns["QtyPer"].Visible = false;   
            frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            frmBOM.dgvBOM.Columns["CostMethod"].Visible = false;
            frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;
            //frmBOM.dgvBOM.Columns["SubAsmMtlPart"].Visible = false; //01/27/2020
            frmBOM.dgvBOM.Columns["RuleHeadID"].Visible = false;  
            frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;
            frmBOM.dgvBOM.Columns["Generation"].Visible = false;

            frmBOM.Text = "Outdoor Air Bill of Materials";
            frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            frmBOM.lbShipDate.Text = lbShipDate.Text;
            frmBOM.lbJobNum.Text = jobNumStr;
            frmBOM.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmBOM.BackColor = GlobalJob.EnvColor;
            frmBOM.lbCustName.Text = lbCustName.Text;
            frmBOM.lbModelNo.Text = modelNoStr;
            frmBOM.lbMCA.Text = mcaStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbUnitType.Text = lbUnitType.Text;
            frmBOM.lbCir1Chrg.Text = circuit1Charge + " lbs";
            frmBOM.lbCir2Chrg.Text = circuit2Charge + " lbs";

            frmBOM.ShowDialog();
        }

        private void CreateAllPartsBOM_OA(string unitTypeStr)
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "";
            string circuit2Charge = "";
            string partNumStr = "";
            string revisionNumStr = "";            

            frmDisplayBOM frmBOM = new frmDisplayBOM();

            frmBOM.lbVoltage.Text = cbVoltage.Text;                      

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "REV6"); // All units the same, therefore only need a single stored proc, not one for each unit type.
            if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            {
                dtBOM = objMain.GetPartListByModelNo(modelNoStr, "REV6");

                frmBOM.btnAddPart.Enabled = false;
                string parentPartNum = "";
                string curParentPartNum = "";
                string curAsmSeq = "";
                string asmQty = "0";
                string partQty = "";

                int seqNo = 0;
                int asmSeq = 0;
                int curAsmSeqInt = 0;
                int curGenInt = 0;
                int asmQtyInt = 0;
                
                decimal partQtyDec = 0;                                            

                if (dtBOM.Rows.Count > 0)
                {
                    //dtBOM = objMain.ProcessSubAssemblies(dtBOM);
                    ////dtBOM = objMain.AddInnerSubAssemblyParts(dtBOM, curAsmSeqInt, out curAsmSeqInt);
                    ////dtBOM = objMain.AddPanelAssemblyBOM(dtBOM, curAsmSeqInt);
                    ////dtBOM = objMain.SortBOM_SetupSubAssemblys(dtBOM); 
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6", jobNumStr);
                    dtBOM = addRefrigAndBreakerAndWarranty(dtBOM, mcaStr, mopStr, "REV6", out circuit1Charge, out circuit2Charge);                    
                }
            }
            else
            {
                mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6", jobNumStr);
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = true;
                frmBOM.btnSave.Enabled = false;
                DataTable dtETL = objMain.GetEtlData(jobNumStr);
                if (dtETL.Rows.Count > 0)
                {
                    DataRow drETL = dtETL.Rows[0];
                    circuit1Charge = drETL["Circuit1Charge"].ToString();
                    circuit2Charge = drETL["Circuit2Charge"].ToString();
                    mcaStr = drETL["MinCKTAmp"].ToString();
                    mopStr = drETL["MFSMCB"].ToString();
                }
                else
                {
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6", jobNumStr);
                }
            }

            if (jobNumStr == "")
            {
                frmBOM.btnSave.Enabled = false;
            }

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();
            frmBOM.dgvBOM.DataSource = dtBOM;
            frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            frmBOM.dgvBOM.Columns["Description"].Width = 300;
            frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            frmBOM.dgvBOM.Columns["Status"].Width = 75;
            frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
            frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;                                  
            frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;            
            frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            frmBOM.dgvBOM.Columns["BuyToOrder"].Visible = false;
            frmBOM.dgvBOM.Columns["NonStock"].Visible = false;
            frmBOM.dgvBOM.Columns["SubAssemblyPart"].Visible = false;     
            frmBOM.dgvBOM.Columns["SubAsmMtlPart"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;           
            frmBOM.dgvBOM.Columns["StationLoc"].Visible = false;           
            frmBOM.dgvBOM.Columns["Runout"].Visible = false;                        
            frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;              
   
            frmBOM.Text = "Outdoor Air Bill of Materials";
            frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            frmBOM.lbShipDate.Text = lbShipDate.Text;
            frmBOM.lbJobNum.Text = jobNumStr;
            frmBOM.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmBOM.BackColor = GlobalJob.EnvColor;
            frmBOM.lbCustName.Text = lbCustName.Text;
            frmBOM.lbModelNo.Text = modelNoStr;
            frmBOM.lbMCA.Text = mcaStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbUnitType.Text = lbUnitType.Text;
            frmBOM.lbCir1Chrg.Text = circuit1Charge + " lbs";
            frmBOM.lbCir2Chrg.Text = circuit2Charge + " lbs";

            frmBOM.ShowDialog();
        }

        private void CreateBOM_VKG(string unitTypeStr)
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "";
            string circuit2Charge = "";
            string partNumStr = "";
            string revisionNumStr = "";

            frmDisplayBOM frmBOM = new frmDisplayBOM();
           
            frmBOM.lbVoltage.Text = cbVoltage.Text;

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "VKG"); // All units the same, therefore only need a single stored proc, not one for each unit type.
            if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            {
                dtBOM = objMain.GetPartListByModelNo(modelNoStr, unitTypeStr);

                frmBOM.btnAddPart.Enabled = false;
                string parentPartNum = "";
                string curParentPartNum = "";
                string curAsmSeq = "";
                string asmQty = "0";
                string partQty = "";

                int seqNo = 0;
                int asmSeq = 0;
                int curAsmSeqInt = 0;
                int curGenInt = 0;
                int asmQtyInt = 0;

                decimal partQtyDec = 0;

                if (dtBOM.Rows.Count > 0)
                {
                    //dtBOM = objMain.ProcessSubAssemblies(dtBOM);
                    //dtBOM = objMain.AddInnerSubAssemblyParts(dtBOM, curAsmSeqInt, out curAsmSeqInt);
                    //dtBOM = objMain.AddPanelAssemblyBOM(dtBOM, curAsmSeqInt);
                    //dtBOM = objMain.SortBOM_SetupSubAssemblys(dtBOM);
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "VKG", jobNumStr);
                    dtBOM = addRefrigAndBreakerAndWarranty(dtBOM, mcaStr, mopStr, "VKG", out circuit1Charge, out circuit2Charge);
                }
            }
            else
            {
                mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "VKG", jobNumStr);
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = true;
                frmBOM.btnSave.Enabled = false;
                DataTable dtETL = objMain.GetEtlData(jobNumStr);
                if (dtETL.Rows.Count > 0)
                {
                    DataRow drETL = dtETL.Rows[0];
                    circuit1Charge = drETL["Circuit1Charge"].ToString();
                    circuit2Charge = drETL["Circuit2Charge"].ToString();
                    mcaStr = drETL["MinCKTAmp"].ToString();
                    mopStr = drETL["MFSMCB"].ToString();
                }
                else
                {
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "VKG", jobNumStr);
                }
            }

            if (jobNumStr == "")
            {
                frmBOM.btnSave.Enabled = false;
            }

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();
            frmBOM.dgvBOM.DataSource = dtBOM;
            frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            frmBOM.dgvBOM.Columns["Description"].Width = 300;
            frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            frmBOM.dgvBOM.Columns["Status"].Width = 75;
            frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
            frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            frmBOM.dgvBOM.Columns["BuyToOrder"].Visible = false;
            frmBOM.dgvBOM.Columns["NonStock"].Visible = false;
            frmBOM.dgvBOM.Columns["SubAssemblyPart"].Visible = false;
            frmBOM.dgvBOM.Columns["SubAsmMtlPart"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;
            frmBOM.dgvBOM.Columns["StationLoc"].Visible = false;
            frmBOM.dgvBOM.Columns["Runout"].Visible = false;
            frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;           

            if (lbModelType.Text.StartsWith("Mixed"))
            {
                frmBOM.Text = lbModelType.Text;
            }
            else
            {
                frmBOM.Text = "Outdoor Air Bill of Materials";
            }
            frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            frmBOM.lbShipDate.Text = lbShipDate.Text;
            frmBOM.lbJobNum.Text = jobNumStr;
            frmBOM.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmBOM.BackColor = GlobalJob.EnvColor;
            frmBOM.lbCustName.Text = lbCustName.Text;
            frmBOM.lbModelNo.Text = modelNoStr;
            frmBOM.lbMCA.Text = mcaStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbUnitType.Text = lbUnitType.Text;
            frmBOM.lbCir1Chrg.Text = circuit1Charge + " lbs";
            frmBOM.lbCir2Chrg.Text = circuit2Charge + " lbs";

            frmBOM.ShowDialog();
        }        

        private DataTable sortBOM_SetupSubAssemblys(DataTable dtBOM)
        {
            string parentPartNum = "";
            string tempParent = "";
            string tempGen = "";
            string curParentPartNum = "nothing";
            string subAsmPart = "";
            string asmSeqStr = "";

            int asmSeqInt = -1;
           
            int seqNoInt = 0;
            int eqPos = -1;
            int parLen = 0;            
            int generationInt = -1;

            DataView dv = dtBOM.DefaultView;
            dv.Sort = "ParentPartNum";
            dtBOM = dv.ToTable();

            foreach (DataRow dr in dtBOM.Rows)   // locate all of the parent part numbers in the BOM and record them to a list for future use.
            {
                parentPartNum = dr["ParentPartNum"].ToString();
                subAsmPart = dr["SubAssemblyPart"].ToString();
                asmSeqStr = dr["AssemblySeq"].ToString();

                eqPos = parentPartNum.IndexOf("=");
                if (eqPos > 0)
                {
                    parLen = parentPartNum.Length;
                    tempParent = parentPartNum.Substring(0, (eqPos + 2));
                    tempParent = tempParent + parentPartNum.Substring((eqPos + 3), (parLen - (eqPos + 3)));                   
                }
                else
                {
                    tempParent = parentPartNum;
                }

                if (parentPartNum != curParentPartNum)
                {                 
                    curParentPartNum = parentPartNum;
                    seqNoInt = 0;
                    ++asmSeqInt;                        
                }

                if (asmSeqInt == 0)
                {
                    if (dr["PartType"].ToString().StartsWith("VMEASM"))
                    {
                        dr["ParentPartNum"] = dr["PartType"];
                    }
                }

                dr["AssemblySeq"] = asmSeqInt;               
                seqNoInt += 10;
                dr["SeqNo"] = seqNoInt;
                //dr["ParentPartNum"] = tempParent;
            }            

            return dtBOM;
        }

        //private DataTable addInnerSubAssemblyParts(DataTable dtBOM)
        //{
        //    string generationStr = "";
        //    string partNumStr = "";
        //    string parentPartNumStr = "";
        //    string revisionNumStr = "";
        //    string mtlRevNumStr = "";
        //    string ruleHeadIdStr = "";
        //    string ruleBatchIdStr = "";
        //    string mtlPartNumStr = "";
        //    string asmUOMStr = "";
        //    string asmQtyStr = "";
        //    string partQtyStr = "";

        //    List<string> PartNumList = new List<string>();
        //    List<string> RevNumList = new List<string>();
        //    List<string> RuleHeadList = new List<string>();
        //    List<string> RuleBatchList = new List<string>();
        //    List<DataRow> RowsToRemove = new List<DataRow>();

        //    int generationInt = 0;
        //    int curGenerationInt = 0;
        //    int seqNumInt = 10;
        //    int asmSeqInt = 5;

        //    bool firstAsm = true;

        //    decimal partQtyDec = 0;
        //    decimal asmQtyDec = 0;

        //    var results = dtBOM.AsEnumerable().Where(dr => dr.Field<Boolean>("SubAssemblyPart") == true && dr.Field<Boolean>("SubAsmMtlPart") == true && dr.Field<String>("PartType").StartsWith("VMEASM"));

        //    foreach (DataRow row in results)
        //    {
        //        RowsToRemove.Add(row);
        //    }

        //    foreach (DataRow drtr in RowsToRemove)
        //    {
        //        partNumStr = drtr["PartNum"].ToString();
        //        parentPartNumStr = drtr["ParentPartNum"].ToString();
        //        revisionNumStr = drtr["RevisionNum"].ToString();
        //        ruleHeadIdStr = drtr["RuleHeadID"].ToString();
        //        ruleBatchIdStr = drtr["RuleBatchID"].ToString();

        //        DataTable dtPartBOM = objMain.GetPartMtlBOM(partNumStr, revisionNumStr, parentPartNumStr);

        //        if (dtPartBOM.Rows.Count > 0)
        //        {
        //            asmSeqInt = getAsmSeq(dtBOM, asmSeqInt);

        //            foreach (DataRow drPrt in dtPartBOM.Rows)
        //            {
        //                if (drPrt["RevisionNum"] == null)
        //                {
        //                    mtlRevNumStr = "";
        //                }
        //                else
        //                {
        //                    mtlRevNumStr = drPrt["RevisionNum"].ToString();
        //                }

        //                mtlPartNumStr = drPrt["MtlPartNum"].ToString();
        //                generationStr = drPrt["Generation"].ToString();
        //                generationInt = Int32.Parse(generationStr);

        //                if (drPrt["QtyPer"] == null)
        //                {
        //                    partQtyDec = 0;
        //                }
        //                else
        //                {
        //                    partQtyStr = drPrt["QtyPer"].ToString();
        //                    partQtyDec = decimal.Parse(partQtyStr);
        //                }

        //                if (curGenerationInt != generationInt)
        //                {
        //                    curGenerationInt = generationInt;

        //                    seqNumInt = 10;
        //                    if (firstAsm)
        //                    {
        //                        firstAsm = false;
        //                        asmQtyDec = 1;
        //                    }
        //                    else
        //                    {
        //                        DataTable dtAsm = objMain.GetPanelAsmBOM(partNumStr, revisionNumStr, 0);
        //                        foreach (DataRow row in dtAsm.Rows)
        //                        {
        //                            if (Int32.Parse(row["Generation"].ToString()) == (curGenerationInt - 1))
        //                            {
        //                                asmQtyStr = row["QtyPer"].ToString();
        //                                asmQtyDec = decimal.Parse(asmQtyStr);
        //                                asmUOMStr = row["UOM"].ToString();
        //                                break;
        //                            }
        //                        }
        //                        ++asmSeqInt;
        //                    }
        //                }

        //                var dr = dtBOM.NewRow();
        //                dr["PartCategory"] = "Panel Assembly";
        //                dr["AssemblySeq"] = asmSeqInt;
        //                dr["SeqNo"] = seqNumInt;
        //                seqNumInt += 10;
        //                dr["PartNum"] = drPrt["MtlPartNum"].ToString();
        //                dr["Description"] = drPrt["PartDescription"].ToString();
        //                dr["QtyPer"] = partQtyDec;                      
        //                partQtyDec = partQtyDec * asmQtyDec;
        //                dr["ReqQty"] = partQtyDec;
        //                dr["UOM"] = drPrt["UOM"].ToString();
        //                dr["UnitCost"] = decimal.Parse(drPrt["UnitCost"].ToString());
        //                dr["WhseCode"] = "LWH5";
        //                dr["RelOp"] = 10;
        //                dr["PartStatus"] = "Active";
        //                dr["RevisionNum"] = mtlRevNumStr;
        //                dr["CostMethod"] = "A";
        //                dr["PartType"] = "";
        //                dr["Status"] = "BackFlush";
        //                dr["PROGRESS_RECID"] = 0;
        //                dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
        //                dr["RuleBatchID"] = Int32.Parse(ruleBatchIdStr);
        //                dr["ParentPartNum"] = drPrt["ParentPartNum"].ToString();
        //                dr["Generation"] = drPrt["Generation"].ToString();

        //                dtBOM.Rows.Add(dr);
        //            }
        //        }
        //    }

        //    foreach (DataRow row in RowsToRemove)
        //    {
        //        dtBOM.Rows.Remove(row);
        //    }

        //    return dtBOM;
        //}

        private DataTable addPanelAssemblyBOM(DataTable dtBOM)
        {
            string generationStr = "";
            string partNumStr = "";
            string revisionNumStr = "";
            string mtlRevNumStr = "";
            string ruleHeadIdStr = "";
            string ruleBatchIdStr = "";
            string mtlPartNumStr = "";
            string asmUOMStr = "";
            string asmQtyStr = "";
            string partQtyStr = "";

            List<string> PartNumList = new List<string>();
            List<string> RevNumList = new List<string>();
            List<string> RuleHeadList = new List<string>();
            List<string> RuleBatchList = new List<string>();
            List<DataRow> RowsToRemove = new List<DataRow>();

            int generationInt = 0;
            int curGenerationInt = 0;
            int seqNumInt = 10;
            int asmSeqInt = 5;

            bool firstAsm = true;

            decimal partQtyDec = 0;
            decimal asmQtyDec = 0;

            var results = dtBOM.AsEnumerable().Where(dr => dr.Field<string>("PartNum").Contains("PNLASM") == true);

            foreach (DataRow row in results)
            {
                RowsToRemove.Add(row);
            }

            foreach (DataRow drtr in RowsToRemove)
            {
                partNumStr = drtr["PartNum"].ToString();
                revisionNumStr = drtr["RevisionNum"].ToString();
                ruleHeadIdStr = drtr["RuleHeadID"].ToString();
                ruleBatchIdStr = drtr["RuleBatchID"].ToString();

                DataTable dtPnl = objMain.GetPanelAsmBOM(partNumStr, revisionNumStr, 1);

                if (dtPnl.Rows.Count > 0)
                {
                    asmSeqInt = getAsmSeq(dtBOM, asmSeqInt);

                    foreach (DataRow drPnl in dtPnl.Rows)
                    {
                        if (drPnl["RevisionNum"] == null)
                        {
                            mtlRevNumStr = "";
                        }
                        else
                        {
                            mtlRevNumStr = drPnl["RevisionNum"].ToString();
                        }

                        mtlPartNumStr = drPnl["MtlPartNum"].ToString();
                        generationStr = drPnl["Generation"].ToString();
                        generationInt = Int32.Parse(generationStr);

                        if (drPnl["QtyPer"] == null)
                        {
                            partQtyDec = 0;
                        }
                        else
                        {
                            partQtyStr = drPnl["QtyPer"].ToString();
                            partQtyDec = decimal.Parse(partQtyStr);
                        }

                        if (curGenerationInt != generationInt)
                        {
                            curGenerationInt = generationInt;

                            seqNumInt = 10;
                            if (firstAsm)
                            {
                                firstAsm = false;
                                asmQtyDec = 1;
                            }
                            else
                            {
                                DataTable dtAsm = objMain.GetPanelAsmBOM(partNumStr, revisionNumStr, 0);
                                foreach (DataRow row in dtAsm.Rows)
                                {
                                    if (Int32.Parse(row["Generation"].ToString()) == (curGenerationInt - 1))
                                    {
                                        asmQtyStr = row["QtyPer"].ToString();
                                        asmQtyDec = decimal.Parse(asmQtyStr);
                                        asmUOMStr = row["UOM"].ToString();
                                        break;
                                    }
                                }
                                ++asmSeqInt;
                            }
                        }

                        var dr = dtBOM.NewRow();
                        dr["PartCategory"] = "Panel Assembly";
                        dr["AssemblySeq"] = asmSeqInt;
                        dr["SeqNo"] = seqNumInt;
                        seqNumInt += 10;
                        dr["PartNum"] = drPnl["MtlPartNum"].ToString();
                        dr["Description"] = drPnl["PartDescription"].ToString();
                        dr["QtyPer"] = partQtyDec;

                        //if (drPnl["QtyPer"] == null)
                        //{
                        //    partQtyDec = 0;
                        //}
                        //else
                        //{
                        //    partQtyDec = partQtyDec * asmQtyDec;
                        //}

                        partQtyDec = partQtyDec * asmQtyDec;
                        dr["ReqQty"] = partQtyDec;
                        dr["UOM"] = drPnl["UOM"].ToString();
                        dr["UnitCost"] = decimal.Parse(drPnl["UnitCost"].ToString());
                        dr["WhseCode"] = "LWH5";
                        dr["RelOp"] = 10;
                        dr["PartStatus"] = "Active";
                        dr["RevisionNum"] = mtlRevNumStr;
                        dr["CostMethod"] = "A";
                        dr["PartType"] = "";
                        dr["Status"] = "BackFlush";
                        dr["PROGRESS_RECID"] = 0;
                        dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                        dr["RuleBatchID"] = Int32.Parse(ruleBatchIdStr);
                        dr["ParentPartNum"] = drPnl["ParentPartNum"].ToString();
                        dr["Generation"] = drPnl["Generation"].ToString();
                       
                        dtBOM.Rows.Add(dr);
                    }
                }
            }

            foreach (DataRow row in RowsToRemove)
            {
                dtBOM.Rows.Remove(row);
            }

            return dtBOM;
        }

        private int getAsmSeq(DataTable dtBOM, int asmSeqInt)
        {
            string asmSeqStr = "";

            int retAsmSeqInt = 0;

            foreach (DataRow row in dtBOM.Rows)
            {
                if (row["AssemblySeq"].ToString() != asmSeqStr)
                {
                    asmSeqStr = row["AssemblySeq"].ToString();
                }
            }
            if (Int32.Parse(asmSeqStr) == asmSeqInt)
            {
                retAsmSeqInt = asmSeqInt;
            }
            else
            {
                retAsmSeqInt = asmSeqInt + 1;
            }

            return retAsmSeqInt;
        }

        private DataTable addRefrigAndBreakerAndWarranty(DataTable dtBOM, string mcaStr, string mopStr, string unitType, out string circuit1ChargeRetVal, out string circuit2ChargeRetVal)
        {
            string modelNoStr = lbModelNo.Text;
            string unitPriceStr = "";            
            string ruleHeadIdStr = "";
            string digit3 = modelNoStr.Substring(2, 1);
            string digit4 = modelNoStr.Substring(3, 1);
            string digit567 = modelNoStr.Substring(4, 3);
            string digit9 = modelNoStr.Substring(8, 1);
            string digit11 = modelNoStr.Substring(10, 1);
            string digit12 = modelNoStr.Substring(11, 1);
            string digit13 = modelNoStr.Substring(12, 1);
            string digit14 = modelNoStr.Substring(13, 1);
            string circuit1ChargeStr = "0";
            string circuit2ChargeStr = "0";
            string stationLoc = "";

            int seqNumInt = 0;
            int relOp = 0;
          
            DataTable dtCCD = objMain.GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow drComp = dtCCD.Rows[0];

                if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = drComp["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(drComp["Circuit1_Charge"].ToString()) + Decimal.Parse(drComp["Circuit1_ReheatCharge"].ToString())).ToString();
                }

                if (drComp["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = drComp["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(drComp["Circuit2_Charge"].ToString()) + Decimal.Parse(drComp["Circuit2_ReheatCharge"].ToString())).ToString();
                    }
                }
            }
            else
            {                
                if (lbModelNo.Text.StartsWith("OA") == true)
                {
                    digit13 = "0";
                }             

                DataTable dtChrg = objMain.GetCompressorChargeData(modelNoStr.Substring(0, 3), modelNoStr.Substring(4, 3), 
                                                  modelNoStr.Substring(10, 1), digit13, modelNoStr.Substring(13, 1));
                
                if (dtChrg.Rows.Count > 0)
                {
                    DataRow drChrg = dtChrg.Rows[0];

                    if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit1ChargeStr = drChrg["Circuit1"].ToString();
                    }
                    else
                    {
                        circuit1ChargeStr = (Decimal.Parse(drChrg["Circuit1"].ToString()) + Decimal.Parse(drChrg["Circuit1_Reheat"].ToString())).ToString();
                    }

                    circuit2ChargeStr = drChrg["Circuit2"].ToString();
                }
            }

            circuit1ChargeRetVal = circuit1ChargeStr;
            circuit2ChargeRetVal = circuit2ChargeStr;

            DataTable dtPart = objMain.GetPartDetail("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                unitPriceStr = drPart["UnitCost"].ToString();
            }

            dtPart = objMain.GetPartRulesHead("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                ruleHeadIdStr = drPart["ID"].ToString();
            }

            var dr = dtBOM.NewRow();
            dr["PartCategory"] = "Refrigeration";
            dr["AssemblySeq"] = 0;
            seqNumInt = getNextSeqNum("0", dtBOM);
            dr["SeqNo"] = seqNumInt;
            dr["PartNum"] = "VCPRFG-0400";
            dr["Description"] = "Tank, 100lb, R-410a";

            string partQtyStr = (decimal.Parse(circuit1ChargeStr) + decimal.Parse(circuit2ChargeStr)).ToString();

            decimal partQtyDec = 0;

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
                if (partQtyDec == 0)
                {
                    partQtyDec = 1;
                }
            }

            //dr["QtyPer"] = partQtyDec;
            dr["ReqQty"] = partQtyDec;
            dr["UOM"] = "LB";
            dr["UnitCost"] = decimal.Parse(unitPriceStr);
            dr["WhseCode"] = "LWH5";
            dr["RelOp"] = 60;
            stationLoc = objMain.findStationLoc(60, unitType, "LT");
            dr["PartStatus"] = "Active";
            dr["RevisionNum"] = "";
            dr["CostMethod"] = "A";
            dr["PartType"] = "";
            dr["BuyToOrder"] = (byte)0;
            dr["NonStock"] = (byte)1;
            dr["RunOut"] = (byte)0;
            dr["Status"] = "BackFlush";
            dr["PROGRESS_RECID"] = 0;
            dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr["RuleBatchID"] = 0;
            dr["ParentPartNum"] = "";
            dr["StationLoc"] = stationLoc;
            dr["ConfigurablePart"] = "No";
            dr["SubAssemblyPart"] = 0;
            dr["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr);

            if (Int32.Parse(mopStr) < 100)
            {
                string tempMopStr = "0" + mopStr;
                mopStr = tempMopStr;
            }

            if (unitType == "VKG")
            {
                dtPart = objMain.GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(40, 1), unitType);
            }
            else if (unitType == "REV6")
            {
                dtPart = objMain.GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(40, 1), unitType);
            }
            else
            {
                dtPart = objMain.GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(36, 1), unitType);
            }

            if (dtPart.Rows.Count > 0)
            {
                var dr1 = dtBOM.NewRow();
               
                DataRow drPart = dtPart.Rows[0];

                dr1["PartCategory"] = drPart["PartCategory"].ToString();
                dr1["AssemblySeq"] = 0;
                dr1["SeqNo"] = seqNumInt + 10;
                dr1["PartNum"] = drPart["PartNum"].ToString();
                dr1["Description"] = drPart["PartDescription"].ToString();

                partQtyStr = drPart["ReqQty"].ToString();

                if (partQtyStr == "")
                {
                    partQtyDec = 0;
                }
                else
                {
                    partQtyDec = decimal.Parse(partQtyStr);
                }

                //dr1["QtyPer"] = partQtyDec;
                dr1["ReqQty"] = partQtyDec;
                dr1["UOM"] = drPart["UOM"].ToString();
                unitPriceStr = drPart["UnitPrice"].ToString();
                dr1["UnitCost"] = decimal.Parse(unitPriceStr);
                dr1["WhseCode"] = "LWH5";
                relOp = Int32.Parse(drPart["RelOp"].ToString());
                dr1["RelOp"] = 70;
                stationLoc = objMain.findStationLoc(relOp, unitType, "LT");
                dr1["PartStatus"] = "Active";
                dr1["RevisionNum"] = drPart["RevisionNum"].ToString();
                dr1["CostMethod"] = drPart["CostMethod"].ToString();
                dr1["PartType"] = drPart["PartType"].ToString();
                dr1["BuyToOrder"] = (byte)1;
                dr1["NonStock"] = (byte)1;
                dr1["RunOut"] = (byte)0;           
                dr1["PROGRESS_RECID"] = 0;
                ruleHeadIdStr = drPart["RuleHeadId"].ToString();
                dr1["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                dr1["RuleBatchID"] = 0;
                dr1["ParentPartNum"] = "";
                dr1["StationLoc"] = stationLoc;
                dr1["ConfigurablePart"] = "No";
                dr1["SubAssemblyPart"] = 0;
                dr1["SubAsmMtlPart"] = 0;      
               
                dtBOM.Rows.Add(dr1);
            }

            var dr2 = dtBOM.NewRow();

            dr2["PartCategory"] = "Warranty";
            dr2["AssemblySeq"] = 0;
            seqNumInt = 9998;
            dr2["SeqNo"] = seqNumInt.ToString();
            dr2["PartNum"] = "OAWARRANTYSTD";
            dr2["Description"] = "Warranty Part Entry";

            partQtyStr = "1.00";

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            //dr2["QtyPer"] = partQtyDec;
            dr2["ReqQty"] = partQtyDec;
            dr2["UOM"] = "";
            //unitPriceStr = drPart["UnitPrice"].ToString();
            //dr2["UnitCost"] = decimal.Parse(unitPriceStr);
            dr2["UnitCost"] = 0;
            dr2["WhseCode"] = "LWH5";
            dr2["RelOp"] = 90;
            stationLoc = objMain.findStationLoc(90, unitType, "LT");
            dr2["PartStatus"] = "Active";
            dr2["RevisionNum"] = "";
            dr2["CostMethod"] = "A";
            dr2["PartType"] = "Warranty";
            dr2["BuyToOrder"] = (byte)0;
            dr2["RunOut"] = (byte)0;
            dr2["NonStock"] = (byte)0;
            dr2["Status"] = "BackFlush";
            dr2["PROGRESS_RECID"] = 0;
            ruleHeadIdStr = "0";
            dr2["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr2["RuleBatchID"] = 0;
            dr2["ParentPartNum"] = "";
            dr2["StationLoc"] = stationLoc;
            dr2["ConfigurablePart"] = "No";
            dr2["SubAssemblyPart"] = 0;
            dr2["SubAsmMtlPart"] = 0;
            
            dtBOM.Rows.Add(dr2);

            var dr3 = dtBOM.NewRow();

            dr3["PartCategory"] = "Warranty";
            dr3["AssemblySeq"] = 0;
            seqNumInt = 9999;
            dr3["SeqNo"] = seqNumInt.ToString();
            dr3["PartNum"] = "OAWARRANTYCOMP";
            dr3["Description"] = "Warranty Part Entry";

            partQtyStr = "1.00";

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            //dr3["QtyPer"] = partQtyDec;
            dr3["ReqQty"] = partQtyDec;
            dr3["UOM"] = "";
            //unitPriceStr = drPart["UnitPrice"].ToString();
            //dr3["UnitCost"] = decimal.Parse(unitPriceStr);
            dr3["UnitCost"] = 0;
            dr3["WhseCode"] = "LWH5";
            dr3["RelOp"] = 90;
            dr3["PartStatus"] = "Active";
            dr3["RevisionNum"] = "";
            dr3["CostMethod"] = "A";
            dr3["PartType"] = "Warranty";
            dr3["BuyToOrder"] = (byte)0;
            dr3["RunOut"] = (byte)0;
            dr3["NonStock"] = (byte)0;
            dr3["Status"] = "BackFlush";
            dr3["PROGRESS_RECID"] = 0;
            ruleHeadIdStr = "0";
            dr3["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr3["RuleBatchID"] = 0;
            dr3["ParentPartNum"] = "";
            dr3["StationLoc"] = stationLoc;
            dr3["ConfigurablePart"] = "No";
            dr3["SubAssemblyPart"] = 0;
            dr3["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr3);

            return dtBOM;
        }

        private int getNextSeqNum(string assemblySeqStr, DataTable dtBOM)
        {
            int retSeqNumInt = 0;
            string seqNumStr = "";

            foreach (DataRow dr in dtBOM.Rows)
            {
                if (dr["AssemblySeq"].ToString() == assemblySeqStr)
                {
                    seqNumStr = dr["SeqNo"].ToString();
                }
            }

            if (seqNumStr != "")
            {
                retSeqNumInt = Int32.Parse(seqNumStr) + 10;
            }

            return retSeqNumInt;

        }

        private void printConfigurationReport(string unitTypeStr)
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string heatTypeStr = "";
            string reportPathStr = "";

            if (unitTypeStr == "OA")
            {
                if (modelNoStr.Substring(15, 1) == "A" || modelNoStr.Substring(15, 1) == "B" ||
                    modelNoStr.Substring(15, 1) == "C" || modelNoStr.Substring(15, 1) == "D" ||
                    modelNoStr.Substring(15, 1) == "E" || modelNoStr.Substring(15, 1) == "F" )  // Indirect Fired (IF)
                {
                    heatTypeStr = "IF";
                }
                else if (modelNoStr.Substring(15, 1) == "H" || modelNoStr.Substring(15, 1) == "J") // If Digit 16 (Primary Heat Type) Equals Electric-Staged or Electric-SCR Modulating 
                {
                    heatTypeStr = "ELEC";
                }
                else if ((modelNoStr.Substring(15, 1) == "0") && (modelNoStr.Substring(17, 1) == "1" || modelNoStr.Substring(17, 1) == "2")) // Direct Fired
                {
                    heatTypeStr = "DF";
                }
                else if (modelNoStr.Substring(19, 1) == "G")  // Hot Water
                {
                    heatTypeStr = "HOTWATER";
                }
                else if (modelNoStr.Substring(19, 1) == "K")  // Steam
                {
                    heatTypeStr = "STEAM";
                }
            }
            else
            {
                if (modelNoStr.Substring(15, 1) == "A" || modelNoStr.Substring(15, 1) == "D" ||
                    modelNoStr.Substring(15, 1) == "L" || modelNoStr.Substring(15, 1) == "M")  // Indirect Fired (IF)
                {
                    heatTypeStr = "IF";
                }
                else if (modelNoStr.Substring(15, 1) == "H") // If Digit 16 (Primary Heat Type) Equals Electric-Staged
                {
                    heatTypeStr = "ELEC";
                }
                else if (modelNoStr.Substring(19, 1) == "G")  // Hot Water
                {
                    heatTypeStr = "HOTWATER";
                }
                else if (modelNoStr.Substring(19, 1) == "K")  // Steam
                {
                    heatTypeStr = "STEAM";
                }
            }
            

            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@ModelNo";
            pdv2.Value = modelNoStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@HeatType";
            pdv3.Value = heatTypeStr;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

            if (unitTypeStr == "OA")
            {
#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\R6_OA_ModelNoConfiguration.rpt";             
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\R6_OA_ModelNoConfiguration.rpt";
#endif                              
            }
            else
            {
#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\VKG_ModelNoConfiguration.rpt";             
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\VKG_ModelNoConfiguration.rpt";
#endif

                cryRpt.Load(objMain.ReportString);   

               
            }

            cryRpt.Load(objMain.ReportString); 
            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }

        #endregion

        private void cbHeatTypePrimary_SelectedIndexChanged(object sender, EventArgs e)
        {
            string digitValStr = cbHeatTypePrimary.SelectedValue.ToString();

            if (digitValStr == "A" || digitValStr == "B" || digitValStr == "C" || digitValStr == "D" || digitValStr == "E" || digitValStr == "F")
            {
                HeatType = "IF";
            }
            else if (digitValStr == "G")
            {
                HeatType = "HOTWATER";
            }
            else if (digitValStr == "H" || digitValStr == "J")
            {
                HeatType = "ELEC";
            }
            else
            {
                HeatType = "NA";
            }

            DataTable dt = objMain.GetModelNumDesc(17, HeatType, null, "OAU");
            cbHeatCapacityPrimary.DataSource = dt;
            cbHeatCapacityPrimary.DisplayMember = "DigitDescription";
            cbHeatCapacityPrimary.ValueMember = "DigitVal";
        }

        private void cbHeatTypeSecondary_SelectedIndexChanged(object sender, EventArgs e)
        {
            string digitValStr = cbHeatTypeSecondary.SelectedValue.ToString();

            if (digitValStr == "1" || digitValStr == "2")
            {
                HeatType = "DF";
            }
            else if (digitValStr == "3")
            {
                HeatType = "HOTWATER";
            }
            else if (digitValStr == "4" || digitValStr == "5")
            {
                HeatType = "ELEC";
            }

            DataTable dt = objMain.GetModelNumDesc(19, HeatType, null, "OAU");
            cbHeatCapacitySecondary.DataSource = dt;
            cbHeatCapacitySecondary.DisplayMember = "DigitDescription";
            cbHeatCapacitySecondary.ValueMember = "DigitVal";

        }

        private void frmModelNoConfig_Load(object sender, EventArgs e)
        {

        }

    }
}
