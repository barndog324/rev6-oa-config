﻿namespace OA_Config_Rev6
{
    partial class frmSheetMetalUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMetalUsage = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lbMainMsg = new System.Windows.Forms.Label();
            this.lbMainTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbJobNumList = new System.Windows.Forms.Label();
            this.lbReleaseID = new System.Windows.Forms.Label();
            this.btnAddPart = new System.Windows.Forms.Button();
            this.gbAddPart = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSelectPart = new System.Windows.Forms.Button();
            this.cbPartNumber = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMetalUsage)).BeginInit();
            this.gbAddPart.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMetalUsage
            // 
            this.dgvMetalUsage.AllowUserToAddRows = false;
            this.dgvMetalUsage.AllowUserToDeleteRows = false;
            this.dgvMetalUsage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMetalUsage.Location = new System.Drawing.Point(12, 64);
            this.dgvMetalUsage.MultiSelect = false;
            this.dgvMetalUsage.Name = "dgvMetalUsage";
            this.dgvMetalUsage.Size = new System.Drawing.Size(826, 594);
            this.dgvMetalUsage.TabIndex = 0;
            this.dgvMetalUsage.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMetalUsage_CellContentClick);
            this.dgvMetalUsage.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMetalUsage_DataBindingComplete);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(761, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.Green;
            this.btnSubmit.Location = new System.Drawing.Point(679, 5);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lbMainMsg
            // 
            this.lbMainMsg.BackColor = System.Drawing.Color.LightSalmon;
            this.lbMainMsg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainMsg.Location = new System.Drawing.Point(12, 663);
            this.lbMainMsg.Name = "lbMainMsg";
            this.lbMainMsg.Size = new System.Drawing.Size(826, 20);
            this.lbMainMsg.TabIndex = 26;
            this.lbMainMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMainTitle
            // 
            this.lbMainTitle.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitle.Location = new System.Drawing.Point(246, 0);
            this.lbMainTitle.Name = "lbMainTitle";
            this.lbMainTitle.Size = new System.Drawing.Size(328, 39);
            this.lbMainTitle.TabIndex = 27;
            this.lbMainTitle.Text = "Sheet Metal Usage";
            this.lbMainTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 16);
            this.label1.TabIndex = 28;
            this.label1.Text = "Job Number List:";
            // 
            // lbJobNumList
            // 
            this.lbJobNumList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNumList.Location = new System.Drawing.Point(15, 45);
            this.lbJobNumList.Name = "lbJobNumList";
            this.lbJobNumList.Size = new System.Drawing.Size(821, 16);
            this.lbJobNumList.TabIndex = 29;
            this.lbJobNumList.Text = "Job Number List:";
            // 
            // lbReleaseID
            // 
            this.lbReleaseID.AutoSize = true;
            this.lbReleaseID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbReleaseID.Location = new System.Drawing.Point(114, 5);
            this.lbReleaseID.Name = "lbReleaseID";
            this.lbReleaseID.Size = new System.Drawing.Size(82, 16);
            this.lbReleaseID.TabIndex = 30;
            this.lbReleaseID.Text = "ReleaseID";
            this.lbReleaseID.Visible = false;
            // 
            // btnAddPart
            // 
            this.btnAddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPart.ForeColor = System.Drawing.Color.Blue;
            this.btnAddPart.Location = new System.Drawing.Point(596, 5);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(75, 23);
            this.btnAddPart.TabIndex = 31;
            this.btnAddPart.Text = "Add Part";
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click);
            // 
            // gbAddPart
            // 
            this.gbAddPart.BackColor = System.Drawing.Color.Silver;
            this.gbAddPart.Controls.Add(this.btnCancel);
            this.gbAddPart.Controls.Add(this.btnSelectPart);
            this.gbAddPart.Controls.Add(this.cbPartNumber);
            this.gbAddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAddPart.ForeColor = System.Drawing.Color.Blue;
            this.gbAddPart.Location = new System.Drawing.Point(244, 308);
            this.gbAddPart.Name = "gbAddPart";
            this.gbAddPart.Size = new System.Drawing.Size(387, 100);
            this.gbAddPart.TabIndex = 32;
            this.gbAddPart.TabStop = false;
            this.gbAddPart.Text = "Select Part Number";
            this.gbAddPart.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Red;
            this.btnCancel.Location = new System.Drawing.Point(302, 65);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 33;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSelectPart
            // 
            this.btnSelectPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectPart.ForeColor = System.Drawing.Color.Blue;
            this.btnSelectPart.Location = new System.Drawing.Point(221, 65);
            this.btnSelectPart.Name = "btnSelectPart";
            this.btnSelectPart.Size = new System.Drawing.Size(75, 23);
            this.btnSelectPart.TabIndex = 32;
            this.btnSelectPart.Text = "Add Part";
            this.btnSelectPart.UseVisualStyleBackColor = true;
            this.btnSelectPart.Click += new System.EventHandler(this.btnSelectPart_Click);
            // 
            // cbPartNumber
            // 
            this.cbPartNumber.DropDownHeight = 200;
            this.cbPartNumber.DropDownWidth = 600;
            this.cbPartNumber.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPartNumber.FormattingEnabled = true;
            this.cbPartNumber.IntegralHeight = false;
            this.cbPartNumber.Location = new System.Drawing.Point(12, 35);
            this.cbPartNumber.Name = "cbPartNumber";
            this.cbPartNumber.Size = new System.Drawing.Size(365, 22);
            this.cbPartNumber.TabIndex = 0;
            // 
            // frmSheetMetalUsage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 687);
            this.Controls.Add(this.gbAddPart);
            this.Controls.Add(this.btnAddPart);
            this.Controls.Add(this.lbReleaseID);
            this.Controls.Add(this.lbJobNumList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbMainTitle);
            this.Controls.Add(this.lbMainMsg);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.dgvMetalUsage);
            this.Name = "frmSheetMetalUsage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sheet Metal Release Metal Usage";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMetalUsage)).EndInit();
            this.gbAddPart.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSubmit;
        public System.Windows.Forms.DataGridView dgvMetalUsage;
        public System.Windows.Forms.Label lbMainMsg;
        public System.Windows.Forms.Label lbMainTitle;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lbJobNumList;
        public System.Windows.Forms.Label lbReleaseID;
        private System.Windows.Forms.Button btnAddPart;
        private System.Windows.Forms.GroupBox gbAddPart;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSelectPart;
        private System.Windows.Forms.ComboBox cbPartNumber;
    }
}