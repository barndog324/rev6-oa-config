﻿namespace OA_Config_Rev6
{
    partial class frmMonitorModelNoConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripConfig = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbLastUpdateBy = new System.Windows.Forms.Label();
            this.lbBomCreateBy = new System.Windows.Forms.Label();
            this.lbCustName = new System.Windows.Forms.Label();
            this.lbLastUpdateDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbCompleteDate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbProdStartDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbBOMCreationDate = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbShipDate = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lbOrderDate = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.groupBoxCabinet = new System.Windows.Forms.GroupBox();
            this.label168 = new System.Windows.Forms.Label();
            this.cbPulleyFan = new System.Windows.Forms.ComboBox();
            this.label167 = new System.Windows.Forms.Label();
            this.cbBelt = new System.Windows.Forms.ComboBox();
            this.label166 = new System.Windows.Forms.Label();
            this.cbPulleyDriver = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbSmokeDectectorOutdoorAirMon = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbSupplyDampers = new System.Windows.Forms.ComboBox();
            this.cbCommunicationsOptions = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cbEnergyRecoveryOptions = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbEnergyRecoveryWheel = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbExhaustDampers = new System.Windows.Forms.ComboBox();
            this.cbConvenienceOutletOptions = new System.Windows.Forms.ComboBox();
            this.cbDisconnectSwitch = new System.Windows.Forms.ComboBox();
            this.cbEfficiency = new System.Windows.Forms.ComboBox();
            this.label133 = new System.Windows.Forms.Label();
            this.cbCoolingCapacity = new System.Windows.Forms.ComboBox();
            this.label132 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.cbExhaustFanSize = new System.Windows.Forms.ComboBox();
            this.label158 = new System.Windows.Forms.Label();
            this.cbExhaustFanMotorType = new System.Windows.Forms.ComboBox();
            this.label157 = new System.Windows.Forms.Label();
            this.cbExhaustFanMotor = new System.Windows.Forms.ComboBox();
            this.label156 = new System.Windows.Forms.Label();
            this.cbPreHeaterOption = new System.Windows.Forms.ComboBox();
            this.label155 = new System.Windows.Forms.Label();
            this.cbAdvancedUnitControls = new System.Windows.Forms.ComboBox();
            this.label154 = new System.Windows.Forms.Label();
            this.cbShortCircuitCurrentRating = new System.Windows.Forms.ComboBox();
            this.label153 = new System.Windows.Forms.Label();
            this.cbUnitHardwareEnhancements = new System.Windows.Forms.ComboBox();
            this.label152 = new System.Windows.Forms.Label();
            this.cbSystemMonitoringControls = new System.Windows.Forms.ComboBox();
            this.label151 = new System.Windows.Forms.Label();
            this.cbTraneSystemMonitoringControls = new System.Windows.Forms.ComboBox();
            this.cbSmokeDetector = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbRefrigerationControls = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbRefrigerationSystemOptions = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelInputsHeatCapacityLabel = new System.Windows.Forms.Label();
            this.cbThruBaseProvisions = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.cbHingedServiceAccessFilters = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.cbSupplyFanMotor = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.cbFreshAirSelection = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.cbServiceSequence = new System.Windows.Forms.ComboBox();
            this.label143 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbVoltage = new System.Windows.Forms.ComboBox();
            this.cbAirflowConfiguration = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.cbNotUsed = new System.Windows.Forms.ComboBox();
            this.label141 = new System.Windows.Forms.Label();
            this.cbMinorDesignSequence = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.cbUnitControls = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbHeatingCapacity = new System.Windows.Forms.ComboBox();
            this.label139 = new System.Windows.Forms.Label();
            this.cbCondenserCoilProtection = new System.Windows.Forms.ComboBox();
            this.label138 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.cbMajorDesignSequence = new System.Windows.Forms.ComboBox();
            this.label135 = new System.Windows.Forms.Label();
            this.cbUnitType = new System.Windows.Forms.ComboBox();
            this.label136 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.btnParseModelNo = new System.Windows.Forms.Button();
            this.lbEnterModelNo = new System.Windows.Forms.Label();
            this.txtModelNo = new System.Windows.Forms.TextBox();
            this.btnCreateBOM = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label164 = new System.Windows.Forms.Label();
            this.txtOrderNum = new System.Windows.Forms.TextBox();
            this.btnCreateSubmittal = new System.Windows.Forms.Button();
            this.gbOrder_ModelNo = new System.Windows.Forms.GroupBox();
            this.label165 = new System.Windows.Forms.Label();
            this.txtOrderLine = new System.Windows.Forms.TextBox();
            this.btnGetOrderDetails = new System.Windows.Forms.Button();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.menuStripConfig.SuspendLayout();
            this.groupBoxCabinet.SuspendLayout();
            this.gbOrder_ModelNo.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripConfig
            // 
            this.menuStripConfig.BackColor = System.Drawing.Color.LightSalmon;
            this.menuStripConfig.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripConfig.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripConfig.Location = new System.Drawing.Point(0, 0);
            this.menuStripConfig.Name = "menuStripConfig";
            this.menuStripConfig.Size = new System.Drawing.Size(1070, 24);
            this.menuStripConfig.TabIndex = 474;
            this.menuStripConfig.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createBOMToolStripMenuItem,
            this.deleteBOMToolStripMenuItem,
            this.printToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createBOMToolStripMenuItem
            // 
            this.createBOMToolStripMenuItem.Name = "createBOMToolStripMenuItem";
            this.createBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.createBOMToolStripMenuItem.Text = "Create BOM";
            this.createBOMToolStripMenuItem.Click += new System.EventHandler(this.createBOMToolStripMenuItem_Click);
            // 
            // deleteBOMToolStripMenuItem
            // 
            this.deleteBOMToolStripMenuItem.Name = "deleteBOMToolStripMenuItem";
            this.deleteBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteBOMToolStripMenuItem.Text = "Delete BOM";
            this.deleteBOMToolStripMenuItem.Click += new System.EventHandler(this.deleteBOMToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "Print";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // lbLastUpdateBy
            // 
            this.lbLastUpdateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateBy.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateBy.Location = new System.Drawing.Point(934, 87);
            this.lbLastUpdateBy.Name = "lbLastUpdateBy";
            this.lbLastUpdateBy.Size = new System.Drawing.Size(90, 20);
            this.lbLastUpdateBy.TabIndex = 593;
            this.lbLastUpdateBy.Text = "??/??/????";
            this.lbLastUpdateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbBomCreateBy
            // 
            this.lbBomCreateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBomCreateBy.ForeColor = System.Drawing.Color.Black;
            this.lbBomCreateBy.Location = new System.Drawing.Point(934, 47);
            this.lbBomCreateBy.Name = "lbBomCreateBy";
            this.lbBomCreateBy.Size = new System.Drawing.Size(90, 20);
            this.lbBomCreateBy.TabIndex = 592;
            this.lbBomCreateBy.Text = "??/??/????";
            this.lbBomCreateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Black;
            this.lbCustName.Location = new System.Drawing.Point(235, 100);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(525, 30);
            this.lbCustName.TabIndex = 591;
            this.lbCustName.Text = "CustomerName";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbLastUpdateDate
            // 
            this.lbLastUpdateDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateDate.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateDate.Location = new System.Drawing.Point(934, 67);
            this.lbLastUpdateDate.Name = "lbLastUpdateDate";
            this.lbLastUpdateDate.Size = new System.Drawing.Size(90, 20);
            this.lbLastUpdateDate.TabIndex = 590;
            this.lbLastUpdateDate.Text = "??/??/????";
            this.lbLastUpdateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(795, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 589;
            this.label3.Text = "Last Updated Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label129
            // 
            this.label129.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Black;
            this.label129.Location = new System.Drawing.Point(794, 183);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(10, 15);
            this.label129.TabIndex = 588;
            this.label129.Text = "9";
            // 
            // label130
            // 
            this.label130.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Black;
            this.label130.Location = new System.Drawing.Point(794, 168);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(10, 15);
            this.label130.TabIndex = 587;
            this.label130.Text = "3";
            // 
            // label131
            // 
            this.label131.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Black;
            this.label131.Location = new System.Drawing.Point(794, 160);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(10, 15);
            this.label131.TabIndex = 586;
            this.label131.Text = "^";
            // 
            // label127
            // 
            this.label127.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Black;
            this.label127.Location = new System.Drawing.Point(634, 183);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(10, 15);
            this.label127.TabIndex = 585;
            this.label127.Text = "9";
            // 
            // label128
            // 
            this.label128.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Black;
            this.label128.Location = new System.Drawing.Point(619, 183);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(10, 15);
            this.label128.TabIndex = 584;
            this.label128.Text = "8";
            // 
            // label119
            // 
            this.label119.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(778, 183);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(10, 15);
            this.label119.TabIndex = 583;
            this.label119.Text = "8";
            // 
            // label120
            // 
            this.label120.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(762, 183);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(10, 15);
            this.label120.TabIndex = 582;
            this.label120.Text = "7";
            // 
            // label121
            // 
            this.label121.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(746, 183);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(10, 15);
            this.label121.TabIndex = 581;
            this.label121.Text = "6";
            // 
            // label122
            // 
            this.label122.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Black;
            this.label122.Location = new System.Drawing.Point(731, 183);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(10, 15);
            this.label122.TabIndex = 580;
            this.label122.Text = "5";
            // 
            // label123
            // 
            this.label123.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Black;
            this.label123.Location = new System.Drawing.Point(715, 183);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(10, 15);
            this.label123.TabIndex = 579;
            this.label123.Text = "4";
            // 
            // label124
            // 
            this.label124.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.Black;
            this.label124.Location = new System.Drawing.Point(698, 183);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(10, 15);
            this.label124.TabIndex = 578;
            this.label124.Text = "3";
            // 
            // label125
            // 
            this.label125.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.Black;
            this.label125.Location = new System.Drawing.Point(681, 183);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(10, 15);
            this.label125.TabIndex = 577;
            this.label125.Text = "2";
            // 
            // label126
            // 
            this.label126.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Black;
            this.label126.Location = new System.Drawing.Point(666, 183);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(10, 15);
            this.label126.TabIndex = 576;
            this.label126.Text = "1";
            // 
            // label111
            // 
            this.label111.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(603, 183);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(10, 15);
            this.label111.TabIndex = 575;
            this.label111.Text = "7";
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Black;
            this.label112.Location = new System.Drawing.Point(588, 183);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(10, 15);
            this.label112.TabIndex = 574;
            this.label112.Text = "6";
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(571, 183);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(10, 15);
            this.label113.TabIndex = 573;
            this.label113.Text = "5";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(555, 183);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(10, 15);
            this.label114.TabIndex = 572;
            this.label114.Text = "4";
            // 
            // label115
            // 
            this.label115.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(539, 183);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(10, 15);
            this.label115.TabIndex = 571;
            this.label115.Text = "3";
            // 
            // label116
            // 
            this.label116.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Black;
            this.label116.Location = new System.Drawing.Point(522, 183);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(10, 15);
            this.label116.TabIndex = 570;
            this.label116.Text = "2";
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(505, 183);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(10, 15);
            this.label117.TabIndex = 569;
            this.label117.Text = "1";
            // 
            // label118
            // 
            this.label118.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(490, 183);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(10, 15);
            this.label118.TabIndex = 568;
            this.label118.Text = "0";
            // 
            // label103
            // 
            this.label103.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(458, 183);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(10, 15);
            this.label103.TabIndex = 567;
            this.label103.Text = "8";
            // 
            // label104
            // 
            this.label104.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(443, 183);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(10, 15);
            this.label104.TabIndex = 566;
            this.label104.Text = "7";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(426, 183);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(10, 15);
            this.label105.TabIndex = 565;
            this.label105.Text = "6";
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(410, 183);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(10, 15);
            this.label106.TabIndex = 564;
            this.label106.Text = "5";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(393, 183);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(10, 15);
            this.label107.TabIndex = 563;
            this.label107.Text = "4";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(377, 183);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(10, 15);
            this.label108.TabIndex = 562;
            this.label108.Text = "3";
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Black;
            this.label109.Location = new System.Drawing.Point(360, 183);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(10, 15);
            this.label109.TabIndex = 561;
            this.label109.Text = "2";
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(345, 183);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(10, 15);
            this.label110.TabIndex = 560;
            this.label110.Text = "1";
            // 
            // label102
            // 
            this.label102.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(314, 168);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(10, 15);
            this.label102.TabIndex = 559;
            this.label102.Text = "9";
            // 
            // label100
            // 
            this.label100.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(778, 168);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(10, 15);
            this.label100.TabIndex = 558;
            this.label100.Text = "3";
            // 
            // label101
            // 
            this.label101.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(762, 168);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(10, 15);
            this.label101.TabIndex = 557;
            this.label101.Text = "3";
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(746, 168);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(10, 15);
            this.label96.TabIndex = 556;
            this.label96.Text = "3";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(731, 168);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(10, 15);
            this.label97.TabIndex = 555;
            this.label97.Text = "3";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(715, 168);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(10, 15);
            this.label98.TabIndex = 554;
            this.label98.Text = "3";
            // 
            // label99
            // 
            this.label99.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(698, 168);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(10, 15);
            this.label99.TabIndex = 553;
            this.label99.Text = "3";
            // 
            // label92
            // 
            this.label92.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(681, 168);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(10, 15);
            this.label92.TabIndex = 552;
            this.label92.Text = "3";
            // 
            // label93
            // 
            this.label93.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(666, 168);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(10, 15);
            this.label93.TabIndex = 551;
            this.label93.Text = "3";
            // 
            // label94
            // 
            this.label94.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(634, 168);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(10, 15);
            this.label94.TabIndex = 550;
            this.label94.Text = "2";
            // 
            // label95
            // 
            this.label95.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(619, 168);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(10, 15);
            this.label95.TabIndex = 549;
            this.label95.Text = "2";
            // 
            // label88
            // 
            this.label88.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(603, 168);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(10, 15);
            this.label88.TabIndex = 548;
            this.label88.Text = "2";
            // 
            // label89
            // 
            this.label89.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(588, 168);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(10, 15);
            this.label89.TabIndex = 547;
            this.label89.Text = "2";
            // 
            // label90
            // 
            this.label90.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(571, 168);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(10, 15);
            this.label90.TabIndex = 546;
            this.label90.Text = "2";
            // 
            // label91
            // 
            this.label91.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(555, 168);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(10, 15);
            this.label91.TabIndex = 545;
            this.label91.Text = "2";
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(539, 168);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(10, 15);
            this.label84.TabIndex = 544;
            this.label84.Text = "2";
            // 
            // label85
            // 
            this.label85.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(522, 168);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(10, 15);
            this.label85.TabIndex = 543;
            this.label85.Text = "2";
            // 
            // label86
            // 
            this.label86.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(505, 168);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(10, 15);
            this.label86.TabIndex = 542;
            this.label86.Text = "2";
            // 
            // label87
            // 
            this.label87.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(490, 168);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(10, 15);
            this.label87.TabIndex = 541;
            this.label87.Text = "2";
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(458, 168);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(10, 15);
            this.label80.TabIndex = 540;
            this.label80.Text = "1";
            // 
            // label81
            // 
            this.label81.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(443, 168);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(10, 15);
            this.label81.TabIndex = 539;
            this.label81.Text = "1";
            // 
            // label82
            // 
            this.label82.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(426, 168);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(10, 15);
            this.label82.TabIndex = 538;
            this.label82.Text = "1";
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(410, 168);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(10, 15);
            this.label83.TabIndex = 537;
            this.label83.Text = "1";
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(393, 168);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(10, 15);
            this.label76.TabIndex = 536;
            this.label76.Text = "1";
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(377, 168);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(10, 15);
            this.label77.TabIndex = 535;
            this.label77.Text = "1";
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(360, 168);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(10, 15);
            this.label78.TabIndex = 534;
            this.label78.Text = "1";
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(345, 168);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(10, 15);
            this.label79.TabIndex = 533;
            this.label79.Text = "1";
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(298, 168);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(10, 15);
            this.label72.TabIndex = 532;
            this.label72.Text = "8";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(282, 168);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(10, 15);
            this.label73.TabIndex = 531;
            this.label73.Text = "7";
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(265, 168);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(10, 15);
            this.label74.TabIndex = 530;
            this.label74.Text = "6";
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(249, 168);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(10, 15);
            this.label75.TabIndex = 529;
            this.label75.Text = "5";
            // 
            // label70
            // 
            this.label70.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(234, 168);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(10, 15);
            this.label70.TabIndex = 528;
            this.label70.Text = "4";
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(218, 168);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(10, 15);
            this.label71.TabIndex = 527;
            this.label71.Text = "3";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(201, 168);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(10, 15);
            this.label69.TabIndex = 526;
            this.label69.Text = "2";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(186, 168);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(10, 15);
            this.label68.TabIndex = 525;
            this.label68.Text = "1";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(314, 160);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(10, 15);
            this.label67.TabIndex = 524;
            this.label67.Text = "^";
            // 
            // label66
            // 
            this.label66.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(635, 160);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(10, 15);
            this.label66.TabIndex = 523;
            this.label66.Text = "^";
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(619, 160);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(10, 15);
            this.label65.TabIndex = 522;
            this.label65.Text = "^";
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(778, 160);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(10, 15);
            this.label61.TabIndex = 521;
            this.label61.Text = "^";
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(762, 160);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(10, 15);
            this.label62.TabIndex = 520;
            this.label62.Text = "^";
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(746, 160);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(10, 15);
            this.label63.TabIndex = 519;
            this.label63.Text = "^";
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(731, 160);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(10, 15);
            this.label64.TabIndex = 518;
            this.label64.Text = "^";
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(715, 160);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(10, 15);
            this.label57.TabIndex = 517;
            this.label57.Text = "^";
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(698, 160);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(10, 15);
            this.label58.TabIndex = 516;
            this.label58.Text = "^";
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(682, 160);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(10, 15);
            this.label59.TabIndex = 515;
            this.label59.Text = "^";
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(667, 160);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(10, 15);
            this.label60.TabIndex = 514;
            this.label60.Text = "^";
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(603, 160);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(10, 15);
            this.label53.TabIndex = 513;
            this.label53.Text = "^";
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(588, 160);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(10, 15);
            this.label54.TabIndex = 512;
            this.label54.Text = "^";
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(571, 160);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(10, 15);
            this.label55.TabIndex = 511;
            this.label55.Text = "^";
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(556, 160);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(10, 15);
            this.label56.TabIndex = 510;
            this.label56.Text = "^";
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(539, 160);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(10, 15);
            this.label49.TabIndex = 509;
            this.label49.Text = "^";
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(522, 160);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(10, 15);
            this.label50.TabIndex = 508;
            this.label50.Text = "^";
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(506, 160);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(10, 15);
            this.label51.TabIndex = 507;
            this.label51.Text = "^";
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(490, 160);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(10, 15);
            this.label52.TabIndex = 506;
            this.label52.Text = "^";
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(458, 160);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(10, 15);
            this.label45.TabIndex = 505;
            this.label45.Text = "^";
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(443, 160);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(10, 15);
            this.label46.TabIndex = 504;
            this.label46.Text = "^";
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(427, 160);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(10, 15);
            this.label47.TabIndex = 503;
            this.label47.Text = "^";
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(411, 160);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(10, 15);
            this.label48.TabIndex = 502;
            this.label48.Text = "^";
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(393, 160);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(10, 15);
            this.label41.TabIndex = 501;
            this.label41.Text = "^";
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(377, 160);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(10, 15);
            this.label42.TabIndex = 500;
            this.label42.Text = "^";
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(361, 160);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(10, 15);
            this.label43.TabIndex = 499;
            this.label43.Text = "^";
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(346, 160);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(10, 15);
            this.label44.TabIndex = 498;
            this.label44.Text = "^";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(299, 160);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(10, 15);
            this.label37.TabIndex = 497;
            this.label37.Text = "^";
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(282, 160);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(10, 15);
            this.label38.TabIndex = 496;
            this.label38.Text = "^";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(266, 160);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(10, 15);
            this.label39.TabIndex = 495;
            this.label39.Text = "^";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(250, 160);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(10, 15);
            this.label40.TabIndex = 494;
            this.label40.Text = "^";
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(234, 160);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(10, 15);
            this.label35.TabIndex = 493;
            this.label35.Text = "^";
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(218, 160);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(10, 15);
            this.label36.TabIndex = 492;
            this.label36.Text = "^";
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(201, 160);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(10, 15);
            this.label34.TabIndex = 491;
            this.label34.Text = "^";
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(186, 160);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(10, 15);
            this.label33.TabIndex = 490;
            this.label33.Text = "^";
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Black;
            this.lbModelNo.Location = new System.Drawing.Point(180, 134);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(707, 30);
            this.lbModelNo.TabIndex = 489;
            this.lbModelNo.Text = "1234567890123456789012345678901234567890123";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCompleteDate
            // 
            this.lbCompleteDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCompleteDate.ForeColor = System.Drawing.Color.Black;
            this.lbCompleteDate.Location = new System.Drawing.Point(139, 87);
            this.lbCompleteDate.Name = "lbCompleteDate";
            this.lbCompleteDate.Size = new System.Drawing.Size(94, 20);
            this.lbCompleteDate.TabIndex = 488;
            this.lbCompleteDate.Text = "??/??/????";
            this.lbCompleteDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(21, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 487;
            this.label4.Text = "Complete Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbProdStartDate
            // 
            this.lbProdStartDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProdStartDate.ForeColor = System.Drawing.Color.Black;
            this.lbProdStartDate.Location = new System.Drawing.Point(139, 67);
            this.lbProdStartDate.Name = "lbProdStartDate";
            this.lbProdStartDate.Size = new System.Drawing.Size(94, 20);
            this.lbProdStartDate.TabIndex = 486;
            this.lbProdStartDate.Text = "??/??/????";
            this.lbProdStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(21, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 485;
            this.label2.Text = "Prod Start Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbBOMCreationDate
            // 
            this.lbBOMCreationDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBOMCreationDate.ForeColor = System.Drawing.Color.Black;
            this.lbBOMCreationDate.Location = new System.Drawing.Point(934, 27);
            this.lbBOMCreationDate.Name = "lbBOMCreationDate";
            this.lbBOMCreationDate.Size = new System.Drawing.Size(90, 20);
            this.lbBOMCreationDate.TabIndex = 484;
            this.lbBOMCreationDate.Text = "??/??/????";
            this.lbBOMCreationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Teal;
            this.label27.Location = new System.Drawing.Point(795, 27);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(140, 20);
            this.label27.TabIndex = 483;
            this.label27.Text = "BOM Creation Date:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbShipDate
            // 
            this.lbShipDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShipDate.ForeColor = System.Drawing.Color.Black;
            this.lbShipDate.Location = new System.Drawing.Point(140, 47);
            this.lbShipDate.Name = "lbShipDate";
            this.lbShipDate.Size = new System.Drawing.Size(94, 20);
            this.lbShipDate.TabIndex = 482;
            this.lbShipDate.Text = "??/??/????";
            this.lbShipDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Teal;
            this.label32.Location = new System.Drawing.Point(0, 47);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 20);
            this.label32.TabIndex = 481;
            this.label32.Text = "Ship Date:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbOrderDate
            // 
            this.lbOrderDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderDate.ForeColor = System.Drawing.Color.Black;
            this.lbOrderDate.Location = new System.Drawing.Point(140, 27);
            this.lbOrderDate.Name = "lbOrderDate";
            this.lbOrderDate.Size = new System.Drawing.Size(100, 20);
            this.lbOrderDate.TabIndex = 480;
            this.lbOrderDate.Text = "??/??/????";
            this.lbOrderDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Teal;
            this.label26.Location = new System.Drawing.Point(1, 27);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 20);
            this.label26.TabIndex = 479;
            this.label26.Text = "Order Date:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(319, 66);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(140, 30);
            this.label25.TabIndex = 478;
            this.label25.Text = "Job #:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Black;
            this.lbJobNum.Location = new System.Drawing.Point(458, 66);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(187, 30);
            this.lbJobNum.TabIndex = 477;
            this.lbJobNum.Text = "??????-??-??";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Tahoma", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.Blue;
            this.lbEnvironment.Location = new System.Drawing.Point(237, 26);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(551, 30);
            this.lbEnvironment.TabIndex = 476;
            this.lbEnvironment.Text = "Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxCabinet
            // 
            this.groupBoxCabinet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxCabinet.Controls.Add(this.label168);
            this.groupBoxCabinet.Controls.Add(this.cbPulleyFan);
            this.groupBoxCabinet.Controls.Add(this.label167);
            this.groupBoxCabinet.Controls.Add(this.cbBelt);
            this.groupBoxCabinet.Controls.Add(this.label166);
            this.groupBoxCabinet.Controls.Add(this.cbPulleyDriver);
            this.groupBoxCabinet.Controls.Add(this.label23);
            this.groupBoxCabinet.Controls.Add(this.cbSmokeDectectorOutdoorAirMon);
            this.groupBoxCabinet.Controls.Add(this.label10);
            this.groupBoxCabinet.Controls.Add(this.cbSupplyDampers);
            this.groupBoxCabinet.Controls.Add(this.cbCommunicationsOptions);
            this.groupBoxCabinet.Controls.Add(this.label22);
            this.groupBoxCabinet.Controls.Add(this.label17);
            this.groupBoxCabinet.Controls.Add(this.cbEnergyRecoveryOptions);
            this.groupBoxCabinet.Controls.Add(this.label16);
            this.groupBoxCabinet.Controls.Add(this.cbEnergyRecoveryWheel);
            this.groupBoxCabinet.Controls.Add(this.label15);
            this.groupBoxCabinet.Controls.Add(this.cbExhaustDampers);
            this.groupBoxCabinet.Controls.Add(this.cbConvenienceOutletOptions);
            this.groupBoxCabinet.Controls.Add(this.cbDisconnectSwitch);
            this.groupBoxCabinet.Controls.Add(this.cbEfficiency);
            this.groupBoxCabinet.Controls.Add(this.label133);
            this.groupBoxCabinet.Controls.Add(this.cbCoolingCapacity);
            this.groupBoxCabinet.Controls.Add(this.label132);
            this.groupBoxCabinet.Controls.Add(this.label159);
            this.groupBoxCabinet.Controls.Add(this.cbExhaustFanSize);
            this.groupBoxCabinet.Controls.Add(this.label158);
            this.groupBoxCabinet.Controls.Add(this.cbExhaustFanMotorType);
            this.groupBoxCabinet.Controls.Add(this.label157);
            this.groupBoxCabinet.Controls.Add(this.cbExhaustFanMotor);
            this.groupBoxCabinet.Controls.Add(this.label156);
            this.groupBoxCabinet.Controls.Add(this.cbPreHeaterOption);
            this.groupBoxCabinet.Controls.Add(this.label155);
            this.groupBoxCabinet.Controls.Add(this.cbAdvancedUnitControls);
            this.groupBoxCabinet.Controls.Add(this.label154);
            this.groupBoxCabinet.Controls.Add(this.cbShortCircuitCurrentRating);
            this.groupBoxCabinet.Controls.Add(this.label153);
            this.groupBoxCabinet.Controls.Add(this.cbUnitHardwareEnhancements);
            this.groupBoxCabinet.Controls.Add(this.label152);
            this.groupBoxCabinet.Controls.Add(this.cbSystemMonitoringControls);
            this.groupBoxCabinet.Controls.Add(this.label151);
            this.groupBoxCabinet.Controls.Add(this.cbTraneSystemMonitoringControls);
            this.groupBoxCabinet.Controls.Add(this.cbSmokeDetector);
            this.groupBoxCabinet.Controls.Add(this.label14);
            this.groupBoxCabinet.Controls.Add(this.cbRefrigerationControls);
            this.groupBoxCabinet.Controls.Add(this.label12);
            this.groupBoxCabinet.Controls.Add(this.cbRefrigerationSystemOptions);
            this.groupBoxCabinet.Controls.Add(this.label11);
            this.groupBoxCabinet.Controls.Add(this.label9);
            this.groupBoxCabinet.Controls.Add(this.labelInputsHeatCapacityLabel);
            this.groupBoxCabinet.Controls.Add(this.cbThruBaseProvisions);
            this.groupBoxCabinet.Controls.Add(this.label8);
            this.groupBoxCabinet.Controls.Add(this.label147);
            this.groupBoxCabinet.Controls.Add(this.cbHingedServiceAccessFilters);
            this.groupBoxCabinet.Controls.Add(this.label7);
            this.groupBoxCabinet.Controls.Add(this.label146);
            this.groupBoxCabinet.Controls.Add(this.cbSupplyFanMotor);
            this.groupBoxCabinet.Controls.Add(this.label6);
            this.groupBoxCabinet.Controls.Add(this.label145);
            this.groupBoxCabinet.Controls.Add(this.cbFreshAirSelection);
            this.groupBoxCabinet.Controls.Add(this.label5);
            this.groupBoxCabinet.Controls.Add(this.label144);
            this.groupBoxCabinet.Controls.Add(this.cbServiceSequence);
            this.groupBoxCabinet.Controls.Add(this.label143);
            this.groupBoxCabinet.Controls.Add(this.label1);
            this.groupBoxCabinet.Controls.Add(this.label13);
            this.groupBoxCabinet.Controls.Add(this.cbVoltage);
            this.groupBoxCabinet.Controls.Add(this.cbAirflowConfiguration);
            this.groupBoxCabinet.Controls.Add(this.label18);
            this.groupBoxCabinet.Controls.Add(this.label142);
            this.groupBoxCabinet.Controls.Add(this.cbNotUsed);
            this.groupBoxCabinet.Controls.Add(this.label141);
            this.groupBoxCabinet.Controls.Add(this.cbMinorDesignSequence);
            this.groupBoxCabinet.Controls.Add(this.label21);
            this.groupBoxCabinet.Controls.Add(this.label140);
            this.groupBoxCabinet.Controls.Add(this.cbUnitControls);
            this.groupBoxCabinet.Controls.Add(this.label20);
            this.groupBoxCabinet.Controls.Add(this.cbHeatingCapacity);
            this.groupBoxCabinet.Controls.Add(this.label139);
            this.groupBoxCabinet.Controls.Add(this.cbCondenserCoilProtection);
            this.groupBoxCabinet.Controls.Add(this.label138);
            this.groupBoxCabinet.Controls.Add(this.label19);
            this.groupBoxCabinet.Controls.Add(this.label134);
            this.groupBoxCabinet.Controls.Add(this.label137);
            this.groupBoxCabinet.Controls.Add(this.cbMajorDesignSequence);
            this.groupBoxCabinet.Controls.Add(this.label135);
            this.groupBoxCabinet.Controls.Add(this.cbUnitType);
            this.groupBoxCabinet.Controls.Add(this.label136);
            this.groupBoxCabinet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxCabinet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBoxCabinet.Location = new System.Drawing.Point(15, 210);
            this.groupBoxCabinet.Name = "groupBoxCabinet";
            this.groupBoxCabinet.Size = new System.Drawing.Size(1055, 514);
            this.groupBoxCabinet.TabIndex = 594;
            this.groupBoxCabinet.TabStop = false;
            this.groupBoxCabinet.Text = "Inputs";
            // 
            // label168
            // 
            this.label168.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.Location = new System.Drawing.Point(517, 485);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(275, 20);
            this.label168.TabIndex = 98;
            this.label168.Text = "Pulley - Fan  (43)";
            this.label168.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPulleyFan
            // 
            this.cbPulleyFan.BackColor = System.Drawing.Color.White;
            this.cbPulleyFan.DropDownWidth = 300;
            this.cbPulleyFan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPulleyFan.ForeColor = System.Drawing.Color.Black;
            this.cbPulleyFan.FormattingEnabled = true;
            this.cbPulleyFan.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbPulleyFan.Location = new System.Drawing.Point(798, 485);
            this.cbPulleyFan.Name = "cbPulleyFan";
            this.cbPulleyFan.Size = new System.Drawing.Size(214, 21);
            this.cbPulleyFan.TabIndex = 99;
            // 
            // label167
            // 
            this.label167.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.Location = new System.Drawing.Point(517, 459);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(275, 20);
            this.label167.TabIndex = 96;
            this.label167.Text = "Belt (42)";
            this.label167.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbBelt
            // 
            this.cbBelt.BackColor = System.Drawing.Color.White;
            this.cbBelt.DropDownWidth = 300;
            this.cbBelt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBelt.ForeColor = System.Drawing.Color.Black;
            this.cbBelt.FormattingEnabled = true;
            this.cbBelt.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbBelt.Location = new System.Drawing.Point(798, 459);
            this.cbBelt.Name = "cbBelt";
            this.cbBelt.Size = new System.Drawing.Size(214, 21);
            this.cbBelt.TabIndex = 97;
            // 
            // label166
            // 
            this.label166.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.Location = new System.Drawing.Point(517, 433);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(275, 20);
            this.label166.TabIndex = 94;
            this.label166.Text = "Pulley - Driver (41)";
            this.label166.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPulleyDriver
            // 
            this.cbPulleyDriver.BackColor = System.Drawing.Color.White;
            this.cbPulleyDriver.DropDownWidth = 300;
            this.cbPulleyDriver.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPulleyDriver.ForeColor = System.Drawing.Color.Black;
            this.cbPulleyDriver.FormattingEnabled = true;
            this.cbPulleyDriver.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbPulleyDriver.Location = new System.Drawing.Point(798, 433);
            this.cbPulleyDriver.Name = "cbPulleyDriver";
            this.cbPulleyDriver.Size = new System.Drawing.Size(214, 21);
            this.cbPulleyDriver.TabIndex = 95;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(517, 407);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(275, 20);
            this.label23.TabIndex = 92;
            this.label23.Text = "Smoke Detector/Outdoor Air Monitoring  (40)";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSmokeDectectorOutdoorAirMon
            // 
            this.cbSmokeDectectorOutdoorAirMon.BackColor = System.Drawing.Color.White;
            this.cbSmokeDectectorOutdoorAirMon.DropDownWidth = 300;
            this.cbSmokeDectectorOutdoorAirMon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSmokeDectectorOutdoorAirMon.ForeColor = System.Drawing.Color.Black;
            this.cbSmokeDectectorOutdoorAirMon.FormattingEnabled = true;
            this.cbSmokeDectectorOutdoorAirMon.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbSmokeDectectorOutdoorAirMon.Location = new System.Drawing.Point(798, 407);
            this.cbSmokeDectectorOutdoorAirMon.Name = "cbSmokeDectectorOutdoorAirMon";
            this.cbSmokeDectectorOutdoorAirMon.Size = new System.Drawing.Size(214, 21);
            this.cbSmokeDectectorOutdoorAirMon.TabIndex = 93;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(517, 381);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(275, 20);
            this.label10.TabIndex = 90;
            this.label10.Text = "Supply Dampers  (39)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSupplyDampers
            // 
            this.cbSupplyDampers.BackColor = System.Drawing.Color.White;
            this.cbSupplyDampers.DropDownWidth = 300;
            this.cbSupplyDampers.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyDampers.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyDampers.FormattingEnabled = true;
            this.cbSupplyDampers.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbSupplyDampers.Location = new System.Drawing.Point(798, 381);
            this.cbSupplyDampers.Name = "cbSupplyDampers";
            this.cbSupplyDampers.Size = new System.Drawing.Size(214, 21);
            this.cbSupplyDampers.TabIndex = 91;
            // 
            // cbCommunicationsOptions
            // 
            this.cbCommunicationsOptions.BackColor = System.Drawing.Color.White;
            this.cbCommunicationsOptions.DropDownWidth = 300;
            this.cbCommunicationsOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCommunicationsOptions.ForeColor = System.Drawing.Color.Black;
            this.cbCommunicationsOptions.FormattingEnabled = true;
            this.cbCommunicationsOptions.Items.AddRange(new object[] {
            "No Corrosive Package",
            "S/S Cabinet, Basepan, Eco Coated Coils",
            "S/S Cabinet, Basepan",
            "S/S Basepan, Eco Coated Coils",
            "S/S Coil Casing",
            "S/S Interior Casing",
            "Eco Coated Coils"});
            this.cbCommunicationsOptions.Location = new System.Drawing.Point(298, 459);
            this.cbCommunicationsOptions.Name = "cbCommunicationsOptions";
            this.cbCommunicationsOptions.Size = new System.Drawing.Size(210, 21);
            this.cbCommunicationsOptions.TabIndex = 89;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(5, 459);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(284, 20);
            this.label22.TabIndex = 88;
            this.label22.Text = "Communications Options  (21)";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(517, 355);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(275, 20);
            this.label17.TabIndex = 86;
            this.label17.Text = "Energy Recovery Options  (38)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEnergyRecoveryOptions
            // 
            this.cbEnergyRecoveryOptions.BackColor = System.Drawing.Color.White;
            this.cbEnergyRecoveryOptions.DropDownWidth = 300;
            this.cbEnergyRecoveryOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEnergyRecoveryOptions.ForeColor = System.Drawing.Color.Black;
            this.cbEnergyRecoveryOptions.FormattingEnabled = true;
            this.cbEnergyRecoveryOptions.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbEnergyRecoveryOptions.Location = new System.Drawing.Point(798, 355);
            this.cbEnergyRecoveryOptions.Name = "cbEnergyRecoveryOptions";
            this.cbEnergyRecoveryOptions.Size = new System.Drawing.Size(214, 21);
            this.cbEnergyRecoveryOptions.TabIndex = 87;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(517, 329);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(275, 20);
            this.label16.TabIndex = 84;
            this.label16.Text = "Energy Recovery Wheel  (37)";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEnergyRecoveryWheel
            // 
            this.cbEnergyRecoveryWheel.BackColor = System.Drawing.Color.White;
            this.cbEnergyRecoveryWheel.DropDownWidth = 300;
            this.cbEnergyRecoveryWheel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEnergyRecoveryWheel.ForeColor = System.Drawing.Color.Black;
            this.cbEnergyRecoveryWheel.FormattingEnabled = true;
            this.cbEnergyRecoveryWheel.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbEnergyRecoveryWheel.Location = new System.Drawing.Point(798, 329);
            this.cbEnergyRecoveryWheel.Name = "cbEnergyRecoveryWheel";
            this.cbEnergyRecoveryWheel.Size = new System.Drawing.Size(214, 21);
            this.cbEnergyRecoveryWheel.TabIndex = 85;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(517, 303);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(275, 20);
            this.label15.TabIndex = 82;
            this.label15.Text = "Exhaust Dampers  (36)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustDampers
            // 
            this.cbExhaustDampers.BackColor = System.Drawing.Color.White;
            this.cbExhaustDampers.DropDownWidth = 300;
            this.cbExhaustDampers.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustDampers.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustDampers.FormattingEnabled = true;
            this.cbExhaustDampers.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbExhaustDampers.Location = new System.Drawing.Point(798, 303);
            this.cbExhaustDampers.Name = "cbExhaustDampers";
            this.cbExhaustDampers.Size = new System.Drawing.Size(214, 21);
            this.cbExhaustDampers.TabIndex = 83;
            // 
            // cbConvenienceOutletOptions
            // 
            this.cbConvenienceOutletOptions.BackColor = System.Drawing.Color.White;
            this.cbConvenienceOutletOptions.DropDownWidth = 300;
            this.cbConvenienceOutletOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConvenienceOutletOptions.ForeColor = System.Drawing.Color.Black;
            this.cbConvenienceOutletOptions.FormattingEnabled = true;
            this.cbConvenienceOutletOptions.Location = new System.Drawing.Point(298, 433);
            this.cbConvenienceOutletOptions.Name = "cbConvenienceOutletOptions";
            this.cbConvenienceOutletOptions.Size = new System.Drawing.Size(210, 21);
            this.cbConvenienceOutletOptions.TabIndex = 81;
            // 
            // cbDisconnectSwitch
            // 
            this.cbDisconnectSwitch.BackColor = System.Drawing.Color.White;
            this.cbDisconnectSwitch.DropDownWidth = 300;
            this.cbDisconnectSwitch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDisconnectSwitch.ForeColor = System.Drawing.Color.Black;
            this.cbDisconnectSwitch.FormattingEnabled = true;
            this.cbDisconnectSwitch.Location = new System.Drawing.Point(298, 407);
            this.cbDisconnectSwitch.Name = "cbDisconnectSwitch";
            this.cbDisconnectSwitch.Size = new System.Drawing.Size(210, 21);
            this.cbDisconnectSwitch.TabIndex = 80;
            // 
            // cbEfficiency
            // 
            this.cbEfficiency.BackColor = System.Drawing.Color.White;
            this.cbEfficiency.DropDownWidth = 300;
            this.cbEfficiency.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEfficiency.ForeColor = System.Drawing.Color.Black;
            this.cbEfficiency.FormattingEnabled = true;
            this.cbEfficiency.Items.AddRange(new object[] {
            "Rev4",
            "Rev5",
            "Heat Pump"});
            this.cbEfficiency.Location = new System.Drawing.Point(298, 43);
            this.cbEfficiency.Name = "cbEfficiency";
            this.cbEfficiency.Size = new System.Drawing.Size(210, 21);
            this.cbEfficiency.TabIndex = 78;
            // 
            // label133
            // 
            this.label133.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.Location = new System.Drawing.Point(5, 43);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(284, 20);
            this.label133.TabIndex = 77;
            this.label133.Text = "Efficiency (2)";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCoolingCapacity
            // 
            this.cbCoolingCapacity.BackColor = System.Drawing.Color.White;
            this.cbCoolingCapacity.DropDownWidth = 300;
            this.cbCoolingCapacity.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCoolingCapacity.ForeColor = System.Drawing.Color.Black;
            this.cbCoolingCapacity.FormattingEnabled = true;
            this.cbCoolingCapacity.Items.AddRange(new object[] {
            "Vertical Discharge/Vertical Return",
            "Vertical Discharge/ Horizontal Return",
            "Horizontal Discharge/ Vertical Return",
            "Horizontal Discharge/ Horizontal Return"});
            this.cbCoolingCapacity.Location = new System.Drawing.Point(298, 95);
            this.cbCoolingCapacity.Name = "cbCoolingCapacity";
            this.cbCoolingCapacity.Size = new System.Drawing.Size(210, 21);
            this.cbCoolingCapacity.TabIndex = 76;
            // 
            // label132
            // 
            this.label132.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(5, 95);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(284, 20);
            this.label132.TabIndex = 75;
            this.label132.Text = "Normal Gross Cooling Capacity Mbh  (456)";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label159
            // 
            this.label159.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(517, 277);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(275, 20);
            this.label159.TabIndex = 73;
            this.label159.Text = "Exhaust Fan Size  (35)";
            this.label159.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustFanSize
            // 
            this.cbExhaustFanSize.BackColor = System.Drawing.Color.White;
            this.cbExhaustFanSize.DropDownWidth = 300;
            this.cbExhaustFanSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustFanSize.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustFanSize.FormattingEnabled = true;
            this.cbExhaustFanSize.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbExhaustFanSize.Location = new System.Drawing.Point(798, 277);
            this.cbExhaustFanSize.Name = "cbExhaustFanSize";
            this.cbExhaustFanSize.Size = new System.Drawing.Size(214, 21);
            this.cbExhaustFanSize.TabIndex = 74;
            // 
            // label158
            // 
            this.label158.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.Location = new System.Drawing.Point(517, 251);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(275, 20);
            this.label158.TabIndex = 71;
            this.label158.Text = "Exhaust Fan Motor Type  (34)";
            this.label158.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustFanMotorType
            // 
            this.cbExhaustFanMotorType.BackColor = System.Drawing.Color.White;
            this.cbExhaustFanMotorType.DropDownWidth = 300;
            this.cbExhaustFanMotorType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustFanMotorType.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustFanMotorType.FormattingEnabled = true;
            this.cbExhaustFanMotorType.Items.AddRange(new object[] {
            "No Hailguards",
            "Hailguards"});
            this.cbExhaustFanMotorType.Location = new System.Drawing.Point(798, 251);
            this.cbExhaustFanMotorType.Name = "cbExhaustFanMotorType";
            this.cbExhaustFanMotorType.Size = new System.Drawing.Size(214, 21);
            this.cbExhaustFanMotorType.TabIndex = 72;
            // 
            // label157
            // 
            this.label157.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.Location = new System.Drawing.Point(517, 225);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(275, 20);
            this.label157.TabIndex = 69;
            this.label157.Text = "Exhaust Fan Motor  (33)";
            this.label157.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustFanMotor
            // 
            this.cbExhaustFanMotor.BackColor = System.Drawing.Color.White;
            this.cbExhaustFanMotor.DropDownWidth = 300;
            this.cbExhaustFanMotor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustFanMotor.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustFanMotor.FormattingEnabled = true;
            this.cbExhaustFanMotor.Items.AddRange(new object[] {
            "No Airflow Monitoring",
            "Airflow Monitoring - IFM Fan Piezo Ring",
            "Airflow Monitoring - IFM with Display",
            "Airflow Monitoring - PE with Display",
            "Airflow Monitoring - Outdoor Air",
            "Airflow Monitoring - Outdoor Air with Display"});
            this.cbExhaustFanMotor.Location = new System.Drawing.Point(798, 225);
            this.cbExhaustFanMotor.Name = "cbExhaustFanMotor";
            this.cbExhaustFanMotor.Size = new System.Drawing.Size(214, 21);
            this.cbExhaustFanMotor.TabIndex = 70;
            // 
            // label156
            // 
            this.label156.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(517, 199);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(275, 20);
            this.label156.TabIndex = 67;
            this.label156.Text = "PreHeater Option  (32)";
            this.label156.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPreHeaterOption
            // 
            this.cbPreHeaterOption.BackColor = System.Drawing.Color.White;
            this.cbPreHeaterOption.DropDownWidth = 300;
            this.cbPreHeaterOption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPreHeaterOption.ForeColor = System.Drawing.Color.Black;
            this.cbPreHeaterOption.FormattingEnabled = true;
            this.cbPreHeaterOption.Items.AddRange(new object[] {
            "Non-Fused Disconnect",
            "Fused Disconnect Switch",
            "Non-Fused Disconnect w/Convenience Outlet",
            "Fused Disconnect Switch w/Convenience Outlet",
            "Dual Point Power"});
            this.cbPreHeaterOption.Location = new System.Drawing.Point(798, 199);
            this.cbPreHeaterOption.Name = "cbPreHeaterOption";
            this.cbPreHeaterOption.Size = new System.Drawing.Size(214, 21);
            this.cbPreHeaterOption.TabIndex = 68;
            // 
            // label155
            // 
            this.label155.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.Location = new System.Drawing.Point(517, 173);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(275, 20);
            this.label155.TabIndex = 65;
            this.label155.Text = "Advanced Unit Controls  (31)";
            this.label155.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAdvancedUnitControls
            // 
            this.cbAdvancedUnitControls.BackColor = System.Drawing.Color.White;
            this.cbAdvancedUnitControls.DropDownWidth = 300;
            this.cbAdvancedUnitControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAdvancedUnitControls.ForeColor = System.Drawing.Color.Black;
            this.cbAdvancedUnitControls.FormattingEnabled = true;
            this.cbAdvancedUnitControls.Items.AddRange(new object[] {
            "No Smoke Detector",
            "Supply Smoke Detector",
            "Return Smoke Detector",
            "Supply & Return Detector"});
            this.cbAdvancedUnitControls.Location = new System.Drawing.Point(798, 173);
            this.cbAdvancedUnitControls.Name = "cbAdvancedUnitControls";
            this.cbAdvancedUnitControls.Size = new System.Drawing.Size(214, 21);
            this.cbAdvancedUnitControls.TabIndex = 66;
            // 
            // label154
            // 
            this.label154.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.Location = new System.Drawing.Point(517, 121);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(275, 20);
            this.label154.TabIndex = 63;
            this.label154.Text = "Short Circuit Current Rating  (28)";
            this.label154.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbShortCircuitCurrentRating
            // 
            this.cbShortCircuitCurrentRating.BackColor = System.Drawing.Color.White;
            this.cbShortCircuitCurrentRating.DropDownWidth = 300;
            this.cbShortCircuitCurrentRating.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShortCircuitCurrentRating.ForeColor = System.Drawing.Color.Black;
            this.cbShortCircuitCurrentRating.FormattingEnabled = true;
            this.cbShortCircuitCurrentRating.Items.AddRange(new object[] {
            "100 % OA 2-Position Damper",
            "100 % OA 2-Position Damper w/RA 2-Position Damper",
            "Modulating Mixed Air Damper"});
            this.cbShortCircuitCurrentRating.Location = new System.Drawing.Point(798, 121);
            this.cbShortCircuitCurrentRating.Name = "cbShortCircuitCurrentRating";
            this.cbShortCircuitCurrentRating.Size = new System.Drawing.Size(214, 21);
            this.cbShortCircuitCurrentRating.TabIndex = 64;
            // 
            // label153
            // 
            this.label153.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.Location = new System.Drawing.Point(517, 95);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(275, 20);
            this.label153.TabIndex = 61;
            this.label153.Text = "Unit Hardware Enhancements  (27)";
            this.label153.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitHardwareEnhancements
            // 
            this.cbUnitHardwareEnhancements.BackColor = System.Drawing.Color.White;
            this.cbUnitHardwareEnhancements.DropDownWidth = 300;
            this.cbUnitHardwareEnhancements.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUnitHardwareEnhancements.ForeColor = System.Drawing.Color.Black;
            this.cbUnitHardwareEnhancements.FormattingEnabled = true;
            this.cbUnitHardwareEnhancements.Items.AddRange(new object[] {
            "No ERV",
            "3014C",
            "3622C",
            "4136C",
            "4634C",
            "5856C",
            "6488C",
            "6876C",
            "74122C"});
            this.cbUnitHardwareEnhancements.Location = new System.Drawing.Point(798, 95);
            this.cbUnitHardwareEnhancements.Name = "cbUnitHardwareEnhancements";
            this.cbUnitHardwareEnhancements.Size = new System.Drawing.Size(214, 21);
            this.cbUnitHardwareEnhancements.TabIndex = 62;
            // 
            // label152
            // 
            this.label152.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.Location = new System.Drawing.Point(517, 69);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(275, 20);
            this.label152.TabIndex = 59;
            this.label152.Text = " System Monitoring Controls  (26)";
            this.label152.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSystemMonitoringControls
            // 
            this.cbSystemMonitoringControls.BackColor = System.Drawing.Color.White;
            this.cbSystemMonitoringControls.DropDownWidth = 300;
            this.cbSystemMonitoringControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSystemMonitoringControls.ForeColor = System.Drawing.Color.Black;
            this.cbSystemMonitoringControls.FormattingEnabled = true;
            this.cbSystemMonitoringControls.Items.AddRange(new object[] {
            "No ERV",
            "ERV-Composite Contruction",
            "ERV-Composite Contruction with Frost Protection",
            "ERV-Composite Contruction with Bypass",
            "ERV-Composite Contruction with Frost Protection & Bypass",
            "ERV-Aluminum Contruction",
            "ERV-Aluminum Contruction with Frost Protection",
            "ERV-Aluminum Contruction with Bypass",
            "ERV-Aluminum Contruction with Frost Protection & Bypass"});
            this.cbSystemMonitoringControls.Location = new System.Drawing.Point(798, 69);
            this.cbSystemMonitoringControls.Name = "cbSystemMonitoringControls";
            this.cbSystemMonitoringControls.Size = new System.Drawing.Size(214, 21);
            this.cbSystemMonitoringControls.TabIndex = 60;
            // 
            // label151
            // 
            this.label151.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.Location = new System.Drawing.Point(517, 43);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(275, 20);
            this.label151.TabIndex = 57;
            this.label151.Text = "Trane System Monitoring Controls  (25)";
            this.label151.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbTraneSystemMonitoringControls
            // 
            this.cbTraneSystemMonitoringControls.BackColor = System.Drawing.Color.White;
            this.cbTraneSystemMonitoringControls.DropDownWidth = 300;
            this.cbTraneSystemMonitoringControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTraneSystemMonitoringControls.ForeColor = System.Drawing.Color.Black;
            this.cbTraneSystemMonitoringControls.FormattingEnabled = true;
            this.cbTraneSystemMonitoringControls.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbTraneSystemMonitoringControls.Location = new System.Drawing.Point(798, 43);
            this.cbTraneSystemMonitoringControls.Name = "cbTraneSystemMonitoringControls";
            this.cbTraneSystemMonitoringControls.Size = new System.Drawing.Size(214, 21);
            this.cbTraneSystemMonitoringControls.TabIndex = 58;
            // 
            // cbSmokeDetector
            // 
            this.cbSmokeDetector.BackColor = System.Drawing.Color.White;
            this.cbSmokeDetector.DropDownWidth = 300;
            this.cbSmokeDetector.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSmokeDetector.ForeColor = System.Drawing.Color.Black;
            this.cbSmokeDetector.FormattingEnabled = true;
            this.cbSmokeDetector.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbSmokeDetector.Location = new System.Drawing.Point(798, 17);
            this.cbSmokeDetector.Name = "cbSmokeDetector";
            this.cbSmokeDetector.Size = new System.Drawing.Size(214, 21);
            this.cbSmokeDetector.TabIndex = 56;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(517, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(275, 20);
            this.label14.TabIndex = 55;
            this.label14.Text = "Smoke Detector  (24)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbRefrigerationControls
            // 
            this.cbRefrigerationControls.BackColor = System.Drawing.Color.White;
            this.cbRefrigerationControls.DropDownWidth = 300;
            this.cbRefrigerationControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRefrigerationControls.ForeColor = System.Drawing.Color.Black;
            this.cbRefrigerationControls.FormattingEnabled = true;
            this.cbRefrigerationControls.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbRefrigerationControls.Location = new System.Drawing.Point(298, 511);
            this.cbRefrigerationControls.Name = "cbRefrigerationControls";
            this.cbRefrigerationControls.Size = new System.Drawing.Size(210, 21);
            this.cbRefrigerationControls.TabIndex = 54;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(5, 511);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(284, 20);
            this.label12.TabIndex = 53;
            this.label12.Text = "Refrigeration Controls  (23)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbRefrigerationSystemOptions
            // 
            this.cbRefrigerationSystemOptions.BackColor = System.Drawing.Color.White;
            this.cbRefrigerationSystemOptions.DropDownWidth = 300;
            this.cbRefrigerationSystemOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRefrigerationSystemOptions.ForeColor = System.Drawing.Color.Black;
            this.cbRefrigerationSystemOptions.FormattingEnabled = true;
            this.cbRefrigerationSystemOptions.Items.AddRange(new object[] {
            "Non DDC - Electromechanical",
            "Trane - Outdoor Air Control w/LON Read-Write w/Display",
            "Trane - Space Control w/LON Read-Write w/Display",
            "Trane - Outdoor Air Control w/BACNET (No Display)",
            "Trane - Space Control w/BACNET  (No Display)",
            "Trane - Discharge Air Control w/BACNET  (No Display)",
            "Trane - Outdoor Air Control w/BACNET  w/Display",
            "Trane - Space Control w/BACNET w/Display",
            "Trane - Discharge Air Control w/BACNET w/Display"});
            this.cbRefrigerationSystemOptions.Location = new System.Drawing.Point(298, 485);
            this.cbRefrigerationSystemOptions.Name = "cbRefrigerationSystemOptions";
            this.cbRefrigerationSystemOptions.Size = new System.Drawing.Size(210, 21);
            this.cbRefrigerationSystemOptions.TabIndex = 52;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 485);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(284, 20);
            this.label11.TabIndex = 51;
            this.label11.Text = "Refrigeration System Options  (22)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 433);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(284, 23);
            this.label9.TabIndex = 44;
            this.label9.Text = "Convenience Outlet Option  (20)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelInputsHeatCapacityLabel
            // 
            this.labelInputsHeatCapacityLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInputsHeatCapacityLabel.Location = new System.Drawing.Point(5, 407);
            this.labelInputsHeatCapacityLabel.Name = "labelInputsHeatCapacityLabel";
            this.labelInputsHeatCapacityLabel.Size = new System.Drawing.Size(284, 23);
            this.labelInputsHeatCapacityLabel.TabIndex = 40;
            this.labelInputsHeatCapacityLabel.Text = "Disconnect Switch/Thru the Base Provisions  (19)";
            this.labelInputsHeatCapacityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbThruBaseProvisions
            // 
            this.cbThruBaseProvisions.BackColor = System.Drawing.Color.White;
            this.cbThruBaseProvisions.DropDownWidth = 300;
            this.cbThruBaseProvisions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbThruBaseProvisions.ForeColor = System.Drawing.Color.Black;
            this.cbThruBaseProvisions.FormattingEnabled = true;
            this.cbThruBaseProvisions.Items.AddRange(new object[] {
            "No Heat",
            "Natural Gas",
            "Propane",
            "Electric - Open Coil",
            "Electric - Sheathed Coil",
            "Hot Water",
            "Steam"});
            this.cbThruBaseProvisions.Location = new System.Drawing.Point(298, 381);
            this.cbThruBaseProvisions.Name = "cbThruBaseProvisions";
            this.cbThruBaseProvisions.Size = new System.Drawing.Size(210, 21);
            this.cbThruBaseProvisions.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 381);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(284, 23);
            this.label8.TabIndex = 38;
            this.label8.Text = "Thru The Base Provisions  (18)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label147
            // 
            this.label147.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.Location = new System.Drawing.Point(5, 329);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(284, 20);
            this.label147.TabIndex = 36;
            this.label147.Text = "Hinged Service Access/Filters  (16)";
            this.label147.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHingedServiceAccessFilters
            // 
            this.cbHingedServiceAccessFilters.BackColor = System.Drawing.Color.White;
            this.cbHingedServiceAccessFilters.DropDownWidth = 300;
            this.cbHingedServiceAccessFilters.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHingedServiceAccessFilters.ForeColor = System.Drawing.Color.Black;
            this.cbHingedServiceAccessFilters.FormattingEnabled = true;
            this.cbHingedServiceAccessFilters.Items.AddRange(new object[] {
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbHingedServiceAccessFilters.Location = new System.Drawing.Point(298, 329);
            this.cbHingedServiceAccessFilters.Name = "cbHingedServiceAccessFilters";
            this.cbHingedServiceAccessFilters.Size = new System.Drawing.Size(210, 21);
            this.cbHingedServiceAccessFilters.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 330);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(284, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Indoor Fan Motor HP (18)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label146
            // 
            this.label146.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.Location = new System.Drawing.Point(5, 303);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(284, 20);
            this.label146.TabIndex = 34;
            this.label146.Text = "Supply Fan/Drive Type/Motor  (15)";
            this.label146.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSupplyFanMotor
            // 
            this.cbSupplyFanMotor.BackColor = System.Drawing.Color.White;
            this.cbSupplyFanMotor.DropDownWidth = 300;
            this.cbSupplyFanMotor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyFanMotor.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyFanMotor.FormattingEnabled = true;
            this.cbSupplyFanMotor.Items.AddRange(new object[] {
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbSupplyFanMotor.Location = new System.Drawing.Point(298, 303);
            this.cbSupplyFanMotor.Name = "cbSupplyFanMotor";
            this.cbSupplyFanMotor.Size = new System.Drawing.Size(210, 21);
            this.cbSupplyFanMotor.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 304);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(284, 20);
            this.label6.TabIndex = 34;
            this.label6.Text = "Indoor Fan Wheel (17)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label145
            // 
            this.label145.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(5, 277);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(284, 20);
            this.label145.TabIndex = 32;
            this.label145.Text = "Fresh Air Selection  (14)";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbFreshAirSelection
            // 
            this.cbFreshAirSelection.BackColor = System.Drawing.Color.White;
            this.cbFreshAirSelection.DropDownWidth = 300;
            this.cbFreshAirSelection.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFreshAirSelection.ForeColor = System.Drawing.Color.Black;
            this.cbFreshAirSelection.FormattingEnabled = true;
            this.cbFreshAirSelection.Items.AddRange(new object[] {
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbFreshAirSelection.Location = new System.Drawing.Point(298, 277);
            this.cbFreshAirSelection.Name = "cbFreshAirSelection";
            this.cbFreshAirSelection.Size = new System.Drawing.Size(210, 21);
            this.cbFreshAirSelection.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(284, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Indoor Fan Motor (IFM) (16)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(5, 251);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(284, 20);
            this.label144.TabIndex = 30;
            this.label144.Text = "Service Sequence  (12,13)";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbServiceSequence
            // 
            this.cbServiceSequence.BackColor = System.Drawing.Color.White;
            this.cbServiceSequence.DropDownWidth = 300;
            this.cbServiceSequence.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbServiceSequence.ForeColor = System.Drawing.Color.Black;
            this.cbServiceSequence.FormattingEnabled = true;
            this.cbServiceSequence.Items.AddRange(new object[] {
            "No RCC Valve",
            "RCC Valve on 1st Circuit",
            "RCC Valve on 1st Circuit & 2nd Circuit",
            "ERCC Valve on 1st Circuit",
            "ERCC Valve on 1st Circuit & 2nd Circuit",
            "HGBP Valve on 1st Circuit",
            "HGBP Valve on 1st Circuit & 2nd Circuit"});
            this.cbServiceSequence.Location = new System.Drawing.Point(298, 251);
            this.cbServiceSequence.Name = "cbServiceSequence";
            this.cbServiceSequence.Size = new System.Drawing.Size(210, 21);
            this.cbServiceSequence.TabIndex = 31;
            // 
            // label143
            // 
            this.label143.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(5, 147);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(284, 20);
            this.label143.TabIndex = 29;
            this.label143.Text = "Voltage  (8)";
            this.label143.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 252);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 20);
            this.label1.TabIndex = 30;
            this.label1.Text = "Refrigerant Capacity Control (15)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(5, 148);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(284, 20);
            this.label13.TabIndex = 29;
            this.label13.Text = "Evaporator Type (11)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbVoltage
            // 
            this.cbVoltage.BackColor = System.Drawing.Color.White;
            this.cbVoltage.DropDownWidth = 300;
            this.cbVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVoltage.ForeColor = System.Drawing.Color.Black;
            this.cbVoltage.FormattingEnabled = true;
            this.cbVoltage.Items.AddRange(new object[] {
            "No Cooling",
            "DX 3-Row",
            "DX 4-Row",
            "DX 4-Row Interlaced",
            "DX 6-Row Interlaced",
            "DX 8-Row",
            "Glycol/Chilled Water Coil"});
            this.cbVoltage.Location = new System.Drawing.Point(298, 147);
            this.cbVoltage.Name = "cbVoltage";
            this.cbVoltage.Size = new System.Drawing.Size(210, 21);
            this.cbVoltage.TabIndex = 28;
            // 
            // cbAirflowConfiguration
            // 
            this.cbAirflowConfiguration.BackColor = System.Drawing.Color.White;
            this.cbAirflowConfiguration.DropDownWidth = 300;
            this.cbAirflowConfiguration.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAirflowConfiguration.ForeColor = System.Drawing.Color.Black;
            this.cbAirflowConfiguration.FormattingEnabled = true;
            this.cbAirflowConfiguration.Items.AddRange(new object[] {
            "No Cooling",
            "5 Tons High Efficiency",
            "6 Tons High Efficiency",
            "7 Tons High Efficiency",
            "8 Tons High Efficiency",
            "10 Tons High Efficiency",
            "12 Tons High Efficiency",
            "15 Tons High Efficiency",
            "17 Tons High Efficiency",
            "20 Tons High Efficiency",
            "22 Tons High Efficiency",
            "25 Tons High Efficiency",
            "30 Tons High Efficiency",
            "35 Tons High Efficiency",
            "40 Tons High Efficiency",
            "45 Tons High Efficiency",
            "50 Tons High Efficiency",
            "54 Tons High Efficiency"});
            this.cbAirflowConfiguration.Location = new System.Drawing.Point(298, 69);
            this.cbAirflowConfiguration.Name = "cbAirflowConfiguration";
            this.cbAirflowConfiguration.Size = new System.Drawing.Size(210, 21);
            this.cbAirflowConfiguration.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(5, 69);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(284, 20);
            this.label18.TabIndex = 25;
            this.label18.Text = "Airflow Configuration (3)";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label142
            // 
            this.label142.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.Location = new System.Drawing.Point(517, 147);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(275, 20);
            this.label142.TabIndex = 21;
            this.label142.Text = "Not Used  (29,30)";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbNotUsed
            // 
            this.cbNotUsed.BackColor = System.Drawing.Color.White;
            this.cbNotUsed.DropDownWidth = 300;
            this.cbNotUsed.Enabled = false;
            this.cbNotUsed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNotUsed.ForeColor = System.Drawing.Color.Black;
            this.cbNotUsed.FormattingEnabled = true;
            this.cbNotUsed.Items.AddRange(new object[] {
            "Aluminum Mesh Intake Filters (ALM)",
            "MERV-8, 30%, and ALM",
            "MERV-13, 80%, and ALM",
            "MERV-14, 95%, and ALM",
            "Aluminum Mesh Intake Filters (ALM), w/UVC",
            "MERV-8, 30%, and ALM, w/UVC",
            "MERV-13, 80%, and ALM, w/UVC",
            "MERV-14, 95%, and ALM, w/UVC",
            "Aluminum Mesh Intake Filters (ALM), w/UVC & Electrostatic Filters",
            "MERV-8, 30%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-13, 80%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-14, 95%, and ALM, w/UVC & Electrostatic Filters",
            "Aluminum Mesh Intake Filters (ALM) & Electrostatic Filters",
            "MERV-8, 30%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM & Electrostatic Filters",
            "MERV-14, 95%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM w/TCACS",
            "MERV-14, 95%, and ALM w/TCACS",
            "Special Filter Options"});
            this.cbNotUsed.Location = new System.Drawing.Point(798, 147);
            this.cbNotUsed.Name = "cbNotUsed";
            this.cbNotUsed.Size = new System.Drawing.Size(214, 21);
            this.cbNotUsed.TabIndex = 22;
            // 
            // label141
            // 
            this.label141.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.Location = new System.Drawing.Point(5, 225);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(284, 20);
            this.label141.TabIndex = 17;
            this.label141.Text = "Minor Design Sequence (11)";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbMinorDesignSequence
            // 
            this.cbMinorDesignSequence.BackColor = System.Drawing.Color.White;
            this.cbMinorDesignSequence.DropDownWidth = 300;
            this.cbMinorDesignSequence.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMinorDesignSequence.ForeColor = System.Drawing.Color.Black;
            this.cbMinorDesignSequence.FormattingEnabled = true;
            this.cbMinorDesignSequence.Items.AddRange(new object[] {
            "No Condenser",
            "Air Cooled",
            "Air Cooled w/Head Pressure on/off control",
            "Water Source Heat Pump",
            "Air Cooled Fin & Tube w/ Head Pressure Variable Speed",
            "Air Cooled Micro Channel",
            "Air Cooled Micro Channel w/Head Pressure on/off control",
            "Air Cooled Micro Channel Variable Speed",
            "Water Cooled DX Condenser Copper/Nickel"});
            this.cbMinorDesignSequence.Location = new System.Drawing.Point(298, 225);
            this.cbMinorDesignSequence.Name = "cbMinorDesignSequence";
            this.cbMinorDesignSequence.Size = new System.Drawing.Size(210, 21);
            this.cbMinorDesignSequence.TabIndex = 18;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(5, 226);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(284, 20);
            this.label21.TabIndex = 17;
            this.label21.Text = "Condenser (14)";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label140
            // 
            this.label140.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.Location = new System.Drawing.Point(5, 173);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(284, 20);
            this.label140.TabIndex = 15;
            this.label140.Text = "Unit Controls  (9)";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitControls
            // 
            this.cbUnitControls.BackColor = System.Drawing.Color.White;
            this.cbUnitControls.DropDownWidth = 300;
            this.cbUnitControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUnitControls.ForeColor = System.Drawing.Color.Black;
            this.cbUnitControls.FormattingEnabled = true;
            this.cbUnitControls.Items.AddRange(new object[] {
            "No HGRH",
            "Modulating",
            "On/Off"});
            this.cbUnitControls.Location = new System.Drawing.Point(298, 173);
            this.cbUnitControls.Name = "cbUnitControls";
            this.cbUnitControls.Size = new System.Drawing.Size(210, 21);
            this.cbUnitControls.TabIndex = 16;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(5, 174);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(284, 20);
            this.label20.TabIndex = 15;
            this.label20.Text = "Hot Gas Reheat (12)";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHeatingCapacity
            // 
            this.cbHeatingCapacity.BackColor = System.Drawing.Color.White;
            this.cbHeatingCapacity.DropDownWidth = 300;
            this.cbHeatingCapacity.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatingCapacity.ForeColor = System.Drawing.Color.Black;
            this.cbHeatingCapacity.FormattingEnabled = true;
            this.cbHeatingCapacity.Items.AddRange(new object[] {
            "No Compressor",
            "Scroll Compressor",
            "Digital Scroll-1st Circuit Only",
            "Digital Scroll-1st Circuit & 2nd Circuit",
            "Variable Speed Scroll"});
            this.cbHeatingCapacity.Location = new System.Drawing.Point(298, 199);
            this.cbHeatingCapacity.Name = "cbHeatingCapacity";
            this.cbHeatingCapacity.Size = new System.Drawing.Size(210, 21);
            this.cbHeatingCapacity.TabIndex = 14;
            // 
            // label139
            // 
            this.label139.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.Location = new System.Drawing.Point(5, 199);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(284, 20);
            this.label139.TabIndex = 13;
            this.label139.Text = "Heating Capacity  (10)";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCondenserCoilProtection
            // 
            this.cbCondenserCoilProtection.BackColor = System.Drawing.Color.White;
            this.cbCondenserCoilProtection.DropDownWidth = 300;
            this.cbCondenserCoilProtection.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCondenserCoilProtection.ForeColor = System.Drawing.Color.Black;
            this.cbCondenserCoilProtection.FormattingEnabled = true;
            this.cbCondenserCoilProtection.Items.AddRange(new object[] {
            "No Heat",
            "Indirect - Fired (IF)",
            "Direct - Fired (DF)",
            "Electric - 2 Stage",
            "Electric - SCR Modulating",
            "Dual Fuel (PRI-DF/SEC-IF)",
            "Dual Fuel (PRI-DF/SEC-ELEC)",
            "Dual Fuel (PRI-IF/SEC-ELEC)",
            "Dual Fuel (PRI-ELEC/SEC-ELEC)",
            "Hot Water",
            "Steam"});
            this.cbCondenserCoilProtection.Location = new System.Drawing.Point(298, 355);
            this.cbCondenserCoilProtection.Name = "cbCondenserCoilProtection";
            this.cbCondenserCoilProtection.Size = new System.Drawing.Size(210, 21);
            this.cbCondenserCoilProtection.TabIndex = 14;
            // 
            // label138
            // 
            this.label138.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.Location = new System.Drawing.Point(5, 355);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(284, 23);
            this.label138.TabIndex = 13;
            this.label138.Text = " Condenser Coil Protection  (17)";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(5, 200);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(284, 20);
            this.label19.TabIndex = 13;
            this.label19.Text = "Compressor (13)";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label134
            // 
            this.label134.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(5, 356);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(284, 23);
            this.label134.TabIndex = 13;
            this.label134.Text = "Heat Type (20)";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label137
            // 
            this.label137.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.Location = new System.Drawing.Point(5, 121);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(284, 20);
            this.label137.TabIndex = 11;
            this.label137.Text = "Major Design Sequence  (7)";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbMajorDesignSequence
            // 
            this.cbMajorDesignSequence.BackColor = System.Drawing.Color.White;
            this.cbMajorDesignSequence.DropDownWidth = 300;
            this.cbMajorDesignSequence.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMajorDesignSequence.ForeColor = System.Drawing.Color.Black;
            this.cbMajorDesignSequence.FormattingEnabled = true;
            this.cbMajorDesignSequence.Items.AddRange(new object[] {
            "115/60/1",
            "208-230/60/1",
            "208-230/60/3",
            "460/60/3",
            "575/60/3"});
            this.cbMajorDesignSequence.Location = new System.Drawing.Point(298, 121);
            this.cbMajorDesignSequence.Name = "cbMajorDesignSequence";
            this.cbMajorDesignSequence.Size = new System.Drawing.Size(210, 21);
            this.cbMajorDesignSequence.TabIndex = 12;
            // 
            // label135
            // 
            this.label135.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.Location = new System.Drawing.Point(5, 122);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(284, 20);
            this.label135.TabIndex = 11;
            this.label135.Text = "Voltage/Phase (9)";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitType
            // 
            this.cbUnitType.BackColor = System.Drawing.Color.White;
            this.cbUnitType.DropDownWidth = 300;
            this.cbUnitType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUnitType.ForeColor = System.Drawing.Color.Black;
            this.cbUnitType.FormattingEnabled = true;
            this.cbUnitType.Items.AddRange(new object[] {
            "625 - 3,600 cfm",
            "1,500 - 9,000 cfm",
            "3,500 - 13,500 cfm"});
            this.cbUnitType.Location = new System.Drawing.Point(298, 17);
            this.cbUnitType.Name = "cbUnitType";
            this.cbUnitType.Size = new System.Drawing.Size(210, 21);
            this.cbUnitType.TabIndex = 1;
            // 
            // label136
            // 
            this.label136.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(5, 17);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(284, 20);
            this.label136.TabIndex = 0;
            this.label136.Text = "Unit Type (1)";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(330, 183);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(10, 15);
            this.label24.TabIndex = 597;
            this.label24.Text = "0";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(330, 168);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(10, 15);
            this.label28.TabIndex = 596;
            this.label28.Text = "1";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(330, 160);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(10, 15);
            this.label29.TabIndex = 595;
            this.label29.Text = "^";
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(475, 183);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(10, 15);
            this.label30.TabIndex = 600;
            this.label30.Text = "9";
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(475, 168);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(10, 15);
            this.label31.TabIndex = 599;
            this.label31.Text = "1";
            // 
            // label148
            // 
            this.label148.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.Black;
            this.label148.Location = new System.Drawing.Point(475, 160);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(10, 15);
            this.label148.TabIndex = 598;
            this.label148.Text = "^";
            // 
            // label149
            // 
            this.label149.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.Black;
            this.label149.Location = new System.Drawing.Point(651, 183);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(10, 15);
            this.label149.TabIndex = 603;
            this.label149.Text = "0";
            // 
            // label150
            // 
            this.label150.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.Black;
            this.label150.Location = new System.Drawing.Point(651, 168);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(10, 15);
            this.label150.TabIndex = 602;
            this.label150.Text = "3";
            // 
            // label160
            // 
            this.label160.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.ForeColor = System.Drawing.Color.Black;
            this.label160.Location = new System.Drawing.Point(651, 160);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(10, 15);
            this.label160.TabIndex = 601;
            this.label160.Text = "^";
            // 
            // label161
            // 
            this.label161.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.ForeColor = System.Drawing.Color.Black;
            this.label161.Location = new System.Drawing.Point(810, 183);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(10, 15);
            this.label161.TabIndex = 606;
            this.label161.Text = "0";
            // 
            // label162
            // 
            this.label162.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.ForeColor = System.Drawing.Color.Black;
            this.label162.Location = new System.Drawing.Point(810, 168);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(10, 15);
            this.label162.TabIndex = 605;
            this.label162.Text = "4";
            // 
            // label163
            // 
            this.label163.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.ForeColor = System.Drawing.Color.Black;
            this.label163.Location = new System.Drawing.Point(810, 160);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(10, 15);
            this.label163.TabIndex = 604;
            this.label163.Text = "^";
            // 
            // btnParseModelNo
            // 
            this.btnParseModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParseModelNo.ForeColor = System.Drawing.Color.Teal;
            this.btnParseModelNo.Location = new System.Drawing.Point(483, 62);
            this.btnParseModelNo.Name = "btnParseModelNo";
            this.btnParseModelNo.Size = new System.Drawing.Size(100, 40);
            this.btnParseModelNo.TabIndex = 612;
            this.btnParseModelNo.Text = "Parse ModelNo";
            this.btnParseModelNo.UseVisualStyleBackColor = true;
            this.btnParseModelNo.Visible = false;
            this.btnParseModelNo.Click += new System.EventHandler(this.btnParseModelNo_Click);
            // 
            // lbEnterModelNo
            // 
            this.lbEnterModelNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnterModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbEnterModelNo.Location = new System.Drawing.Point(9, 76);
            this.lbEnterModelNo.Name = "lbEnterModelNo";
            this.lbEnterModelNo.Size = new System.Drawing.Size(112, 23);
            this.lbEnterModelNo.TabIndex = 611;
            this.lbEnterModelNo.Text = "ModelNo: ";
            this.lbEnterModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbEnterModelNo.Visible = false;
            // 
            // txtModelNo
            // 
            this.txtModelNo.Location = new System.Drawing.Point(125, 77);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(344, 20);
            this.txtModelNo.TabIndex = 610;
            this.txtModelNo.TextChanged += new System.EventHandler(this.txtModelNo_TextChanged);
            // 
            // btnCreateBOM
            // 
            this.btnCreateBOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateBOM.ForeColor = System.Drawing.Color.Teal;
            this.btnCreateBOM.Location = new System.Drawing.Point(784, 735);
            this.btnCreateBOM.Name = "btnCreateBOM";
            this.btnCreateBOM.Size = new System.Drawing.Size(100, 47);
            this.btnCreateBOM.TabIndex = 609;
            this.btnCreateBOM.Text = "Create BOM";
            this.btnCreateBOM.UseVisualStyleBackColor = true;
            this.btnCreateBOM.Click += new System.EventHandler(this.btnCreateBOM_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Blue;
            this.btnPrint.Location = new System.Drawing.Point(635, 735);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 47);
            this.btnPrint.TabIndex = 608;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(930, 735);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 47);
            this.btnExit.TabIndex = 607;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label164
            // 
            this.label164.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label164.Location = new System.Drawing.Point(9, 20);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(112, 23);
            this.label164.TabIndex = 613;
            this.label164.Text = "Order Num: ";
            this.label164.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtOrderNum
            // 
            this.txtOrderNum.Location = new System.Drawing.Point(125, 21);
            this.txtOrderNum.Name = "txtOrderNum";
            this.txtOrderNum.Size = new System.Drawing.Size(89, 20);
            this.txtOrderNum.TabIndex = 612;
            // 
            // btnCreateSubmittal
            // 
            this.btnCreateSubmittal.Enabled = false;
            this.btnCreateSubmittal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateSubmittal.ForeColor = System.Drawing.Color.Teal;
            this.btnCreateSubmittal.Location = new System.Drawing.Point(482, 16);
            this.btnCreateSubmittal.Name = "btnCreateSubmittal";
            this.btnCreateSubmittal.Size = new System.Drawing.Size(100, 40);
            this.btnCreateSubmittal.TabIndex = 614;
            this.btnCreateSubmittal.Text = "Create Submittal";
            this.btnCreateSubmittal.UseVisualStyleBackColor = true;
            this.btnCreateSubmittal.Click += new System.EventHandler(this.btnCreateSubmittal_Click);
            // 
            // gbOrder_ModelNo
            // 
            this.gbOrder_ModelNo.Controls.Add(this.label165);
            this.gbOrder_ModelNo.Controls.Add(this.txtOrderLine);
            this.gbOrder_ModelNo.Controls.Add(this.btnGetOrderDetails);
            this.gbOrder_ModelNo.Controls.Add(this.lbEnterModelNo);
            this.gbOrder_ModelNo.Controls.Add(this.label164);
            this.gbOrder_ModelNo.Controls.Add(this.txtModelNo);
            this.gbOrder_ModelNo.Controls.Add(this.txtOrderNum);
            this.gbOrder_ModelNo.Controls.Add(this.btnParseModelNo);
            this.gbOrder_ModelNo.Controls.Add(this.btnCreateSubmittal);
            this.gbOrder_ModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOrder_ModelNo.Location = new System.Drawing.Point(15, 735);
            this.gbOrder_ModelNo.Name = "gbOrder_ModelNo";
            this.gbOrder_ModelNo.Size = new System.Drawing.Size(598, 111);
            this.gbOrder_ModelNo.TabIndex = 615;
            this.gbOrder_ModelNo.TabStop = false;
            this.gbOrder_ModelNo.Text = "Enter OrderNum/OrderLine Or Model No";
            // 
            // label165
            // 
            this.label165.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label165.Location = new System.Drawing.Point(9, 46);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(112, 23);
            this.label165.TabIndex = 618;
            this.label165.Text = "Order Line: ";
            this.label165.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtOrderLine
            // 
            this.txtOrderLine.Location = new System.Drawing.Point(125, 47);
            this.txtOrderLine.Name = "txtOrderLine";
            this.txtOrderLine.Size = new System.Drawing.Size(89, 20);
            this.txtOrderLine.TabIndex = 617;
            // 
            // btnGetOrderDetails
            // 
            this.btnGetOrderDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetOrderDetails.ForeColor = System.Drawing.Color.Teal;
            this.btnGetOrderDetails.Location = new System.Drawing.Point(232, 24);
            this.btnGetOrderDetails.Name = "btnGetOrderDetails";
            this.btnGetOrderDetails.Size = new System.Drawing.Size(100, 40);
            this.btnGetOrderDetails.TabIndex = 616;
            this.btnGetOrderDetails.Text = "Get OrderDetails";
            this.btnGetOrderDetails.UseVisualStyleBackColor = true;
            this.btnGetOrderDetails.Click += new System.EventHandler(this.btnGetOrderDetails_Click);
            // 
            // label169
            // 
            this.label169.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.ForeColor = System.Drawing.Color.Black;
            this.label169.Location = new System.Drawing.Point(826, 183);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(10, 15);
            this.label169.TabIndex = 618;
            this.label169.Text = "1";
            // 
            // label170
            // 
            this.label170.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.ForeColor = System.Drawing.Color.Black;
            this.label170.Location = new System.Drawing.Point(826, 168);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(10, 15);
            this.label170.TabIndex = 617;
            this.label170.Text = "4";
            // 
            // label171
            // 
            this.label171.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.Black;
            this.label171.Location = new System.Drawing.Point(826, 160);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(10, 15);
            this.label171.TabIndex = 616;
            this.label171.Text = "^";
            // 
            // label172
            // 
            this.label172.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.ForeColor = System.Drawing.Color.Black;
            this.label172.Location = new System.Drawing.Point(843, 183);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(10, 15);
            this.label172.TabIndex = 621;
            this.label172.Text = "2";
            // 
            // label173
            // 
            this.label173.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.ForeColor = System.Drawing.Color.Black;
            this.label173.Location = new System.Drawing.Point(843, 168);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(10, 15);
            this.label173.TabIndex = 620;
            this.label173.Text = "4";
            // 
            // label174
            // 
            this.label174.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.ForeColor = System.Drawing.Color.Black;
            this.label174.Location = new System.Drawing.Point(843, 160);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(10, 15);
            this.label174.TabIndex = 619;
            this.label174.Text = "^";
            // 
            // label175
            // 
            this.label175.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.ForeColor = System.Drawing.Color.Black;
            this.label175.Location = new System.Drawing.Point(859, 183);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(10, 15);
            this.label175.TabIndex = 624;
            this.label175.Text = "3";
            // 
            // label176
            // 
            this.label176.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.ForeColor = System.Drawing.Color.Black;
            this.label176.Location = new System.Drawing.Point(859, 168);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(10, 15);
            this.label176.TabIndex = 623;
            this.label176.Text = "4";
            // 
            // label177
            // 
            this.label177.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.ForeColor = System.Drawing.Color.Black;
            this.label177.Location = new System.Drawing.Point(859, 160);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(10, 15);
            this.label177.TabIndex = 622;
            this.label177.Text = "^";
            // 
            // label178
            // 
            this.label178.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.ForeColor = System.Drawing.Color.Teal;
            this.label178.Location = new System.Drawing.Point(796, 47);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(140, 20);
            this.label178.TabIndex = 625;
            this.label178.Text = "BOM Creation By:";
            this.label178.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label179
            // 
            this.label179.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.ForeColor = System.Drawing.Color.Teal;
            this.label179.Location = new System.Drawing.Point(794, 87);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(140, 20);
            this.label179.TabIndex = 626;
            this.label179.Text = "Last Updated By:";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmMonitorModelNoConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1058, 749);
            this.Controls.Add(this.label179);
            this.Controls.Add(this.label178);
            this.Controls.Add(this.gbOrder_ModelNo);
            this.Controls.Add(this.label175);
            this.Controls.Add(this.label176);
            this.Controls.Add(this.label177);
            this.Controls.Add(this.label172);
            this.Controls.Add(this.label173);
            this.Controls.Add(this.label174);
            this.Controls.Add(this.label169);
            this.Controls.Add(this.label170);
            this.Controls.Add(this.label171);
            this.Controls.Add(this.btnCreateBOM);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label161);
            this.Controls.Add(this.label162);
            this.Controls.Add(this.label163);
            this.Controls.Add(this.label149);
            this.Controls.Add(this.label150);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label148);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.groupBoxCabinet);
            this.Controls.Add(this.lbLastUpdateBy);
            this.Controls.Add(this.lbBomCreateBy);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.lbLastUpdateDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label129);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.lbCompleteDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbProdStartDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbBOMCreationDate);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lbShipDate);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.lbOrderDate);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbJobNum);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.menuStripConfig);
            this.Name = "frmMonitorModelNoConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monitor ModelNo Configuration";
            this.menuStripConfig.ResumeLayout(false);
            this.menuStripConfig.PerformLayout();
            this.groupBoxCabinet.ResumeLayout(false);
            this.gbOrder_ModelNo.ResumeLayout(false);
            this.gbOrder_ModelNo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.Label lbLastUpdateBy;
        public System.Windows.Forms.Label lbBomCreateBy;
        public System.Windows.Forms.Label lbCustName;
        public System.Windows.Forms.Label lbLastUpdateDate;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label129;
        public System.Windows.Forms.Label label130;
        public System.Windows.Forms.Label label131;
        public System.Windows.Forms.Label label127;
        public System.Windows.Forms.Label label128;
        public System.Windows.Forms.Label label119;
        public System.Windows.Forms.Label label120;
        public System.Windows.Forms.Label label121;
        public System.Windows.Forms.Label label122;
        public System.Windows.Forms.Label label123;
        public System.Windows.Forms.Label label124;
        public System.Windows.Forms.Label label125;
        public System.Windows.Forms.Label label126;
        public System.Windows.Forms.Label label111;
        public System.Windows.Forms.Label label112;
        public System.Windows.Forms.Label label113;
        public System.Windows.Forms.Label label114;
        public System.Windows.Forms.Label label115;
        public System.Windows.Forms.Label label116;
        public System.Windows.Forms.Label label117;
        public System.Windows.Forms.Label label118;
        public System.Windows.Forms.Label label103;
        public System.Windows.Forms.Label label104;
        public System.Windows.Forms.Label label105;
        public System.Windows.Forms.Label label106;
        public System.Windows.Forms.Label label107;
        public System.Windows.Forms.Label label108;
        public System.Windows.Forms.Label label109;
        public System.Windows.Forms.Label label110;
        public System.Windows.Forms.Label label102;
        public System.Windows.Forms.Label label100;
        public System.Windows.Forms.Label label101;
        public System.Windows.Forms.Label label96;
        public System.Windows.Forms.Label label97;
        public System.Windows.Forms.Label label98;
        public System.Windows.Forms.Label label99;
        public System.Windows.Forms.Label label92;
        public System.Windows.Forms.Label label93;
        public System.Windows.Forms.Label label94;
        public System.Windows.Forms.Label label95;
        public System.Windows.Forms.Label label88;
        public System.Windows.Forms.Label label89;
        public System.Windows.Forms.Label label90;
        public System.Windows.Forms.Label label91;
        public System.Windows.Forms.Label label84;
        public System.Windows.Forms.Label label85;
        public System.Windows.Forms.Label label86;
        public System.Windows.Forms.Label label87;
        public System.Windows.Forms.Label label80;
        public System.Windows.Forms.Label label81;
        public System.Windows.Forms.Label label82;
        public System.Windows.Forms.Label label83;
        public System.Windows.Forms.Label label76;
        public System.Windows.Forms.Label label77;
        public System.Windows.Forms.Label label78;
        public System.Windows.Forms.Label label79;
        public System.Windows.Forms.Label label72;
        public System.Windows.Forms.Label label73;
        public System.Windows.Forms.Label label74;
        public System.Windows.Forms.Label label75;
        public System.Windows.Forms.Label label70;
        public System.Windows.Forms.Label label71;
        public System.Windows.Forms.Label label69;
        public System.Windows.Forms.Label label68;
        public System.Windows.Forms.Label label67;
        public System.Windows.Forms.Label label66;
        public System.Windows.Forms.Label label65;
        public System.Windows.Forms.Label label61;
        public System.Windows.Forms.Label label62;
        public System.Windows.Forms.Label label63;
        public System.Windows.Forms.Label label64;
        public System.Windows.Forms.Label label57;
        public System.Windows.Forms.Label label58;
        public System.Windows.Forms.Label label59;
        public System.Windows.Forms.Label label60;
        public System.Windows.Forms.Label label53;
        public System.Windows.Forms.Label label54;
        public System.Windows.Forms.Label label55;
        public System.Windows.Forms.Label label56;
        public System.Windows.Forms.Label label49;
        public System.Windows.Forms.Label label50;
        public System.Windows.Forms.Label label51;
        public System.Windows.Forms.Label label52;
        public System.Windows.Forms.Label label45;
        public System.Windows.Forms.Label label46;
        public System.Windows.Forms.Label label47;
        public System.Windows.Forms.Label label48;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label lbModelNo;
        public System.Windows.Forms.Label lbCompleteDate;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lbProdStartDate;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lbBOMCreationDate;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label lbShipDate;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label lbOrderDate;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.Label lbEnvironment;
        public System.Windows.Forms.GroupBox groupBoxCabinet;
        public System.Windows.Forms.ComboBox cbConvenienceOutletOptions;
        public System.Windows.Forms.ComboBox cbDisconnectSwitch;
        public System.Windows.Forms.ComboBox cbEfficiency;
        private System.Windows.Forms.Label label133;
        public System.Windows.Forms.ComboBox cbCoolingCapacity;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label159;
        public System.Windows.Forms.ComboBox cbExhaustFanSize;
        private System.Windows.Forms.Label label158;
        public System.Windows.Forms.ComboBox cbExhaustFanMotorType;
        private System.Windows.Forms.Label label157;
        public System.Windows.Forms.ComboBox cbExhaustFanMotor;
        private System.Windows.Forms.Label label156;
        public System.Windows.Forms.ComboBox cbPreHeaterOption;
        private System.Windows.Forms.Label label155;
        public System.Windows.Forms.ComboBox cbAdvancedUnitControls;
        private System.Windows.Forms.Label label154;
        public System.Windows.Forms.ComboBox cbShortCircuitCurrentRating;
        private System.Windows.Forms.Label label153;
        public System.Windows.Forms.ComboBox cbUnitHardwareEnhancements;
        private System.Windows.Forms.Label label152;
        public System.Windows.Forms.ComboBox cbSystemMonitoringControls;
        private System.Windows.Forms.Label label151;
        public System.Windows.Forms.ComboBox cbTraneSystemMonitoringControls;
        public System.Windows.Forms.ComboBox cbSmokeDetector;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.ComboBox cbRefrigerationControls;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.ComboBox cbRefrigerationSystemOptions;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelInputsHeatCapacityLabel;
        public System.Windows.Forms.ComboBox cbThruBaseProvisions;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label147;
        public System.Windows.Forms.ComboBox cbHingedServiceAccessFilters;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label146;
        public System.Windows.Forms.ComboBox cbSupplyFanMotor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label145;
        public System.Windows.Forms.ComboBox cbFreshAirSelection;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label144;
        public System.Windows.Forms.ComboBox cbServiceSequence;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.ComboBox cbVoltage;
        public System.Windows.Forms.ComboBox cbAirflowConfiguration;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label142;
        public System.Windows.Forms.ComboBox cbNotUsed;
        private System.Windows.Forms.Label label141;
        public System.Windows.Forms.ComboBox cbMinorDesignSequence;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label140;
        public System.Windows.Forms.ComboBox cbUnitControls;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox cbHeatingCapacity;
        private System.Windows.Forms.Label label139;
        public System.Windows.Forms.ComboBox cbCondenserCoilProtection;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label137;
        public System.Windows.Forms.ComboBox cbMajorDesignSequence;
        private System.Windows.Forms.Label label135;
        public System.Windows.Forms.ComboBox cbUnitType;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.ComboBox cbExhaustDampers;
        public System.Windows.Forms.ComboBox cbCommunicationsOptions;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.ComboBox cbEnergyRecoveryOptions;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.ComboBox cbEnergyRecoveryWheel;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.ComboBox cbSmokeDectectorOutdoorAirMon;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox cbSupplyDampers;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label label148;
        public System.Windows.Forms.Label label149;
        public System.Windows.Forms.Label label150;
        public System.Windows.Forms.Label label160;
        public System.Windows.Forms.Label label161;
        public System.Windows.Forms.Label label162;
        public System.Windows.Forms.Label label163;
        public System.Windows.Forms.Button btnParseModelNo;
        public System.Windows.Forms.Label lbEnterModelNo;
        public System.Windows.Forms.TextBox txtModelNo;
        private System.Windows.Forms.Button btnCreateBOM;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.Label label164;
        public System.Windows.Forms.TextBox txtOrderNum;
        private System.Windows.Forms.Button btnCreateSubmittal;
        private System.Windows.Forms.Button btnGetOrderDetails;
        public System.Windows.Forms.Label label165;
        public System.Windows.Forms.TextBox txtOrderLine;
        private System.Windows.Forms.Label label168;
        public System.Windows.Forms.ComboBox cbPulleyFan;
        private System.Windows.Forms.Label label167;
        public System.Windows.Forms.ComboBox cbBelt;
        private System.Windows.Forms.Label label166;
        public System.Windows.Forms.ComboBox cbPulleyDriver;
        public System.Windows.Forms.Label label169;
        public System.Windows.Forms.Label label170;
        public System.Windows.Forms.Label label171;
        public System.Windows.Forms.Label label172;
        public System.Windows.Forms.Label label173;
        public System.Windows.Forms.Label label174;
        public System.Windows.Forms.Label label175;
        public System.Windows.Forms.Label label176;
        public System.Windows.Forms.Label label177;
        public System.Windows.Forms.GroupBox gbOrder_ModelNo;
        public System.Windows.Forms.Label label178;
        public System.Windows.Forms.Label label179;
        public System.Windows.Forms.MenuStrip menuStripConfig;
    }
}