﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmPicklistStations : Form
    {
        public frmPicklistStations()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDisplayRpt_Click(object sender, EventArgs e)
        {
            bool stationChecked = false;                    

            int reportID = 0;

            string retReportID = "";
            string jobNumStr = "";
            string jobNumList = "";
            string stationLst = "";
            string relOpList = "";
            //string oauDescStr = lbOAU_Desc.Text;
            string oauDescStr = "Picklist";

            bool allStationsSelected = false;

            MainBO objMain = new MainBO();

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum", typeof(string)).MaxLength = 14;
            dt.Columns.Add("StationList", typeof(string)).MaxLength = 200;
            dt.Columns.Add("StationNumList", typeof(string)).MaxLength = 50;

            foreach (DataGridViewRow row in dgvPickListJobs.Rows)
            {
                jobNumStr = row.Cells["JobNum"].Value.ToString();  // Job Number cell
                //jobNumList += jobNumStr + ", ";

                stationLst = "";
                relOpList = "";
                allStationsSelected = false;

                var dr = dt.NewRow();
                dr["JobNum"] = jobNumStr;

                if (Convert.ToBoolean(row.Cells[0].Value) == true) // All station checkbox cell
                {
                    stationChecked = true;
                    stationLst += "ASY 1,ASY 2,ASY 3,ASY 4,ASY 5,ASY 6,ASY 7,ASY 8,ASY 9,ASY 10,TEST,CLOSE,";
                    relOpList += "40,41,50,51,60,70,71,72,80,85,90,100";
                    allStationsSelected = true;
                }

                if (allStationsSelected == false)
                {
                    if (Convert.ToBoolean(row.Cells[1].Value) == true) // Station 1 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 1";
                        relOpList += "40";
                    }

                    if (Convert.ToBoolean(row.Cells[2].Value) == true)  // Station 2 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 2";
                        relOpList += "41";
                    }

                    if (Convert.ToBoolean(row.Cells[3].Value) == true) // Station 3 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 3";
                        relOpList += "50";
                    }

                    if (Convert.ToBoolean(row.Cells[4].Value) == true) // Station 4 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 4";
                        relOpList += "51";
                    }

                    if (Convert.ToBoolean(row.Cells[5].Value) == true)   // Station 5 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 5";
                        relOpList += "60";
                    }

                    if (Convert.ToBoolean(row.Cells[6].Value) == true) // Station 6 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 6";
                        relOpList += "70";
                    }

                    if (Convert.ToBoolean(row.Cells[7].Value) == true)  // Station 7 checkbox cell
                    {
                        stationChecked = true;
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 7";
                        relOpList += "71";
                    }

                    if (Convert.ToBoolean(row.Cells[8].Value) == true) // Station 8 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 8";
                        relOpList += "72";
                    }

                    if (Convert.ToBoolean(row.Cells[9].Value) == true)   // Station 9 checkbox cell
                    {
                        stationChecked = true;
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 9";
                        relOpList += "80";
                    }

                    if (Convert.ToBoolean(row.Cells[10].Value) == true) // Station 10 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "ASY 10";
                        relOpList += "85";
                    }

                    if (Convert.ToBoolean(row.Cells[11].Value) == true)   // Test Station checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "TEST";
                        relOpList += "90";
                    }

                    if (Convert.ToBoolean(row.Cells[12].Value) == true)   // Test Station checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            stationLst += ",";
                            relOpList += ",";
                        }
                        stationChecked = true;
                        stationLst += "CLOSE";
                        relOpList += "100";
                    }
                }

                //if (Convert.ToBoolean(row.Cells[12].Value) == true) // Sub Assembly Station 1 checkbox cell
                //{                  
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 01";
                //    stationNumLst += "S1";
                //}

                //if (Convert.ToBoolean(row.Cells[13].Value) == true) // Sub Assembly Station 2 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 02";
                //    stationNumLst += "S2";
                //}

                //if (Convert.ToBoolean(row.Cells[14].Value) == true) // Sub Assembly Station 3 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 03";
                //    stationNumLst += "S3";
                //}

                //if (Convert.ToBoolean(row.Cells[15].Value) == true) // Sub Assembly Station 4 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 04";
                //    stationNumLst += "S4";
                //}

                //if (Convert.ToBoolean(row.Cells[16].Value) == true) // Sub Assembly Station 5 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 05";
                //    stationNumLst += "S5";
                //}

                //if (Convert.ToBoolean(row.Cells[17].Value) == true) // Sub Assembly Station 6 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 06";
                //    stationNumLst += "S6";
                //}

                //if (Convert.ToBoolean(row.Cells[18].Value) == true) // Sub Assembly Station 7 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 07";
                //    stationNumLst += "S7";
                //}

                //if (Convert.ToBoolean(row.Cells[19].Value) == true) // Sub Assembly Station 8 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 08";
                //    stationNumLst += "S8";
                //}

                //if (Convert.ToBoolean(row.Cells[20].Value) == true) // Sub Assembly Station 9 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 09";
                //    stationNumLst += "S9";
                //}

                //if (Convert.ToBoolean(row.Cells[21].Value) == true) // Sub Assembly Station 10 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 10";
                //    stationNumLst += "S10";
                //}

                //if (Convert.ToBoolean(row.Cells[22].Value) == true) // Sub Assembly Station 11 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 11";
                //    stationNumLst += "S11";
                //}

                //if (Convert.ToBoolean(row.Cells[23].Value) == true) // Sub Assembly Station 12 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 12";
                //    stationNumLst += "S12";
                //}

                //if (Convert.ToBoolean(row.Cells[24].Value) == true) // Sub Assembly Station 13 checkbox cell
                //{
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "SA 13";
                //    stationNumLst += "S13";
                //}

                //if (stationChecked == false)
                //{
                //    MessageBox.Show("ERROR - No Stations selected for Job # " + jobNumStr);
                //    break;
                //}
                //else
                //{
                //    stationChecked = false;                   
                //    dr["StationList"] = stationLst.Substring(0, stationLst.Length);
                //    dr["StationNumList"] = stationNumLst.Substring(0, stationNumLst.Length);
                //    dt.Rows.Add(dr);                                      
                //}
            

                if (jobNumList.Length > 0)
                {
                    jobNumList = jobNumList.Substring(0, (jobNumList.Length - 2));
                }

            //if (dt.Rows.Count > 0)
            //{
            //    retReportID = objMain.Insert_OAU_PickListHistory(dt, System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString());
            //}

            //if (retReportID != "" && retReportID != null)
            //{
                //reportID = int.Parse(retReportID);

                ReportDocument cryRpt = new ReportDocument();
                frmPrintBOM frmPrint = new frmPrintBOM();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNum";
                pdv1.Value = jobNumStr;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@StationLocList";
                pdv2.Value = stationLst;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                ParameterField pf3 = new ParameterField();
                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                pf3.Name = "@RelOpList";
                pdv3.Value = relOpList;
                pf3.CurrentValues.Add(pdv3);

                paramFields.Add(pf3);

                //ParameterField pf4 = new ParameterField();
                //ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
                //pf4.Name = "@OAU_Desc";
                //pdv4.Value = oauDescStr;
                //pf4.CurrentValues.Add(pdv4);

                //paramFields.Add(pf4);

                //ParameterField pf5 = new ParameterField();
                //ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
                //pf5.Name = "@StationNumList";
                //pdv5.Value = stationNumLst;
                //pf5.CurrentValues.Add(pdv5);

                //paramFields.Add(pf5);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUPickList\\OAU_PickListByStation.rpt";             
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUPickList\\OAU_PickListByStation.rpt";
#endif

                cryRpt.Load(objMain.ReportString);
                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.Show();
                frmPrint.crystalReportViewer1.Refresh();
            }
            //}
            //else
            //{
            //    MessageBox.Show("WARNING: Required Report ID is missing. Please contact IT to resolve.");
            //}
        }
    }
}
