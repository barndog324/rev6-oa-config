﻿namespace OA_Config_Rev6
{
    partial class frmModelNoConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msConfigMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbBOMCreationDate = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbShipDate = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lbOrderDate = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.lbProdStartDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbCompleteDate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbLastUpdateDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.gbModelNoConfig = new System.Windows.Forms.GroupBox();
            this.cbDigit69 = new System.Windows.Forms.ComboBox();
            this.lbDigit69Desc = new System.Windows.Forms.Label();
            this.cbDigit68 = new System.Windows.Forms.ComboBox();
            this.lbDigit68Desc = new System.Windows.Forms.Label();
            this.cbDigit67 = new System.Windows.Forms.ComboBox();
            this.lbDigit67Desc = new System.Windows.Forms.Label();
            this.cbDigit66 = new System.Windows.Forms.ComboBox();
            this.lbDigit66Desc = new System.Windows.Forms.Label();
            this.cbDigit65 = new System.Windows.Forms.ComboBox();
            this.lbDigit65Desc = new System.Windows.Forms.Label();
            this.cbDigit64 = new System.Windows.Forms.ComboBox();
            this.lbDigit64Desc = new System.Windows.Forms.Label();
            this.cbDigit63 = new System.Windows.Forms.ComboBox();
            this.lbDigit63Desc = new System.Windows.Forms.Label();
            this.cbMinimumDamperLeakage = new System.Windows.Forms.ComboBox();
            this.label245 = new System.Windows.Forms.Label();
            this.cbOutdoorCoilFluidType = new System.Windows.Forms.ComboBox();
            this.label211 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label208 = new System.Windows.Forms.Label();
            this.txtEnteringTemp = new System.Windows.Forms.TextBox();
            this.txtFlowRate = new System.Windows.Forms.TextBox();
            this.cbFreezstat = new System.Windows.Forms.ComboBox();
            this.label207 = new System.Windows.Forms.Label();
            this.cbCondensateOverflowSwitch = new System.Windows.Forms.ComboBox();
            this.label206 = new System.Windows.Forms.Label();
            this.cbAltitude = new System.Windows.Forms.ComboBox();
            this.label205 = new System.Windows.Forms.Label();
            this.cbThermostat = new System.Windows.Forms.ComboBox();
            this.label204 = new System.Windows.Forms.Label();
            this.cbFaceAndBypassEvap = new System.Windows.Forms.ComboBox();
            this.label203 = new System.Windows.Forms.Label();
            this.cbCoolingControls = new System.Windows.Forms.ComboBox();
            this.label202 = new System.Windows.Forms.Label();
            this.cbControlsDisplay = new System.Windows.Forms.ComboBox();
            this.label201 = new System.Windows.Forms.Label();
            this.cbConvenienceOutlet = new System.Windows.Forms.ComboBox();
            this.label200 = new System.Windows.Forms.Label();
            this.cbInstallation = new System.Windows.Forms.ComboBox();
            this.label199 = new System.Windows.Forms.Label();
            this.cbUV_Lights = new System.Windows.Forms.ComboBox();
            this.label198 = new System.Windows.Forms.Label();
            this.cbServiceLights = new System.Windows.Forms.ComboBox();
            this.label197 = new System.Windows.Forms.Label();
            this.cbHailguards = new System.Windows.Forms.ComboBox();
            this.label196 = new System.Windows.Forms.Label();
            this.cbSmokeDetector = new System.Windows.Forms.ComboBox();
            this.label195 = new System.Windows.Forms.Label();
            this.cbExhaustFanWheelDiamater = new System.Windows.Forms.ComboBox();
            this.label194 = new System.Windows.Forms.Label();
            this.cbExhaustFanMotorType = new System.Windows.Forms.ComboBox();
            this.label193 = new System.Windows.Forms.Label();
            this.cbExhaustFanMotor = new System.Windows.Forms.ComboBox();
            this.label192 = new System.Windows.Forms.Label();
            this.cbSupplyFanWheelDiameter = new System.Windows.Forms.ComboBox();
            this.label191 = new System.Windows.Forms.Label();
            this.cbFanPiezoRing = new System.Windows.Forms.ComboBox();
            this.cbSupplyFanMotorType = new System.Windows.Forms.ComboBox();
            this.cbMajorDesign = new System.Windows.Forms.ComboBox();
            this.label161 = new System.Windows.Forms.Label();
            this.cbAirflowConfig = new System.Windows.Forms.ComboBox();
            this.label162 = new System.Windows.Forms.Label();
            this.cbSoundAttenuationPackage = new System.Windows.Forms.ComboBox();
            this.label163 = new System.Windows.Forms.Label();
            this.cbCondenserFanOptions = new System.Windows.Forms.ComboBox();
            this.label164 = new System.Windows.Forms.Label();
            this.cbOutdoorAirMonitoring = new System.Windows.Forms.ComboBox();
            this.label165 = new System.Windows.Forms.Label();
            this.cbCorrisiveEnvironmentPackage = new System.Windows.Forms.ComboBox();
            this.label166 = new System.Windows.Forms.Label();
            this.cbElectricalOptions = new System.Windows.Forms.ComboBox();
            this.label167 = new System.Windows.Forms.Label();
            this.cbDamperOptions = new System.Windows.Forms.ComboBox();
            this.label168 = new System.Windows.Forms.Label();
            this.cbERV_RotationSensor = new System.Windows.Forms.ComboBox();
            this.label169 = new System.Windows.Forms.Label();
            this.cbEnergyWheelRecoverySize = new System.Windows.Forms.ComboBox();
            this.label170 = new System.Windows.Forms.Label();
            this.cbEnergyRecoveryWheelOptions = new System.Windows.Forms.ComboBox();
            this.label171 = new System.Windows.Forms.Label();
            this.cbEnergyRecovery = new System.Windows.Forms.ComboBox();
            this.label172 = new System.Windows.Forms.Label();
            this.cbFilterOptions = new System.Windows.Forms.ComboBox();
            this.label173 = new System.Windows.Forms.Label();
            this.cbBuildingInterface = new System.Windows.Forms.ComboBox();
            this.label174 = new System.Windows.Forms.Label();
            this.cbUnitControls = new System.Windows.Forms.ComboBox();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.labelInputsHeatCapacityLabel = new System.Windows.Forms.Label();
            this.cbSupplyFanMotor = new System.Windows.Forms.ComboBox();
            this.label177 = new System.Windows.Forms.Label();
            this.cbHeatTypeSecondary = new System.Windows.Forms.ComboBox();
            this.label178 = new System.Windows.Forms.Label();
            this.cbHeatCapacityPrimary = new System.Windows.Forms.ComboBox();
            this.label179 = new System.Windows.Forms.Label();
            this.cbHeatTypePrimary = new System.Windows.Forms.ComboBox();
            this.label180 = new System.Windows.Forms.Label();
            this.cbRefridgerantCapacityControl = new System.Windows.Forms.ComboBox();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.cbIndoorCoilType = new System.Windows.Forms.ComboBox();
            this.cbCoolingCapacity = new System.Windows.Forms.ComboBox();
            this.label183 = new System.Windows.Forms.Label();
            this.cbExhaustDampers = new System.Windows.Forms.ComboBox();
            this.label184 = new System.Windows.Forms.Label();
            this.cbOutdoorCoilType = new System.Windows.Forms.ComboBox();
            this.label185 = new System.Windows.Forms.Label();
            this.cbHotGasReheat = new System.Windows.Forms.ComboBox();
            this.label186 = new System.Windows.Forms.Label();
            this.cbCompressor = new System.Windows.Forms.ComboBox();
            this.cbHeatCapacitySecondary = new System.Windows.Forms.ComboBox();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.cbVoltage = new System.Windows.Forms.ComboBox();
            this.label189 = new System.Windows.Forms.Label();
            this.cbCabinet = new System.Windows.Forms.ComboBox();
            this.label190 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCreateBOM = new System.Windows.Forms.Button();
            this.txtModelNo = new System.Windows.Forms.TextBox();
            this.lbEnterModelNo = new System.Windows.Forms.Label();
            this.btnParseModelNo = new System.Windows.Forms.Button();
            this.lbCustName = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this.label219 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this.label235 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.lbLastUpdateBy = new System.Windows.Forms.Label();
            this.lbBomCreateBy = new System.Windows.Forms.Label();
            this.lbUnitType = new System.Windows.Forms.Label();
            this.lbModelType = new System.Windows.Forms.Label();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.msConfigMenu.SuspendLayout();
            this.gbModelNoConfig.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // msConfigMenu
            // 
            this.msConfigMenu.BackColor = System.Drawing.Color.LightSalmon;
            this.msConfigMenu.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msConfigMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.msConfigMenu.Location = new System.Drawing.Point(0, 0);
            this.msConfigMenu.Name = "msConfigMenu";
            this.msConfigMenu.Size = new System.Drawing.Size(1436, 24);
            this.msConfigMenu.TabIndex = 1;
            this.msConfigMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createBOMToolStripMenuItem,
            this.deleteBOMToolStripMenuItem,
            this.printToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createBOMToolStripMenuItem
            // 
            this.createBOMToolStripMenuItem.Name = "createBOMToolStripMenuItem";
            this.createBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.createBOMToolStripMenuItem.Text = "Create BOM";
            this.createBOMToolStripMenuItem.Click += new System.EventHandler(this.createBOMToolStripMenuItem_Click);
            // 
            // deleteBOMToolStripMenuItem
            // 
            this.deleteBOMToolStripMenuItem.Name = "deleteBOMToolStripMenuItem";
            this.deleteBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteBOMToolStripMenuItem.Text = "Delete BOM";
            this.deleteBOMToolStripMenuItem.Click += new System.EventHandler(this.deleteBOMToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lbBOMCreationDate
            // 
            this.lbBOMCreationDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBOMCreationDate.ForeColor = System.Drawing.Color.Black;
            this.lbBOMCreationDate.Location = new System.Drawing.Point(151, 50);
            this.lbBOMCreationDate.Name = "lbBOMCreationDate";
            this.lbBOMCreationDate.Size = new System.Drawing.Size(100, 20);
            this.lbBOMCreationDate.TabIndex = 40;
            this.lbBOMCreationDate.Text = "??/??/????";
            this.lbBOMCreationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Teal;
            this.label27.Location = new System.Drawing.Point(12, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(140, 20);
            this.label27.TabIndex = 39;
            this.label27.Text = "BOM Creation Date:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbShipDate
            // 
            this.lbShipDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShipDate.ForeColor = System.Drawing.Color.Black;
            this.lbShipDate.Location = new System.Drawing.Point(1327, 72);
            this.lbShipDate.Name = "lbShipDate";
            this.lbShipDate.Size = new System.Drawing.Size(94, 20);
            this.lbShipDate.TabIndex = 38;
            this.lbShipDate.Text = "??/??/????";
            this.lbShipDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Teal;
            this.label32.Location = new System.Drawing.Point(1187, 72);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 20);
            this.label32.TabIndex = 37;
            this.label32.Text = "Ship Date:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbOrderDate
            // 
            this.lbOrderDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderDate.ForeColor = System.Drawing.Color.Black;
            this.lbOrderDate.Location = new System.Drawing.Point(151, 30);
            this.lbOrderDate.Name = "lbOrderDate";
            this.lbOrderDate.Size = new System.Drawing.Size(100, 20);
            this.lbOrderDate.TabIndex = 36;
            this.lbOrderDate.Text = "??/??/????";
            this.lbOrderDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Teal;
            this.label26.Location = new System.Drawing.Point(12, 30);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 20);
            this.label26.TabIndex = 35;
            this.label26.Text = "Order Date:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(568, 50);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(105, 25);
            this.label25.TabIndex = 34;
            this.label25.Text = "Job #:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Tahoma", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Black;
            this.lbJobNum.Location = new System.Drawing.Point(672, 50);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(225, 25);
            this.lbJobNum.TabIndex = 33;
            this.lbJobNum.Text = "??????-??-??";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbProdStartDate
            // 
            this.lbProdStartDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProdStartDate.ForeColor = System.Drawing.Color.Black;
            this.lbProdStartDate.Location = new System.Drawing.Point(1327, 30);
            this.lbProdStartDate.Name = "lbProdStartDate";
            this.lbProdStartDate.Size = new System.Drawing.Size(94, 20);
            this.lbProdStartDate.TabIndex = 42;
            this.lbProdStartDate.Text = "??/??/????";
            this.lbProdStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(1209, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 41;
            this.label2.Text = "Prod Start Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbCompleteDate
            // 
            this.lbCompleteDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCompleteDate.ForeColor = System.Drawing.Color.Black;
            this.lbCompleteDate.Location = new System.Drawing.Point(1327, 50);
            this.lbCompleteDate.Name = "lbCompleteDate";
            this.lbCompleteDate.Size = new System.Drawing.Size(94, 20);
            this.lbCompleteDate.TabIndex = 44;
            this.lbCompleteDate.Text = "??/??/????";
            this.lbCompleteDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(1209, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 43;
            this.label4.Text = "Complete Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label129
            // 
            this.label129.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Black;
            this.label129.Location = new System.Drawing.Point(778, 157);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(13, 15);
            this.label129.TabIndex = 230;
            this.label129.Text = "9";
            // 
            // label130
            // 
            this.label130.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Black;
            this.label130.Location = new System.Drawing.Point(778, 142);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(13, 15);
            this.label130.TabIndex = 229;
            this.label130.Text = "3";
            // 
            // label131
            // 
            this.label131.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Black;
            this.label131.Location = new System.Drawing.Point(778, 134);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(13, 15);
            this.label131.TabIndex = 228;
            this.label131.Text = "^";
            // 
            // label127
            // 
            this.label127.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Black;
            this.label127.Location = new System.Drawing.Point(618, 157);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(13, 15);
            this.label127.TabIndex = 227;
            this.label127.Text = "9";
            // 
            // label128
            // 
            this.label128.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Black;
            this.label128.Location = new System.Drawing.Point(603, 157);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(13, 15);
            this.label128.TabIndex = 226;
            this.label128.Text = "8";
            // 
            // label119
            // 
            this.label119.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(762, 157);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(13, 15);
            this.label119.TabIndex = 225;
            this.label119.Text = "8";
            // 
            // label120
            // 
            this.label120.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(746, 157);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(13, 15);
            this.label120.TabIndex = 224;
            this.label120.Text = "7";
            // 
            // label121
            // 
            this.label121.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(730, 157);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(13, 15);
            this.label121.TabIndex = 223;
            this.label121.Text = "6";
            // 
            // label122
            // 
            this.label122.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Black;
            this.label122.Location = new System.Drawing.Point(715, 157);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(13, 15);
            this.label122.TabIndex = 222;
            this.label122.Text = "5";
            // 
            // label123
            // 
            this.label123.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Black;
            this.label123.Location = new System.Drawing.Point(699, 157);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(13, 15);
            this.label123.TabIndex = 221;
            this.label123.Text = "4";
            // 
            // label124
            // 
            this.label124.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.Black;
            this.label124.Location = new System.Drawing.Point(682, 157);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(13, 15);
            this.label124.TabIndex = 220;
            this.label124.Text = "3";
            // 
            // label125
            // 
            this.label125.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.Black;
            this.label125.Location = new System.Drawing.Point(665, 157);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(13, 15);
            this.label125.TabIndex = 219;
            this.label125.Text = "2";
            // 
            // label126
            // 
            this.label126.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Black;
            this.label126.Location = new System.Drawing.Point(650, 157);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(13, 15);
            this.label126.TabIndex = 218;
            this.label126.Text = "1";
            // 
            // label111
            // 
            this.label111.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(587, 157);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(13, 15);
            this.label111.TabIndex = 217;
            this.label111.Text = "7";
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Black;
            this.label112.Location = new System.Drawing.Point(572, 157);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(13, 15);
            this.label112.TabIndex = 216;
            this.label112.Text = "6";
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(555, 157);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(13, 15);
            this.label113.TabIndex = 215;
            this.label113.Text = "5";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(539, 157);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(13, 15);
            this.label114.TabIndex = 214;
            this.label114.Text = "4";
            // 
            // label115
            // 
            this.label115.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(523, 157);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(13, 15);
            this.label115.TabIndex = 213;
            this.label115.Text = "3";
            // 
            // label116
            // 
            this.label116.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Black;
            this.label116.Location = new System.Drawing.Point(506, 157);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(13, 15);
            this.label116.TabIndex = 212;
            this.label116.Text = "2";
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(489, 157);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(13, 15);
            this.label117.TabIndex = 211;
            this.label117.Text = "1";
            // 
            // label118
            // 
            this.label118.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(458, 157);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(13, 15);
            this.label118.TabIndex = 210;
            this.label118.Text = "9";
            // 
            // label103
            // 
            this.label103.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(442, 157);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(13, 15);
            this.label103.TabIndex = 209;
            this.label103.Text = "8";
            // 
            // label104
            // 
            this.label104.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(427, 157);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(13, 15);
            this.label104.TabIndex = 208;
            this.label104.Text = "7";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(410, 157);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(13, 15);
            this.label105.TabIndex = 207;
            this.label105.Text = "6";
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(394, 157);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(13, 15);
            this.label106.TabIndex = 206;
            this.label106.Text = "5";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(377, 157);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(13, 15);
            this.label107.TabIndex = 205;
            this.label107.Text = "4";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(361, 157);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(13, 15);
            this.label108.TabIndex = 204;
            this.label108.Text = "3";
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Black;
            this.label109.Location = new System.Drawing.Point(344, 157);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(13, 15);
            this.label109.TabIndex = 203;
            this.label109.Text = "2";
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(329, 157);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(13, 15);
            this.label110.TabIndex = 202;
            this.label110.Text = "1";
            // 
            // label102
            // 
            this.label102.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(298, 142);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(13, 15);
            this.label102.TabIndex = 201;
            this.label102.Text = "9";
            // 
            // label100
            // 
            this.label100.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(762, 142);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(13, 15);
            this.label100.TabIndex = 200;
            this.label100.Text = "3";
            // 
            // label101
            // 
            this.label101.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(746, 142);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(13, 15);
            this.label101.TabIndex = 199;
            this.label101.Text = "3";
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(730, 142);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(13, 15);
            this.label96.TabIndex = 198;
            this.label96.Text = "3";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(715, 142);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(13, 15);
            this.label97.TabIndex = 197;
            this.label97.Text = "3";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(699, 142);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(13, 15);
            this.label98.TabIndex = 196;
            this.label98.Text = "3";
            // 
            // label99
            // 
            this.label99.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(682, 142);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(13, 15);
            this.label99.TabIndex = 195;
            this.label99.Text = "3";
            // 
            // label92
            // 
            this.label92.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(665, 142);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(13, 15);
            this.label92.TabIndex = 194;
            this.label92.Text = "3";
            // 
            // label93
            // 
            this.label93.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(650, 142);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(13, 15);
            this.label93.TabIndex = 193;
            this.label93.Text = "3";
            // 
            // label94
            // 
            this.label94.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(618, 142);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(13, 15);
            this.label94.TabIndex = 192;
            this.label94.Text = "2";
            // 
            // label95
            // 
            this.label95.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(603, 142);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(13, 15);
            this.label95.TabIndex = 191;
            this.label95.Text = "2";
            // 
            // label88
            // 
            this.label88.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(587, 142);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(13, 15);
            this.label88.TabIndex = 190;
            this.label88.Text = "2";
            // 
            // label89
            // 
            this.label89.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(572, 142);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(13, 15);
            this.label89.TabIndex = 189;
            this.label89.Text = "2";
            // 
            // label90
            // 
            this.label90.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(555, 142);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(13, 15);
            this.label90.TabIndex = 188;
            this.label90.Text = "2";
            // 
            // label91
            // 
            this.label91.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(539, 142);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(13, 15);
            this.label91.TabIndex = 187;
            this.label91.Text = "2";
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(523, 142);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(13, 15);
            this.label84.TabIndex = 186;
            this.label84.Text = "2";
            // 
            // label85
            // 
            this.label85.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(506, 142);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(13, 15);
            this.label85.TabIndex = 185;
            this.label85.Text = "2";
            // 
            // label86
            // 
            this.label86.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(489, 142);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(13, 15);
            this.label86.TabIndex = 184;
            this.label86.Text = "2";
            // 
            // label87
            // 
            this.label87.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(458, 142);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(13, 15);
            this.label87.TabIndex = 183;
            this.label87.Text = "1";
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(442, 142);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(13, 15);
            this.label80.TabIndex = 182;
            this.label80.Text = "1";
            // 
            // label81
            // 
            this.label81.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(427, 142);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(13, 15);
            this.label81.TabIndex = 181;
            this.label81.Text = "1";
            // 
            // label82
            // 
            this.label82.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(410, 142);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(13, 15);
            this.label82.TabIndex = 180;
            this.label82.Text = "1";
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(394, 142);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(13, 15);
            this.label83.TabIndex = 179;
            this.label83.Text = "1";
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(377, 142);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(13, 15);
            this.label76.TabIndex = 178;
            this.label76.Text = "1";
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(361, 142);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(13, 15);
            this.label77.TabIndex = 177;
            this.label77.Text = "1";
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(344, 142);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(13, 15);
            this.label78.TabIndex = 176;
            this.label78.Text = "1";
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(329, 142);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(13, 15);
            this.label79.TabIndex = 175;
            this.label79.Text = "1";
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(282, 142);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(13, 15);
            this.label72.TabIndex = 174;
            this.label72.Text = "8";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(266, 142);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(13, 15);
            this.label73.TabIndex = 173;
            this.label73.Text = "7";
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(250, 142);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(13, 15);
            this.label74.TabIndex = 172;
            this.label74.Text = "6";
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(232, 142);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(13, 15);
            this.label75.TabIndex = 171;
            this.label75.Text = "5";
            // 
            // label70
            // 
            this.label70.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(217, 142);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(13, 15);
            this.label70.TabIndex = 170;
            this.label70.Text = "4";
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(201, 142);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(13, 15);
            this.label71.TabIndex = 169;
            this.label71.Text = "3";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(187, 142);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(13, 15);
            this.label69.TabIndex = 168;
            this.label69.Text = "2";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(172, 142);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(13, 15);
            this.label68.TabIndex = 167;
            this.label68.Text = "1";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(298, 134);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(13, 15);
            this.label67.TabIndex = 166;
            this.label67.Text = "^";
            // 
            // label66
            // 
            this.label66.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(619, 134);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(13, 15);
            this.label66.TabIndex = 165;
            this.label66.Text = "^";
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(603, 134);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(13, 15);
            this.label65.TabIndex = 164;
            this.label65.Text = "^";
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(762, 134);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(13, 15);
            this.label61.TabIndex = 163;
            this.label61.Text = "^";
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(746, 134);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(13, 15);
            this.label62.TabIndex = 162;
            this.label62.Text = "^";
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(730, 134);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(13, 15);
            this.label63.TabIndex = 161;
            this.label63.Text = "^";
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(715, 134);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(13, 15);
            this.label64.TabIndex = 160;
            this.label64.Text = "^";
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(699, 134);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(13, 15);
            this.label57.TabIndex = 159;
            this.label57.Text = "^";
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(682, 134);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(13, 15);
            this.label58.TabIndex = 158;
            this.label58.Text = "^";
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(666, 134);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(13, 15);
            this.label59.TabIndex = 157;
            this.label59.Text = "^";
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(651, 134);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(13, 15);
            this.label60.TabIndex = 156;
            this.label60.Text = "^";
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(587, 134);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(13, 15);
            this.label53.TabIndex = 155;
            this.label53.Text = "^";
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(572, 134);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(13, 15);
            this.label54.TabIndex = 154;
            this.label54.Text = "^";
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(555, 134);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(13, 15);
            this.label55.TabIndex = 153;
            this.label55.Text = "^";
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(540, 134);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(13, 15);
            this.label56.TabIndex = 152;
            this.label56.Text = "^";
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(523, 134);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(13, 15);
            this.label49.TabIndex = 151;
            this.label49.Text = "^";
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(506, 134);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(13, 15);
            this.label50.TabIndex = 150;
            this.label50.Text = "^";
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(490, 134);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(13, 15);
            this.label51.TabIndex = 149;
            this.label51.Text = "^";
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(459, 134);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(13, 15);
            this.label52.TabIndex = 148;
            this.label52.Text = "^";
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(442, 134);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(13, 15);
            this.label45.TabIndex = 147;
            this.label45.Text = "^";
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(427, 134);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(13, 15);
            this.label46.TabIndex = 146;
            this.label46.Text = "^";
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(411, 134);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(13, 15);
            this.label47.TabIndex = 145;
            this.label47.Text = "^";
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(395, 134);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 15);
            this.label48.TabIndex = 144;
            this.label48.Text = "^";
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(377, 134);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(13, 15);
            this.label41.TabIndex = 143;
            this.label41.Text = "^";
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(361, 134);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(13, 15);
            this.label42.TabIndex = 142;
            this.label42.Text = "^";
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(345, 134);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(13, 15);
            this.label43.TabIndex = 141;
            this.label43.Text = "^";
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(330, 134);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(13, 15);
            this.label44.TabIndex = 140;
            this.label44.Text = "^";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(283, 134);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(13, 15);
            this.label37.TabIndex = 139;
            this.label37.Text = "^";
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(266, 134);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(13, 15);
            this.label38.TabIndex = 138;
            this.label38.Text = "^";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(251, 134);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 15);
            this.label39.TabIndex = 137;
            this.label39.Text = "^";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(233, 134);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 15);
            this.label40.TabIndex = 136;
            this.label40.Text = "^";
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(217, 134);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(13, 15);
            this.label35.TabIndex = 135;
            this.label35.Text = "^";
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(201, 134);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(13, 15);
            this.label36.TabIndex = 134;
            this.label36.Text = "^";
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(187, 134);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(13, 15);
            this.label34.TabIndex = 133;
            this.label34.Text = "^";
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(172, 134);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(13, 15);
            this.label33.TabIndex = 132;
            this.label33.Text = "^";
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Black;
            this.lbModelNo.Location = new System.Drawing.Point(163, 108);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(1120, 30);
            this.lbModelNo.TabIndex = 131;
            this.lbModelNo.Text = "123456789-123456789-123456789-123456789-123456789-123456789-123456789\r\n\r\n";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLastUpdateDate
            // 
            this.lbLastUpdateDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateDate.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateDate.Location = new System.Drawing.Point(151, 70);
            this.lbLastUpdateDate.Name = "lbLastUpdateDate";
            this.lbLastUpdateDate.Size = new System.Drawing.Size(100, 20);
            this.lbLastUpdateDate.TabIndex = 232;
            this.lbLastUpdateDate.Text = "??/??/????";
            this.lbLastUpdateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(12, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 231;
            this.label3.Text = "Last Updated Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(938, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 15);
            this.label1.TabIndex = 259;
            this.label1.Text = "9";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(938, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 15);
            this.label5.TabIndex = 258;
            this.label5.Text = "4";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(938, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 15);
            this.label6.TabIndex = 257;
            this.label6.Text = "^";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(922, 157);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 15);
            this.label7.TabIndex = 256;
            this.label7.Text = "8";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(906, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 15);
            this.label8.TabIndex = 255;
            this.label8.Text = "7";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(890, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 15);
            this.label9.TabIndex = 254;
            this.label9.Text = "6";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(874, 157);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 15);
            this.label10.TabIndex = 253;
            this.label10.Text = "5";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(859, 157);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 15);
            this.label11.TabIndex = 252;
            this.label11.Text = "4";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(842, 157);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 15);
            this.label12.TabIndex = 251;
            this.label12.Text = "3";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(825, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 15);
            this.label13.TabIndex = 250;
            this.label13.Text = "2";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(809, 157);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 15);
            this.label14.TabIndex = 249;
            this.label14.Text = "1";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(922, 142);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 15);
            this.label15.TabIndex = 248;
            this.label15.Text = "4";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(906, 142);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 15);
            this.label16.TabIndex = 247;
            this.label16.Text = "4";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(890, 142);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 15);
            this.label17.TabIndex = 246;
            this.label17.Text = "4";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(874, 142);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 15);
            this.label18.TabIndex = 245;
            this.label18.Text = "4";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(859, 142);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 15);
            this.label19.TabIndex = 244;
            this.label19.Text = "4";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(842, 142);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 15);
            this.label20.TabIndex = 243;
            this.label20.Text = "4";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(825, 142);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 15);
            this.label21.TabIndex = 242;
            this.label21.Text = "4";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(809, 142);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 15);
            this.label22.TabIndex = 241;
            this.label22.Text = "4";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(922, 134);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 15);
            this.label23.TabIndex = 240;
            this.label23.Text = "^";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(906, 134);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 15);
            this.label24.TabIndex = 239;
            this.label24.Text = "^";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(890, 134);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 15);
            this.label28.TabIndex = 238;
            this.label28.Text = "^";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(874, 134);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 15);
            this.label29.TabIndex = 237;
            this.label29.Text = "^";
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(859, 134);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 15);
            this.label30.TabIndex = 236;
            this.label30.Text = "^";
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(842, 134);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(13, 15);
            this.label31.TabIndex = 235;
            this.label31.Text = "^";
            // 
            // label132
            // 
            this.label132.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.Black;
            this.label132.Location = new System.Drawing.Point(826, 134);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(13, 15);
            this.label132.TabIndex = 234;
            this.label132.Text = "^";
            // 
            // label133
            // 
            this.label133.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.Black;
            this.label133.Location = new System.Drawing.Point(810, 134);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(13, 15);
            this.label133.TabIndex = 233;
            this.label133.Text = "^";
            // 
            // label134
            // 
            this.label134.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.ForeColor = System.Drawing.Color.Black;
            this.label134.Location = new System.Drawing.Point(1098, 157);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(13, 15);
            this.label134.TabIndex = 286;
            this.label134.Text = "9";
            // 
            // label135
            // 
            this.label135.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.ForeColor = System.Drawing.Color.Black;
            this.label135.Location = new System.Drawing.Point(1098, 142);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(13, 15);
            this.label135.TabIndex = 285;
            this.label135.Text = "5";
            // 
            // label136
            // 
            this.label136.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.Color.Black;
            this.label136.Location = new System.Drawing.Point(1098, 134);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(13, 15);
            this.label136.TabIndex = 284;
            this.label136.Text = "^";
            // 
            // label137
            // 
            this.label137.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.ForeColor = System.Drawing.Color.Black;
            this.label137.Location = new System.Drawing.Point(1082, 157);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(13, 15);
            this.label137.TabIndex = 283;
            this.label137.Text = "8";
            // 
            // label138
            // 
            this.label138.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.ForeColor = System.Drawing.Color.Black;
            this.label138.Location = new System.Drawing.Point(1066, 157);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(13, 15);
            this.label138.TabIndex = 282;
            this.label138.Text = "7";
            // 
            // label139
            // 
            this.label139.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.ForeColor = System.Drawing.Color.Black;
            this.label139.Location = new System.Drawing.Point(1050, 157);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(13, 15);
            this.label139.TabIndex = 281;
            this.label139.Text = "6";
            // 
            // label140
            // 
            this.label140.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.ForeColor = System.Drawing.Color.Black;
            this.label140.Location = new System.Drawing.Point(1034, 157);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(13, 15);
            this.label140.TabIndex = 280;
            this.label140.Text = "5";
            // 
            // label141
            // 
            this.label141.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.ForeColor = System.Drawing.Color.Black;
            this.label141.Location = new System.Drawing.Point(1019, 157);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(13, 15);
            this.label141.TabIndex = 279;
            this.label141.Text = "4";
            // 
            // label142
            // 
            this.label142.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.ForeColor = System.Drawing.Color.Black;
            this.label142.Location = new System.Drawing.Point(1002, 157);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(13, 15);
            this.label142.TabIndex = 278;
            this.label142.Text = "3";
            // 
            // label143
            // 
            this.label143.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.ForeColor = System.Drawing.Color.Black;
            this.label143.Location = new System.Drawing.Point(985, 157);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(13, 15);
            this.label143.TabIndex = 277;
            this.label143.Text = "2";
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.Black;
            this.label144.Location = new System.Drawing.Point(969, 157);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(13, 15);
            this.label144.TabIndex = 276;
            this.label144.Text = "1";
            // 
            // label145
            // 
            this.label145.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.ForeColor = System.Drawing.Color.Black;
            this.label145.Location = new System.Drawing.Point(1082, 142);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(13, 15);
            this.label145.TabIndex = 275;
            this.label145.Text = "5";
            // 
            // label146
            // 
            this.label146.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.Black;
            this.label146.Location = new System.Drawing.Point(1066, 142);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(13, 15);
            this.label146.TabIndex = 274;
            this.label146.Text = "5";
            // 
            // label147
            // 
            this.label147.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.ForeColor = System.Drawing.Color.Black;
            this.label147.Location = new System.Drawing.Point(1050, 142);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(13, 15);
            this.label147.TabIndex = 273;
            this.label147.Text = "5";
            // 
            // label148
            // 
            this.label148.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.Black;
            this.label148.Location = new System.Drawing.Point(1034, 142);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(13, 15);
            this.label148.TabIndex = 272;
            this.label148.Text = "5";
            // 
            // label149
            // 
            this.label149.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.Black;
            this.label149.Location = new System.Drawing.Point(1019, 142);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(13, 15);
            this.label149.TabIndex = 271;
            this.label149.Text = "5";
            // 
            // label150
            // 
            this.label150.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.Black;
            this.label150.Location = new System.Drawing.Point(1002, 142);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(13, 15);
            this.label150.TabIndex = 270;
            this.label150.Text = "5";
            // 
            // label151
            // 
            this.label151.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.ForeColor = System.Drawing.Color.Black;
            this.label151.Location = new System.Drawing.Point(985, 142);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(13, 15);
            this.label151.TabIndex = 269;
            this.label151.Text = "5";
            // 
            // label152
            // 
            this.label152.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.ForeColor = System.Drawing.Color.Black;
            this.label152.Location = new System.Drawing.Point(969, 142);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(13, 15);
            this.label152.TabIndex = 268;
            this.label152.Text = "5";
            // 
            // label153
            // 
            this.label153.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.ForeColor = System.Drawing.Color.Black;
            this.label153.Location = new System.Drawing.Point(1082, 134);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(13, 15);
            this.label153.TabIndex = 267;
            this.label153.Text = "^";
            // 
            // label154
            // 
            this.label154.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.ForeColor = System.Drawing.Color.Black;
            this.label154.Location = new System.Drawing.Point(1066, 134);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(13, 15);
            this.label154.TabIndex = 266;
            this.label154.Text = "^";
            // 
            // label155
            // 
            this.label155.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.ForeColor = System.Drawing.Color.Black;
            this.label155.Location = new System.Drawing.Point(1050, 134);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(13, 15);
            this.label155.TabIndex = 265;
            this.label155.Text = "^";
            // 
            // label156
            // 
            this.label156.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.ForeColor = System.Drawing.Color.Black;
            this.label156.Location = new System.Drawing.Point(1034, 134);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(13, 15);
            this.label156.TabIndex = 264;
            this.label156.Text = "^";
            // 
            // label157
            // 
            this.label157.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.ForeColor = System.Drawing.Color.Black;
            this.label157.Location = new System.Drawing.Point(1019, 134);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(13, 15);
            this.label157.TabIndex = 263;
            this.label157.Text = "^";
            // 
            // label158
            // 
            this.label158.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.ForeColor = System.Drawing.Color.Black;
            this.label158.Location = new System.Drawing.Point(1002, 134);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(13, 15);
            this.label158.TabIndex = 262;
            this.label158.Text = "^";
            // 
            // label159
            // 
            this.label159.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.ForeColor = System.Drawing.Color.Black;
            this.label159.Location = new System.Drawing.Point(986, 134);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(13, 15);
            this.label159.TabIndex = 261;
            this.label159.Text = "^";
            // 
            // label160
            // 
            this.label160.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.ForeColor = System.Drawing.Color.Black;
            this.label160.Location = new System.Drawing.Point(970, 134);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(13, 15);
            this.label160.TabIndex = 260;
            this.label160.Text = "^";
            // 
            // gbModelNoConfig
            // 
            this.gbModelNoConfig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gbModelNoConfig.Controls.Add(this.cbDigit69);
            this.gbModelNoConfig.Controls.Add(this.lbDigit69Desc);
            this.gbModelNoConfig.Controls.Add(this.cbDigit68);
            this.gbModelNoConfig.Controls.Add(this.lbDigit68Desc);
            this.gbModelNoConfig.Controls.Add(this.cbDigit67);
            this.gbModelNoConfig.Controls.Add(this.lbDigit67Desc);
            this.gbModelNoConfig.Controls.Add(this.cbDigit66);
            this.gbModelNoConfig.Controls.Add(this.lbDigit66Desc);
            this.gbModelNoConfig.Controls.Add(this.cbDigit65);
            this.gbModelNoConfig.Controls.Add(this.lbDigit65Desc);
            this.gbModelNoConfig.Controls.Add(this.cbDigit64);
            this.gbModelNoConfig.Controls.Add(this.lbDigit64Desc);
            this.gbModelNoConfig.Controls.Add(this.cbDigit63);
            this.gbModelNoConfig.Controls.Add(this.lbDigit63Desc);
            this.gbModelNoConfig.Controls.Add(this.cbMinimumDamperLeakage);
            this.gbModelNoConfig.Controls.Add(this.label245);
            this.gbModelNoConfig.Controls.Add(this.cbOutdoorCoilFluidType);
            this.gbModelNoConfig.Controls.Add(this.label211);
            this.gbModelNoConfig.Controls.Add(this.groupBox1);
            this.gbModelNoConfig.Controls.Add(this.cbFreezstat);
            this.gbModelNoConfig.Controls.Add(this.label207);
            this.gbModelNoConfig.Controls.Add(this.cbCondensateOverflowSwitch);
            this.gbModelNoConfig.Controls.Add(this.label206);
            this.gbModelNoConfig.Controls.Add(this.cbAltitude);
            this.gbModelNoConfig.Controls.Add(this.label205);
            this.gbModelNoConfig.Controls.Add(this.cbThermostat);
            this.gbModelNoConfig.Controls.Add(this.label204);
            this.gbModelNoConfig.Controls.Add(this.cbFaceAndBypassEvap);
            this.gbModelNoConfig.Controls.Add(this.label203);
            this.gbModelNoConfig.Controls.Add(this.cbCoolingControls);
            this.gbModelNoConfig.Controls.Add(this.label202);
            this.gbModelNoConfig.Controls.Add(this.cbControlsDisplay);
            this.gbModelNoConfig.Controls.Add(this.label201);
            this.gbModelNoConfig.Controls.Add(this.cbConvenienceOutlet);
            this.gbModelNoConfig.Controls.Add(this.label200);
            this.gbModelNoConfig.Controls.Add(this.cbInstallation);
            this.gbModelNoConfig.Controls.Add(this.label199);
            this.gbModelNoConfig.Controls.Add(this.cbUV_Lights);
            this.gbModelNoConfig.Controls.Add(this.label198);
            this.gbModelNoConfig.Controls.Add(this.cbServiceLights);
            this.gbModelNoConfig.Controls.Add(this.label197);
            this.gbModelNoConfig.Controls.Add(this.cbHailguards);
            this.gbModelNoConfig.Controls.Add(this.label196);
            this.gbModelNoConfig.Controls.Add(this.cbSmokeDetector);
            this.gbModelNoConfig.Controls.Add(this.label195);
            this.gbModelNoConfig.Controls.Add(this.cbExhaustFanWheelDiamater);
            this.gbModelNoConfig.Controls.Add(this.label194);
            this.gbModelNoConfig.Controls.Add(this.cbExhaustFanMotorType);
            this.gbModelNoConfig.Controls.Add(this.label193);
            this.gbModelNoConfig.Controls.Add(this.cbExhaustFanMotor);
            this.gbModelNoConfig.Controls.Add(this.label192);
            this.gbModelNoConfig.Controls.Add(this.cbSupplyFanWheelDiameter);
            this.gbModelNoConfig.Controls.Add(this.label191);
            this.gbModelNoConfig.Controls.Add(this.cbFanPiezoRing);
            this.gbModelNoConfig.Controls.Add(this.cbSupplyFanMotorType);
            this.gbModelNoConfig.Controls.Add(this.cbMajorDesign);
            this.gbModelNoConfig.Controls.Add(this.label161);
            this.gbModelNoConfig.Controls.Add(this.cbAirflowConfig);
            this.gbModelNoConfig.Controls.Add(this.label162);
            this.gbModelNoConfig.Controls.Add(this.cbSoundAttenuationPackage);
            this.gbModelNoConfig.Controls.Add(this.label163);
            this.gbModelNoConfig.Controls.Add(this.cbCondenserFanOptions);
            this.gbModelNoConfig.Controls.Add(this.label164);
            this.gbModelNoConfig.Controls.Add(this.cbOutdoorAirMonitoring);
            this.gbModelNoConfig.Controls.Add(this.label165);
            this.gbModelNoConfig.Controls.Add(this.cbCorrisiveEnvironmentPackage);
            this.gbModelNoConfig.Controls.Add(this.label166);
            this.gbModelNoConfig.Controls.Add(this.cbElectricalOptions);
            this.gbModelNoConfig.Controls.Add(this.label167);
            this.gbModelNoConfig.Controls.Add(this.cbDamperOptions);
            this.gbModelNoConfig.Controls.Add(this.label168);
            this.gbModelNoConfig.Controls.Add(this.cbERV_RotationSensor);
            this.gbModelNoConfig.Controls.Add(this.label169);
            this.gbModelNoConfig.Controls.Add(this.cbEnergyWheelRecoverySize);
            this.gbModelNoConfig.Controls.Add(this.label170);
            this.gbModelNoConfig.Controls.Add(this.cbEnergyRecoveryWheelOptions);
            this.gbModelNoConfig.Controls.Add(this.label171);
            this.gbModelNoConfig.Controls.Add(this.cbEnergyRecovery);
            this.gbModelNoConfig.Controls.Add(this.label172);
            this.gbModelNoConfig.Controls.Add(this.cbFilterOptions);
            this.gbModelNoConfig.Controls.Add(this.label173);
            this.gbModelNoConfig.Controls.Add(this.cbBuildingInterface);
            this.gbModelNoConfig.Controls.Add(this.label174);
            this.gbModelNoConfig.Controls.Add(this.cbUnitControls);
            this.gbModelNoConfig.Controls.Add(this.label175);
            this.gbModelNoConfig.Controls.Add(this.label176);
            this.gbModelNoConfig.Controls.Add(this.labelInputsHeatCapacityLabel);
            this.gbModelNoConfig.Controls.Add(this.cbSupplyFanMotor);
            this.gbModelNoConfig.Controls.Add(this.label177);
            this.gbModelNoConfig.Controls.Add(this.cbHeatTypeSecondary);
            this.gbModelNoConfig.Controls.Add(this.label178);
            this.gbModelNoConfig.Controls.Add(this.cbHeatCapacityPrimary);
            this.gbModelNoConfig.Controls.Add(this.label179);
            this.gbModelNoConfig.Controls.Add(this.cbHeatTypePrimary);
            this.gbModelNoConfig.Controls.Add(this.label180);
            this.gbModelNoConfig.Controls.Add(this.cbRefridgerantCapacityControl);
            this.gbModelNoConfig.Controls.Add(this.label181);
            this.gbModelNoConfig.Controls.Add(this.label182);
            this.gbModelNoConfig.Controls.Add(this.cbIndoorCoilType);
            this.gbModelNoConfig.Controls.Add(this.cbCoolingCapacity);
            this.gbModelNoConfig.Controls.Add(this.label183);
            this.gbModelNoConfig.Controls.Add(this.cbExhaustDampers);
            this.gbModelNoConfig.Controls.Add(this.label184);
            this.gbModelNoConfig.Controls.Add(this.cbOutdoorCoilType);
            this.gbModelNoConfig.Controls.Add(this.label185);
            this.gbModelNoConfig.Controls.Add(this.cbHotGasReheat);
            this.gbModelNoConfig.Controls.Add(this.label186);
            this.gbModelNoConfig.Controls.Add(this.cbCompressor);
            this.gbModelNoConfig.Controls.Add(this.cbHeatCapacitySecondary);
            this.gbModelNoConfig.Controls.Add(this.label187);
            this.gbModelNoConfig.Controls.Add(this.label188);
            this.gbModelNoConfig.Controls.Add(this.cbVoltage);
            this.gbModelNoConfig.Controls.Add(this.label189);
            this.gbModelNoConfig.Controls.Add(this.cbCabinet);
            this.gbModelNoConfig.Controls.Add(this.label190);
            this.gbModelNoConfig.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbModelNoConfig.Location = new System.Drawing.Point(12, 182);
            this.gbModelNoConfig.Name = "gbModelNoConfig";
            this.gbModelNoConfig.Size = new System.Drawing.Size(1424, 583);
            this.gbModelNoConfig.TabIndex = 287;
            this.gbModelNoConfig.TabStop = false;
            this.gbModelNoConfig.Text = "Inputs";
            // 
            // cbDigit69
            // 
            this.cbDigit69.BackColor = System.Drawing.Color.White;
            this.cbDigit69.DropDownWidth = 300;
            this.cbDigit69.Enabled = false;
            this.cbDigit69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDigit69.ForeColor = System.Drawing.Color.Black;
            this.cbDigit69.FormattingEnabled = true;
            this.cbDigit69.Location = new System.Drawing.Point(1201, 384);
            this.cbDigit69.Name = "cbDigit69";
            this.cbDigit69.Size = new System.Drawing.Size(210, 21);
            this.cbDigit69.TabIndex = 134;
            // 
            // lbDigit69Desc
            // 
            this.lbDigit69Desc.Enabled = false;
            this.lbDigit69Desc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDigit69Desc.ForeColor = System.Drawing.Color.Teal;
            this.lbDigit69Desc.Location = new System.Drawing.Point(968, 384);
            this.lbDigit69Desc.Name = "lbDigit69Desc";
            this.lbDigit69Desc.Size = new System.Drawing.Size(229, 23);
            this.lbDigit69Desc.TabIndex = 133;
            this.lbDigit69Desc.Text = "Future Use (69)";
            this.lbDigit69Desc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit68
            // 
            this.cbDigit68.BackColor = System.Drawing.Color.White;
            this.cbDigit68.DropDownWidth = 300;
            this.cbDigit68.Enabled = false;
            this.cbDigit68.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDigit68.ForeColor = System.Drawing.Color.Black;
            this.cbDigit68.FormattingEnabled = true;
            this.cbDigit68.Location = new System.Drawing.Point(1201, 357);
            this.cbDigit68.Name = "cbDigit68";
            this.cbDigit68.Size = new System.Drawing.Size(210, 21);
            this.cbDigit68.TabIndex = 132;
            // 
            // lbDigit68Desc
            // 
            this.lbDigit68Desc.Enabled = false;
            this.lbDigit68Desc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDigit68Desc.ForeColor = System.Drawing.Color.Teal;
            this.lbDigit68Desc.Location = new System.Drawing.Point(968, 357);
            this.lbDigit68Desc.Name = "lbDigit68Desc";
            this.lbDigit68Desc.Size = new System.Drawing.Size(229, 23);
            this.lbDigit68Desc.TabIndex = 131;
            this.lbDigit68Desc.Text = "Future Use (68)";
            this.lbDigit68Desc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit67
            // 
            this.cbDigit67.BackColor = System.Drawing.Color.White;
            this.cbDigit67.DropDownWidth = 300;
            this.cbDigit67.Enabled = false;
            this.cbDigit67.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDigit67.ForeColor = System.Drawing.Color.Black;
            this.cbDigit67.FormattingEnabled = true;
            this.cbDigit67.Location = new System.Drawing.Point(1201, 331);
            this.cbDigit67.Name = "cbDigit67";
            this.cbDigit67.Size = new System.Drawing.Size(210, 21);
            this.cbDigit67.TabIndex = 130;
            // 
            // lbDigit67Desc
            // 
            this.lbDigit67Desc.Enabled = false;
            this.lbDigit67Desc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDigit67Desc.ForeColor = System.Drawing.Color.Teal;
            this.lbDigit67Desc.Location = new System.Drawing.Point(968, 331);
            this.lbDigit67Desc.Name = "lbDigit67Desc";
            this.lbDigit67Desc.Size = new System.Drawing.Size(229, 23);
            this.lbDigit67Desc.TabIndex = 129;
            this.lbDigit67Desc.Text = "Future Use (67)";
            this.lbDigit67Desc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit66
            // 
            this.cbDigit66.BackColor = System.Drawing.Color.White;
            this.cbDigit66.DropDownWidth = 300;
            this.cbDigit66.Enabled = false;
            this.cbDigit66.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDigit66.ForeColor = System.Drawing.Color.Black;
            this.cbDigit66.FormattingEnabled = true;
            this.cbDigit66.Location = new System.Drawing.Point(1201, 305);
            this.cbDigit66.Name = "cbDigit66";
            this.cbDigit66.Size = new System.Drawing.Size(210, 21);
            this.cbDigit66.TabIndex = 128;
            // 
            // lbDigit66Desc
            // 
            this.lbDigit66Desc.Enabled = false;
            this.lbDigit66Desc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDigit66Desc.ForeColor = System.Drawing.Color.Teal;
            this.lbDigit66Desc.Location = new System.Drawing.Point(968, 305);
            this.lbDigit66Desc.Name = "lbDigit66Desc";
            this.lbDigit66Desc.Size = new System.Drawing.Size(229, 23);
            this.lbDigit66Desc.TabIndex = 127;
            this.lbDigit66Desc.Text = "Future Use (66)";
            this.lbDigit66Desc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit65
            // 
            this.cbDigit65.BackColor = System.Drawing.Color.White;
            this.cbDigit65.DropDownWidth = 300;
            this.cbDigit65.Enabled = false;
            this.cbDigit65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDigit65.ForeColor = System.Drawing.Color.Black;
            this.cbDigit65.FormattingEnabled = true;
            this.cbDigit65.Location = new System.Drawing.Point(1201, 279);
            this.cbDigit65.Name = "cbDigit65";
            this.cbDigit65.Size = new System.Drawing.Size(210, 21);
            this.cbDigit65.TabIndex = 126;
            // 
            // lbDigit65Desc
            // 
            this.lbDigit65Desc.Enabled = false;
            this.lbDigit65Desc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDigit65Desc.ForeColor = System.Drawing.Color.Teal;
            this.lbDigit65Desc.Location = new System.Drawing.Point(968, 279);
            this.lbDigit65Desc.Name = "lbDigit65Desc";
            this.lbDigit65Desc.Size = new System.Drawing.Size(229, 23);
            this.lbDigit65Desc.TabIndex = 125;
            this.lbDigit65Desc.Text = "Future Use (65)";
            this.lbDigit65Desc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit64
            // 
            this.cbDigit64.BackColor = System.Drawing.Color.White;
            this.cbDigit64.DropDownWidth = 300;
            this.cbDigit64.Enabled = false;
            this.cbDigit64.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDigit64.ForeColor = System.Drawing.Color.Black;
            this.cbDigit64.FormattingEnabled = true;
            this.cbDigit64.Location = new System.Drawing.Point(1201, 253);
            this.cbDigit64.Name = "cbDigit64";
            this.cbDigit64.Size = new System.Drawing.Size(210, 21);
            this.cbDigit64.TabIndex = 124;
            // 
            // lbDigit64Desc
            // 
            this.lbDigit64Desc.Enabled = false;
            this.lbDigit64Desc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDigit64Desc.ForeColor = System.Drawing.Color.Teal;
            this.lbDigit64Desc.Location = new System.Drawing.Point(968, 253);
            this.lbDigit64Desc.Name = "lbDigit64Desc";
            this.lbDigit64Desc.Size = new System.Drawing.Size(229, 23);
            this.lbDigit64Desc.TabIndex = 123;
            this.lbDigit64Desc.Text = "Future Use (64)";
            this.lbDigit64Desc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit63
            // 
            this.cbDigit63.BackColor = System.Drawing.Color.White;
            this.cbDigit63.DropDownWidth = 300;
            this.cbDigit63.Enabled = false;
            this.cbDigit63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDigit63.ForeColor = System.Drawing.Color.Black;
            this.cbDigit63.FormattingEnabled = true;
            this.cbDigit63.Location = new System.Drawing.Point(1201, 227);
            this.cbDigit63.Name = "cbDigit63";
            this.cbDigit63.Size = new System.Drawing.Size(210, 21);
            this.cbDigit63.TabIndex = 122;
            // 
            // lbDigit63Desc
            // 
            this.lbDigit63Desc.Enabled = false;
            this.lbDigit63Desc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDigit63Desc.ForeColor = System.Drawing.Color.Teal;
            this.lbDigit63Desc.Location = new System.Drawing.Point(968, 227);
            this.lbDigit63Desc.Name = "lbDigit63Desc";
            this.lbDigit63Desc.Size = new System.Drawing.Size(229, 23);
            this.lbDigit63Desc.TabIndex = 121;
            this.lbDigit63Desc.Text = "Future Use (63)";
            this.lbDigit63Desc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbMinimumDamperLeakage
            // 
            this.cbMinimumDamperLeakage.BackColor = System.Drawing.Color.White;
            this.cbMinimumDamperLeakage.DropDownWidth = 300;
            this.cbMinimumDamperLeakage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMinimumDamperLeakage.ForeColor = System.Drawing.Color.Black;
            this.cbMinimumDamperLeakage.FormattingEnabled = true;
            this.cbMinimumDamperLeakage.Location = new System.Drawing.Point(1201, 201);
            this.cbMinimumDamperLeakage.Name = "cbMinimumDamperLeakage";
            this.cbMinimumDamperLeakage.Size = new System.Drawing.Size(210, 21);
            this.cbMinimumDamperLeakage.TabIndex = 120;
            // 
            // label245
            // 
            this.label245.Enabled = false;
            this.label245.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label245.ForeColor = System.Drawing.Color.Teal;
            this.label245.Location = new System.Drawing.Point(968, 201);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(229, 23);
            this.label245.TabIndex = 119;
            this.label245.Text = "Minimum Damper Leakage (62)";
            this.label245.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbOutdoorCoilFluidType
            // 
            this.cbOutdoorCoilFluidType.BackColor = System.Drawing.Color.White;
            this.cbOutdoorCoilFluidType.DropDownWidth = 300;
            this.cbOutdoorCoilFluidType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOutdoorCoilFluidType.ForeColor = System.Drawing.Color.Black;
            this.cbOutdoorCoilFluidType.FormattingEnabled = true;
            this.cbOutdoorCoilFluidType.Location = new System.Drawing.Point(1201, 175);
            this.cbOutdoorCoilFluidType.Name = "cbOutdoorCoilFluidType";
            this.cbOutdoorCoilFluidType.Size = new System.Drawing.Size(210, 21);
            this.cbOutdoorCoilFluidType.TabIndex = 118;
            // 
            // label211
            // 
            this.label211.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label211.ForeColor = System.Drawing.Color.Teal;
            this.label211.Location = new System.Drawing.Point(968, 175);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(229, 23);
            this.label211.TabIndex = 117;
            this.label211.Text = "Outdoor Coil Fluid Type (61)";
            this.label211.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label209);
            this.groupBox1.Controls.Add(this.label208);
            this.groupBox1.Controls.Add(this.txtEnteringTemp);
            this.groupBox1.Controls.Add(this.txtFlowRate);
            this.groupBox1.Location = new System.Drawing.Point(1110, 436);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 94);
            this.groupBox1.TabIndex = 116;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hot Water Inputs";
            this.groupBox1.Visible = false;
            // 
            // label208
            // 
            this.label208.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label208.ForeColor = System.Drawing.Color.Teal;
            this.label208.Location = new System.Drawing.Point(91, 21);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(101, 20);
            this.label208.TabIndex = 99;
            this.label208.Text = "Flow Rate:";
            this.label208.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEnteringTemp
            // 
            this.txtEnteringTemp.Location = new System.Drawing.Point(197, 54);
            this.txtEnteringTemp.Name = "txtEnteringTemp";
            this.txtEnteringTemp.Size = new System.Drawing.Size(100, 21);
            this.txtEnteringTemp.TabIndex = 1;
            // 
            // txtFlowRate
            // 
            this.txtFlowRate.Location = new System.Drawing.Point(197, 20);
            this.txtFlowRate.Name = "txtFlowRate";
            this.txtFlowRate.Size = new System.Drawing.Size(100, 21);
            this.txtFlowRate.TabIndex = 0;
            // 
            // cbFreezstat
            // 
            this.cbFreezstat.BackColor = System.Drawing.Color.White;
            this.cbFreezstat.DropDownWidth = 300;
            this.cbFreezstat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFreezstat.ForeColor = System.Drawing.Color.Black;
            this.cbFreezstat.FormattingEnabled = true;
            this.cbFreezstat.Location = new System.Drawing.Point(1201, 149);
            this.cbFreezstat.Name = "cbFreezstat";
            this.cbFreezstat.Size = new System.Drawing.Size(210, 21);
            this.cbFreezstat.TabIndex = 115;
            // 
            // label207
            // 
            this.label207.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label207.ForeColor = System.Drawing.Color.Teal;
            this.label207.Location = new System.Drawing.Point(968, 149);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(229, 23);
            this.label207.TabIndex = 114;
            this.label207.Text = "Freezstat (59)";
            this.label207.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCondensateOverflowSwitch
            // 
            this.cbCondensateOverflowSwitch.BackColor = System.Drawing.Color.White;
            this.cbCondensateOverflowSwitch.DropDownWidth = 300;
            this.cbCondensateOverflowSwitch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCondensateOverflowSwitch.ForeColor = System.Drawing.Color.Black;
            this.cbCondensateOverflowSwitch.FormattingEnabled = true;
            this.cbCondensateOverflowSwitch.Location = new System.Drawing.Point(1201, 123);
            this.cbCondensateOverflowSwitch.Name = "cbCondensateOverflowSwitch";
            this.cbCondensateOverflowSwitch.Size = new System.Drawing.Size(210, 21);
            this.cbCondensateOverflowSwitch.TabIndex = 113;
            // 
            // label206
            // 
            this.label206.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label206.ForeColor = System.Drawing.Color.Teal;
            this.label206.Location = new System.Drawing.Point(968, 123);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(229, 23);
            this.label206.TabIndex = 112;
            this.label206.Text = "Condensate Oveflow Switch (58)";
            this.label206.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAltitude
            // 
            this.cbAltitude.BackColor = System.Drawing.Color.White;
            this.cbAltitude.DropDownWidth = 300;
            this.cbAltitude.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAltitude.ForeColor = System.Drawing.Color.Black;
            this.cbAltitude.FormattingEnabled = true;
            this.cbAltitude.Location = new System.Drawing.Point(1201, 97);
            this.cbAltitude.Name = "cbAltitude";
            this.cbAltitude.Size = new System.Drawing.Size(210, 21);
            this.cbAltitude.TabIndex = 111;
            // 
            // label205
            // 
            this.label205.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label205.ForeColor = System.Drawing.Color.Teal;
            this.label205.Location = new System.Drawing.Point(968, 97);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(229, 23);
            this.label205.TabIndex = 110;
            this.label205.Text = "Altitude (57)";
            this.label205.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbThermostat
            // 
            this.cbThermostat.BackColor = System.Drawing.Color.White;
            this.cbThermostat.DropDownWidth = 300;
            this.cbThermostat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbThermostat.ForeColor = System.Drawing.Color.Black;
            this.cbThermostat.FormattingEnabled = true;
            this.cbThermostat.Location = new System.Drawing.Point(1201, 71);
            this.cbThermostat.Name = "cbThermostat";
            this.cbThermostat.Size = new System.Drawing.Size(210, 21);
            this.cbThermostat.TabIndex = 109;
            // 
            // label204
            // 
            this.label204.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label204.ForeColor = System.Drawing.Color.Teal;
            this.label204.Location = new System.Drawing.Point(968, 71);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(229, 23);
            this.label204.TabIndex = 108;
            this.label204.Text = "Thermostat (56)";
            this.label204.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbFaceAndBypassEvap
            // 
            this.cbFaceAndBypassEvap.BackColor = System.Drawing.Color.White;
            this.cbFaceAndBypassEvap.DropDownWidth = 300;
            this.cbFaceAndBypassEvap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFaceAndBypassEvap.ForeColor = System.Drawing.Color.Black;
            this.cbFaceAndBypassEvap.FormattingEnabled = true;
            this.cbFaceAndBypassEvap.Location = new System.Drawing.Point(1201, 45);
            this.cbFaceAndBypassEvap.Name = "cbFaceAndBypassEvap";
            this.cbFaceAndBypassEvap.Size = new System.Drawing.Size(210, 21);
            this.cbFaceAndBypassEvap.TabIndex = 107;
            // 
            // label203
            // 
            this.label203.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label203.ForeColor = System.Drawing.Color.Teal;
            this.label203.Location = new System.Drawing.Point(968, 45);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(229, 23);
            this.label203.TabIndex = 106;
            this.label203.Text = "Face and Bypass Evap (55)";
            this.label203.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCoolingControls
            // 
            this.cbCoolingControls.BackColor = System.Drawing.Color.White;
            this.cbCoolingControls.DropDownWidth = 300;
            this.cbCoolingControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCoolingControls.ForeColor = System.Drawing.Color.Black;
            this.cbCoolingControls.FormattingEnabled = true;
            this.cbCoolingControls.Location = new System.Drawing.Point(1201, 19);
            this.cbCoolingControls.Name = "cbCoolingControls";
            this.cbCoolingControls.Size = new System.Drawing.Size(210, 21);
            this.cbCoolingControls.TabIndex = 105;
            // 
            // label202
            // 
            this.label202.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label202.ForeColor = System.Drawing.Color.Teal;
            this.label202.Location = new System.Drawing.Point(968, 19);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(229, 23);
            this.label202.TabIndex = 104;
            this.label202.Text = "Cooling Controls (54)";
            this.label202.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbControlsDisplay
            // 
            this.cbControlsDisplay.BackColor = System.Drawing.Color.White;
            this.cbControlsDisplay.DropDownWidth = 300;
            this.cbControlsDisplay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbControlsDisplay.ForeColor = System.Drawing.Color.Black;
            this.cbControlsDisplay.FormattingEnabled = true;
            this.cbControlsDisplay.Location = new System.Drawing.Point(745, 546);
            this.cbControlsDisplay.Name = "cbControlsDisplay";
            this.cbControlsDisplay.Size = new System.Drawing.Size(214, 21);
            this.cbControlsDisplay.TabIndex = 103;
            // 
            // label201
            // 
            this.label201.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label201.ForeColor = System.Drawing.Color.Teal;
            this.label201.Location = new System.Drawing.Point(486, 546);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(253, 23);
            this.label201.TabIndex = 102;
            this.label201.Text = "Controls Display (53)";
            this.label201.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbConvenienceOutlet
            // 
            this.cbConvenienceOutlet.BackColor = System.Drawing.Color.White;
            this.cbConvenienceOutlet.DropDownWidth = 300;
            this.cbConvenienceOutlet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConvenienceOutlet.ForeColor = System.Drawing.Color.Black;
            this.cbConvenienceOutlet.FormattingEnabled = true;
            this.cbConvenienceOutlet.Location = new System.Drawing.Point(745, 519);
            this.cbConvenienceOutlet.Name = "cbConvenienceOutlet";
            this.cbConvenienceOutlet.Size = new System.Drawing.Size(214, 21);
            this.cbConvenienceOutlet.TabIndex = 101;
            // 
            // label200
            // 
            this.label200.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label200.ForeColor = System.Drawing.Color.Teal;
            this.label200.Location = new System.Drawing.Point(486, 519);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(253, 23);
            this.label200.TabIndex = 100;
            this.label200.Text = "Convenience Outlet (52)";
            this.label200.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbInstallation
            // 
            this.cbInstallation.BackColor = System.Drawing.Color.White;
            this.cbInstallation.DropDownWidth = 300;
            this.cbInstallation.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInstallation.ForeColor = System.Drawing.Color.Black;
            this.cbInstallation.FormattingEnabled = true;
            this.cbInstallation.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbInstallation.Location = new System.Drawing.Point(745, 492);
            this.cbInstallation.Name = "cbInstallation";
            this.cbInstallation.Size = new System.Drawing.Size(214, 21);
            this.cbInstallation.TabIndex = 99;
            // 
            // label199
            // 
            this.label199.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label199.ForeColor = System.Drawing.Color.Teal;
            this.label199.Location = new System.Drawing.Point(486, 493);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(253, 20);
            this.label199.TabIndex = 98;
            this.label199.Text = "Installation (51)";
            this.label199.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUV_Lights
            // 
            this.cbUV_Lights.BackColor = System.Drawing.Color.White;
            this.cbUV_Lights.DropDownWidth = 300;
            this.cbUV_Lights.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUV_Lights.ForeColor = System.Drawing.Color.Black;
            this.cbUV_Lights.FormattingEnabled = true;
            this.cbUV_Lights.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbUV_Lights.Location = new System.Drawing.Point(745, 465);
            this.cbUV_Lights.Name = "cbUV_Lights";
            this.cbUV_Lights.Size = new System.Drawing.Size(214, 21);
            this.cbUV_Lights.TabIndex = 97;
            // 
            // label198
            // 
            this.label198.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label198.ForeColor = System.Drawing.Color.Teal;
            this.label198.Location = new System.Drawing.Point(486, 466);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(253, 20);
            this.label198.TabIndex = 96;
            this.label198.Text = "UV LIghts (49)";
            this.label198.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbServiceLights
            // 
            this.cbServiceLights.BackColor = System.Drawing.Color.White;
            this.cbServiceLights.DropDownWidth = 300;
            this.cbServiceLights.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbServiceLights.ForeColor = System.Drawing.Color.Black;
            this.cbServiceLights.FormattingEnabled = true;
            this.cbServiceLights.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbServiceLights.Location = new System.Drawing.Point(746, 438);
            this.cbServiceLights.Name = "cbServiceLights";
            this.cbServiceLights.Size = new System.Drawing.Size(214, 21);
            this.cbServiceLights.TabIndex = 95;
            // 
            // label197
            // 
            this.label197.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label197.ForeColor = System.Drawing.Color.Teal;
            this.label197.Location = new System.Drawing.Point(487, 439);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(253, 20);
            this.label197.TabIndex = 94;
            this.label197.Text = "Service Lights (48)";
            this.label197.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHailguards
            // 
            this.cbHailguards.BackColor = System.Drawing.Color.White;
            this.cbHailguards.DropDownWidth = 300;
            this.cbHailguards.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHailguards.ForeColor = System.Drawing.Color.Black;
            this.cbHailguards.FormattingEnabled = true;
            this.cbHailguards.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbHailguards.Location = new System.Drawing.Point(747, 411);
            this.cbHailguards.Name = "cbHailguards";
            this.cbHailguards.Size = new System.Drawing.Size(214, 21);
            this.cbHailguards.TabIndex = 93;
            // 
            // label196
            // 
            this.label196.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label196.ForeColor = System.Drawing.Color.Teal;
            this.label196.Location = new System.Drawing.Point(488, 412);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(253, 20);
            this.label196.TabIndex = 92;
            this.label196.Text = "Hailguards (47)";
            this.label196.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSmokeDetector
            // 
            this.cbSmokeDetector.BackColor = System.Drawing.Color.White;
            this.cbSmokeDetector.DropDownWidth = 300;
            this.cbSmokeDetector.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSmokeDetector.ForeColor = System.Drawing.Color.Black;
            this.cbSmokeDetector.FormattingEnabled = true;
            this.cbSmokeDetector.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbSmokeDetector.Location = new System.Drawing.Point(746, 384);
            this.cbSmokeDetector.Name = "cbSmokeDetector";
            this.cbSmokeDetector.Size = new System.Drawing.Size(214, 21);
            this.cbSmokeDetector.TabIndex = 91;
            // 
            // label195
            // 
            this.label195.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label195.ForeColor = System.Drawing.Color.Teal;
            this.label195.Location = new System.Drawing.Point(487, 385);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(253, 20);
            this.label195.TabIndex = 90;
            this.label195.Text = "Smoke Detector (46)";
            this.label195.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustFanWheelDiamater
            // 
            this.cbExhaustFanWheelDiamater.BackColor = System.Drawing.Color.White;
            this.cbExhaustFanWheelDiamater.DropDownWidth = 300;
            this.cbExhaustFanWheelDiamater.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustFanWheelDiamater.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustFanWheelDiamater.FormattingEnabled = true;
            this.cbExhaustFanWheelDiamater.Location = new System.Drawing.Point(269, 519);
            this.cbExhaustFanWheelDiamater.Name = "cbExhaustFanWheelDiamater";
            this.cbExhaustFanWheelDiamater.Size = new System.Drawing.Size(210, 21);
            this.cbExhaustFanWheelDiamater.TabIndex = 89;
            // 
            // label194
            // 
            this.label194.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label194.ForeColor = System.Drawing.Color.Teal;
            this.label194.Location = new System.Drawing.Point(7, 519);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(253, 23);
            this.label194.TabIndex = 88;
            this.label194.Text = "Exhaust Fan Wheel Diameter (27/28)";
            this.label194.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustFanMotorType
            // 
            this.cbExhaustFanMotorType.BackColor = System.Drawing.Color.White;
            this.cbExhaustFanMotorType.DropDownWidth = 300;
            this.cbExhaustFanMotorType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustFanMotorType.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustFanMotorType.FormattingEnabled = true;
            this.cbExhaustFanMotorType.Location = new System.Drawing.Point(269, 492);
            this.cbExhaustFanMotorType.Name = "cbExhaustFanMotorType";
            this.cbExhaustFanMotorType.Size = new System.Drawing.Size(210, 21);
            this.cbExhaustFanMotorType.TabIndex = 87;
            // 
            // label193
            // 
            this.label193.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label193.ForeColor = System.Drawing.Color.Teal;
            this.label193.Location = new System.Drawing.Point(7, 492);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(253, 23);
            this.label193.TabIndex = 86;
            this.label193.Text = "Exhaust Fan Motor Type (26)";
            this.label193.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustFanMotor
            // 
            this.cbExhaustFanMotor.BackColor = System.Drawing.Color.White;
            this.cbExhaustFanMotor.DropDownWidth = 300;
            this.cbExhaustFanMotor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustFanMotor.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustFanMotor.FormattingEnabled = true;
            this.cbExhaustFanMotor.Location = new System.Drawing.Point(269, 465);
            this.cbExhaustFanMotor.Name = "cbExhaustFanMotor";
            this.cbExhaustFanMotor.Size = new System.Drawing.Size(210, 21);
            this.cbExhaustFanMotor.TabIndex = 85;
            // 
            // label192
            // 
            this.label192.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label192.ForeColor = System.Drawing.Color.Teal;
            this.label192.Location = new System.Drawing.Point(7, 465);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(253, 23);
            this.label192.TabIndex = 84;
            this.label192.Text = "Exhaust Fan Motor (25)";
            this.label192.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSupplyFanWheelDiameter
            // 
            this.cbSupplyFanWheelDiameter.BackColor = System.Drawing.Color.White;
            this.cbSupplyFanWheelDiameter.DropDownWidth = 300;
            this.cbSupplyFanWheelDiameter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyFanWheelDiameter.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyFanWheelDiameter.FormattingEnabled = true;
            this.cbSupplyFanWheelDiameter.Location = new System.Drawing.Point(269, 438);
            this.cbSupplyFanWheelDiameter.Name = "cbSupplyFanWheelDiameter";
            this.cbSupplyFanWheelDiameter.Size = new System.Drawing.Size(210, 21);
            this.cbSupplyFanWheelDiameter.TabIndex = 83;
            // 
            // label191
            // 
            this.label191.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label191.ForeColor = System.Drawing.Color.Teal;
            this.label191.Location = new System.Drawing.Point(7, 438);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(253, 23);
            this.label191.TabIndex = 82;
            this.label191.Text = "Supply Fan Wheel Diameter (23/24)";
            this.label191.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbFanPiezoRing
            // 
            this.cbFanPiezoRing.BackColor = System.Drawing.Color.White;
            this.cbFanPiezoRing.DropDownWidth = 300;
            this.cbFanPiezoRing.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFanPiezoRing.ForeColor = System.Drawing.Color.Black;
            this.cbFanPiezoRing.FormattingEnabled = true;
            this.cbFanPiezoRing.Location = new System.Drawing.Point(266, 546);
            this.cbFanPiezoRing.Name = "cbFanPiezoRing";
            this.cbFanPiezoRing.Size = new System.Drawing.Size(213, 21);
            this.cbFanPiezoRing.TabIndex = 81;
            // 
            // cbSupplyFanMotorType
            // 
            this.cbSupplyFanMotorType.BackColor = System.Drawing.Color.White;
            this.cbSupplyFanMotorType.DropDownWidth = 300;
            this.cbSupplyFanMotorType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyFanMotorType.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyFanMotorType.FormattingEnabled = true;
            this.cbSupplyFanMotorType.Location = new System.Drawing.Point(269, 411);
            this.cbSupplyFanMotorType.Name = "cbSupplyFanMotorType";
            this.cbSupplyFanMotorType.Size = new System.Drawing.Size(210, 21);
            this.cbSupplyFanMotorType.TabIndex = 80;
            // 
            // cbMajorDesign
            // 
            this.cbMajorDesign.BackColor = System.Drawing.Color.White;
            this.cbMajorDesign.DropDownWidth = 300;
            this.cbMajorDesign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMajorDesign.ForeColor = System.Drawing.Color.Black;
            this.cbMajorDesign.FormattingEnabled = true;
            this.cbMajorDesign.Items.AddRange(new object[] {
            "Rev4",
            "Rev5",
            "Heat Pump"});
            this.cbMajorDesign.Location = new System.Drawing.Point(269, 45);
            this.cbMajorDesign.Name = "cbMajorDesign";
            this.cbMajorDesign.Size = new System.Drawing.Size(210, 21);
            this.cbMajorDesign.TabIndex = 78;
            // 
            // label161
            // 
            this.label161.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.ForeColor = System.Drawing.Color.Teal;
            this.label161.Location = new System.Drawing.Point(7, 45);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(253, 20);
            this.label161.TabIndex = 77;
            this.label161.Text = "Major Design Sequence (4)";
            this.label161.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAirflowConfig
            // 
            this.cbAirflowConfig.BackColor = System.Drawing.Color.White;
            this.cbAirflowConfig.DropDownWidth = 300;
            this.cbAirflowConfig.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAirflowConfig.ForeColor = System.Drawing.Color.Black;
            this.cbAirflowConfig.FormattingEnabled = true;
            this.cbAirflowConfig.Items.AddRange(new object[] {
            "Vertical Discharge/Vertical Return",
            "Vertical Discharge/ Horizontal Return",
            "Horizontal Discharge/ Vertical Return",
            "Horizontal Discharge/ Horizontal Return"});
            this.cbAirflowConfig.Location = new System.Drawing.Point(269, 97);
            this.cbAirflowConfig.Name = "cbAirflowConfig";
            this.cbAirflowConfig.Size = new System.Drawing.Size(210, 21);
            this.cbAirflowConfig.TabIndex = 76;
            // 
            // label162
            // 
            this.label162.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.ForeColor = System.Drawing.Color.Teal;
            this.label162.Location = new System.Drawing.Point(7, 97);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(253, 20);
            this.label162.TabIndex = 75;
            this.label162.Text = "Airflow Configuration  (8)";
            this.label162.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSoundAttenuationPackage
            // 
            this.cbSoundAttenuationPackage.BackColor = System.Drawing.Color.White;
            this.cbSoundAttenuationPackage.DropDownWidth = 300;
            this.cbSoundAttenuationPackage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSoundAttenuationPackage.ForeColor = System.Drawing.Color.Black;
            this.cbSoundAttenuationPackage.FormattingEnabled = true;
            this.cbSoundAttenuationPackage.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbSoundAttenuationPackage.Location = new System.Drawing.Point(746, 357);
            this.cbSoundAttenuationPackage.Name = "cbSoundAttenuationPackage";
            this.cbSoundAttenuationPackage.Size = new System.Drawing.Size(214, 21);
            this.cbSoundAttenuationPackage.TabIndex = 74;
            // 
            // label163
            // 
            this.label163.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.ForeColor = System.Drawing.Color.Teal;
            this.label163.Location = new System.Drawing.Point(487, 358);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(253, 20);
            this.label163.TabIndex = 73;
            this.label163.Text = "Sound Attenuation Package (45)";
            this.label163.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCondenserFanOptions
            // 
            this.cbCondenserFanOptions.BackColor = System.Drawing.Color.White;
            this.cbCondenserFanOptions.DropDownWidth = 300;
            this.cbCondenserFanOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCondenserFanOptions.ForeColor = System.Drawing.Color.Black;
            this.cbCondenserFanOptions.FormattingEnabled = true;
            this.cbCondenserFanOptions.Items.AddRange(new object[] {
            "No Hailguards",
            "Hailguards"});
            this.cbCondenserFanOptions.Location = new System.Drawing.Point(746, 331);
            this.cbCondenserFanOptions.Name = "cbCondenserFanOptions";
            this.cbCondenserFanOptions.Size = new System.Drawing.Size(214, 21);
            this.cbCondenserFanOptions.TabIndex = 72;
            // 
            // label164
            // 
            this.label164.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.ForeColor = System.Drawing.Color.Teal;
            this.label164.Location = new System.Drawing.Point(487, 331);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(253, 20);
            this.label164.TabIndex = 71;
            this.label164.Text = " Condenser Fan Options (44)";
            this.label164.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbOutdoorAirMonitoring
            // 
            this.cbOutdoorAirMonitoring.BackColor = System.Drawing.Color.White;
            this.cbOutdoorAirMonitoring.DropDownWidth = 300;
            this.cbOutdoorAirMonitoring.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOutdoorAirMonitoring.ForeColor = System.Drawing.Color.Black;
            this.cbOutdoorAirMonitoring.FormattingEnabled = true;
            this.cbOutdoorAirMonitoring.Items.AddRange(new object[] {
            "No Airflow Monitoring",
            "Airflow Monitoring - IFM Fan Piezo Ring",
            "Airflow Monitoring - IFM with Display",
            "Airflow Monitoring - PE with Display",
            "Airflow Monitoring - Outdoor Air",
            "Airflow Monitoring - Outdoor Air with Display"});
            this.cbOutdoorAirMonitoring.Location = new System.Drawing.Point(746, 305);
            this.cbOutdoorAirMonitoring.Name = "cbOutdoorAirMonitoring";
            this.cbOutdoorAirMonitoring.Size = new System.Drawing.Size(214, 21);
            this.cbOutdoorAirMonitoring.TabIndex = 70;
            // 
            // label165
            // 
            this.label165.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.ForeColor = System.Drawing.Color.Teal;
            this.label165.Location = new System.Drawing.Point(487, 305);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(253, 20);
            this.label165.TabIndex = 69;
            this.label165.Text = "Outdoor Air Monitoring (43)";
            this.label165.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCorrisiveEnvironmentPackage
            // 
            this.cbCorrisiveEnvironmentPackage.BackColor = System.Drawing.Color.White;
            this.cbCorrisiveEnvironmentPackage.DropDownWidth = 300;
            this.cbCorrisiveEnvironmentPackage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCorrisiveEnvironmentPackage.ForeColor = System.Drawing.Color.Black;
            this.cbCorrisiveEnvironmentPackage.FormattingEnabled = true;
            this.cbCorrisiveEnvironmentPackage.Items.AddRange(new object[] {
            "Non-Fused Disconnect",
            "Fused Disconnect Switch",
            "Non-Fused Disconnect w/Convenience Outlet",
            "Fused Disconnect Switch w/Convenience Outlet",
            "Dual Point Power"});
            this.cbCorrisiveEnvironmentPackage.Location = new System.Drawing.Point(746, 279);
            this.cbCorrisiveEnvironmentPackage.Name = "cbCorrisiveEnvironmentPackage";
            this.cbCorrisiveEnvironmentPackage.Size = new System.Drawing.Size(214, 21);
            this.cbCorrisiveEnvironmentPackage.TabIndex = 68;
            // 
            // label166
            // 
            this.label166.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.ForeColor = System.Drawing.Color.Teal;
            this.label166.Location = new System.Drawing.Point(487, 279);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(253, 20);
            this.label166.TabIndex = 67;
            this.label166.Text = "Corrisive Environment Package (42)";
            this.label166.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbElectricalOptions
            // 
            this.cbElectricalOptions.BackColor = System.Drawing.Color.White;
            this.cbElectricalOptions.DropDownWidth = 300;
            this.cbElectricalOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbElectricalOptions.ForeColor = System.Drawing.Color.Black;
            this.cbElectricalOptions.FormattingEnabled = true;
            this.cbElectricalOptions.Items.AddRange(new object[] {
            "No Smoke Detector",
            "Supply Smoke Detector",
            "Return Smoke Detector",
            "Supply & Return Detector"});
            this.cbElectricalOptions.Location = new System.Drawing.Point(746, 253);
            this.cbElectricalOptions.Name = "cbElectricalOptions";
            this.cbElectricalOptions.Size = new System.Drawing.Size(214, 21);
            this.cbElectricalOptions.TabIndex = 66;
            // 
            // label167
            // 
            this.label167.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.ForeColor = System.Drawing.Color.Teal;
            this.label167.Location = new System.Drawing.Point(487, 253);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(253, 20);
            this.label167.TabIndex = 65;
            this.label167.Text = "Electrical Options (41)";
            this.label167.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDamperOptions
            // 
            this.cbDamperOptions.BackColor = System.Drawing.Color.White;
            this.cbDamperOptions.DropDownWidth = 300;
            this.cbDamperOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDamperOptions.ForeColor = System.Drawing.Color.Black;
            this.cbDamperOptions.FormattingEnabled = true;
            this.cbDamperOptions.Items.AddRange(new object[] {
            "100 % OA 2-Position Damper",
            "100 % OA 2-Position Damper w/RA 2-Position Damper",
            "Modulating Mixed Air Damper"});
            this.cbDamperOptions.Location = new System.Drawing.Point(746, 201);
            this.cbDamperOptions.Name = "cbDamperOptions";
            this.cbDamperOptions.Size = new System.Drawing.Size(214, 21);
            this.cbDamperOptions.TabIndex = 64;
            // 
            // label168
            // 
            this.label168.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.ForeColor = System.Drawing.Color.Teal;
            this.label168.Location = new System.Drawing.Point(487, 201);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(253, 20);
            this.label168.TabIndex = 63;
            this.label168.Text = "Damper Options (38)";
            this.label168.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbERV_RotationSensor
            // 
            this.cbERV_RotationSensor.BackColor = System.Drawing.Color.White;
            this.cbERV_RotationSensor.DropDownWidth = 300;
            this.cbERV_RotationSensor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbERV_RotationSensor.ForeColor = System.Drawing.Color.Black;
            this.cbERV_RotationSensor.FormattingEnabled = true;
            this.cbERV_RotationSensor.Items.AddRange(new object[] {
            "No ERV",
            "3014C",
            "3622C",
            "4136C",
            "4634C",
            "5856C",
            "6488C",
            "6876C",
            "74122C"});
            this.cbERV_RotationSensor.Location = new System.Drawing.Point(746, 175);
            this.cbERV_RotationSensor.Name = "cbERV_RotationSensor";
            this.cbERV_RotationSensor.Size = new System.Drawing.Size(214, 21);
            this.cbERV_RotationSensor.TabIndex = 62;
            // 
            // label169
            // 
            this.label169.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.ForeColor = System.Drawing.Color.Teal;
            this.label169.Location = new System.Drawing.Point(487, 175);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(253, 20);
            this.label169.TabIndex = 61;
            this.label169.Text = "ERV Rotation Sensor (37)";
            this.label169.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEnergyWheelRecoverySize
            // 
            this.cbEnergyWheelRecoverySize.BackColor = System.Drawing.Color.White;
            this.cbEnergyWheelRecoverySize.DropDownWidth = 300;
            this.cbEnergyWheelRecoverySize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEnergyWheelRecoverySize.ForeColor = System.Drawing.Color.Black;
            this.cbEnergyWheelRecoverySize.FormattingEnabled = true;
            this.cbEnergyWheelRecoverySize.Items.AddRange(new object[] {
            "No ERV",
            "ERV-Composite Contruction",
            "ERV-Composite Contruction with Frost Protection",
            "ERV-Composite Contruction with Bypass",
            "ERV-Composite Contruction with Frost Protection & Bypass",
            "ERV-Aluminum Contruction",
            "ERV-Aluminum Contruction with Frost Protection",
            "ERV-Aluminum Contruction with Bypass",
            "ERV-Aluminum Contruction with Frost Protection & Bypass"});
            this.cbEnergyWheelRecoverySize.Location = new System.Drawing.Point(746, 149);
            this.cbEnergyWheelRecoverySize.Name = "cbEnergyWheelRecoverySize";
            this.cbEnergyWheelRecoverySize.Size = new System.Drawing.Size(214, 21);
            this.cbEnergyWheelRecoverySize.TabIndex = 60;
            // 
            // label170
            // 
            this.label170.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.ForeColor = System.Drawing.Color.Teal;
            this.label170.Location = new System.Drawing.Point(487, 149);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(253, 20);
            this.label170.TabIndex = 59;
            this.label170.Text = "Energy Wheel Recovery Size (36)";
            this.label170.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEnergyRecoveryWheelOptions
            // 
            this.cbEnergyRecoveryWheelOptions.BackColor = System.Drawing.Color.White;
            this.cbEnergyRecoveryWheelOptions.DropDownWidth = 300;
            this.cbEnergyRecoveryWheelOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEnergyRecoveryWheelOptions.ForeColor = System.Drawing.Color.Black;
            this.cbEnergyRecoveryWheelOptions.FormattingEnabled = true;
            this.cbEnergyRecoveryWheelOptions.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbEnergyRecoveryWheelOptions.Location = new System.Drawing.Point(746, 123);
            this.cbEnergyRecoveryWheelOptions.Name = "cbEnergyRecoveryWheelOptions";
            this.cbEnergyRecoveryWheelOptions.Size = new System.Drawing.Size(214, 21);
            this.cbEnergyRecoveryWheelOptions.TabIndex = 58;
            // 
            // label171
            // 
            this.label171.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.Teal;
            this.label171.Location = new System.Drawing.Point(487, 123);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(253, 20);
            this.label171.TabIndex = 57;
            this.label171.Text = "Energy Recovery Wheel Options (35)";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEnergyRecovery
            // 
            this.cbEnergyRecovery.BackColor = System.Drawing.Color.White;
            this.cbEnergyRecovery.DropDownWidth = 300;
            this.cbEnergyRecovery.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEnergyRecovery.ForeColor = System.Drawing.Color.Black;
            this.cbEnergyRecovery.FormattingEnabled = true;
            this.cbEnergyRecovery.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbEnergyRecovery.Location = new System.Drawing.Point(746, 97);
            this.cbEnergyRecovery.Name = "cbEnergyRecovery";
            this.cbEnergyRecovery.Size = new System.Drawing.Size(214, 21);
            this.cbEnergyRecovery.TabIndex = 56;
            // 
            // label172
            // 
            this.label172.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.ForeColor = System.Drawing.Color.Teal;
            this.label172.Location = new System.Drawing.Point(487, 97);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(253, 20);
            this.label172.TabIndex = 55;
            this.label172.Text = "Energy Recovery (34)";
            this.label172.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbFilterOptions
            // 
            this.cbFilterOptions.BackColor = System.Drawing.Color.White;
            this.cbFilterOptions.DropDownWidth = 300;
            this.cbFilterOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFilterOptions.ForeColor = System.Drawing.Color.Black;
            this.cbFilterOptions.FormattingEnabled = true;
            this.cbFilterOptions.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbFilterOptions.Location = new System.Drawing.Point(746, 71);
            this.cbFilterOptions.Name = "cbFilterOptions";
            this.cbFilterOptions.Size = new System.Drawing.Size(214, 21);
            this.cbFilterOptions.TabIndex = 54;
            // 
            // label173
            // 
            this.label173.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.ForeColor = System.Drawing.Color.Teal;
            this.label173.Location = new System.Drawing.Point(487, 71);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(253, 20);
            this.label173.TabIndex = 53;
            this.label173.Text = "Filter Options (33)";
            this.label173.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbBuildingInterface
            // 
            this.cbBuildingInterface.BackColor = System.Drawing.Color.White;
            this.cbBuildingInterface.DropDownWidth = 300;
            this.cbBuildingInterface.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBuildingInterface.ForeColor = System.Drawing.Color.Black;
            this.cbBuildingInterface.FormattingEnabled = true;
            this.cbBuildingInterface.Items.AddRange(new object[] {
            "Non DDC - Electromechanical",
            "Trane - Outdoor Air Control w/LON Read-Write w/Display",
            "Trane - Space Control w/LON Read-Write w/Display",
            "Trane - Outdoor Air Control w/BACNET (No Display)",
            "Trane - Space Control w/BACNET  (No Display)",
            "Trane - Discharge Air Control w/BACNET  (No Display)",
            "Trane - Outdoor Air Control w/BACNET  w/Display",
            "Trane - Space Control w/BACNET w/Display",
            "Trane - Discharge Air Control w/BACNET w/Display"});
            this.cbBuildingInterface.Location = new System.Drawing.Point(746, 45);
            this.cbBuildingInterface.Name = "cbBuildingInterface";
            this.cbBuildingInterface.Size = new System.Drawing.Size(214, 21);
            this.cbBuildingInterface.TabIndex = 52;
            // 
            // label174
            // 
            this.label174.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.ForeColor = System.Drawing.Color.Teal;
            this.label174.Location = new System.Drawing.Point(487, 45);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(253, 20);
            this.label174.TabIndex = 51;
            this.label174.Text = "Building Interface (32)";
            this.label174.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitControls
            // 
            this.cbUnitControls.BackColor = System.Drawing.Color.White;
            this.cbUnitControls.DropDownWidth = 300;
            this.cbUnitControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUnitControls.ForeColor = System.Drawing.Color.Black;
            this.cbUnitControls.FormattingEnabled = true;
            this.cbUnitControls.Items.AddRange(new object[] {
            "No Corrosive Package",
            "S/S Cabinet, Basepan, Eco Coated Coils",
            "S/S Cabinet, Basepan",
            "S/S Basepan, Eco Coated Coils",
            "S/S Coil Casing",
            "S/S Interior Casing",
            "Eco Coated Coils"});
            this.cbUnitControls.Location = new System.Drawing.Point(746, 19);
            this.cbUnitControls.Name = "cbUnitControls";
            this.cbUnitControls.Size = new System.Drawing.Size(214, 21);
            this.cbUnitControls.TabIndex = 50;
            // 
            // label175
            // 
            this.label175.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.ForeColor = System.Drawing.Color.Teal;
            this.label175.Location = new System.Drawing.Point(487, 19);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(253, 20);
            this.label175.TabIndex = 49;
            this.label175.Text = "Unit Controls (31)";
            this.label175.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label176
            // 
            this.label176.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.ForeColor = System.Drawing.Color.Teal;
            this.label176.Location = new System.Drawing.Point(7, 546);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(253, 23);
            this.label176.TabIndex = 44;
            this.label176.Text = "Fan Piezo Ring (29)";
            this.label176.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelInputsHeatCapacityLabel
            // 
            this.labelInputsHeatCapacityLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInputsHeatCapacityLabel.ForeColor = System.Drawing.Color.Teal;
            this.labelInputsHeatCapacityLabel.Location = new System.Drawing.Point(7, 411);
            this.labelInputsHeatCapacityLabel.Name = "labelInputsHeatCapacityLabel";
            this.labelInputsHeatCapacityLabel.Size = new System.Drawing.Size(253, 23);
            this.labelInputsHeatCapacityLabel.TabIndex = 40;
            this.labelInputsHeatCapacityLabel.Text = "Supply Fan Motor Type (22)";
            this.labelInputsHeatCapacityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSupplyFanMotor
            // 
            this.cbSupplyFanMotor.BackColor = System.Drawing.Color.White;
            this.cbSupplyFanMotor.DropDownWidth = 300;
            this.cbSupplyFanMotor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSupplyFanMotor.ForeColor = System.Drawing.Color.Black;
            this.cbSupplyFanMotor.FormattingEnabled = true;
            this.cbSupplyFanMotor.Items.AddRange(new object[] {
            "No Heat",
            "Natural Gas",
            "Propane",
            "Electric - Open Coil",
            "Electric - Sheathed Coil",
            "Hot Water",
            "Steam"});
            this.cbSupplyFanMotor.Location = new System.Drawing.Point(269, 384);
            this.cbSupplyFanMotor.Name = "cbSupplyFanMotor";
            this.cbSupplyFanMotor.Size = new System.Drawing.Size(210, 21);
            this.cbSupplyFanMotor.TabIndex = 39;
            // 
            // label177
            // 
            this.label177.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.ForeColor = System.Drawing.Color.Teal;
            this.label177.Location = new System.Drawing.Point(7, 384);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(253, 23);
            this.label177.TabIndex = 38;
            this.label177.Text = "Supply Fan Motor (21)";
            this.label177.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHeatTypeSecondary
            // 
            this.cbHeatTypeSecondary.BackColor = System.Drawing.Color.White;
            this.cbHeatTypeSecondary.DropDownWidth = 300;
            this.cbHeatTypeSecondary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatTypeSecondary.ForeColor = System.Drawing.Color.Black;
            this.cbHeatTypeSecondary.FormattingEnabled = true;
            this.cbHeatTypeSecondary.Items.AddRange(new object[] {
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbHeatTypeSecondary.Location = new System.Drawing.Point(269, 332);
            this.cbHeatTypeSecondary.Name = "cbHeatTypeSecondary";
            this.cbHeatTypeSecondary.Size = new System.Drawing.Size(210, 21);
            this.cbHeatTypeSecondary.TabIndex = 37;
            this.cbHeatTypeSecondary.SelectedIndexChanged += new System.EventHandler(this.cbHeatTypeSecondary_SelectedIndexChanged);
            // 
            // label178
            // 
            this.label178.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.ForeColor = System.Drawing.Color.Teal;
            this.label178.Location = new System.Drawing.Point(7, 332);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(253, 20);
            this.label178.TabIndex = 36;
            this.label178.Text = "Heat Type Secondary (18)";
            this.label178.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHeatCapacityPrimary
            // 
            this.cbHeatCapacityPrimary.BackColor = System.Drawing.Color.White;
            this.cbHeatCapacityPrimary.DropDownWidth = 300;
            this.cbHeatCapacityPrimary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatCapacityPrimary.ForeColor = System.Drawing.Color.Black;
            this.cbHeatCapacityPrimary.FormattingEnabled = true;
            this.cbHeatCapacityPrimary.Items.AddRange(new object[] {
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbHeatCapacityPrimary.Location = new System.Drawing.Point(269, 306);
            this.cbHeatCapacityPrimary.Name = "cbHeatCapacityPrimary";
            this.cbHeatCapacityPrimary.Size = new System.Drawing.Size(210, 21);
            this.cbHeatCapacityPrimary.TabIndex = 35;
            // 
            // label179
            // 
            this.label179.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.ForeColor = System.Drawing.Color.Teal;
            this.label179.Location = new System.Drawing.Point(7, 306);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(253, 20);
            this.label179.TabIndex = 34;
            this.label179.Text = "Heat Capacity Primary (17)";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHeatTypePrimary
            // 
            this.cbHeatTypePrimary.BackColor = System.Drawing.Color.White;
            this.cbHeatTypePrimary.DropDownWidth = 300;
            this.cbHeatTypePrimary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatTypePrimary.ForeColor = System.Drawing.Color.Black;
            this.cbHeatTypePrimary.FormattingEnabled = true;
            this.cbHeatTypePrimary.Items.AddRange(new object[] {
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbHeatTypePrimary.Location = new System.Drawing.Point(269, 280);
            this.cbHeatTypePrimary.Name = "cbHeatTypePrimary";
            this.cbHeatTypePrimary.Size = new System.Drawing.Size(210, 21);
            this.cbHeatTypePrimary.TabIndex = 33;
            this.cbHeatTypePrimary.SelectedIndexChanged += new System.EventHandler(this.cbHeatTypePrimary_SelectedIndexChanged);
            // 
            // label180
            // 
            this.label180.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.ForeColor = System.Drawing.Color.Teal;
            this.label180.Location = new System.Drawing.Point(7, 280);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(253, 20);
            this.label180.TabIndex = 32;
            this.label180.Text = "Heat Type Primary (16)";
            this.label180.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbRefridgerantCapacityControl
            // 
            this.cbRefridgerantCapacityControl.BackColor = System.Drawing.Color.White;
            this.cbRefridgerantCapacityControl.DropDownWidth = 300;
            this.cbRefridgerantCapacityControl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRefridgerantCapacityControl.ForeColor = System.Drawing.Color.Black;
            this.cbRefridgerantCapacityControl.FormattingEnabled = true;
            this.cbRefridgerantCapacityControl.Items.AddRange(new object[] {
            "No RCC Valve",
            "RCC Valve on 1st Circuit",
            "RCC Valve on 1st Circuit & 2nd Circuit",
            "ERCC Valve on 1st Circuit",
            "ERCC Valve on 1st Circuit & 2nd Circuit",
            "HGBP Valve on 1st Circuit",
            "HGBP Valve on 1st Circuit & 2nd Circuit"});
            this.cbRefridgerantCapacityControl.Location = new System.Drawing.Point(269, 254);
            this.cbRefridgerantCapacityControl.Name = "cbRefridgerantCapacityControl";
            this.cbRefridgerantCapacityControl.Size = new System.Drawing.Size(210, 21);
            this.cbRefridgerantCapacityControl.TabIndex = 31;
            // 
            // label181
            // 
            this.label181.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label181.ForeColor = System.Drawing.Color.Teal;
            this.label181.Location = new System.Drawing.Point(7, 254);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(253, 20);
            this.label181.TabIndex = 30;
            this.label181.Text = "Refrigerant Capacity Control (15)";
            this.label181.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label182
            // 
            this.label182.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label182.ForeColor = System.Drawing.Color.Teal;
            this.label182.Location = new System.Drawing.Point(7, 150);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(253, 20);
            this.label182.TabIndex = 29;
            this.label182.Text = "Indoor Coil Type (11)";
            this.label182.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbIndoorCoilType
            // 
            this.cbIndoorCoilType.BackColor = System.Drawing.Color.White;
            this.cbIndoorCoilType.DropDownWidth = 300;
            this.cbIndoorCoilType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIndoorCoilType.ForeColor = System.Drawing.Color.Black;
            this.cbIndoorCoilType.FormattingEnabled = true;
            this.cbIndoorCoilType.Items.AddRange(new object[] {
            "No Cooling",
            "DX 3-Row",
            "DX 4-Row",
            "DX 4-Row Interlaced",
            "DX 6-Row Interlaced",
            "DX 8-Row",
            "Glycol/Chilled Water Coil"});
            this.cbIndoorCoilType.Location = new System.Drawing.Point(269, 150);
            this.cbIndoorCoilType.Name = "cbIndoorCoilType";
            this.cbIndoorCoilType.Size = new System.Drawing.Size(210, 21);
            this.cbIndoorCoilType.TabIndex = 28;
            // 
            // cbCoolingCapacity
            // 
            this.cbCoolingCapacity.BackColor = System.Drawing.Color.White;
            this.cbCoolingCapacity.DropDownWidth = 300;
            this.cbCoolingCapacity.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCoolingCapacity.ForeColor = System.Drawing.Color.Black;
            this.cbCoolingCapacity.FormattingEnabled = true;
            this.cbCoolingCapacity.Items.AddRange(new object[] {
            "No Cooling",
            "5 Tons High Efficiency",
            "6 Tons High Efficiency",
            "7 Tons High Efficiency",
            "8 Tons High Efficiency",
            "10 Tons High Efficiency",
            "12 Tons High Efficiency",
            "15 Tons High Efficiency",
            "17 Tons High Efficiency",
            "20 Tons High Efficiency",
            "22 Tons High Efficiency",
            "25 Tons High Efficiency",
            "30 Tons High Efficiency",
            "35 Tons High Efficiency",
            "40 Tons High Efficiency",
            "45 Tons High Efficiency",
            "50 Tons High Efficiency",
            "54 Tons High Efficiency"});
            this.cbCoolingCapacity.Location = new System.Drawing.Point(269, 71);
            this.cbCoolingCapacity.Name = "cbCoolingCapacity";
            this.cbCoolingCapacity.Size = new System.Drawing.Size(210, 21);
            this.cbCoolingCapacity.TabIndex = 26;
            // 
            // label183
            // 
            this.label183.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label183.ForeColor = System.Drawing.Color.Teal;
            this.label183.Location = new System.Drawing.Point(7, 71);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(253, 20);
            this.label183.TabIndex = 25;
            this.label183.Text = "Cooling Capacity (567)";
            this.label183.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbExhaustDampers
            // 
            this.cbExhaustDampers.BackColor = System.Drawing.Color.White;
            this.cbExhaustDampers.DropDownWidth = 300;
            this.cbExhaustDampers.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExhaustDampers.ForeColor = System.Drawing.Color.Black;
            this.cbExhaustDampers.FormattingEnabled = true;
            this.cbExhaustDampers.Items.AddRange(new object[] {
            "Aluminum Mesh Intake Filters (ALM)",
            "MERV-8, 30%, and ALM",
            "MERV-13, 80%, and ALM",
            "MERV-14, 95%, and ALM",
            "Aluminum Mesh Intake Filters (ALM), w/UVC",
            "MERV-8, 30%, and ALM, w/UVC",
            "MERV-13, 80%, and ALM, w/UVC",
            "MERV-14, 95%, and ALM, w/UVC",
            "Aluminum Mesh Intake Filters (ALM), w/UVC & Electrostatic Filters",
            "MERV-8, 30%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-13, 80%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-14, 95%, and ALM, w/UVC & Electrostatic Filters",
            "Aluminum Mesh Intake Filters (ALM) & Electrostatic Filters",
            "MERV-8, 30%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM & Electrostatic Filters",
            "MERV-14, 95%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM w/TCACS",
            "MERV-14, 95%, and ALM w/TCACS",
            "Special Filter Options"});
            this.cbExhaustDampers.Location = new System.Drawing.Point(746, 227);
            this.cbExhaustDampers.Name = "cbExhaustDampers";
            this.cbExhaustDampers.Size = new System.Drawing.Size(214, 21);
            this.cbExhaustDampers.TabIndex = 22;
            // 
            // label184
            // 
            this.label184.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label184.ForeColor = System.Drawing.Color.Teal;
            this.label184.Location = new System.Drawing.Point(487, 226);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(253, 20);
            this.label184.TabIndex = 21;
            this.label184.Text = "Exhaust Dampers (39)";
            this.label184.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbOutdoorCoilType
            // 
            this.cbOutdoorCoilType.BackColor = System.Drawing.Color.White;
            this.cbOutdoorCoilType.DropDownWidth = 300;
            this.cbOutdoorCoilType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOutdoorCoilType.ForeColor = System.Drawing.Color.Black;
            this.cbOutdoorCoilType.FormattingEnabled = true;
            this.cbOutdoorCoilType.Items.AddRange(new object[] {
            "No Condenser",
            "Air Cooled",
            "Air Cooled w/Head Pressure on/off control",
            "Water Source Heat Pump",
            "Air Cooled Fin & Tube w/ Head Pressure Variable Speed",
            "Air Cooled Micro Channel",
            "Air Cooled Micro Channel w/Head Pressure on/off control",
            "Air Cooled Micro Channel Variable Speed",
            "Water Cooled DX Condenser Copper/Nickel"});
            this.cbOutdoorCoilType.Location = new System.Drawing.Point(269, 228);
            this.cbOutdoorCoilType.Name = "cbOutdoorCoilType";
            this.cbOutdoorCoilType.Size = new System.Drawing.Size(210, 21);
            this.cbOutdoorCoilType.TabIndex = 18;
            // 
            // label185
            // 
            this.label185.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label185.ForeColor = System.Drawing.Color.Teal;
            this.label185.Location = new System.Drawing.Point(7, 228);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(253, 20);
            this.label185.TabIndex = 17;
            this.label185.Text = "Outdoor Coil Type (14)";
            this.label185.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHotGasReheat
            // 
            this.cbHotGasReheat.BackColor = System.Drawing.Color.White;
            this.cbHotGasReheat.DropDownWidth = 300;
            this.cbHotGasReheat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHotGasReheat.ForeColor = System.Drawing.Color.Black;
            this.cbHotGasReheat.FormattingEnabled = true;
            this.cbHotGasReheat.Items.AddRange(new object[] {
            "No HGRH",
            "Modulating",
            "On/Off"});
            this.cbHotGasReheat.Location = new System.Drawing.Point(269, 176);
            this.cbHotGasReheat.Name = "cbHotGasReheat";
            this.cbHotGasReheat.Size = new System.Drawing.Size(210, 21);
            this.cbHotGasReheat.TabIndex = 16;
            // 
            // label186
            // 
            this.label186.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label186.ForeColor = System.Drawing.Color.Teal;
            this.label186.Location = new System.Drawing.Point(7, 176);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(253, 20);
            this.label186.TabIndex = 15;
            this.label186.Text = "Hot Gas Reheat (12)";
            this.label186.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCompressor
            // 
            this.cbCompressor.BackColor = System.Drawing.Color.White;
            this.cbCompressor.DropDownWidth = 300;
            this.cbCompressor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCompressor.ForeColor = System.Drawing.Color.Black;
            this.cbCompressor.FormattingEnabled = true;
            this.cbCompressor.Items.AddRange(new object[] {
            "No Compressor",
            "Scroll Compressor",
            "Digital Scroll-1st Circuit Only",
            "Digital Scroll-1st Circuit & 2nd Circuit",
            "Variable Speed Scroll"});
            this.cbCompressor.Location = new System.Drawing.Point(269, 202);
            this.cbCompressor.Name = "cbCompressor";
            this.cbCompressor.Size = new System.Drawing.Size(210, 21);
            this.cbCompressor.TabIndex = 14;
            // 
            // cbHeatCapacitySecondary
            // 
            this.cbHeatCapacitySecondary.BackColor = System.Drawing.Color.White;
            this.cbHeatCapacitySecondary.DropDownWidth = 300;
            this.cbHeatCapacitySecondary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatCapacitySecondary.ForeColor = System.Drawing.Color.Black;
            this.cbHeatCapacitySecondary.FormattingEnabled = true;
            this.cbHeatCapacitySecondary.Items.AddRange(new object[] {
            "No Heat",
            "Indirect - Fired (IF)",
            "Direct - Fired (DF)",
            "Electric - 2 Stage",
            "Electric - SCR Modulating",
            "Dual Fuel (PRI-DF/SEC-IF)",
            "Dual Fuel (PRI-DF/SEC-ELEC)",
            "Dual Fuel (PRI-IF/SEC-ELEC)",
            "Dual Fuel (PRI-ELEC/SEC-ELEC)",
            "Hot Water",
            "Steam"});
            this.cbHeatCapacitySecondary.Location = new System.Drawing.Point(269, 358);
            this.cbHeatCapacitySecondary.Name = "cbHeatCapacitySecondary";
            this.cbHeatCapacitySecondary.Size = new System.Drawing.Size(210, 21);
            this.cbHeatCapacitySecondary.TabIndex = 14;
            // 
            // label187
            // 
            this.label187.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label187.ForeColor = System.Drawing.Color.Teal;
            this.label187.Location = new System.Drawing.Point(7, 202);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(253, 20);
            this.label187.TabIndex = 13;
            this.label187.Text = "Compressor (13)";
            this.label187.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label188
            // 
            this.label188.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label188.ForeColor = System.Drawing.Color.Teal;
            this.label188.Location = new System.Drawing.Point(7, 358);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(253, 23);
            this.label188.TabIndex = 13;
            this.label188.Text = "Heat Capacity Secondary (19)";
            this.label188.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbVoltage
            // 
            this.cbVoltage.BackColor = System.Drawing.Color.White;
            this.cbVoltage.DropDownWidth = 300;
            this.cbVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVoltage.ForeColor = System.Drawing.Color.Black;
            this.cbVoltage.FormattingEnabled = true;
            this.cbVoltage.Items.AddRange(new object[] {
            "115/60/1",
            "208-230/60/1",
            "208-230/60/3",
            "460/60/3",
            "575/60/3"});
            this.cbVoltage.Location = new System.Drawing.Point(269, 124);
            this.cbVoltage.Name = "cbVoltage";
            this.cbVoltage.Size = new System.Drawing.Size(210, 21);
            this.cbVoltage.TabIndex = 12;
            // 
            // label189
            // 
            this.label189.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label189.ForeColor = System.Drawing.Color.Teal;
            this.label189.Location = new System.Drawing.Point(7, 124);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(253, 20);
            this.label189.TabIndex = 11;
            this.label189.Text = "Voltage/Phase (9)";
            this.label189.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCabinet
            // 
            this.cbCabinet.BackColor = System.Drawing.Color.White;
            this.cbCabinet.DropDownWidth = 300;
            this.cbCabinet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCabinet.ForeColor = System.Drawing.Color.Black;
            this.cbCabinet.FormattingEnabled = true;
            this.cbCabinet.Items.AddRange(new object[] {
            "625 - 3,600 cfm",
            "1,500 - 9,000 cfm",
            "3,500 - 13,500 cfm"});
            this.cbCabinet.Location = new System.Drawing.Point(269, 19);
            this.cbCabinet.Name = "cbCabinet";
            this.cbCabinet.Size = new System.Drawing.Size(210, 21);
            this.cbCabinet.TabIndex = 1;
            // 
            // label190
            // 
            this.label190.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label190.ForeColor = System.Drawing.Color.Teal;
            this.label190.Location = new System.Drawing.Point(7, 19);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(253, 20);
            this.label190.TabIndex = 0;
            this.label190.Text = "Cabinet (3)";
            this.label190.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1336, 770);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 25);
            this.btnExit.TabIndex = 288;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Blue;
            this.btnPrint.Location = new System.Drawing.Point(1082, 770);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 25);
            this.btnPrint.TabIndex = 289;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCreateBOM
            // 
            this.btnCreateBOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateBOM.ForeColor = System.Drawing.Color.Teal;
            this.btnCreateBOM.Location = new System.Drawing.Point(1210, 771);
            this.btnCreateBOM.Name = "btnCreateBOM";
            this.btnCreateBOM.Size = new System.Drawing.Size(100, 25);
            this.btnCreateBOM.TabIndex = 290;
            this.btnCreateBOM.Text = "Create BOM";
            this.btnCreateBOM.UseVisualStyleBackColor = true;
            this.btnCreateBOM.Click += new System.EventHandler(this.btnCreateBOM_Click);
            // 
            // txtModelNo
            // 
            this.txtModelNo.Location = new System.Drawing.Point(127, 773);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(473, 20);
            this.txtModelNo.TabIndex = 291;
            this.txtModelNo.Visible = false;
            // 
            // lbEnterModelNo
            // 
            this.lbEnterModelNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnterModelNo.ForeColor = System.Drawing.Color.Teal;
            this.lbEnterModelNo.Location = new System.Drawing.Point(17, 772);
            this.lbEnterModelNo.Name = "lbEnterModelNo";
            this.lbEnterModelNo.Size = new System.Drawing.Size(101, 20);
            this.lbEnterModelNo.TabIndex = 292;
            this.lbEnterModelNo.Text = "Enter ModelNo:";
            this.lbEnterModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbEnterModelNo.Visible = false;
            // 
            // btnParseModelNo
            // 
            this.btnParseModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParseModelNo.ForeColor = System.Drawing.Color.Teal;
            this.btnParseModelNo.Location = new System.Drawing.Point(629, 770);
            this.btnParseModelNo.Name = "btnParseModelNo";
            this.btnParseModelNo.Size = new System.Drawing.Size(100, 25);
            this.btnParseModelNo.TabIndex = 293;
            this.btnParseModelNo.Text = "Parse Model#";
            this.btnParseModelNo.UseVisualStyleBackColor = true;
            this.btnParseModelNo.Visible = false;
            this.btnParseModelNo.Click += new System.EventHandler(this.btnParseModelNo_Click);
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Black;
            this.lbCustName.Location = new System.Drawing.Point(446, 79);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(537, 25);
            this.lbCustName.TabIndex = 295;
            this.lbCustName.Text = "CustomerName";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label212
            // 
            this.label212.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label212.ForeColor = System.Drawing.Color.Black;
            this.label212.Location = new System.Drawing.Point(1129, 157);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(13, 15);
            this.label212.TabIndex = 298;
            this.label212.Text = "1";
            // 
            // label213
            // 
            this.label213.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label213.ForeColor = System.Drawing.Color.Black;
            this.label213.Location = new System.Drawing.Point(1129, 142);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(13, 15);
            this.label213.TabIndex = 297;
            this.label213.Text = "6";
            // 
            // label214
            // 
            this.label214.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label214.ForeColor = System.Drawing.Color.Black;
            this.label214.Location = new System.Drawing.Point(1129, 134);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(13, 15);
            this.label214.TabIndex = 296;
            this.label214.Text = "^";
            // 
            // label215
            // 
            this.label215.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label215.ForeColor = System.Drawing.Color.Black;
            this.label215.Location = new System.Drawing.Point(1258, 157);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(13, 15);
            this.label215.TabIndex = 322;
            this.label215.Text = "9";
            // 
            // label216
            // 
            this.label216.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label216.ForeColor = System.Drawing.Color.Black;
            this.label216.Location = new System.Drawing.Point(1258, 142);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(13, 15);
            this.label216.TabIndex = 321;
            this.label216.Text = "6";
            // 
            // label217
            // 
            this.label217.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label217.ForeColor = System.Drawing.Color.Black;
            this.label217.Location = new System.Drawing.Point(1258, 134);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(13, 15);
            this.label217.TabIndex = 320;
            this.label217.Text = "^";
            // 
            // label218
            // 
            this.label218.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label218.ForeColor = System.Drawing.Color.Black;
            this.label218.Location = new System.Drawing.Point(1242, 157);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(13, 15);
            this.label218.TabIndex = 319;
            this.label218.Text = "8";
            // 
            // label219
            // 
            this.label219.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label219.ForeColor = System.Drawing.Color.Black;
            this.label219.Location = new System.Drawing.Point(1226, 157);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(13, 15);
            this.label219.TabIndex = 318;
            this.label219.Text = "7";
            // 
            // label220
            // 
            this.label220.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label220.ForeColor = System.Drawing.Color.Black;
            this.label220.Location = new System.Drawing.Point(1210, 157);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(13, 15);
            this.label220.TabIndex = 317;
            this.label220.Text = "6";
            // 
            // label221
            // 
            this.label221.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label221.ForeColor = System.Drawing.Color.Black;
            this.label221.Location = new System.Drawing.Point(1194, 157);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(13, 15);
            this.label221.TabIndex = 316;
            this.label221.Text = "5";
            // 
            // label222
            // 
            this.label222.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label222.ForeColor = System.Drawing.Color.Black;
            this.label222.Location = new System.Drawing.Point(1179, 157);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(13, 15);
            this.label222.TabIndex = 315;
            this.label222.Text = "4";
            // 
            // label223
            // 
            this.label223.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label223.ForeColor = System.Drawing.Color.Black;
            this.label223.Location = new System.Drawing.Point(1162, 157);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(13, 15);
            this.label223.TabIndex = 314;
            this.label223.Text = "3";
            // 
            // label224
            // 
            this.label224.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label224.ForeColor = System.Drawing.Color.Black;
            this.label224.Location = new System.Drawing.Point(1145, 157);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(13, 15);
            this.label224.TabIndex = 313;
            this.label224.Text = "2";
            // 
            // label225
            // 
            this.label225.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label225.ForeColor = System.Drawing.Color.Black;
            this.label225.Location = new System.Drawing.Point(1242, 142);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(13, 15);
            this.label225.TabIndex = 312;
            this.label225.Text = "6";
            // 
            // label226
            // 
            this.label226.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label226.ForeColor = System.Drawing.Color.Black;
            this.label226.Location = new System.Drawing.Point(1226, 142);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(13, 15);
            this.label226.TabIndex = 311;
            this.label226.Text = "6";
            // 
            // label227
            // 
            this.label227.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label227.ForeColor = System.Drawing.Color.Black;
            this.label227.Location = new System.Drawing.Point(1210, 142);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(13, 15);
            this.label227.TabIndex = 310;
            this.label227.Text = "6";
            // 
            // label228
            // 
            this.label228.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label228.ForeColor = System.Drawing.Color.Black;
            this.label228.Location = new System.Drawing.Point(1194, 142);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(13, 15);
            this.label228.TabIndex = 309;
            this.label228.Text = "6";
            // 
            // label229
            // 
            this.label229.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label229.ForeColor = System.Drawing.Color.Black;
            this.label229.Location = new System.Drawing.Point(1179, 142);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(13, 15);
            this.label229.TabIndex = 308;
            this.label229.Text = "6";
            // 
            // label230
            // 
            this.label230.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label230.ForeColor = System.Drawing.Color.Black;
            this.label230.Location = new System.Drawing.Point(1162, 142);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(13, 15);
            this.label230.TabIndex = 307;
            this.label230.Text = "6";
            // 
            // label231
            // 
            this.label231.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label231.ForeColor = System.Drawing.Color.Black;
            this.label231.Location = new System.Drawing.Point(1145, 142);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(13, 15);
            this.label231.TabIndex = 306;
            this.label231.Text = "6";
            // 
            // label232
            // 
            this.label232.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label232.ForeColor = System.Drawing.Color.Black;
            this.label232.Location = new System.Drawing.Point(1242, 134);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(13, 15);
            this.label232.TabIndex = 305;
            this.label232.Text = "^";
            // 
            // label233
            // 
            this.label233.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label233.ForeColor = System.Drawing.Color.Black;
            this.label233.Location = new System.Drawing.Point(1226, 134);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(13, 15);
            this.label233.TabIndex = 304;
            this.label233.Text = "^";
            // 
            // label234
            // 
            this.label234.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label234.ForeColor = System.Drawing.Color.Black;
            this.label234.Location = new System.Drawing.Point(1210, 134);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(13, 15);
            this.label234.TabIndex = 303;
            this.label234.Text = "^";
            // 
            // label235
            // 
            this.label235.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label235.ForeColor = System.Drawing.Color.Black;
            this.label235.Location = new System.Drawing.Point(1194, 134);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(13, 15);
            this.label235.TabIndex = 302;
            this.label235.Text = "^";
            // 
            // label236
            // 
            this.label236.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label236.ForeColor = System.Drawing.Color.Black;
            this.label236.Location = new System.Drawing.Point(1179, 134);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(13, 15);
            this.label236.TabIndex = 301;
            this.label236.Text = "^";
            // 
            // label237
            // 
            this.label237.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label237.ForeColor = System.Drawing.Color.Black;
            this.label237.Location = new System.Drawing.Point(1162, 134);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(13, 15);
            this.label237.TabIndex = 300;
            this.label237.Text = "^";
            // 
            // label238
            // 
            this.label238.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label238.ForeColor = System.Drawing.Color.Black;
            this.label238.Location = new System.Drawing.Point(1146, 134);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(13, 15);
            this.label238.TabIndex = 299;
            this.label238.Text = "^";
            // 
            // lbLastUpdateBy
            // 
            this.lbLastUpdateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateBy.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateBy.Location = new System.Drawing.Point(233, 70);
            this.lbLastUpdateBy.Name = "lbLastUpdateBy";
            this.lbLastUpdateBy.Size = new System.Drawing.Size(165, 20);
            this.lbLastUpdateBy.TabIndex = 324;
            this.lbLastUpdateBy.Text = "??/??/????";
            this.lbLastUpdateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbBomCreateBy
            // 
            this.lbBomCreateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBomCreateBy.ForeColor = System.Drawing.Color.Black;
            this.lbBomCreateBy.Location = new System.Drawing.Point(233, 50);
            this.lbBomCreateBy.Name = "lbBomCreateBy";
            this.lbBomCreateBy.Size = new System.Drawing.Size(222, 20);
            this.lbBomCreateBy.TabIndex = 323;
            this.lbBomCreateBy.Text = "??/??/????";
            this.lbBomCreateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbUnitType
            // 
            this.lbUnitType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUnitType.ForeColor = System.Drawing.Color.Black;
            this.lbUnitType.Location = new System.Drawing.Point(1333, 118);
            this.lbUnitType.Name = "lbUnitType";
            this.lbUnitType.Size = new System.Drawing.Size(100, 20);
            this.lbUnitType.TabIndex = 325;
            this.lbUnitType.Text = "UnitType";
            this.lbUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbUnitType.Visible = false;
            // 
            // lbModelType
            // 
            this.lbModelType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelType.ForeColor = System.Drawing.Color.Black;
            this.lbModelType.Location = new System.Drawing.Point(1333, 137);
            this.lbModelType.Name = "lbModelType";
            this.lbModelType.Size = new System.Drawing.Size(100, 20);
            this.lbModelType.TabIndex = 326;
            this.lbModelType.Text = "ModelType";
            this.lbModelType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbModelType.Visible = false;
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Engravers MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.Blue;
            this.lbEnvironment.Location = new System.Drawing.Point(380, 28);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(667, 20);
            this.lbEnvironment.TabIndex = 327;
            this.lbEnvironment.Text = "Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label209
            // 
            this.label209.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label209.ForeColor = System.Drawing.Color.Teal;
            this.label209.Location = new System.Drawing.Point(7, 54);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(185, 20);
            this.label209.TabIndex = 135;
            this.label209.Text = "Entering Temp in Fahrenheit:";
            this.label209.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmModelNoConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.lbModelType);
            this.Controls.Add(this.lbUnitType);
            this.Controls.Add(this.lbLastUpdateBy);
            this.Controls.Add(this.lbBomCreateBy);
            this.Controls.Add(this.label215);
            this.Controls.Add(this.label216);
            this.Controls.Add(this.label217);
            this.Controls.Add(this.label218);
            this.Controls.Add(this.label219);
            this.Controls.Add(this.label220);
            this.Controls.Add(this.label221);
            this.Controls.Add(this.label222);
            this.Controls.Add(this.label223);
            this.Controls.Add(this.label224);
            this.Controls.Add(this.label225);
            this.Controls.Add(this.label226);
            this.Controls.Add(this.label227);
            this.Controls.Add(this.label228);
            this.Controls.Add(this.label229);
            this.Controls.Add(this.label230);
            this.Controls.Add(this.label231);
            this.Controls.Add(this.label232);
            this.Controls.Add(this.label233);
            this.Controls.Add(this.label234);
            this.Controls.Add(this.label235);
            this.Controls.Add(this.label236);
            this.Controls.Add(this.label237);
            this.Controls.Add(this.label238);
            this.Controls.Add(this.label212);
            this.Controls.Add(this.label213);
            this.Controls.Add(this.label214);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.btnParseModelNo);
            this.Controls.Add(this.lbEnterModelNo);
            this.Controls.Add(this.txtModelNo);
            this.Controls.Add(this.btnCreateBOM);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.gbModelNoConfig);
            this.Controls.Add(this.label134);
            this.Controls.Add(this.label135);
            this.Controls.Add(this.label136);
            this.Controls.Add(this.label137);
            this.Controls.Add(this.label138);
            this.Controls.Add(this.label139);
            this.Controls.Add(this.label140);
            this.Controls.Add(this.label141);
            this.Controls.Add(this.label142);
            this.Controls.Add(this.label143);
            this.Controls.Add(this.label144);
            this.Controls.Add(this.label145);
            this.Controls.Add(this.label146);
            this.Controls.Add(this.label147);
            this.Controls.Add(this.label148);
            this.Controls.Add(this.label149);
            this.Controls.Add(this.label150);
            this.Controls.Add(this.label151);
            this.Controls.Add(this.label152);
            this.Controls.Add(this.label153);
            this.Controls.Add(this.label154);
            this.Controls.Add(this.label155);
            this.Controls.Add(this.label156);
            this.Controls.Add(this.label157);
            this.Controls.Add(this.label158);
            this.Controls.Add(this.label159);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label132);
            this.Controls.Add(this.label133);
            this.Controls.Add(this.lbLastUpdateDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label129);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.lbCompleteDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbProdStartDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbBOMCreationDate);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lbShipDate);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.lbOrderDate);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbJobNum);
            this.Controls.Add(this.msConfigMenu);
            this.ForeColor = System.Drawing.Color.Blue;
            this.Name = "frmModelNoConfig";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ModelNo Configuration";
            this.Load += new System.EventHandler(this.frmModelNoConfig_Load);
            this.msConfigMenu.ResumeLayout(false);
            this.msConfigMenu.PerformLayout();
            this.gbModelNoConfig.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msConfigMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.Label lbBOMCreationDate;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label lbShipDate;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label lbOrderDate;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.Label lbProdStartDate;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lbCompleteDate;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label129;
        public System.Windows.Forms.Label label130;
        public System.Windows.Forms.Label label131;
        public System.Windows.Forms.Label label127;
        public System.Windows.Forms.Label label128;
        public System.Windows.Forms.Label label119;
        public System.Windows.Forms.Label label120;
        public System.Windows.Forms.Label label121;
        public System.Windows.Forms.Label label122;
        public System.Windows.Forms.Label label123;
        public System.Windows.Forms.Label label124;
        public System.Windows.Forms.Label label125;
        public System.Windows.Forms.Label label126;
        public System.Windows.Forms.Label label111;
        public System.Windows.Forms.Label label112;
        public System.Windows.Forms.Label label113;
        public System.Windows.Forms.Label label114;
        public System.Windows.Forms.Label label115;
        public System.Windows.Forms.Label label116;
        public System.Windows.Forms.Label label117;
        public System.Windows.Forms.Label label118;
        public System.Windows.Forms.Label label103;
        public System.Windows.Forms.Label label104;
        public System.Windows.Forms.Label label105;
        public System.Windows.Forms.Label label106;
        public System.Windows.Forms.Label label107;
        public System.Windows.Forms.Label label108;
        public System.Windows.Forms.Label label109;
        public System.Windows.Forms.Label label110;
        public System.Windows.Forms.Label label102;
        public System.Windows.Forms.Label label100;
        public System.Windows.Forms.Label label101;
        public System.Windows.Forms.Label label96;
        public System.Windows.Forms.Label label97;
        public System.Windows.Forms.Label label98;
        public System.Windows.Forms.Label label99;
        public System.Windows.Forms.Label label92;
        public System.Windows.Forms.Label label93;
        public System.Windows.Forms.Label label94;
        public System.Windows.Forms.Label label95;
        public System.Windows.Forms.Label label88;
        public System.Windows.Forms.Label label89;
        public System.Windows.Forms.Label label90;
        public System.Windows.Forms.Label label91;
        public System.Windows.Forms.Label label84;
        public System.Windows.Forms.Label label85;
        public System.Windows.Forms.Label label86;
        public System.Windows.Forms.Label label87;
        public System.Windows.Forms.Label label80;
        public System.Windows.Forms.Label label81;
        public System.Windows.Forms.Label label82;
        public System.Windows.Forms.Label label83;
        public System.Windows.Forms.Label label76;
        public System.Windows.Forms.Label label77;
        public System.Windows.Forms.Label label78;
        public System.Windows.Forms.Label label79;
        public System.Windows.Forms.Label label72;
        public System.Windows.Forms.Label label73;
        public System.Windows.Forms.Label label74;
        public System.Windows.Forms.Label label75;
        public System.Windows.Forms.Label label70;
        public System.Windows.Forms.Label label71;
        public System.Windows.Forms.Label label69;
        public System.Windows.Forms.Label label68;
        public System.Windows.Forms.Label label67;
        public System.Windows.Forms.Label label66;
        public System.Windows.Forms.Label label65;
        public System.Windows.Forms.Label label61;
        public System.Windows.Forms.Label label62;
        public System.Windows.Forms.Label label63;
        public System.Windows.Forms.Label label64;
        public System.Windows.Forms.Label label57;
        public System.Windows.Forms.Label label58;
        public System.Windows.Forms.Label label59;
        public System.Windows.Forms.Label label60;
        public System.Windows.Forms.Label label53;
        public System.Windows.Forms.Label label54;
        public System.Windows.Forms.Label label55;
        public System.Windows.Forms.Label label56;
        public System.Windows.Forms.Label label49;
        public System.Windows.Forms.Label label50;
        public System.Windows.Forms.Label label51;
        public System.Windows.Forms.Label label52;
        public System.Windows.Forms.Label label45;
        public System.Windows.Forms.Label label46;
        public System.Windows.Forms.Label label47;
        public System.Windows.Forms.Label label48;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label lbModelNo;
        public System.Windows.Forms.Label lbLastUpdateDate;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label label132;
        public System.Windows.Forms.Label label133;
        public System.Windows.Forms.Label label134;
        public System.Windows.Forms.Label label135;
        public System.Windows.Forms.Label label136;
        public System.Windows.Forms.Label label137;
        public System.Windows.Forms.Label label138;
        public System.Windows.Forms.Label label139;
        public System.Windows.Forms.Label label140;
        public System.Windows.Forms.Label label141;
        public System.Windows.Forms.Label label142;
        public System.Windows.Forms.Label label143;
        public System.Windows.Forms.Label label144;
        public System.Windows.Forms.Label label145;
        public System.Windows.Forms.Label label146;
        public System.Windows.Forms.Label label147;
        public System.Windows.Forms.Label label148;
        public System.Windows.Forms.Label label149;
        public System.Windows.Forms.Label label150;
        public System.Windows.Forms.Label label151;
        public System.Windows.Forms.Label label152;
        public System.Windows.Forms.Label label153;
        public System.Windows.Forms.Label label154;
        public System.Windows.Forms.Label label155;
        public System.Windows.Forms.Label label156;
        public System.Windows.Forms.Label label157;
        public System.Windows.Forms.Label label158;
        public System.Windows.Forms.Label label159;
        public System.Windows.Forms.Label label160;
        public System.Windows.Forms.GroupBox gbModelNoConfig;
        public System.Windows.Forms.ComboBox cbFreezstat;
        private System.Windows.Forms.Label label207;
        public System.Windows.Forms.ComboBox cbCondensateOverflowSwitch;
        private System.Windows.Forms.Label label206;
        public System.Windows.Forms.ComboBox cbAltitude;
        private System.Windows.Forms.Label label205;
        public System.Windows.Forms.ComboBox cbThermostat;
        private System.Windows.Forms.Label label204;
        public System.Windows.Forms.ComboBox cbFaceAndBypassEvap;
        private System.Windows.Forms.Label label203;
        public System.Windows.Forms.ComboBox cbCoolingControls;
        private System.Windows.Forms.Label label202;
        public System.Windows.Forms.ComboBox cbControlsDisplay;
        private System.Windows.Forms.Label label201;
        public System.Windows.Forms.ComboBox cbConvenienceOutlet;
        private System.Windows.Forms.Label label200;
        public System.Windows.Forms.ComboBox cbInstallation;
        private System.Windows.Forms.Label label199;
        public System.Windows.Forms.ComboBox cbUV_Lights;
        private System.Windows.Forms.Label label198;
        public System.Windows.Forms.ComboBox cbServiceLights;
        private System.Windows.Forms.Label label197;
        public System.Windows.Forms.ComboBox cbHailguards;
        private System.Windows.Forms.Label label196;
        public System.Windows.Forms.ComboBox cbSmokeDetector;
        private System.Windows.Forms.Label label195;
        public System.Windows.Forms.ComboBox cbExhaustFanWheelDiamater;
        private System.Windows.Forms.Label label194;
        public System.Windows.Forms.ComboBox cbExhaustFanMotorType;
        private System.Windows.Forms.Label label193;
        public System.Windows.Forms.ComboBox cbExhaustFanMotor;
        private System.Windows.Forms.Label label192;
        public System.Windows.Forms.ComboBox cbSupplyFanWheelDiameter;
        private System.Windows.Forms.Label label191;
        public System.Windows.Forms.ComboBox cbFanPiezoRing;
        public System.Windows.Forms.ComboBox cbSupplyFanMotorType;
        public System.Windows.Forms.ComboBox cbMajorDesign;
        private System.Windows.Forms.Label label161;
        public System.Windows.Forms.ComboBox cbAirflowConfig;
        private System.Windows.Forms.Label label162;
        public System.Windows.Forms.ComboBox cbSoundAttenuationPackage;
        private System.Windows.Forms.Label label163;
        public System.Windows.Forms.ComboBox cbCondenserFanOptions;
        private System.Windows.Forms.Label label164;
        public System.Windows.Forms.ComboBox cbOutdoorAirMonitoring;
        private System.Windows.Forms.Label label165;
        public System.Windows.Forms.ComboBox cbCorrisiveEnvironmentPackage;
        private System.Windows.Forms.Label label166;
        public System.Windows.Forms.ComboBox cbElectricalOptions;
        private System.Windows.Forms.Label label167;
        public System.Windows.Forms.ComboBox cbDamperOptions;
        private System.Windows.Forms.Label label168;
        public System.Windows.Forms.ComboBox cbERV_RotationSensor;
        private System.Windows.Forms.Label label169;
        public System.Windows.Forms.ComboBox cbEnergyWheelRecoverySize;
        private System.Windows.Forms.Label label170;
        public System.Windows.Forms.ComboBox cbEnergyRecoveryWheelOptions;
        private System.Windows.Forms.Label label171;
        public System.Windows.Forms.ComboBox cbEnergyRecovery;
        private System.Windows.Forms.Label label172;
        public System.Windows.Forms.ComboBox cbFilterOptions;
        private System.Windows.Forms.Label label173;
        public System.Windows.Forms.ComboBox cbBuildingInterface;
        private System.Windows.Forms.Label label174;
        public System.Windows.Forms.ComboBox cbUnitControls;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label labelInputsHeatCapacityLabel;
        public System.Windows.Forms.ComboBox cbSupplyFanMotor;
        private System.Windows.Forms.Label label177;
        public System.Windows.Forms.ComboBox cbHeatTypeSecondary;
        private System.Windows.Forms.Label label178;
        public System.Windows.Forms.ComboBox cbHeatCapacityPrimary;
        private System.Windows.Forms.Label label179;
        public System.Windows.Forms.ComboBox cbHeatTypePrimary;
        private System.Windows.Forms.Label label180;
        public System.Windows.Forms.ComboBox cbRefridgerantCapacityControl;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        public System.Windows.Forms.ComboBox cbIndoorCoilType;
        public System.Windows.Forms.ComboBox cbCoolingCapacity;
        private System.Windows.Forms.Label label183;
        public System.Windows.Forms.ComboBox cbExhaustDampers;
        private System.Windows.Forms.Label label184;
        public System.Windows.Forms.ComboBox cbOutdoorCoilType;
        private System.Windows.Forms.Label label185;
        public System.Windows.Forms.ComboBox cbHotGasReheat;
        private System.Windows.Forms.Label label186;
        public System.Windows.Forms.ComboBox cbCompressor;
        public System.Windows.Forms.ComboBox cbHeatCapacitySecondary;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        public System.Windows.Forms.ComboBox cbVoltage;
        private System.Windows.Forms.Label label189;
        public System.Windows.Forms.ComboBox cbCabinet;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.TextBox txtEnteringTemp;
        private System.Windows.Forms.TextBox txtFlowRate;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCreateBOM;
        public System.Windows.Forms.TextBox txtModelNo;
        public System.Windows.Forms.Label lbCustName;
        public System.Windows.Forms.ComboBox cbOutdoorCoilFluidType;
        private System.Windows.Forms.Label label211;
        public System.Windows.Forms.Label label212;
        public System.Windows.Forms.Label label213;
        public System.Windows.Forms.Label label214;
        public System.Windows.Forms.Label label215;
        public System.Windows.Forms.Label label216;
        public System.Windows.Forms.Label label217;
        public System.Windows.Forms.Label label218;
        public System.Windows.Forms.Label label219;
        public System.Windows.Forms.Label label220;
        public System.Windows.Forms.Label label221;
        public System.Windows.Forms.Label label222;
        public System.Windows.Forms.Label label223;
        public System.Windows.Forms.Label label224;
        public System.Windows.Forms.Label label225;
        public System.Windows.Forms.Label label226;
        public System.Windows.Forms.Label label227;
        public System.Windows.Forms.Label label228;
        public System.Windows.Forms.Label label229;
        public System.Windows.Forms.Label label230;
        public System.Windows.Forms.Label label231;
        public System.Windows.Forms.Label label232;
        public System.Windows.Forms.Label label233;
        public System.Windows.Forms.Label label234;
        public System.Windows.Forms.Label label235;
        public System.Windows.Forms.Label label236;
        public System.Windows.Forms.Label label237;
        public System.Windows.Forms.Label label238;
        public System.Windows.Forms.ComboBox cbDigit69;
        public System.Windows.Forms.ComboBox cbDigit68;
        public System.Windows.Forms.ComboBox cbDigit67;
        public System.Windows.Forms.ComboBox cbDigit66;
        public System.Windows.Forms.ComboBox cbDigit65;
        public System.Windows.Forms.ComboBox cbDigit64;
        public System.Windows.Forms.ComboBox cbDigit63;
        public System.Windows.Forms.ComboBox cbMinimumDamperLeakage;
        private System.Windows.Forms.Label label245;
        public System.Windows.Forms.Label lbEnterModelNo;
        public System.Windows.Forms.Button btnParseModelNo;
        public System.Windows.Forms.Label lbLastUpdateBy;
        public System.Windows.Forms.Label lbBomCreateBy;
        public System.Windows.Forms.Label lbUnitType;
        public System.Windows.Forms.Label lbModelType;
        public System.Windows.Forms.Label lbEnvironment;
        public System.Windows.Forms.Label lbDigit67Desc;
        public System.Windows.Forms.Label lbDigit66Desc;
        public System.Windows.Forms.Label lbDigit65Desc;
        public System.Windows.Forms.Label lbDigit64Desc;
        public System.Windows.Forms.Label lbDigit63Desc;
        public System.Windows.Forms.Label lbDigit69Desc;
        public System.Windows.Forms.Label lbDigit68Desc;
        private System.Windows.Forms.Label label209;
    }
}