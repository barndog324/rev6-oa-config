﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmSheetMetalUsage : Form
    {
        MainBO objMain = new MainBO();

        public frmSheetMetalUsage()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvMetalUsage_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIdx = dgvMetalUsage.CurrentRow.Index;

            if (e.ColumnIndex == 3)  // Add button within datagridview.
            {
                dgvMetalUsage.BeginEdit(true);
            }
        }

        private void dgvMetalUsage_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn dc in dgvMetalUsage.Columns)
            {
                if (dc.Index.Equals(3))
                {
                    dc.ReadOnly = false;
                }
                else
                {
                    dc.ReadOnly = true;
                }
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            SubmitNew();
            //string errors = "";
            //string partNum = "";
            //string[] jobNumAry = lbJobNumList.Text.Split();
            //string jobNumStr;            
            //string progressRecIdStr = "";
            //string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString(); ;

            //int actQty = 0;
            //int estQty = 0;
            //int jobCount = 0;
            //int jobQty = 0;
            //int relJobCount = jobNumAry.Length;
            //int jobAmtUsed = 0;           
            //int totUpdQty = 0;
            //int reqQty = 0;
            //int remainingQty = 0;
            //int auxBoxJobCount = 0;
            //int auxJobsWrittenCnt = 0;

            //long releaseIdInt = 0;            

            //bool validValues = true;
            //bool processUpdates = true;           
            //bool auxOnly = false;
            
            //int slashPos = modByStr.IndexOf("\\");
            //if (slashPos > 0)
            //{
            //    modByStr = "- " + modByStr.Substring((slashPos + 1), (modByStr.Length - (slashPos + 1)));
            //}

            //foreach(DataGridViewRow row in dgvMetalUsage.Rows)
            //{
            //    if (row.Cells["PartNum"] != null)
            //    {
            //        actQty = (int)row.Cells["ActualQtyUsed"].Value;
            //        if (actQty == 0)
            //        {
            //            if (errors.Length > 0)
            //            {
            //                errors += "\n";
            //            }
            //            errors += row.Cells["PartNum"].Value.ToString();
            //        }
            //        else if (actQty < 0)
            //        {
            //            validValues = false;
            //            MessageBox.Show("ERROR - The Part Number: " + row.Cells["PartNum"].Value.ToString() + " has a negative value.");
            //        }
            //    }
            //}

            //if (validValues == true)
            //{
            //    if (errors.Length > 0)
            //    {
            //        DialogResult result1 = MessageBox.Show("WARNING - The following Part Numbers have 0 for Actual Qty Used: " + errors +
            //                                               "\nIf the zero quantities are correct, then press 'Yes' to continue. Otherwise press 'No' to edit values.",
            //                                               "WARNING WARNING WARNING",
            //                                               MessageBoxButtons.YesNo,
            //                                               MessageBoxIcon.Exclamation);
            //        if (result1 == DialogResult.No)
            //        {
            //            processUpdates = false;                       
            //        }
            //    }
            //}

            //if (processUpdates == true)
            //{
            //    releaseIdInt = long.Parse(lbReleaseID.Text);

            //    foreach (DataGridViewRow row in dgvMetalUsage.Rows)
            //    {
            //        actQty = (int)row.Cells["ActualQtyUsed"].Value;
            //        estQty = (int)row.Cells["EstimatedQtyUsed"].Value;
            //        partNum = row.Cells["PartNum"].Value.ToString();
            //        auxBoxJobCount = 0;
            //        auxJobsWrittenCnt = 0;
            //        auxOnly = false;                   

            //        DataTable dtPartJobQty = objMain.GetSheetMetalQtyByPart(lbJobNumList.Text, partNum);
            //        jobCount = dtPartJobQty.Rows.Count;

            //        if (jobCount > 0)
            //        {
            //            jobAmtUsed = actQty / jobCount;

            //            if (jobAmtUsed < 1)
            //            {
            //                if (actQty == 0)
            //                {
            //                    reqQty = 0;
            //                }
            //                else
            //                {
            //                    reqQty = 1;
            //                }
            //                foreach(DataRow dr in dtPartJobQty.Rows)
            //                {
            //                    jobNumStr = dr["JobNum"].ToString();
            //                    progressRecIdStr = dr["PROGRESS_RECID"].ToString();
            //                    jobQty = (int)dr["EstQtyUsedByJob"];
            //                    if (actQty <= estQty && jobQty > 0)
            //                    {
            //                        objMain.UpdateJobMtlPartQty(releaseIdInt, jobNumStr, partNum, estQty.ToString(), reqQty.ToString(), modByStr, progressRecIdStr);
            //                        totUpdQty += reqQty;
            //                    }
                              
            //                    if (totUpdQty >= actQty)
            //                    {
            //                        reqQty = 0;
            //                    }
            //                }
            //                if (totUpdQty < actQty)
            //                {
            //                    foreach (DataRow dr in dtPartJobQty.Rows)
            //                    {
            //                        jobNumStr = dr["JobNum"].ToString();
            //                        progressRecIdStr = dr["PROGRESS_RECID"].ToString();
            //                        jobQty = (int)dr["EstQtyUsedByJob"];

            //                        if (jobQty == 0)
            //                        {
            //                            objMain.UpdateJobMtlPartQty(releaseIdInt, jobNumStr, partNum, estQty.ToString(), reqQty.ToString(), modByStr, progressRecIdStr);
            //                            totUpdQty += reqQty;
            //                        }

            //                        if (totUpdQty >= actQty)
            //                        {
            //                            reqQty = 0;
            //                        }
            //                    }
            //                }

            //            }
            //            else
            //            {
            //                remainingQty = actQty % jobCount;
            //                foreach (DataRow dr1 in dtPartJobQty.Rows)
            //                {
            //                    if (dr1["AuxBox"].ToString().ToUpper() == "AUXBOX")
            //                    {
            //                        ++auxBoxJobCount;
            //                    }
            //                }

            //                if (remainingQty < auxBoxJobCount)
            //                {
            //                    auxOnly = true;
            //                }                           

            //                foreach (DataRow dr in dtPartJobQty.Rows)
            //                {
            //                    jobNumStr = dr["JobNum"].ToString();
            //                    reqQty = jobAmtUsed;
            //                    progressRecIdStr = dr["PROGRESS_RECID"].ToString();
            //                    if (remainingQty >= 1)
            //                    {
            //                        if (auxOnly == true)
            //                        {
            //                            if (dr["AuxBox"].ToString().ToUpper() == "AUXBOX")
            //                            {
            //                                reqQty += 1;
            //                                remainingQty -= 1;
            //                            }
            //                        }
            //                        else
            //                        {
            //                            if (auxJobsWrittenCnt < auxBoxJobCount)
            //                            {
            //                                if (dr["AuxBox"].ToString().ToUpper() == "AUXBOX")
            //                                {
            //                                    reqQty += 1;
            //                                    remainingQty -= 1;
            //                                    ++auxJobsWrittenCnt;
            //                                }                                                                                       
            //                            }
            //                            else if (remainingQty >= 1)
            //                            {
            //                                reqQty += 1;
            //                                remainingQty -= 1;
            //                            }
            //                        }
            //                    }
            //                    objMain.UpdateJobMtlPartQty(releaseIdInt, jobNumStr, partNum, estQty.ToString(), reqQty.ToString(), modByStr, progressRecIdStr);                                                              
            //                }
            //            }
            //        }
            //    }
            //    objMain.UpdateSheetMetalRelease(releaseIdInt);
            //    lbMainMsg.Text = "Process Complete!";
            //    this.Close();
            //}
        }

        private void SubmitNew()
        {
            string errors = "";
            string partNum = "";
            string partDesc = "";            
            string[] jobNumAry = lbJobNumList.Text.Split(',');
            string jobNumStr;
            string progressRecIdStr = "";
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string curJobQtyStr = "";

            int actQty = 0;
            int estQty = 0;
            int jobCount = 0;
            int jobQty = 0;
            int curJobQty = 0;
            //int relJobCount = (jobNumAry.Length - 1);
            int relJobCount = 0;
            int jobAmtUsed = 0;
            int totUpdQty = 0;
            int reqQty = 0;
            int remainingQty = 0;
            int auxBoxJobCount = 0;
            int auxJobsWrittenCnt = 0;
            int additionalQty = 1;

            long releaseIdInt = 0;

            decimal unitCostDec = 0;

            bool validValues = true;
            bool processUpdates = true;
            bool auxOnly = false;
            bool jobFound = false;
            bool addPartAllJobs = false;

            int slashPos = modByStr.IndexOf("\\");
            if (slashPos > 0)
            {
                modByStr = modByStr.Substring((slashPos + 1), (modByStr.Length - (slashPos + 1)));
            }

            if (lbJobNumList.Text.Substring((lbJobNumList.Text.Length-1), 1) == ",")
            {
                relJobCount = jobNumAry.Length - 1;
            }
            else
            {
                relJobCount = jobNumAry.Length;
            }

            foreach (DataGridViewRow row in dgvMetalUsage.Rows)
            {
                if (row.Cells["PartNum"] != null)
                {
                    actQty = (int)row.Cells["ActualQtyUsed"].Value;
                    if (actQty == 0)
                    {
                        if (errors.Length > 0)
                        {
                            errors += "\n";
                        }
                        errors += row.Cells["PartNum"].Value.ToString();
                    }
                    else if (actQty < 0)
                    {
                        validValues = false;
                        MessageBox.Show("ERROR - The Part Number: " + row.Cells["PartNum"].Value.ToString() + " has a negative value.");
                    }
                }
            }

            if (validValues == true)
            {
                if (errors.Length > 0)
                {
                    DialogResult result1 = MessageBox.Show("WARNING - The following Part Numbers have 0 for Actual Qty Used: " + errors +
                                                           "\nIf the zero quantities are correct, then press 'Yes' to continue. Otherwise press 'No' to edit values.",
                                                           "WARNING WARNING WARNING",
                                                           MessageBoxButtons.YesNo,
                                                           MessageBoxIcon.Exclamation);
                    if (result1 == DialogResult.No)
                    {
                        processUpdates = false;
                    }
                }
            }

            if (processUpdates == true)
            {
                releaseIdInt = long.Parse(lbReleaseID.Text);

                foreach (DataGridViewRow row in dgvMetalUsage.Rows)
                {
                    actQty = (int)row.Cells["ActualQtyUsed"].Value;
                    estQty = (int)row.Cells["EstimatedQtyUsed"].Value;
                    partNum = row.Cells["PartNum"].Value.ToString();
                    partDesc = row.Cells["Description"].Value.ToString();
                    auxBoxJobCount = 0;
                    auxJobsWrittenCnt = 0;
                    auxOnly = false;
                    totUpdQty = 0;

                    DataTable dtPartJobQty = objMain.GetSheetMetalQtyByPart(lbJobNumList.Text, partNum);
                    if (dtPartJobQty.Rows.Count > 0)
                    {
                        dtPartJobQty = consolidatePartNumsByJob(dtPartJobQty);
                    }

                    jobCount = dtPartJobQty.Rows.Count;
                              
                    reqQty = actQty / relJobCount;
                    remainingQty = actQty % relJobCount;
                                       
                    foreach (DataRow dr in dtPartJobQty.Rows)
                    {
                        jobNumStr = dr["JobNum"].ToString();
                        progressRecIdStr = dr["PROGRESS_RECID"].ToString();
                        curJobQtyStr = dr["CurrentJobQty"].ToString();
                     
                        if (actQty == 0 || totUpdQty >= actQty)
                        {
                            jobQty = 0;
                        }
                        else
                        {
                            jobQty = reqQty;                                
                        }

                        if (jobQty > 0)
                        {
                            objMain.UpdateJobMtlPartQty(releaseIdInt, jobNumStr, partNum, curJobQtyStr, jobQty.ToString(), modByStr, progressRecIdStr);
                        }
                        else if (remainingQty == 0)
                        {
                            objMain.DeletePartNumFromJobMtl(jobNumStr, partNum, Int32.Parse(progressRecIdStr), "SM_AllocDelete");
                        }

                        totUpdQty += jobQty;                            
                    }


                    if (totUpdQty == 0)
                    {
                        addPartAllJobs = true;                        
                    }

                    if (totUpdQty < actQty)
                    {
                        foreach (string searchJobNum in jobNumAry)
                        {
                            jobFound = false;
                            foreach (DataRow dr in dtPartJobQty.Rows)
                            {
                                jobNumStr = dr["JobNum"].ToString();

                                if (searchJobNum == jobNumStr)
                                {
                                    jobFound = true;
                                    break;
                                }
                            }
                            if (jobFound == false && searchJobNum.Length > 0)
                            {
                                DataTable dtPart = objMain.GetPartDetail(partNum);
                                if (dtPart.Rows.Count > 0)
                                {
                                    DataRow drPart = dtPart.Rows[0];
                                    unitCostDec = (decimal)drPart["UnitCost"];
                                }

                                if (addPartAllJobs == true)
                                {
                                    jobQty = reqQty;
                                }
                                else
                                {
                                    if (totUpdQty > reqQty)
                                    {
                                        jobQty = reqQty;
                                    }
                                    else
                                    {
                                        jobQty = 1;
                                    }
                                }
                            
                                objMain.InsertPartJobMtl(searchJobNum, partNum, partDesc, jobQty.ToString(), unitCostDec);

                                totUpdQty += jobQty;

                                if (totUpdQty >= actQty)
                                {
                                    break;
                                }
                            }                                       
                        }

                        if  (remainingQty > 0)
                        {
                            dtPartJobQty = objMain.GetSheetMetalQtyByPart(lbJobNumList.Text, partNum);                                

                            foreach (DataRow dr in dtPartJobQty.Rows)
                            {
                                jobNumStr = dr["JobNum"].ToString();
                                progressRecIdStr = dr["PROGRESS_RECID"].ToString();
                                curJobQty = (int)dr["CurrentJobQty"];

                                if (reqQty == 0)
                                {
                                    jobQty = 1;
                                }
                                else
                                {
                                    jobQty = curJobQty + 1;
                                }

                                if (remainingQty > 0)
                                {
                                    objMain.UpdateJobMtlPartQty(releaseIdInt, jobNumStr, partNum, estQty.ToString(), jobQty.ToString(), modByStr, progressRecIdStr);
                                }
                                else if (reqQty == 0)
                                {
                                    objMain.DeletePartNumFromJobMtl(jobNumStr, partNum, Int32.Parse(progressRecIdStr), "SM_AllocDelete");
                                }

                                --remainingQty;

                                //if (remainingQty == 0)
                                //{
                                //    break;
                                //}
                            }
                        }                            
                    }                                                                          
                }
               
                objMain.UpdateSheetMetalRelease(releaseIdInt);
                lbMainMsg.Text = "Process Complete!";
                this.Close();
            }
        }

        private void btnAddPart_Click(object sender, EventArgs e)
        {
            DataTable dt = objMain.GetMetalPartNumbers();
            cbPartNumber.DataSource = dt;
            cbPartNumber.DisplayMember = "PartNumDesc";
            cbPartNumber.ValueMember = "PartNum";
            gbAddPart.Visible = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.gbAddPart.Visible = false;
        }

        private void btnSelectPart_Click(object sender, EventArgs e)
        {
            string partNum = "";
            string partDesc = "";

            bool dupFound = false;

            int    descLen = 0;

            if (cbPartNumber.SelectedIndex == 0)
            {
                MessageBox.Show("ERROR - No Part Number has been selected!");
            }
            else
            {
                partNum = cbPartNumber.SelectedValue.ToString();
                foreach (DataGridViewRow row in dgvMetalUsage.Rows)
                {
                    if (row.Cells["PartNum"].Value.ToString() == partNum)
                    {
                        dupFound = true;
                    }
                }

                if (dupFound == true)
                {
                    MessageBox.Show("ERROR - Part Number already exist!");
                }
                else
                {
                    partDesc = cbPartNumber.Text.ToString();
                    descLen = partDesc.Length - (partNum.Length + 3);
                    partDesc = partDesc.Substring((partNum.Length + 3), descLen);

                    DataTable dtGridSource = (DataTable)dgvMetalUsage.DataSource;
                    var dr = dtGridSource.NewRow();
                    dr["PartNum"] = partNum;
                    dr["Description"] = partDesc;
                    dr["EstimatedQtyUsed"] = 0;
                    dr["ActualQtyUsed"] = 0;
                    dr["TransType"] = "Insert";
                    dtGridSource.Rows.Add(dr);
                    dgvMetalUsage.DataSource = dtGridSource;
                    dgvMetalUsage.Refresh();
                    this.gbAddPart.Visible = false;
                }
            }
        }   

        private DataTable consolidatePartNumsByJob(DataTable dtPartJobQty)
        {
            string curJobNum = "";
            string curPartNum = "";
            string curAuxBox = "";

            int jobPartQty = 0;
            int jobPartCount = 0;
            long curProgress_Recid = 0;
            long ProgressRecid = 0;

            bool firstRow = true;

            var retDataTable = new DataTable();

            retDataTable.Columns.Add("JobNum");
            retDataTable.Columns.Add("PartNum");
            retDataTable.Columns.Add("CurrentJobQty");
            retDataTable.Columns.Add("AuxBox");
            retDataTable.Columns.Add("PROGRESS_RECID");

            foreach (DataRow row in dtPartJobQty.Rows)
            {
                if (row["JobNum"].ToString() != curJobNum)
                {
                    if (firstRow)
                    {
                        firstRow = false;
                    }
                    else
                    {
                        var dr = retDataTable.NewRow();
                        dr["JobNum"] = curJobNum;
                        dr["PartNum"] = curPartNum;
                        dr["CurrentjobQty"] = jobPartCount;
                        dr["AuxBox"] = curAuxBox;
                        dr["PROGRESS_RECID"] = curProgress_Recid;
                        retDataTable.Rows.Add(dr);
                    }

                    curJobNum = row["JobNum"].ToString();
                    curPartNum = row["PartNum"].ToString();
                    curAuxBox = row["AuxBox"].ToString();
                    curProgress_Recid = (long)row["PROGRESS_RECID"];
                    jobPartQty = 0;
                    jobPartCount = 0;
                }

                ++jobPartCount;
                jobPartQty += (int)row["CurrentjobQty"];

                if (jobPartCount > 1)
                {
                    ProgressRecid = (long)row["PROGRESS_RECID"];
                    objMain.DeletePartNumFromJobMtl(curJobNum, curPartNum, ProgressRecid, "SM_AllocProcess");
                }
            }

            var dr2 = retDataTable.NewRow();
            dr2["JobNum"] = curJobNum;
            dr2["PartNum"] = curPartNum;
            dr2["CurrentjobQty"] = jobPartCount;
            dr2["AuxBox"] = curAuxBox;
            dr2["PROGRESS_RECID"] = curProgress_Recid;
            retDataTable.Rows.Add(dr2);

            return retDataTable;
        }
    }
}
