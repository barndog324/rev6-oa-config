﻿namespace OA_Config_Rev6
{
    partial class frmJobOperationComp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvActualOpr = new System.Windows.Forms.DataGridView();
            this.dgvNeededOpr = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.lbMsg = new System.Windows.Forms.Label();
            this.lbActualJobOpr = new System.Windows.Forms.Label();
            this.lbNeededJobOpr = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActualOpr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNeededOpr)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvActualOpr
            // 
            this.dgvActualOpr.AllowUserToAddRows = false;
            this.dgvActualOpr.AllowUserToDeleteRows = false;
            this.dgvActualOpr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvActualOpr.Location = new System.Drawing.Point(476, 47);
            this.dgvActualOpr.Name = "dgvActualOpr";
            this.dgvActualOpr.Size = new System.Drawing.Size(410, 500);
            this.dgvActualOpr.TabIndex = 0;
            // 
            // dgvNeededOpr
            // 
            this.dgvNeededOpr.AllowUserToAddRows = false;
            this.dgvNeededOpr.AllowUserToDeleteRows = false;
            this.dgvNeededOpr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNeededOpr.Location = new System.Drawing.Point(31, 47);
            this.dgvNeededOpr.Name = "dgvNeededOpr";
            this.dgvNeededOpr.Size = new System.Drawing.Size(410, 500);
            this.dgvNeededOpr.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(807, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Exit";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lbMsg
            // 
            this.lbMsg.BackColor = System.Drawing.Color.LightSalmon;
            this.lbMsg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMsg.Location = new System.Drawing.Point(3, 580);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(910, 25);
            this.lbMsg.TabIndex = 253;
            this.lbMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbActualJobOpr
            // 
            this.lbActualJobOpr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbActualJobOpr.Location = new System.Drawing.Point(476, 22);
            this.lbActualJobOpr.Name = "lbActualJobOpr";
            this.lbActualJobOpr.Size = new System.Drawing.Size(312, 23);
            this.lbActualJobOpr.TabIndex = 254;
            this.lbActualJobOpr.Text = "ActualJob Operations";
            this.lbActualJobOpr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbNeededJobOpr
            // 
            this.lbNeededJobOpr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNeededJobOpr.Location = new System.Drawing.Point(33, 21);
            this.lbNeededJobOpr.Name = "lbNeededJobOpr";
            this.lbNeededJobOpr.Size = new System.Drawing.Size(408, 23);
            this.lbNeededJobOpr.TabIndex = 255;
            this.lbNeededJobOpr.Text = "Needed Job Operations";
            this.lbNeededJobOpr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmJobOperationComp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 606);
            this.Controls.Add(this.lbNeededJobOpr);
            this.Controls.Add(this.lbActualJobOpr);
            this.Controls.Add(this.lbMsg);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgvNeededOpr);
            this.Controls.Add(this.dgvActualOpr);
            this.Name = "frmJobOperationComp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Verify Job Operations";
            ((System.ComponentModel.ISupportInitialize)(this.dgvActualOpr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNeededOpr)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dgvActualOpr;
        public System.Windows.Forms.DataGridView dgvNeededOpr;
        private System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Label lbMsg;
        public System.Windows.Forms.Label lbActualJobOpr;
        public System.Windows.Forms.Label lbNeededJobOpr;

    }
}