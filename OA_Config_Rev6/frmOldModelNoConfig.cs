﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OA_Config_Rev6
{
    public partial class frmOldModelNoConfig : Form
    {
        MainBO objMain = new MainBO();
        int gVoltage = 0;
        int gPhase = 0;

        decimal gHertz = 60;

        public frmOldModelNoConfig()
        {
            InitializeComponent();
        }       

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (GlobalJob.ProgramMode == "DisplayMode")
            {
                this.Close();
            }
            else
            {
                Environment.Exit(0);
            }            
        }

        private void deleteBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //deleteAllPartsOnTheBOM();

            string jobNumStr;
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string modelNoStr = lbModelNo.Text;

            int deletedSuccessInt = 0;

            jobNumStr = this.lbJobNum.Text;

            if (modelNoStr.StartsWith("OA") == true || modelNoStr.StartsWith("HA") == true)
            {
                deleteAllPartsOnTheBOM();
            }
            else
            {
                DialogResult result1 = MessageBox.Show("Attempting to delete an existing BOM for Job# " + jobNumStr +
                                           " do you wish to continue???",
                                            "WARNING WARNING WARNING",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Exclamation);

                if (result1 == DialogResult.Yes)
                {
                    deletedSuccessInt = objMain.deleteJobFromDatabase(jobNumStr, userName, "OAU");
                    if (deletedSuccessInt == 0)
                    {
                        //this.lbBOMCreationDate.Text = "";
                        MessageBox.Show("You have successfully deleted the BOM for Job# " + jobNumStr + " from the database");
                    }
                    else if (deletedSuccessInt == 1)
                    {
                        MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                            jobNumStr + " from the OAU_BOMs table! Please contact IT to resolve this issue.");
                    }
                    else if (deletedSuccessInt == 2)
                    {
                        MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                            jobNumStr + " from the jobmtl table! Please contact IT to resolve this issue.");
                    }
                    else if (deletedSuccessInt == 3)
                    {
                        MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                            jobNumStr + " from the jobopdtl table! Please contact IT to resolve this issue.");
                    }
                    else if (deletedSuccessInt == 4)
                    {
                        MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                            jobNumStr + " from the joboper table! Please contact IT to resolve this issue.");
                    }
                }
            }
        }

        private void createBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string errorMsgStr = "";
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";

            bool foundExistingBOM = true;

            //CreateAllPartsBOM();
            CreateBOM_New();

            //frmDisplayBOM frmBOM = new frmDisplayBOM();

            //DataTable dtBOM = objMain.GetRev5ExistingBOMPartListByJobNum(jobNumStr);
            //if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            //{
            //    if (modelNoStr.Substring(19, 1) == "J")
            //    {
            //        if (this.txtFlowRate.Text == "")
            //        {
            //            errorMsgStr = "ERROR - Hot Water Flow Rate is required!\n";
            //        }
            //        if (this.txtEnteringTemp.Text == "")
            //        {
            //            errorMsgStr += "ERROR - Hot Water Entering Temp is required!";
            //        }
            //    }

            //    if (errorMsgStr.Length > 0)
            //    {
            //        MessageBox.Show(errorMsgStr);
            //    }
            //    else
            //    {
            //        frmBOM.lbFlowRate.Text = this.txtFlowRate.Text;
            //        frmBOM.lbEnteringTemp.Text = this.txtEnteringTemp.Text;
            //        dtBOM = CreateBOM(dtBOM, jobNumStr, modelNoStr, frmBOM);
            //    }
            //}

            ////if (dtBOM.Rows.Count > 0)
            ////{
            ////    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr);
            ////}       

            //dtBOM.DefaultView.Sort = "AssemblySeq";
            //dtBOM = dtBOM.DefaultView.ToTable();
            //frmBOM.dgvBOM.DataSource = dtBOM;
            //frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            //frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            //frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            //frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            //frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm Seq";
            //frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            //frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            //frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            //frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            //frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            //frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            //frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            //frmBOM.dgvBOM.Columns["Description"].Width = 375;
            //frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            //frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            //frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            //frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            //frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            //frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            //frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            //frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            //frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            //frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            //frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            //frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            //frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            //frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            //frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            //frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            //frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            //frmBOM.dgvBOM.Columns["Status"].Width = 75;
            //frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            //frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            //frmBOM.dgvBOM.Columns["PartStatus"].Width = 80;
            //frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            //frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            //frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 80;
            //frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            //frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            //frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            //frmBOM.dgvBOM.Columns["CostMethod"].Visible = false;
            //frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            //frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;
            //frmBOM.dgvBOM.Columns["RuleHeadID"].Visible = false;


            //frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            //frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            //frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            //frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            //frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            //frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            //frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            //frmBOM.lbShipDate.Text = lbShipDate.Text;
            //frmBOM.lbJobNum.Text = jobNumStr;
            //frmBOM.lbCustName.Text = lbCustName.Text;
            //frmBOM.lbModelNo.Text = modelNoStr;
            //frmBOM.lbUnitDescription.Text = lbUnitDescription.Text;
            //frmBOM.Text = lbCustName.Text + " -- " + modelNoStr;

            ////if (mcaStr != "0")
            ////{
            ////    frmBOM.lbMCA.Text = mcaStr;
            ////    frmBOM.lbMOP.Text = mopStr;
            ////}

            //frmBOM.ShowDialog();
        }

        private void btnCreateBOM_Click(object sender, EventArgs e)
        {
            CreateAllPartsBOM();
            
            //CreateBOM_New();


            //string jobNumStr = lbJobNum.Text;
            //string modelNoStr = lbModelNo.Text;
            //string errorMsgStr = "";
            //string mcaStr = "0";
            //string mopStr = "0";
            //string rawMopStr = "0";

            //bool foundExistingBOM = true;

            //frmDisplayBOM frmBOM = new frmDisplayBOM();

            //DataTable dtBOM = objMain.GetRev5ExistingBOMPartListByJobNum(jobNumStr);
            //if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            //{
            //    if (modelNoStr.Substring(19, 1) == "J")
            //    {
            //        if (this.txtFlowRate.Text == "")
            //        {
            //            errorMsgStr = "ERROR - Hot Water Flow Rate is required!\n";
            //        }
            //        if (this.txtEnteringTemp.Text == "")
            //        {
            //            errorMsgStr += "ERROR - Hot Water Entering Temp is required!";
            //        }
            //    }

            //    if (errorMsgStr.Length > 0)
            //    {
            //        MessageBox.Show(errorMsgStr);
            //    }
            //    else
            //    {
            //        //frmBOM.lbFlowRate.Text = this.txtFlowRate.Text;
            //        //frmBOM.lbEnteringTemp.Text = this.txtEnteringTemp.Text;
            //        dtBOM = CreateBOM(dtBOM, jobNumStr, modelNoStr, frmBOM);
            //        frmBOM.btnSave.Enabled = true;
            //        frmBOM.btnAddPart.Enabled = false;
            //    }
            //}
            //else
            //{
            //    frmBOM.btnSave.Enabled = false;
            //    frmBOM.btnPrint.Enabled = true;
            //    frmBOM.btnAddPart.Enabled = true;
            //    DataTable dt = objMain.GetEtlData(jobNumStr);
            //    if (dt.Rows.Count > 0)
            //    {
            //        DataRow dr = dt.Rows[0];
            //        frmBOM.lbEnteringTemp.Text = dr["EnteringTemp"].ToString();
            //        frmBOM.lbFlowRate.Text = dr["FlowRate"].ToString();
            //        frmBOM.lbMCA.Text = dr["MinCKTAmp"].ToString();
            //        frmBOM.lbMOP.Text = dr["MFSMCB"].ToString();
            //    }
            //}

            ////if (dtBOM.Rows.Count > 0)
            ////{
            ////    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr);
            ////}       

            //dtBOM.DefaultView.Sort = "AssemblySeq";
            //dtBOM = dtBOM.DefaultView.ToTable();
            //frmBOM.dgvBOM.DataSource = dtBOM;
            //frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            //frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            //frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            //frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            //frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            //frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            //frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            //frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            //frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            //frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            //frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            //frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            //frmBOM.dgvBOM.Columns["Description"].Width = 300;
            //frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            //frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            //frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            //frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            //frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            //frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            //frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            //frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            //frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            //frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            //frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            //frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            //frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            //frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            //frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            //frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            //frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            //frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            //frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            //frmBOM.dgvBOM.Columns["Status"].Width = 75;
            //frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            //frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            //frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            //frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            //frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            //frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 120;
            //frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            //frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            //frmBOM.dgvBOM.Columns["PartType"].Width = 120;
            //frmBOM.dgvBOM.Columns["PartType"].HeaderText = "MotorType";
            //frmBOM.dgvBOM.Columns["PartType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //frmBOM.dgvBOM.Columns["PartType"].DisplayIndex = 13;
            //frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            //frmBOM.dgvBOM.Columns["CostMethod"].Visible = false;
            ////frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            //frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;
            //frmBOM.dgvBOM.Columns["RuleHeadID"].Visible = false;


            //frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            //frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            //frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            //frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            //frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            //frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            //frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            //frmBOM.lbShipDate.Text = lbShipDate.Text;
            //frmBOM.lbJobNum.Text = jobNumStr;
            //frmBOM.lbCustName.Text = lbCustName.Text;
            //frmBOM.lbModelNo.Text = modelNoStr;
            //frmBOM.lbUnitDescription.Text = lbUnitDescription.Text;
            //frmBOM.Text = lbCustName.Text + " -- " + modelNoStr;
            ////frmBOM.btnAddPart.Enabled = true;            

            ////if (mcaStr != "0")
            ////{
            ////    frmBOM.lbMCA.Text = mcaStr;
            ////    frmBOM.lbMOP.Text = mopStr;
            ////}

            //frmBOM.ShowDialog();
        }               

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintModelNoConfiguration();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            PrintModelNoConfiguration();
        }

        private void btnParseModelNo_Click(object sender, EventArgs e)
        {
            string modelNoStr;
            string digitValStr;
            string heatingTypeStr = "NA";            
            string oauTypeCode = "OAU123DKN";                        
           
            if (this.txtModelNo.Text == "")
            {
                MessageBox.Show("Model Number Blank. Please enter and press Parse Model# button");
            }
            else
            {
                modelNoStr = this.txtModelNo.Text;
                if (modelNoStr.Length != 39)
                {
                    MessageBox.Show("Invalid Model Number entered. Model# is 39 char long.");
                }
                else
                {                                                                 
                    // Retrieve Cabinet Size.                    
                    digitValStr = modelNoStr.Substring(2, 1);                                                   
                    
                    Text = modelNoStr;

                    if (modelNoStr.Substring(0, 3) == "OAB" || modelNoStr.Substring(0, 3) == "OAG")
                    {
                        oauTypeCode = "OALBG";
                    }

                    //labelConfigModelNo.Text = modelNoStr;    

                    lbJobNum.Text = "";
                    lbOrderDate.Text = "";
                    lbShipDate.Text = "";
                    lbBOMCreationDate.Text = "";
                    lbLastUpdateDate.Text = "";
                    lbProdStartDate.Text = "";
                    lbCompleteDate.Text = "";
                    lbEnvironment.Text = "";
                    lbModelNo.Text = modelNoStr;
                    this.Text = modelNoStr;      
                    lbCustName.Text = "";

                    // Digit 3 - Cabinet Size
                    digitValStr = modelNoStr.Substring(2, 1);
                    DataTable dt = objMain.GetOAU_ModelNoValues(3, "NA", oauTypeCode);
                    cbCabinet.DataSource = dt;
                    cbCabinet.DisplayMember = "DigitDescription";
                    cbCabinet.ValueMember = "DigitVal";
                    cbCabinet.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 4 - Major Design
                    digitValStr = modelNoStr.Substring(3, 1);
                    dt = objMain.GetOAU_ModelNoValues(4, "NA", oauTypeCode);
                    cbMajorDesign.DataSource = dt;
                    cbMajorDesign.DisplayMember = "DigitDescription";
                    cbMajorDesign.ValueMember = "DigitVal";
                    cbMajorDesign.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 567 - Cooling Capacity               
                    digitValStr = modelNoStr.Substring(4, 3);
                    dt = objMain.GetOAU_ModelNoValues(567, "NA", oauTypeCode);
                    cbCoolingCapacity.DataSource = dt;
                    cbCoolingCapacity.DisplayMember = "DigitDescription";
                    cbCoolingCapacity.ValueMember = "DigitVal";
                    cbCoolingCapacity.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 8 - Airflow Configuration               
                    digitValStr = modelNoStr.Substring(7, 1);
                    dt = objMain.GetOAU_ModelNoValues(8, "NA", oauTypeCode);
                    cbMinorDesign.DataSource = dt;
                    cbMinorDesign.DisplayMember = "DigitDescription";
                    cbMinorDesign.ValueMember = "DigitVal";
                    cbMinorDesign.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 9 - Voltage
                    digitValStr = modelNoStr.Substring(8, 1);
                    dt = objMain.GetOAU_ModelNoValues(9, "NA", oauTypeCode);
                    cbVoltage.DataSource = dt;
                    cbVoltage.DisplayMember = "DigitDescription";
                    cbVoltage.ValueMember = "DigitVal";
                    cbVoltage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 11 - Indoor Coil Type
                    digitValStr = modelNoStr.Substring(10, 1);
                    dt = objMain.GetOAU_ModelNoValues(11, "NA", oauTypeCode);
                    cbEvapType.DataSource = dt;
                    cbEvapType.DisplayMember = "DigitDescription";
                    cbEvapType.ValueMember = "DigitVal";
                    cbEvapType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 12 - Hot Gas Reheat
                    digitValStr = modelNoStr.Substring(11, 1);
                    dt = objMain.GetOAU_ModelNoValues(12, "NA", oauTypeCode);
                    cbHotGasReheat.DataSource = dt;
                    cbHotGasReheat.DisplayMember = "DigitDescription";
                    cbHotGasReheat.ValueMember = "DigitVal";
                    cbHotGasReheat.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 13 - Compressor
                    digitValStr = modelNoStr.Substring(12, 1);
                    dt = objMain.GetOAU_ModelNoValues(13, "NA", oauTypeCode);
                    cbCompressor.DataSource = dt;
                    cbCompressor.DisplayMember = "DigitDescription";
                    cbCompressor.ValueMember = "DigitVal";
                    cbCompressor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 14 - Outdoor Coil Type
                    digitValStr = modelNoStr.Substring(13, 1);
                    dt = objMain.GetOAU_ModelNoValues(14, "NA", oauTypeCode);
                    cbCondenser.DataSource = dt;
                    cbCondenser.DisplayMember = "DigitDescription";
                    cbCondenser.ValueMember = "DigitVal";
                    cbCondenser.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 15 - Refridgerant Capacity Control
                    digitValStr = modelNoStr.Substring(14, 1);
                    dt = objMain.GetOAU_ModelNoValues(15, "NA", oauTypeCode);
                    cbRefridgerantCapacityControl.DataSource = dt;
                    cbRefridgerantCapacityControl.DisplayMember = "DigitDescription";
                    cbRefridgerantCapacityControl.ValueMember = "DigitVal";
                    cbRefridgerantCapacityControl.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 16 - Heat Type Primary
                    digitValStr = modelNoStr.Substring(15, 1);
                    dt = objMain.GetOAU_ModelNoValues(16, "NA", oauTypeCode);
                    cbIndoorFanMotor.DataSource = dt;
                    cbIndoorFanMotor.DisplayMember = "DigitDescription";
                    cbIndoorFanMotor.ValueMember = "DigitVal";
                    cbIndoorFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    if (digitValStr == "A" || digitValStr == "B" || digitValStr == "C" || digitValStr == "D" || digitValStr == "E" || digitValStr == "F")
                    {
                        heatingTypeStr = "IF";
                    }
                    else if (digitValStr == "G")
                    {
                        heatingTypeStr = "HOTWATER";
                    }
                    else if (digitValStr == "H" || digitValStr == "J")
                    {
                        heatingTypeStr = "ELEC";
                    }

                    // Digit 17 - Heat Capacity Primary
                    digitValStr = modelNoStr.Substring(16, 1);
                    dt = objMain.GetOAU_ModelNoValues(17, "NA", oauTypeCode);
                    cbIndoorFanWheel.DataSource = dt;
                    cbIndoorFanWheel.DisplayMember = "DigitDescription";
                    cbIndoorFanWheel.ValueMember = "DigitVal";
                    cbIndoorFanWheel.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 18 - Heat Type Secondary
                    digitValStr = modelNoStr.Substring(17, 1);
                    dt = objMain.GetOAU_ModelNoValues(18, "NA", oauTypeCode);
                    cbIndoorFanMotorHP.DataSource = dt;
                    cbIndoorFanMotorHP.DisplayMember = "DigitDescription";
                    cbIndoorFanMotorHP.ValueMember = "DigitVal";
                    cbIndoorFanMotorHP.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    if (digitValStr == "1" || digitValStr == "2")
                    {
                        heatingTypeStr = "DF";
                    }
                    else if (digitValStr == "3")
                    {
                        heatingTypeStr = "HOTWATER";
                    }
                    else if (digitValStr == "4" || digitValStr == "5")
                    {
                        heatingTypeStr = "ELEC";
                    }

                    // Digit 20 - Heat Type
                    digitValStr = modelNoStr.Substring(19, 1);
                    dt = objMain.GetOAU_ModelNoValues(20, heatingTypeStr, oauTypeCode);
                    cbHeatType.DataSource = dt;
                    cbHeatType.DisplayMember = "DigitDescription";
                    cbHeatType.ValueMember = "DigitVal";
                    cbHeatType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 21 - Fuel Type
                    digitValStr = modelNoStr.Substring(20, 1);
                    dt = objMain.GetOAU_ModelNoValues(21, "NA", oauTypeCode);
                    cbFuelType.DataSource = dt;
                    cbFuelType.DisplayMember = "DigitDescription";
                    cbFuelType.ValueMember = "DigitVal";
                    cbFuelType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 22 - Heat Source Primary
                    digitValStr = modelNoStr.Substring(21, 1);
                    dt = objMain.GetOAU_ModelNoValues(22, heatingTypeStr, oauTypeCode);
                    cbHeatSourcePrimary.DataSource = dt;
                    cbHeatSourcePrimary.DisplayMember = "DigitDescription";
                    cbHeatSourcePrimary.ValueMember = "DigitVal";
                    cbHeatSourcePrimary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 23 - Heat Source Secondary
                    digitValStr = modelNoStr.Substring(22, 1);
                    dt = objMain.GetOAU_ModelNoValues(23, heatingTypeStr, oauTypeCode);
                    cbHeatSourceSecondary.DataSource = dt;
                    cbHeatSourceSecondary.DisplayMember = "DigitDescription";
                    cbHeatSourceSecondary.ValueMember = "DigitVal";
                    cbHeatSourceSecondary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 24 - Corrosive Env Package
                    digitValStr = modelNoStr.Substring(23, 1);
                    dt = objMain.GetOAU_ModelNoValues(24, "NA", oauTypeCode);
                    cbCorrosiveEnvPackage.DataSource = dt;
                    cbCorrosiveEnvPackage.DisplayMember = "DigitDescription";
                    cbCorrosiveEnvPackage.ValueMember = "DigitVal";
                    cbCorrosiveEnvPackage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 25,26 - Unit Controls
                    digitValStr = modelNoStr.Substring(24, 2);
                    dt = objMain.GetOAU_ModelNoValues(2526, "NA", oauTypeCode);
                    cbUnitControls.DataSource = dt;
                    cbUnitControls.DisplayMember = "DigitDescription";
                    cbUnitControls.ValueMember = "DigitVal";
                    cbUnitControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 27 - Powered Exhaust Fan Motor
                    digitValStr = modelNoStr.Substring(26, 1);
                    dt = objMain.GetOAU_ModelNoValues(27, "NA", oauTypeCode);
                    cbPoweredExhaustFanMotor.DataSource = dt;
                    cbPoweredExhaustFanMotor.DisplayMember = "DigitDescription";
                    cbPoweredExhaustFanMotor.ValueMember = "DigitVal";
                    cbPoweredExhaustFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 28 - Powered Exhaust Fan Wheel
                    digitValStr = modelNoStr.Substring(27, 1);
                    dt = objMain.GetOAU_ModelNoValues(28, "NA", oauTypeCode);
                    cbPoweredExhaustFanWheel.DataSource = dt;
                    cbPoweredExhaustFanWheel.DisplayMember = "DigitDescription";
                    cbPoweredExhaustFanWheel.ValueMember = "DigitVal";
                    cbPoweredExhaustFanWheel.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 29 - Powered Exhaust Fan Motor HP
                    digitValStr = modelNoStr.Substring(28, 1);
                    dt = objMain.GetOAU_ModelNoValues(29, "NA", oauTypeCode);
                    cbPoweredExhaustFanMotorHP.DataSource = dt;
                    cbPoweredExhaustFanMotorHP.DisplayMember = "DigitDescription";
                    cbPoweredExhaustFanMotorHP.ValueMember = "DigitVal";
                    cbPoweredExhaustFanMotorHP.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 30 -Tane PPS Compatibility
                    digitValStr = modelNoStr.Substring(29, 1);
                    dt = objMain.GetOAU_ModelNoValues(30, "NA", oauTypeCode);
                    cbTranePPS_Compatibility.DataSource = dt;
                    cbTranePPS_Compatibility.DisplayMember = "DigitDescription";
                    cbTranePPS_Compatibility.ValueMember = "DigitVal";
                    cbTranePPS_Compatibility.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 31 - ERV_HRV
                    digitValStr = modelNoStr.Substring(30, 1);
                    dt = objMain.GetOAU_ModelNoValues(31, "NA", oauTypeCode);
                    cbERV_HRV.DataSource = dt;
                    cbERV_HRV.DisplayMember = "DigitDescription";
                    cbERV_HRV.ValueMember = "DigitVal";
                    cbERV_HRV.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 32 - ERV Size
                    digitValStr = modelNoStr.Substring(31, 1);
                    dt = objMain.GetOAU_ModelNoValues(32, "NA", oauTypeCode);
                    cbERVSize.DataSource = dt;
                    cbERVSize.DisplayMember = "DigitDescription";
                    cbERVSize.ValueMember = "DigitVal";
                    cbERVSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 33 - Economizer
                    digitValStr = modelNoStr.Substring(32, 1);
                    dt = objMain.GetOAU_ModelNoValues(33, "NA", oauTypeCode);
                    cbEconomizer.DataSource = dt;
                    cbEconomizer.DisplayMember = "DigitDescription";
                    cbEconomizer.ValueMember = "DigitVal";
                    cbEconomizer.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 34 - Filtration Options
                    digitValStr = modelNoStr.Substring(33, 1);
                    dt = objMain.GetOAU_ModelNoValues(34, "NA", oauTypeCode);
                    cbFiltrationOptions.DataSource = dt;
                    cbFiltrationOptions.DisplayMember = "DigitDescription";
                    cbFiltrationOptions.ValueMember = "DigitVal";
                    cbFiltrationOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 35 - Energy Recovery Wheel Size
                    digitValStr = modelNoStr.Substring(34, 1);
                    dt = objMain.GetOAU_ModelNoValues(35, "NA", oauTypeCode);
                    cbSmokeDetector.DataSource = dt;
                    cbSmokeDetector.DisplayMember = "DigitDescription";
                    cbSmokeDetector.ValueMember = "DigitVal";
                    cbSmokeDetector.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 36 - Electrical Options
                    digitValStr = modelNoStr.Substring(35, 1);
                    dt = objMain.GetOAU_ModelNoValues(36, "NA", oauTypeCode);
                    cbElectricalOptions.DataSource = dt;
                    cbElectricalOptions.DisplayMember = "DigitDescription";
                    cbElectricalOptions.ValueMember = "DigitVal";
                    cbElectricalOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 37 - Damper Options
                    digitValStr = modelNoStr.Substring(36, 1);
                    dt = objMain.GetOAU_ModelNoValues(37, "NA", oauTypeCode);
                    cbAirFlowMonitoring.DataSource = dt;
                    cbAirFlowMonitoring.DisplayMember = "DigitDescription";
                    cbAirFlowMonitoring.ValueMember = "DigitVal";
                    cbAirFlowMonitoring.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 38 - Hailguard 
                    digitValStr = modelNoStr.Substring(37, 1);
                    dt = objMain.GetOAU_ModelNoValues(38, "NA", oauTypeCode);
                    cbHailguard.DataSource = dt;
                    cbHailguard.DisplayMember = "DigitDescription";
                    cbHailguard.ValueMember = "DigitVal";
                    cbHailguard.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    // Digit 39 - Altitude 
                    digitValStr = modelNoStr.Substring(38, 1);
                    dt = objMain.GetOAU_ModelNoValues(39, "NA", oauTypeCode);
                    cbAltitude.DataSource = dt;
                    cbAltitude.DisplayMember = "DigitDescription";
                    cbAltitude.ValueMember = "DigitVal";
                    cbAltitude.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                    this.Refresh();

                }
            }
        }

        #region Other Methods
        private int equations(string equationvalue, string compRLAStr, string compQtyStr, string comp2QtyStr, string digCompRLAStr, string digCompQtyStr, string mcaStr, string modelNoStr)
        {
            int partQtyInt = 0;
            if (equationvalue == "LT")
            {
                if ((double.Parse(compRLAStr) * 1.25) < 40)
                {
                    partQtyInt = Int32.Parse(compQtyStr) + Int32.Parse(comp2QtyStr);// +Int32.Parse(digCompQtyStr);

                }
            }
            else if (equationvalue == "COMP_RLA65")
            {
                if (((double.Parse(compRLAStr) * 1.25) >= 40) &&
                     ((double.Parse(compRLAStr) * 1.25) < 65))
                {
                    if (digCompRLAStr == "")
                    {
                        partQtyInt = Int32.Parse(compQtyStr) + Int32.Parse(comp2QtyStr);
                    }
                    else
                    {
                        if (((double.Parse(digCompRLAStr) * 1.25) >= 40) &&
                            ((double.Parse(digCompRLAStr) * 1.25) < 65))
                        {
                            partQtyInt = Int32.Parse(compQtyStr) + Int32.Parse(comp2QtyStr) + Int32.Parse(digCompQtyStr);
                        }
                        else
                        {
                            partQtyInt = Int32.Parse(compQtyStr) + Int32.Parse(comp2QtyStr);
                        }
                    }

                }
            }
            else if (equationvalue == "COMP_RLA72")
            {
                if (digCompRLAStr != "")
                {
                    if (((double.Parse(digCompRLAStr) * 1.25) >= 66) &&
                        ((double.Parse(digCompRLAStr) * 1.25) < 72))
                    {
                        partQtyInt += Int32.Parse(digCompQtyStr) + Int32.Parse(comp2QtyStr);

                    }
                }
            }
            else if (equationvalue == "COMP1") // One per compressor
            {
                partQtyInt = Int32.Parse(compQtyStr) + Int32.Parse(comp2QtyStr);// + Int32.Parse(digCompQtyStr);

            }
            else if (equationvalue == "MCA_LT_165")
            {
                if (double.Parse(mcaStr) < 165)
                {
                    if (modelNoStr.Substring(19, 1) == "D")
                    {

                    }
                    else
                    {
                        partQtyInt = 1;

                    }
                }
            }
            else if (equationvalue == "MCA_GT_165")
            {
                if (double.Parse(mcaStr) > 165)
                {
                    partQtyInt = 1;

                }

            }

            return partQtyInt;
        }

        private string getFLAValue(string flaValue, string voltage)
        {
            int dashPos = flaValue.IndexOf("-");

            if (dashPos > 0)
            {
                if (voltage == "208")
                {
                    flaValue = flaValue.Substring(0, dashPos);
                }
                else
                {
                    flaValue = flaValue.Substring((dashPos + 1), (flaValue.Length - (dashPos+1)));
                }
            }

            return flaValue;
        }

        private DataTable AddPartToBOMDataTable(DataTable dtBOM, string partNumStr, string ruleHeadIdStr, string qtyStr, string partDescStr, string relatedOpStr, string revisionNumStr,
                                                 string partCategoryStr, string partStatusStr, string seqNumStr, string motorTypeStr, string jobNumStr, string asmSeqStr, string parentPartNumStr)            
        {
            decimal partQtyDec = 0;

            string iumStr = "";
            string unitPriceStr = "0";
            string inActiveStr = "";
            string costMethodStr = "";

            DataTable dtPart = objMain.GetPartDetail(partNumStr);

            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                partDescStr = drPart["PartDescription"].ToString();
                iumStr = drPart["IUM"].ToString();
                unitPriceStr = drPart["UnitCost"].ToString();
                inActiveStr = drPart["InActive"].ToString();
                costMethodStr = drPart["CostMethod"].ToString();

                if (inActiveStr == "0")
                {
                    partStatusStr = "Active";
                }
                else
                {
                    partStatusStr = "InActive";
                }
            }

            if (ruleHeadIdStr.Length == 0)
            {
                ruleHeadIdStr = "0";
            }

            var dr = dtBOM.NewRow();
            dr["PartCategory"] = partCategoryStr;
            dr["AssemblySeq"] = Int32.Parse(asmSeqStr);
            dr["SeqNo"] = seqNumStr;
            dr["PartNum"] = partNumStr;
            dr["Description"] = partDescStr;

            string partQtyStr = qtyStr;

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            dr["ReqQty"] = partQtyDec;
            dr["UOM"] = iumStr;
            dr["UnitCost"] = decimal.Parse(unitPriceStr);
            dr["WhseCode"] = "LWH2";
            dr["RelOp"] = Int32.Parse(relatedOpStr);
            dr["PartStatus"] = partStatusStr;
            dr["RevisionNum"] = revisionNumStr;
            dr["CostMethod"] = costMethodStr;
            dr["PartType"] = motorTypeStr;
            dr["Status"] = "BackFlush";
            dr["ParentPartNum"] = parentPartNumStr;
            dr["PROGRESS_RECID"] = 0;
            dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
           

            dtBOM.Rows.Add(dr);

            return dtBOM;
        }

        private string GetMotorKw(string modelNoValStr)
        {
            string retValStr = "";

            switch (modelNoValStr)
            {
                case "A":
                    retValStr = "1KW";
                    break;
                case "B":
                    retValStr = "2KW";
                    break;
                case "C":
                    retValStr = "3KW";
                    break;
                case "D":
                    retValStr = "4KW";
                    break;
                default:
                    retValStr = "0KW";
                    break;
            }

            return retValStr;
        }

        //private DataTable CreateBOM(DataTable dtBOM, string jobNum, string modelNo, frmDisplayBOM frmBOM)
        //{
        //    DataTable dtMotor = new DataTable();
        //    DataTable dtModel = new DataTable();
        //    DataTable dtPartRules = new DataTable();
        //    DataTable dtPartCondition = new DataTable();

        //    bool rfgAsmFound = false;
        //    bool standAloneCompFound = false;
        //    bool multiFanMotorsBool = false;
        //    bool multiPwrExhMotorsBool = false;

        //    int voltageInt = 0;
        //    int phaseInt = 0;
        //    int seqNumInt = 10;
        //    int fanEvapMotorCnt = 0;
        //    int fanPwrExhMotorCnt = 0;
        //    int condFanMotorQtyInt = 0;
        //    int compNum = 0;
        //    int tempQtyInt = 0;
        //    int qtyInt = 0;
        //    int tandemAsmSeq = 1;
        //    int assemblySeqNum0Int = 10;
        //    int assemblySeqNum1Int = 10;
        //    int assemblySeqNum2Int = 10;
        //    int assemblySeqNum3Int = 10;

        //    decimal hertzDec = 0;
        //    decimal tmpDec = 0;

        //    string fanTypeStr = "";
        //    string partNumStr = "";
        //    string parentPartNumStr = "";
        //    string partTypeStr = "";
        //    string motorTypeStr = "";
        //    string unitTypeStr = modelNo.Substring(0, 3);
        //    string revisionNumStr = "";
        //    string relatedOpStr = "";
        //    string mopStr = "";
        //    string ruleHeadID = "";
        //    string ruleBatchID = "";
        //    string voltageStr = "";
        //    string statusStr = "Backflush";      
        //    string fanCondQtyStr = "";
        //    string fanCondFLAStr = "";
        //    string fanCondHPStr = "";
        //    string fanEvapQtyStr = "0";
        //    string fanEvapFLAStr = "";
        //    string fanEvapHPStr = "";
        //    string fanEvapPartNumStr = "";
        //    string fanERVQtyStr = "";
        //    string fanERVFLAStr = "";
        //    string fanERVHPStr = "";
        //    string fanPWRExhQtyStr = "0";
        //    string fanPWRExhFLAStr = "";
        //    string fanPWRExhHPStr = "";
        //    string fanPWRExhPartNumStr = "";
        //    string tmp_MinCKTAmpStr = "";
        //    string tmp_MopStr = "";
        //    string compTypeStr = "";
        //    string compRLA_VoltsStr = "";
        //    string compRLAStr = "0";
        //    string compQtyStr = "0";
        //    string compLRAStr = "0";
        //    string comp2QtyStr = "0";
        //    string comp2RLAStr = "0";
        //    string comp2LRAStr = "0";
        //    string digCompQtyStr = "0";
        //    string digCompLRAStr = "0";
        //    string digCompRLAStr = "0";
        //    string kwStr = "";
        //    string heatingInputElecStr = "";
        //    string mcaStr = "";
        //    string rawMopStr = "";
        //    string qtyStr = "0";
        //    string flaStr = "";
        //    string hpStr = "";
        //    string partDescStr = "";
        //    string partCat = "";
        //    string relatedOperationStr = "";
        //    string assemblySeqStr = "";
        //    string parentPartNum = "";
        //    string preHtgInputStr = "0";

        //    int alt = Convert.ToInt32(modelNo.Substring(38, 1));
        //    if (alt >= 3)
        //    {
        //        MessageBox.Show("Altitude is above 3,000 feet. Please Check Burner Comfiguration.");
        //    }

        //    if (("AK, AL, AM, AN, AO, AP, AQ, AR").Contains(modelNo.Substring(24, 2).ToString()))
        //    {
        //        if (!(("B,C").Contains(modelNo.Substring(12, 1).ToString())))
        //        {
        //            MessageBox.Show("VAV Control Digit Compressor Required");
        //        }
        //        else if (modelNo.Substring(15, 1).ToString() != "0")
        //        {
        //            MessageBox.Show("VAV Control NO APR Allowed");
        //        }
        //        else if (("0,2").Contains(modelNo.Substring(36, 1).ToString()))
        //        {
        //            MessageBox.Show("VAV Control Unit Must Have Piezo Ring");
        //        }
        //    }

        //    //method to check for X in any digit
        //    for (int x = 0; x <= 38; x++)
        //    {
        //        if ((modelNo.Substring(x, 1) == "X") || (modelNo.Substring(x, 1) == "x"))
        //        {
        //            MessageBox.Show("Model digit: " + (x + 1) + " contains an X");
        //        }
        //    }

        //    if ((modelNo.Substring(7, 1) == "D") && (modelNo.Substring(32, 1) == "0"))
        //    {
        //        MessageBox.Show("WARNING, Horizontal Discharge/Horizontal Return with 2-Position Damper.");
        //    }

        //    string unitType = modelNo.Substring(0, 3);

        //    switch (modelNo.Substring(8, 1))
        //    {
        //        case "1":
        //            gVoltage = 115;
        //            gHertz = 60;
        //            gPhase = 1;
        //            break;
        //        case "2":
        //            gVoltage = 208;
        //            gHertz = 60;
        //            gPhase = 1;
        //            break;
        //        case "3":
        //            gVoltage = 208;
        //            gHertz = 60;
        //            gPhase = 3;
        //            break;
        //        case "4":
        //            gVoltage = 460;
        //            gHertz = 60;
        //            gPhase = 3;
        //            break;
        //        case "5":
        //            gVoltage = 575;
        //            gHertz = 60;
        //            gPhase = 3;
        //            break;
        //        default:
        //            gVoltage = 208;
        //            gHertz = 60;
        //            gPhase = 3;
        //            break;
        //    }

        //    voltageStr = gVoltage.ToString();

        //    if (modelNo.Substring(16, 1) == "L" || modelNo.Substring(16, 1) == "M")
        //    {
        //        multiFanMotorsBool = true;
        //    }

        //    if (modelNo.Substring(27, 1) == "L" || modelNo.Substring(27, 1) == "M")
        //    {
        //        multiPwrExhMotorsBool = true;
        //    }

        //    if (modelNo.Substring(19, 1) == "J")
        //    {
        //        frmBOM.lbFlowRate.Text = this.txtFlowRate.Text;
        //        frmBOM.lbEnteringTemp.Text = this.txtEnteringTemp.Text;
        //    }


        //    DataTable dtPartCat = objMain.GetPartCategoryHead("REV5");

        //    foreach (DataRow categoryrow in dtPartCat.Rows)
        //    {
        //        partCat = categoryrow["CategoryName"].ToString();

        //        dtPartRules = objMain.GetPartNumbersByCategory(partCat);

        //        foreach (DataRow Part in dtPartRules.Rows)
        //        {
        //            int rulecount = 0;
        //            int truecount = 0;
        //            int batchID = 0;
        //            int currentBatchID = -1;
        //            string qty = "";

        //            //Part Conditions sorted by RuleBatchID ASC
        //            ruleHeadID = Part["RuleHeadID"].ToString();                     
        //            partNumStr = Part["PartNum"].ToString();
        //            assemblySeqStr = Part["AssemblySeq"].ToString();

        //            if (Part["PartNum"].ToString().Contains("VCPRFG-0096"))
        //            {
        //                string tcao = Part["PartNum"].ToString();
        //            }

        //            dtPartCondition = objMain.GetPartConditionsByRuleHeadID(Convert.ToInt32(ruleHeadID), false);

        //            foreach (DataRow PartConditionrow in dtPartCondition.Rows)
        //            {
        //                int ruledigit = 0;
        //                int ruledigitStr = 0;
        //                batchID = Convert.ToInt32(PartConditionrow["RuleBatchID"].ToString());

        //                motorTypeStr = "";
        //                if (currentBatchID == batchID)
        //                {
        //                    if (PartConditionrow["Digit"].ToString().Contains("567"))
        //                    {
        //                        ruledigit = 5;
        //                        ruledigitStr = 3;
        //                    }
        //                    else if (PartConditionrow["Digit"].ToString().Contains("2526"))
        //                    {
        //                        ruledigit = 25;
        //                        ruledigitStr = 2;
        //                    }
        //                    else if (PartConditionrow["Digit"].ToString() == "0")
        //                    {
        //                        ruledigit = 1;
        //                        ruledigitStr = 1;
        //                    }
        //                    else
        //                    {
        //                        ruledigit = Convert.ToInt16(PartConditionrow["Digit"].ToString());
        //                        ruledigitStr = 1;
        //                    }

        //                    if (PartConditionrow["Digit"].ToString() == "18")
        //                    {
        //                        fanTypeStr = "Evap";
        //                    }
        //                    else if (PartConditionrow["Digit"].ToString() == "29")
        //                    {
        //                        fanTypeStr = "PwrExh";
        //                    }

        //                    if (ruledigit == 0)
        //                    {
        //                        if (PartConditionrow["Unit"].ToString().Trim() == unitTypeStr && PartConditionrow["Value"].ToString() == mopStr)
        //                        {
        //                            ++truecount;
        //                        }
        //                    }
        //                    else if (PartConditionrow["Unit"].ToString().Trim() == unitTypeStr && PartConditionrow["Value"].ToString().Contains((modelNo.Substring((ruledigit - 1), ruledigitStr))))
        //                    {
        //                        ++truecount;
        //                    }
        //                    ++rulecount;
        //                }
        //                else
        //                {
        //                    if (rulecount > 0)
        //                    {
        //                        if (rulecount == truecount)
        //                        {
        //                            //method add part to list

        //                            if (Part["PartNum"].ToString().Contains("VCPRFG-0430"))
        //                            {
        //                                string tcao = "Bell";
        //                            }

        //                            if (Part["PartCategory"].ToString().Contains("Motor") || Part["PartCategory"].ToString().Contains("Mtr"))
        //                            {
        //                                DataTable dtMotorData = objMain.GetMotorData(Convert.ToInt32(ruleHeadID));

        //                                if (dtMotorData.Rows.Count > 0)
        //                                {
        //                                    DataRow dr = dtMotorData.Rows[0];
        //                                    motorTypeStr = dr["MotorType"].ToString();
        //                                    if (fanTypeStr != "")
        //                                    {
        //                                        motorTypeStr += "-" + fanTypeStr;
        //                                    }
        //                                }
        //                            }

        //                            if (assemblySeqStr == "0")
        //                            {
        //                                relatedOperationStr = Part["RelatedOperation"].ToString();
        //                                seqNumInt = assemblySeqNum0Int;
        //                                assemblySeqNum0Int += 10;
        //                                parentPartNum = "";
        //                            }
        //                            else if (assemblySeqStr == "1")
        //                            {
        //                                relatedOperationStr = "10";
        //                                seqNumInt = assemblySeqNum1Int;
        //                                assemblySeqNum1Int += 10;
        //                                parentPartNum = "OACONTROLPANEL";
        //                            }
        //                            else if (assemblySeqStr == "3")
        //                            {
        //                                relatedOperationStr = "10";
        //                                seqNumInt = assemblySeqNum3Int;
        //                                assemblySeqNum3Int += 10;
        //                                parentPartNum = "RFG-COMPRESSOR";
        //                                standAloneCompFound = true;
        //                            }

        //                            if (Part["PartNum"].ToString().Contains("RFGASM"))
        //                            {
        //                                rfgAsmFound = true;
        //                                parentPartNum = Part["PartNum"].ToString();
        //                                parentPartNum = parentPartNum + " (" + decimal.Parse(qty).ToString("f0") + ")";
        //                                assemblySeqNum2Int = 10;
        //                                ++tandemAsmSeq;
        //                                assemblySeqStr = tandemAsmSeq.ToString();
        //                                DataTable dtAsmParts = objMain.GetTandemCompAssemblyMtls(partNumStr);
        //                                foreach (DataRow dr in dtAsmParts.Rows)
        //                                {
        //                                    qtyStr = dr["ReqQty"].ToString();
        //                                    tmpDec = decimal.Parse(qty) * decimal.Parse(qtyStr);
        //                                    qtyStr = tmpDec.ToString();

        //                                    dtBOM = AddPartToBOMDataTable(dtBOM, dr["PartNum"].ToString(), dr["RuleHeadID"].ToString(), qtyStr, dr["Description"].ToString(),
        //                                                                  dr["RelOp"].ToString(), dr["RevisionNum"].ToString(), dr["PartCategory"].ToString(), dr["Status"].ToString(),
        //                                                                  assemblySeqNum2Int.ToString(), dr["PartType"].ToString(), jobNum, assemblySeqStr, parentPartNum);

        //                                    //AddPartToBOMDataTable(dr["PartNum"].ToString(), dr["ReqQty"].ToString(), dr["Description"].ToString(), dr["RelOp"].ToString(),
        //                                    //                      dr["RevisionNum"].ToString(), dr["PartCategory"].ToString(), dr["Status"].ToString(), assemblySeqNum2Int.ToString(),
        //                                    //                      dr["PartType"].ToString(), assemblySeqStr, parentPartNum, frmBOM);

        //                                    assemblySeqNum2Int += 10;
        //                                }
        //                            }                                   
        //                            else
        //                            {                                        
        //                                dtBOM = AddPartToBOMDataTable(dtBOM, Part["PartNum"].ToString(), Part["RuleHeadID"].ToString(), qty, "desc", relatedOperationStr, revisionNumStr,
        //                                                              Part["PartCategory"].ToString(), statusStr, seqNumInt.ToString(), motorTypeStr, jobNum, assemblySeqStr, parentPartNum);
        //                                //addPartToListView(Part["PartNum"].ToString(), qty, "desc", relatedOperationStr, revisionNumStr,
        //                                //                  Part["PartCategory"].ToString(), statusStr, seqNumInt.ToString(), motorTypeStr, assemblySeqStr, parentPartNum, frmBOM);
        //                                seqNumInt = seqNumInt + 10;
        //                                fanTypeStr = "";
        //                            }
        //                        }
        //                    }

        //                    truecount = 0;
        //                    rulecount = 0;
        //                    fanTypeStr = "";
        //                    currentBatchID = batchID;
        //                    qty = PartConditionrow["Qty"].ToString();
        //                    if (PartConditionrow["Digit"].ToString().Contains("567"))
        //                    {
        //                        ruledigit = 5;
        //                        ruledigitStr = 3;
        //                    }
        //                    else if (PartConditionrow["Digit"].ToString().Contains("2526"))
        //                    {
        //                        ruledigit = 25;
        //                        ruledigitStr = 2;
        //                    }
        //                    else if (PartConditionrow["Digit"].ToString() == "0")
        //                    {
        //                        ruledigit = 1;
        //                        ruledigitStr = 1;
        //                    }
        //                    else
        //                    {
        //                        ruledigit = Convert.ToInt16(PartConditionrow["Digit"].ToString());
        //                        ruledigitStr = 1;
        //                    }

        //                    if (PartConditionrow["Digit"].ToString() == "18")
        //                    {
        //                        fanTypeStr = "Evap";
        //                    }
        //                    else if (PartConditionrow["Digit"].ToString() == "29")
        //                    {
        //                        fanTypeStr = "PwrExh";
        //                    }

        //                    //if (Part["PartNum"].ToString().Contains("VCPRFG-0066"))
        //                    //{
        //                    //    string DIGIGI = PartConditionrow["Digit"].ToString();
        //                    //    int NUM = Convert.ToInt16(PartConditionrow["Digit"].ToString());
        //                    //    string partnumer = Part["PartNum"].ToString();
        //                    //    string unita = PartConditionrow["Unit"].ToString();
        //                    //    string modelsubstring = model.Substring((ruledigit - 1), ruledigitStr);
        //                    //    string prat = PartConditionrow["Value"].ToString();                                         
        //                    //}

        //                    if (ruledigit == 0)
        //                    {
        //                        if (PartConditionrow["Unit"].ToString().Trim() == unitTypeStr && PartConditionrow["Value"].ToString() == mopStr)
        //                        {
        //                            ++truecount;
        //                        }
        //                    }
        //                    else if (PartConditionrow["Unit"].ToString().Trim() == unitTypeStr && PartConditionrow["Value"].ToString().Contains((modelNo.Substring((ruledigit - 1), ruledigitStr))))
        //                    {
        //                        ++truecount;
        //                    }
        //                    ++rulecount;
        //                }
        //            }

        //            if (rulecount > 0)
        //            {
        //                if (rulecount == truecount)
        //                {
        //                    //method add part to list
        //                    if (Part["PartNum"].ToString().Contains("VCPRFG-0430"))
        //                    {
        //                        string taco = "Bell";
        //                    }

        //                    if (Part["PartCategory"].ToString().Contains("Motor") || Part["PartCategory"].ToString().Contains("Mtr"))
        //                    {
        //                        dtMotor = objMain.GetMotorData(Convert.ToInt32(ruleHeadID));

        //                        if (dtMotor.Rows.Count > 0)
        //                        {
        //                            DataRow dr = dtMotor.Rows[0];
        //                            motorTypeStr = dr["MotorType"].ToString();
        //                            if (fanTypeStr != "")
        //                            {
        //                                motorTypeStr += "-" + fanTypeStr;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if ((Part["PartNum"].ToString().Contains("ERV")) || (Part["PartNum"].ToString().Contains("MTR")))
        //                            {
        //                                MessageBox.Show("WARNING -- No FLA Data found for Part Number ==> " + Part["PartNum"].ToString() + "!  This will make MCA & MOP calculations incorrect.");
        //                            }
        //                        }
        //                    }

        //                    if (assemblySeqStr == "0")
        //                    {
        //                        relatedOperationStr = Part["RelatedOperation"].ToString();
        //                        seqNumInt = assemblySeqNum0Int;
        //                        assemblySeqNum0Int += 10;
        //                        parentPartNum = "";
        //                    }
        //                    else if (assemblySeqStr == "1")
        //                    {
        //                        relatedOperationStr = "10";
        //                        seqNumInt = assemblySeqNum1Int;
        //                        assemblySeqNum1Int += 10;
        //                        parentPartNum = "OACONTROLPANEL";
        //                    }
        //                    else if (assemblySeqStr == "3")
        //                    {
        //                        relatedOperationStr = "10";
        //                        seqNumInt = assemblySeqNum3Int;
        //                        assemblySeqNum3Int += 10;
        //                        parentPartNum = "RFG-COMPRESSOR";
        //                        standAloneCompFound = true;
        //                    }

        //                    if (Part["PartNum"].ToString().Contains("RFGASM"))
        //                    {
        //                        rfgAsmFound = true;
        //                        parentPartNum = Part["PartNum"].ToString();
        //                        parentPartNum = parentPartNum + " (" + decimal.Parse(qty).ToString("f0") + ")";
        //                        assemblySeqNum2Int = 10;
        //                        ++tandemAsmSeq;
        //                        assemblySeqStr = tandemAsmSeq.ToString();
        //                        DataTable dtAsmParts = objMain.GetTandemCompAssemblyMtls(partNumStr);
        //                        foreach (DataRow dr in dtAsmParts.Rows)
        //                        {
        //                            qtyStr = dr["ReqQty"].ToString();
        //                            tmpDec = decimal.Parse(qty) * decimal.Parse(qtyStr);
        //                            qtyStr = tmpDec.ToString();

        //                            dtBOM = AddPartToBOMDataTable(dtBOM, dr["PartNum"].ToString(), dr["RuleHeadID"].ToString(), qtyStr, dr["Description"].ToString(),
        //                                                          dr["RelOp"].ToString(), dr["RevisionNum"].ToString(), dr["PartCategory"].ToString(), dr["Status"].ToString(),
        //                                                          assemblySeqNum2Int.ToString(), dr["PartType"].ToString(), jobNum, assemblySeqStr, parentPartNum);

        //                            //AddPartToBOMDataTable(dr["PartNum"].ToString(), dr["ReqQty"].ToString(), dr["Description"].ToString(), dr["RelOp"].ToString(),
        //                            //                      dr["RevisionNum"].ToString(), dr["PartCategory"].ToString(), dr["Status"].ToString(), assemblySeqNum2Int.ToString(),
        //                            //                      dr["PartType"].ToString(), assemblySeqStr, parentPartNum, frmBOM);

        //                            assemblySeqNum2Int += 10;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        dtBOM = AddPartToBOMDataTable(dtBOM, Part["PartNum"].ToString(), Part["RuleHeadID"].ToString(), qty, "desc", relatedOperationStr, revisionNumStr,
        //                                                      Part["PartCategory"].ToString(), statusStr, seqNumInt.ToString(), motorTypeStr, jobNum, assemblySeqStr, parentPartNum);                                
        //                        seqNumInt = seqNumInt + 10;
        //                        fanTypeStr = "";
        //                    }

        //                    //dtBOM = AddPartToBOMDataTable(dtBOM, Part["PartNum"].ToString(), Part["RuleHeadID"].ToString(), qty, "desc", relatedOperationStr, revisionNumStr,
        //                    //                                          Part["PartCategory"].ToString(), statusStr, seqNumInt.ToString(), motorTypeStr, jobNum, assemblySeqStr, "");
        //                    //seqNumInt = seqNumInt + 10;
        //                    //fanTypeStr = "";
        //                }
        //            }
        //        }
        //    }

        //    //calculation of mop, compRLA, compressor qtys, 
        //    List<Compressor> CompList = new List<Compressor>();

        //    foreach (DataRow PartRow in dtBOM.Rows)
        //    {
        //        partNumStr = PartRow["PartNum"].ToString();
        //        parentPartNumStr = PartRow["ParentPartNum"].ToString();
        //        motorTypeStr = PartRow["PartType"].ToString();
        //        partTypeStr = PartRow["PartCategory"].ToString();
        //        ruleHeadID = PartRow["RuleHeadID"].ToString();
        //        fanTypeStr = "";

        //        if (partTypeStr == "Compressor" || partTypeStr.Contains("DigitalScroll") || partTypeStr.Contains("TandemComp"))
        //        {
        //            compQtyStr = PartRow["ReqQty"].ToString().Substring(0, PartRow["ReqQty"].ToString().IndexOf('.'));
        //            compTypeStr = partTypeStr;
        //            objMain.getCompRLA_Data(partNumStr, voltageStr, out compRLA_VoltsStr, out compLRAStr, "REV5", ruleHeadID);
        //            if (decimal.Parse(compRLA_VoltsStr) > 0)
        //            {
        //                ++compNum;
        //                CompList.Add(new Compressor(compNum.ToString(), partNumStr, parentPartNumStr, compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
        //            }                    
        //        }

        //        if ((partNumStr.Contains("VCPERV-0105")) || (partNumStr.Contains("VELMTR-0081")))
        //        {
        //            string taco = "bell";
        //        }

        //        if (motorTypeStr == "Condensor")//"Cond FAN Motor")
        //        {
        //            condFanMotorQtyInt = Int32.Parse(PartRow["ReqQty"].ToString().Substring(0, PartRow["ReqQty"].ToString().IndexOf('.')));
        //            fanCondQtyStr = condFanMotorQtyInt.ToString();

        //            dtMotor = objMain.GetMotorDataDTL(gVoltage, gHertz, gPhase, "REV5", partNumStr);

        //            if (dtMotor.Rows.Count > 0)
        //            {
        //                DataRow drMotor = dtMotor.Rows[0];
        //                fanCondFLAStr = getFLAValue(drMotor["FLA"].ToString(), voltageStr);
        //                fanCondHPStr = drMotor["HP"].ToString();
        //            }
        //        }
        //        else if (motorTypeStr == "ERV")
        //        {
        //            tempQtyInt = Int32.Parse(PartRow["ReqQty"].ToString().Substring(0, PartRow["ReqQty"].ToString().IndexOf('.')));
        //            fanERVQtyStr = tempQtyInt.ToString();

        //            dtMotor = objMain.GetMotorDataDTL(gVoltage, gHertz, gPhase, "REV5", partNumStr);

        //            if (dtMotor.Rows.Count > 0)
        //            {
        //                DataRow drMotor = dtMotor.Rows[0];
        //                fanERVFLAStr = getFLAValue(drMotor["FLA"].ToString(), voltageStr);
        //                fanERVHPStr = drMotor["HP"].ToString();
        //            }
        //        }
        //        else if (motorTypeStr.Contains("FAN"))
        //        {
        //            qtyStr = PartRow["ReqQty"].ToString().Substring(0, PartRow["ReqQty"].ToString().IndexOf('.'));

        //            dtMotor = objMain.GetMotorDataDTL(gVoltage, gHertz, gPhase, "REV5", partNumStr);

        //            if (dtMotor.Rows.Count > 0)
        //            {
        //                DataRow drMotor = dtMotor.Rows[0];
        //                flaStr = getFLAValue(drMotor["FLA"].ToString(), voltageStr);
        //                hpStr = drMotor["HP"].ToString();
        //            }

        //            if (unitTypeStr == "OAB" || unitTypeStr == "OAG")
        //            {
        //                if (motorTypeStr.Contains("Evap"))
        //                {
        //                    kwStr = GetMotorKw(modelNo.Substring(17, 1));
        //                    partDescStr = PartRow["Description"].ToString().ToUpper();
        //                    fanEvapPartNumStr = partNumStr;
        //                    fanEvapQtyStr = (decimal.Parse(qtyStr) + decimal.Parse(fanEvapQtyStr)).ToString();
        //                    fanEvapFLAStr = flaStr;
        //                    fanEvapHPStr = hpStr;
        //                    PartRow["PartType"] = "Indoor FAN";
        //                    fanEvapMotorCnt += Int32.Parse(qtyStr);
        //                }
        //                else
        //                {
        //                    kwStr = GetMotorKw(modelNo.Substring(28, 1));
        //                    fanPWRExhPartNumStr = partNumStr;
        //                    fanPWRExhQtyStr = (decimal.Parse(qtyStr) + decimal.Parse(fanPWRExhQtyStr)).ToString(); ;
        //                    fanPWRExhFLAStr = flaStr;
        //                    fanPWRExhHPStr = hpStr;
        //                    PartRow["PartType"] = "Powered Exhaust";
        //                    fanPwrExhMotorCnt += Int32.Parse(qtyStr);
        //                }
        //            }
        //            else
        //            {
        //                if (motorTypeStr.Contains("Evap"))
        //                {
        //                    fanEvapPartNumStr = partNumStr;
        //                    fanEvapQtyStr = (decimal.Parse(qtyStr) + decimal.Parse(fanEvapQtyStr)).ToString();
        //                    fanEvapFLAStr = flaStr;
        //                    fanEvapHPStr = hpStr;
        //                    PartRow["PartType"] = "Indoor FAN";
        //                    ++fanEvapMotorCnt;
        //                }
        //                else
        //                {
        //                    fanPWRExhPartNumStr = partNumStr;
        //                    fanPWRExhQtyStr = (decimal.Parse(qtyStr) + decimal.Parse(fanPWRExhQtyStr)).ToString(); ;
        //                    fanPWRExhFLAStr = flaStr;
        //                    fanPWRExhHPStr = hpStr;
        //                    PartRow["PartType"] = "Powered Exhaust";
        //                    ++fanPwrExhMotorCnt;
        //                }
        //            }
        //        }
        //    }

        //    if (fanEvapQtyStr == "0")
        //    {
        //        fanEvapQtyStr = "";
        //    }

        //    if (fanPWRExhQtyStr == "0")
        //    {
        //        fanPWRExhQtyStr = "";
        //    }

        //    objMain.calcAmpacityValues(out mcaStr, out mopStr, out rawMopStr, CompList, fanCondFLAStr, fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr, 
        //                               fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageStr, preHtgInputStr);

        //    if (modelNo.Substring(19, 1) == "C" || modelNo.Substring(19, 1) == "D" || modelNo.Substring(19, 1) == "F" || modelNo.Substring(19, 1) == "G" || modelNo.Substring(19, 1) == "H")
        //    {
        //        string OAUTypeCode = "";
        //        unitType = modelNo.Substring(2, 1);
        //        //kwStr = this.comboBoxInputsHeatCapPrimarySourceElectric.Text;
        //        if (unitType == "L" || unitType == "M" || unitType == "B" || unitType == "G")
        //        {
        //            OAUTypeCode = "OALBG";
        //        }
        //        else
        //        {
        //            OAUTypeCode = "OAU123DKN";
        //        }

        //        if (modelNo.Substring(19, 1) == "C" || modelNo.Substring(19, 1) == "D" || modelNo.Substring(19, 1) == "F" || modelNo.Substring(19, 1) == "H")
        //        {
        //            if ((modelNo.Substring(21, 1) != "0") && (modelNo.Substring(21, 1) != "X"))
        //            {
        //                dtModel = objMain.GetRev5ModelNoDigitDesc(22, modelNo.Substring(21, 1), "ELEC", OAUTypeCode);//"ELEC", OAUTypeCode);

        //                if (dtModel.Rows.Count > 0)
        //                {
        //                    DataRow drModel = dtModel.Rows[0];
        //                    kwStr = drModel["DigitDescription"].ToString();
        //                }
        //            }
        //            else
        //            {
        //                kwStr = "0";
        //            }
        //        }
        //        else
        //        {
        //            if ((modelNo.Substring(22, 1) != "0") && (modelNo.Substring(22, 1) != "X"))
        //            {
        //                dtModel = objMain.GetRev5ModelNoDigitDesc(23, modelNo.Substring(22, 1), "ELEC", OAUTypeCode);//"ELEC", OAUTypeCode);

        //                if (dtModel.Rows.Count > 0)
        //                {
        //                    DataRow drModel = dtModel.Rows[0];
        //                    kwStr = drModel["DigitDescription"].ToString();
        //                }
        //            }
        //            else
        //            {
        //                kwStr = "0";
        //            }
        //        }

        //        if (kwStr != "0")
        //        {
        //            if (OAUTypeCode == "OALBG" && !(modelNo.Substring(19, 1) == "C" || modelNo.Substring(19, 1) == "D" || modelNo.Substring(19, 1) == "F" || modelNo.Substring(19, 1) == "H"))
        //            {
        //                int index = 0;
        //                int index2 = kwStr.ToUpper().IndexOf("KW");
        //                int leg = index2 - index;
        //                int scount = kwStr.Length;
        //                kwStr = kwStr.Substring((index + 1), (leg - 1));
        //                kwStr = kwStr.Trim() + "000";
        //            }
        //            else
        //            {
        //                int index = kwStr.IndexOf("/");
        //                int index2 = kwStr.ToUpper().IndexOf("KW");
        //                int leg = index2 - index;
        //                int scount = kwStr.Length;
        //                kwStr = kwStr.Substring((index + 1), (leg - 1));
        //                kwStr = kwStr.Trim() + "000";
        //            }
        //        }

        //        double tmpDbl = double.Parse(kwStr) / double.Parse(voltageStr);
        //        double primHtgInputElecDbl = tmpDbl / Math.Sqrt(3);
        //        double secHtgInputElecDbl = 0;

        //        if (modelNo.Substring(19, 1) == "H")
        //        {
        //            dtModel = objMain.GetRev5ModelNoDigitDesc(23, modelNo.Substring(22, 1), "ELEC", OAUTypeCode);//"ELEC", OAUTypeCode);

        //            if (dtModel.Rows.Count > 0)
        //            {
        //                DataRow drModel = dtModel.Rows[0];
        //                kwStr = drModel["DigitDescription"].ToString();
        //            }

        //            if (kwStr.ToUpper().IndexOf("KW") > 0)
        //            {
        //                kwStr = kwStr.Substring(0, kwStr.ToUpper().IndexOf("KW"));
        //                kwStr = kwStr.Trim();
        //                kwStr = kwStr + "000";
        //            }

        //            tmpDbl = double.Parse(kwStr) / double.Parse(voltageStr);
        //            secHtgInputElecDbl = tmpDbl / Math.Sqrt(3);
        //        }

        //        heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
        //        //tmp_MinCKTAmpStr = mcaStr;
        //        //tmp_MFSMCBStr = mopStr;                                             

        //        objMain.calcElecAmpacityValues(out tmp_MinCKTAmpStr, out tmp_MopStr, fanEvapFLAStr, fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr,
        //                                       fanPWRExhFLAStr, fanPWRExhQtyStr, voltageStr, heatingInputElecStr);

        //        if (double.Parse(tmp_MinCKTAmpStr) > double.Parse(mcaStr))
        //        {
        //            mcaStr = tmp_MinCKTAmpStr;

        //            if (double.Parse(tmp_MopStr) > double.Parse(mopStr))
        //            {
        //                mopStr = objMain.roundElecMOP(double.Parse(mcaStr));
        //            }
        //        }
        //    }
        //    frmBOM.lbMCA.Text = mcaStr;
        //    frmBOM.lbMOP.Text = mopStr;

        //    //get parts based on calculations/equations
        //    foreach (DataRow categoryrow in dtPartCat.Rows)
        //    {
        //        partCat = categoryrow["CategoryName"].ToString();

        //        dtPartRules = objMain.GetPartNumbersByCategory(partCat);

        //        foreach (DataRow Part in dtPartRules.Rows)
        //        {
        //            int rulecount = 0;
        //            int truecount = 0;
        //            int batchID = 0;
        //            int currentBatchID = -1;
        //            int qty = 0;

        //            //Part Conditions sorted by RuleBatchID ASC

        //            //Part Conditions sorted by RuleBatchID ASC
        //            ruleHeadID = Part["RuleHeadID"].ToString();
        //            partNumStr = Part["PartNum"].ToString();

        //            dtPartCondition = objMain.GetPartConditionsByRuleHeadID(Convert.ToInt32(ruleHeadID), true);

        //            foreach (DataRow PartConditionrow in dtPartCondition.Rows)
        //            {
        //                int ruledigit = 0;
        //                string ruledigitStr = "";

        //                qtyInt = equations(PartConditionrow["EquationValue"].ToString(), compRLAStr, compQtyStr, comp2QtyStr, digCompRLAStr, digCompQtyStr, mcaStr, modelNo);

        //                if (qty > 0)
        //                {
        //                    if (Part["PartCategory"].ToString() == "Motor")
        //                    {
        //                        dtMotor = objMain.GetMotorData(Int32.Parse(ruleHeadID));

        //                        if (dtMotor.Rows.Count > 0)
        //                        {
        //                            DataRow dr = dtMotor.Rows[0];
        //                            motorTypeStr = dr["MotorType"].ToString();
        //                        }
        //                        else
        //                        {
        //                            motorTypeStr = "";
        //                        }
        //                    }

        //                    dtBOM = AddPartToBOMDataTable(dtBOM, Part["PartNum"].ToString(), ruleHeadID, qtyInt.ToString(), "desc", Part["RelatedOperation"].ToString(), revisionNumStr,
        //                                                  Part["PartCategory"].ToString(), "Active", seqNumInt.ToString(), motorTypeStr, jobNum, "0", "");

        //                    seqNumInt = seqNumInt + 10;
        //                }
        //            }
        //        }
        //    }

        //    //Get fuse, circuitbreaker, and fusiable based on digit = 0 and remaining digits = modelnumber
        //    DataTable dtPartConditionsZero = objMain.GetPartRulesByDigit(0);

        //    foreach (DataRow PartConditionsZeroRow in dtPartConditionsZero.Rows)
        //    {
        //        int rulecount = 0;
        //        int truecount = 0;
        //        string qty = "";

        //        ruleHeadID = PartConditionsZeroRow["RuleHeadID"].ToString();
        //        ruleBatchID = PartConditionsZeroRow["RuleBatchID"].ToString();
        //        partNumStr = PartConditionsZeroRow["PartNum"].ToString();
        //        partCat = PartConditionsZeroRow["PartCategory"].ToString();
        //        partDescStr = PartConditionsZeroRow["Description"].ToString();

        //        if (partNumStr.Contains("VELCON-0323"))
        //        {
        //            string taco = "bell";
        //        }


        //        dtPartCondition = objMain.GetPartRulesByRuleHeadIdAndRuleBatchId(Int32.Parse(ruleHeadID), Int32.Parse(ruleBatchID));

        //        foreach (DataRow PartConditionrow in dtPartCondition.Rows)
        //        {
        //            int ruledigit = 0;
        //            int ruledigitStr = 0;

        //            qty = PartConditionrow["Qty"].ToString();
        //            motorTypeStr = "";

        //            if (PartConditionrow["Digit"].ToString().Contains("567"))
        //            {
        //                ruledigit = 5;
        //                ruledigitStr = 3;
        //            }
        //            else if (PartConditionrow["Digit"].ToString().Contains("2526"))
        //            {
        //                ruledigit = 25;
        //                ruledigitStr = 2;
        //            }
        //            else if (PartConditionrow["Digit"].ToString() == "0")
        //            {
        //                ruledigit = 0;
        //                ruledigitStr = 1;
        //            }
        //            else
        //            {
        //                ruledigit = Convert.ToInt16(PartConditionrow["Digit"].ToString());
        //                ruledigitStr = 1;
        //            }

        //            if (ruledigit == 0)
        //            {
        //                if (PartConditionrow["Unit"].ToString().Trim() == unitTypeStr && PartConditionrow["Value"].ToString() == mopStr)
        //                {
        //                    ++truecount;
        //                }
        //            }
        //            else if (PartConditionrow["Unit"].ToString().Trim() == unitTypeStr && PartConditionrow["Value"].ToString().Contains((modelNo.Substring((ruledigit - 1), ruledigitStr))))
        //            {
        //                ++truecount;
        //            }
        //            ++rulecount;
        //        }

        //        if (rulecount > 0)
        //        {
        //            if (rulecount == truecount)
        //            {
        //                //method add part to list                        
        //                if (partCat == "Motor")
        //                {
        //                    dtMotor = objMain.GetMotorData(Int32.Parse(ruleHeadID));

        //                    if (dtMotor.Rows.Count > 0)
        //                    {
        //                        DataRow dr = dtMotor.Rows[0];
        //                        motorTypeStr = dr["MotorType"].ToString();
        //                    }

        //                }

        //                dtBOM = AddPartToBOMDataTable(dtBOM, partNumStr, ruleHeadID, qty, partDescStr, PartConditionsZeroRow["RelatedOperation"].ToString(), revisionNumStr,
        //                                              partCat, "Active", seqNumInt.ToString(), motorTypeStr, jobNum, "0", "");
                        
        //                seqNumInt = seqNumInt + 10;
        //                truecount = 0;
        //                rulecount = 0;
        //            }
        //        }
        //    }

        //    if (!(modelNo.Substring(4, 3).Contains("000")))
        //    {
        //        decimal chargeTotal = 0;
                
        //        if (modelNo.Substring(10, 1).ToString() != "F")  // if Digit 11 is equal to  'F' then No refrigerant is required for this type of unit.
        //        {
        //            string digit3 = modelNo.Substring(2, 1);
        //            string digit4 = modelNo.Substring(3, 1);
        //            string digit567 = modelNo.Substring(4, 3);
        //            string digit9 = modelNo.Substring(8, 1);
        //            string digit11 = modelNo.Substring(10, 1);
        //            string digit12 = modelNo.Substring(11, 1);
        //            string digit13 = modelNo.Substring(12, 1);
        //            string digit14 = modelNo.Substring(13, 1);

        //            //if (digit4 != "G")
        //            //{
        //            //    digit9 = "0";
        //            //    digit13 = "0";
        //            //}

        //            DataTable dtCCD = objMain.GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
        //            if (dtCCD.Rows.Count > 0)
        //            {
        //                DataRow ChargeRow = dtCCD.Rows[0];

        //                if ((modelNo.Substring(11, 1).ToString() == "0"))
        //                {
        //                    chargeTotal = Convert.ToDecimal(ChargeRow["Circuit1_Charge"].ToString()) + Convert.ToDecimal(ChargeRow["Circuit2_Charge"].ToString());
        //                }
        //                else
        //                {
        //                    chargeTotal = Convert.ToDecimal(ChargeRow["Circuit1_Charge"].ToString()) + Convert.ToDecimal(ChargeRow["Circuit2_Charge"].ToString()) + Convert.ToDecimal(ChargeRow["Circuit1_ReheatCharge"].ToString());
        //                }
        //            }
        //            else
        //            {
        //                MessageBox.Show("Error Finding Refigeration Charge date for " + modelNo.Substring(0, 3).ToString() + " - " + modelNo.Substring(4, 3).ToString());
        //                MessageBox.Show("Please fix quantity for VCPRFG-400");
        //            }

        //            DataTable dtChargePart = objMain.GetPartRulesHead("VCPRFG-0400");
        //            if (dtChargePart.Rows.Count > 0)
        //            {
        //                DataRow drChargePart = dtChargePart.Rows[0];
        //                ruleHeadID = drChargePart["ID"].ToString();
        //                partCat = drChargePart["PartCategory"].ToString();
        //                relatedOpStr = drChargePart["RelatedOperation"].ToString();
        //                partDescStr = drChargePart["PartDescription"].ToString();
        //            }

        //            dtBOM = AddPartToBOMDataTable(dtBOM, "VCPRFG-0400", ruleHeadID, chargeTotal.ToString(), "Tank, 100lb, R-410a", "60",
        //                                          "", "Refrigeration", "Active", seqNumInt.ToString(), motorTypeStr, jobNum, "0", "");
        //        }
        //    }

        //    //Keep warranty as is for now.
        //    dtBOM = AddPartToBOMDataTable(dtBOM, "OAWARRANTYSTD", "0", "1.00", "Warranty Part Entry", "10", revisionNumStr, "Warranty", "Active", "9998", motorTypeStr, jobNum, "0", "");

        //    dtBOM = AddPartToBOMDataTable(dtBOM, "OAWARRANTYCOMP", "0", "1.00", "Warranty Part Entry", "10", revisionNumStr, "Warranty", "Active", "9999", motorTypeStr, jobNum, "0", "");

        //    //GlobalLastSeqNo.LastSeqNoInt = seqNumInt;

        //    //if (this.labelBOMCreationDate.Text != "")
        //    //{
        //    //    frmBOM.buttonBOMPrint.Enabled = true;
        //    //}

        //    return dtBOM;
        //}

        private void CreateBOM_New()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "0";
            string circuit2Charge = "0";
            string partNumStr = "";
            string revisionNumStr = "";

            string errorMsgStr = "";

            bool existingJob = false;            

            frmDisplayBOM frmBOM = new frmDisplayBOM();

            frmBOM.lbVoltage.Text = cbVoltage.Text;

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "REV5");           

            if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            {
                dtBOM = objMain.GetPartListByModelNo(modelNoStr, "REV5");

                frmBOM.btnAddPart.Enabled = false;
                string parentPartNum = "";
                string curParentPartNum = "";
                string curAsmSeq = "";
                string asmQty = "0";
                string partQty = "";
                int seqNo = 0;
                int asmSeq = 1;
                int asmQtyInt = 0;
                decimal partQtyDec = 0;

                foreach (DataRow dr in dtBOM.Rows)
                {
                    parentPartNum = dr["ParentPartNum"].ToString();
                    partNumStr = dr["PartNum"].ToString();
                    revisionNumStr = dr["RevisionNum"].ToString();

                    if (parentPartNum.Contains("RFGASM"))
                    {
                        if (parentPartNum != curParentPartNum)
                        {
                            curParentPartNum = parentPartNum;
                            asmQty = parentPartNum.Substring((parentPartNum.Length - 2), 1);
                            asmQtyInt = int.Parse(asmQty);
                            curAsmSeq = dr["AssemblySeq"].ToString();
                            seqNo = 10;
                            ++asmSeq;
                        }

                        partQty = dr["ReqQty"].ToString();
                        partQtyDec = decimal.Parse(partQty);

                        dr["ReqQty"] = (partQtyDec * asmQtyInt).ToString();

                        if (asmSeq > 2)
                        {
                            dr["AssemblySeq"] = asmSeq.ToString();
                            dr["SeqNo"] = seqNo;
                            seqNo += 10;
                        }
                    }
                    else if (parentPartNum.Length > 0 && !parentPartNum.StartsWith("OACON"))
                    {
                        if (parentPartNum != curParentPartNum)
                        {
                            curParentPartNum = parentPartNum;
                            asmQtyInt = 1;
                            curAsmSeq = dr["AssemblySeq"].ToString();
                            seqNo = 10;
                            ++asmSeq;
                        }

                        partQty = dr["ReqQty"].ToString();
                        partQtyDec = decimal.Parse(partQty);

                        dr["ReqQty"] = (partQtyDec * asmQtyInt).ToString();
                        dr["AssemblySeq"] = asmSeq.ToString();
                        dr["SeqNo"] = seqNo;
                        seqNo += 10;
                    }                   
                    
                }
                if (dtBOM.Rows.Count > 0)
                {
                    dtBOM = addPanelAssemblyBOM(dtBOM);
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV5", jobNumStr);
                    dtBOM = addRefrigAndBreakerAndWarranty(dtBOM, mcaStr, mopStr, out circuit1Charge, out circuit2Charge);                    
                }
            }
            else
            {
                //mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV5");
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = true;
                existingJob = true;
                DataTable dtETL = objMain.GetEtlData(jobNumStr);
                if (dtETL.Rows.Count > 0)
                {
                    DataRow drETL = dtETL.Rows[0];
                    circuit1Charge = drETL["Circuit1Charge"].ToString();
                    circuit2Charge = drETL["Circuit2Charge"].ToString();
                    mcaStr = drETL["MinCKTAmp"].ToString();
                    mopStr = drETL["MFSMCB"].ToString();
                }
                else
                {
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV5", jobNumStr);
                }
            }            
            
            if (jobNumStr == "")
            {
                frmBOM.btnSave.Enabled = false;
            }

            //if (dtBOM.Rows.Count > 0)
            //{
            //    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6");               
            //}

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();
            frmBOM.dgvBOM.DataSource = dtBOM;
            frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            frmBOM.dgvBOM.Columns["Description"].Width = 300;
            frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            frmBOM.dgvBOM.Columns["Status"].Width = 75;
            frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
            frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            frmBOM.dgvBOM.Columns["BuyToOrder"].Visible = false;
            frmBOM.dgvBOM.Columns["RunOut"].Visible = false;
            frmBOM.dgvBOM.Columns["SubAssemblyPart"].Visible = false;
            frmBOM.dgvBOM.Columns["SubAsmMtlPart"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;
            frmBOM.dgvBOM.Columns["StationLoc"].Visible = false;
            frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;

            frmBOM.Text = "Outdoor Air Bill of Materials";
            frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            frmBOM.lbShipDate.Text = lbShipDate.Text;
            frmBOM.lbJobNum.Text = jobNumStr;
            frmBOM.lbEnvironment.Text = cbCoolingCapacity.Text;
            frmBOM.lbCustName.Text = lbCustName.Text;
            frmBOM.lbModelNo.Text = modelNoStr;
            frmBOM.lbEnteringTemp.Text = txtEnteringTemp.Text;
            frmBOM.lbFlowRate.Text = txtFlowRate.Text;
            frmBOM.lbMCA.Text = mcaStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbUnitType.Text = lbUnitType.Text;
            frmBOM.lbCir1Chrg.Text = circuit1Charge + " lbs";
            frmBOM.lbCir2Chrg.Text = circuit2Charge + " lbs";

            //if (modelNoStr.Substring(19, 1) == "J" || modelNoStr.Substring(19, 1) == "P" || 
            //    modelNoStr.Substring(19, 1) == "Q" || modelNoStr.Substring(19, 1) == "Y")
            //{
            //    if (existingJob == true)
            //    {
            //        // Read EtlOAU table to get flow numbers
            //    }

            //    if (this.txtFlowRate.Text == "")
            //    {
            //        errorMsgStr = "ERROR - Hot Water Flow Rate is required!\n";
            //    }
            //    if (this.txtEnteringTemp.Text == "")
            //    {
            //        errorMsgStr += "ERROR - Hot Water Entering Temp is required!";
            //    }
            //}

            if (errorMsgStr.Length > 0)
            {
                MessageBox.Show(errorMsgStr);
            }
            else
            {
                frmBOM.ShowDialog();
            }
        }

        private void CreateAllPartsBOM()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "0";
            string circuit2Charge = "0";
            string partNumStr = "";
            string revisionNumStr = "";

            string errorMsgStr = "";

            bool existingJob = false;

            frmDisplayBOM frmBOM = new frmDisplayBOM();

            frmBOM.lbVoltage.Text = cbVoltage.Text;

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "REV5");

            if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            {
                dtBOM = objMain.GetPartListByModelNo(modelNoStr, "REV5");

                frmBOM.btnAddPart.Enabled = false;
                string parentPartNum = "";
                string curParentPartNum = "";
                string curAsmSeq = "";
                string asmQty = "0";
                string partQty = "";
                int seqNo = 0;
                int asmSeqInt = 1;
                int asmQtyInt = 0;
                decimal partQtyDec = 0;

                
                if (dtBOM.Rows.Count > 0)
                {
                    //dtBOM = objMain.ProcessSubAssemblies(dtBOM);
                    //dtBOM = objMain.AddInnerSubAssemblyParts(dtBOM, asmSeqInt, out asmSeqInt);
                    //dtBOM = objMain.AddPanelAssemblyBOM(dtBOM, asmSeqInt);
                    //dtBOM = objMain.SortBOM_SetupSubAssemblys(dtBOM);
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV5", jobNumStr);
                    dtBOM = addRefrigAndBreakerAndWarranty(dtBOM, mcaStr, mopStr, out circuit1Charge, out  circuit2Charge);                  
                }
            }
            else
            {
                mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV5", jobNumStr);
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = true;
                frmBOM.btnSave.Enabled = false;
                existingJob = true;
                DataTable dtETL = objMain.GetEtlData(jobNumStr);
                if (dtETL.Rows.Count > 0)
                {
                    DataRow drETL = dtETL.Rows[0];
                    circuit1Charge = drETL["Circuit1Charge"].ToString();
                    circuit2Charge = drETL["Circuit2Charge"].ToString();
                    mcaStr = drETL["MinCKTAmp"].ToString();
                    mopStr = drETL["MFSMCB"].ToString();
                }
                else
                {
                    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV5", jobNumStr);
                }
            }

            if (jobNumStr == "")
            {
                frmBOM.btnSave.Enabled = false;
            }

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();
            frmBOM.dgvBOM.DataSource = dtBOM;
            frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            frmBOM.dgvBOM.Columns["Description"].Width = 300;
            frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            frmBOM.dgvBOM.Columns["Status"].Width = 75;
            frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
            frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            frmBOM.dgvBOM.Columns["BuyToOrder"].Visible = false;
            frmBOM.dgvBOM.Columns["SubAssemblyPart"].Visible = false;
            frmBOM.dgvBOM.Columns["SubAsmMtlPart"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;
            frmBOM.dgvBOM.Columns["StationLoc"].Visible = false;          
            frmBOM.dgvBOM.Columns["CostMethod"].Visible = false;
            frmBOM.dgvBOM.Columns["NonStock"].Visible = false;
            frmBOM.dgvBOM.Columns["Runout"].Visible = false;
            frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;         

            frmBOM.Text = "Outdoor Air Bill of Materials";
            frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            frmBOM.lbShipDate.Text = lbShipDate.Text;
            frmBOM.lbJobNum.Text = jobNumStr;
            frmBOM.lbEnvironment.Text = cbCoolingCapacity.Text;
            frmBOM.lbCustName.Text = lbCustName.Text;
            frmBOM.lbModelNo.Text = modelNoStr;
            frmBOM.lbEnteringTemp.Text = txtEnteringTemp.Text;
            frmBOM.lbFlowRate.Text = txtFlowRate.Text;
            frmBOM.lbMCA.Text = mcaStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbUnitType.Text = lbUnitType.Text;
            frmBOM.lbCir1Chrg.Text = circuit1Charge + " lbs";
            frmBOM.lbCir2Chrg.Text = circuit2Charge + " lbs";

            //if (modelNoStr.Substring(19, 1) == "J" || modelNoStr.Substring(19, 1) == "P" || 
            //    modelNoStr.Substring(19, 1) == "Q" || modelNoStr.Substring(19, 1) == "Y")
            //{
            //    if (existingJob == true)
            //    {
            //        // Read EtlOAU table to get flow numbers
            //    }

            //    if (this.txtFlowRate.Text == "")
            //    {
            //        errorMsgStr = "ERROR - Hot Water Flow Rate is required!\n";
            //    }
            //    if (this.txtEnteringTemp.Text == "")
            //    {
            //        errorMsgStr += "ERROR - Hot Water Entering Temp is required!";
            //    }
            //}

            if (errorMsgStr.Length > 0)
            {
                MessageBox.Show(errorMsgStr);
            }
            else
            {
                frmBOM.ShowDialog();
            }
        }

        private void deleteAllPartsOnTheBOM()
        {
            string errorMsg = "";
            string jobNumStr = lbJobNum.Text;
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string epicorServer = "";
            string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;

            if (GlobalJob.DisplayMode == "Production")
            {
                epicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            }
            else if (GlobalJob.DisplayMode == "Debug")
            {
                epicorServer = ConfigurationManager.AppSettings["DevEpicorAppServer"];
            }
            else
            {
                epicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            }

            Console.WriteLine(epicorServer);

            DeleteBOM db = new DeleteBOM(epicorServer, epicorSqlConnectionString);

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "REV5"); // All units the same, therefore only need a single stored proc, not one for each unit type.            

            if (dtBOM.Rows.Count > 0)
            {
                DialogResult result1 = MessageBox.Show("Are you sure you want to Delete the BOM for Job# " + jobNumStr +
                                                       " Press 'Yes' to delete & 'No' to cancel!",
                                                       "WARNING WARNING WARNING",
                                                       MessageBoxButtons.YesNo,
                                                       MessageBoxIcon.Exclamation);
                if (result1 == DialogResult.Yes)
                {
                    try
                    {
                        objMain.CopyJobMtlToHistory(jobNumStr, modByStr);
                    }
                    catch (Exception f)
                    {
                        MessageBox.Show("Error copying job# " + jobNumStr + " from jobmtl Table to JobMtlHistory Table - " + f);
                    }

                    this.lbBOMCreationDate.Text = "";

                    try
                    {
                        errorMsg = db.DeleteBOM_FromEpicor(jobNumStr);

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                objMain.DeleteOAU_BOMsData(jobNumStr, modByStr);
                            }
                            catch (Exception f)
                            {
                                MessageBox.Show("Error deleting job# " + jobNumStr + " from OAU_BOMs Table - " + f);
                            }

                            try
                            {
                                objMain.DeleteEtlOAUData(jobNumStr);
                            }
                            catch (Exception f)
                            {
                                MessageBox.Show("Error deleting job# " + jobNumStr + " from R6_OA_EtlOAU Table - " + f);
                            }

                            MessageBox.Show("You have successfully deleted the BOM for Job# " + jobNumStr + " from the database");
                        }
                        else
                        {
                            MessageBox.Show(errorMsg);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR: Something went awry while deleting Job# " + jobNumStr + " from the database");
                    }
                }
            }
            else
            {
                MessageBox.Show("ERROR: BOM does Not existfor this JobNum " + jobNumStr);
            }
        }

        private DataTable addPanelAssemblyBOM(DataTable dtBOM)
        {
            string generationStr = "";
            string partNumStr = "";
            string revisionNumStr = "";
            string ruleHeadIdStr = "";
            string ruleBatchIdStr = "";
            string mtlPartNumStr = "";
            string curParentPartNum = "";
            string asmUOMStr = "";
            string asmQtyStr = "";

            List<string> PartNumList = new List<string>();
            List<string> RevNumList = new List<string>();
            List<string> RuleHeadList = new List<string>();
            List<string> RuleBatchList = new List<string>();
            List<DataRow> RowsToRemove = new List<DataRow>();

            int generationInt = 0;
            int curGenerationInt = 0;
            int seqNumInt = 10;
            int asmSeqInt = 5;

            bool firstAsm = true;

            decimal partQtyDec;
            decimal asmQtyDec = 0;

            var results = dtBOM.AsEnumerable().Where(dr => dr.Field<string>("PartNum").Contains("PNLASM") == true);

            foreach (DataRow row in results)
            {
                RowsToRemove.Add(row);
            }

            foreach (DataRow drtr in RowsToRemove)
            {
                partNumStr = drtr["PartNum"].ToString();
                revisionNumStr = drtr["RevisionNum"].ToString();
                ruleHeadIdStr = drtr["RuleHeadID"].ToString();
                ruleBatchIdStr = drtr["RuleBatchID"].ToString();
                curParentPartNum = partNumStr;

                DataTable dtPnl = objMain.GetPanelAsmBOM(partNumStr, revisionNumStr, 1);

                if (dtPnl.Rows.Count > 0)
                {
                    asmSeqInt = getAsmSeq(dtBOM, asmSeqInt);

                    foreach (DataRow drPnl in dtPnl.Rows)
                    {
                        mtlPartNumStr = drPnl["MtlPartNum"].ToString();
                        generationStr = drPnl["Generation"].ToString();
                        generationInt = Int32.Parse(generationStr);

                        if (curGenerationInt != generationInt)
                        {
                            curGenerationInt = generationInt;

                            seqNumInt = 10;
                            if (firstAsm)
                            {
                                firstAsm = false;
                                asmQtyDec = 1;                                
                            }
                            else
                            {
                                DataTable dtAsm = objMain.GetPanelAsmBOM(partNumStr, revisionNumStr, 0);
                                foreach(DataRow row in dtAsm.Rows)
                                {
                                    if (Int32.Parse(row["Generation"].ToString()) == (curGenerationInt-1))
                                    {
                                        asmQtyStr = row["QtyPer"].ToString();
                                        asmQtyDec = decimal.Parse(asmQtyStr);
                                        asmUOMStr = row["UOM"].ToString();
                                        break;
                                    }
                                }
                                ++asmSeqInt;
                            }
                        }

                        var dr = dtBOM.NewRow();
                        dr["PartCategory"] = "Panel Assembly";
                        dr["AssemblySeq"] = asmSeqInt;
                        dr["SeqNo"] = seqNumInt;
                        seqNumInt += 10;
                        dr["PartNum"] = drPnl["MtlPartNum"].ToString();
                        dr["Description"] = drPnl["PartDescription"].ToString();
                        partQtyDec = decimal.Parse(drPnl["QtyPer"].ToString());
                        dr["QtyPer"] = partQtyDec;

                        if (drPnl["QtyPer"] == null)
                        {
                            partQtyDec = 0;
                        }
                        else
                        {                            
                            partQtyDec = partQtyDec * asmQtyDec;
                        }

                        dr["ReqQty"] = partQtyDec;
                        dr["UOM"] = drPnl["UOM"].ToString();
                        dr["UnitCost"] = decimal.Parse(drPnl["UnitCost"].ToString());
                        dr["WhseCode"] = "LWH5";
                        dr["RelOp"] = 10;
                        dr["PartStatus"] = "Active";
                        dr["RevisionNum"] = "";
                        dr["CostMethod"] = "A";
                        dr["PartType"] = "";
                        dr["Status"] = "BackFlush";
                        dr["PROGRESS_RECID"] = 0;
                        dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                        dr["RuleBatchID"] = Int32.Parse(ruleBatchIdStr);
                        dr["ParentPartNum"] = drPnl["ParentPartNum"].ToString();
                        dr["Generation"] = drPnl["Generation"].ToString();

                        dtBOM.Rows.Add(dr);
                    }
                }
            }

            foreach (DataRow row in RowsToRemove)
            {
                dtBOM.Rows.Remove(row);
            }

            return dtBOM;
        }       

        private int getAsmSeq(DataTable dtBOM, int asmSeqInt)
        {
            string asmSeqStr = "";

            int retAsmSeqInt = 0;

            foreach (DataRow row in dtBOM.Rows)
            {
                if (row["AssemblySeq"].ToString() != asmSeqStr)
                {
                    asmSeqStr = row["AssemblySeq"].ToString();
                }
            }
            if (Int32.Parse(asmSeqStr) == asmSeqInt)
            {
                retAsmSeqInt = asmSeqInt;
            }
            else
            {
                retAsmSeqInt = asmSeqInt + 1;
            }

            return retAsmSeqInt;
        }

        private DataTable addRefrigAndBreakerAndWarranty(DataTable dtBOM, string mcaStr, string mopStr, out string circuit1ChargeRetVal, out string circuit2ChargeRetVal)
        {
            string modelNoStr = lbModelNo.Text;
            string unitPriceStr = "";
            string ruleHeadIdStr = "";
            string digit3 = modelNoStr.Substring(2, 1);
            string digit4 = modelNoStr.Substring(3, 1);
            string digit567 = modelNoStr.Substring(4, 3);
            string digit9 = modelNoStr.Substring(8, 1);
            string digit11 = modelNoStr.Substring(10, 1);
            string digit12 = modelNoStr.Substring(11, 1);
            string digit13 = modelNoStr.Substring(12, 1);
            string digit14 = modelNoStr.Substring(13, 1);
            string circuit1ChargeStr = "0";
            string circuit2ChargeStr = "0";
            string unitType = modelNoStr.Substring(0, 3);
            string stationLoc = "";

            int seqNumInt = 0;
            int relOp = 0;

            DataTable dtCCD = objMain.GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow drComp = dtCCD.Rows[0];

                if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = drComp["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(drComp["Circuit1_Charge"].ToString()) + Decimal.Parse(drComp["Circuit1_ReheatCharge"].ToString())).ToString();
                }

                if (drComp["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = drComp["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(drComp["Circuit2_Charge"].ToString()) + Decimal.Parse(drComp["Circuit2_ReheatCharge"].ToString())).ToString();
                    }
                }
            }

            circuit1ChargeRetVal = circuit1ChargeStr;
            circuit2ChargeRetVal = circuit2ChargeStr;

            DataTable dtPart = objMain.GetPartDetail("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                unitPriceStr = drPart["UnitCost"].ToString();
            }

            dtPart = objMain.GetPartRulesHead("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                ruleHeadIdStr = drPart["ID"].ToString();
            }

            var dr = dtBOM.NewRow();
            dr["PartCategory"] = "Refrigeration";
            dr["AssemblySeq"] = 0;
            seqNumInt = getNextSeqNum("0", dtBOM);
            dr["SeqNo"] = seqNumInt;
            dr["PartNum"] = "VCPRFG-0400";
            dr["Description"] = "Tank, 100lb, R-410a";

            string partQtyStr = (decimal.Parse(circuit1ChargeStr) + decimal.Parse(circuit2ChargeStr)).ToString();

            decimal partQtyDec = 0;

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
                if (partQtyDec == 0)
                {
                    partQtyDec = 1;
                }
            }

            //dr["QtyPer"] = partQtyDec;
            dr["ReqQty"] = partQtyDec;
            dr["UOM"] = "LB";
            dr["UnitCost"] = decimal.Parse(unitPriceStr);
            dr["WhseCode"] = "LWH5";
            dr["RelOp"] = 60;
            stationLoc = objMain.findStationLoc(60, unitType, "LT");
            dr["PartStatus"] = "Active";
            dr["RevisionNum"] = "";
            dr["CostMethod"] = "A";
            dr["PartType"] = "";
            dr["BuyToOrder"] = (byte)0;
            dr["Runout"] = (byte)0;
            dr["NonStock"] = (byte)0;
            dr["Status"] = "BackFlush";
            dr["PROGRESS_RECID"] = 0;
            dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr["RuleBatchID"] = 0;
            dr["ParentPartNum"] = "";
            dr["StationLoc"] = stationLoc;
            dr["ConfigurablePart"] = "No";
            dr["SubAssemblyPart"] = 0;
            dr["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr);

            if (Int32.Parse(mopStr) < 100)
            {
                string tempMopStr = "0" + mopStr;
                mopStr = tempMopStr;
            }

            if (unitType == "VKG")
            {
                dtPart = objMain.GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(40, 1), unitType);
            }
            else if (unitType == "REV6")
            {
                dtPart = objMain.GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(40, 1), unitType);
            }
            else
            {
                dtPart = objMain.GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(36, 1), unitType);
            }

            if (dtPart.Rows.Count > 0)
            {
                var dr1 = dtBOM.NewRow();

                DataRow drPart = dtPart.Rows[0];

                dr1["PartCategory"] = drPart["PartCategory"].ToString();
                dr1["AssemblySeq"] = 0;
                dr1["SeqNo"] = seqNumInt + 10;
                dr1["PartNum"] = drPart["PartNum"].ToString();
                dr1["Description"] = drPart["PartDescription"].ToString();

                partQtyStr = drPart["ReqQty"].ToString();

                if (partQtyStr == "")
                {
                    partQtyDec = 0;
                }
                else
                {
                    partQtyDec = decimal.Parse(partQtyStr);
                }

                //dr1["QtyPer"] = partQtyDec;
                dr1["ReqQty"] = partQtyDec;
                dr1["UOM"] = drPart["UOM"].ToString();
                unitPriceStr = drPart["UnitPrice"].ToString();
                dr1["UnitCost"] = decimal.Parse(unitPriceStr);
                dr1["WhseCode"] = "LWH5";
                relOp = Int32.Parse(drPart["RelOp"].ToString());
                dr1["RelOp"] = relOp;
                stationLoc = objMain.findStationLoc(relOp, unitType, "LT");
                dr1["PartStatus"] = "Active";
                dr1["RevisionNum"] = drPart["RevisionNum"].ToString();
                dr1["CostMethod"] = drPart["CostMethod"].ToString();
                dr1["PartType"] = drPart["PartType"].ToString();
                dr1["BuyToOrder"] = (byte)1;
                dr1["NonStock"] = (byte)0;
                dr1["Runout"] = (byte)0;
                dr1["Status"] = "BackFlush";
                dr1["PROGRESS_RECID"] = 0;
                ruleHeadIdStr = drPart["RuleHeadId"].ToString();
                dr1["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                dr1["RuleBatchID"] = 0;
                dr1["ParentPartNum"] = "";
                dr1["StationLoc"] = stationLoc;
                dr1["ConfigurablePart"] = "No";
                dr1["SubAssemblyPart"] = 0;
                dr1["SubAsmMtlPart"] = 0;

                dtBOM.Rows.Add(dr1);
            }

            var dr2 = dtBOM.NewRow();

            dr2["PartCategory"] = "Warranty";
            dr2["AssemblySeq"] = 0;
            seqNumInt = 9998;
            dr2["SeqNo"] = seqNumInt.ToString();
            dr2["PartNum"] = "OAWARRANTYSTD";
            dr2["Description"] = "Warranty Part Entry";

            partQtyStr = "1.00";

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            //dr2["QtyPer"] = partQtyDec;
            dr2["ReqQty"] = partQtyDec;
            dr2["UOM"] = "";
            //unitPriceStr = drPart["UnitPrice"].ToString();
            //dr2["UnitCost"] = decimal.Parse(unitPriceStr);
            dr2["UnitCost"] = 0;
            dr2["WhseCode"] = "LWH5";
            dr2["RelOp"] = 100;
            stationLoc = objMain.findStationLoc(100, unitType, "LT");
            dr2["PartStatus"] = "Active";
            dr2["RevisionNum"] = "";
            dr2["CostMethod"] = "A";
            dr2["PartType"] = "Warranty";
            dr2["BuyToOrder"] = (byte)0;
            dr2["NonStock"] = (byte)1;
            dr2["Runout"] = (byte)0;
            dr2["Status"] = "BackFlush";
            dr2["PROGRESS_RECID"] = 0;
            ruleHeadIdStr = "0";
            dr2["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr2["RuleBatchID"] = 0;
            dr2["ParentPartNum"] = "";
            dr2["StationLoc"] = stationLoc;
            dr2["ConfigurablePart"] = "No";
            dr2["SubAssemblyPart"] = 0;
            dr2["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr2);

            var dr3 = dtBOM.NewRow();

            dr3["PartCategory"] = "Warranty";
            dr3["AssemblySeq"] = 0;
            seqNumInt = 9999;
            dr3["SeqNo"] = seqNumInt.ToString();
            dr3["PartNum"] = "OAWARRANTYCOMP";
            dr3["Description"] = "Warranty Part Entry";

            partQtyStr = "1.00";

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            //dr3["QtyPer"] = partQtyDec;
            dr3["ReqQty"] = partQtyDec;
            dr3["UOM"] = "";
            //unitPriceStr = drPart["UnitPrice"].ToString();
            //dr3["UnitCost"] = decimal.Parse(unitPriceStr);
            dr3["UnitCost"] = 0;
            dr3["WhseCode"] = "LWH5";
            dr3["RelOp"] = 100;
            dr3["PartStatus"] = "Active";
            dr3["RevisionNum"] = "";
            dr3["CostMethod"] = "A";
            dr3["PartType"] = "Warranty";
            dr3["BuyToOrder"] = (byte)0;
            dr3["NonStock"] = (byte)1;
            dr3["Runout"] = (byte)0;
            dr3["Status"] = "BackFlush";
            dr3["PROGRESS_RECID"] = 0;
            ruleHeadIdStr = "0";
            dr3["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr3["RuleBatchID"] = 0;
            dr3["ParentPartNum"] = "";
            dr3["StationLoc"] = stationLoc;
            dr3["ConfigurablePart"] = "No";
            dr3["SubAssemblyPart"] = 0;
            dr3["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr3);

            return dtBOM;
        }

        private int getNextSeqNum(string assemblySeqStr, DataTable dtBOM)
        {
            int retSeqNumInt = 0;
            string seqNumStr = "";

            foreach (DataRow dr in dtBOM.Rows)
            {
                if (dr["AssemblySeq"].ToString() == assemblySeqStr)
                {
                    seqNumStr = dr["SeqNo"].ToString();
                }
            }

            if (seqNumStr != "")
            {
                retSeqNumInt = Int32.Parse(seqNumStr) + 10;
            }

            return retSeqNumInt;

        }

        private void PrintModelNoConfiguration()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string heatType = "";
            string secHeatType = "";
            string oauTypeCode = "";

            switch (modelNoStr.Substring(19, 1))
            {
                case "A":
                    heatType = "IF";
                    secHeatType = "NA";
                    break;
                case "B":
                    heatType = "DF";
                    secHeatType = "NA";
                    break;
                case "C":
                case "D":
                    heatType = "ELEC";
                    secHeatType = "NA";
                    break;
                case "E":
                    heatType = "IF";
                    secHeatType = "DF";
                    break;
                case "F":
                    heatType = "ELEC";
                    secHeatType = "DF";
                    break;
                case "G":
                    heatType = "IF";
                    secHeatType = "ELEC";
                    break;
                case "H":
                case "N":
                case "U":
                case "W":
                    heatType = "ELEC";
                    secHeatType = "ELEC";
                    break;
                case "J":
                    heatType = "HOTWATER";
                    secHeatType = "NA";
                    break;
                case "K":
                    heatType = "STEAM";
                    secHeatType = "NA";
                    break;
                case "M":
                    heatType = "ELEC";
                    secHeatType = "DF";
                    break;
                case "P":
                    heatType = "HOTWATER";
                    secHeatType = "DF";
                    break;
                case "Q":
                case "Y":
                    heatType = "HOTWATER";
                    secHeatType = "ELEC";
                    break;
                case "R":
                    heatType = "STEAM";
                    secHeatType = "DF";
                    break;
                case "S":
                case "Z":
                    heatType = "STEAM";
                    secHeatType = "ELEC";
                    break;
                case "T":
                    heatType = "IF";
                    secHeatType = "ELEC";
                    break;
                case "V":
                    heatType = "NA";
                    secHeatType = "ELEC";
                    break;
                default:
                    heatType = "NA";
                    secHeatType = "NA";
                    break;
            }

            if (modelNoStr.Substring(2, 1) == "B" || modelNoStr.Substring(2, 1) == "G")
            {
                oauTypeCode = "OALBG";
            }
            else
            {
                oauTypeCode = "OAU123DKN";
            }

            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@ModelNo";
            pdv2.Value = modelNoStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@HeatType";
            pdv3.Value = heatType;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@SecHeatType";
            pdv4.Value = secHeatType;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@OAUTypeCode";
            pdv5.Value = oauTypeCode;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\OA_OldModelNoConfiguration.rpt";             
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\OA_OldModelNoConfiguration.rpt";
#endif

            cryRpt.Load(objMain.ReportString);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();

        }


        #endregion

        private void txtModelNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbEnterModelNo_Click(object sender, EventArgs e)
        {

        }

        private void lbLastUpdateBy_Click(object sender, EventArgs e)
        {

        }
                       
    }
}
