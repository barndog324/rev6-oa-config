﻿namespace OA_Config_Rev6
{
    partial class frmCriticalCompBOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCriticalCompVoltage = new System.Windows.Forms.Label();
            this.lbCritCompJobNum = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCritCompCancel = new System.Windows.Forms.Button();
            this.btnCritCompPrint = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCritCompJobName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbCritCompModelNo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.richTxtCritCompNotes = new System.Windows.Forms.RichTextBox();
            this.lbCir1Charge = new System.Windows.Forms.Label();
            this.lbCir2Charge = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbCriticalCompVoltage
            // 
            this.lbCriticalCompVoltage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCriticalCompVoltage.ForeColor = System.Drawing.Color.Black;
            this.lbCriticalCompVoltage.Location = new System.Drawing.Point(54, 147);
            this.lbCriticalCompVoltage.Name = "lbCriticalCompVoltage";
            this.lbCriticalCompVoltage.Size = new System.Drawing.Size(57, 15);
            this.lbCriticalCompVoltage.TabIndex = 52;
            this.lbCriticalCompVoltage.Text = "??? ";
            this.lbCriticalCompVoltage.Visible = false;
            // 
            // lbCritCompJobNum
            // 
            this.lbCritCompJobNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCritCompJobNum.ForeColor = System.Drawing.Color.Teal;
            this.lbCritCompJobNum.Location = new System.Drawing.Point(118, 26);
            this.lbCritCompJobNum.Name = "lbCritCompJobNum";
            this.lbCritCompJobNum.Size = new System.Drawing.Size(206, 23);
            this.lbCritCompJobNum.TabIndex = 51;
            this.lbCritCompJobNum.Text = "Job Number";
            this.lbCritCompJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(12, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 50;
            this.label5.Text = "Job#:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCritCompCancel
            // 
            this.btnCritCompCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCritCompCancel.ForeColor = System.Drawing.Color.Red;
            this.btnCritCompCancel.Location = new System.Drawing.Point(349, 214);
            this.btnCritCompCancel.Name = "btnCritCompCancel";
            this.btnCritCompCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCritCompCancel.TabIndex = 49;
            this.btnCritCompCancel.Text = "Cancel";
            this.btnCritCompCancel.UseVisualStyleBackColor = true;
            this.btnCritCompCancel.Click += new System.EventHandler(this.btnCritCompCancel_Click);
            // 
            // btnCritCompPrint
            // 
            this.btnCritCompPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCritCompPrint.ForeColor = System.Drawing.Color.Blue;
            this.btnCritCompPrint.Location = new System.Drawing.Point(253, 214);
            this.btnCritCompPrint.Name = "btnCritCompPrint";
            this.btnCritCompPrint.Size = new System.Drawing.Size(75, 23);
            this.btnCritCompPrint.TabIndex = 48;
            this.btnCritCompPrint.Text = "Print";
            this.btnCritCompPrint.UseVisualStyleBackColor = true;
            this.btnCritCompPrint.Click += new System.EventHandler(this.btnCritCompPrint_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(11, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 47;
            this.label3.Text = "Notes:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCritCompJobName
            // 
            this.txtCritCompJobName.ForeColor = System.Drawing.Color.Teal;
            this.txtCritCompJobName.Location = new System.Drawing.Point(121, 76);
            this.txtCritCompJobName.Name = "txtCritCompJobName";
            this.txtCritCompJobName.Size = new System.Drawing.Size(445, 20);
            this.txtCritCompJobName.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(11, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 45;
            this.label2.Text = "Job Name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbCritCompModelNo
            // 
            this.lbCritCompModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCritCompModelNo.ForeColor = System.Drawing.Color.Teal;
            this.lbCritCompModelNo.Location = new System.Drawing.Point(118, 51);
            this.lbCritCompModelNo.Name = "lbCritCompModelNo";
            this.lbCritCompModelNo.Size = new System.Drawing.Size(448, 23);
            this.lbCritCompModelNo.TabIndex = 44;
            this.lbCritCompModelNo.Text = "Model Number";
            this.lbCritCompModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 43;
            this.label1.Text = "Model No:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // richTxtCritCompNotes
            // 
            this.richTxtCritCompNotes.ForeColor = System.Drawing.Color.Teal;
            this.richTxtCritCompNotes.Location = new System.Drawing.Point(121, 102);
            this.richTxtCritCompNotes.Name = "richTxtCritCompNotes";
            this.richTxtCritCompNotes.Size = new System.Drawing.Size(445, 96);
            this.richTxtCritCompNotes.TabIndex = 53;
            this.richTxtCritCompNotes.Text = "";
            // 
            // lbCir1Charge
            // 
            this.lbCir1Charge.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCir1Charge.ForeColor = System.Drawing.Color.Black;
            this.lbCir1Charge.Location = new System.Drawing.Point(41, 214);
            this.lbCir1Charge.Name = "lbCir1Charge";
            this.lbCir1Charge.Size = new System.Drawing.Size(57, 15);
            this.lbCir1Charge.TabIndex = 54;
            this.lbCir1Charge.Text = "Circuit1";
            this.lbCir1Charge.Visible = false;
            // 
            // lbCir2Charge
            // 
            this.lbCir2Charge.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCir2Charge.ForeColor = System.Drawing.Color.Black;
            this.lbCir2Charge.Location = new System.Drawing.Point(41, 233);
            this.lbCir2Charge.Name = "lbCir2Charge";
            this.lbCir2Charge.Size = new System.Drawing.Size(65, 20);
            this.lbCir2Charge.TabIndex = 271;
            this.lbCir2Charge.Text = "Circuit2:";
            this.lbCir2Charge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbCir2Charge.Visible = false;
            // 
            // frmCriticalCompBOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 262);
            this.Controls.Add(this.lbCir2Charge);
            this.Controls.Add(this.lbCir1Charge);
            this.Controls.Add(this.richTxtCritCompNotes);
            this.Controls.Add(this.lbCriticalCompVoltage);
            this.Controls.Add(this.lbCritCompJobNum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCritCompCancel);
            this.Controls.Add(this.btnCritCompPrint);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCritCompJobName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbCritCompModelNo);
            this.Controls.Add(this.label1);
            this.Name = "frmCriticalCompBOM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Critical Component BOM";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbCriticalCompVoltage;
        public System.Windows.Forms.Label lbCritCompJobNum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCritCompCancel;
        private System.Windows.Forms.Button btnCritCompPrint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lbCritCompModelNo;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtCritCompJobName;
        public System.Windows.Forms.RichTextBox richTxtCritCompNotes;
        public System.Windows.Forms.Label lbCir1Charge;
        public System.Windows.Forms.Label lbCir2Charge;
    }
}