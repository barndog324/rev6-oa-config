﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmMSP_ModelNoConfig : Form
    {
        MainBO objMain = new MainBO();
        public frmMSP_ModelNoConfig()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCreateBOM_Click(object sender, EventArgs e)
        {
            CreateBOM();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printConfigurationReport();
        }

        private void btnParseModelNo_Click(object sender, EventArgs e)
        {
            string digitValStr = "";
            string heatingTypeStr = "NA";
            string modelNoStr = "";
            string unitTypeStr = "MSP";

            modelNoStr = txtModelNo.Text;           

            //labelConfigModelNo.Text = modelNoStr;    

            lbJobNum.Text = "";
            lbOrderDate.Text = "";
            lbShipDate.Text = "";
            lbBOMCreationDate.Text = "";
            lbLastUpdateDate.Text = "";
            lbProdStartDate.Text = "";
            lbCompleteDate.Text = "";

            lbModelNo.Text = modelNoStr;
            lbCustName.Text = "";

            if (modelNoStr.Length == 69)
            {
                // Digit 1 - UnitType

                digitValStr = modelNoStr.Substring(0, 1);
                DataTable dt = objMain.MSP_GetModelNoValues(1, "NA");

                cbUnitType.DataSource = dt;
                cbUnitType.DisplayMember = "DigitDescription";
                cbUnitType.ValueMember = "DigitVal";
                cbUnitType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 2 - CabinetDesign
                digitValStr = modelNoStr.Substring(1, 1);

                dt = objMain.MSP_GetModelNoValues(2, "NA");
                cbCabinetDesign.DataSource = dt;
                cbCabinetDesign.DisplayMember = "DigitDescription";
                cbCabinetDesign.ValueMember = "DigitVal";
                cbCabinetDesign.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 3456 - Model Size               
                digitValStr = modelNoStr.Substring(2, 4);

                dt = objMain.MSP_GetModelNoValues(3456, "NA");
                cbModelSize.DataSource = dt;
                cbModelSize.DisplayMember = "DigitDescription";
                cbModelSize.ValueMember = "DigitVal";
                cbModelSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 7 - Voltage               
                digitValStr = modelNoStr.Substring(6, 1);

                dt = objMain.MSP_GetModelNoValues(7, "NA");
                cbVoltage.DataSource = dt;
                cbVoltage.DisplayMember = "DigitDescription";
                cbVoltage.ValueMember = "DigitVal";
                cbVoltage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 8 - MajorDesignSeq
                digitValStr = modelNoStr.Substring(7, 1);

                dt = objMain.MSP_GetModelNoValues(8, "NA");
                cbMajorDesignSeq.DataSource = dt;
                cbMajorDesignSeq.DisplayMember = "DigitDescription";
                cbMajorDesignSeq.ValueMember = "DigitVal";
                cbMajorDesignSeq.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 11 - CoilType
                digitValStr = modelNoStr.Substring(10, 1);

                dt = objMain.MSP_GetModelNoValues(11, "NA");
                cbCoilType.DataSource = dt;
                cbCoilType.DisplayMember = "DigitDescription";
                cbCoilType.ValueMember = "DigitVal";
                cbCoilType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 12 - CoilSize
                digitValStr = modelNoStr.Substring(11, 1);

                dt = objMain.MSP_GetModelNoValues(12, "NA");
                cbCoilSize.DataSource = dt;
                cbCoilSize.DisplayMember = "DigitDescription";
                cbCoilSize.ValueMember = "DigitVal";
                cbCoilSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 13 - MSP_CoolingCoilFluidType
                digitValStr = modelNoStr.Substring(12, 1);

                dt = objMain.MSP_GetModelNoValues(13, "NA");
                cbMSP_CoolingCoilFluidType.DataSource = dt;
                cbMSP_CoolingCoilFluidType.DisplayMember = "DigitDescription";
                cbMSP_CoolingCoilFluidType.ValueMember = "DigitVal";
                cbMSP_CoolingCoilFluidType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 14 - PostCoolingCoilType
                digitValStr = modelNoStr.Substring(13, 1);

                dt = objMain.MSP_GetModelNoValues(14, "NA");
                cbPostCoolingCoilType.DataSource = dt;
                cbPostCoolingCoilType.DisplayMember = "DigitDescription";
                cbPostCoolingCoilType.ValueMember = "DigitVal";
                cbPostCoolingCoilType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 15 - PostCoolingCoilSize
                digitValStr = modelNoStr.Substring(14, 1);

                dt = objMain.MSP_GetModelNoValues(15, "NA");
                cbPostCoolingCoilSize.DataSource = dt;
                cbPostCoolingCoilSize.DisplayMember = "DigitDescription";
                cbPostCoolingCoilSize.ValueMember = "DigitVal";
                cbPostCoolingCoilSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 16 - PostHeatingCoilType
                digitValStr = modelNoStr.Substring(15, 1);

                dt = objMain.MSP_GetModelNoValues(16, "NA");
                cbPostHeatingCoilType.DataSource = dt;
                cbPostHeatingCoilType.DisplayMember = "DigitDescription";
                cbPostHeatingCoilType.ValueMember = "DigitVal";
                cbPostHeatingCoilType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 17 - PostHeatingCoilSize
                digitValStr = modelNoStr.Substring(16, 1);

                dt = objMain.MSP_GetModelNoValues(17, heatingTypeStr);
                cbPostHeatingCoilSize.DataSource = dt;
                cbPostHeatingCoilSize.DisplayMember = "DigitDescription";
                cbPostHeatingCoilSize.ValueMember = "DigitVal";
                cbPostHeatingCoilSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 18 HeatingCoilFluidType
                digitValStr = modelNoStr.Substring(17, 1);

                dt = objMain.MSP_GetModelNoValues(18, "NA");
                cbHeatingCoilFluidType.DataSource = dt;
                cbHeatingCoilFluidType.DisplayMember = "DigitDescription";
                cbHeatingCoilFluidType.ValueMember = "DigitVal";
                cbHeatingCoilFluidType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 19 - HeatExchangerType           
                digitValStr = modelNoStr.Substring(18, 1);

                dt = objMain.MSP_GetModelNoValues(19, heatingTypeStr);
                cbHeatExchangerType.DataSource = dt;
                cbHeatExchangerType.DisplayMember = "DigitDescription";
                cbHeatExchangerType.ValueMember = "DigitVal";
                cbHeatExchangerType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                 // Digit 21 - CabinetConstuctIndoor
                digitValStr = modelNoStr.Substring(20, 1);

                dt = objMain.MSP_GetModelNoValues(21, "NA");
                cbCabinetConstuctIndoor.DataSource = dt;
                cbCabinetConstuctIndoor.DisplayMember = "DigitDescription";
                cbCabinetConstuctIndoor.ValueMember = "DigitVal";
                cbCabinetConstuctIndoor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 22 - CabinetConstructOutdoor
                digitValStr = modelNoStr.Substring(21, 1);

                dt = objMain.MSP_GetModelNoValues(22, "NA");
                cbCabinetConstructOutdoor.DataSource = dt;
                cbCabinetConstructOutdoor.DisplayMember = "DigitDescription";
                cbCabinetConstructOutdoor.ValueMember = "DigitVal";
                cbCabinetConstructOutdoor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 23 - CoilCorrisionPackage
                digitValStr = modelNoStr.Substring(22, 1);

                dt = objMain.MSP_GetModelNoValues(23, "NA");
                cbCoilCorrisionPackage.DataSource = dt;
                cbCoilCorrisionPackage.DisplayMember = "DigitDescription";
                cbCoilCorrisionPackage.ValueMember = "DigitVal";
                cbCoilCorrisionPackage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 24 - SupplyFanWheel
                digitValStr = modelNoStr.Substring(23, 1);

                dt = objMain.MSP_GetModelNoValues(24, "NA");
                cbSupplyFanWheel.DataSource = dt;
                cbSupplyFanWheel.DisplayMember = "DigitDescription";
                cbSupplyFanWheel.ValueMember = "DigitVal";
                cbSupplyFanWheel.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 25 - SupplyFanMotorType
                digitValStr = modelNoStr.Substring(24, 1);

                dt = objMain.MSP_GetModelNoValues(25, "NA");
                cbSupplyFanMotorType.DataSource = dt;
                cbSupplyFanMotorType.DisplayMember = "DigitDescription";
                cbSupplyFanMotorType.ValueMember = "DigitVal";
                cbSupplyFanMotorType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 26 - SupplyFanMotorSize
                digitValStr = modelNoStr.Substring(25, 1);

                dt = objMain.MSP_GetModelNoValues(26, "NA");
                cbSupplyFanMotorSize.DataSource = dt;
                cbSupplyFanMotorSize.DisplayMember = "DigitDescription";
                cbSupplyFanMotorSize.ValueMember = "DigitVal";
                cbSupplyFanMotorSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 27 - FilterOptions
                digitValStr = modelNoStr.Substring(26, 1);

                dt = objMain.MSP_GetModelNoValues(27, "NA");
                cbFilterOptions.DataSource = dt;
                cbFilterOptions.DisplayMember = "DigitDescription";
                cbFilterOptions.ValueMember = "DigitVal";
                cbFilterOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 28 - BypassDamper
                digitValStr = modelNoStr.Substring(27, 1);

                dt = objMain.MSP_GetModelNoValues(28, "NA");
                cbBypassDamper.DataSource = dt;
                cbBypassDamper.DisplayMember = "DigitDescription";
                cbBypassDamper.ValueMember = "DigitVal";
                cbBypassDamper.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 29 - Altitude
                digitValStr = modelNoStr.Substring(28, 1);

                dt = objMain.MSP_GetModelNoValues(29, "NA");
                cbAltitude.DataSource = dt;
                cbAltitude.DisplayMember = "DigitDescription";
                cbAltitude.ValueMember = "DigitVal";
                cbAltitude.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 31 - AirflowMonitoring
                digitValStr = modelNoStr.Substring(30, 1);

                dt = objMain.MSP_GetModelNoValues(31, "NA");
                cbAirflowMonitoring.DataSource = dt;
                cbAirflowMonitoring.DisplayMember = "DigitDescription";
                cbAirflowMonitoring.ValueMember = "DigitVal";
                cbAirflowMonitoring.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 32 - Accessories
                digitValStr = modelNoStr.Substring(31, 1);

                dt = objMain.MSP_GetModelNoValues(32, "NA");
                cbAccessories.DataSource = dt;
                cbAccessories.DisplayMember = "DigitDescription";
                cbAccessories.ValueMember = "DigitVal";
                cbAccessories.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 33 - SmokeDetector
                digitValStr = modelNoStr.Substring(32, 1);

                dt = objMain.MSP_GetModelNoValues(33, "NA");
                cbSmokeDetector.DataSource = dt;
                cbSmokeDetector.DisplayMember = "DigitDescription";
                cbSmokeDetector.ValueMember = "DigitVal";
                cbSmokeDetector.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 34 - CondensateOverflowSwitch
                digitValStr = modelNoStr.Substring(33, 1);

                dt = objMain.MSP_GetModelNoValues(34, "NA");
                cbCondensateOverflowSwitch.DataSource = dt;
                cbCondensateOverflowSwitch.DisplayMember = "DigitDescription";
                cbCondensateOverflowSwitch.ValueMember = "DigitVal";
                cbCondensateOverflowSwitch.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 35 Controls
                digitValStr = modelNoStr.Substring(34, 1);

                dt = objMain.MSP_GetModelNoValues(35, "NA");
                cbControls.DataSource = dt;
                cbControls.DisplayMember = "DigitDescription";
                cbControls.ValueMember = "DigitVal";
                cbControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 36 - ControlsDisplay
                digitValStr = modelNoStr.Substring(35, 1);

                dt = objMain.MSP_GetModelNoValues(36, "NA");
                cbControlsDisplay.DataSource = dt;
                cbControlsDisplay.DisplayMember = "DigitDescription";
                cbControlsDisplay.ValueMember = "DigitVal";
                cbControlsDisplay.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 37 - ElectricalOptions 
                digitValStr = modelNoStr.Substring(36, 1);

                dt = objMain.MSP_GetModelNoValues(37, "NA");
                cbElectricalOptions.DataSource = dt;
                cbElectricalOptions.DisplayMember = "DigitDescription";
                cbElectricalOptions.ValueMember = "DigitVal";
                cbElectricalOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 38 - ConvenienceOutlet
                digitValStr = modelNoStr.Substring(37, 1);

                dt = objMain.MSP_GetModelNoValues(38, "NA");
                cbConvenienceOutlet.DataSource = dt;
                cbConvenienceOutlet.DisplayMember = "DigitDescription";
                cbConvenienceOutlet.ValueMember = "DigitVal";
                cbConvenienceOutlet.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 39 - ServiceLights
                digitValStr = modelNoStr.Substring(38, 1);

                dt = objMain.MSP_GetModelNoValues(39, "NA");
                cbServiceLights.DataSource = dt;
                cbServiceLights.DisplayMember = "DigitDescription";
                cbServiceLights.ValueMember = "DigitVal";
                cbServiceLights.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 41 - Warranty
                digitValStr = modelNoStr.Substring(40, 1);

                dt = objMain.MSP_GetModelNoValues(41, "NA");
                cbWarranty.DataSource = dt;
                cbWarranty.DisplayMember = "DigitDescription";
                cbWarranty.ValueMember = "DigitVal";
                cbWarranty.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
                
            }
            else
            {
                modelNoStr = cbUnitType.SelectedValue.ToString();
                modelNoStr += cbCabinetDesign.SelectedValue.ToString();
                modelNoStr += cbModelSize.SelectedValue.ToString();
                modelNoStr += cbVoltage.SelectedValue.ToString();
                modelNoStr += cbMajorDesignSeq.SelectedValue.ToString();
                modelNoStr += "--";
                modelNoStr += cbCoilType.SelectedValue.ToString();
                modelNoStr += cbCoilSize.SelectedValue.ToString();
                modelNoStr += cbMSP_CoolingCoilFluidType.SelectedValue.ToString();
                modelNoStr += cbPostCoolingCoilType.SelectedValue.ToString();
                modelNoStr += cbPostCoolingCoilSize.SelectedValue.ToString();
                modelNoStr += cbPostHeatingCoilType.SelectedValue.ToString();
                modelNoStr += cbPostHeatingCoilSize.SelectedValue.ToString();
                modelNoStr += cbHeatingCoilFluidType.SelectedValue.ToString();
                modelNoStr += cbHeatExchangerType.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbCabinetConstuctIndoor.SelectedValue.ToString();
                modelNoStr += cbCabinetConstructOutdoor.SelectedValue.ToString();
                modelNoStr += cbCoilCorrisionPackage.SelectedValue.ToString();
                modelNoStr += cbSupplyFanWheel.SelectedValue.ToString();
                modelNoStr += cbSupplyFanMotorType.SelectedValue.ToString();
                modelNoStr += cbSupplyFanMotorSize.SelectedValue.ToString();
                modelNoStr += cbFilterOptions.SelectedValue.ToString();
                modelNoStr += cbBypassDamper.SelectedValue.ToString();
                modelNoStr += cbAltitude.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbAirflowMonitoring.SelectedValue.ToString();
                modelNoStr += cbAccessories.SelectedValue.ToString();
                modelNoStr += cbSmokeDetector.SelectedValue.ToString();
                modelNoStr += cbCondensateOverflowSwitch.SelectedValue.ToString();
                modelNoStr += cbControls.SelectedValue.ToString();
                modelNoStr += cbControlsDisplay.SelectedValue.ToString();
                modelNoStr += cbElectricalOptions.SelectedValue.ToString();
                modelNoStr += cbConvenienceOutlet.SelectedValue.ToString();
                modelNoStr += cbServiceLights.SelectedValue.ToString();
                modelNoStr += "-";
                modelNoStr += cbWarranty.SelectedValue.ToString();                                            
                modelNoStr += "00000000-000000000-000000000";                             

                txtModelNo.Text = modelNoStr;
                lbModelNo.Text = modelNoStr;

            }
        }

        #region Other Methods
        private void CreateBOM()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string minCKTAmpStr = "0";
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "";
            string circuit2Charge = "";

            frmDisplayBOM frmBOM = new frmDisplayBOM();

            frmBOM.Text = "MSP Bill of Materials";
            frmBOM.lbVoltage.Text = cbVoltage.Text;
            frmBOM.lbCir1Label.Visible = false;
            frmBOM.lbCir1Chrg.Visible = false;
            frmBOM.lbCir2Label.Visible = false;
            frmBOM.lbCir2Chrg.Visible = false;

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "MSP");

            if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            {
                dtBOM = objMain.MSP_GetPartListByModelNo(modelNoStr);

                frmBOM.btnAddPart.Enabled = false;
                string parentPartNum = "";
                string curParentPartNum = "";
                string curAsmSeq = "";
                int seqNo = 0;
                int asmSeq = 0;

                if (dtBOM.Rows.Count > 0)
                {
                    mcaStr = objMain.CalculateMSP_AmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr);                    
                    dtBOM = addBreakerAndWarranty(dtBOM, minCKTAmpStr, mopStr);
                }
            }
            else
            {
                DataTable dtEtl = objMain.GetMonitorEtlData(jobNumStr);
                if (dtEtl.Rows.Count > 0)
                {
                    DataRow dr = dtEtl.Rows[0];
                    minCKTAmpStr = dr["MinCKTAmp"].ToString();
                    mopStr = dr["MOP"].ToString();
                }
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = true;
            }

            if (jobNumStr == "")
            {
                frmBOM.btnSave.Enabled = false;
            }

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();
            frmBOM.dgvBOM.DataSource = dtBOM;
            frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            frmBOM.dgvBOM.Columns["Description"].Width = 300;
            frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            frmBOM.dgvBOM.Columns["Status"].Width = 75;
            frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
            frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            frmBOM.dgvBOM.Columns["QtyPer"].Visible = false;
            frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            frmBOM.dgvBOM.Columns["CostMethod"].Visible = false;
            frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleHeadID"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;
            frmBOM.dgvBOM.Columns["Generation"].Visible = false;

            frmBOM.Text = "Outdoor Air Bill of Materials";
            frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            frmBOM.lbShipDate.Text = lbShipDate.Text;
            frmBOM.lbJobNum.Text = jobNumStr;
            frmBOM.lbEnvironment.Text = cbModelSize.Text;
            frmBOM.lbCustName.Text = lbCustName.Text;
            frmBOM.lbModelNo.Text = modelNoStr;
            frmBOM.lbMCA.Text = mcaStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbUnitType.Text = lbUnitType.Text;
            frmBOM.lbCir1Chrg.Text = circuit1Charge + " lbs";
            frmBOM.lbCir2Chrg.Text = circuit2Charge + " lbs";

            frmBOM.ShowDialog();
        }

        private DataTable addBreakerAndWarranty(DataTable dtBOM, string mcaStr, string mopStr)
        {
            string modelNoStr = lbModelNo.Text;
            string unitPriceStr = "";
            string ruleHeadIdStr = "";
            string partQtyStr = "";

            int seqNumInt = 0;

            decimal partQtyDec = 0;
            
            if (Int32.Parse(mopStr) < 100)
            {
                string tempMopStr = "0" + mopStr;
                mopStr = tempMopStr;
            }
           
            DataTable dtPart = objMain.MSP_GetBreakerByModelNo(mopStr);                                  

            if (dtPart.Rows.Count > 0)
            {
                var dr1 = dtBOM.NewRow();

                DataRow drPart = dtPart.Rows[0];

                dr1["PartCategory"] = drPart["PartCategory"].ToString();
                dr1["AssemblySeq"] = 0;
                seqNumInt = getNextSeqNum("0", dtBOM);
                dr1["SeqNo"] = seqNumInt;
                dr1["PartNum"] = drPart["PartNum"].ToString();
                dr1["Description"] = drPart["PartDescription"].ToString();

                partQtyStr = drPart["ReqQty"].ToString();

                if (partQtyStr == "")
                {
                    partQtyDec = 0;
                }
                else
                {
                    partQtyDec = decimal.Parse(partQtyStr);
                }

                dr1["QtyPer"] = partQtyDec;
                dr1["ReqQty"] = partQtyDec;
                dr1["UOM"] = drPart["UOM"].ToString();
                unitPriceStr = drPart["UnitPrice"].ToString();
                dr1["UnitCost"] = decimal.Parse(unitPriceStr);
                dr1["WhseCode"] = "LWH5";
                dr1["RelOp"] = Int32.Parse(drPart["RelOp"].ToString());
                dr1["PartStatus"] = "Active";
                dr1["RevisionNum"] = drPart["RevisionNum"].ToString();
                dr1["CostMethod"] = drPart["CostMethod"].ToString();
                dr1["PartType"] = drPart["PartType"].ToString();
                dr1["Status"] = "BackFlush";
                dr1["PROGRESS_RECID"] = 0;
                ruleHeadIdStr = drPart["RuleHeadId"].ToString();
                dr1["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                dr1["ParentPartNum"] = "";
                dr1["Generation"] = 0;

                dtBOM.Rows.Add(dr1);
            }
            //var dr2 = dtBOM.NewRow();

            //dr2["PartCategory"] = "Warranty";
            //dr2["AssemblySeq"] = 0;
            //seqNumInt = 9998;
            //dr2["SeqNo"] = seqNumInt.ToString();
            //dr2["PartNum"] = "OAWARRANTYSTD";
            //dr2["Description"] = "Warranty Part Entry";

            //partQtyStr = "1.00";

            //if (partQtyStr == "")
            //{
            //    partQtyDec = 0;
            //}
            //else
            //{
            //    partQtyDec = decimal.Parse(partQtyStr);
            //}

            //dr2["QtyPer"] = partQtyDec;
            //dr2["ReqQty"] = partQtyDec;
            //dr2["UOM"] = "";
            ////unitPriceStr = drPart["UnitPrice"].ToString();
            ////dr2["UnitCost"] = decimal.Parse(unitPriceStr);
            //dr2["UnitCost"] = 0;
            //dr2["WhseCode"] = "LWH5";
            //dr2["RelOp"] = 10;
            //dr2["PartStatus"] = "Active";
            //dr2["RevisionNum"] = "";
            //dr2["CostMethod"] = "A";
            //dr2["PartType"] = "Warranty";
            //dr2["Status"] = "BackFlush";
            //dr2["PROGRESS_RECID"] = 0;
            //ruleHeadIdStr = "0";
            //dr2["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            //dr2["ParentPartNum"] = "";
            //dr2["Generation"] = 0;

            //dtBOM.Rows.Add(dr2);

            //var dr3 = dtBOM.NewRow();

            //dr3["PartCategory"] = "Warranty";
            //dr3["AssemblySeq"] = 0;
            //seqNumInt = 9999;
            //dr3["SeqNo"] = seqNumInt.ToString();
            //dr3["PartNum"] = "OAWARRANTYCOMP";
            //dr3["Description"] = "Warranty Part Entry";

            //partQtyStr = "1.00";

            //if (partQtyStr == "")
            //{
            //    partQtyDec = 0;
            //}
            //else
            //{
            //    partQtyDec = decimal.Parse(partQtyStr);
            //}

            //dr3["QtyPer"] = partQtyDec;
            //dr3["ReqQty"] = partQtyDec;
            //dr3["UOM"] = "";
            ////unitPriceStr = drPart["UnitPrice"].ToString();
            ////dr3["UnitCost"] = decimal.Parse(unitPriceStr);
            //dr3["UnitCost"] = 0;
            //dr3["WhseCode"] = "LWH5";
            //dr3["RelOp"] = 10;
            //dr3["PartStatus"] = "Active";
            //dr3["RevisionNum"] = "";
            //dr3["CostMethod"] = "A";
            //dr3["PartType"] = "Warranty";
            //dr3["Status"] = "BackFlush";
            //dr3["PROGRESS_RECID"] = 0;
            //ruleHeadIdStr = "0";
            //dr3["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            //dr3["ParentPartNum"] = "";
            //dr3["Generation"] = 0;

            //dtBOM.Rows.Add(dr3);

            return dtBOM;
        }

        private int getNextSeqNum(string assemblySeqStr, DataTable dtBOM)
        {
            int retSeqNumInt = 0;
            string seqNumStr = "";

            foreach (DataRow dr in dtBOM.Rows)
            {
                if (dr["AssemblySeq"].ToString() == assemblySeqStr)
                {
                    seqNumStr = dr["SeqNo"].ToString();
                }
            }

            if (seqNumStr != "")
            {
                retSeqNumInt = Int32.Parse(seqNumStr) + 10;
            }

            return retSeqNumInt;

        }

        private void printConfigurationReport()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string heatType = "NA";
          
            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@ModelNo";
            pdv2.Value = modelNoStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@HeatType";
            pdv3.Value = heatType;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\MSP_ModelNoConfiguration.rpt";             
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\MSP_ModelNoConfiguration.rpt";
#endif

            cryRpt.Load(objMain.ReportString);
            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }
        #endregion

        #region Drop Down Menu Events

        private void createBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateBOM();
        }

        private void deleteBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumStr = lbJobNum.Text;
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            //DataTable dt = objMain.GetOAU_BOMsData(jobNumStr);
            //DataTable dtJobMtl = objMain.GetExistingBOMPartListByJobNum(jobNumStr);

            //if (dtJobMtl.Rows.Count > 0)
            //{
            DialogResult result1 = MessageBox.Show("Are you sure you want to Delete the BOM for Job# " + jobNumStr +
                                                   " Press 'Yes' to delete & 'No' to cancel!",
                                                   "WARNING WARNING WARNING",
                                                   MessageBoxButtons.YesNo,
                                                   MessageBoxIcon.Exclamation);
            if (result1 == DialogResult.Yes)
            {
                objMain.deleteJobFromDatabase(jobNumStr, modByStr, "OAU");

                this.lbBOMCreationDate.Text = "";
                MessageBox.Show("You have successfully deleted the BOM for Job# " + jobNumStr + " from the database");
            }
            //}
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printConfigurationReport();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
