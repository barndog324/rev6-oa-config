﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Configuration;

namespace OA_Config_Rev6
{
    public partial class frmCriticalCompBOM : Form
    {
        MainBO objMain = new MainBO();

        public frmCriticalCompBOM()
        {
            InitializeComponent();
        }

        private void btnCritCompCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCritCompPrint_Click(object sender, EventArgs e)
        {
            string modelNoStr = lbCritCompModelNo.Text;

            if (modelNoStr.Length == 39)
            {
                shortModelNumCritCompBOM(modelNoStr);
            }
            else if (modelNoStr.Length == 69)
            {
                longModelNumCritCompBOM(modelNoStr);
            }
            else if (modelNoStr.Length == 43)
            {
                monitorModelNumCritCompBOM(modelNoStr);
            }
            else
            {
                MessageBox.Show("No Critical Component Report matching this ModelNo format.");
            }
        }

        private void shortModelNumCritCompBOM(string modelNoStr)
        {
            string jobNumStr = lbCritCompJobNum.Text;           
            string jobNameStr = txtCritCompJobName.Text;
            string notesStr = "";
            string orderNumStr = jobNumStr.Substring(0, jobNumStr.LastIndexOf('-'));
            string tonageStr = modelNoStr.Substring(4, 3);
            string voltageStr = lbCriticalCompVoltage.Text;
            string OAUTypeCode = "OAU123DKN";
            string digitValStr = "";
            string digitDescStr = "";
            string heatType = "NA";
            string refCir1Chrg = lbCir1Charge.Text;
            string refCir2Chrg = lbCir2Charge.Text;

            int tonageInt = Int32.Parse(tonageStr) / 12;

            tonageStr = tonageInt.ToString() + " Tons";

            if (modelNoStr.Substring(2, 1) == "B" || modelNoStr.Substring(2, 1) == "G")
            {
                OAUTypeCode = "OALBG";
            }

            digitValStr = modelNoStr.Substring(7, 1); // Digit 8
            DataTable dt = objMain.GetRev5ModelNoDigitDesc(8, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr = "8 Minor Design: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(16, 1); // Digit 17
            dt = objMain.GetRev5ModelNoDigitDesc(17, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n17 Indoor Fan Wheel: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(17, 1); // Digit 18
            dt = objMain.GetRev5ModelNoDigitDesc(18, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n18 Indoor Fan Mtr HP: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(24, 2); // Digit 25 & 26
            dt = objMain.GetRev5ModelNoDigitDesc(2526, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n26 Unit Controls: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(27, 1); // Digit 28
            dt = objMain.GetRev5ModelNoDigitDesc(28, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n28 Pwr Exh Wheel: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(28, 1); // Digit 29
            dt = objMain.GetRev5ModelNoDigitDesc(29, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n29 Pwr Exh Mtr HP: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(31, 1); // Digit 32
            dt = objMain.GetRev5ModelNoDigitDesc(32, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n32 ERV Size: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(33, 1); // Digit 34
            dt = objMain.GetRev5ModelNoDigitDesc(34, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n34 Filter Options: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(34, 1); // Digit 35
            dt = objMain.GetRev5ModelNoDigitDesc(35, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n35 Smoke Detector: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(35, 1); // Digit 36
            dt = objMain.GetRev5ModelNoDigitDesc(36, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n36 Elec Options: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(36, 1); // Digit 37
            dt = objMain.GetRev5ModelNoDigitDesc(37, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n37 Airflow Monitoring: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(37, 1); // Digit 38
            dt = objMain.GetRev5ModelNoDigitDesc(38, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n38 Hailgaurds: " + digitDescStr;
            }

            notesStr += "\nSPECIAL NOTES:\n" + richTxtCritCompNotes.Text;
            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@OrderNum";
            pdv2.Value = orderNumStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@ModelNo";
            pdv3.Value = modelNoStr;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@JobName";
            pdv4.Value = jobNameStr;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@Tonage";
            pdv5.Value = tonageStr;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@Voltage";
            pdv6.Value = voltageStr;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            ParameterField pf7 = new ParameterField();
            ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
            pf7.Name = "@Notes";
            pdv7.Value = notesStr;
            pf7.CurrentValues.Add(pdv7);

            paramFields.Add(pf7);

            ParameterField pf8 = new ParameterField();
            ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
            pf8.Name = "@RefCir1Chrg";
            pdv8.Value = refCir1Chrg;
            pf8.CurrentValues.Add(pdv8);

            paramFields.Add(pf8);

            ParameterField pf9 = new ParameterField();
            ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
            pf9.Name = "@RefCir2Chrg";
            pdv9.Value = refCir2Chrg;
            pf9.CurrentValues.Add(pdv9);

            paramFields.Add(pf9);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];  
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\CriticalCompBOM.rpt";  
                //cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAUConfiguratorcrystalreports\VME_and_RFGASM_PartsMultiJob.rpt");
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\CriticalCompBOM.rpt";
#endif
            
            cryRpt.Load(objMain.ReportString);     

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }

        private void longModelNumCritCompBOM(string modelNoStr) 
        {
            string jobNumStr = lbCritCompJobNum.Text;           
            string jobNameStr = txtCritCompJobName.Text;
            string notesStr = "";
            string orderNumStr = jobNumStr.Substring(0, jobNumStr.LastIndexOf('-'));
            string tonageStr = modelNoStr.Substring(4, 3);
            string voltageStr = lbCriticalCompVoltage.Text;           
            string digitValStr = "";
            string digitDescStr = "";
            string heatType = "NA";
            string refCir1Chrg = lbCir1Charge.Text;
            string refCir2Chrg = lbCir2Charge.Text;
            string unitTypeStr = "OAU";

            //int tonageInt = Int32.Parse(tonageStr) / 12;            

            if (modelNoStr.StartsWith("HA") ==  true )
            {
                unitTypeStr = "Viking";
            }
            
            // Digit 567 Tonage
            DataTable dt = objMain.GetModelNumDesc(567, heatType, tonageStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                tonageStr = digitDescStr + " Tons";
            }
            else
            {
                tonageStr += " Tons";
            }
                       
            digitValStr = modelNoStr.Substring(7, 1); // Digit 8
            dt = objMain.GetModelNumDesc(8, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr = "8 Minor Design: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(20, 1); // Digit 21
            dt = objMain.GetModelNumDesc(21, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n21 Indoor Fan Mtr HP: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(22, 2); // Digit 2324
            dt = objMain.GetModelNumDesc(2324, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n23/24 Indoor Fan Wheel: " + digitDescStr;
            }                

            digitValStr = modelNoStr.Substring(24, 1); // Digit 25
            dt = objMain.GetModelNumDesc(25, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n25 Pwr Exh Mtr HP: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(26, 2); // Digit 2728
            dt = objMain.GetModelNumDesc(2728, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n27/28 Pwr Exh Wheel: " + digitDescStr;
            }                

            digitValStr = modelNoStr.Substring(30, 1); // Digit 31
            dt = objMain.GetModelNumDesc(31, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n31 Unit Controls: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(32, 1); // Digit 33
            dt = objMain.GetModelNumDesc(33, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n33 Filter OPtions: " + digitDescStr;
            }
            
            digitValStr = modelNoStr.Substring(35, 1); // Digit 36
            dt = objMain.GetModelNumDesc(36, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n36 ERV Size: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(40, 1); // Digit 41
            dt = objMain.GetModelNumDesc(41, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n41 Elec Options: " + digitDescStr;
            }
                           
            digitValStr = modelNoStr.Substring(42, 1); // Digit 43
            dt = objMain.GetModelNumDesc(43, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n43 Airflow Monitoring: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(45, 1); // Digit 46
            dt = objMain.GetModelNumDesc(35, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n46 Smoke Detector: " + digitDescStr;
            }                

            digitValStr = modelNoStr.Substring(46, 1); // Digit 47
            dt = objMain.GetModelNumDesc(47, heatType, digitValStr, unitTypeStr);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n47 Hailguards: " + digitDescStr;
            }           

            notesStr += "\nSPECIAL NOTES:\n" + richTxtCritCompNotes.Text;
            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@OrderNum";
            pdv2.Value = orderNumStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@ModelNo";
            pdv3.Value = modelNoStr;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@JobName";
            pdv4.Value = jobNameStr;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@Tonage";
            pdv5.Value = tonageStr;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@Voltage";
            pdv6.Value = voltageStr;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            ParameterField pf7 = new ParameterField();
            ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
            pf7.Name = "@Notes";
            pdv7.Value = notesStr;
            pf7.CurrentValues.Add(pdv7);

            paramFields.Add(pf7);

            ParameterField pf8 = new ParameterField();
            ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
            pf8.Name = "@RefCir1Chrg";
            pdv8.Value = refCir1Chrg;
            pf8.CurrentValues.Add(pdv8);

            paramFields.Add(pf8);

            ParameterField pf9 = new ParameterField();
            ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
            pf9.Name = "@RefCir2Chrg";
            pdv9.Value = refCir2Chrg;
            pf9.CurrentValues.Add(pdv9);

            paramFields.Add(pf9);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];  
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\CriticalCompBOM.rpt";  
                //cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAUConfiguratorcrystalreports\VME_and_RFGASM_PartsMultiJob.rpt");
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\CriticalCompBOM.rpt";
#endif

            cryRpt.Load(objMain.ReportString);     

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;           
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();

        }

        private void monitorModelNumCritCompBOM(string modelNoStr)
        {
            string jobNumStr = lbCritCompJobNum.Text;
            string jobNameStr = txtCritCompJobName.Text;
            string notesStr = "";
            string orderNumStr = jobNumStr.Substring(0, jobNumStr.LastIndexOf('-'));
            string tonageStr = modelNoStr.Substring(3, 3);
            string voltageStr = lbCriticalCompVoltage.Text;
            string digitValStr = modelNoStr.Substring(3,3);
            string digitDescStr = "";
            string heatType = "NA";
            string refCir1Chrg = lbCir1Charge.Text;
            string refCir2Chrg = lbCir2Charge.Text;

            int tonageInt = Int32.Parse(tonageStr) / 12;

            // Digit 456 Tonage
            DataTable dt = objMain.MonitorGetModelNoDigitDesc(456, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                tonageStr = digitDescStr;
            }
            else
            {
                tonageStr = tonageInt.ToString() + " Ton";
            }            

            if (modelNoStr.Substring(0, 1) == "T")
            {
                heatType = "ELEC";
            }
            else if (modelNoStr.Substring(0, 1) == "Y")
            {
                heatType = "GE";
            }                 

            digitValStr = modelNoStr.Substring(9, 1); // Digit 10 Heating Capacity
            dt = objMain.MonitorGetModelNoDigitDesc(10, digitValStr, heatType);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr = "10 - Heating Capacity: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(32, 1); // Digit 33 Exhaust Fan HP
            dt = objMain.MonitorGetModelNoDigitDesc(33, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n33 - Exhaust Fan HP: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(33, 1); // Digit 34 Exhaust Fan Motor Type
            dt = objMain.MonitorGetModelNoDigitDesc(34, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n34 - Exhaust Fan Motor Type: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(34, 1); // Digit 35 Exhaust Fan Size
            dt = objMain.MonitorGetModelNoDigitDesc(35, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n35 - Exhaust Fan Size: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(35, 1); // Digit 35 Exhaust Dampers
            dt = objMain.MonitorGetModelNoDigitDesc(36, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n36 - Exhaust Dampers: " + digitDescStr;
            }           

            digitValStr = modelNoStr.Substring(36, 1); // Digit 37  ERV Wheel
            dt = objMain.MonitorGetModelNoDigitDesc(37, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n37 - ERV Wheel: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(37, 1); // Digit 38  ERV Options
            dt = objMain.MonitorGetModelNoDigitDesc(38, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n38 - ERV Options: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(38, 1); // Digit 39 Supply Dampers
            dt = objMain.MonitorGetModelNoDigitDesc(39, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n39 - Supply Dampers: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(39, 1); // Digit 40 Smoke Detector/Outdoor Monitoring
            dt = objMain.MonitorGetModelNoDigitDesc(40, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n40 - Smoke Detector/Outdoor Monitoring: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(40, 1); // Digit 41  Pulley - Driver
            dt = objMain.MonitorGetModelNoDigitDesc(41, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n41 - Pulley-Driver: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(41, 1); // Digit 42  Belt
            dt = objMain.MonitorGetModelNoDigitDesc(42, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n42 - Belt: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(42, 1); // Digit 43  Pulley - Fan
            dt = objMain.MonitorGetModelNoDigitDesc(43, digitValStr, "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n43 - Pulley-Fan: " + digitDescStr;
            }

            notesStr += "\nSPECIAL NOTES:\n" + richTxtCritCompNotes.Text;
            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@OrderNum";
            pdv2.Value = orderNumStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@ModelNo";
            pdv3.Value = modelNoStr;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@JobName";
            pdv4.Value = jobNameStr;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@Tonage";
            pdv5.Value = tonageStr;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@Voltage";
            pdv6.Value = voltageStr;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            ParameterField pf7 = new ParameterField();
            ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
            pf7.Name = "@Notes";
            pdv7.Value = notesStr;
            pf7.CurrentValues.Add(pdv7);

            paramFields.Add(pf7);

            ParameterField pf8 = new ParameterField();
            ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
            pf8.Name = "@RefCir1Chrg";
            pdv8.Value = refCir1Chrg;
            pf8.CurrentValues.Add(pdv8);

            paramFields.Add(pf8);

            ParameterField pf9 = new ParameterField();
            ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
            pf9.Name = "@RefCir2Chrg";
            pdv9.Value = refCir2Chrg;
            pf9.CurrentValues.Add(pdv9);

            paramFields.Add(pf9);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];  
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\CriticalCompBOM.rpt";  
                //cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAUConfiguratorcrystalreports\VME_and_RFGASM_PartsMultiJob.rpt");
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\CriticalCompBOM.rpt";
#endif

            cryRpt.Load(objMain.ReportString);     

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();

        }

        
    }
}
