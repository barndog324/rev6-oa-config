﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmPartDemand : Form
    {
        public frmPartDemand()
        {
            InitializeComponent();
        }

        private void btnDatePickOK_Click(object sender, EventArgs e)
        {
            string partNum = txtPartNum.Text;
            string tempDate = "";
            string errors = "";

            DateTime StartDateDt = dtpStartDate.Value;
            DateTime EndDateDt = dtpEndDate.Value;

            tempDate = StartDateDt.ToShortDateString();
            StartDateDt = Convert.ToDateTime(tempDate);

            tempDate = EndDateDt.ToShortDateString();
            EndDateDt = Convert.ToDateTime(tempDate);

            if (EndDateDt < StartDateDt)
            {
                errors = "ERROR - Start Date cannot be greater than End Date!";
            }

            if (partNum.Length == 0)
            {
                errors += "ERROR - Part Number is a required field!";
            }

            if (errors.Length == 0)
            {
                frmPrintBOM frmPrint = new frmPrintBOM();

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@StartDate";
                pdv1.Value = StartDateDt;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@EndDate";
                pdv2.Value = EndDateDt;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                ParameterField pf3 = new ParameterField();
                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                pf3.Name = "@PartNum";
                pdv3.Value = partNum;
                pf3.CurrentValues.Add(pdv3);

                paramFields.Add(pf3);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG
                //cryRpt.Load(@"\\EPICOR905TEST\Apps\OAU\OAU.Configurator\crystalreports\PartDemandByDateRange.rpt");
                cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\PartDemandByDateRange.rpt");
#else
                cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\PartDemandByDateRange.rpt");
#endif
                //cryRpt.Load(@"F:\Projects\OAUConfig\PartDemandByDateRange.rpt");

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            else
            {
                MessageBox.Show(errors);
            }
        }

        private void btnDatePickExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
