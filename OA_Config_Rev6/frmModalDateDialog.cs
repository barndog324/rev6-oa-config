﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmModalDateDialog : Form
    {
        public String RunDate { get; set; }

        public frmModalDateDialog()
        {
            InitializeComponent();
            RunDate = "";
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            RunDate = dtpRunDate.Value.ToShortDateString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            RunDate = "";
        }
    }
}
