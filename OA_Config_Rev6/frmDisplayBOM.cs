﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Drawing.Imaging;
using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;
using System.IO;


namespace OA_Config_Rev6
{
    public partial class frmDisplayBOM : Form
    {
        MainBO objMain = new MainBO();
        List<string> TandemCompAsm = new List<string>();
        string WarehouseCode = "";

        List<SubAssembly> SubAsm = new List<SubAssembly>();

        public frmDisplayBOM()
        {
            InitializeComponent();                      
        }

        private void frmDisplayBOM_Load(object sender, EventArgs e)
        {
            if (lbModelNo.Text.Length == 39)
            {
                objMain.UnitDesign = "Rev5";
            }
            else if (lbModelNo.Text.Length == 40)
            {
                objMain.UnitDesign = "Monitor";
            }
            else if (lbModelNo.Text.Length == 69)
            {
                objMain.UnitDesign = "Rev6";
            }
            else
            {
                objMain.UnitDesign = "";
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void recalculateMCAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNum = lbJobNum.Text;
            string modelNo = lbModelNo.Text;
            string unitTypeStr = "REV5";

            try
            {
                if (modelNo.Length == 43)
                {
                    unitTypeStr = "MON";
                    objMain.MonitorDeleteEtlData(jobNum);
                }
                else
                {
                    if (modelNo.Length == 69)
                    {
                        unitTypeStr = "REV6";
                        if (modelNo.StartsWith("HA") == true)
                        {
                            unitTypeStr = "VKG";
                        }                                          
                    }
                    objMain.DeleteEtlOAUData(jobNum);
                }
                addETLStickerData(jobNum, unitTypeStr);
                DataTable dtETL = objMain.GetEtlData(jobNum);
                if (dtETL.Rows.Count > 0)
                {
                    DataRow drETL = dtETL.Rows[0];
                    lbMCA.Text = drETL["MinCKTAmp"].ToString();
                    lbMOP.Text = drETL["MFSMCB"].ToString();
                }
                lbMsg.Text = "Recalculation of MCA & ETL Label complete!.";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in the Recalculating the EtlOAU Table data for Job Number: " + jobNum + " - " + ex);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string unitType = lbModelNo.Text.Substring(0,3);

            if (unitType.StartsWith("OA") == true || unitType.StartsWith("HA") == true)
            {
                saveBOM_StatLoc();
            }
            else
            {
                saveBOM();
            }
                        
        }

        private void saveBOM_StatLoc()
        {
            decimal totMaterialCostDec = 0;

            int jobDelSuccessInt = 0;
            int dash1Pos = 0;
            int dash2Pos = 0;            
           
            bool saveBOMBool = true;

            string laborErrors = "";
            string oprSeq = "";
            string partNumStr = "";
            string revTypeStr = "REV5";
            string modelNoStr = this.lbModelNo.Text;
            string jobNumStr = lbJobNum.Text;
            string orderNumStr = "";
            string orderLineStr = "";
            string unitTypeStr = modelNoStr.Substring(0, 3);
            string msp_UnitTypes = "DD,DH,DU,DV";
            string lastUpdatedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string errorMsg = "";

            gbSaveBomProgress.Location = new Point(600, 200);
            gbSaveBomProgress.Visible = true;           
            this.Refresh();

            // Check each Part in the BOM to make sure it doesn't have a Zero cost.
            foreach(DataGridViewRow dr in dgvBOM.Rows)
            {
                string unitCost = dr.Cells["UnitCost"].Value.ToString();
                if (decimal.Parse(unitCost) == 0)
                {
                    if (!dr.Cells["PartNum"].Value.ToString().Contains("OAWARRANTY"))
                    {
                        // This was added to allow for zero cost in the development env but not prod.
#if RELEASE
                            createBOMBool = false;
                            MessageBox.Show("ERROR -- Part# --> " + individualPartNumStr + " - has a zero value unit cost! You must resolve this " +
                                            "before you can save this BOM");
                            break;
#endif
                    }
                }
            }

            dash1Pos = jobNumStr.IndexOf("-");
            dash2Pos = jobNumStr.LastIndexOf("-");

            if (dash1Pos > 0 && dash2Pos > 0)
            {
                orderNumStr = jobNumStr.Substring(0, dash1Pos);
                orderLineStr = jobNumStr.Substring((dash1Pos + 1), (dash2Pos - (dash1Pos + 1)));
            }
            else
            {
                orderNumStr = jobNumStr;
                orderLineStr = "0";
            }            

            DataTable dtOrder = objMain.GetOrderDetail(orderNumStr, orderLineStr);
            if (dtOrder.Rows.Count > 0)
            {
                DataRow drOrder = dtOrder.Rows[0];
                partNumStr = drOrder["PartNum"].ToString();
            }
            else
            {
                if ((modelNoStr.Length == 69)  && (msp_UnitTypes.Contains(modelNoStr.Substring(0,2))))
                {
                    partNumStr = modelNoStr.Substring(0, 6);
                }
                else
                {
                    partNumStr = modelNoStr.Substring(0, 7);
                }
            }

            if (modelNoStr.Substring(0, 2) == "OA")
            {
                if (modelNoStr.Length == 69)
                {
                    revTypeStr = "REV6";
                }
                else if (modelNoStr.Length == 39)
                {
                    revTypeStr = "REV5";
                }
            }
            else if (modelNoStr.Substring(0, 2) == "HA")
            {                
                revTypeStr = "VKG";                
            }            
            else  if ((modelNoStr.Length == 69)  && (msp_UnitTypes.Contains(modelNoStr.Substring(0,2))))
            {
                revTypeStr = "MSP";
            }
            else if (partNumStr.Substring(0, 3) == "MON")
            {
                revTypeStr = "MON";
            }

            DataTable dtJobMtl = objMain.GetExistingBOMPartListByJobNum(jobNumStr, revTypeStr);

            DataTable dtOauBom = objMain.GetOAU_BOMsData(jobNumStr);

            DataTable dtLabor = objMain.GetLaborDetailDataByJobNum(jobNumStr);
           
            if (dtJobMtl.Rows.Count == 0)
            {
                saveBOMBool = true;

                if (dtOauBom.Rows.Count == 0)
                {
                    objMain.InsertOAU_BOMsData(jobNumStr, lastUpdatedByStr);   // Insert entry for Job into the R6_OA_BOMs
                }
                else
                {
                    objMain.UpdateOAU_BOMsData(jobNumStr, lastUpdatedByStr);
                }

                lbMsg.Text = "Writing Job# " + jobNumStr + " to KCC.dbo.OAU_BOMs table!";
                this.Refresh();
            }
            else
            {
                saveBOMBool = false;                
            }

            if ((saveBOMBool == true) && (jobDelSuccessInt == 0))
            {

                lbMsg.Text = "Writing Job# " + jobNumStr + " to epicor905.dbo.JobMtl table!";
                this.Refresh();                

                int troubleCount = 0;                

                if (troubleCount == 10)
                {
                    MessageBox.Show("WARNING - Database Issue, database is running extremely slow, contact IT!");
                }
                else
                {
                    //getSubAssemblies();
                    //setupStationSubAssembliesInEpicor();
                  
                   errorMsg = saveBomDataIntoEpicor(jobNumStr, revTypeStr);

                    //if (revTypeStr == "VKG")
                    //{
                    //    totMaterialCostDec = writeVikingBomDataIntoJobMtl(jobNumStr, revTypeStr);
                    //}
                    //else
                    //{
                    //    //totMaterialCostDec = writeVikingBomDataIntoJobMtl(jobNumStr, revTypeStr);
                    //    //totMaterialCostDec = writeBomDataIntoJobMtl(jobNumStr, revTypeStr); // NOTE: This stored proc also updates the PartQty & PartWhse tables demand fields 
                    //                                                                        // so that Epicors demandQty is kept correct in the system.
                    //}

                    //lbMsg.Text = "Writing Job# " + jobNumStr + " to epicor905.dbo.JobOpDtl & JobOper tables!";
                    //this.Refresh();                   
                  
                    //partNumStr = writeJobOperationsNew(jobNumStr, partNumStr, revTypeStr);
                               
                    //lbMsg.Text = "Updating Job# " + jobNumStr + " in epicor905.dbo.JobAsmbl table!";
                    //this.Refresh();
                    //updateJobAsmblNew(jobNumStr, lbModelNo.Text, totMaterialCostDec);

                   if (errorMsg.Length == 0)
                   {
                       lbMsg.Text = "Adding Job# " + jobNumStr + " to KCC.dbo.R6_OA_EtlOAU table!";
                       this.Refresh();
                       addETLStickerData(jobNumStr, revTypeStr);

                       ////validateJobOperations(jobNumStr, partNumStr, revTypeStr);

                       lbMsg.Text = "Writing Job# " + jobNumStr + " Completed.";
                       btnAddPart.Enabled = true;
                       btnSave.Enabled = false;
                   }                    
                }
            }           
        }

        private void saveBOM()
        {
            decimal totMaterialCostDec = 0;

            int jobDelSuccessInt = 0;
            int dash1Pos = 0;
            int dash2Pos = 0;

            bool saveBOMBool = true;

            string laborErrors = "";
            string oprSeq = "";
            string partNumStr = "";
            string revTypeStr = "REV5";
            string modelNoStr = this.lbModelNo.Text;
            string jobNumStr = lbJobNum.Text;
            string orderNumStr = "";
            string orderLineStr = "";
            string unitTypeStr = modelNoStr.Substring(0, 3);
            string msp_UnitTypes = "DD,DH,DU,DV";
            string lastUpdatedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            // Check each Part in the BOM to make sure it doesn't have a Zero cost.
            foreach (DataGridViewRow dr in dgvBOM.Rows)
            {
                string unitCost = dr.Cells["UnitCost"].Value.ToString();
                if (decimal.Parse(unitCost) == 0)
                {
                    if (!dr.Cells["PartNum"].Value.ToString().Contains("OAWARRANTY"))
                    {
                        // This was added to allow for zero cost in the development env but not prod.
#if RELEASE
                            createBOMBool = false;
                            MessageBox.Show("ERROR -- Part# --> " + individualPartNumStr + " - has a zero value unit cost! You must resolve this " +
                                            "before you can save this BOM");
                            break;
#endif
                    }
                }
            }

            dash1Pos = jobNumStr.IndexOf("-");
            dash2Pos = jobNumStr.LastIndexOf("-");

            if (dash1Pos > 0 && dash2Pos > 0)
            {
                orderNumStr = jobNumStr.Substring(0, dash1Pos);
                orderLineStr = jobNumStr.Substring((dash1Pos + 1), (dash2Pos - (dash1Pos + 1)));
            }
            else
            {
                orderNumStr = jobNumStr;
                orderLineStr = "0";
            }

            DataTable dtOrder = objMain.GetOrderDetail(orderNumStr, orderLineStr);
            if (dtOrder.Rows.Count > 0)
            {
                DataRow drOrder = dtOrder.Rows[0];
                partNumStr = drOrder["PartNum"].ToString();
            }
            else
            {
                if ((modelNoStr.Length == 69) && (msp_UnitTypes.Contains(modelNoStr.Substring(0, 2))))
                {
                    partNumStr = modelNoStr.Substring(0, 6);
                }
                else
                {
                    partNumStr = modelNoStr.Substring(0, 7);
                }
            }

            if (modelNoStr.Substring(0, 2) == "OA")
            {
                if (modelNoStr.Length == 69)
                {
                    revTypeStr = "REV6";
                }
                else if (modelNoStr.Length == 39)
                {
                    revTypeStr = "REV5";
                }
            }
            else if (modelNoStr.Substring(0, 2) == "HA")
            {
                revTypeStr = "VKG";
            }
            else if ((modelNoStr.Length == 69) && (msp_UnitTypes.Contains(modelNoStr.Substring(0, 2))))
            {
                revTypeStr = "MSP";
            }
            else if (partNumStr.Substring(0, 3) == "MON")
            {
                revTypeStr = "MON";
            }

            DataTable dtJobMtl = objMain.GetExistingBOMPartListByJobNum(jobNumStr, revTypeStr);

            DataTable dtOauBom = objMain.GetOAU_BOMsData(jobNumStr);

            DataTable dtLabor = objMain.GetLaborDetailDataByJobNum(jobNumStr);

            if (dtJobMtl.Rows.Count == 0)
            {
                saveBOMBool = true;

                if (dtOauBom.Rows.Count == 0)
                {
                    objMain.InsertOAU_BOMsData(jobNumStr, lastUpdatedByStr);   // Insert entry for Job into the R6_OA_BOMs
                }
                else
                {
                    objMain.UpdateOAU_BOMsData(jobNumStr, lastUpdatedByStr);
                }

                lbMsg.Text = "Writing Job# " + jobNumStr + " to KCC.dbo.OAU_BOMs table!";
                this.Refresh();
            }
            else
            {
                saveBOMBool = false;

                DialogResult result1 = MessageBox.Show("A BOM for Job# " + jobNumStr +
                                                       " already exist, do you wish to overwrite it???",
                                                       "WARNING WARNING WARNING",
                                                       MessageBoxButtons.YesNo,
                                                       MessageBoxIcon.Exclamation);
                if (result1 == DialogResult.Yes)
                {
                    if (dtLabor.Rows.Count > 0)
                    {
                        laborErrors = "WARNING -- The following operations have labor clocked against them:\n";
                        foreach (DataRow drRow in dtLabor.Rows)
                        {
                            oprSeq = drRow["OprSeq"].ToString();
                            laborErrors += "OprSeq = " + oprSeq + "\n";
                        }
                        saveBOMBool = false;
                        MessageBox.Show(laborErrors);
                    }
                    else
                    {
                        saveBOMBool = true;
                        lbMsg.Text = "Deleting Job# " + jobNumStr + " from Database!";
                        jobDelSuccessInt = objMain.deleteJobFromDatabase(jobNumStr, lastUpdatedByStr, revTypeStr);

                        if (jobDelSuccessInt == 0)
                        {
                            objMain.UpdateOAU_BOMsData(jobNumStr, lastUpdatedByStr);
                        }
                    }
                }
            }

            if ((saveBOMBool == true) && (jobDelSuccessInt == 0))
            {

                lbMsg.Text = "Writing Job# " + jobNumStr + " to epicor905.dbo.JobMtl table!";
                this.Refresh();

                int troubleCount = 0;

                if (troubleCount == 10)
                {
                    MessageBox.Show("WARNING - Database Issue, database is running extremely slow, contact IT!");
                }
                else
                {
                    if (revTypeStr == "VKG")
                    {
                        totMaterialCostDec = writeVikingBomDataIntoJobMtl(jobNumStr, revTypeStr);
                    }
                    else
                    {
                        //totMaterialCostDec = writeVikingBomDataIntoJobMtl(jobNumStr, revTypeStr);
                        totMaterialCostDec = writeBomDataIntoJobMtl(jobNumStr, revTypeStr); // NOTE: This stored proc also updates the PartQty & PartWhse tables demand fields 
                        // so that Epicors demandQty is kept correct in the system.
                    }

                    lbMsg.Text = "Writing Job# " + jobNumStr + " to epicor905.dbo.JobOpDtl & JobOper tables!";
                    this.Refresh();

                    //partNumStr = lbModelNo.Text.Substring(0, 7);

                    partNumStr = writeJobOperationsNew(jobNumStr, partNumStr, revTypeStr);
                    //writeJobOperations(jobNumStr, partNumStr);

                    //if (jobOperationsExist(jobNumStr) == false)
                    //{
                    //    writeJobOperations(jobNumStr, lbModelNo.Text);
                    //}                   

                    lbMsg.Text = "Updating Job# " + jobNumStr + " in epicor905.dbo.JobAsmbl table!";
                    this.Refresh();
                    updateJobAsmblNew(jobNumStr, lbModelNo.Text, totMaterialCostDec);


                    lbMsg.Text = "Add Job# " + jobNumStr + " to KCC.dbo.EtlOAU table!";
                    this.Refresh();
                    addETLStickerData(jobNumStr, revTypeStr);

                    validateJobOperations(jobNumStr, partNumStr, revTypeStr);
                    //string errors = validateJobOperationFormat(jobNumStr, partNumStr);
                    //if (errors.Length > 0)
                    //{
                    //    MessageBox.Show("ERROR\n" + errors);
                    //}
                    //else
                    //{
                    //    lbMsg.Text = "Writing Job# " + jobNumStr + " Completed.";
                    btnAddPart.Enabled = true;
                    btnSave.Enabled = false;
                    //}
                }
            }
        }

        private bool jobOperationsExist(string jobNumStr)
        {
            bool operationsFound = false;            

            DataTable dt = objMain.GetOA_JobOperations(jobNumStr);

            if (dt.Rows.Count > 0)
            {
                operationsFound = true;
            }

            return operationsFound;
        }

        private decimal writeBomDataIntoJobMtl(string jobNumStr, string unitTypeStr)
        {
            string categoryStr = "";
            string parentPartNumStr = "";            
            string assemblySeqStr = "0";
            string generationStr = "";
            string asmPartNum = "";
            string revNumStr = "";
            string qtyPerStr = "";
            string weightUOMStr = "";
            string subAsmPart = "";
            string immedParentPartNum = "";           

            int parentInt = 0;
            int priorPeerInt = -1;
            int nextPeerInt = -1;
            int childInt = -1;
            int primaryAsmSeqInt = 1;
            int asmSeqInt = 0;
            int generationInt = 0;
            int prevPriorPeer = 0;
            int pnlSubCnt = 1;
            int bomSeq = 0;
            int bomLevel = 0;            
            int refReqDes = 0;
            int eqPos = -1;
            int parLen = 0;
            
            decimal totalMaterialCostDec = 0;
           
            bool insertPartBool;
            bool configurableSubAsm = false;

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum");
            dt.Columns.Add("AssemblySeqNum");
            dt.Columns.Add("SeqNum");
            dt.Columns.Add("PartNum");
            dt.Columns.Add("PartDescription");
            dt.Columns.Add("QtyPerDec");
            dt.Columns.Add("PartQtyDec");
            dt.Columns.Add("PartQtyInt");
            dt.Columns.Add("IUM");
            dt.Columns.Add("UnitOfCost");
            dt.Columns.Add("TotalCost");
            dt.Columns.Add("PartAssembly");
            dt.Columns.Add("PartType");
            dt.Columns.Add("WarehouseCode");
            dt.Columns.Add("RelatedOperation");
            dt.Columns.Add("Status");
            dt.Columns.Add("RevisionNumber");
            dt.Columns.Add("CostMethod");
            dt.Columns.Add("MotorType");
            dt.Columns.Add("ParentPartNum");
            dt.Columns.Add("RuleHeadID");
            dt.Columns.Add("RuleBatchID");
            dt.Columns.Add("Generation");
           // dt.Columns.Add("SubAssemblyPart"); 03/11/2020
            dt.Columns.Add("GuidID");

            string curParentPartNum = "";

            foreach (DataGridViewRow dgvr in dgvBOM.Rows)
            {
                configurableSubAsm = false;
                if (dgvr.Cells["PartCategory"].Value.ToString() != "")
                {
                    categoryStr = dgvr.Cells["PartCategory"].Value.ToString();
                }

                insertPartBool = true;
                var dr = dt.NewRow();

                parentPartNumStr = dgvr.Cells["ParentPartNum"].Value.ToString();

                //if (unitTypeStr == "VKG")
                //{
                //subAsmPart = dgvr.Cells["SubAssemblyPart"].Value.ToString(); Commented out temporarily 03/11/2020
                if (subAsmPart == "True")
                {
                    configurableSubAsm = true;
                }
               //}

                assemblySeqStr = dgvr.Cells["AssemblySeq"].Value.ToString();
                asmSeqInt = Int32.Parse(assemblySeqStr);

                if (parentPartNumStr.Length > 0 && asmSeqInt > 0)
                {                                                             
                    generationStr = dgvr.Cells["Generation"].Value.ToString();
                    generationInt = Int32.Parse(generationStr);
                                             
                    if (curParentPartNum != parentPartNumStr)
                    {
                        curParentPartNum = parentPartNumStr;
                        asmPartNum = parentPartNumStr;
                        
                        if (parentPartNumStr.Contains("OACONTROL") == true)
                        {                            
                            parentInt = 0;
                            priorPeerInt = -1;
                            nextPeerInt = 2;
                            childInt = -1;
                            bomLevel = 1;
                            bomSeq = 100;                           
                        }
                        else if (parentPartNumStr.Contains("RFG") == true)
                        {
                            if (parentPartNumStr.Contains("RFGASM") == true)
                            {
                                asmPartNum = parentPartNumStr.Substring(0, parentPartNumStr.IndexOf(" ("));
                            }
                            else
                            {
                                asmPartNum = parentPartNumStr;
                            }
                            parentInt = 0;
                            priorPeerInt = asmSeqInt - 1;
                            nextPeerInt = asmSeqInt+1;
                            childInt = -1;
                            bomLevel = 1;
                            bomSeq += 100;                            
                        }
                        else if (parentPartNumStr.StartsWith("PNL") == true || generationInt >= 1)
                        {
                            parLen = parentPartNumStr.Length;
                            eqPos = parentPartNumStr.IndexOf("=");
                            if (eqPos > 0)
                            {
                                asmPartNum = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                            }
                            
                            bomLevel = generationInt;
                            
                            if (generationInt == 1)
                            {
                                parentInt = 0;
                                if (pnlSubCnt == 1)
                                {
                                    priorPeerInt = asmSeqInt - 1;
                                    prevPriorPeer = asmSeqInt;
                                    ++pnlSubCnt;
                                }
                                else
                                {
                                    priorPeerInt = prevPriorPeer;
                                    prevPriorPeer = asmSeqInt;
                                    ++pnlSubCnt;
                                }

                                nextPeerInt = -1;
                                childInt = asmSeqInt + 1;
                                bomSeq += 100;
                            }                            
                            else if (generationInt > 1)
                            {
                                parentInt = asmSeqInt - 1;
                                priorPeerInt = -1;
                                nextPeerInt = -1;
                                childInt = asmSeqInt + 1;
                                bomSeq += 1;
                            }
                        }
                        else if (configurableSubAsm == true)
                        {
                            if (parentPartNumStr.Contains("=") == true)
                            {
                                asmPartNum = parentPartNumStr.Substring(0, parentPartNumStr.IndexOf(" ="));
                            }
                            else
                            {
                                asmPartNum = parentPartNumStr;
                            }
                            parentInt = 0;
                            priorPeerInt = -1;
                            nextPeerInt = 2;
                            childInt = -1;
                            bomLevel = 1;
                            bomSeq += 100;       
                        }

                        SubAsm.Add(new SubAssembly(asmPartNum, revNumStr, dgvr.Cells["ReqQty"].Value.ToString(), dgvr.Cells["UOM"].Value.ToString(),
                                                   asmSeqInt, parentInt, priorPeerInt, nextPeerInt, childInt, bomSeq, bomLevel, primaryAsmSeqInt,
                                                   immedParentPartNum, refReqDes, weightUOMStr, configurableSubAsm));
                        //TandemCompAsm.Add((parentPartNumStr + assemblySeqStr));                        
                    }                    
                }

                dr["JobNum"] = jobNumStr;
                dr["AssemblySeqNum"] = assemblySeqStr;
                dr["SeqNum"] = dgvr.Cells["SeqNo"].Value.ToString();
                dr["PartNum"] = dgvr.Cells["PartNum"].Value.ToString();
                dr["PartDescription"] = dgvr.Cells["Description"].Value.ToString();
                dr["SeqNum"] = dgvr.Cells["SeqNo"].Value.ToString();
                qtyPerStr = dgvr.Cells["QtyPer"].Value.ToString();
                dr["QtyPerDec"] = decimal.Parse(qtyPerStr);
                string reqQtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                decimal reqQtyDec = decimal.Parse(reqQtyStr);
                dr["PartQtyDec"] = reqQtyDec;
                reqQtyStr = Math.Round(reqQtyDec, MidpointRounding.AwayFromZero).ToString();
                dr["PartQtyInt"] = int.Parse(reqQtyStr);
                dr["IUM"] = dgvr.Cells["UOM"].Value.ToString();
                dr["UnitOfCost"] = dgvr.Cells["UnitCost"].Value.ToString();
                dr["TotalCost"] = (decimal.Parse(dgvr.Cells["UnitCost"].Value.ToString()) * reqQtyDec).ToString();
                dr["PartAssembly"] = categoryStr;
                dr["PartType"] = dgvr.Cells["PartType"].Value.ToString();
                dr["WarehouseCode"] = dgvr.Cells["WhseCode"].Value.ToString();
                dr["RelatedOperation"] = dgvr.Cells["RelOp"].Value.ToString();
                dr["Status"] = dgvr.Cells["Status"].Value.ToString();
                dr["RevisionNumber"] = dgvr.Cells["RevisionNum"].Value.ToString();
                dr["CostMethod"] = dgvr.Cells["CostMethod"].Value.ToString();
                dr["MotorType"] = "";
                dr["ParentPartNum"] = parentPartNumStr;
                //dr["RuleHeadID"] = dgvr.Cells["RuleHeadID"].Value.ToString();
                dr["RuleHeadID"] = 0;
                dr["RuleBatchID"] = dgvr.Cells["RuleBatchID"].Value;
                dr["Generation"] = dgvr.Cells["Generation"].Value;
                //if (configurableSubAsm == true)  // Commented out temp 03/11/2020
                //{
                //    dr["SubAssemblyPart"] = 1;
                //}
                //else
                //{
                //    dr["SubAssemblyPart"] = 0;
                //}
                dr["GuidID"] = Guid.NewGuid().ToString();

                if (dgvr.Cells["PartStatus"].Value.ToString() == "InActive")
                {
                    DialogResult result1 = MessageBox.Show("Part Number => " + dgvr.Cells["PartNum"].Value.ToString() + " shows an InActive status. Do you wish to include this Part Number in the BOM?",
                                                            "WARNING",
                                                            MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.No)
                    {
                        insertPartBool = false;
                    }
                }

                if (insertPartBool == true)
                {
                    dt.Rows.Add(dr);
                }
            }

            try
            {
                string partNum = "";
                string seqNum = "";
                foreach(DataRow dr in dt.Rows)
                {
                    partNum = dr["PartNum"].ToString();
                    assemblySeqStr = dr["AssemblySeqNum"].ToString();
                    seqNum = dr["SeqNum"].ToString();
                }

                if (dt.Rows.Count > 0)
                {
                    objMain.InsertBOM_DataIntoJobMtl(dt);
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error inserting data into jobmtl table for job# " + jobNumStr + " - " + f);
            }

            //adjustSubAssemblyNextPeersAndChildValues(); 03/12/2021 Obsolete
            return totalMaterialCostDec;
        }

        private void getSubAssemblies()
        {            
            string parentPartNumStr = "";
            string partNumStr = "";
            string assemblySeqStr = "0";
            string asmPartNum = "";
            string revNumStr = "";            
            string weightUOMStr = "";
            string subAsmPartStr = "";
            string tmpGen = "";
            string innerSubAsmPartNum = "";
            string primarySubAsmPartNum = "";
            string curPrimarySubAsmPartNum = "";
            string immediateParentPartNum = "";
            string generationStr = "";

            int parentInt = 0;
            int priorPeerInt = -1;
            int nextPeerInt = 2;
            int childInt = -1;
            int primaryAsmSeqInt = -1;
            int asmSeqInt = 0;
            int generationInt = 0;     
            int bomSeq = 0;
            int bomLevel = 0;
            int refReqDes = 0;
            int eqPos = -1;
            int lastEqPos = -1;
            int parenPos = -1;
            int parLen = 0;

            bool subAsmPart = false;
            bool subAsmMtlPart = false;
            bool preConfigAsm = false;
            bool configurablePart = false; 

            string curImmedParentPartNum = "";

            foreach (DataGridViewRow dgvr in dgvBOM.Rows)
            {
                asmSeqInt = (int)dgvr.Cells["AssemblySeq"].Value;
                configurablePart = false;                                      

                //assemblySeqStr = dgvr.Cells["AssemblySeq"].Value.ToString();
                //asmSeqInt = Int32.Parse(assemblySeqStr);

                if (asmSeqInt > 0) // Indicates that this PartNum is part of a SubAssembly
                {                    
                    generationInt = (Int32)dgvr.Cells["Generation"].Value;                                                           
                    partNumStr = dgvr.Cells["PartNum"].Value.ToString();
                    revNumStr = dgvr.Cells["RevisionNum"].Value.ToString();
                    parentPartNumStr = dgvr.Cells["ParentPartNum"].Value.ToString();
                    immediateParentPartNum = dgvr.Cells["ImmediateParent"].Value.ToString();                   
                  
                    if (immediateParentPartNum.Length == 0)
                    {
                        eqPos = parentPartNumStr.IndexOf("=");
                        if (eqPos > 0) // Indicates that this Parent PartNum contains an InnerSub Assembly
                        {
                            primarySubAsmPartNum = parentPartNumStr.Substring(0, (eqPos - 1));
                        }
                        immediateParentPartNum = primarySubAsmPartNum;
                    }

                    if (curImmedParentPartNum != immediateParentPartNum)
                    {
                        curImmedParentPartNum = immediateParentPartNum;

                        if (dgvr.Cells["ConfigurablePart"].Value.ToString() == "Yes")
                        {
                            configurablePart = true;
                            DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
                            if (dtRev.Rows.Count > 0)
                            { 
                            
                            }
                        }

                        //if (curPrimarySubAsmPartNum != primarySubAsmPartNum)
                        //{
                        //    curPrimarySubAsmPartNum = primarySubAsmPartNum;
                        //    primaryAsmSeqInt = (int)dgvr.Cells["AssemblySeq"].Value;
                        //}

                        primaryAsmSeqInt = (int)dgvr.Cells["AssemblySeq"].Value;
                        asmPartNum = immediateParentPartNum;                                               

                        //lastEqPos = parentPartNumStr.LastIndexOf("=");
                        //if (eqPos == lastEqPos)
                        //{
                        //    tmpGen = parentPartNumStr.Substring((eqPos + 2), 1);
                        //    parLen = parentPartNumStr.Length;
                        //    innerSubAsmPartNum = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                        //    asmPartNum = parentPartNumStr.Substring(0, (eqPos - 1));                                   
                        //}
                        //else
                        //{
                        //    tmpGen = parentPartNumStr.Substring((lastEqPos + 2), 1);
                        //    parLen = parentPartNumStr.Length;
                        //    innerSubAsmPartNum = parentPartNumStr.Substring((lastEqPos + 3), (parLen - (lastEqPos + 3)));
                        //    asmPartNum = parentPartNumStr.Substring(0, (eqPos - 1));                                   
                        //}
                                              
                        if (generationInt < 2)
                        {
                            parentInt = 0;
                            nextPeerInt = -1;
                            childInt = -1;
                            priorPeerInt = -1;

                            if (generationInt == 0)
                            {
                                generationInt = 1;
                            }

                            bomLevel = generationInt;
                            bomSeq = 100;
                        }
                        else
                        {
                            parentInt = -1;
                            priorPeerInt = -1;
                            nextPeerInt = -1;
                            childInt = -1;
                            bomLevel = generationInt;
                            bomSeq += 100;
                            asmPartNum = innerSubAsmPartNum;
                        }

                        SubAsm.Add(new SubAssembly(asmPartNum, revNumStr, dgvr.Cells["ReqQty"].Value.ToString(), dgvr.Cells["UOM"].Value.ToString(),
                                                   asmSeqInt, parentInt, priorPeerInt, nextPeerInt, childInt, bomSeq, bomLevel, primaryAsmSeqInt,
                                                   immediateParentPartNum, refReqDes, weightUOMStr, configurablePart));                                                                        
                    }
                }
                else
                {
                    immediateParentPartNum = "";
                }              
            }
          
            //adjustSubAssemblyNextPeersAndChildValues();            
        }


        private string saveBomDataIntoEpicor(string jobNumStr, string unitTypeStr)
        {
            string errorMsg = "";
            string epicorServer = "";

            decimal totalMaterialCostDec = 0;

            epicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

#if Debug
            epicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
#endif           
            
            string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;
            
            Console.WriteLine(epicorServer);

            if (epicorServer.ToUpper().StartsWith("EPICOR905") == true || epicorServer.ToUpper().StartsWith("KCCWVTEPIC") == true || epicorServer.ToUpper().StartsWith("KCCWVPEPIC") == true)
            {
                BuildBOM bb = new BuildBOM(epicorServer, epicorSqlConnectionString);
                BindingSource bs = new BindingSource();
                bs.DataSource = dgvBOM.DataSource;
                DataTable dtBOM = (DataTable)(bs.DataSource);

                errorMsg = bb.SaveBOM(jobNumStr, dtBOM);
            }
            else
            {
                KineticBuild_BOM bb = new KineticBuild_BOM(epicorServer, epicorSqlConnectionString);
                BindingSource bs = new BindingSource();
                bs.DataSource = dgvBOM.DataSource;
                DataTable dtBOM = (DataTable)(bs.DataSource);

                errorMsg = bb.SaveBOM(jobNumStr, dtBOM);
            }
            //MessageBox.Show("epicorServer = " + epicorServer + "  epicorSqlConnectionString = " + epicorSqlConnectionString); 
           
            //BindingSource bs = new BindingSource();
            //bs.DataSource = dgvBOM.DataSource;
            //DataTable dtBOM = (DataTable)(bs.DataSource);
           
            //errorMsg = bb.SaveBOM(jobNumStr, dtBOM);
            //errorMsg = bb.SaveFullyIndentedBOM(jobNumStr, dtBOM);
           
            if (errorMsg.Length == 0)
            {
                btnAddPart.Enabled = true;
                btnSave.Enabled = false;
                lbSaveMsg.Text = "Save Complete!";
                MessageBox.Show("Job# " + jobNumStr + " has been saved successfully.");                
            }
            else
            {
                gbSaveBomProgress.Visible = false;
                //MessageBox.Show("Here");
                MessageBox.Show(errorMsg);
            }
            gbSaveBomProgress.Visible = false;

            //foreach (DataGridViewRow dgvr in dgvBOM.Rows)
            //{
            //    if (dgvr.Cells["PartCategory"].Value.ToString() != "")
            //    {
            //        categoryStr = dgvr.Cells["PartCategory"].Value.ToString();
            //    }
            //}
            return errorMsg;
        }

        private decimal writeVikingBomDataIntoJobMtl(string jobNumStr, string unitTypeStr)
        {
            string categoryStr = "";
            string parentPartNumStr = "";
            string partNumStr = "";
            string assemblySeqStr = "0";            
            string asmPartNum = "";
            string revNumStr = "";
            string qtyPerStr = "";
            string weightUOMStr = "";
            string subAsmPart = "";
            string tmpGen = "";
            string innerSubAsmPartNum = "";
            string primarySubAsmPartNum = "";
            string curPrimarySubAsmPartNum = "";
            string immediateParentPartNum = "";           
            string generationStr = "";

            int parentInt = 0;
            int priorPeerInt = -1;
            int nextPeerInt = 2;
            int childInt = -1;
            int primaryAsmSeqInt = -1;
            int asmSeqInt = 0;
            int generationInt = 0;
            int prevPriorPeer = 0;
            int pnlSubCnt = 1;
            int bomSeq = 0;
            int bomLevel = 0;
            int refReqDes = 0;
            int eqPos = -1;
            int lastEqPos = -1;
            int parenPos = -1;    
            int parLen = 0;
            int curParent = 0;
            int curGen1PriorPeer = -1;
            int curGen1NextPeer = 1;
            int curGen2PriorPeer = 0;
            int curGen2NextPeer = 0;
            int curGen3PriorPeer = 0;
            int curGen3NextPeer = 0;

            decimal totalMaterialCostDec = 0;

            bool insertPartBool;
            bool configurableSubAsm = false;
            bool firstGen2Asm = true;
            bool firstGen3Asm = true;

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum");
            dt.Columns.Add("AssemblySeqNum");
            dt.Columns.Add("SeqNum");
            dt.Columns.Add("PartNum");
            dt.Columns.Add("PartDescription");
            dt.Columns.Add("QtyPerDec");
            dt.Columns.Add("PartQtyDec");
            dt.Columns.Add("PartQtyInt");
            dt.Columns.Add("IUM");
            dt.Columns.Add("UnitOfCost");
            dt.Columns.Add("TotalCost");
            dt.Columns.Add("PartAssembly");
            dt.Columns.Add("PartType");
            dt.Columns.Add("WarehouseCode");
            dt.Columns.Add("RelatedOperation");
            dt.Columns.Add("Status");
            dt.Columns.Add("RevisionNumber");
            dt.Columns.Add("CostMethod");
            dt.Columns.Add("ImmediateParent");
            dt.Columns.Add("ParentPartNum");
            dt.Columns.Add("SubAsmMtlPart");
            dt.Columns.Add("RuleBatchID");
            dt.Columns.Add("Generation");
            dt.Columns.Add("SubAssemblyPart");
            dt.Columns.Add("GuidID");

            string curParentPartNum = "";

            foreach (DataGridViewRow dgvr in dgvBOM.Rows)
            {
                configurableSubAsm = false;
                if (dgvr.Cells["PartCategory"].Value.ToString() != "")
                {
                    categoryStr = dgvr.Cells["PartCategory"].Value.ToString();
                }

                insertPartBool = true;
                var dr = dt.NewRow();

                partNumStr = dgvr.Cells["PartNum"].Value.ToString();  
                parentPartNumStr = dgvr.Cells["ParentPartNum"].Value.ToString();                

                assemblySeqStr = dgvr.Cells["AssemblySeq"].Value.ToString();
                asmSeqInt = Int32.Parse(assemblySeqStr);

                if (parentPartNumStr.Length > 0 && asmSeqInt > 0) // Indicates that this PartNum is part of a SubAssembly
                {                    
                    subAsmPart = dgvr.Cells["SubAssemblyPart"].Value.ToString();
                    if (subAsmPart == "True")
                    {
                        configurableSubAsm = true;
                    }                    

                    generationStr = dgvr.Cells["Generation"].Value.ToString();
                    generationInt = Int32.Parse(generationStr);
                    
                    firstGen2Asm = true;
                    firstGen3Asm = true;
                    innerSubAsmPartNum = "";
                    immediateParentPartNum = "";

                    immediateParentPartNum = dgvr.Cells["ImmediateParent"].Value.ToString();

                    if (parentPartNumStr.StartsWith("RFG") == true)
                    {
                        parenPos = parentPartNumStr.IndexOf(" (");
                        if (parenPos > 0)
                        {
                            parentPartNumStr = parentPartNumStr.Substring(0, parentPartNumStr.IndexOf(" ("));
                        }                        
                    }

                    if (curParentPartNum != parentPartNumStr)
                    {
                        curParentPartNum = parentPartNumStr;

                        if (asmSeqInt > 0) // Ignore Parent part number if part of the main assembly
                        {                            
                            eqPos = parentPartNumStr.IndexOf("=");
                            if (eqPos > 0) // Indicates that this Parent PartNum contains an InnerSub Assembly
                            {
                                primarySubAsmPartNum = parentPartNumStr.Substring(0, (eqPos - 1));

                                //parentPartNumStr = parentPartNumStr.Substring(0, (eqPos - 1));
                                
                                if (curPrimarySubAsmPartNum != primarySubAsmPartNum)
                                {
                                    curPrimarySubAsmPartNum = primarySubAsmPartNum;
                                    primaryAsmSeqInt = (int)dgvr.Cells["AssemblySeq"].Value;
                                }

                                lastEqPos = parentPartNumStr.LastIndexOf("=");
                                if (eqPos == lastEqPos)
                                {
                                    tmpGen = parentPartNumStr.Substring((eqPos + 2), 1);
                                    parLen = parentPartNumStr.Length;
                                    innerSubAsmPartNum = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                                    asmPartNum = parentPartNumStr.Substring(0, (eqPos - 1));
                                    //generationInt = Int32.Parse(tmpGen);
                                }
                                else
                                {
                                    tmpGen = parentPartNumStr.Substring((lastEqPos + 2), 1);
                                    parLen = parentPartNumStr.Length;
                                    innerSubAsmPartNum = parentPartNumStr.Substring((lastEqPos + 3), (parLen - (lastEqPos + 3)));
                                    asmPartNum = parentPartNumStr.Substring(0, (eqPos - 1));
                                    //generationInt = Int32.Parse(tmpGen);
                                }

                            }
                            else // This is child assembly of the main level or Generation = 1
                            {
                                generationInt = 1;
                                asmPartNum = parentPartNumStr;
                            }

                            if (generationInt < 2)
                            {
                                parentInt = 0;
                                nextPeerInt = -1;
                                childInt = -1;                               
                                priorPeerInt = -1;
                                
                                if (generationInt == 0)
                                {
                                    generationInt = 1;
                                }

                                bomLevel = generationInt;
                                bomSeq = 100;
                                //asmPartNum = parentPartNumStr;
                            }
                            else 
                            {
                                parentInt = -1;
                                priorPeerInt = -1;
                                nextPeerInt = -1;
                                childInt = -1;
                                bomLevel = generationInt;
                                bomSeq += 100;
                                asmPartNum = innerSubAsmPartNum;
                            }
                            

                            SubAsm.Add(new SubAssembly(asmPartNum, revNumStr, dgvr.Cells["ReqQty"].Value.ToString(), dgvr.Cells["UOM"].Value.ToString(),
                                                       asmSeqInt, parentInt, priorPeerInt, nextPeerInt, childInt, bomSeq, bomLevel, primaryAsmSeqInt,
                                                       immediateParentPartNum, refReqDes, weightUOMStr, configurableSubAsm));
                            //TandemCompAsm.Add((parentPartNumStr + assemblySeqStr));                        
                        }
                    }
                }
                else
                {
                    immediateParentPartNum = "";
                }

                dr["JobNum"] = jobNumStr;
                dr["AssemblySeqNum"] = assemblySeqStr;
                dr["SeqNum"] = dgvr.Cells["SeqNo"].Value.ToString();
                dr["PartNum"] = dgvr.Cells["PartNum"].Value.ToString();
                dr["PartDescription"] = dgvr.Cells["Description"].Value.ToString();                
                qtyPerStr = dgvr.Cells["QtyPer"].Value.ToString();
                dr["QtyPerDec"] = decimal.Parse(qtyPerStr);
                string reqQtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                decimal reqQtyDec = decimal.Parse(reqQtyStr);
                dr["PartQtyDec"] = reqQtyDec;
                reqQtyStr = Math.Round(reqQtyDec, MidpointRounding.AwayFromZero).ToString();
                dr["PartQtyInt"] = int.Parse(reqQtyStr);
                dr["IUM"] = dgvr.Cells["UOM"].Value.ToString();
                dr["UnitOfCost"] = dgvr.Cells["UnitCost"].Value.ToString();
                dr["TotalCost"] = (decimal.Parse(dgvr.Cells["UnitCost"].Value.ToString()) * reqQtyDec).ToString();
                dr["PartAssembly"] = categoryStr;
                dr["PartType"] = dgvr.Cells["PartType"].Value.ToString();
                dr["WarehouseCode"] = dgvr.Cells["WhseCode"].Value.ToString();
                dr["RelatedOperation"] = dgvr.Cells["RelOp"].Value.ToString();
                dr["Status"] = dgvr.Cells["Status"].Value.ToString();
                dr["RevisionNumber"] = dgvr.Cells["RevisionNum"].Value.ToString();
                dr["CostMethod"] = dgvr.Cells["CostMethod"].Value.ToString();
                dr["ImmediateParent"] = immediateParentPartNum;
                dr["ParentPartNum"] = parentPartNumStr;
                dr["SubAsmMtlPart"] = dgvr.Cells["SubAsmMtlPart"].Value.ToString(); ;
                dr["RuleBatchID"] = dgvr.Cells["RuleBatchID"].Value.ToString();
                dr["Generation"] = dgvr.Cells["Generation"].Value.ToString();
                if (configurableSubAsm == true)
                {
                    dr["SubAssemblyPart"] = 1;
                }
                else
                {
                    dr["SubAssemblyPart"] = 0;
                }
                dr["GuidID"] = Guid.NewGuid().ToString();

                if (dgvr.Cells["PartStatus"].Value.ToString() == "InActive")
                {
                    DialogResult result1 = MessageBox.Show("Part Number => " + dgvr.Cells["PartNum"].Value.ToString() + " shows an InActive status. Do you wish to include this Part Number in the BOM?",
                                                            "WARNING",
                                                            MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.No)
                    {
                        insertPartBool = false;
                    }
                }

                if (insertPartBool == true)
                {
                    dt.Rows.Add(dr);
                }
            }

            try
            {
                string partNum = "";
                string seqNum = "";
                foreach (DataRow dr in dt.Rows)
                {
                    partNum = dr["PartNum"].ToString();
                    assemblySeqStr = dr["AssemblySeqNum"].ToString();
                    seqNum = dr["SeqNum"].ToString();
                }

                if (dt.Rows.Count > 0)
                {
                    objMain.InsertBOM_DataIntoJobMtl(dt);
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error inserting data into jobmtl table for job# " + jobNumStr + " - " + f);
            }

            adjustSubAssemblyNextPeersAndChildValues();
            return totalMaterialCostDec;
        }
       
        private string writeJobOperationsNew(string jobNumStr, string partNumStr, string revTypeStr)
        {
            string revisionNumStr = "";
            string partNumSchdStr = "";
            string partRevStr = "";
            string burdenRateStr = "";
            string laborRateStr = "";
            string oprSeqStr = "";
            string schdLine = "";
            string curProdLine = "";                     

            DataTable dtRev = objMain.GetPartRevisionNum(partNumStr);
            if (dtRev.Rows.Count > 0)
            {
                DataRow drRev = dtRev.Rows[0];
                partRevStr = drRev["RevisionNum"].ToString();
            }

            DataTable dtJobOper = objMain.GetOA_JobOperations(jobNumStr);

            DataTable dtSchd = objMain.GetOA_ScheduleData(jobNumStr);

            if (dtSchd.Rows.Count > 0)
            {
                DataRow drSchd = dtSchd.Rows[0];
                schdLine = drSchd["ProdLine"].ToString();
            }

            if (dtJobOper.Rows.Count > 0)
            {
                curProdLine = getCurrentProdLine(dtJobOper, "Job");

                //if (curProdLine != schdLine)
                //{
                    //partNumSchdStr = partNumStr.Substring(0, 3);
                    //partNumSchdStr += "LINE";
                    //partNumSchdStr += schdLine.Substring((schdLine.Length - 1), 1);

                    objMain.DeleteJobOpDtl(jobNumStr);
                    objMain.DeleteJobOper(jobNumStr);
                    
                //}
                //else
                //{
                //    partNumSchdStr = partNumStr; // If Operations already exist for the corrent Line, then return the Part number passed into the method.
                //}
            }
            //else
            //{
                //DataTable drPartOpr = objMain.GetPartOperationData(partNumStr, partRevStr);
                //curProdLine = getCurrentProdLine(drPartOpr, "Part");
                //string lineNum = "";

                //if (curProdLine != schdLine)
                //{
                //    lineNum = schdLine.Substring((schdLine.Length - 1), 1);
                //    if (lineNum == "1" || lineNum == "2" || lineNum == "3" || lineNum == "4")
                //    {
                //        partNumSchdStr = "GRSLINE" + lineNum;
                //    }
                //    else
                //    {
                //        partNumSchdStr = partNumStr.Substring(0, 3) + "LINE" + lineNum;                       
                //    }
                //}
                //else
                //{
                //    partNumSchdStr = partNumStr; // If Operations already exist for the corrent Line, then return the Part number passed into the method.
                //}           

            if ((revTypeStr == "REV5" || revTypeStr == "REV6") && partNumStr.Length > 3)
            {
                partNumSchdStr = partNumStr.Substring(0, 3);
                partNumSchdStr += "LINE";
                partNumSchdStr += schdLine.Substring((schdLine.Length - 1), 1);
            }
            else
            {
                partNumSchdStr = partNumStr;
            }

            writeJobOperations(jobNumStr, partNumSchdStr, revTypeStr);
            //}

            return partNumSchdStr;
           
        }

        private void adjustSubAssemblyNextPeersAndChildValues()
        {
            int subCnt = 0;
            int curParent = 0;
            int childAsm = 0;
            string curAsmPartNum = "";
            bool nextPeerFound = false;

            foreach(SubAssembly sub in SubAsm)
            {                
                if (sub.Parent == 0 && sub.AssemblyPartNum.Contains("PNLASM") )
                {                    
                    for (int i = (subCnt + 1); i < SubAsm.Count; ++i )
                    {                                
                        if (SubAsm[i].Parent == 0)
                        {
                            childAsm = i;
                            nextPeerFound = true;
                            break;
                        }
                    }
                    if (nextPeerFound)
                    {
                        sub.NextPeer = childAsm;
                    }
                    else
                    {
                        sub.NextPeer = -1;
                    }                   
                }
                else if(sub.Parent > 0)  // Check for correct Child assignment.
                {
                    curParent = sub.Parent;
                    if ((subCnt + 1) < SubAsm.Count)
                    {
                        if (SubAsm[subCnt + 1].Parent > curParent)
                        {
                            sub.Child = SubAsm[subCnt + 1].Parent + 1;
                        }
                        else
                        {
                            sub.Child = -1;
                        }
                    }
                    else
                    {
                        sub.Child = -1;
                    }
                                            
                }
                ++subCnt;
            }
        }

        private string getCurrentProdLine(DataTable dtJobOper, string tableType)
        {
            string prodLine = "";
            string oprSeq = "";

            if (tableType == "Job")
            {
                foreach (DataRow row in dtJobOper.Rows)
                {
                    if (row["AssemblySeq"].ToString() == "0" && row["OprSeq"].ToString() == "40")
                    {
                        if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "NoDtl")
                        {
                            prodLine = row["ResourceID"].ToString().Substring(0, 2);
                            switch (prodLine)
                            {
                                case "L1":
                                    prodLine = "Line 1";
                                    break;
                                case "L2":
                                    prodLine = "Line 2";
                                    break;
                                case "L3":
                                    prodLine = "Line 3";
                                    break;
                                case "L4":
                                    prodLine = "Line 4";
                                    break;
                                case "LA":
                                    prodLine = "Line A";
                                    break;
                                case "LB":
                                    prodLine = "Line B";
                                    break;
                                case "LC":
                                    prodLine = "Line C";
                                    break;
                                case "LD":
                                    prodLine = "Line D";
                                    break;
                                case "LE":
                                    prodLine = "Line E";
                                    break;
                                default:
                                    prodLine = "????";
                                    break;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (DataRow row in dtJobOper.Rows)
                {
                    if (row["OprSeq"].ToString() == "40")
                    {
                        if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "NoDtl")
                        {
                            prodLine = row["ResourceID"].ToString().Substring(0, 2);
                            switch (prodLine)
                            {
                                case "L1":
                                    prodLine = "Line 1";
                                    break;
                                case "L2":
                                    prodLine = "Line 2";
                                    break;
                                case "L3":
                                    prodLine = "Line 3";
                                    break;
                                case "L4":
                                    prodLine = "Line 4";
                                    break;
                                case "LA":
                                    prodLine = "Line A";
                                    break;
                                case "LB":
                                    prodLine = "Line B";
                                    break;
                                case "LC":
                                    prodLine = "Line C";
                                    break;
                                case "LD":
                                    prodLine = "Line D";
                                    break;
                                case "LE":
                                    prodLine = "Line E";
                                    break;
                                default:
                                    prodLine = "????";
                                    break;
                            }
                        }
                    }
                }
            }

            return prodLine;
        }

        private string getScheduleLine(string jobNum)
        {
            string schdLine = "";

            DataTable dtSchd = objMain.GetOA_ScheduleData(jobNum);

            //if ()
            return schdLine;
        }

        private void writeJobOperations(string jobNumStr, string partNumStr, string revType)
        {
            string revisionNumStr = "";
            //string partNumStr = modelNoStr.Substring(0, 7);
            string burdenRateStr = "";
            string laborRateStr = "";
            string oprSeqStr = "";           
            string schdLine = "";
            
            int asmSeqInt = 0;

            bool operationFound = false;
            bool operDtlFound = false;
            bool addOperation = false;

            DataTable dtRev = objMain.GetPartRevisionNum(partNumStr);

            DataTable dtJobOper = objMain.GetOA_JobOperations(jobNumStr);
          
            if (dtRev.Rows.Count > 0)
            {               
                DataRow drRev = dtRev.Rows[0];
                revisionNumStr = drRev["RevisionNum"].ToString();                

                DataTable dtPartOper = objMain.GetPartOperationData(partNumStr, revisionNumStr);

                foreach (DataRow dr in dtPartOper.Rows)
                {
                    oprSeqStr = dr["OprSeq"].ToString();
                    operationFound = false;
                    operDtlFound = false;

                    if (revType == "REV6" && oprSeqStr == "35")
                    {                      
                        addOperation = false;                        
                    }
                    else
                    {
                        addOperation = true;
                    }

                    if (addOperation == true)
                    {
                        foreach (DataRow row in dtJobOper.Rows)
                        {
                            if ((Int32.Parse(row["AssemblySeq"].ToString()) == 0) && (Int32.Parse(row["OprSeq"].ToString()) == Int32.Parse(oprSeqStr)))
                            {
                                if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "NoDtl")
                                {
                                    operDtlFound = true;
                                }
                                WarehouseCode = "LWH5";
                                operationFound = true;
                                break;
                            }
                        }

                        if (operationFound == false)
                        {
                            try
                            {
                                objMain.InsertJobOper(jobNumStr, revisionNumStr, "0", oprSeqStr, dr["OpCode"].ToString(), dr["EstProdHours"].ToString(), dr["ProdStandard"].ToString(),
                                                      dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(), dr["QtyPer"].ToString(), dr["ProdLabRate"].ToString(),
                                                      dr["ProdBurRate"].ToString(), dr["LaborEntryMethod"].ToString(), dr["OpDtlDesc"].ToString(), Guid.NewGuid().ToString());
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOper table for Assembly Seq = 0. Error msg -> " + ex);
                            }
                        }

                        if (operDtlFound == false)
                        {
                            try
                            {
                                if (oprSeqStr == "10" || oprSeqStr == "15" || oprSeqStr == "20" || oprSeqStr == "30")
                                {
                                    burdenRateStr = dr["ProdBurRate"].ToString();
                                    laborRateStr = dr["ProdLabRate"].ToString();
                                }
                                else
                                {
                                    WarehouseCode = "LWH5";
                                    burdenRateStr = "0";
                                    laborRateStr = "0";
                                }

                                objMain.InsertJobOpDtl(jobNumStr, "0", oprSeqStr, dr["OpDtlSeq"].ToString(), dr["ResourceGrpID"].ToString(), dr["ResourceID"].ToString(), dr["EstProdHours"].ToString(),
                                                       dr["ProdStandard"].ToString(), dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(), laborRateStr, burdenRateStr,
                                                       dr["OpDtlDesc"].ToString(), dr["ProdCrewSize"].ToString(), dr["SetupCrewSize"].ToString(), Guid.NewGuid().ToString());
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOpDtl table for Assembly Seq = 0. Error msg -> " + ex);
                            }
                        }
                    }
                }

                foreach (SubAssembly sub in SubAsm)
                {
                    partNumStr = sub.AssemblyPartNum;                   

                    try
                    {
                        dtRev = objMain.GetPartRevisionNum(partNumStr);

                        if (dtRev.Rows.Count > 0)
                        {
                            DataRow drRev2 = dtRev.Rows[0];
                            revisionNumStr = drRev2["RevisionNum"].ToString();
                            sub.AssemblyRevNum = revisionNumStr;
                        }
                        else
                        {
                            MessageBox.Show("ERROR: GetPartRevisionNum " + partNumStr + ". Error msg -> RevisionNum = '" + revisionNumStr + "' does not exist. ");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR: GetPartRevisionNum " + partNumStr + ". Error msg -> " + ex);
                    }

                    asmSeqInt = sub.AsmSeq;

                    DataTable dtCP = objMain.GetPartOperationData(partNumStr, revisionNumStr);

                    foreach (DataRow dr in dtCP.Rows)
                    {
                        oprSeqStr = dr["OprSeq"].ToString();
                        operationFound = false;
                        operDtlFound = false;

                        foreach (DataRow row in dtJobOper.Rows)
                        {
                            if ((Int32.Parse(row["AssemblySeq"].ToString()) == asmSeqInt) && (Int32.Parse(row["OprSeq"].ToString()) == Int32.Parse(oprSeqStr)))
                            {
                                if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "")
                                {
                                    operDtlFound = true;
                                }
                                operationFound = true;
                                break;
                            }
                        }

                        if (operationFound == false)
                        {
                            try
                            {
                                objMain.InsertJobOper(jobNumStr, revisionNumStr, asmSeqInt.ToString(), dr["OprSeq"].ToString(), dr["OpCode"].ToString(), dr["EstProdHours"].ToString(), 
                                                      dr["ProdStandard"].ToString(), dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(), dr["QtyPer"].ToString(), 
                                                      dr["ProdLabRate"].ToString(), dr["ProdBurRate"].ToString(), dr["LaborEntryMethod"].ToString(), dr["OpDtlDesc"].ToString(), Guid.NewGuid().ToString());
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOpr table for Assembly Seq = 1. Error msg -> " + ex);
                            }
                        }

                        if (operDtlFound == false)
                        {
                            try
                            {
                                burdenRateStr = dr["ProdBurRate"].ToString();
                                laborRateStr = dr["ProdLabRate"].ToString();

                                objMain.InsertJobOpDtl(jobNumStr, asmSeqInt.ToString(), dr["OprSeq"].ToString(), dr["OpDtlSeq"].ToString(), dr["ResourceGrpID"].ToString(), dr["ResourceID"].ToString(), 
                                                       dr["EstProdHours"].ToString(), dr["ProdStandard"].ToString(), dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(), 
                                                       laborRateStr, burdenRateStr, dr["OpDtlDesc"].ToString(), dr["ProdCrewSize"].ToString(), dr["SetupCrewSize"].ToString(), Guid.NewGuid().ToString());
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOpDtl table for Assembly Seq = 1. Error msg -> " + ex);
                            }
                        }
                    }                       
                }

            //    revisionNumStr = "A";
            //    partNumStr = "OACONTROLPANEL";
            //    dtRev = objMain.GetPartRevisionNum(partNumStr);

            //    if (dtRev.Rows.Count > 0)
            //    {
            //        DataRow drRevCP = dtRev.Rows[0];
            //        revisionNumStr = drRevCP["RevisionNum"].ToString();


            //        DataTable dtCP = objMain.GetPartOperationData(partNumStr, revisionNumStr);

            //        foreach (DataRow dr in dtCP.Rows)
            //        {
            //            oprSeqStr = dr["OprSeq"].ToString();
            //            operationFound = false;
            //            operDtlFound = false;

            //            foreach (DataRow row in dtJobOper.Rows)
            //            {
            //                if ((Int32.Parse(row["AssemblySeq"].ToString()) == 1) && (Int32.Parse(row["OprSeq"].ToString()) == Int32.Parse(oprSeqStr)))
            //                {
            //                    if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "")
            //                    {
            //                        operDtlFound = true;
            //                    }
            //                    operationFound = true;
            //                    break;
            //                }
            //            }

            //            if (operationFound == false)
            //            {
            //                try
            //                {
            //                    objMain.InsertJobOper(jobNumStr, revisionNumStr, "1", dr["OprSeq"].ToString(), dr["OpCode"].ToString(), dr["EstProdHours"].ToString(), dr["ProdStandard"].ToString(),
            //                                         dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(), dr["QtyPer"].ToString(), dr["ProdLabRate"].ToString(),
            //                                         dr["ProdBurRate"].ToString(), dr["LaborEntryMethod"].ToString(), dr["OpDtlDesc"].ToString(), Guid.NewGuid().ToString());
            //                }
            //                catch (Exception ex)
            //                {
            //                    MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOpr table for Assembly Seq = 1. Error msg -> " + ex);
            //                }
            //            }

            //            if (operDtlFound == false)
            //            {
            //                try
            //                {
            //                    burdenRateStr = dr["ProdBurRate"].ToString();
            //                    laborRateStr = dr["ProdLabRate"].ToString();

            //                    objMain.InsertJobOpDtl(jobNumStr, "1", dr["OprSeq"].ToString(), dr["OpDtlSeq"].ToString(), dr["ResourceGrpID"].ToString(), dr["ResourceID"].ToString(), dr["EstProdHours"].ToString(),
            //                                           dr["ProdStandard"].ToString(), dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(), laborRateStr, burdenRateStr,
            //                                           dr["OpDtlDesc"].ToString(), dr["ProdCrewSize"].ToString(), dr["SetupCrewSize"].ToString(), Guid.NewGuid().ToString());
            //                }
            //                catch (Exception ex)
            //                {
            //                    MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOpDtl table for Assembly Seq = 1. Error msg -> " + ex);
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("WARNING: Part Number => '" + partNumStr + "' DOES NOT have an approved Revision in Epicor.  This must be resolved immediately, otherwise no operations will exist for the Control Panel!");
            //    }

            //    string assemblySeq = "";
            //    int pos = 0;
            //    foreach (string pnStr in TandemCompAsm)
            //    {
            //        pos = pnStr.IndexOf("(");
            //        if (pos > 0)
            //        {
            //            partNumStr = pnStr.Substring(0, (pos - 1));
            //        }
            //        else
            //        {
            //            partNumStr = pnStr.Substring(0, (pnStr.Length - 1));
            //        }
            //        assemblySeq = pnStr.Substring((pnStr.Length - 1), 1);
            //        revisionNumStr = "";

            //        dtRev = objMain.GetPartRevisionNum(partNumStr);

            //        if (dtRev.Rows.Count > 0)
            //        {
            //            DataRow drRevAsm = dtRev.Rows[0];
            //            revisionNumStr = drRevAsm["RevisionNum"].ToString();


            //            DataTable dtAsm = objMain.GetPartOperationData(partNumStr, revisionNumStr);

            //            foreach (DataRow dr in dtAsm.Rows)
            //            {
            //                oprSeqStr = dr["OprSeq"].ToString();
            //                operationFound = false;
            //                operDtlFound = false;

            //                foreach (DataRow row in dtJobOper.Rows)
            //                {
            //                    if ((Int32.Parse(row["AssemblySeq"].ToString()) == Int32.Parse(assemblySeq)) && (Int32.Parse(row["OprSeq"].ToString()) == Int32.Parse(oprSeqStr)))
            //                    {
            //                        if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "")
            //                        {
            //                            operDtlFound = true;
            //                        }
            //                        operationFound = true;
            //                        break;
            //                    }
            //                }

            //                if (operationFound == false)
            //                {
            //                    try
            //                    {
            //                        objMain.InsertJobOper(jobNumStr, revisionNumStr, assemblySeq, dr["OprSeq"].ToString(), dr["OpCode"].ToString(), dr["EstProdHours"].ToString(), dr["ProdStandard"].ToString(),
            //                                              dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(), dr["QtyPer"].ToString(), dr["ProdLabRate"].ToString(),
            //                                              dr["ProdBurRate"].ToString(), dr["LaborEntryMethod"].ToString(), dr["OpDtlDesc"].ToString(), Guid.NewGuid().ToString());
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOpr table for Assembly Seq = " + assemblySeq + ". Error msg -> " + ex);
            //                    }
            //                }

            //                if (operDtlFound == false)
            //                {
            //                    try
            //                    {
            //                        burdenRateStr = dr["ProdBurRate"].ToString();
            //                        laborRateStr = dr["ProdLabRate"].ToString();

            //                        objMain.InsertJobOpDtl(jobNumStr, assemblySeq, dr["OprSeq"].ToString(), dr["OpDtlSeq"].ToString(), dr["ResourceGrpID"].ToString(), dr["ResourceID"].ToString(),
            //                                               dr["EstProdHours"].ToString(), dr["ProdStandard"].ToString(), dr["StdFormat"].ToString(), dr["StdBasis"].ToString(), dr["OpsPerPart"].ToString(),
            //                                               laborRateStr, burdenRateStr, dr["OpDtlDesc"].ToString(), dr["ProdCrewSize"].ToString(), dr["SetupCrewSize"].ToString(), Guid.NewGuid().ToString());
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobOpDtl table for Assembly Seq = " + assemblySeq + ". Error msg -> " + ex);
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            MessageBox.Show("WARNING: Part Number => '" + partNumStr + "' DOES NOT have an approved Revision in Epicor.  This must be resolved immediately, otherwise no operations will exist for this Tandem Assembly!");
            //        }
            //    }

            }
            else
            {
                MessageBox.Show("WARNING: Part Number => '" + partNumStr + "' Does Not have an approved Revision in Epicor.  This must be resolved immediately, otherwise no operations will exist for this job!");
            }
        }

        private void updateJobAsmblNew(string jobNumStr, string modelNoStr, decimal tleMaterialCostDec)
        {
            string auxBoxStr = "Yes";
            string unitTypeStr;            
            string asmPartNum = "";
            string asmDescStr = "";
            string revisionNumStr = "";
            string laborCostStr = "";
            string burdenCostStr = "";
            string materialCostStr = "";
            string prodStdStr = "";
            string relatedOpStr = "";
            string curParentPartNum = "";
            string asmUOMStr = "";
            string asmQtyStr = "";
            string wgtUOMStr = "";

            int pos = 0;
            int parentInt = 0;
            int childInt = 0;
            int nextPeerInt = 0;
            int priorPeerInt = 0;
            int asmSeqInt = 0;
            int bomSeq = 0;
            int bomLevel = 0;
            int reqRefDes = 0;
            int bitFlag = 0;
            int idxCnt = 0;
            int maxIdx = 0;

            decimal tleLaborCostDec = 0;
            decimal tleBurdenCostDec = 0;
            decimal tleProdHoursDec = 0;
            decimal asmQtyDec = 0;           

            try
            {
                unitTypeStr = modelNoStr.Substring(0, 3);

                if (modelNoStr.Substring(26, 1) == "0")
                {
                    auxBoxStr = "No";
                }

                objMain.getEstimateProdCost(unitTypeStr, auxBoxStr, out tleLaborCostDec, out tleBurdenCostDec, out tleProdHoursDec);

                objMain.UpdateJobAsmbl(jobNumStr, 0, 1, tleLaborCostDec, tleBurdenCostDec, tleMaterialCostDec, tleProdHoursDec);
            }
            catch (Exception f)
            {
                MessageBox.Show("Error Updating jobasmbl Table for JobNum: " + jobNumStr + " ----> " + f);
            }

            setupPeersForSubAssemblys();
            
            maxIdx = SubAsm.Count;
            foreach(SubAssembly sub in SubAsm)
            {
                asmPartNum = sub.AssemblyPartNum;
                asmSeqInt = sub.AsmSeq;
                revisionNumStr = sub.AssemblyRevNum;
                bomSeq = sub.BomSeq;
                bomLevel = sub.BomLevel;               

                try
                {
                    DataTable dtCost = objMain.GetPartAsmCost(asmPartNum, revisionNumStr);

                    if (dtCost.Rows.Count > 0)
                    {
                        DataRow drCost = dtCost.Rows[0];
                        prodStdStr = drCost["ProdStandard"].ToString();
                        laborCostStr = drCost["TotalLaborCost"].ToString();
                        burdenCostStr = drCost["TotalBurdenCost"].ToString();
                        materialCostStr = drCost["TotalMaterialCost"].ToString();
                        asmDescStr = drCost["OpDesc"].ToString(); 
                    } 
                    else
                    {
                         if (sub.AssemblyPartNum.Contains("OACONTROL"))
                         {
                             prodStdStr = "8.00";
                             laborCostStr = "1.00";
                             burdenCostStr = "1.00";
                             materialCostStr = "1.00";
                             asmDescStr = "OA Control panel manufactured at electron";                             
                         }
                         else if (sub.AssemblyPartNum.Contains("RFG"))
                         {
                             prodStdStr = "8.00";
                             laborCostStr = "1.00";
                             burdenCostStr = "1.00";
                             materialCostStr = "1.00";
                             asmDescStr = "Tandem Compressor Assembly";                             
                         }
                         else if (sub.AssemblyPartNum.Contains("PNL"))
                         {
                             prodStdStr = "8.00";
                             laborCostStr = "1.00";
                             burdenCostStr = "1.00";
                             materialCostStr = "1.00";
                             asmDescStr = "Panel Sub Assembly";
                         }
                         else
                         {
                             prodStdStr = "8.00";
                             laborCostStr = "1.00";
                             burdenCostStr = "1.00";
                             materialCostStr = "1.00";
                             asmDescStr = sub.AssemblyPartNum + "  Sub Assembly";
                         }
                    }                                         
                }
                catch (Exception ex)
                {                    
                    MessageBox.Show("ERROR: GetPartAsmCost " + asmPartNum + ". Error msg -> " + ex);
                }

                DataTable dtPart = objMain.GetPartRulesHead(asmPartNum);
                if (dtPart.Rows.Count > 0)
                {
                    DataRow drPart = dtPart.Rows[0];
                    relatedOpStr = drPart["RelatedOperation"].ToString();
                }
                else
                {
                    relatedOpStr = "nothing";
                }


                if (sub.AssemblyPartNum.Contains("OACONTROL"))
                {
                    if (relatedOpStr == "nothing")
                    {
                        relatedOpStr = "60";
                    }
                    parentInt = sub.Parent;
                    childInt = sub.Child;
                    nextPeerInt = sub.NextPeer;
                    priorPeerInt = sub.PriorPeer;
                    asmQtyDec = 1;
                    asmUOMStr = "EA";                   
                    wgtUOMStr = "";
                    bitFlag = 0;
                }
                else if (sub.AssemblyPartNum.Contains("RFG"))
                {
                    if (relatedOpStr == "nothing")
                    {
                        relatedOpStr = "40";
                    }
                    parentInt = sub.Parent;
                    childInt = sub.Child;
                    nextPeerInt = sub.NextPeer;
                    priorPeerInt = sub.PriorPeer;
                    asmQtyDec = 1;
                    asmUOMStr = "EA";                   
                    wgtUOMStr = "";
                    bitFlag = 0;
                }
                else
                {
                    if (relatedOpStr == "nothing")
                    {
                        relatedOpStr = "50";
                    }
                    parentInt = sub.Parent;
                    childInt = sub.Child;
                    nextPeerInt = sub.NextPeer;
                    priorPeerInt = sub.PriorPeer;
                    bitFlag = 2;

                    if (sub.Parent == 0)
                    {
                        asmQtyDec = 1;
                        asmUOMStr = "EA";
                        wgtUOMStr = "";
                        curParentPartNum = sub.AssemblyPartNum; 
                    }
                    else
                    {
                        DataTable dtAsm = objMain.GetAssemblyQtyAndUOM(curParentPartNum, sub.AssemblyPartNum);
                        if (dtAsm.Rows.Count > 0 )
                        {
                            DataRow drAsm = dtAsm.Rows[0];
                            asmQtyStr = drAsm["QtyPer"].ToString();
                            asmQtyDec = decimal.Parse(asmQtyStr);
                            asmUOMStr = drAsm["UOM"].ToString();
                            wgtUOMStr = drAsm["WeightUOM"].ToString();
                        }
                        curParentPartNum = sub.AssemblyPartNum;    
                        if ((idxCnt + 1) < maxIdx)
                        {
                            if (SubAsm[idxCnt+1].Parent == 0)
                            {
                                childInt = -1;
                                bitFlag = 0;
                            }
                        }
                    }                   
                }

                try
                {
                    ++idxCnt;

                    if (sub.Parent == 0 && asmSeqInt == 0)
                    {
                        reqRefDes = 0;
                    }
                    else if (sub.Parent == 0 && asmSeqInt > 0)
                    {
                        reqRefDes = 1;
                    }
                    else if (sub.BomLevel == 2)
                    {
                        reqRefDes = 0;
                    }
                    else
                    {
                        reqRefDes = (int) Math.Round(asmQtyDec, 0);
                    }
                         
                    objMain.InsertJobAssembly(jobNumStr, asmSeqInt, asmPartNum, asmDescStr, revisionNumStr, asmQtyDec, asmUOMStr, "LWH5", 
                                              parentInt, priorPeerInt, nextPeerInt, childInt, bomSeq, bomLevel, laborCostStr, burdenCostStr, 
                                              materialCostStr, prodStdStr, relatedOpStr, reqRefDes, bitFlag, wgtUOMStr, Guid.NewGuid().ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobAsmbl table for Assembly Seq = 1. Error msg -> " + ex);
                }                
            }           
        }

        private void setupPeersForSubAssemblys()
        {
            foreach (SubAssembly sub in SubAsm)
            {              
                determineParentAssembly(sub);
                determinePriorPeerProperty(sub);
                determineNextPeerProperty(sub);
                if (sub.ConfigurableSubAsm == true)
                {
                    determineConfigurableSubAsmChildProperty(sub);
                }
                else
                {
                    determinePreConfiguredSubAsmChild(sub);
                }              
            }
        }

        private void determineParentAssembly(SubAssembly curSub)
        {
            int curParentAsmSeq = -1;

            string curSubAsmPartNum = curSub.AssemblyPartNum;
            int curPrimaryAsmSeq = curSub.ParentAsmSeq;
            int curSubBOMLevel = curSub.BomLevel;
            string subImmedParent = "";
            string subAsmPartNum = "";
            int subBOMLevel = -1;

            if (curSub.AsmSeq == 1)
            {
                curSub.Parent = 0;
            }
            else
            {
                if (curSub.BomLevel == 1)
                {
                    curSub.Parent = 0;
                }
                else if (curSub.BomLevel > 1)
                {
                    foreach(SubAssembly sub in SubAsm )
                    {
                        subImmedParent = sub.ImmediateParent;
                        subAsmPartNum = sub.AssemblyPartNum;
                        subBOMLevel = sub.BomLevel;

                        if (curSub.ParentAsmSeq == sub.ParentAsmSeq && curSub.ImmediateParent == sub.AssemblyPartNum)
                        {
                            curParentAsmSeq = sub.AsmSeq;
                            break;
                        }
                        else
                        {
                            curParentAsmSeq = -1;
                        }
                    }
                    curSub.Parent = curParentAsmSeq;
                }                                  
            }            
        }        

        private void determinePriorPeerProperty(SubAssembly curSub)
        {
            int curPriorPeer = -1;
            int prevPriorPeerAsmSeq = -1;
         
            if (curSub.AsmSeq == 1)
            {
                curSub.PriorPeer = -1;
            }
            else if (curSub.BomLevel > 1)
            {
                foreach (SubAssembly sub in SubAsm)
                {
                    if (curSub.ParentAsmSeq == sub.ParentAsmSeq)
                    {
                        if (curSub.BomLevel == sub.BomLevel)
                        {
                            if (sub.AssemblyPartNum == curSub.AssemblyPartNum)
                            {
                                curPriorPeer = prevPriorPeerAsmSeq;
                                break;
                            }
                            else
                            {
                                prevPriorPeerAsmSeq = sub.AsmSeq;
                            }
                        }
                    }                        
                }
                curSub.PriorPeer = curPriorPeer;
            }
            else
            {
                foreach (SubAssembly sub in SubAsm)
                {
                    if (sub.BomLevel == 1)
                    {
                        if (sub.AssemblyPartNum == curSub.AssemblyPartNum)
                        {
                            curPriorPeer = prevPriorPeerAsmSeq;
                            break;
                        }
                        else
                        {
                            prevPriorPeerAsmSeq = sub.AsmSeq;
                        }
                    }                                     
                }
                curSub.PriorPeer = curPriorPeer;
            }
        }

        private void determineNextPeerProperty(SubAssembly curSub)
        {
            int curNextPeer = -1;
            int parentCount = 0;

            bool firstParentMatch = false;
            bool firstSubAfterMatch = false;

            if (curSub.BomLevel == 1)
            {
                foreach (SubAssembly sub in SubAsm)
                {
                    if (sub.BomLevel == 1)
                    {
                        ++parentCount;
                        if (sub.AssemblyPartNum == curSub.AssemblyPartNum)
                        {
                            firstParentMatch = true;
                            parentCount = 0;
                        }

                        if (firstParentMatch == true && parentCount == 1)
                        {
                            curNextPeer = sub.AsmSeq;
                        }                        
                    }                   
                }
                
                curSub.NextPeer = curNextPeer;
            }      
            else
            {
                foreach (SubAssembly sub in SubAsm)
                {
                    if (sub.AssemblyPartNum == curSub.AssemblyPartNum) 
                    {
                        firstSubAfterMatch = true;                       
                    }   
                    else                     
                    {
                        if (sub.BomLevel == curSub.BomLevel)
                        {
                            if (firstSubAfterMatch ==  true)
                            {
                                curNextPeer = sub.AsmSeq;
                                break;
                            }
                        }
                        else
                        {
                            if (firstSubAfterMatch == true)
                            {
                                break;
                            }
                        }
                    }

                }
                curSub.NextPeer = curNextPeer;
            }
        }

        private void determineConfigurableSubAsmChildProperty(SubAssembly curSub)
        {                       
            curSub.Child = -1;

            foreach (SubAssembly sub in SubAsm)
            {              
                if (curSub.AssemblyPartNum == sub.ImmediateParent && sub.BomLevel > curSub.BomLevel)
                {
                    curSub.Child = sub.AsmSeq;
                    break;
                }
                else
                {
                    curSub.Child = -1;
                }
            }            
        }

        private void determinePreConfiguredSubAsmChild(SubAssembly curSub)
        {            
            if (curSub.AsmSeq == 0)
            {
                curSub.Child = -1;
            }
            else
            {
                for (int x = 0; x < SubAsm.Count; x++)
                {                    
                    if (SubAsm[x].AssemblyPartNum == curSub.AssemblyPartNum)
                    {
                        if ((x + 1) < SubAsm.Count)
                        {
                            if (curSub.BomLevel + 1 == SubAsm[x + 1].BomLevel)
                            {
                                curSub.Child = SubAsm[x + 1].AsmSeq;
                                break;
                            }
                            else
                            {
                                curSub.Child = -1;
                            }
                        }
                        else
                        {
                            curSub.Child = -1;
                        }
                    }
                }
            }
        }

        private void updateJobAsmbl(string jobNumStr, string modelNoStr, decimal tleMaterialCostDec)
        {
            //string auxBoxStr = "Yes";
            //string unitTypeStr;
            //string parentStr = "0";

            //int pos = 0;
            //int parentInt = 0;
            //int childInt = 0;
            //int nextPeerInt = 0;
            //int priorPeerInt = 0;

            //decimal tleLaborCostDec = 0;
            //decimal tleBurdenCostDec = 0;
            //decimal tleProdHoursDec = 0;            
           
            //try
            //{
            //    unitTypeStr = modelNoStr.Substring(0, 3);
               
            //    if (modelNoStr.Substring(26, 1) == "0")
            //    {
            //        auxBoxStr = "No";
            //    }

            //    objMain.getEstimateProdCost(unitTypeStr, auxBoxStr, out tleLaborCostDec, out tleBurdenCostDec, out tleProdHoursDec);

            //    objMain.UpdateJobAsmbl(jobNumStr, 0, 1, tleLaborCostDec, tleBurdenCostDec, tleMaterialCostDec, tleProdHoursDec);
            //}
            //catch (Exception f)
            //{
            //    MessageBox.Show("Error Updating jobasmbl Table for JobNum: " + jobNumStr + " ----> " + f);
            //}

            //DataTable dtAsm = objMain.GetOA_JobAssemblys(jobNumStr);
            
            //bool assemblyFound = false;

            //foreach (DataRow row in dtAsm.Rows)
            //{
            //    if (Int32.Parse(row["AssemblySeq"].ToString()) == 1)
            //    {                       
            //        assemblyFound = true;
            //        break;
            //    }
            //}

            //if (assemblyFound == false)
            //{
            //    try
            //    {
            //        foreach(SubAssembly sub in SubAsm)
            //        {
            //            if (sub.AssemblyPartNum.Contains("OACONTROL"))
            //            {
            //                parentInt = sub.Parent;
            //                childInt = sub.Child;
            //                nextPeerInt = sub.NextPeer;
            //                priorPeerInt = sub.PriorPeer;
            //                break;
            //            }
            //        }
            //        objMain.InsertJobAssembly(jobNumStr, "1", "OACONTROLPANEL", "OA Control panel manufactured at electron", 
            //                                  "A", "1", "LWH5", parentInt, priorPeerInt, nextPeerInt, childInt, "1.00", "1.00", "1.00", "8.00", "60", Guid.NewGuid().ToString());
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobAsmbl table for Assembly Seq = 1. Error msg -> " + ex);
            //    }
            //}                

            //string tandemAsmSeq = "";
            //string parentPartNum = "";
            //foreach (string pn in TandemCompAsm)
            //{
            //    assemblyFound = false;
                
            //    pos = pn.IndexOf("(");
            //    if (pos > 0)
            //    {
            //        parentPartNum = pn.Substring(0, (pos - 1));
            //    }
            //    else
            //    {
            //        parentPartNum = pn.Substring(0, (pn.Length - 1));
            //    }

            //    //parentPartNum = pn.Substring(0, (pn.Length - 1));
            //    tandemAsmSeq = pn.Substring((pn.Length - 1), 1);

            //    foreach (DataRow row in dtAsm.Rows)
            //    {
            //        if (Int32.Parse(row["AssemblySeq"].ToString()) == Int32.Parse(tandemAsmSeq))
            //        {
            //            assemblyFound = true;
            //            break;
            //        }
            //    }

            //    if (assemblyFound == false)
            //    {
            //        try
            //        {
            //            foreach (SubAssembly sub in SubAsm)
            //            {
            //                if (sub.AssemblyPartNum.Contains(parentPartNum))
            //                {
            //                    parentInt = sub.Parent;
            //                    childInt = sub.Child;
            //                    nextPeerInt = sub.NextPeer;
            //                    priorPeerInt = sub.PriorPeer;
            //                    break;
            //                }
            //            }
            //            objMain.InsertJobAssembly(jobNumStr, tandemAsmSeq, parentPartNum, "Tandem Compressor Assembly", 
            //                                      "A", "1", "LWH5", parentInt, priorPeerInt, nextPeerInt , childInt, "1.00", "1.00", "1.00", "8.00", "40", Guid.NewGuid().ToString());
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show("ERROR: Inserting Job# " + jobNumStr + " in the JobAsmbl table for Assembly Seq = 1. Error msg -> " + ex);
            //        }
            //    }                                   
            //}            

        }

        private void addETLStickerData(string jobNumStr, string unitTypeStr)
        {
            string orderNumStr;
            string orderLineStr;
            string modelNumStr;                        
            string releaseNumStr;
            string custNameStr = "";            
            string endConsumerName = this.lbCustName.Text;

            int dash1PosInt = jobNumStr.IndexOf('-');
            int dash2PosInt = jobNumStr.LastIndexOf('-');

            if (dash1PosInt > 0 && dash1PosInt > 0)
            {
                orderNumStr = jobNumStr.Substring(0, dash1PosInt);
                orderLineStr = jobNumStr.Substring((dash1PosInt + 1), (dash2PosInt - (dash1PosInt + 1)));
                releaseNumStr = jobNumStr.Substring((dash2PosInt + 1), (jobNumStr.Length - (dash2PosInt + 1)));
            }
            else
            {
                orderNumStr = jobNumStr;
                orderLineStr = "0";
                releaseNumStr = "0";
            }

            //orderNumStr = jobNumStr.Substring(0, jobNumStr.IndexOf('-'));
            //orderLineStr = jobNumStr.Substring((jobNumStr.IndexOf('-') + 1),
            //                                   (jobNumStr.LastIndexOf('-') - (jobNumStr.IndexOf('-') + 1)));
            //dashPosInt = jobNumStr.LastIndexOf('-');
            //releaseNumStr = jobNumStr.Substring((dashPosInt + 1), (jobNumStr.Length - (dashPosInt + 1)));
            
            modelNumStr = this.lbModelNo.Text;            

            if (unitTypeStr == "MON") // Use Monitor Etl method.
            {
                writeEtlMonitorStickerData(jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNumStr);
            }
            else if (unitTypeStr == "MSP")  // Use MSP Etl table.
            {
                writeMSP_EtlStickerData(jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNumStr);
                unitTypeStr = "MSP"; // This is changed for the ETL Label program to be able to recognize the unit type.
            }
            else if (unitTypeStr == "REV6")  // Use Rev 6 Etl table.
            {
                writeRev6EtlOAUStickerData(jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNumStr);
                unitTypeStr = "OAU"; // This is changed for the ETL Label program to be able to recognize the unit type.
            }
            else if (unitTypeStr == "VKG")  // Use Rev 6 Etl table.
            {
                writeRev6EtlOAUStickerData(jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNumStr);               
            }
            else
            {
                writeRev5EtlOAUStickerData(jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNumStr);
                unitTypeStr = "OAU"; // This is changed for the ETL Label program to be able to recognize the unit type.
            }

            DataTable dtEtl = objMain.GetEtlJobHeadData(jobNumStr);

            if (dtEtl.Rows.Count > 0)
            {
                objMain.DeleteEtlJobHeadData(jobNumStr);
            }

            DataTable dtOrd = objMain.GetOrderHedData(Int32.Parse(orderNumStr));
            if (dtOrd.Rows.Count > 0)
            {
                DataRow drOrd = dtOrd.Rows[0];
                custNameStr = drOrd["Name"].ToString();
            }

            objMain.InsertEtlJobHeadData(jobNumStr, custNameStr, orderNumStr, orderLineStr, releaseNumStr, unitTypeStr.Substring(0, 3), endConsumerName);
        }

        private void writeRev5EtlOAUStickerData(string jobNumStr, string orderNumStr, string orderLineStr, string releaseNumStr, string modelNumStr)
        {
            string partNumStr;
            string parentPartNumStr = "";
            string partCategoryStr;
            string motorTypeStr;
            string voltageStr = "";
            string unitTypeStr;
            string compQtyStr = "";
            string compTypeStr = "";
            string compLRAStr = "";
            string compRLA_VoltsStr = "";
            string comp1RLA_VoltsStr = "";
            string comp2RLA_VoltsStr = "";
            string comp3RLA_VoltsStr = "";
            string comp4RLA_VoltsStr = "";
            string comp5RLA_VoltsStr = "";
            string comp6RLA_VoltsStr = "";  
            string comp1QtyStr = "";
            string comp2QtyStr = "";
            string comp3QtyStr = "";
            string comp4QtyStr = "";
            string comp5QtyStr = "";
            string comp6QtyStr = ""; 
            string comp1PhaseStr = "";
            string comp2PhaseStr = "";
            string comp3PhaseStr = "";
            string comp4PhaseStr = "";
            string comp5PhaseStr = "";
            string comp6PhaseStr = "";   
            string comp1LRAStr = "";
            string comp2LRAStr = "";
            string comp3LRAStr = "";
            string comp4LRAStr = "";
            string comp5LRAStr = "";
            string comp6LRAStr = "";     
            string circuit1ChargeStr = "";
            string circuit2ChargeStr = "";
            string fanCondQtyStr = "";
            string fanCondPHStr = "";
            string fanCondFLAStr = "";
            string fanCondFLAVoltStr = "";
            string fanCondHPStr = "";
            string fanEvapQtyStr = "";
            string fanEvapPHStr = "";
            string fanEvapFLAStr = "";
            string fanEvapFLAVoltStr = "";
            string fanEvapHPStr = "";
            string fanERVQtyStr = "";
            string fanERVPHStr = "";
            string fanERVFLAStr = "";
            string fanERVFLAVoltStr = "";
            string fanERVHPStr = "";
            string fanPWRExhQtyStr = "";
            string fanPWRExhPHStr = "";
            string fanPWRExhFLAStr = "";
            string fanPWRExhFLAVoltStr = "";
            string fanPWRExhHPStr = "";
            string operatingPressureStr = "";
            string waterGlycol = "";
            string df_StaticPressureStr = "";
            string df_MaxHtgInputBTUHStr = "";
            string df_MinHeatingInputStr = "";
            string df_TempRiseStr = "";
            string df_MaxGasPressureStr = "";
            string df_MinGasPressureStr = "";
            string df_MaxPressureDropStr = "";
            string df_MinPressureDropStr = "";
            string df_ManifoldPressureStr = "";
            string df_CutOutTempStr = "";
            string if_MaxHtgInputBTUHStr = "";
            string if_HtgOutputBTUHStr = "";
            string if_MinInputBTUStr = "";
            string if_MaxEntStr = "";
            string if_TempRiseStr = "";
            string if_MaxGasPressureStr = "";
            string if_MinGasPressureStr = "";
            string if_ManifoldPressureStr = "";
            string maxOutletAirTempStr = "";
            string secHtgInputStr = "0";
            string preHtgInputStr = "0";
            string primaryHtgInputStr = "0";
            string minCKTAmpStr = "";
            string mfsMCBStr = "";
            string mcaStr = "0";
            string mopStr = "N/A";
            string elecRatingStr = "";
            string operatingVoltsStr = "";
            string testPresHighStr = "";
            string testPresLowStr = "";
            string mbhStr = "0";
            string qtyStr;
            string heatingInputElecStr = "0";
            string kwStr;
            string elecMcaStr = "0";
            string elecMopStr = "0";
            string elecRawMopStr = "0";
            string coolingMcaStr = "0";
            string coolingMopStr = "0";
            string coolingRawMopStr = "0";
            string fansOnlyMcaStr = "0";
            string fansOnlyFLAStr = "0";
            string fansOnlyRawMopStr = "0";
            string nonElecPrimNoSecFlaStr = "0";
            string nonElecPrimNoSecMcaStr = "0";
            string nonElecPrimNoSecMopStr = "0";
            string nonElecPrimWithSecFlaStr = "0";
            string nonElecPrimWithSecMcaStr = "0";
            string nonElecPrimWithSecMopStr = "0";
            string heatPumpNoSecFlaStr = "0";
            string heatPumpNoSecMcaStr = "0";
            string heatPumpNoSecMopStr = "0";
            string heatPumpWithSecFlaStr = "0";
            string heatPumpWithSecMcaStr = "0";
            string heatPumpWithSecMopStr = "0";
            string tmp_MinCKTAmpStr = "";
            string tmp_MFSMCBStr = "";
            string rawMopStr = "";
            string mfgDateStr = "";
            string currentPartTypeStr = "";
            string flowRateStr = "";
            string enteringTempStr = "";
            string heatTypeStr = "";
            string ruleHeadID = "";
            string ruleBatchID = "";
            string serialNoStr = "";
            string fuelType = "";
            string whseCode = "";
            string revisionNum = "";

            //todo: needs to be looked into
            string minCKTAmpStr2 = "";
            string mfsMCBStr2 = "";

            int voltageInt = 0;
            int evapFanCntInt = 0;
            int pwrExhFanCntInt = 0;
            int phaseInt = 0;
            int primKwInt = 0;
            int secKwInt = 0;

            decimal tempDec;
            decimal hertzDec = 0;

            double tempDbl;
            double primHtgInputElecDbl = 0;
            double secHtgInputElecDbl = 0;
            double preHtgInputElecDbl = 0;

            bool calcElecHeatMCA = false;          
            bool heatPumpExist = false;
            bool airSrcHeatPumpExist = false;
            bool secHeatExist = false;
            bool secElecHeatExist = false;
            bool nonElecPrimNoSecHeat = false;
            bool nonElecPrimWithSecHeat = false;     

            DataTable dtModel = new DataTable();

            DateTime mfgDateDate;

            Byte dualPointPower = 0;           

            serialNoStr = "OA" + orderNumStr + "-" + orderLineStr + "-" + releaseNumStr;
            unitTypeStr = modelNumStr.Substring(2, 1);
            mfgDateStr = DateTime.Today.ToShortDateString();
            mfgDateDate = Convert.ToDateTime(mfgDateStr);

            switch (modelNumStr.Substring(8, 1)) //Voltage 
            {
                case "1":
                    voltageInt = 115;
                    hertzDec = 60;
                    phaseInt = 1;
                    elecRatingStr = "115/60/1";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "2":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "208/60/1";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "3":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                     elecRatingStr = "208/60/3";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "4":
                    voltageInt = 460;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "460/60/3";
                    operatingVoltsStr = "414 / 506";
                    break;
                case "5":
                    voltageInt = 575;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "575/60/3";
                    operatingVoltsStr = "517 / 633";
                    break;
                default:
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                     elecRatingStr = "208/60/3";
                    operatingVoltsStr = "187 / 253";
                    break;
            }

            voltageStr = voltageInt.ToString();
            testPresHighStr = "446";
            testPresLowStr = "238";

            if (unitTypeStr == "L" || unitTypeStr == "M" || unitTypeStr == "B" || unitTypeStr == "G")
            {
                unitTypeStr = "OALBG";
            }
            else
            {
                unitTypeStr = "OAU123DKN";
            }           

            // Digit 36 Dual Point Power
            if (unitTypeStr == "OALBG")
            {
                if (modelNumStr.Substring(35, 1) == "E" || modelNumStr.Substring(35, 1) == "F")
                {
                    dualPointPower = 1;
                }
                else
                {
                    dualPointPower = 0;
                }
            }
            else
            {
                if (modelNumStr.Substring(35, 1) == "D" || modelNumStr.Substring(35, 1) == "E")
                {
                    dualPointPower = 1;
                }
                else
                {
                    dualPointPower = 0;
                }
            }

            if (modelNumStr.Substring(22, 1) != "0")
            {
                secHeatExist = true;
            }

           
            // If Heat Pump then account  for PreHeat in MCA & MOP
            if (modelNumStr.Substring(3, 1) == "E" || modelNumStr.Substring(3, 1) == "F")
            {
                heatPumpExist = true;
                preHtgInputStr = "0";

                if ((modelNumStr.Substring(3, 1) == "E") &&
                       (modelNumStr.Substring(13, 1) == "1" || modelNumStr.Substring(3, 1) == "2" || modelNumStr.Substring(3, 1) == "4"))
                {
                    airSrcHeatPumpExist = true;
                }

                if (modelNumStr.Substring(19, 1) == "G" || modelNumStr.Substring(19, 1) == "H" || modelNumStr.Substring(19, 1) == "L" ||
                    modelNumStr.Substring(19, 1) == "N" || modelNumStr.Substring(19, 1) == "Q" || modelNumStr.Substring(19, 1) == "S" ||
                    modelNumStr.Substring(19, 1) == "T" || modelNumStr.Substring(19, 1) == "U" || modelNumStr.Substring(19, 1) == "V" ||
                    modelNumStr.Substring(19, 1) == "W" || modelNumStr.Substring(19, 1) == "Y" || modelNumStr.Substring(19, 1) == "Z")
                {
                    secHeatExist = true;
                    secElecHeatExist = true;
                    dtModel = objMain.GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);                   

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = objMain.stripKwFromDescription(kwStr, voltageInt, "REV5");
                        primKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";                        

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        preHtgInputElecDbl = tempDbl;
                        
                        heatingInputElecStr = preHtgInputElecDbl.ToString("f1");
                        calcElecHeatMCA = true;
                    }                    
                }                
            }

            if (modelNumStr.Substring(19, 1) == "A" || modelNumStr.Substring(19, 1) == "E" ||
                modelNumStr.Substring(19, 1) == "G" || modelNumStr.Substring(19, 1) == "T")  // Indirect Fired (IF) & DuelFuel IF/????
            {
                try
                {
                    heatTypeStr = "Indirect";                    

                    dtModel = objMain.GetRev5ModelNoDigitDesc(22, modelNumStr.Substring(21, 1), "NA", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        //mbhStr = drModel["DigitDescription"].ToString();
                        //mbhStr = mbhStr.Substring(0, (mbhStr.IndexOf("MBH") - 1));
                        //if_MaxHtgInputBTUHStr = mbhStr + "000";
                        //tempDec = decimal.Parse(if_MaxHtgInputBTUHStr) * 0.8m;
                        //if_HtgOutputBTUHStr = tempDec.ToString("f0");
                        //if_MinInputBTUStr = getMinInputBTU(mbhStr, modelNumStr.Substring(2, 1));
                       
                        mbhStr = drModel["DigitDescription"].ToString();
                        mbhStr = mbhStr.Substring(0, (mbhStr.IndexOf("MBH") - 1));
                        if_MaxHtgInputBTUHStr = mbhStr + "000";

                        double tmpKW = objMain.CalcKilowatts(if_MaxHtgInputBTUHStr);

                        tempDec = decimal.Parse(if_MaxHtgInputBTUHStr) * 0.8m;
                        if_MaxHtgInputBTUHStr = if_MaxHtgInputBTUHStr + "(" + tmpKW.ToString("f0") + "kW)";

                        if_HtgOutputBTUHStr = tempDec.ToString("f0");
                        tmpKW = objMain.CalcKilowatts(if_HtgOutputBTUHStr);
                        if_HtgOutputBTUHStr = if_HtgOutputBTUHStr + "(" + tmpKW.ToString("f0") + "kW)";

                        if_MinInputBTUStr = getMinInputBTU(mbhStr, modelNumStr.Substring(2, 1));                       
                    }

                    if_MaxEntStr = "2\"WC(498Pa)";
                    if_TempRiseStr = "10F to 100F/-12C to 38C";
                    maxOutletAirTempStr = "125 F/52 C";
                    if_MaxGasPressureStr = "14\"(3487Pa)";
                    if (modelNumStr.Substring(20, 1) == "2") // if Fuel Type is Liquid Propane
                    {
                        if_MinGasPressureStr = "11\"(2740Pa)";
                        if_ManifoldPressureStr = "10\"WC(2491Pa)";
                    }
                    else
                    {
                        if_MinGasPressureStr = "7\"WC(1744Pa)";
                        if_ManifoldPressureStr = "3.5\"WC(872Pa)";
                    }

                    if (voltageInt == 208 || voltageInt == 230 || voltageInt == 240)
                    {
                        primaryHtgInputStr = "6.25";
                    }
                    else if (voltageInt == 460)
                    {
                        primaryHtgInputStr = "3.13";
                    }
                    else if (voltageInt == 575)
                    {
                        primaryHtgInputStr = "2.5";
                    }          

                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 22 - " + f);
                }

                try
                {
                    // Get fuel type
                    dtModel = objMain.GetRev5ModelNoDigitDesc(21, modelNumStr.Substring(20, 1), "NA", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];
                        fuelType = drModel["DigitDescription"].ToString(); // Stores fuel type
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 20 - " + f);
                }

                //try
                //{
                //    if (modelNumStr.Substring(19, 1) == "G" || modelNumStr.Substring(19, 1) == "T")
                //    {
                //        dtModel = objMain.GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);

                //        if (dtModel.Rows.Count > 0)
                //        {
                //            DataRow drModel = dtModel.Rows[0];
                //            kwStr = drModel["DigitDescription"].ToString();

                //            kwStr = objMain.stripKwFromDescription(kwStr);
                            
                //            kwStr = kwStr + "000";
                //            tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                //            tempDbl = tempDbl / Math.Sqrt(3);
                //            secHtgInputElecDbl = tempDbl;

                //            secHtgInputStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f1");
                //        }
                //        else
                //        {
                //            secHtgInputStr = "0";
                //        }
                //    }
                //}
                //catch (Exception f)
                //{
                //    MessageBox.Show("Error Reading ModelNumDesc Table for digit 23 - " + f);
                //}
            }
            else if (modelNumStr.Substring(19, 1) == "C" || modelNumStr.Substring(19, 1) == "D" ) // Electric, Electric SCR 
            {
                //heatingInputElecStr = (((kW * 1000) / voltage) / square root of 3)
                heatTypeStr = "ELEC";
                calcElecHeatMCA = true;
                try
                {
                    dtModel = objMain.GetRev5ModelNoDigitDesc(22, modelNumStr.Substring(21, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = objMain.stripKwFromDescription(kwStr, voltageInt, "REV5");
                        primKwInt = Int32.Parse(kwStr);
      
                        kwStr = kwStr + "000";
                        
                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        heatingInputElecStr = tempDbl.ToString("f1");
                        primHtgInputElecDbl = tempDbl;
                        maxOutletAirTempStr = "90 F/32 C";
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 22 - " + f);
                }
            }
            else if (modelNumStr.Substring(19, 1) == "B")  // Direct Fired
            {
                heatTypeStr = "Direct";

                df_TempRiseStr = "100 F/38 C";
                df_MaxGasPressureStr = "14";
                df_MinHeatingInputStr = "22,000";

                df_MaxPressureDropStr = "";
                df_MinPressureDropStr = "";
                df_StaticPressureStr = "";
                df_CutOutTempStr = "";

                if (modelNumStr.Substring(20, 1) == "1") // Natural Gas
                {
                    df_MinGasPressureStr = "7";
                    df_ManifoldPressureStr = "3.5";
                }
                else   // Liquid Propane
                {
                    df_MinGasPressureStr = "10";
                    df_ManifoldPressureStr = "2";
                }

                if (modelNumStr.Substring(22, 1) == "A")
                {
                    df_MaxHtgInputBTUHStr = "330,000";
                }
                else if (modelNumStr.Substring(22, 1) == "B")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                }
                else if (modelNumStr.Substring(22, 1) == "C")
                {
                    df_MaxHtgInputBTUHStr = "600,000";
                }
                else if (modelNumStr.Substring(22, 1) == "D")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                    df_MinHeatingInputStr = "40,000";
                }
                else if (modelNumStr.Substring(22, 1) == "E")
                {
                    df_MaxHtgInputBTUHStr = "900,000";
                    df_MinHeatingInputStr = "40,000";
                }
            }
            else if (modelNumStr.Substring(19, 1) == "H" || modelNumStr.Substring(19, 1) == "N" ||
                     modelNumStr.Substring(19, 1) == "U" || modelNumStr.Substring(19, 1) == "W")  // Dual Heat Type Electric\Elctric
            {
                //heatingInputElecStr = (((kW * 1000) / voltage) / square root of 3)
                heatTypeStr = "ELEC";
                calcElecHeatMCA = true;
                secHeatExist = true;
                secElecHeatExist = true;
                nonElecPrimNoSecHeat = false;
                nonElecPrimWithSecHeat = false;

                try
                {
                    dtModel = objMain.GetRev5ModelNoDigitDesc(22, modelNumStr.Substring(21, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = objMain.stripKwFromDescription(kwStr, voltageInt, "REV5");
                        primKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";                        

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        primHtgInputElecDbl = tempDbl;
                        maxOutletAirTempStr = "90 F/32 C";
                    }

                    dtModel = objMain.GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = objMain.stripKwFromDescription(kwStr, voltageInt, "REV5");
                        secKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";                        

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        secHtgInputElecDbl = tempDbl;
                        secHtgInputStr = secHtgInputElecDbl.ToString("f1");
                    }

                    //heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f1");

                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 23 - " + f);
                }
            }
            else if (modelNumStr.Substring(19, 1) == "J" || modelNumStr.Substring(19, 1) == "P" ||
                     modelNumStr.Substring(19, 1) == "Q" || modelNumStr.Substring(19, 1) == "Y")  // Hot Water
            {
                heatTypeStr = "HotWater";
                flowRateStr = this.lbFlowRate.Text;
                enteringTempStr = this.lbEnteringTemp.Text;

                if (enteringTempStr.Length > 0 && enteringTempStr.IndexOf("F") == -1)
                {
                    double tempCel = (Int32.Parse(enteringTempStr) - 32) / 1.8;
                    enteringTempStr = enteringTempStr + " F/" + tempCel.ToString("f0") + " C";
                }                

                if (modelNumStr.Substring(19, 1) == "Q" || modelNumStr.Substring(19, 1) == "Y")
                {
                    secHeatExist = true;
                    secElecHeatExist = true;
                }
            }
            else if (modelNumStr.Substring(19, 1) == "K" || modelNumStr.Substring(19, 1) == "R" ||
                     modelNumStr.Substring(19, 1) == "S" || modelNumStr.Substring(19, 1) == "Z")  // Steam
            {
                heatTypeStr = "Steam";
            }

            if (heatTypeStr != "ELEC")
            {
                if (modelNumStr.Substring(22, 1) == "0")
                {
                    nonElecPrimNoSecHeat = true;
                }
                else
                {
                    secHeatExist = true;
                    secElecHeatExist = true;
                    calcElecHeatMCA = true;
                    nonElecPrimWithSecHeat = true;

                    dtModel = objMain.GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel;
                        drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = objMain.stripKwFromDescription(kwStr, voltageInt, "REV5");
                        secKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                        tempDbl = tempDbl / Math.Sqrt(3);
                        secHtgInputElecDbl = tempDbl;
                        secHtgInputStr = secHtgInputElecDbl.ToString("f1");
                    }
                }
            }
            

            // Determine the number of Compressor for this unit and record the compressor data.              

            List<Compressor> CompList = new List<Compressor>();
            compQtyStr = "0";
            compTypeStr = "";

            int compNum = 0;
            string taco = "";

            foreach (DataGridViewRow dgvr in dgvBOM.Rows)
            {
                //ruleHeadID = dgvr.Cells["RuleHeadID"].Value.ToString();

                //if (ruleHeadID == "")
                //{
                ruleHeadID = "0";
                //}

                partCategoryStr = dgvr.Cells["PartCategory"].Value.ToString();
                partNumStr = dgvr.Cells["PartNum"].Value.ToString();
                revisionNum = dgvr.Cells["RevisionNum"].Value.ToString();          

                if (partCategoryStr != "")
                {
                    currentPartTypeStr = partCategoryStr;
                }

                motorTypeStr = dgvr.Cells["PartType"].Value.ToString();
               
                if ((currentPartTypeStr == "Compressor") || (currentPartTypeStr.Contains("DigitalScroll")) || (currentPartTypeStr.Contains("TandemComp")))
                {
                    if (partNumStr.Contains("RFGASM") == true)
                    {
                        string rfgQtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                        string immedParent = partNumStr;

                        DataTable dtComp = objMain.GetPartBOM(partNumStr, revisionNum);

                        foreach (DataRow drComp in dtComp.Rows)
                        {
                            partNumStr = drComp["MtlPartNum"].ToString();
                            qtyStr = drComp["QtyPer"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                compQtyStr = qtyStr;
                            }

                            decimal compQtyPerDec = decimal.Parse(compQtyStr) * decimal.Parse(rfgQtyStr);
                            compQtyStr = compQtyPerDec.ToString("f0");
                            compTypeStr = currentPartTypeStr;
                            objMain.getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, "REV5", ruleHeadID);

                            if (decimal.Parse(compRLA_VoltsStr) > 0)
                            {
                                CompList.Add(new Compressor("", partNumStr, parentPartNumStr, immedParent, compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                            }
                        }
                    }
                    else
                    {
                        qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                        if (qtyStr.IndexOf(".") > 0)
                        {
                            compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        }
                        else
                        {
                            compQtyStr = qtyStr;
                        }
                        compTypeStr = currentPartTypeStr;
                        objMain.getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, "REV5", ruleHeadID);

                        if (decimal.Parse(compRLA_VoltsStr) > 0)
                        {
                            CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                        }
                    }
                    //parentPartNumStr = dgvr.Cells["ParentPartNum"].Value.ToString();
                    //qtyStr = dgvr.Cells["ReqQty"].Value.ToString();

                    //if (qtyStr.IndexOf(".") > 0)
                    //{
                    //    compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    //}
                    //else
                    //{
                    //    compQtyStr = qtyStr;
                    //}
                    
                    //compTypeStr = "Compressor";

                    //objMain.getCompRLA_Data(partNumStr, voltageStr, out compRLA_VoltsStr, out compLRAStr, "REV5", ruleHeadID);

                    //if (decimal.Parse(compRLA_VoltsStr) > 0)
                    //{
                    //    ++compNum;
                    //    CompList.Add(new Compressor(compNum.ToString(), partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                    //}                    
                }
                else if (motorTypeStr == "Condensor")
                {
                    qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanCondQtyStr = qtyStr;
                    }                    

                    objMain.getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanCondFLAStr, out fanCondHPStr, partNumStr);

                    if (fanCondFLAStr != "0")
                    {                        
                        fanCondFLAVoltStr = fanCondFLAStr + "-" + voltageStr;
                        fanCondPHStr = phaseInt.ToString();
                    }
                    
                }
                else if (motorTypeStr == "Indoor FAN" || motorTypeStr == "SupplyFan")
                {
                    qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanEvapQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanEvapQtyStr = qtyStr;
                    }                   

                    objMain.getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanEvapFLAStr, out fanEvapHPStr, partNumStr);

                    if (fanEvapFLAStr != "0")
                    {
                        fanEvapFLAVoltStr = fanEvapFLAStr + "-" + voltageStr;
                        fanEvapPHStr = phaseInt.ToString();
                    }                                     

                    ++evapFanCntInt;
                }
                else if (motorTypeStr == "Powered Exhaust" || motorTypeStr == "PoweredExhaust")
                {

                    qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanPWRExhQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanPWRExhQtyStr = qtyStr;
                    }                  

                    objMain.getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanPWRExhFLAStr, out fanPWRExhHPStr, partNumStr);

                    if (fanPWRExhFLAStr != "0")
                    {
                        fanPWRExhFLAVoltStr = fanPWRExhFLAStr + "-" + voltageStr;
                        fanPWRExhPHStr = phaseInt.ToString();
                    }                    

                    ++pwrExhFanCntInt;
                }
                else if (motorTypeStr == "ERV")
                {
                    qtyStr = dgvr.Cells["ReqQty"].Value.ToString();

                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanERVQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanERVQtyStr = qtyStr;
                    }                    

                    objMain.getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanERVFLAStr, out fanERVHPStr, partNumStr);

                    if (fanERVFLAStr != "0")
                    {
                        fanERVFLAVoltStr = fanERVFLAStr + "-" + voltageStr;
                        fanERVPHStr = phaseInt.ToString();
                    }                                        
                }
                //else if (partNumStr.ToUpper().Contains("VELHTR") || partNumStr.ToUpper().Contains("VGSBRN"))
                //{
                //    if (heatTypeStr != "ELEC" && secHeatExist == false)
                //    {
                //        DataTable dtFurnace = objMain.GetFurnaceDataDTL(partNumStr);
                //        if (dtFurnace.Rows.Count > 0)
                //        {
                //            DataRow drFurn = dtFurnace.Rows[0];
                //            primaryHtgInputStr = drFurn["Amps"].ToString();
                //        }
                //    }
                //}
            }

            //if ((heatTypeStr == "Indirect" && modelNumStr.Substring(4, 3) == "000") || (nonElecPrimWithSecHeat == true))
            //{
            //    int mbhInt = Int32.Parse(mbhStr);

            //    if (mbhInt > 0)
            //    {
            //        if (mbhInt < 225)
            //        {
            //            primaryHtgInputStr = "1.69";
            //        }
            //        else if (mbhInt < 425)
            //        {
            //            primaryHtgInputStr = "3.3";
            //        }
            //        else if (mbhInt < 825)
            //        {
            //            primaryHtgInputStr = "6.14";
            //        }
            //        else if (mbhInt < 1225)
            //        {
            //            primaryHtgInputStr = "10";
            //        }
            //    }
            //    else
            //    {
            //        primaryHtgInputStr = "0";
            //    }
            //}

            //objMain.calcAmpacityValues(out minCKTAmpStr, out mfsMCBStr, out rawMopStr, CompList, fanCondFLAStr, fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr,
            //                           fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString(), preHtgInputStr);

            //if (calcHeatMCA == true)
            //{
            //    objMain.calcElecAmpacityValues(out tmp_MinCKTAmpStr, out tmp_MFSMCBStr, fanEvapFLAStr, fanEvapQtyStr,
            //        fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageStr, heatingInputElecStr);

            //    if (double.Parse(tmp_MinCKTAmpStr) > double.Parse(minCKTAmpStr))
            //    {
            //        minCKTAmpStr = tmp_MinCKTAmpStr;

            //        if (double.Parse(tmp_MFSMCBStr) > double.Parse(mfsMCBStr))
            //        {
            //            mfsMCBStr = objMain.roundElecMOP(double.Parse(tmp_MinCKTAmpStr));
            //        }

            //        rawMopStr = "NA";
            //    }
            //}

            mcaStr = "0";

            objMain.calcAmpacityValuesFansOnly(out fansOnlyFLAStr, out fansOnlyMcaStr, out fansOnlyRawMopStr, fanEvapFLAStr,
                                               fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr,
                                               voltageInt.ToString());

            if (nonElecPrimNoSecHeat == true) // If the unit has non electric primary heat and no secondary heat then add the additional values to MCA & MOP
            {                                 // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                nonElecPrimNoSecMcaStr = (double.Parse(fansOnlyMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                nonElecPrimNoSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            }
            else if (nonElecPrimWithSecHeat == true)  // If the unit has non electric primart heat and has secondary elec heat then add the additional values to MCA & MOP
            {                                         // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                if (secKwInt >= 50)
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + secHtgInputElecDbl).ToString("f1");
                }
                else
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + (secHtgInputElecDbl * 1.25)).ToString("f1");
                }
                nonElecPrimWithSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                //nonElecPrimWithSecMopStr = ((1.25 * (double.Parse(fansOnlyRawMopStr)) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl)).ToString("f1");
            }

            if (modelNumStr.Substring(4, 3) != "000") // if the unit has cooling then calculate the cooling MCA & MOP
            {                                    // B. DX Cool (Horizon & Viking)
                objMain.calcAmpacityValues(out coolingMcaStr, out coolingMopStr, out coolingRawMopStr, CompList, fanCondFLAStr, 
                                           fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr,
                                           fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString());
            }

            if (heatPumpExist == true) // if the unit has a heat pump and has secondary electric heating then add the additional values to MCA & MOP
            {                          // E. DX Heat (Air or Water Source Heat Pump) Normal Operation (Horizon)
                if (airSrcHeatPumpExist == true)  // F.	DX Heat (Air Source Heat Pump) Defrost Mode Operation (Horizon)
                {
                    if (nonElecPrimWithSecHeat == true) // If the unit has non electric primary heat and has secondary heat then add the additional values to MCA & MOP
                    {
                        if (secElecHeatExist == false) //Mode F-a
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                        }
                        else // Secondary heat exist   Mode F-b
                        {
                            if (secKwInt >= 50)
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                            }
                            else
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                            }
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                        }
                    }
                    else if (heatTypeStr == "ELEC") // Mode F-c Elec primary heat; no secondary elec heat
                    {
                        if (primKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString();
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * double.Parse(primaryHtgInputStr))).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                    }
                }
                else
                {
                    if (secElecHeatExist == true) //Mode E-a
                    {
                        if (secKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + secHtgInputElecDbl).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + secHtgInputElecDbl).ToString("f1");
                    }
                    // If no secondary elec heat then just use the coolingMcaStr & coolingRawMopStr values
                    // Mode E-b
                }
                // Else if no secondary elec heat the coolingMcaStr doesn't change from what was calculate earlier.
            }

            if (calcElecHeatMCA == true) // if the unit contains some type of electric heating
            {                            // C.	Primary Heat: Electric (Viking and Horizon)
                objMain.calcElecAmpacityValues(out elecMcaStr, out elecRawMopStr, primKwInt, secKwInt, fanEvapFLAStr,
                                               fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, 
                                               voltageInt.ToString(), primHtgInputElecDbl, secHtgInputElecDbl);
            }

            //objMain.calcAmpacityValuesFansOnly(out fansOnlyFLAStr, out fansOnlyMcaStr, out fansOnlyRawMopStr, fanEvapFLAStr, fanEvapQtyStr, fanERVFLAStr,
            //                            fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString());

            //if (nonElecPrimNoSecHeat == true) // If the unit has non electric primart heat and no secondary heat then add the additional values to MCA & MOP
            //{
            //    nonElecPrimNoSecMcaStr = (double.Parse(fansOnlyMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            //    nonElecPrimNoSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            //}
            //else if (nonElecPrimWithSecHeat == true)  // If the unit has non electric primart heat and has secondary elec heat then add the additional values to MCA & MOP
            //{
            //    nonElecPrimWithSecMcaStr = ((double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl) * 1.25).ToString("f1");
            //    nonElecPrimWithSecMopStr = ((double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl)).ToString("f1");
            //}

            //if (modelNumStr.Substring(4, 3) != "000") // if the unit has cooling then calculate the cooling MCA & MOP
            //{
            //    objMain.calcAmpacityValues(out coolingMcaStr, out coolingMopStr, out coolingRawMopStr, CompList, fanCondFLAStr, fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr,
            //                       fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString());
            //}

            //if (heatPumpExist == true) // if the unit has a heat pump and has secondary electric heating then add the additional values to MCA & MOP
            //{
            //    if (secElecHeatExist == true)
            //    {
            //        if (secKwInt >= 50)
            //        {
            //            coolingMcaStr = (double.Parse(coolingMcaStr) + secHtgInputElecDbl).ToString("f1");
            //        }
            //        else
            //        {
            //            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
            //        }
            //        coolingRawMopStr = (double.Parse(coolingRawMopStr) + secHtgInputElecDbl).ToString("f1");

            //    }

            //    if (airSrcHeatPumpExist == true)  // F.	DX Heat (Air Source Heat Pump) Defrost Mode Operation (Horizon)
            //    {
            //        if (nonElecPrimWithSecHeat == true) // If the unit has non electric primary heat and has secondary heat then add the additional values to MCA & MOP
            //        {
            //            if (secElecHeatExist == false)
            //            {
            //                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            //                coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            //            }
            //            else // Secondary heat exist
            //            {
            //                if (secKwInt >= 50)
            //                {
            //                    coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
            //                }
            //                else
            //                {
            //                    coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
            //                }
            //                coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            //            }
            //        }
            //        else if (heatTypeStr == "ELEC")
            //        {

            //            if (primKwInt >= 50)
            //            {
            //                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString();
            //            }
            //            else
            //            {
            //                coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * double.Parse(primaryHtgInputStr))).ToString("f1");
            //            }
            //            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            //        }
            //    }
            //}

            //if (calcElecHeatMCA == true) // if the unit contains some type of electric heating
            //{
            //    objMain.calcElecAmpacityValues(out elecMcaStr, out elecRawMopStr, primKwInt, secKwInt, fanEvapFLAStr, 
            //                                   fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, 
            //                                   voltageInt.ToString(), primHtgInputElecDbl, secHtgInputElecDbl);
            //}

            // determine which MCA value is the largest 
            if (double.Parse(fansOnlyMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(elecMcaStr) &&
                double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = fansOnlyMcaStr;
            }
            else if (double.Parse(coolingMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = coolingMcaStr;
            }
            else if (double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = nonElecPrimNoSecMcaStr;
            }
            else if (double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr))
            {
                mcaStr = nonElecPrimWithSecMcaStr;
            }
            else
            {
                mcaStr = elecMcaStr;
            }

            // determine which MOP value is the largest 
            if (double.Parse(fansOnlyRawMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(elecRawMopStr) &&
               double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = fansOnlyRawMopStr;
            }
            else if (double.Parse(coolingRawMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = coolingRawMopStr;
            }
            else if (double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = nonElecPrimNoSecMopStr;
            }
            else if (double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(nonElecPrimNoSecMopStr))
            {
                rawMopStr = nonElecPrimWithSecMopStr;
            }
            else
            {
                rawMopStr = elecRawMopStr;
            }

            // Do the rounding for both electrical MCA & non electrical MCA
            if (mcaStr != "0")
            {
                elecMopStr = objMain.roundElecMOP(double.Parse(mcaStr)); // Electrical MCA rounding
            }
            else
            {
                elecMopStr = "0";
            }
            mopStr = objMain.roundMOP(double.Parse(rawMopStr)); // Non-Electrical MCA rounding

            if (double.Parse(elecMopStr) > double.Parse(mopStr)) // Use the larger of the 2 values
            {
                mopStr = elecMopStr;
            }          

            lbMCA.Text = mcaStr;
            lbMOP.Text = mopStr;

            //if (modelNumStr.Substring(19, 1) == "G")
            //{
            //    objMain.calcElecAmpacityValues(out tmp_MinCKTAmpStr, out tmp_MFSMCBStr, fanEvapFLAStr, fanEvapQtyStr,
            //        fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageStr, secHtgInputStr);

            //    if (double.Parse(tmp_MinCKTAmpStr) > double.Parse(minCKTAmpStr))
            //    {
            //        minCKTAmpStr = tmp_MinCKTAmpStr;
            //        mfsMCBStr = objMain.roundElecMOP(double.Parse(tmp_MinCKTAmpStr));
            //    }
            //}

            string digit3 = modelNumStr.Substring(2, 1);
            string digit4 = modelNumStr.Substring(3, 1);
            string digit567 = modelNumStr.Substring(4, 3);
            string digit9 = modelNumStr.Substring(8, 1);
            string digit11 = modelNumStr.Substring(10, 1);
            string digit12 = modelNumStr.Substring(11, 1);
            string digit13 = modelNumStr.Substring(12, 1);
            string digit14 = modelNumStr.Substring(13, 1);

            DataTable dtCCD = objMain.GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow dr = dtCCD.Rows[0];

                comp1QtyStr = "1";
                comp1RLA_VoltsStr = dr["Comp1_1_RLA"].ToString() + "-" + voltageStr;
                comp1LRAStr = dr["Comp1_1_LRA"].ToString();
                comp1PhaseStr = "3";

                if (dr["Circuit1_2_Compressor"].ToString() != "NA")
                {
                    comp2QtyStr = "1";
                    comp2RLA_VoltsStr = dr["Comp1_2_RLA"].ToString() + "-" + voltageStr;
                    comp2LRAStr = dr["Comp1_2_LRA"].ToString();
                    comp2PhaseStr = "3";
                }

                if (dr["Circuit2_1_Compressor"].ToString() != "NA")
                {
                    comp4QtyStr = "1";
                    comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    comp4LRAStr = dr["Comp2_1_LRA"].ToString();
                    comp4PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp2QtyStr = "1";
                    //    comp2RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp2LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp2PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                }

                if (dr["Circuit2_2_Compressor"].ToString() != "NA")
                {
                    comp5QtyStr = "1";
                    comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    comp5LRAStr = dr["Comp2_2_LRA"].ToString();
                    comp5PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                }                

                if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = dr["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(dr["Circuit1_Charge"].ToString()) + Decimal.Parse(dr["Circuit1_ReheatCharge"].ToString())).ToString();
                }

                if (dr["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = dr["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(dr["Circuit2_Charge"].ToString()) + Decimal.Parse(dr["Circuit2_ReheatCharge"].ToString())).ToString();
                    }
                }
            }

            lbCir1Chrg.Text = circuit1ChargeStr + " lbs";
            lbCir2Chrg.Text = circuit2ChargeStr + " lbs";

            objMain.JobNum = jobNumStr;
            objMain.OrderNum = Int32.Parse(orderNumStr);
            objMain.OrderLine = Int32.Parse(orderLineStr);
            objMain.ReleaseNum = Int32.Parse(releaseNumStr);
            objMain.ModelNo = modelNumStr;
            objMain.SerialNo = serialNoStr;
            objMain.MfgDate = mfgDateStr;
            objMain.UnitType = unitTypeStr;
            objMain.ElecRating = elecRatingStr;
            objMain.OperVoltage = operatingVoltsStr;
            objMain.DualPointPower = dualPointPower;
            objMain.MinCKTAmp = mcaStr;
            objMain.MinCKTAmp2 = minCKTAmpStr2;
            objMain.MfsMcb = mopStr;
            objMain.MfsMcb2 = mfsMCBStr2;
            objMain.MOP = rawMopStr;
            objMain.HeatingType = heatTypeStr;
            objMain.Voltage = voltageStr;
            objMain.TestPressureHigh = testPresHighStr;
            objMain.TestPressureLow = testPresLowStr;
            objMain.SecondaryHtgInput = secHtgInputStr;
            objMain.HeatingInputElectric = heatingInputElecStr;
            objMain.FuelType = fuelType;
            objMain.Comp1Qty = comp1QtyStr;
            objMain.Comp2Qty = comp2QtyStr;
            objMain.Comp3Qty = comp3QtyStr;
            objMain.Comp4Qty = comp4QtyStr;
            objMain.Comp5Qty = comp5QtyStr;
            objMain.Comp6Qty = comp6QtyStr;
            objMain.Comp1Phase = comp1PhaseStr;
            objMain.Comp2Phase = comp2PhaseStr;
            objMain.Comp3Phase = comp3PhaseStr;
            objMain.Comp4Phase = comp4PhaseStr;
            objMain.Comp5Phase = comp5PhaseStr;
            objMain.Comp6Phase = comp6PhaseStr;
            objMain.Comp1RLA_Volts = comp1RLA_VoltsStr;
            objMain.Comp2RLA_Volts = comp2RLA_VoltsStr;
            objMain.Comp3RLA_Volts = comp3RLA_VoltsStr;
            objMain.Comp4RLA_Volts = comp4RLA_VoltsStr;
            objMain.Comp5RLA_Volts = comp5RLA_VoltsStr;
            objMain.Comp6RLA_Volts = comp6RLA_VoltsStr;
            objMain.Comp1LRA = comp1LRAStr;
            objMain.Comp2LRA = comp2LRAStr;
            objMain.Comp3LRA = comp3LRAStr;
            objMain.Comp4LRA = comp4LRAStr;
            objMain.Comp5LRA = comp5LRAStr;
            objMain.Comp6LRA = comp6LRAStr;
            objMain.Circuit1Charge = circuit1ChargeStr;
            objMain.Circuit2Charge = circuit2ChargeStr;
            objMain.FanCondQty = fanCondQtyStr;
            objMain.FanEvapQty = fanEvapQtyStr;
            objMain.FanErvQty = fanERVQtyStr;
            objMain.FanPwrExhQty = fanPWRExhQtyStr;
            objMain.FanCondPhase = fanCondPHStr;
            objMain.FanEvapPhase = fanEvapPHStr;
            objMain.FanErvPhase = fanERVPHStr;
            objMain.FanPwrExhPhase = fanPWRExhPHStr;
            objMain.FanCondFLA = fanCondFLAVoltStr;
            objMain.FanEvapFLA = fanEvapFLAVoltStr;
            objMain.FanErvFLA = fanERVFLAVoltStr;
            objMain.FanPwrExhFLA = fanPWRExhFLAVoltStr;
            objMain.FanCondHP = fanCondHPStr;
            objMain.FanEvapHP = fanEvapHPStr;
            objMain.FanErvHP = fanERVHPStr;
            objMain.FanPwrExhHP = fanPWRExhHPStr;
            objMain.FlowRate = flowRateStr;
            objMain.EnteringTemp = enteringTempStr;
            objMain.OperatingPressure = operatingPressureStr;
            objMain.WaterGlycol = waterGlycol;
            objMain.DF_StaticPressure = df_StaticPressureStr;
            objMain.DF_MaxHtgInputBTUH = df_MaxHtgInputBTUHStr;
            objMain.DF_MinHeatingInput = df_MinHeatingInputStr;
            objMain.DF_MinPressureDrop = df_MinPressureDropStr;
            objMain.DF_MaxPressureDrop = df_MaxPressureDropStr;
            objMain.DF_ManifoldPressure = df_ManifoldPressureStr;
            objMain.DF_CutoutTemp = df_CutOutTempStr;
            objMain.DF_MinGasPressure = df_MinGasPressureStr;
            objMain.DF_MaxGasPressure = df_MaxGasPressureStr;
            objMain.DF_TempRise = df_TempRiseStr;
            objMain.IN_MaxHtgInputBTUH = if_MaxHtgInputBTUHStr;
            objMain.IN_HeatingOutputBTUH = if_HtgOutputBTUHStr;
            objMain.IN_MinInputBTU = if_MinInputBTUStr;
            objMain.IN_MaxExt = if_MaxEntStr;
            objMain.IN_TempRise = if_TempRiseStr;
            objMain.IN_MaxOutAirTemp = maxOutletAirTempStr;
            objMain.IN_MaxGasPressure = if_MaxGasPressureStr;
            objMain.IN_MinGasPressure = if_MinGasPressureStr;
            objMain.IN_ManifoldPressure = if_ManifoldPressureStr;
            objMain.ModelNoVerifiedBy = "OAU_Config";
            objMain.French = "French";
            objMain.WhseCode = WarehouseCode;

            try
            {
                objMain.InsertETL_Oau();
            }
            catch (Exception f)
            {
                MessageBox.Show("Error writing to the R6_OA_EtlOAU Table - " + f);
            }
        }

        private void  writeRev6EtlOAUStickerData(string jobNumStr, string orderNumStr, string orderLineStr, string releaseNumStr, string modelNumStr)
        {
            string partNumStr;
            string parentPartNumStr;
            string partCategoryStr;
            string motorTypeStr;
            string voltageStr = "";
            string unitTypeStr;
            string compQtyStr = "";
            string compTypeStr = "";
            string compLRAStr = "";           
            string compRLA_VoltsStr = "";            
            string comp1RLA_VoltsStr = "";
            string comp2RLA_VoltsStr = "";
            string comp3RLA_VoltsStr = "";
            string comp4RLA_VoltsStr = "";
            string comp5RLA_VoltsStr = "";
            string comp6RLA_VoltsStr = "";
            string comp1QtyStr = "";
            string comp2QtyStr = "";
            string comp3QtyStr = "";
            string comp4QtyStr = "";
            string comp5QtyStr = "";
            string comp6QtyStr = "";
            string comp1PhaseStr = "";
            string comp2PhaseStr = "";
            string comp3PhaseStr = "";
            string comp4PhaseStr = "";
            string comp5PhaseStr = "";
            string comp6PhaseStr = "";
            string comp1LRAStr = "";
            string comp2LRAStr = "";
            string comp3LRAStr = "";
            string comp4LRAStr = "";
            string comp5LRAStr = "";
            string comp6LRAStr = "";
            string circuit1ChargeStr = "";
            string circuit2ChargeStr = "";            
            string fanCondQtyStr = "0";
            string fanCondPHStr = "";
            string fanCondFLAStr = "0";
            string fanCondFLAVoltStr = "";
            string fanCondHPStr = "";
            string fanEvapQtyStr = "0";
            string fanEvapPHStr = "";
            string fanEvapFLAStr = "0";
            string fanEvapFLAVoltStr = "";
            string fanEvapHPStr = "";
            string fanERVQtyStr = "0";
            string fanERVPHStr = "";
            string fanERVFLAStr = "0";
            string fanERVFLAVoltStr = "";
            string fanERVHPStr = "";
            string fanPWRExhQtyStr = "0";
            string fanPWRExhPHStr = "";
            string fanPWRExhFLAStr = "0";
            string fanPWRExhFLAVoltStr = "";
            string fanPWRExhHPStr = "";
            string operatingPressureStr = "";
            string waterGlycol = "";
            string df_StaticPressureStr = "";
            string df_MaxHtgInputBTUHStr = "";
            string df_MinHeatingInputStr = "";
            string df_TempRiseStr = "";
            string df_MaxGasPressureStr = "";
            string df_MinGasPressureStr = "";
            string df_MaxPressureDropStr = "";
            string df_MinPressureDropStr = "";
            string df_ManifoldPressureStr = "";
            string df_CutOutTempStr = "";
            string if_MaxHtgInputBTUHStr = "";
            string if_HtgOutputBTUHStr = "";
            string if_MinInputBTUStr = "";
            string if_MaxEntStr = "";
            string if_TempRiseStr = "";
            string if_MaxGasPressureStr = "";
            string if_MinGasPressureStr = "";
            string if_ManifoldPressureStr = "";
            string maxOutletAirTempStr = "";
            string secHtgInputStr = "";
            string elecMcaStr = "0";
            string elecMopStr = "0";
            string elecRawMopStr = "0";
            string coolingMcaStr = "0";
            string coolingMopStr = "0";
            string coolingRawMopStr = "0";
            string fansOnlyMcaStr = "0";
            string fansOnlyFLAStr = "0";
            string fansOnlyRawMopStr = "0";
            string nonElecPrimNoSecFlaStr = "0";
            string nonElecPrimNoSecMcaStr = "0";
            string nonElecPrimNoSecMopStr = "0";
            string nonElecPrimWithSecFlaStr = "0";
            string nonElecPrimWithSecMcaStr = "0";
            string nonElecPrimWithSecMopStr = "0";
            string heatPumpNoSecFlaStr = "0";
            string heatPumpNoSecMcaStr = "0";
            string heatPumpNoSecMopStr = "0";
            string heatPumpWithSecFlaStr = "0";
            string heatPumpWithSecMcaStr = "0";
            string heatPumpWithSecMopStr = "0";
            string mcaStr = "0";
            string minCKTAmpStr = "";
            string mfsMCBStr = "";
            string mopStr = "N/A";
            string elecRatingStr = "";
            string operatingVoltsStr = "";
            string testPresHighStr = "";
            string testPresLowStr = "";
            string mbhStr = "0";            
            string qtyStr;
            string heatingInputElecStr = "0";
            string kwStr = "0";
            string tmp_MinCKTAmpStr = "";
            string tmp_MFSMCBStr = "";
            string rawMopStr = "";            
            string mfgDateStr = "";
            string currentPartTypeStr = "";
            string flowRateStr = "";
            string enteringTempStr = "";
            string heatTypeStr = "";
            string ruleHeadID = "";
            string ruleBatchID = "";
            string serialNoStr =  "";
            string fuelType = "";
            string primaryHtgInputStr = "0";
            string revisionNum = "";
            string revType = "REV6";

            //todo: needs to be looked into
            string minCKTAmpStr2 = "";
            string mfsMCBStr2 = "";

           
            int voltageInt = 0;            
            int evapFanCntInt = 0;
            int pwrExhFanCntInt = 0;
            int phaseInt = 0;
            int primKwInt = 0; 
            int secKwInt = 0;
            
            decimal tempDec;            
            decimal hertzDec = 0;

            double tempDbl;
            double primHtgInputElecDbl = 0;
            double secHtgInputElecDbl = 0;

            bool calcElecHeatMCA = false;
            //bool calcCoolMCA = false;
            bool heatPumpExist = false;
            bool secHeatExist = false;
            bool airSrcHeatPumpExist = false;
            bool secElecHeatExist = false;
            bool nonElecPrimNoSecHeat = false;
            bool nonElecPrimWithSecHeat = false;    

            DateTime mfgDateDate;
            
            Byte dualPointPower = 0;            

            serialNoStr = "OA" + orderNumStr + "-" + orderLineStr + "-" + releaseNumStr;

            unitTypeStr = modelNumStr.Substring(0, 3);

            if (unitTypeStr.StartsWith("OA") == true)
            {
                revType = "REV6";
            }
            else
            {
                revType = "VKG";
            }

            mfgDateStr = DateTime.Today.ToShortDateString();
            mfgDateDate = Convert.ToDateTime(mfgDateStr);
           
            switch (modelNumStr.Substring(8, 1)) //Voltage 
            {
                case "1":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "208-230/60/3";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "2":
                    voltageInt = 230;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "230-240/60/3";
                    break;
                case "3":
                    voltageInt = 460;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "460/60/3";
                    operatingVoltsStr = "414 / 506";
                    break;
                case "4":
                    voltageInt = 575;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "575/60/3";
                    operatingVoltsStr = "517 / 633";
                    break;
                case "5":
                    voltageInt = 115;
                    hertzDec = 60;
                    phaseInt = 1;
                    break;
                case "6":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 1;
                    break;
                case "7":
                    voltageInt = 208;
                    hertzDec = 50;
                    phaseInt = 3;
                    break;
                case "8":
                    voltageInt = 380;
                    hertzDec = 50;
                    phaseInt = 3;
                    break;
                case "9":
                    voltageInt = 208;
                    hertzDec = 50;
                    phaseInt = 1;
                    break;
                default:
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
            }

            voltageStr = voltageInt.ToString();
            testPresHighStr = "446";
            testPresLowStr = "238";

            // If Digit 41 equal G, H, or J then unit has Dual Point Power
            if (modelNumStr.Substring(40, 1) == "G" || modelNumStr.Substring(40, 1) == "H" || modelNumStr.Substring(40, 1) == "J")
            {
                dualPointPower = 1;
            }
            else
            {
                dualPointPower = 0;
            }            

            if (modelNumStr.Substring(15, 1) == "A" || modelNumStr.Substring(15, 1) == "B" ||
                modelNumStr.Substring(15, 1) == "C" || modelNumStr.Substring(15, 1) == "D" || 
                modelNumStr.Substring(15, 1) == "E" || modelNumStr.Substring(15, 1) == "F" ||
                modelNumStr.Substring(15, 1) == "L" || modelNumStr.Substring(15, 1) == "M")  // Indirect Fired (IF)
            {
                if (voltageInt == 208 || voltageInt == 230 || voltageInt == 240)
                {
                    primaryHtgInputStr = "6.25";
                }
                else if (voltageInt == 460)
                {
                    primaryHtgInputStr = "3.13";
                }
                else if (voltageInt == 575)
                {
                    primaryHtgInputStr = "2.5";
                }                   

                try
                {                   
                    if_MaxGasPressureStr = "14\"WC(3487Pa)";

                    if (modelNumStr.Substring(15, 1) == "A" || modelNumStr.Substring(15, 1) == "B" || modelNumStr.Substring(15, 1) == "C" || modelNumStr.Substring(15, 1) == "L")
                    {
                        fuelType = "NaturalGas";
                        if_MinGasPressureStr = "7\"WC(1744Pa)";
                        if_ManifoldPressureStr = "3.5\"WC(872Pa)";
                        fuelType = "Natural Gas"; // Stores fuel type
                    }
                    else
                    {
                        fuelType = "LiquidPropane";
                        if_MinGasPressureStr = "11\"WC(2740Pa)";
                        if_ManifoldPressureStr = "10\"WC(2491Pa)";
                        fuelType = "Liquid Propane"; // Stores fuel type
                    }
                    heatTypeStr = "Indirect";

                    if (unitTypeStr.StartsWith("OA") == true)
                    {
                        DataTable dtModel = objMain.GetModelNumDigitDesc(17, "IF", modelNumStr.Substring(16, 1), "REV6");

                        if (dtModel.Rows.Count > 0) // Calculate MBH 
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            mbhStr = drModel["DigitDescription"].ToString();
                            mbhStr = mbhStr.Substring(0, (mbhStr.IndexOf("MBH") - 1));
                            if_MaxHtgInputBTUHStr = mbhStr + "000";

                            double tmpKW = objMain.CalcKilowatts(if_MaxHtgInputBTUHStr);                            

                            tempDec = decimal.Parse(if_MaxHtgInputBTUHStr) * 0.8m;
                            if_MaxHtgInputBTUHStr = if_MaxHtgInputBTUHStr + "(" + tmpKW.ToString("f0") + "kW)";
                            
                            if_HtgOutputBTUHStr = tempDec.ToString("f0");
                            tmpKW = objMain.CalcKilowatts(if_HtgOutputBTUHStr);
                            if_HtgOutputBTUHStr = if_HtgOutputBTUHStr + "(" + tmpKW.ToString("f0") + "kW)";

                            if_MinInputBTUStr = getMinInputBTU(mbhStr, modelNumStr.Substring(2, 1));                           

                            if_TempRiseStr = "10F to 100F/-12C to 38C";
                        }                                         
                    }
                    else
                    {
                        DataTable dtModel = objMain.GetModelNumDigitDesc(17, "NA", modelNumStr.Substring(16, 1), "VKG");

                        if (dtModel.Rows.Count > 0) // Calculate MBH 
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            mbhStr = drModel["DigitDescription"].ToString();
                            mbhStr = mbhStr.Substring(0, (mbhStr.IndexOf("MBH") - 1));
                            if_MaxHtgInputBTUHStr = mbhStr + "000";

                            double tmpKW = objMain.CalcKilowatts(if_MaxHtgInputBTUHStr);
                          
                            tempDec = decimal.Parse(if_MaxHtgInputBTUHStr) * 0.8m;
                            if_MaxHtgInputBTUHStr = if_MaxHtgInputBTUHStr + "(" + tmpKW.ToString("f0") + "kW)";

                            if_HtgOutputBTUHStr = tempDec.ToString("f0");
                            tmpKW = objMain.CalcKilowatts(if_HtgOutputBTUHStr);
                            if_HtgOutputBTUHStr = if_HtgOutputBTUHStr + "(" + tmpKW.ToString("f0") + "kW)";

                            if_MinInputBTUStr = getMinInputBTU(mbhStr, modelNumStr.Substring(2, 1));                         

                            if_TempRiseStr = "10F to 100F/-12C to 38C";                        
                        }

                        if (modelNumStr.Substring(15, 1) == "A" || modelNumStr.Substring(15, 1) == "L") // If Digit 16 Equals L or M unit is 81% efficient
                        {
                            if_TempRiseStr = "20F to 85F/-7C to 29C"; // If Digit 16 Equals A or D
                            if_MinGasPressureStr = "5\"WC(1245Pa)";
                        }

                        if (modelNumStr.Substring(15, 1) == "D" || modelNumStr.Substring(15, 1) == "M") // If Digit 16 Equals L or M unit is 81% efficient
                        {
                            if_TempRiseStr = "20F to 50F/-7C to 10C";
                            if_MinGasPressureStr = "10\"WC(2491Pa)";

                            //if (mbhStr == "200")
                            //{
                            //    // If Digit 16 A, D 0.81m
                            //    if_MaxHtgInputBTUHStr = "162,000(47kW)";
                            //    if_HtgOutputBTUHStr = "110,000(32kW)";
                            //}
                            //else if (mbhStr == "300")
                            //{
                            //    if_MaxHtgInputBTUHStr = "243,000(71kW)";
                            //    if_HtgOutputBTUHStr = "165,000(48kW)";
                            //}
                            //else if (mbhStr == "400")
                            //{
                            //    if_MaxHtgInputBTUHStr = "324,000(95kw)";
                            //    if_HtgOutputBTUHStr = "220,000(64kW)";
                            //}
                        }
                    }

                    if_MaxEntStr = "2\"WC(498Pa)"; 
                    maxOutletAirTempStr = "85 F/52 C";                    
                    
                    if (modelNumStr.Substring(15, 1) == "D" || modelNumStr.Substring(15, 1) == "M") // If Digit 16 Equals D or M 
                    {
                        if_MinGasPressureStr = "10\"WC(2490Pa)";
                        if_ManifoldPressureStr = "10\"(2490Pa)";
                    }

                    //if (modelNumStr.Substring(15, 1) == "D" || modelNumStr.Substring(15, 1) == "E" ||                                   // Commented out 11/18/20 - same if statement above.
                    //    modelNumStr.Substring(15, 1) == "F" || modelNumStr.Substring(15, 1) == "M") // if Fuel Type is Liquid Propane
                    //{
                    //    if_MinGasPressureStr = "11";
                    //    if_ManifoldPressureStr = "10";
                    //    fuelType = "Liquid Propane"; // Stores fuel type
                    //}
                    //else
                    //{
                    //    if_MinGasPressureStr = "7";
                    //    if_ManifoldPressureStr = "3.5";
                    //    fuelType = "Natural Gas"; // Stores fuel type
                    //}                       
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 17 - " + f);
                }                

                //try
                //{
                //    if (modelNumStr.Substring(17, 1) == "4" )    // IF Digit 18 (Secondary Heat) equals Electric Staged
                //    {
                //        DataTable dtModel = objMain.GetModelNumDigitDesc(18, "IF", modelNumStr.Substring(17, 1));

                //        if (dtModel.Rows.Count > 0) // Calculate MBH 
                //        {
                //            DataRow drModel;
                //            drModel = dtModel.Rows[0];

                //            kwStr = drModel["DigitDescription"].ToString();

                //            kwStr = objMain.stripKwFromDescription(kwStr);                            
                //            kwStr = kwStr + "000";
                //            tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                //            tempDbl = tempDbl / Math.Sqrt(3);
                //            secHtgInputElecDbl = tempDbl;

                //            secHtgInputStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                //        }
                //        else
                //        {
                //            secHtgInputStr = "0";
                //        }
                //    }                                    
                //}
                //catch (Exception f)
                //{
                //    MessageBox.Show("Error Reading ModelNumDesc Table for digit 18 - " + f);
                //}
            }
            //else if (modelNumStr.Substring(15, 1) == "H" || modelNumStr.Substring(15, 1) == "J" ) // If Digit 16 (Primary Heat Type) Equals Electric-Staged or Electric-SCR Modulating 
            //{
            //    //heatingInputElecStr = (((kW * 1000) / voltage) / square root of 3)
            //    heatTypeStr = "Electric";
            //    try
            //    {
            //        DataTable dtModel = objMain.GetModelNumDesc(17, "ELEC", modelNumStr.Substring(16, 1));

            //        if (dtModel.Rows.Count > 0) // Calculate MBH 
            //        {
            //            DataRow drModel;
            //            drModel = dtModel.Rows[0];

            //            kwStr = drModel["DigitDescription"].ToString();
            //            kwStr = objMain.stripKwFromDescription(kwStr);
                                        
            //            kwStr = kwStr + "000";
            //            tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
            //            tempDbl = tempDbl / Math.Sqrt(3);
            //            heatingInputElecStr = tempDbl.ToString("f2");
            //            maxOutletAirTempStr = "90 Deg F";
            //        }
            //    }
            //    catch (Exception f)
            //    {
            //        MessageBox.Show("Error Reading ModelNumDesc Table for digit 17 - " + f);
            //    }
            //}
            else if ((modelNumStr.Substring(15, 1) == "0") && 
                     (modelNumStr.Substring(17, 1) == "1" || modelNumStr.Substring(17, 1) == "2")) // Direct Fired
            {
                heatTypeStr = "Direct";

                df_TempRiseStr = "100 F/52 F";
                df_MaxGasPressureStr = "14";
                df_MinHeatingInputStr = "22,000";

                df_MaxPressureDropStr = "";
                df_MinPressureDropStr = "";
                df_StaticPressureStr = "";
                df_CutOutTempStr = "";

                if (modelNumStr.Substring(20, 1) == "1") // Natural Gas
                {
                    df_MinGasPressureStr = "7";
                    df_ManifoldPressureStr = "3.5";
                }
                else   // Liquid Propane
                {
                    df_MinGasPressureStr = "10";
                    df_ManifoldPressureStr = "2";
                }

                if (modelNumStr.Substring(22, 1) == "A")
                {
                    df_MaxHtgInputBTUHStr = "330,000";
                }
                else if (modelNumStr.Substring(22, 1) == "B")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                }
                else if (modelNumStr.Substring(22, 1) == "C")
                {
                    df_MaxHtgInputBTUHStr = "600,000";
                }
                else if (modelNumStr.Substring(22, 1) == "D")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                    df_MinHeatingInputStr = "40,000";
                }
                else if (modelNumStr.Substring(22, 1) == "E")
                {
                    df_MaxHtgInputBTUHStr = "900,000";
                    df_MinHeatingInputStr = "40,000";
                }
            }
            else if (modelNumStr.Substring(15, 1) == "H" || modelNumStr.Substring(15, 1) == "J")  // Electric
            {               
                heatTypeStr = "Electric";
                calcElecHeatMCA = true;
                try
                {
                    DataTable dtModel = new DataTable();
                    if (revType == "REV6")
                    {
                        dtModel = objMain.GetModelNumDigitDesc(17, "ELEC", modelNumStr.Substring(16, 1), "REV6"); // OA uses digit 17
                    }
                    else
                    {
                        dtModel = objMain.GetModelNumDigitDesc(65, "NA", modelNumStr.Substring(64, 1), "VKG"); // Viking uses digit 65
                    }

                    if (dtModel.Rows.Count > 0) // Calculate Heating electric input value.
                    {
                        DataRow drModel;
                        drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = objMain.stripKwFromDescription(kwStr, voltageInt, revType);
                        primKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";
                       
                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        primHtgInputElecDbl = tempDbl;
                        heatingInputElecStr = tempDbl.ToString("f2");
                        maxOutletAirTempStr = "90 F/32 C";
                    }

                    //if (modelNumStr.Substring(17, 1) == "4" || modelNumStr.Substring(17, 1) == "5")
                    //{
                    //    secHeatExist = true;
                    //    secElecHeatExist = true;
                    //    dtModel = objMain.GetModelNumDigitDesc(19, "ELEC", modelNumStr.Substring(18, 1));
                    //    if (dtModel.Rows.Count > 0)
                    //    {
                    //        DataRow drModel;
                    //        drModel = dtModel.Rows[0];

                    //        kwStr = drModel["DigitDescription"].ToString();
                    //        kwStr = objMain.stripKwFromDescription(kwStr);

                    //        kwStr = kwStr + "000";
                    //        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                    //        tempDbl = tempDbl / Math.Sqrt(3);
                    //        secHtgInputElecDbl = tempDbl;
                    //        secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                    //    }
                    //}
                    //heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 19 - " + f);
                }
            }
            else if (modelNumStr.Substring(15, 1) == "G")  // Hot Water
            {
                heatTypeStr = "HotWater";
                flowRateStr = this.lbFlowRate.Text;
                enteringTempStr = this.lbEnteringTemp.Text;

                if (enteringTempStr.Length > 0)
                {
                    double tempCel = (Int32.Parse(enteringTempStr) - 32) / 1.8;
                    enteringTempStr = tempCel.ToString("f0");

                }              
            }
            else if (modelNumStr.Substring(15, 1) == "K")  // Steam
            {
                heatTypeStr = "Steam";               
            }
            else if (modelNumStr.Substring(15, 1) == "0") //NoHeat
            {
                heatTypeStr = "NoHeat";
                if_MaxEntStr = "2\"WC(498Pa)";                
            }

            if (heatTypeStr != "Electric")  // Potential bug - always assumes secondary heat type is electric
            {
                if ( modelNumStr.Substring(17, 1) == "4" || modelNumStr.Substring(17, 1) == "5")
                {
                    nonElecPrimWithSecHeat = true;
                }
                else if ( modelNumStr.Substring(17, 1) == "0")
                {
                    nonElecPrimNoSecHeat = true;
                }
            }
           
            if (modelNumStr.Substring(13, 1) == "5" || modelNumStr.Substring(13, 1) == "6" ||
                modelNumStr.Substring(13, 1) == "7" || modelNumStr.Substring(13, 1) == "8")
            {
                heatPumpExist = true;

                if (modelNumStr.Substring(13, 1) == "5" || modelNumStr.Substring(13, 1) == "6")
                {
                    airSrcHeatPumpExist = true;                    
                }
            }

            if (modelNumStr.Substring(17, 1) != "0")
            {
                secHeatExist = true;
                if (modelNumStr.Substring(17, 1) == "4" || modelNumStr.Substring(17, 1) == "5")
                {
                    secElecHeatExist = true;
                    
                    DataTable dtModel = objMain.GetModelNumDigitDesc(19, "ELEC", modelNumStr.Substring(18, 1), revType);
                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel;
                        drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = objMain.stripKwFromDescription(kwStr, voltageInt, revType);
                        secKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";                        

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                        tempDbl = tempDbl / Math.Sqrt(3);
                        secHtgInputElecDbl = tempDbl;
                        secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                    }
                }
                heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                calcElecHeatMCA = true;
            }
                        
            
            // Determine the number of Compressor for this unit and record the compressor data.              

            List<Compressor> CompList = new List<Compressor>();

            foreach(DataGridViewRow dgvr in dgvBOM.Rows)   
            {
                ruleHeadID = "0";
                ruleBatchID = dgvr.Cells["RuleBatchID"].Value.ToString();
                partCategoryStr = dgvr.Cells["PartCategory"].Value.ToString();
                partNumStr = dgvr.Cells["PartNum"].Value.ToString();
                parentPartNumStr = dgvr.Cells["ParentPartNum"].Value.ToString();
                revisionNum = dgvr.Cells["RevisionNum"].Value.ToString();

                if (ruleHeadID == "") // Safeguard in case RuleHeadID is null or blank
                {
                    ruleHeadID = "0";
                }

                if (ruleBatchID == "")  // Safeguard in case RuleBatchID is null or blank
                {
                    ruleBatchID = "0";
                }

               //string taco = "";

                //if (partNumStr == "VMEASM-201377")
                //{
                //    MessageBox.Show( "PartNum = VMEASM-201377");
                //}

                if (partCategoryStr != "")
                {
                    currentPartTypeStr = partCategoryStr;
                }

                motorTypeStr = dgvr.Cells["PartType"].Value.ToString();

                if ((currentPartTypeStr == "Compressor") || (currentPartTypeStr.Contains("DigitalScroll")) || (currentPartTypeStr.Contains("TandemComp")))
                {
                    if (partNumStr.Contains("RFGASM") == true)
                    {
                        
                        string immedParent = partNumStr;
                        DataTable dtComp = objMain.GetPartBOM(partNumStr, revisionNum);

                        foreach (DataRow drComp in dtComp.Rows)
                        {
                            partNumStr = drComp["MtlPartNum"].ToString();
                            qtyStr = drComp["QtyPer"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                compQtyStr = qtyStr;
                            }
                            compTypeStr = partCategoryStr;
                            objMain.getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, "REV6", ruleHeadID);

                            if (decimal.Parse(compRLA_VoltsStr) > 0)
                            {
                                CompList.Add(new Compressor("", partNumStr, parentPartNumStr, immedParent, compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                            }
                        }
                    }                   
                    else
                    {
                        objMain.getCompRLA_Data(partNumStr, voltageStr, out compRLA_VoltsStr, out compLRAStr, "REV6", "");

                        if (decimal.Parse(compRLA_VoltsStr) > 0)
                        {
                            parentPartNumStr = dgvr.Cells["ParentPartNum"].Value.ToString();
                            qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                            compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            compTypeStr = "Compressor";
                            if (Int32.Parse(compQtyStr) > 1) // Checking to see if qty is greater than 1, If true then account for additional compressors.
                            {
                                int compQtyInt = Int32.Parse(compQtyStr);
                                compQtyStr = "1";
                                for (int x = 0; x < compQtyInt; ++x)
                                {
                                    CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                                }
                            }
                            else
                            {
                                CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                            }
                        }
                    }           
                }
                else if (modelNumStr.StartsWith("HA") == true && partNumStr.StartsWith("VMEASM") == true && parentPartNumStr.StartsWith("VK-COND") == true)
                {
                    string immedParent = partNumStr;
                    DataTable dtAsm = objMain.GetPartBOM(partNumStr, revisionNum);

                    foreach (DataRow drAsm in dtAsm.Rows)
                    {
                        partNumStr = drAsm["MtlPartNum"].ToString();
                       
                        DataTable dtMotor = objMain.GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);

                        if (dtMotor.Rows.Count > 0)
                        {
                            DataRow drMotor = dtMotor.Rows[0];
                            qtyStr = drAsm["QtyPer"].ToString();

                            if (qtyStr.IndexOf(".") > 0)
                            {
                                fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                fanCondQtyStr = qtyStr;
                            }

                            fanCondFLAStr = drMotor["FLA"].ToString();
                            fanCondHPStr = drMotor["HP"].ToString();
                            fanCondFLAVoltStr = fanCondFLAStr + "-" + voltageStr;
                            fanCondPHStr = phaseInt.ToString();
                        }
                    }
                }
                else if (motorTypeStr == "Condensor")
                {
                    DataTable dtMotor = objMain.GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);   
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                        if (fanCondQtyStr == "0")
                        {
                            fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        }
                        else
                        {
                            int fanQty = Int32.Parse(fanCondQtyStr);
                            fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            fanQty += Int32.Parse(fanCondQtyStr);
                            fanCondQtyStr = fanQty.ToString();
                        }
                        DataRow dr = dtMotor.Rows[0];
                        fanCondFLAStr = dr["FLA"].ToString();
                        fanCondHPStr = dr["HP"].ToString();
                        fanCondFLAVoltStr = fanCondFLAStr + "-" + voltageStr;
                        fanCondPHStr = phaseInt.ToString();
                    }                    
                }
                else if (motorTypeStr == "SupplyFan" || motorTypeStr == "Indoor FAN")
                {
                    DataTable dtMotor = objMain.GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                        fanEvapQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        DataRow dr = dtMotor.Rows[0];
                        fanEvapFLAStr = dr["FLA"].ToString();
                        fanEvapHPStr = dr["HP"].ToString();
                        fanEvapFLAVoltStr = fanEvapFLAStr + "-" + voltageStr;
                        fanEvapPHStr = phaseInt.ToString();
                    }                
                    
                    ++evapFanCntInt;
                }
                else if (motorTypeStr == "PoweredExhaust" || motorTypeStr == "Powered Exhaust")
                {
                    DataTable dtMotor = objMain.GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                        fanPWRExhQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        DataRow dr = dtMotor.Rows[0];
                        fanPWRExhFLAStr = dr["FLA"].ToString();
                        fanPWRExhHPStr = dr["HP"].ToString();
                        fanPWRExhFLAVoltStr = fanPWRExhFLAStr + "-" + voltageStr;
                        fanPWRExhPHStr = phaseInt.ToString();
                    }                
                    
                    ++pwrExhFanCntInt;
                }
                else if (motorTypeStr == "ERV")
                {
                    DataTable dtMotor = objMain.GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = dgvr.Cells["ReqQty"].Value.ToString();
                        fanERVQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        DataRow dr = dtMotor.Rows[0];
                        fanERVFLAStr = dr["FLA"].ToString();
                        fanERVHPStr = dr["HP"].ToString();
                        fanERVFLAVoltStr = fanERVFLAStr + "-" + voltageStr;
                        fanERVPHStr = phaseInt.ToString();
                    }                                                        
                }
            }

            if  ((heatTypeStr == "IF" && modelNumStr.Substring(4, 3) == "000") || (nonElecPrimWithSecHeat == true))
            {
                int mbhInt = Int32.Parse(mbhStr);                

                if (mbhInt > 0)
                {
                    if (mbhInt < 225)
                    {
                        primaryHtgInputStr = "1.69";
                    }
                    else if (mbhInt < 425)
                    {
                        primaryHtgInputStr = "3.3";
                    }
                    else if (mbhInt < 825)
                    {
                        primaryHtgInputStr = "6.14";
                    }
                    else if (mbhInt < 1225)
                    {
                        primaryHtgInputStr = "10";
                    }
                }
                else
                {
                    primaryHtgInputStr = "0";
                }
            }

            //objMain.calcAmpacityValues(out minCKTAmpStr, out mfsMCBStr, out rawMopStr, CompList, fanCondFLAStr, fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr,
            //                           fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString(), preHtgInputStr);   
          
            //if (calcHeatMCA == true)
            //{                
            //    objMain.calcElecAmpacityValues(out tmp_MinCKTAmpStr, out tmp_MFSMCBStr, fanEvapFLAStr, fanEvapQtyStr,
            //        fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageStr, heatingInputElecStr);

            //    if (double.Parse(tmp_MinCKTAmpStr) > double.Parse(minCKTAmpStr))
            //    {
            //        minCKTAmpStr = tmp_MinCKTAmpStr;

            //        if (double.Parse(tmp_MFSMCBStr) > double.Parse(mfsMCBStr))
            //        {
            //            mfsMCBStr = objMain.roundElecMOP(double.Parse(tmp_MinCKTAmpStr));
            //        }

            //        rawMopStr = "NA";
            //    }
            //}

            mcaStr = "0";

            // Calculate the different modes that the unit can operate in.
            // Fans are calculated for every unit.

            objMain.calcAmpacityValuesFansOnly(out fansOnlyFLAStr, out fansOnlyMcaStr, out fansOnlyRawMopStr, fanEvapFLAStr, 
                                               fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, 
                                               voltageInt.ToString());

            if (nonElecPrimNoSecHeat == true) // If the unit has non electric primary heat and no secondary heat then add the additional values to MCA & MOP
            {                                 // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                nonElecPrimNoSecMcaStr = (double.Parse(fansOnlyMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                nonElecPrimNoSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            }
            else if (nonElecPrimWithSecHeat == true)  // If the unit has non electric primart heat and has secondary elec heat then add the additional values to MCA & MOP
            {                                         // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                if (secKwInt >= 50)
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + secHtgInputElecDbl).ToString("f1");
                }
                else
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + (secHtgInputElecDbl * 1.25)).ToString("f1");
                }

                nonElecPrimWithSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
            }

            if (modelNumStr.Substring(4, 3) != "000") // if the unit has cooling then calculate the cooling MCA & MOP
            {                                    // B. DX Cool (Horizon & Viking)
                objMain.calcAmpacityValues(out coolingMcaStr, out coolingMopStr, out coolingRawMopStr, CompList, fanCondFLAStr, fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr,
                                   fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString());
            }

            if (heatPumpExist == true) // if the unit has a heat pump and has secondary electric heating then add the additional values to MCA & MOP
            {                          // E. DX Heat (Air or Water Source Heat Pump) Normal Operation (Horizon)
                if (airSrcHeatPumpExist == true)  // F.	DX Heat (Air Source Heat Pump) Defrost Mode Operation (Horizon)
                {
                    if (nonElecPrimWithSecHeat == true) // If the unit has non electric primary heat and has secondary heat then add the additional values to MCA & MOP
                    {
                        if (secElecHeatExist == false) //Mode F-a
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                        }
                        else // Secondary heat exist   Mode F-b
                        {
                            if (secKwInt >= 50)
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                            }
                            else
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                            }
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                        }
                    }
                    else if (heatTypeStr == "Electric") // Mode F-c Elec primary heat; no secondary elec heat
                    {
                        if (primKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * double.Parse(primaryHtgInputStr))).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                    }
                }
                else
                {
                    if (secElecHeatExist == true) //Mode E-a
                    {
                        if (secKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + secHtgInputElecDbl).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + secHtgInputElecDbl).ToString("f1");
                    }
                    // If no secondary elec heat then just use the coolingMcaStr & coolingRawMopStr values
                    // Mode E-b
                }
                // Else if no secondary elec heat the coolingMcaStr doesn't change from what was calculate earlier.
            }

            if (calcElecHeatMCA == true) // if the unit contains some type of electric heating
            {                            // C.	Primary Heat: Electric (Viking and Horizon)
                objMain.calcElecAmpacityValues(out elecMcaStr, out elecRawMopStr, primKwInt, secKwInt, fanEvapFLAStr,
                                               fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr,  
                                               voltageInt.ToString(), primHtgInputElecDbl, secHtgInputElecDbl);
            }


            //objMain.calcAmpacityValuesFansOnly(out fansOnlyFLAStr, out fansOnlyMcaStr, out fansOnlyRawMopStr, fanEvapFLAStr, fanEvapQtyStr, fanERVFLAStr,
            //                            fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString());

            //if (nonElecPrimNoSecHeat == true) // If the unit has non electric primart heat and no secondary heat then add the additional values to MCA & MOP
            //{
            //    nonElecPrimNoSecMcaStr = (double.Parse(fansOnlyMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f2");
            //    nonElecPrimNoSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f2");
            //}
            //else if (nonElecPrimWithSecHeat == true)  // If the unit has non electric primart heat and has secondary elec heat then add the additional values to MCA & MOP
            //{
            //    nonElecPrimWithSecMcaStr = ((double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl) * 1.25).ToString("f2");
            //    nonElecPrimWithSecMopStr = ((double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl)).ToString("f2");
            //}

            //if (modelNumStr.Substring(4, 3) != "000") // if the unit has cooling then calculate the cooling MCA & MOP
            //{
            //    objMain.calcAmpacityValues(out coolingMcaStr, out coolingMopStr, out coolingRawMopStr, CompList, fanCondFLAStr, fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr,
            //                       fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString());
            //}

            //if (heatPumpExist == true) // if the unit has a heat pump and has secondary electric heating then add the additional values to MCA & MOP
            //{
            //    if (secElecHeatExist == true)
            //    {
            //        if (secKwInt >= 50)
            //        {
            //            coolingMcaStr = (double.Parse(coolingMcaStr) + secHtgInputElecDbl).ToString("f2");
            //        }
            //        else
            //        {
            //            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * secHtgInputElecDbl)).ToString("f2");
            //        }
            //        coolingRawMopStr = (double.Parse(coolingRawMopStr) + secHtgInputElecDbl).ToString("f2");

            //    }

            //    if (airSrcHeatPumpExist == true)  // F.	DX Heat (Air Source Heat Pump) Defrost Mode Operation (Horizon)
            //    {
            //        if (nonElecPrimWithSecHeat == true) // If the unit has non electric primary heat and has secondary heat then add the additional values to MCA & MOP
            //        {
            //            if (secElecHeatExist == false)
            //            {
            //                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f2");
            //                coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f2");
            //            }
            //            else // Secondary heat exist
            //            {
            //                if (secKwInt >= 50)
            //                {
            //                    coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f2");
            //                }
            //                else
            //                {
            //                    coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + (1.25 * secHtgInputElecDbl)).ToString("f2");
            //                }
            //                coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f2");
            //            }
            //        }
            //        else if (heatTypeStr == "ELEC")
            //        {
            //            if (primKwInt >= 50)
            //            {
            //                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString();
            //            }
            //            else
            //            {
            //                coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * double.Parse(primaryHtgInputStr))).ToString("f2");
            //            }
            //            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f2");
            //        }
            //    }
            //}

            //if (calcElecHeatMCA == true) // if the unit contains some type of electric heating
            //{
            //    objMain.calcElecAmpacityValues(out elecMcaStr, out elecRawMopStr, primKwInt, secKwInt, fanEvapFLAStr, 
            //                                   fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, 
            //                                   voltageInt.ToString(), primHtgInputElecDbl, secHtgInputElecDbl);
            //}

            // determine which MCA value is the largest 
            if (double.Parse(fansOnlyMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(elecMcaStr) &&
                double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = fansOnlyMcaStr;
            }
            else if (double.Parse(coolingMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = coolingMcaStr;
            }
            else if (double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = nonElecPrimNoSecMcaStr;
            }
            else if (double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr))
            {
                mcaStr = nonElecPrimWithSecMcaStr;
            }
            else
            {
                mcaStr = elecMcaStr;
            }

            minCKTAmpStr = mcaStr;

            // determine which MOP value is the largest 
            if (double.Parse(fansOnlyRawMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(elecRawMopStr) &&
               double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = fansOnlyRawMopStr;
            }
            else if (double.Parse(coolingRawMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = coolingRawMopStr;
            }
            else if (double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = nonElecPrimNoSecMopStr;
            }
            else if (double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(nonElecPrimNoSecMopStr))
            {
                rawMopStr = nonElecPrimWithSecMopStr;
            }
            else
            {
                rawMopStr = elecRawMopStr;
            }

            // Do the rounding for both electrical MCA & non electrical MCA
            if (mcaStr != "0" &&  calcElecHeatMCA == true)
            {
                elecMopStr = objMain.roundElecMOP(double.Parse(mcaStr)); // Electrical MCA rounding
            }
            else
            {
                elecMopStr = "0";
            }

            mopStr = objMain.roundMOP(double.Parse(rawMopStr)); // Non-Electrical MCA rounding

            if (double.Parse(elecMopStr) > double.Parse(mopStr)) // Use the larger of the 2 values
            {
                mopStr = elecMopStr;
            }

            mfsMCBStr = mopStr;

            lbMCA.Text = minCKTAmpStr;
            lbMOP.Text = mfsMCBStr;

            //if (modelNumStr.Substring(17, 1) == "1" || modelNumStr.Substring(17, 1) == "2")
            //{              
            //    objMain.calcElecAmpacityValues(out tmp_MinCKTAmpStr, out tmp_MFSMCBStr, fanEvapFLAStr, fanEvapQtyStr,
            //        fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr, voltageStr, secHtgInputStr);

            //    if (double.Parse(tmp_MinCKTAmpStr) > double.Parse(minCKTAmpStr))
            //    {
            //        minCKTAmpStr = tmp_MinCKTAmpStr;
            //        mfsMCBStr = objMain.roundElecMOP(double.Parse(tmp_MinCKTAmpStr));
            //    }
            //}

            string digit3 = modelNumStr.Substring(2, 1);
            string digit4 = modelNumStr.Substring(3, 1);
            string digit567 = modelNumStr.Substring(4, 3);
            string digit9 = modelNumStr.Substring(8, 1);
            string digit11 = modelNumStr.Substring(10, 1);
            string digit12 = modelNumStr.Substring(11, 1);
            string digit13 = modelNumStr.Substring(12, 1);
            string digit14 = modelNumStr.Substring(13, 1);     

            DataTable dtCCD = objMain.GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow dr = dtCCD.Rows[0];
                               
                comp1QtyStr = "1";
                //comp1RLA_VoltsStr = dr["Comp1_1_RLA"].ToString() + "-" + voltageStr;
                comp1RLA_VoltsStr = dr["Comp1_1_RLA"].ToString();
                comp1LRAStr = dr["Comp1_1_LRA"].ToString();
                comp1PhaseStr = "3";

                if (dr["Circuit1_2_Compressor"].ToString() != "NA")
                {
                    comp2QtyStr = "1";
                    //comp2RLA_VoltsStr = dr["Comp1_2_RLA"].ToString() + "-" + voltageStr;
                    comp2RLA_VoltsStr = dr["Comp1_2_RLA"].ToString();
                    comp2LRAStr = dr["Comp1_2_LRA"].ToString();
                    comp2PhaseStr = "3";
                }

                if (dr["Circuit1_3_Compressor"].ToString() != "NA")
                {
                    comp3QtyStr = "1";
                    //comp3RLA_VoltsStr = dr["Comp1_2_RLA"].ToString() + "-" + voltageStr;
                    comp3RLA_VoltsStr = dr["Comp1_3_RLA"].ToString();
                    comp3LRAStr = dr["Comp1_3_LRA"].ToString();
                    comp3PhaseStr = "3";
                }

                if (dr["Circuit2_1_Compressor"].ToString() != "NA")
                {
                    comp4QtyStr = "1";
                    //comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString();
                    comp4LRAStr = dr["Comp2_1_LRA"].ToString();
                    comp4PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp2QtyStr = "1";
                    //    comp2RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp2LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp2PhaseStr = "3";
                    //}
                    //else if (dr["Circuit1_3_Compressor"].ToString() == "NA")
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                }

                if (dr["Circuit2_2_Compressor"].ToString() != "NA")
                {
                    comp5QtyStr = "1";
                    //comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString();
                    comp5LRAStr = dr["Comp2_2_LRA"].ToString();
                    comp5PhaseStr = "3";
                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                    //else if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp5QtyStr = "1";
                    //    comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp5LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp5PhaseStr = "3";
                    //}
                }

                if (dr["Circuit2_3_Compressor"].ToString() != "NA")
                {
                    comp6QtyStr = "1";
                    //comp6RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    comp6RLA_VoltsStr = dr["Comp2_3_RLA"].ToString();
                    comp6LRAStr = dr["Comp2_3_LRA"].ToString();
                    comp6PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_3_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                    //else if (dr["Circuit1_3_Compressor"].ToString() == "NA")
                    //{
                    //    comp5QtyStr = "1";
                    //    comp5RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    //    comp5LRAStr = dr["Comp2_3_LRA"].ToString();
                    //    comp5PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp6QtyStr = "1";
                    //    comp6RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    //    comp6LRAStr = dr["Comp2_3_LRA"].ToString();
                    //    comp6PhaseStr = "3";
                    //}
                }

                if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = dr["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(dr["Circuit1_Charge"].ToString()) + Decimal.Parse(dr["Circuit1_ReheatCharge"].ToString())).ToString();
                }
                
                if (dr["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = dr["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(dr["Circuit2_Charge"].ToString()) + Decimal.Parse(dr["Circuit2_ReheatCharge"].ToString())).ToString();
                    }                   
                }
            }
            else
            {
                int cir1CompNum = 1;
                int cir2CompNum = 1;
                foreach (var comp in CompList)
                {
                    //parentPartNumStr = comp.ParentPartNum.Substring(0, 11);
                    if (revType == "VKG")
                    {
                        partNumStr = comp.ImmedParent;
                    }
                    else
                    {
                        if (comp.ParentPartNum.StartsWith("RFGASM") == true)
                        {
                            int parenPos = comp.ParentPartNum.IndexOf("(");
                            int partLen = 0;

                            if (parenPos > 0)
                            {
                                partLen = parenPos - 1;
                            }
                            else
                            {
                                partLen = comp.ParentPartNum.Length;
                            }
                            partNumStr = comp.ParentPartNum.Substring(0, partLen);
                        }
                        else
                        {
                            partNumStr = comp.PartNum;
                        }
                    }

                    parentPartNumStr = comp.ParentPartNum;                    
                    ruleBatchID = comp.RuleBatchID;

                    DataTable dtComp = objMain.GetPartRulesByRuleHeadID_AndRuleBatchID(partNumStr, ruleBatchID, revType);
                  
                    if (dtComp.Rows.Count > 0)
                    {
                        DataRow row = dtComp.Rows[0];

                        if (row["Circuit1"].ToString() == "True" && row["Circuit2"].ToString() == "True")
                        {
                            if (cir1CompNum > 1)
                            {
                                comp.CircuitPos = "Circuit2_" + cir1CompNum++.ToString();
                            }
                            else
                            {
                                comp.CircuitPos = "Circuit1_" + cir1CompNum++.ToString();
                            }
                        }
                        else if (row["Circuit1"].ToString() == "True")
                        {
                            comp.CircuitPos = "Circuit1_" + cir1CompNum++.ToString();                            
                        }
                        else if (row["Circuit2"].ToString() == "True")
                        {
                            comp.CircuitPos = "Circuit2_" + cir2CompNum++.ToString();
                        }
                    }
                    else
                    {
                        if (cir1CompNum < 4)
                        {
                            comp.CircuitPos = "Circuit1";
                            ++cir1CompNum;
                        }
                        else
                        {
                            comp.CircuitPos = "Circuit2";
                            ++cir1CompNum;
                        }

                    }
                }

                decimal largestCir1CompRLA = 0;
                decimal largestCir2CompRLA = 0;
                cir1CompNum = 1;
                cir2CompNum = 1;

                foreach (var comp in CompList)
                {
                    if (comp.CircuitPos.Contains("Circuit1"))
                    {
                        if (decimal.Parse(comp.RLA) > largestCir1CompRLA)
                        {
                            largestCir1CompRLA = decimal.Parse(comp.RLA);
                            if (cir1CompNum > 1)
                            {
                                if (cir1CompNum > 2)
                                {                                    
                                    comp3RLA_VoltsStr = comp2RLA_VoltsStr;
                                    comp3LRAStr = comp2LRAStr;
                                    comp3QtyStr = comp2QtyStr;
                                }
                                comp2RLA_VoltsStr = comp1RLA_VoltsStr;
                                comp2LRAStr = comp1LRAStr;
                                comp2QtyStr = comp1QtyStr;
                            }
                            comp1RLA_VoltsStr = comp.RLA;
                            comp1LRAStr = comp.LRA;
                            comp1QtyStr = comp.Qty;
                        }
                        else
                        {
                            if (cir1CompNum > 2)
                            {
                                if (decimal.Parse(comp.RLA) > decimal.Parse(comp2RLA_VoltsStr))
                                {
                                    comp3RLA_VoltsStr = comp2RLA_VoltsStr;
                                    comp3LRAStr = comp2LRAStr;
                                    comp3QtyStr = comp2QtyStr;
                                    comp2RLA_VoltsStr = comp.RLA;
                                    comp2LRAStr = comp.LRA;
                                    comp2QtyStr = comp.Qty;
                                }
                                else
                                {
                                    comp3RLA_VoltsStr = comp.RLA;
                                    comp3LRAStr = comp.LRA;
                                    comp3QtyStr = comp.Qty;
                                }
                            }
                            else
                            {
                                comp2RLA_VoltsStr = comp.RLA;
                                comp2LRAStr = comp.LRA;
                                comp2QtyStr = comp.Qty;
                            }                            
                        }
                        ++cir1CompNum;
                    }
                    else if (comp.CircuitPos.Contains("Circuit2"))
                    {                        
                        if (decimal.Parse(comp.RLA) > largestCir2CompRLA)
                        {
                            largestCir2CompRLA = decimal.Parse(comp.RLA);
                            if (cir2CompNum > 1)
                            {
                                if (cir2CompNum > 2)
                                {
                                    comp6RLA_VoltsStr = comp5RLA_VoltsStr;
                                    comp6LRAStr = comp5LRAStr;
                                    comp6QtyStr = comp5QtyStr;
                                }
                                comp5RLA_VoltsStr = comp4RLA_VoltsStr;
                                comp5LRAStr = comp4LRAStr;
                                comp5QtyStr = comp4QtyStr;
                            }
                            comp4RLA_VoltsStr = comp.RLA;
                            comp4LRAStr = comp.LRA;
                            comp4QtyStr = comp.Qty;
                        }
                        else
                        {
                            if (cir2CompNum > 2)
                            {
                                if (decimal.Parse(comp.RLA) > decimal.Parse(comp5RLA_VoltsStr))
                                {
                                    comp6RLA_VoltsStr = comp5RLA_VoltsStr;
                                    comp6LRAStr = comp5LRAStr;
                                    comp5RLA_VoltsStr = comp.RLA;
                                    comp5LRAStr = comp.LRA;
                                }
                                else
                                {
                                    comp6RLA_VoltsStr = comp.RLA;
                                    comp6LRAStr = comp.LRA; ;
                                }
                            }
                            else
                            {
                                comp5RLA_VoltsStr = comp.RLA;
                                comp5LRAStr = comp.LRA;
                            }
                        }
                        ++cir2CompNum;
                    }                                      
                }
                DataTable dtChrg = objMain.GetCompressorChargeData(unitTypeStr, modelNumStr.Substring(4, 3), modelNumStr.Substring(10, 1), modelNumStr.Substring(12, 1), modelNumStr.Substring(13, 1));
                if (dtChrg.Rows.Count > 0)
                {
                    DataRow drChrg = dtChrg.Rows[0];                    
                                      
                    if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit1ChargeStr = drChrg["Circuit1"].ToString();
                    }
                    else
                    {
                        circuit1ChargeStr = (Decimal.Parse(drChrg["Circuit1"].ToString()) + Decimal.Parse(drChrg["Circuit1_Reheat"].ToString())).ToString();
                    }

                    circuit2ChargeStr = drChrg["Circuit2"].ToString();  
                }
            }         
  
            if (comp1RLA_VoltsStr != "")
            {
                comp1RLA_VoltsStr += "-" + voltageStr;
                //comp1QtyStr = "1";
                comp1PhaseStr = "3";
            }

            if (comp2RLA_VoltsStr != "")
            {
                comp2RLA_VoltsStr += "-" + voltageStr;
                //comp2QtyStr = "1";
                comp2PhaseStr = "3";
            }

            if (comp3RLA_VoltsStr != "")
            {
                comp3RLA_VoltsStr += "-" + voltageStr;
                //comp3QtyStr = "1";
                comp3PhaseStr = "3";
            }

            if (comp4RLA_VoltsStr != "")
            {
                comp4RLA_VoltsStr += "-" + voltageStr;
                //comp4QtyStr = "1";
                comp4PhaseStr = "3";
            }

            if (comp5RLA_VoltsStr != "")
            {
                comp5RLA_VoltsStr += "-" + voltageStr;
                //comp5QtyStr = "1";
                comp5PhaseStr = "3";
            }

            if (comp6RLA_VoltsStr != "")
            {
                comp6RLA_VoltsStr += "-" + voltageStr;
                //comp6QtyStr = "1";
                comp6PhaseStr = "3";
            }

            lbCir1Chrg.Text = circuit1ChargeStr + " lbs";
            lbCir2Chrg.Text = circuit2ChargeStr + " lbs";

            objMain.JobNum = jobNumStr; 
			objMain.OrderNum = Int32.Parse(orderNumStr);
			objMain.OrderLine = Int32.Parse(orderLineStr); 
			objMain.ReleaseNum = Int32.Parse(releaseNumStr);
			objMain.ModelNo = modelNumStr;
			objMain.SerialNo = serialNoStr;
			objMain.MfgDate = mfgDateStr;
			objMain.UnitType = unitTypeStr;
			objMain.ElecRating = elecRatingStr;
			objMain.OperVoltage = operatingVoltsStr;
			objMain.DualPointPower = dualPointPower;
			objMain.MinCKTAmp = minCKTAmpStr;
			objMain.MinCKTAmp2 = minCKTAmpStr2;
			objMain.MfsMcb = mfsMCBStr;
			objMain.MfsMcb2 = mfsMCBStr2;
            objMain.MOP = rawMopStr;
			objMain.HeatingType = heatTypeStr;
			objMain.Voltage = voltageStr;
			objMain.TestPressureHigh = testPresHighStr;
			objMain.TestPressureLow = testPresLowStr;
			objMain.SecondaryHtgInput = secHtgInputStr;
			objMain.HeatingInputElectric = heatingInputElecStr;
			objMain.FuelType = fuelType;
			objMain.Comp1Qty = comp1QtyStr;
			objMain.Comp2Qty = comp2QtyStr;
			objMain.Comp3Qty = comp3QtyStr;
			objMain.Comp4Qty = comp4QtyStr;
			objMain.Comp5Qty = comp5QtyStr;
			objMain.Comp6Qty = comp6QtyStr;
            objMain.Comp1Phase = comp1PhaseStr;
            objMain.Comp2Phase = comp2PhaseStr;
            objMain.Comp3Phase = comp3PhaseStr;
            objMain.Comp4Phase = comp4PhaseStr;
            objMain.Comp5Phase = comp5PhaseStr;
            objMain.Comp6Phase = comp6PhaseStr;
			objMain.Comp1RLA_Volts = comp1RLA_VoltsStr;
			objMain.Comp2RLA_Volts = comp2RLA_VoltsStr;
			objMain.Comp3RLA_Volts = comp3RLA_VoltsStr;
			objMain.Comp4RLA_Volts = comp4RLA_VoltsStr;
			objMain.Comp5RLA_Volts = comp5RLA_VoltsStr;
			objMain.Comp6RLA_Volts = comp6RLA_VoltsStr;
			objMain.Comp1LRA = comp1LRAStr;
			objMain.Comp2LRA = comp2LRAStr;
			objMain.Comp3LRA = comp3LRAStr;
			objMain.Comp4LRA = comp4LRAStr;
			objMain.Comp5LRA = comp5LRAStr;
			objMain.Comp6LRA = comp6LRAStr;
			objMain.Circuit1Charge = circuit1ChargeStr;
			objMain.Circuit2Charge = circuit2ChargeStr;
			objMain.FanCondQty = fanCondQtyStr;
			objMain.FanEvapQty = fanEvapQtyStr;
			objMain.FanErvQty = fanERVQtyStr;
			objMain.FanPwrExhQty = fanPWRExhQtyStr;
			objMain.FanCondPhase = fanCondPHStr;
			objMain.FanEvapPhase = fanEvapPHStr;
			objMain.FanErvPhase = fanERVPHStr;
			objMain.FanPwrExhPhase = fanPWRExhPHStr;
			objMain.FanCondFLA= fanCondFLAVoltStr;
			objMain.FanEvapFLA= fanEvapFLAVoltStr;
			objMain.FanErvFLA = fanERVFLAVoltStr;
			objMain.FanPwrExhFLA = fanPWRExhFLAVoltStr;
			objMain.FanCondHP = fanCondHPStr;
			objMain.FanEvapHP = fanEvapHPStr;
			objMain.FanErvHP= fanERVHPStr;
			objMain.FanPwrExhHP = fanPWRExhHPStr;
			objMain.FlowRate = flowRateStr;
			objMain.EnteringTemp = enteringTempStr;
			objMain.OperatingPressure = operatingPressureStr;
            objMain.WaterGlycol = waterGlycol;
			objMain.DF_StaticPressure = df_StaticPressureStr;
			objMain.DF_MaxHtgInputBTUH = df_MaxHtgInputBTUHStr;
			objMain.DF_MinHeatingInput = df_MinHeatingInputStr;
			objMain.DF_MinPressureDrop = df_MinPressureDropStr;
			objMain.DF_MaxPressureDrop = df_MaxPressureDropStr;
			objMain.DF_ManifoldPressure = df_ManifoldPressureStr;
            objMain.DF_CutoutTemp = df_CutOutTempStr;
            objMain.DF_MinGasPressure = df_MinGasPressureStr;
            objMain.DF_MaxGasPressure = df_MaxGasPressureStr;
            objMain.DF_TempRise = df_TempRiseStr;
			objMain.IN_MaxHtgInputBTUH = if_MaxHtgInputBTUHStr;
			objMain.IN_HeatingOutputBTUH = if_HtgOutputBTUHStr;
			objMain.IN_MinInputBTU = if_MinInputBTUStr;
			objMain.IN_MaxExt = if_MaxEntStr;
			objMain.IN_TempRise = if_TempRiseStr;
            objMain.IN_MaxOutAirTemp = maxOutletAirTempStr;
			objMain.IN_MaxGasPressure = if_MaxGasPressureStr;
			objMain.IN_MinGasPressure = if_MinGasPressureStr;
			objMain.IN_ManifoldPressure = if_ManifoldPressureStr;
			objMain.ModelNoVerifiedBy = "OAU_Config";
			objMain.French = "French";
            objMain.WhseCode = WarehouseCode;

            try
            {
                objMain.InsertETL_Oau();
            }
            catch (Exception f)
            {
                MessageBox.Show("Error writing to the R6_OA_EtlOAU Table - " + f);
            }
                   
        }

        private void writeEtlMonitorStickerData(string jobNumStr, string orderNumStr, string orderLineStr, string releaseNumStr, string modelNoStr)
        {            
            string voltageStr = "";
            string unitTypeStr;                       
                   
            string orderNum = "";
            string orderLine = "";
            string relNum = "";
            
           
            string maxOutletAirTempStr = "0";            
            string minCKTAmpStr = "";            
            string mopStr = "N/A";
            string elecRatingStr = "";
            string operatingVoltsStr = "";
                      
            string rawMopStr = "";
            string mfgDateStr = "";            
            string heatTypeStr = "";
            string exhaustFanHP = "";
            string ervWheelHP = "";
            string partType = "";
            string ervType = "";
            
            string fuelType = "";         

            int voltageInt = 0;           
            int phaseInt = 0;
            int pos = 0;
            
            decimal hertzDec = 0;

            DateTime mfgDateDate;

            Byte dualPointPower = 0;

            //string serialNoStr = modelNoStr.Substring(0,2) + orderNumStr + "-" + orderLineStr + "-" + releaseNumStr;            
            mfgDateStr = DateTime.Today.ToShortDateString();
            mfgDateDate = Convert.ToDateTime(mfgDateStr);

            switch (modelNoStr.Substring(7, 1)) //Voltage 
            {
                case "1":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 1;
                    elecRatingStr = "208-230/60/1";
                    operatingVoltsStr = "187 / 253";
                    break;                
                case "3":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "208-230/60/3";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "4":
                    voltageInt = 460;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "460/60/3";
                    operatingVoltsStr = "414 / 506";
                    break;
                case "W":
                    voltageInt = 575;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "575/60/3";
                    operatingVoltsStr = "517 / 633";
                    break;                
                default:
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
            }

            voltageStr = voltageInt.ToString();

            if (modelNoStr.Substring(0, 1) == "Y")  // Gas/Electric Heat
            {
                try
                {
                    heatTypeStr = "Gas/Electric";                                        
                    maxOutletAirTempStr = "125 Deg F";
                    fuelType = "Gas";
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 10 - " + f);
                }
            }
            else if (modelNoStr.Substring(0, 1) == "T")  // Electric
            {
                heatTypeStr = "Electric";
                fuelType = "Electric";
            }

            objMain.Digit1 = modelNoStr.Substring(0, 1);
            objMain.Digit2 = modelNoStr.Substring(1, 1);
            objMain.Digit456 = modelNoStr.Substring(3, 3);
            objMain.Digit7 = modelNoStr.Substring(6, 1);
            objMain.Digit8 = modelNoStr.Substring(7, 1);
            objMain.Digit10 = modelNoStr.Substring(9, 1);
            objMain.Digit22 = modelNoStr.Substring(21, 1);
            objMain.Digit32 = modelNoStr.Substring(31, 1);
            objMain.Digit33 = modelNoStr.Substring(32, 1);
                     
            int dashPos = -1;
            int dashPos2 = -1;

            DataTable dtCMD = objMain.MonitorGetCompAndMotorData();

            if (dtCMD.Rows.Count > 0)
            {
                DataRow row = dtCMD.Rows[0];

                dashPos = jobNumStr.IndexOf("-");
                dashPos2 = jobNumStr.LastIndexOf("-");
                orderNum = jobNumStr.Substring(0, dashPos);
                orderLine = jobNumStr.Substring((dashPos+1),  (dashPos2 - (dashPos + 1)));
                relNum = jobNumStr.Substring((dashPos2+1), 1);
                objMain.JobNum = jobNumStr;
                objMain.SerialNo = "MON" + jobNumStr;
                objMain.OrderNum = Int32.Parse(orderNum);
                objMain.OrderLine = Int32.Parse(orderLine);
                objMain.ReleaseNum = Int32.Parse(relNum);
                objMain.MfgDate = DateTime.Now.ToShortDateString();
                objMain.ModelNo = modelNoStr;
                objMain.Voltage = voltageInt.ToString();
                objMain.ElecRating = elecRatingStr;
                objMain.OperVoltage = operatingVoltsStr;
                objMain.HeatingType = heatTypeStr;
                objMain.FuelType = fuelType;
                objMain.HeatingInputElectric = "0";
                objMain.MaxOutletTemp = maxOutletAirTempStr;
                objMain.Circuit1Charge = " ";
                objMain.Circuit2Charge = " ";

                foreach(DataGridViewRow dgvr in dgvBOM.Rows)
                {
                    if (dgvr.Cells["PartCategory"].Value.ToString().Contains("Motor") || dgvr.Cells["PartCategory"].Value.ToString().Contains("Mtr"))
                    {
                        if (dgvr.Cells["PartNum"].Value.ToString().Contains("ERV"))
                        {
                            if (dgvr.Cells["Description"].ToString().Contains("Composite"))
                            {
                                ervType = "SemcoWheel";
                            }
                            else
                            {
                                ervType = "AirXchange";
                            }
                        }
                        else
                        {
                            partType = "ExhaustFan";
                        }

                        //objMain.RuleHeadID = dgvr.Cells["RuleHeadID"].Value.ToString();
                        //DataTable dtPart = objMain.MonitorGetMotorDataDTL();
                        //if (dtPart.Rows.Count > 0)
                        //{
                        //    DataRow drPart = dtPart.Rows[0];
                        //    if (partType == "ExhaustFan")
                        //    {
                        //        exhaustFanHP = drPart["HP"].ToString();
                        //    }
                        //    else
                        //    {
                        //        ervWheelHP = drPart["HP"].ToString();
                        //    }
                        //}
                    }
                }

                objMain.DD_SupplyFanStdFLA = row["DD_SupplyFanStdFLA"].ToString();
                if (objMain.DD_SupplyFanStdFLA != "0.00")
                {
                    objMain.DD_SupplyFanStdQty = "1";
                    objMain.DD_SupplyFanStdPhase = phaseInt.ToString();
                }
                else
                {
                    objMain.DD_SupplyFanStdQty = "NA";
                    objMain.DD_SupplyFanStdPhase = "NA";                                       
                }

                objMain.DD_SupplyFanOvrFLA = row["DD_SupplyFanOvrFLA"].ToString();
                if (objMain.DD_SupplyFanOvrFLA != "0.00")
                {
                    objMain.DD_SupplyFanOvrQty = "1";
                    objMain.DD_SupplyFanOvrPhase = phaseInt.ToString();
                }
                else
                {
                    objMain.DD_SupplyFanOvrQty = "NA";
                    objMain.DD_SupplyFanOvrPhase = "NA";                   
                }

                objMain.ElectricHeatFLA = row["ElectricHeatFLA"].ToString();
                objMain.PreHeatFLA = row["PreHeatFLA"].ToString();                        

                objMain.BeltSupplyFanStdFLA = row["Belt_SupplyFanStdFLA"].ToString();
                if (objMain.BeltSupplyFanStdFLA != "0.00")
                {
                    objMain.BeltSupplyFanStdQty = "1";
                    objMain.BeltSupplyFanStdPhase = phaseInt.ToString();
                }
                else
                {
                    objMain.BeltSupplyFanStdQty = "NA";
                    objMain.BeltSupplyFanStdPhase = "NA";                    
                }

                objMain.BeltSupplyFanOvrFLA = row["Belt_SupplyFanOvrFLA"].ToString();
                if (objMain.BeltSupplyFanOvrFLA != "0.00")
                {
                    objMain.BeltSupplyFanOvrQty = "1";
                    objMain.BeltSupplyFanOvrPhase = phaseInt.ToString();
                }
                else
                {
                    objMain.BeltSupplyFanOvrQty = "NA";
                    objMain.BeltSupplyFanOvrPhase = "NA";
                }

                objMain.CondenserFanStdFLA = row["CondFanStdFLA"].ToString();
                if (objMain.CondenserFanStdFLA != "0.00")
                {
                    objMain.CondenserFanStdQty = "1";
                    objMain.CondenserFanStdPhase = phaseInt.ToString();
                }
                else
                {
                    objMain.CondenserFanStdQty = "NA";
                    objMain.CondenserFanStdPhase = "NA";
                }

                objMain.CondenserFanHighFLA = row["CondFanHighFLA"].ToString();
                if (objMain.CondenserFanHighFLA != "0.00")
                {
                    objMain.CondenserFanHighQty = "1";
                    objMain.CondenserFanHighPhase = phaseInt.ToString();
                }
                else
                {
                    objMain.CondenserFanHighQty = "NA";
                    objMain.CondenserFanHighPhase = "NA";
                }

                objMain.Comp1StdRLA = row["Compr1StdRLA"].ToString();
                if (objMain.Comp1StdRLA != "0.00")
                {
                    objMain.Comp1StdQty = "1";
                    objMain.Comp1StdPhase = phaseInt.ToString();                    
                }
                else
                {
                    objMain.Comp1StdQty = "NA";
                    objMain.Comp1StdPhase = "NA";
                }

                objMain.Comp2StdRLA = row["Compr2StdRLA"].ToString();
                if (objMain.Comp2StdRLA != "0.00")
                {
                    objMain.Comp2StdQty = "1";
                    objMain.Comp2StdPhase = phaseInt.ToString();                   
                }
                else
                {
                    objMain.Comp2StdQty = "NA";
                    objMain.Comp2StdPhase = "NA";
                }

                objMain.Comp3StdRLA = row["Compr3StdRLA"].ToString();
                if (objMain.Comp3StdRLA != "0.00")
                {
                    objMain.Comp3StdQty = "1";
                    objMain.Comp3StdPhase = phaseInt.ToString();                    
                }
                else
                {
                    objMain.Comp3StdQty = "NA";
                    objMain.Comp3StdPhase = "NA";
                }

                objMain.Comp1HighRLA = row["Compr1HighRLA"].ToString();
                if (objMain.Comp1HighRLA != "0.00")
                {
                    objMain.Comp1HighQty = "1";
                    objMain.Comp1HighPhase = phaseInt.ToString();                    
                }
                else
                {
                    objMain.Comp1HighQty = "NA";
                    objMain.Comp1HighPhase = "NA";
                }

                objMain.Comp2HighRLA = row["Compr2HighRLA"].ToString();
                if (objMain.Comp2HighRLA != "0.00")
                {
                    objMain.Comp2HighQty = "1";
                    objMain.Comp2HighPhase = phaseInt.ToString();                    
                }
                else
                {
                    objMain.Comp2HighQty = "NA";
                    objMain.Comp2HighPhase = "NA";
                }
               
                if (row["ExhaustFanFLA"].ToString() != "0.00")
                {
                    objMain.ExhaustFanQty = "1";
                    objMain.ExhaustFanPhase = phaseInt.ToString();
                    objMain.ExhaustFanFLA = row["ExhaustFanFLA"].ToString();
                    objMain.ExhaustFanHP = exhaustFanHP;
                }
                else
                {
                    objMain.ExhaustFanQty = "NA";
                    objMain.ExhaustFanPhase = "NA";
                    objMain.ExhaustFanFLA = "0.00";
                    objMain.ExhaustFanHP = "NA";
                }

                if (ervType == "SemcoWheel")
                {
                    if (row["SemcoWheelFLA"].ToString() != "0.00")
                    {
                        objMain.SemcoWheelQty = "1";
                        objMain.SemcoWheelPhase = phaseInt.ToString();
                        objMain.SemcoWheelFLA = row["SemcoWheelFLA"].ToString();
                        objMain.SemcoWheelHP = ervWheelHP;
                    }                    
                }
                else
                {
                    objMain.SemcoWheelQty = "NA";
                    objMain.SemcoWheelPhase = "NA";
                    objMain.SemcoWheelFLA = "0.00";
                    objMain.SemcoWheelHP = "NA";
                }

                if (ervType == "AirXchange")
                {
                    if (row["AirXchangeWheelFLA"].ToString() != "0.00")
                    {
                        objMain.AirXchangeWheelQty = "1";
                        objMain.AirXchangeWheelPhase = phaseInt.ToString();
                        objMain.AirXchangeWheelFLA = row["AirXchangeWheelFLA"].ToString();
                        objMain.AirXchangeWheelHP = ervWheelHP;
                    }                    
                }
                else
                {
                    objMain.AirXchangeWheelQty = "NA";
                    objMain.AirXchangeWheelPhase = "NA";
                    objMain.AirXchangeWheelFLA = "0.00";
                    objMain.AirXchangeWheelHP = "NA";
                }

                objMain.CalculateMonitorAmpacityValues(out minCKTAmpStr, out mopStr, out rawMopStr, modelNoStr);

                objMain.MinCKTAmp = minCKTAmpStr;
                objMain.MfsMcb = mopStr;
                objMain.RawMOP = rawMopStr;

                lbMCA.Text = minCKTAmpStr;
                lbMOP.Text = mopStr;

                objMain.ModelNoVerifiedBy = "OAU_Config";
                objMain.French = "French";

                try
                {
                    objMain.MonitorInsertEtl();
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error writing to the MonitorEtl Table - " + f);
                }
            }          
            else
            {
                MessageBox.Show("ERROR - No entry was found for this combination of Digits 1, 2, 456, 7, 8, 22,  & 33!");
            }           
        }

        private void writeMSP_EtlStickerData(string jobNumStr, string orderNumStr, string orderLineStr, string releaseNumStr, string modelNoStr)
        {
            string voltageStr = "";            
            string partNumStr = "";
            string partCategoryStr = "";
            string maxOutletAirTempStr = "0";
            string minCKTAmpStr = "";
            string mfsMCBStr = "";            
            string rawMopStr = "";
            string mfgDateStr = "";
            string heatTypeStr = "";            
            string qtyStr = "";
            string kwStr = "";
            string supplyFanQtyStr = "";
            string supplyFanFLAStr = "";
            string Digit17Values = "A,B,C";
            string serialNoStr = orderNumStr + "-" + orderLineStr + "-" + releaseNumStr;   
           
            int voltageInt = 0;
            int phaseInt = 0;
            int supplyFanQtyInt = 0;
            int pos = 0;

            int dashPos = 0;
            int dashPos2 = 0;

            decimal hertzDec = 0;
           
            double supplyFanFLADbl = 0;
            double electricHeatFLADbl = 0;
            double tempDbl = 0;
            double controlAmpDbl = 0;
            double mcaAmpsDbl = 0;    
            double mopDbl = 0;
            double largestLoadDbl = 0;

            //DateTime mfgDateDate;           
                    
            //mfgDateStr = DateTime.Today.ToShortDateString();
            //mfgDateDate = Convert.ToDateTime(mfgDateStr);

            switch (modelNoStr.Substring(6, 1)) //Voltage 
            {
                case "1":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    //elecRatingStr = "208-230/60/1";
                    //operatingVoltsStr = "187 / 253";
                    break;                
                case "3":
                    voltageInt = 460;
                    hertzDec = 60;
                    phaseInt = 3;
                    //elecRatingStr = "460/60/3";
                    //operatingVoltsStr = "414 / 506";
                    break;
                case "5":
                    voltageInt = 575;
                    hertzDec = 60;
                    phaseInt = 3;
                    //elecRatingStr = "575/60/3";
                    //operatingVoltsStr = "517 / 633";
                    break;
                default:
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
            }

            voltageStr = voltageInt.ToString();

            switch (modelNoStr.Substring(15, 1)) //Post Heating Coil Type
            {
                case "0":
                    heatTypeStr = "None";
                    break;
                case "1":
                    heatTypeStr = "DX (HeatPump)";
                    break;
                case "2":
                    heatTypeStr = "HotWater";
                    break;
                case "3":
                    heatTypeStr = "Electric";
                    break;
                case "4":
                    heatTypeStr = "Gas";
                    break;                
                default:
                    heatTypeStr = "Unknown";
                    break;
            }         
           
            objMain.JobNum = jobNumStr;
            objMain.SerialNo = serialNoStr;
            objMain.OrderNum = Int32.Parse(orderNumStr);
            objMain.OrderLine = Int32.Parse(orderLineStr);
            objMain.ReleaseNum = Int32.Parse(releaseNumStr);
            objMain.MfgDate = "";
            objMain.ModelNo = modelNoStr;
            objMain.Voltage = voltageInt.ToString();               
            objMain.HeatingType = heatTypeStr;

            //foreach (DataGridViewRow dr in dgvBOM.Rows)
            //{
            //    partNumStr = dr.Cells["PartNum"].Value.ToString();

            //    partCategoryStr = dr.Cells["PartCategory"].ToString();

            //    //if (partNumStr == "VCPRFG-0991")
            //    //{
            //    //    partCategoryStr = dr["PartCategory"].ToString();
            //    //}

            //    if (partCategoryStr.Contains("Motor") || partCategoryStr.Contains("Mtr"))
            //    {
            //        DataTable dtMotor = objMain.GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);

            //        if (dtMotor.Rows.Count > 0)
            //        {
            //            DataRow drMotor = dtMotor.Rows[0];
            //            if (dr.Cells["PartType"].Value.ToString() == "SupplyFan")
            //            {
            //                qtyStr = dr.Cells["ReqQty"].Value.ToString();
            //                supplyFanQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
            //                supplyFanQtyInt = Int32.Parse(supplyFanQtyStr);
            //                supplyFanFLAStr = drMotor["FLA"].ToString();
            //                supplyFanFLADbl = double.Parse(supplyFanFLAStr);
            //            }
            //        }
            //    }
            //}

            //try
            //{
            //    if (Digit17Values.Contains(modelNoStr.Substring(16, 1)) == true)  // If Digit 17 equals A, B, or C
            //    {
            //        DataTable dtModel = objMain.MSP_GetModelNumDesc(17, "NA", modelNoStr.Substring(16, 1));

            //        if (dtModel.Rows.Count > 0) // Calculate Heating electric input value.
            //        {
            //            DataRow drModel;
            //            drModel = dtModel.Rows[0];

            //            kwStr = drModel["DigitDescription"].ToString();
            //            kwStr = objMain.stripKwFromDescription(kwStr);

            //            kwStr = kwStr + "000";
            //            tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
            //            tempDbl = tempDbl / Math.Sqrt(3);
            //            electricHeatFLADbl = tempDbl;
            //        }
            //    }
            //    else
            //    {
            //        electricHeatFLADbl = 0;
            //    }
            //}
            //catch (Exception f)
            //{
            //    MessageBox.Show("Error Reading MSP_ModelNumDesc Table for digit 17 - " + f);
            //}

            if (modelNoStr.Substring(6, 1) == "1")
            {
                controlAmpDbl = 2.4;
            }
            else
            {
                controlAmpDbl = 1.0;
            }

            if (modelNoStr.Substring(15, 1) == "3")  //IF Digit16 equals 3 then unit has electric heat.
            {
                mcaAmpsDbl = 1.25 * (supplyFanFLADbl + electricHeatFLADbl + controlAmpDbl);
                mfsMCBStr = objMain.roundElecMOP(mcaAmpsDbl);
            }
            else
            {
                largestLoadDbl = supplyFanFLADbl;
                --supplyFanQtyInt;

                mcaAmpsDbl = (1.25 * largestLoadDbl) + (electricHeatFLADbl + (supplyFanFLADbl * supplyFanQtyInt) + controlAmpDbl);

                mopDbl = (2.25 * largestLoadDbl) + (electricHeatFLADbl + (supplyFanFLADbl * supplyFanQtyInt) + controlAmpDbl);
                mfsMCBStr = objMain.roundMOP(mopDbl);
            }

            minCKTAmpStr = mcaAmpsDbl.ToString("f2");

            rawMopStr = mopDbl.ToString("f2");

            try
            {                                                
                objMain.MSPDesignation = "";
                objMain.ModelNo = modelNoStr;
                objMain.PartNum = modelNoStr.Substring(0,6);
                objMain.SerialNo = serialNoStr;
                objMain.MfgDate = mfgDateStr;
                objMain.MSPFanFilter = "";
                objMain.ModelNoVerifiedBy = "";
                
                objMain.MSPFanPower = "";
                objMain.MSPFanVoltage = "";
                objMain.MSPFanDraw = "";
                //if (modelNoStr.Substring(1, 1) == "U")
                //{
                //    objMain.MSPFanDraw = "";
                //    objMain.MSPFanVoltage = this.tbNewMSPFanVoltageDraw.Text; 
                //}
                //else
                //{
                //    objMain.MSPFanDraw = this.tbNewMSPFanVoltageDraw.Text;
                //    objMain.MSPFanVoltage = ""; 
                //}
                
                objMain.Refrigerant = "";
                objMain.AmbientTemp = "";
                 objMain.MSPFanAirVolume = "";
                objMain.MSPFanExternalSP = "";
                objMain.MSPFanTotalSP = "";
            
                //if (tbNewMSPFanQty.Text.Length == 0 )
                //{
                    objMain.MSPFanQty = "1";
                //}
                //else
                //{
                //    objMain.MSPFanQty = tbNewMSPFanQty.Text;
                //}

                objMain.Dehumidifier_BTUH = "";
                objMain.Dehumidifier_EWT = "";
                objMain.Dehumidifier_LWT = "";

                //if (this.lbDehumidGPM_SST.Text == "GPM")
                //{
                //    objMain.Dehumidifier_GPM = this.tbNewMSPDehumidGPM_SST.Text;
                    objMain.Dehumidifier_SST = "";
                //}
                //else
                //{
                //    objMain.Dehumidifier_SST = this.tbNewMSPDehumidGPM_SST.Text;
                    objMain.Dehumidifier_GPM = "";
                //}

                objMain.Dehumidifier_PD = "";
                objMain.Dehumidifier_Glycol = "";
                objMain.Dehumidifier_EAT = "";
                objMain.Dehumidifier_LAT = "";
                objMain.Dehumidifier_CAPACITY = "";

                objMain.PostCooling_BTUH = "";
                objMain.PostCooling_EWT = "";
                objMain.PostCooling_LWT = "";

                //if (this.lbPostCoolingGPM_SST.Text == "GPM")
                //{
                //    objMain.PostCooling_GPM = this.tbNewMSPPostCoolingGPM_SST.Text;
                    objMain.PostCooling_SST = "";   
                //}
                //else
                //{
                //    objMain.PostCooling_SST = this.tbNewMSPPostCoolingGPM_SST.Text;
                    objMain.PostCooling_GPM = "";
                //}
                           
                objMain.PostCooling_PD = "";
                objMain.PostCooling_Glycol = "";
                objMain.PostCooling_EAT = "";
                objMain.PostCooling_LAT = "";

                objMain.PostHeating_BTUH = "";
                objMain.PostHeating_EWT = "";
                objMain.PostHeating_LWT = "";
                objMain.PostHeating_GPM = "";
                objMain.PostHeating_PD = "";
                objMain.PostHeating_Glycol = "";
                objMain.PostHeating_EAT = "";
                objMain.PostHeating_LAT = "";

                objMain.ModelNoVerifiedBy = "";               
                objMain.French = "False";
               


                objMain.MSP_InsertEtl();
            }
            catch (Exception f)
            {
                MessageBox.Show("Error writing to the MSP_Etl Table - " + f);
            }          

        }

        private string getMinInputBTU(string mbhStr, string unitType)
        {
            string minInputBTUStr = "";

            decimal mbhDec;
            decimal mbhKW_Dec;

            if (Int32.Parse(mbhStr) <= 125)
            {
                mbhStr = mbhStr + "000";
                mbhDec = decimal.Parse(mbhStr) * .2m;  // Change from .25m to .2m - 7/28/15 per Dale - Reflects changes from 8:1 turndown to a 10:1 turndown
                //minInputBTUStr = mbhDec.ToString("f0");
            }
            else if ((Int32.Parse(mbhStr) <= 400) || ((Int32.Parse(mbhStr) == 500) && (unitType == "N" || unitType == "G")) || ((Int32.Parse(mbhStr) == 600) && unitType == "G"))
            {
                mbhStr = mbhStr + "000";
                mbhDec = decimal.Parse(mbhStr) * .1m;  // Change from .125m to .1m - 7/28/15 per Dale - Reflects changes from 8:1 turndown to a 10:1 turndown                
            }
            //else if (Int32.Parse(mbhStr) == 1000)   // Changed 7/28/15 per Dale - Reflects changes from 8:1 turndown to a 10:1 turndown
            //{
            //    mbhStr = mbhStr + "000";
            //    mbhDec = decimal.Parse(mbhStr) * .03125m;
            //    minInputBTUStr = mbhDec.ToString("f0");
            //}
            else
            {
                mbhStr = mbhStr + "000";
                mbhDec = decimal.Parse(mbhStr) * .05m;  // Change from .0625m to .05m - 7/28/15 per Dale - Reflects changes from 8:1 turndown to a 10:1 turndown
                //minInputBTUStr = mbhDec.ToString("f0");
            }

            mbhKW_Dec = mbhDec * 0.000293m;
            minInputBTUStr = mbhDec.ToString("f0") + "(" + mbhKW_Dec.ToString("f0") + "kW)";

            return minInputBTUStr;

        }

        private void pictureBoxSearchIcon_MouseClick(object sender, MouseEventArgs e)
        {
            createPartList();
        }

        private void createPartList()
        {
            string partNumStr;

            partNumStr = txtModPartNum.Text.ToUpper();            

            try
            {
                DataTable dtParts = objMain.GetOAU_PartNumbers(partNumStr, objMain.UnitDesign);

                if (dtParts.Rows.Count == 1)
                {                
                    DataRow dr = dtParts.Rows[0];

                    txtModPartNum.Text = dr["PartNum"].ToString();
                    txtModPartDes.Text = dr["PartDescription"].ToString();
                    txtModUOM.Text = dr["UOM"].ToString();
                    txtModPartCategory.Text = dr["PartCategory"].ToString();
                    txtModWhseLoc.Text = dr["WhseCode"].ToString();

                    if (this.txtModPartCategory.Text.Contains("Motor") || this.txtModPartCategory.Text.Contains("Mtr"))
                    {
                        this.cbModMotorType.Enabled = true;
                        this.cbModMotorType.SelectedIndex = 0;
                    }

                    if (txtModAsmSeq.Text == "0")
                    {
                        txtModRelOP.Text = dr["RelatedOperation"].ToString();
                    }
                    else
                    {
                        txtModRelOP.Text = "10";
                    }
                    txtModUnitCost.Text = dr["UnitCost"].ToString();
                    txtModStatus.Text = dr["Status"].ToString();
                    lbModPartStatus.Text = dr["PartStatus"].ToString();
                    lbModCostMethod.Text = dr["CostMethod"].ToString();
                    lbModRevNum.Text = dr["RevisionNum"].ToString();
                    lbModJobMtlProgressRecID.Text = dr["RuleHeadID"].ToString();
                    
                    txtModPartQty.Focus();
                    txtModPartQty.Select();
                }
                else
                {
                    dgvPartSearchResults.DataSource = dtParts;
                    dgvPartSearchResults.Columns["PartNum"].Width = 150;
                    dgvPartSearchResults.Columns["PartNum"].HeaderText = "Part Number";
                    dgvPartSearchResults.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    dgvPartSearchResults.Columns["PartNum"].DisplayIndex = 0;
                    dgvPartSearchResults.Columns["PartDescription"].Width = 375;
                    dgvPartSearchResults.Columns["PartDescription"].HeaderText = "Part Description";
                    dgvPartSearchResults.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    dgvPartSearchResults.Columns["PartDescription"].DisplayIndex = 1;
                    dgvPartSearchResults.Columns["PartCategory"].Width = 150;
                    dgvPartSearchResults.Columns["PartCategory"].HeaderText = "Category";
                    dgvPartSearchResults.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    dgvPartSearchResults.Columns["PartCategory"].DisplayIndex = 2;
                    dgvPartSearchResults.Columns["RelatedOperation"].Visible = false;
                    dgvPartSearchResults.Columns["UnitCost"].Visible = false;
                    dgvPartSearchResults.Columns["UOM"].Visible = false;                                       
                    dgvPartSearchResults.Columns["WhseCode"].Visible = false;
                    dgvPartSearchResults.Columns["Status"].Visible = false;
                    dgvPartSearchResults.Columns["PartStatus"].Visible = false;
                    dgvPartSearchResults.Columns["CostMethod"].Visible = false;
                    dgvPartSearchResults.Columns["RevisionNum"].Visible = false;                   
                    dgvPartSearchResults.Columns["RuleHeadID"].Visible = false;
                    gbModPartSearchResults.Visible = true;
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error occurred reading part table ==> " + f);
            }
        }

        private void txtModPartCategory_TextChanged(object sender, EventArgs e)
        {
            if (txtModPartCategory.Text != "")
            {
                if (txtModPartCategory.Text == "Motor")
                {
                    cbModMotorType.Enabled = true;
                    cbModMotorType.SelectedIndex = 0;
                }
                else
                {
                    cbModMotorType.Enabled = false;
                }
            }
        }

        private void btnAddPart_Click(object sender, EventArgs e)
        {
            if (lbJobNum.Text != "")
            {
                txtModAsmSeq.Enabled = true;
                txtModAsmSeq.Text = "";
                txtModPartNum.Enabled = true;
                txtModRelOP.Enabled = true;
                btnModUpdate.Text = "Add";
                btnModUpdate.Enabled = true;
                btnModDelete.Visible = false;
                btnExit.Enabled = false;
                btnAddPart.Enabled = false;
                btnSave.Enabled = false;
                btnPrint.Enabled = false;
                gbAddModDelPart.BringToFront();
                gbAddModDelPart.Visible = true;
                txtModAsmSeq.Focus();
                txtModAsmSeq.Select();

            }
        }

        private void btnModExit_Click(object sender, EventArgs e)
        {
            gbAddModDelPart.Visible = false;
            resetScreen();
        }

        private void btnModUpdate_Click(object sender, EventArgs e)
        {            
            int progressRecid;
            int asmSeqInt = 0;            

            string errors = "";
            string motorType = "";
            string jobNum = "";
            string recId = "";
            string modelNo = "";            
            string asmSeqStr = "0";
            string parentPartNumStr = "";
            string seqNoStr = "";
            string nextSeqNoStr = "";
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string unitTypeStr = "REV5";
            string ruleHeadID = "0";
            string ruleBatchID = "0";
            string modelTypeStr = lbModelNo.Text.Substring(0,3);

            decimal reqQty = 0;

            bool insertPartBool = true;

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            jobNum = lbJobNum.Text;
            modelNo = lbModelNo.Text;
            recId = lbModJobMtlProgressRecID.Text;
            progressRecid = Int32.Parse(recId);

            if (modelNo.Length == 43)
            {
                unitTypeStr = "MON";
            }
            else if (modelNo.Length == 69)
            {
                if (modelNo.StartsWith("HA"))
                {
                    unitTypeStr = "VKG";
                }
                else
                {
                    unitTypeStr = "REV6";
                }
            }

            if (txtModAsmSeq.Text.Length == 0)
            {
                errors += "ERROR - AssemblySeq number is a required field! Please enter the AssemblySeq number.\n";
            }
            else
            {
                asmSeqStr = txtModAsmSeq.Text;

                foreach (DataGridViewRow dr in dgvBOM.Rows)
                {
                    seqNoStr = dr.Cells["SeqNo"].Value.ToString();

                    if (dr.Cells["AssemblySeq"].Value.ToString() == txtModAsmSeq.Text)
                    {
                        if (seqNoStr != "9998" && seqNoStr != "9999")
                        {
                            nextSeqNoStr = seqNoStr;
                        }
                    }
                }
            }

            //if (nextSeqNoStr != "")
            //{
            //    txtModSeqNo.Text = (Int32.Parse(nextSeqNoStr) + 10).ToString();
            //}
            //else
            //{
            //    errors += "ERROR - SeqNo not found! Please enter a valid integer SeqNo.\n";
            //}

            if (txtModPartQty.Text.Length == 0)
            {
                errors += "ERROR - Required Qty is a required field!\n";
            }
            else
            {
                try
                {
                    reqQty = decimal.Parse(txtModPartQty.Text);
                }
                catch
                {
                    errors += "ERROR - Invalid Req Qty value. Required Qty must be numeric!\n";
                }
            }

            if (txtModPartCategory.Text == "Motor" && cbModMotorType.SelectedIndex == 0)
            {
                errors += "ERROR - The Part Category 'Motor' requires you to select a Motor Type!\n";
            }
            else
            {
                motorType = cbModMotorType.Text;
            }

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
            }
            else
            {
                string epicorServer = "";

                epicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

#if Debug
            epicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
#endif           

                string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;

                Console.WriteLine(epicorServer);

                BuildBOM bb = new BuildBOM(epicorServer, epicorSqlConnectionString);

                if (btnModUpdate.Text == "Add")
                {
                    if (lbModPartStatus.Text == "InActive")
                    {
                        DialogResult result1 = MessageBox.Show("Part Number => " + txtModPartNum.Text + " shows an InActive status. Do you wish to include this Part Number in the BOM?",
                                                                "WARNING",
                                                                MessageBoxButtons.YesNo);
                        if (result1 == DialogResult.No)
                        {
                            insertPartBool = false;
                        }
                        else
                        {
                            insertPartBool = true;
                        }
                    }

                    if (txtModAsmSeq.Text.Length > 0)
                    {
                        asmSeqStr = txtModAsmSeq.Text;
                    }

                    asmSeqInt = Int32.Parse(asmSeqStr);

                    //if (asmSeqInt == 1)
                    //{
                    //    parentPartNumStr = "OACONTROLPANEL";
                    //}
                    //else if (asmSeqInt > 1)
                    //{
                        //foreach(DataGridViewRow dr in dgvBOM.Rows)
                        //{
                        //    if ( (int)dr.Cells["AssemblySeq"].Value == asmSeqInt)
                        //    {
                        //        parentPartNumStr = dr.Cells["ParentPartNum"].Value.ToString();
                        //        break;
                        //    }
                        //}
                    //}                    

                    if (insertPartBool == true)
                    {                          
                        string reqQtyStr = txtModPartQty.Text;                       

                        lbMsg.Text = bb.SavePart(jobNum, txtModPartNum.Text, lbModRevNum.Text, reqQty, asmSeqInt, 
                                                 Int32.Parse(txtModRelOP.Text), txtModPartCategory.Text, parentPartNumStr,
                                                 Int32.Parse(ruleBatchID), lbModCostMethod.Text, motorType, modelTypeStr,
                                                 lbModPartStatus.Text, "ADD");

                        //lbMsg.Text = "Adding " + txtModPartNum.Text + " to respective tables in Epicor for Job# " + jobNum;
                        //objMain.Insert_SinglePartJobMtl(jobNum, Int32.Parse(txtModSeqNo.Text), txtModPartNum.Text, txtModPartDes.Text, decimal.Parse(reqQtyStr), txtModUOM.Text, 
                        //                                decimal.Parse(txtModUnitCost.Text), (decimal.Parse(txtModUnitCost.Text) * reqQty), txtModPartCategory.Text, motorType, 
                        //                                txtModWhseLoc.Text, Int32.Parse(txtModRelOP.Text), txtModStatus.Text, lbModRevNum.Text, lbModCostMethod.Text, 
                        //                                Guid.NewGuid().ToString(), Int32.Parse(reqQtyStr), asmSeqInt, parentPartNumStr, Int32.Parse(ruleHeadID), Int32.Parse(ruleBatchID), modByStr);                        
                    }                                              
                }
                else
                {
                    //objMain.UpdateJobMtl(jobNum, txtModPartNum.Text, reqQty, motorType, progressRecid, modByStr);
                    lbMsg.Text = bb.SavePart(jobNum, txtModPartNum.Text, lbModRevNum.Text, reqQty, asmSeqInt,
                                                 Int32.Parse(txtModRelOP.Text), txtModPartCategory.Text, parentPartNumStr,
                                                 Int32.Parse(ruleBatchID), lbModCostMethod.Text, motorType, modelTypeStr,
                                                 lbModPartStatus.Text, "UPDATED");
                }

                if (txtModPartCategory.Text.Contains("Motor") || txtModPartCategory.Text.Contains("Compressor") || 
                    txtModPartCategory.Text.Contains("DigitalScroll") || txtModPartCategory.Text.Contains("TandemComp"))
                {
                    //MessageBox.Show("WARNING - The MCA & MOP values have changed. You will need to run the RecalculateMCA process.");                            
                    System.Threading.Thread.Sleep(2000);
                    lbMsg.Text = "MCA & MOP values have changed, updating ETL Sticker data for Job# " + jobNum;
                    objMain.DeleteEtlOAUData(jobNum);
                    refreshPartsList(jobNum, unitTypeStr);
                    addETLStickerData(jobNum, unitTypeStr);
                    //lbMCA.Text = "";
                    //lbMOP.Text = "";
                    //lbRawMOP.Text = "";
                }
                lbMsg.Text = "Updates complete.";
                gbAddModDelPart.Visible = false;

                string partsOrdered = objMain.GetOA_SchedulePartsOrdered(jobNum);
                string partsAddedBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                if (partsOrdered == "True")
                {
                    MessageBox.Show("The Parts for this Job# " + jobNum +
                                    " have already been ordered, Please, notify purchasing that this part has been added/updated!",
                                    "WARNING WARNING WARNING");
                    objMain.UpdateOA_SchedulePartsAdded(jobNum, DateTime.Now, partsAddedBy);
                }
                resetScreen();
                refreshPartsList(jobNum, unitTypeStr);
            }
        }

        private void btnSearchResultsCancel_Click(object sender, EventArgs e)
        {
            gbModPartSearchResults.Visible = false;
        }

        private void txtModPartNum_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                createPartList();
            }
        }

        private void dgvPartSearchResults_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            selectPartFromList(e.RowIndex);
        }

        private void selectPartFromList(int rowIdx)
        {
            txtModPartNum.Text = dgvPartSearchResults.Rows[rowIdx].Cells[0].Value.ToString();
            txtModPartDes.Text = dgvPartSearchResults.Rows[rowIdx].Cells[1].Value.ToString();
            txtModPartCategory.Text = dgvPartSearchResults.Rows[rowIdx].Cells[2].Value.ToString();
            txtModRelOP.Text = dgvPartSearchResults.Rows[rowIdx].Cells[3].Value.ToString();
            txtModUnitCost.Text = dgvPartSearchResults.Rows[rowIdx].Cells[4].Value.ToString();
            txtModUOM.Text = dgvPartSearchResults.Rows[rowIdx].Cells[5].Value.ToString();
            txtModWhseLoc.Text = dgvPartSearchResults.Rows[rowIdx].Cells[6].Value.ToString();
            txtModStatus.Text = dgvPartSearchResults.Rows[rowIdx].Cells[7].Value.ToString();
            lbModPartStatus.Text = dgvPartSearchResults.Rows[rowIdx].Cells[8].Value.ToString();
            lbModCostMethod.Text = dgvPartSearchResults.Rows[rowIdx].Cells[9].Value.ToString();
            lbModRevNum.Text = dgvPartSearchResults.Rows[rowIdx].Cells[10].Value.ToString();
            lbModJobMtlProgressRecID.Text = dgvPartSearchResults.Rows[rowIdx].Cells[11].Value.ToString();

            if (this.txtModPartCategory.Text.Contains("Motor") || this.txtModPartCategory.Text.Contains("Mtr"))
            {
                this.cbModMotorType.Enabled = true;
                this.cbModMotorType.SelectedIndex = 0;
            }
            
            if (txtModSeqNo.Text == "")
            {   
                txtModSeqNo.Focus();
                txtModSeqNo.Select();
            }
            else
            {
                txtModPartQty.Focus();
                txtModPartQty.Select();
            }

            btnAddPart.Enabled = true;
            btnExit.Enabled = true;
            gbModPartSearchResults.Visible = false;

        }

        private void refreshPartsList(string jobNumStr, string unitTypeStr)
        {
            string modelNo = lbModelNo.Text;

            dgvBOM.DataSource = null;
            DataTable dtBOM = new DataTable();

            dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, unitTypeStr);
          
            //if (dtBOM.Rows.Count > 0)
            //{
            //    mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr);
            //}

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();            
            dgvBOM.DataSource = dtBOM;

            //dgvBOM.Columns["PartCategory"].Width = 150;
            //dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            //dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            //dgvBOM.Columns["AssemblySeq"].Width = 50;
            //dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            //dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            //dgvBOM.Columns["SeqNo"].Width = 50;
            //dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            //dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            //dgvBOM.Columns["PartNum"].Width = 150;
            //dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            //dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            //dgvBOM.Columns["Description"].Width = 300;
            //dgvBOM.Columns["Description"].HeaderText = "Part Description";
            //dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvBOM.Columns["Description"].DisplayIndex = 4;
            //dgvBOM.Columns["ReqQty"].Width = 75;
            //dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            //dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            //dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            //dgvBOM.Columns["UOM"].Width = 50;
            //dgvBOM.Columns["UOM"].HeaderText = "UOM";
            //dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvBOM.Columns["UOM"].DisplayIndex = 6;
            //dgvBOM.Columns["UnitCost"].Width = 75;
            //dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            //dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            //dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            //dgvBOM.Columns["WhseCode"].Width = 75;
            //dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            //dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            //dgvBOM.Columns["RelOp"].Width = 70;
            //dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            //dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            //dgvBOM.Columns["Status"].Width = 75;
            //dgvBOM.Columns["Status"].HeaderText = "Status";
            //dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvBOM.Columns["Status"].DisplayIndex = 10;
            //dgvBOM.Columns["PartStatus"].Width = 60;
            //dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            //dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            //dgvBOM.Columns["ParentPartNum"].Width = 120;
            //dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            //dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            //dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            //dgvBOM.Columns["RevisionNum"].Visible = false;
            ////dgvBOM.Columns["CostMethod"].Visible = false;
            //dgvBOM.Columns["PartType"].Visible = false;
            //dgvBOM.Columns["PROGRESS_RECID"].Visible = false;
            ////dgvBOM.Columns["RuleHeadID"].Visible = false;

            DataTable dtEtl = objMain.GetEtlData(jobNumStr);

            if (dtEtl.Rows.Count > 0)
            {
                DataRow drEtl = dtEtl.Rows[0];
                lbMCA.Text = drEtl["MinCKTAmp"].ToString();
                lbMOP.Text = drEtl["MFSMCB"].ToString();
                lbRawMOP.Text = drEtl["MOP"].ToString();
            }
        }

        private void btnSearchResultsOK_Click(object sender, EventArgs e)
        {
            int rowIdx = 0;

            foreach (DataGridViewRow dr in dgvBOM.Rows)
            {                
                if (dr.Selected == true)
                {
                    break;
                }
                ++rowIdx;
            }
            selectPartFromList(rowIdx);
        }

        private void resetScreen()
        {
            txtModPartCategory.Text = "";
            txtModPartDes.Text = "";
            txtModPartNum.Text = "";
            txtModPartQty.Text = "";
            txtModRelOP.Text = "";
            txtModSeqNo.Text = "";
            txtModStatus.Text = "";
            txtModUnitCost.Text = "";
            txtModUOM.Text = "";
            txtModWhseLoc.Text = "";
            cbModMotorType.Text = "";
            btnExit.Enabled = true;
            btnAddPart.Enabled = true;
            btnSave.Enabled = true;
            btnPrint.Enabled = true;
        }

        private void dgvBOM_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = e.RowIndex;
            int progressRecid = 0;

            string motorTypeStr = "";
            string tempStr = "";
        
            decimal tempDec = 0;

            tempStr = dgvBOM.Rows[rowIdx].Cells["PROGRESS_RECID"].Value.ToString();
            if (tempStr != "")
            {
                progressRecid = Int32.Parse(tempStr);
            }

            if (progressRecid != 0)
            {
                txtModPartCategory.Text = dgvBOM.Rows[rowIdx].Cells["PartCategory"].Value.ToString();
                txtModPartCategory.Enabled = false;
                txtModAsmSeq.Text = dgvBOM.Rows[rowIdx].Cells["AssemblySeq"].Value.ToString();
                txtModAsmSeq.Enabled = false;
                txtModSeqNo.Text = dgvBOM.Rows[rowIdx].Cells["SeqNo"].Value.ToString();
                txtModSeqNo.Enabled = false;
                txtModPartNum.Text = dgvBOM.Rows[rowIdx].Cells["PartNum"].Value.ToString();
                txtModPartNum.Enabled = false;
                txtModPartDes.Text = dgvBOM.Rows[rowIdx].Cells["Description"].Value.ToString();
                tempDec = (decimal)dgvBOM.Rows[rowIdx].Cells["ReqQty"].Value;
                txtModPartQty.Text = tempDec.ToString("f2");
                txtModUOM.Text = dgvBOM.Rows[rowIdx].Cells["UOM"].Value.ToString();
                tempDec = (decimal)dgvBOM.Rows[rowIdx].Cells["UnitCost"].Value;
                txtModUnitCost.Text = tempDec.ToString("f2");

                txtModWhseLoc.Text = dgvBOM.Rows[rowIdx].Cells["WhseCode"].Value.ToString();
                txtModRelOP.Text = dgvBOM.Rows[rowIdx].Cells["RelOp"].Value.ToString();
                lbModPartStatus.Text = dgvBOM.Rows[rowIdx].Cells["PartStatus"].Value.ToString();
                lbModRevNum.Text = dgvBOM.Rows[rowIdx].Cells["RevisionNum"].Value.ToString();
                //lbModCostMethod.Text = dgvBOM.Rows[rowIdx].Cells["CostMethod"].Value.ToString();
                motorTypeStr = dgvBOM.Rows[rowIdx].Cells["PartType"].Value.ToString();

                if (motorTypeStr != "")
                {
                    cbModMotorType.Text = motorTypeStr;
                }

                lbModParentPartNum.Text = dgvBOM.Rows[rowIdx].Cells["ParentPartNum"].Value.ToString();
                txtModStatus.Text = dgvBOM.Rows[rowIdx].Cells["Status"].Value.ToString();
                lbModJobMtlProgressRecID.Text = progressRecid.ToString();
     
                btnModDelete.Enabled = true;
                btnModDelete.Visible = true;
                btnModUpdate.Text = "Update";
                btnModUpdate.Enabled = true;
                btnExit.Enabled = false;
                btnAddPart.Enabled = false;
                btnSave.Enabled = false;
                btnPrint.Enabled = false;
                gbAddModDelPart.Visible = true;
            }
            else
            {
                MessageBox.Show("WARNING - Data must be Saved first before it can be edited.");
            }
        }

        private void btnModDelete_Click(object sender, EventArgs e)
        {
            string jobNum = lbJobNum.Text;
            string partNum = txtModPartNum.Text;
            string parentPartNum = "";
            string modelNo = lbModelNo.Text;
            string unitTypeStr = "REV5";           
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string epicorServer = "";
            string errorMsg = "";

            int progressRecid = Int32.Parse(lbModJobMtlProgressRecID.Text);
            int sPos = -1;
          
            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            if (modelNo.Length == 39)
            {
                unitTypeStr = "REV5";
            }
            else if (modelNo.Length == 43)
            {
                unitTypeStr = "MON";
            }
            else if (modelNo.Length == 69)
            {
                if (modelNo.StartsWith("HA"))
                {
                    unitTypeStr = "VKG";
                }
                else
                {
                    unitTypeStr = "REV6";
                }
            }

            parentPartNum = lbModParentPartNum.Text;

            DialogResult result2 = MessageBox.Show("You have selected Part# " + partNum + " to be deleted.\n " +                                                   
                                                   "Press 'Yes' to Proceed!\n" +
                                                   "Press 'No' to Cancel!",
                                                   "Deleting a Part",
                                                    MessageBoxButtons.YesNo);

            if (result2 == DialogResult.Yes)
            {
                if (unitTypeStr != "MON")
                {
                    epicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

#if Debug
            epicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
#endif           

                    string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;

                    //Console.WriteLine(epicorServer);

                    BuildBOM bb = new BuildBOM(epicorServer, epicorSqlConnectionString);

                    errorMsg = bb.DeletePart(jobNum, txtModPartNum.Text, Int32.Parse(txtModAsmSeq.Text), Int32.Parse(txtModSeqNo.Text));

                    if (errorMsg.Length == 0)
                    {
                        refreshPartsList(jobNum, unitTypeStr);
                        lbMsg.Text = "Part deleted successfully!";
                    }

                    gbAddModDelPart.Visible = false;
                }
            }
            else
            {
                // Delete part via SQL
            }

            btnExit.Enabled = true;
            btnAddPart.Enabled = true;
            btnSave.Enabled = true;
            btnPrint.Enabled = true;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            dgvBOM.Enabled = false;
            //btnAddPart.Enabled = false;
            btnExit.Enabled = false;
            //btnSave.Enabled = false;
            btnPrint.Enabled = false;
            gbBOMPrintOptions.Location = new Point(525, 125);
            gbBOMPrintOptions.Visible = true;
        }

        private void buttonPrintOpCancel_Click(object sender, EventArgs e)
        {
            dgvBOM.Enabled = true;
            //btnAddPart.Enabled = true;
            btnExit.Enabled = true;
            //btnSave.Enabled = true;
            btnPrint.Enabled = true;
            gbBOMPrintOptions.Visible = false;
        }

        private void buttonPrintOpOK_Click(object sender, EventArgs e)
        {
            gbBOMPrintOptions.Visible = false;

            dgvBOM.Enabled = true;
            //btnAddPart.Enabled = true;
            btnExit.Enabled = true;
            //btnSave.Enabled = true;
            btnPrint.Enabled = true;

            if (rbEngBOM.Checked == true)
            {
                printEngineeringBOM();
            }
            //else if (rbProdBOM.Checked == true)
            //{
            //    //printProductionBOM("ProductionBOM");
            //}
            //else if (this.radioButtonSheetMetalKitBOM.Checked == true)
            //{
            //    printProductionBOM("SheetMetalKitBOM");
            //}
            //else if (rbPurchaseRpt.Checked == true)
            //{
            //    //printPurchasingReport();
            //}
            //else if (rbTotalCostRpt.Checked == true)
            //{
            //    //printTotalUnitCostReport();
            //}
            else if (rbCriticalCompBOM.Checked == true)
            {
                printCriticalCompBOM();
            }
            else if (rbJobNumSticker.Checked == true)
            {
                printJobNumberSticker();
            }
            else if (rbRfgCompRpt.Checked == true)
            {
                printRefrigerationCompRpt();
            }
        }

        private void deleteSubAssemblyFromBom(string parentPartNum, string jobNum, string modBy)
        {
            string partNum = "";
            string progressIdStr = "";
            string asmPartNum = "";

            int progressRecId = 0;
            int parLen = 0;
            int eqPos = 0;

            bool parentFound = false;

            if (parentPartNum.StartsWith("RFGASM"))
            {
                parLen = parentPartNum.Length;
                eqPos = parentPartNum.IndexOf(" (");
                asmPartNum = parentPartNum.Substring(0, eqPos );
            }
            else if (parentPartNum.StartsWith("PNL"))
            {
                parLen = parentPartNum.Length;
                eqPos = parentPartNum.IndexOf("=");
                asmPartNum = parentPartNum.Substring(0, (eqPos - 1));
            }
            else
            {
                asmPartNum = parentPartNum;
            }
            
           
            for (int i = dgvBOM.Rows.Count - 1; i >= 0; i--)
            {
                if (dgvBOM.Rows[i].Cells["ParentPartNum"].Value.ToString().StartsWith(asmPartNum))
                {
                    parentFound = true;
                    partNum = dgvBOM.Rows[i].Cells["PartNum"].Value.ToString();
                    progressIdStr = dgvBOM.Rows[i].Cells["PROGRESS_RECID"].Value.ToString();
                    progressRecId = Int32.Parse(progressIdStr);
                    lbMsg.Text = "Deleting SubAssembly - " + parentPartNum + " from the JobMtl table in Epicor for Job# " + jobNum;
                    objMain.DeletePartNumFromJobMtl(jobNum, partNum, progressRecId, modBy);
                    dgvBOM.Rows.Remove(dgvBOM.Rows[i]);
                }
                else
                {
                    if (parentFound == true)
                    {
                        break;
                    }
                }
            }
        }      

        private void printCriticalCompBOM()
        {            
            string superRadiatorPartNum = "";
            string superRadiatorNotes = "";
            string refChargeVal = "";

            if (lbModelNo.Text.StartsWith("HA") == true)
            {
                frmPrintBOM frmPrint = new frmPrintBOM();

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNum";
                pdv1.Value = lbJobNum.Text;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];  
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\NewCriticalCompBOM.rpt";       
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\NewCriticalCompBOM.rpt";
#endif

                cryRpt.Load(objMain.ReportString);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();

            }
            else
            {
                frmCriticalCompBOM frmCCB = new frmCriticalCompBOM();
                frmCCB.lbCritCompJobNum.Text = lbJobNum.Text;
                frmCCB.lbCritCompModelNo.Text = lbModelNo.Text;
                frmCCB.lbCriticalCompVoltage.Text = lbVoltage.Text;
                frmCCB.txtCritCompJobName.Text = lbCustName.Text;
                foreach (DataGridViewRow row in dgvBOM.Rows)
                {
                    if (row.Cells["PartNum"].Value.ToString().Contains("RFGCOL"))
                    {
                        superRadiatorPartNum = row.Cells["PartNum"].Value.ToString();
                    }
                }

                if (superRadiatorPartNum != "")
                {
                    DataTable dt = objMain.GetTandemCompAssemblyMtls(superRadiatorPartNum);
                    foreach (DataRow dr in dt.Rows)
                    {
                        superRadiatorNotes += superRadiatorPartNum + " - " + dr["PartNum"].ToString() + " - " + dr["Description"].ToString() + " -  Required Qty = " + dr["ReqQty"].ToString() + "\n";
                    }
                }
                frmCCB.richTxtCritCompNotes.Text = superRadiatorNotes;
                frmCCB.lbCir1Charge.Text = lbCir1Chrg.Text;
                frmCCB.lbCir2Charge.Text = lbCir2Chrg.Text;
                frmCCB.ShowDialog();
            }
        }

        private void printRefrigerationCompRpt()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string unitTypeStr = "REV5";

            string circuit1Charge = this.lbCir1Chrg.Text;
            string circuit2Charge = this.lbCir2Chrg.Text;
            //string qtyPer = "";            

            string digit3 = modelNoStr.Substring(2, 1);
            string digit4 = modelNoStr.Substring(3, 1);
            string digit567 = modelNoStr.Substring(4, 3);
            //string digit9 = modelNoStr.Substring(8, 1);
            string digit11 = modelNoStr.Substring(10, 1);
            //string digit12 = modelNoStr.Substring(11, 1);
            //string digit13 = modelNoStr.Substring(12, 1);
            //string digit14 = modelNoStr.Substring(13, 1);

            //string cabinetSize = "OA" + digit3;
            //string cir1Comp1_1_PartNum = "NA";
            //string cir1Comp1_2_PartNum = "NA";
            //string cir1Comp1_3_PartNum = "NA";
            //string cir1Comp2_1_PartNum = "NA";
            //string cir1Comp2_2_PartNum = "NA";
            //string cir1Comp2_3_PartNum = "NA";
            //string circuit1ChargeStr = "0";
            //string circuit2ChargeStr = "0";

            //string cir1TXV_PartNum = "NA";
            //string cir2TXV_PartNum = "NA";

            string circuit1Suction = "";
            string circuit1LiquidLine = "";
            string circuit1DischargeLine = "";
            string circuit2Suction = "";
            string circuit2LiquidLine = "";
            string circuit2DischargeLine = "";

            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();                      

            //DataSet1 ds = new DataSet1();

            //DataTable dtCCD = objMain.GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            //if (dtCCD.Rows.Count > 0)
            //{
            //    DataRow drComp = dtCCD.Rows[0];

            //    qtyPer = "0";
            //    if (drComp["Circuit1_1_Compressor"].ToString() != "NA")
            //    {
            //        cir1Comp1_1_PartNum = drComp["Circuit1_1_Compressor"].ToString();
            //        qtyPer = "1";
            //    }

            //    ds = addPartToDataSet(ds, cir1Comp1_1_PartNum, "", "Compressors1-1", "Circuit 1", qtyPer, "EA");

            //    qtyPer = "0";
            //    if (drComp["Circuit1_2_Compressor"].ToString() != "NA")
            //    {
            //        cir1Comp1_2_PartNum = drComp["Circuit1_2_Compressor"].ToString();
            //        qtyPer = "1";
            //    }
            //    ds = addPartToDataSet(ds, cir1Comp1_2_PartNum, "", "Compressor1-2", "Circuit 1", qtyPer, "EA");

            //    qtyPer = "0";
            //    if (drComp["Circuit1_3_Compressor"].ToString() != "NA")
            //    {
            //        cir1Comp1_3_PartNum = drComp["Circuit1_3_Compressor"].ToString();
            //        qtyPer = "1";
            //    }
            //    ds = addPartToDataSet(ds, cir1Comp1_3_PartNum, "", "Compressor1-3", "Circuit 1", qtyPer, "EA");

            //    if ((modelNoStr.Substring(11, 1).ToString() == "0"))
            //    {
            //        circuit1ChargeStr = drComp["Circuit1_Charge"].ToString();
            //    }
            //    else
            //    {
            //        circuit1ChargeStr = (Decimal.Parse(drComp["Circuit1_Charge"].ToString()) + Decimal.Parse(drComp["Circuit1_ReheatCharge"].ToString())).ToString();
            //    }
            //    ds = addPartToDataSet(ds, "VCPRFG-0400", "", "Compressor Charge", "Circuit 1", circuit1ChargeStr, "LBs");                

            //    qtyPer = "0";
            //    if (drComp["Circuit2_1_Compressor"].ToString() != "NA")
            //    {
            //        cir1Comp2_1_PartNum = drComp["Circuit2_1_Compressor"].ToString();
            //        qtyPer = "1";
            //    }
            //    ds = addPartToDataSet(ds, cir1Comp2_1_PartNum, "", "Compressor2-1", "Circuit 2", qtyPer, "EA");

            //    qtyPer = "0";
            //    if (drComp["Circuit2_2_Compressor"].ToString() != "NA")
            //    {
            //        cir1Comp2_2_PartNum = drComp["Circuit2_2_Compressor"].ToString();
            //        qtyPer = "1";
            //    }
            //    ds = addPartToDataSet(ds, cir1Comp2_2_PartNum, "", "Compressor2-2", "Circuit 2", qtyPer, "EA");

            //    qtyPer = "0";
            //    if (drComp["Circuit2_3_Compressor"].ToString() != "NA")
            //    {
            //        cir1Comp2_3_PartNum = drComp["Circuit2_3_Compressor"].ToString();
            //        qtyPer = "1";
            //    }
            //    ds = addPartToDataSet(ds, cir1Comp2_3_PartNum, "", "Compressor2-3", "Circuit 2", qtyPer, "EA");

            //    if (drComp["Circuit2_Charge"].ToString() != "")
            //    {
            //        if ((modelNoStr.Substring(11, 1).ToString() == "0"))
            //        {
            //            circuit2ChargeStr = drComp["Circuit2_Charge"].ToString();
            //        }
            //        else
            //        {
            //            circuit2ChargeStr = (Decimal.Parse(drComp["Circuit2_Charge"].ToString()) + Decimal.Parse(drComp["Circuit2_ReheatCharge"].ToString())).ToString();
            //        }
            //    }
            //    ds = addPartToDataSet(ds, "VCPRFG-0400", "", "Compressor Charge", "Circuit 2", circuit2ChargeStr, "LBs");                
            //}

            //DataTable dtRfgComp = objMain.GetRefrigerationComponentData(cabinetSize, digit567, unitType);
            //if (dtRfgComp.Rows.Count > 0)
            //{
            //    DataRow row = dtRfgComp.Rows[0];
               
            //    if (modelNoStr.Substring(12, 1) == "A" || modelNoStr.Substring(12, 1) == "F")  //Scroll - Scroll
            //    {
            //        cir1TXV_PartNum = row["Circuit1_1_TXV_FS_NonASHP_PartNum"].ToString();
            //        cir2TXV_PartNum = row["Circuit2_1_TXV_FS_NonASHP_PartNum"].ToString();
            //    }
            //    else if (modelNoStr.Substring(12, 1) == "C" || modelNoStr.Substring(12, 1) == "E" || modelNoStr.Substring(12, 1) == "H" || modelNoStr.Substring(12, 1) == "K" || 
            //             modelNoStr.Substring(12, 1) == "L" || modelNoStr.Substring(12, 1) == "M") // Digital - Digital
            //    {
            //        cir1TXV_PartNum = row["Circuit1_1_TXV_DS_VS_ASHP_PartNum"].ToString();
            //        cir2TXV_PartNum = row["Circuit2_1_TXV_DS_VS_ASHP_PartNum"].ToString();
            //    }
            //    else if (modelNoStr.Substring(12, 1) == "B" || modelNoStr.Substring(12, 1) == "D" || modelNoStr.Substring(12, 1) == "G" || modelNoStr.Substring(12, 1) == "J") // Digital - Scroll
            //    {
            //        cir1TXV_PartNum = row["Circuit1_1_TXV_DS_VS_ASHP_PartNum"].ToString();
            //        cir2TXV_PartNum = row["Circuit2_1_TXV_FS_NonASHP_PartNum"].ToString();
            //    }
            //    ds = addPartToDataSet(ds, cir1TXV_PartNum, "", "TXV Valves", "Circuit 1", "", "");
            //    ds = addPartToDataSet(ds, cir2TXV_PartNum, "", "TXV Valves", "Circuit 2", "", "");

            //    ds = addPartToDataSet(ds, row["HGRH_KCC_ValvePartNum"].ToString(), "", "KCC_ValvePartNum", "HGRH Valves", "", "");
            //    ds = addPartToDataSet(ds, row["HGRH_KCC_PreBentPartNum"].ToString(), "", "KCC_PreBentPartNum", "HGRH Valves", "", "");
            //    ds = addPartToDataSet(ds, row["HGRH_PN_ValvePartNum"].ToString(), "", "PN_ValvePartNum", "HGRH Valves", "", "");
            //    ds = addPartToDataSet(ds, row["HGRH_LineSize"].ToString(), row["HGRH_LineSize"].ToString(), "HGRH LineSize", "HGRH Valves", "", "");                

            //    ds = addPartToDataSet(ds, row["Circuit1_SightGlassPartNum"].ToString(), row["Circuit1_SightGlassPartDesc"].ToString(), "Sight Glass", "Circuit 1", "", "");
            //    ds = addPartToDataSet(ds, row["Circuit1_SightGlassSize"].ToString(), row["Circuit1_SightGlassSize"].ToString(), "SightGlassSize", "Circuit 1", "", "");
               
            //    if (row["Circuit2_SightGlassPartNum"].ToString() != "NA")
            //    {
            //        ds = addPartToDataSet(ds, row["Circuit2_SightGlassPartNum"].ToString(), row["Circuit2_SightGlassPartDesc"].ToString(), "Sight Glass", "Circuit 2", "", "");
            //        ds = addPartToDataSet(ds, row["Circuit2_SightGlassSize"].ToString(), row["Circuit2_SightGlassSize"].ToString(), "SightGlassSize", "Circuit 2", "", "");                    
            //    }
            //}

            if (modelNoStr.Length == 39)
            {
                digit4 = "A";
            }

            DataTable dtLineSize = objMain.GetLineSize(digit3, digit4, digit567, digit11);

            if (dtLineSize.Rows.Count > 0)
            {
                DataRow row2 = dtLineSize.Rows[0];

                circuit1Suction = row2["Circuit1Suction"].ToString();
                circuit1LiquidLine =  row2["Circuit1LiquidLine"].ToString();
                circuit1DischargeLine = row2["Circuit1DischargeLine"].ToString();               

                if (row2["Circuit2Suction"] != null)
                {
                    circuit2Suction = row2["Circuit2Suction"].ToString();
                    circuit2LiquidLine = row2["Circuit2LiquidLine"].ToString();
                    circuit2DischargeLine = row2["Circuit2DischargeLine"].ToString();        
                }
            }

            if (modelNoStr.Length == 69)
            {
                unitTypeStr = "REV6";
            }

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@OA_Rev";
            pdv2.Value = unitTypeStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@ModelNo";
            pdv3.Value = modelNoStr;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@Cir1Chrg";
            pdv4.Value = circuit1Charge;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@Cir2Chrg";
            pdv5.Value = circuit2Charge;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@Circuit1Suction";
            pdv6.Value = circuit1Suction;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            ParameterField pf7 = new ParameterField();
            ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
            pf7.Name = "@Circuit1LiquidLine";
            pdv7.Value = circuit1LiquidLine;
            pf7.CurrentValues.Add(pdv7);

            paramFields.Add(pf7);

            ParameterField pf8 = new ParameterField();
            ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
            pf8.Name = "@Circuit1DischargeLine";
            pdv8.Value = circuit1DischargeLine;
            pf8.CurrentValues.Add(pdv8);

            paramFields.Add(pf8);

            ParameterField pf9 = new ParameterField();
            ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
            pf9.Name = "@Circuit2Suction";
            pdv9.Value = circuit2Suction;
            pf9.CurrentValues.Add(pdv9);

            paramFields.Add(pf9);

            ParameterField pf10 = new ParameterField();
            ParameterDiscreteValue pdv10 = new ParameterDiscreteValue();
            pf10.Name = "@Circuit2LiquidLine";
            pdv10.Value = circuit2LiquidLine;
            pf10.CurrentValues.Add(pdv10);

            paramFields.Add(pf10);

            ParameterField pf11 = new ParameterField();
            ParameterDiscreteValue pdv11 = new ParameterDiscreteValue();
            pf11.Name = "@Circuit2DischargeLine";
            pdv11.Value = circuit2DischargeLine;
            pf11.CurrentValues.Add(pdv11);

            paramFields.Add(pf11);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\R6_OA_RefrigerationComp.v2.rpt";             
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\R6_OA_RefrigerationComp.v2.rpt";
#endif
            cryRpt.Load(objMain.ReportString);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();            
        }

        private DataSet1 addPartToDataSet(DataSet1 ds, string partNum, string partDescription, string partCategory, string circuitPos, string qtyPer, string uom)
        {
            string partDesc = "";

            if (partNum != "NA")
            {
                if (partDescription == "")
                {
                    DataTable dtPart = objMain.GetPartDetail(partNum);

                    if (dtPart.Rows.Count > 0)
                    {
                        DataRow dr = dtPart.Rows[0];
                        partDesc = dr["PartDescription"].ToString();
                    }
                }
                else
                {
                    partDesc = partDescription;
                }
            }

            ds.RfgCompData.Rows.Add(partCategory,
                                    partNum,
                                    partDesc,
                                    qtyPer,
                                    uom,
                                    circuitPos);
            return ds;
        }

        private void printEngineeringBOM()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string jobNameStr = lbCustName.Text;
            string descriptionStr = lbEnvironment.Text;
            string startDate = lbProdStartDate.Text;
            string shipDate = lbShipDate.Text;
            string serverName = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

#if DEBUG
            serverName = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
#endif

            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            if (jobNumStr.Length > 0)
            {                               
                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNum";
                pdv1.Value = jobNumStr;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);
                               
                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@ModelNo";
                pdv2.Value = modelNoStr;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                ParameterField pf3 = new ParameterField();
                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                pf3.Name = "@Description";
                pdv3.Value = descriptionStr;
                pf3.CurrentValues.Add(pdv3);

                paramFields.Add(pf3);

                ParameterField pf4 = new ParameterField();
                ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
                pf4.Name = "@CustName";
                pdv4.Value = jobNameStr;
                pf4.CurrentValues.Add(pdv4);

                paramFields.Add(pf4);

              
                ParameterField pf5 = new ParameterField();
                ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
                pf5.Name = "@StartDate";
                pdv5.Value = startDate;
                pf5.CurrentValues.Add(pdv5);

                paramFields.Add(pf5);

                ParameterField pf6 = new ParameterField();
                ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
                pf6.Name = "@ShipDate";
                pdv6.Value = shipDate;
                pf6.CurrentValues.Add(pdv6);

                paramFields.Add(pf6);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;
#if DEBUG
                cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAU.Configurator\crystalreports\R6_OA_JobTraveler.rpt");                
#else
                //cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_JobTraveler.rpt");
                //cryRpt.Load(@"\\KCCWVPEPIC9APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_JobTraveler.rpt");
                cryRpt.Load(@"\\" + serverName + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\R6_OA_JobTraveler.rpt");                
#endif
                //cryRpt.Load(@"C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OAUConfig\R6_OA_JobTraveler.rpt");

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            else
            {                
                DataTable dtBom = dgvBOM.DataSource as DataTable;

                DataSet1 ds = new DataSet1();

                foreach(DataRow dr in dtBom.Rows)
                {
                    ds.BomData.Rows.Add(dr["PartCategory"].ToString(),
                                        dr["AssemblySeq"].ToString(),
                                        dr["SeqNo"].ToString(),
                                        dr["PartNum"].ToString(),
                                        dr["Description"].ToString(),
                                        dr["ReqQty"].ToString(),
                                        dr["UOM"].ToString(),
                                        dr["UnitCost"].ToString(),
                                        dr["WhseCode"].ToString(),
                                        dr["RelOp"].ToString(),
                                        dr["Status"].ToString(),
                                        dr["RevisionNum"].ToString(),
                                        dr["CostMethod"].ToString(),
                                        dr["PartType"].ToString(),
                                        dr["PartStatus"].ToString(),
                                        dr["ParentPartNum"].ToString(),
                                        dr["Progress_RecID"].ToString(),
                                        dr["RuleHeadID"].ToString()
                                       );
                }

#if DEBUG
                cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_NoJobNumJobTraveler.rpt");
#else
                //cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_NoJobNumJobTraveler.rpt");
                //cryRpt.Load(@"\\KCCWVPEPIC9APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_NoJobNumJobTraveler.rpt");
                cryRpt.Load(@"\\" + serverName + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\R6_OA_NoJobNumJobTraveler.rpt");
#endif
                //cryRpt.Load(@"C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OA_Config_Rev6\OA_Config_Rev6\Reports\R6_OA_NoJobNumJobTraveler.rpt");
               
                cryRpt.SetDataSource(ds.Tables["BomData"]);
                cryRpt.SetParameterValue("ModelNo", modelNoStr);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;               
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();

            }
        }

        private void printJobNumberSticker()
        {
            string jobNumStr;
            string serverName = ConfigurationManager.AppSettings["ProdEpicorAppServer"];            

            frmPrintBOM frmPrint = new frmPrintBOM();

            jobNumStr = this.lbJobNum.Text;          

            ReportDocument cryRpt = new ReportDocument();

            //cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\JobNumBarCode.rpt");
            //cryRpt.Load(@"\\KCCWVPEPIC9APP\Apps\OAU\OAUConfiguratorcrystalreports\JobNumBarCode.rpt");

            cryRpt.Load(@"\\" + serverName + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\JobNumBarCode.rpt");

            cryRpt.SetParameterValue("JobNum", jobNumStr);

            //cryRpt.Load(@"C:\Documents and Settings\tonyt\My Documents\Visual Studio 2005\Projects\OAU_AssemblySystem\OAU_AssemblySystem\JobNumBarCode.rpt");          

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            frmPrint.crystalReportViewer1.Refresh();
            frmPrint.ShowDialog();
        }

        private void printJobNumberQRCode()
        {
            string jobNumStr = lbJobNum.Text;

            QRCodeEncoder enc = new QRCodeEncoder();


            //frmPrintBOM frmPrint = new frmPrintBOM();

            //jobNumStr = this.lbJobNum.Text;

            //ReportDocument cryRpt = new ReportDocument();

            //cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\JobNumBarCode.rpt");

            //cryRpt.SetParameterValue("JobNum", jobNumStr);

            ////cryRpt.Load(@"C:\Documents and Settings\tonyt\My Documents\Visual Studio 2005\Projects\OAU_AssemblySystem\OAU_AssemblySystem\JobNumBarCode.rpt");          

            //frmPrint.crystalReportViewer1.ReportSource = cryRpt;

            //frmPrint.crystalReportViewer1.Refresh();
            //frmPrint.ShowDialog();
        }

        private void saveBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string unitType = lbModelNo.Text.Substring(0, 3);

            if (unitType.StartsWith("OA") == true || unitType.StartsWith("HA") == true)
            {
                saveBOM_StatLoc();
            }
            else
            {
                saveBOM();
            }
        }

        private void validateJobOperations(string jobNumStr, string partNumStr, string revTypeStr)
        {
            string revisionNumStr = "";
            string curAsmSeq = "";
            string curOprSeq = "";
            string curOpCode = "";
            string curResourceGrpID = "";
            string curResourceID = "";
            string parentPartNum = "";
            string partNum = "";

            int parLen = 0;
            int eqPos = 0;

            bool jobFormatGood = false;
            bool missingAsmSeq = false;
            bool missingOprSeq = false;
            bool addOpr = false;

             DataTable dtRev = objMain.GetPartRevisionNum(partNumStr);

            if (dtRev.Rows.Count > 0)
            {
                DataRow drRev = dtRev.Rows[0];
                revisionNumStr = drRev["RevisionNum"].ToString();
            }

            frmJobOperationComp frmJOC = new frmJobOperationComp();

            DataTable dtJobOpData = objMain.GetOA_JobOperations(jobNumStr);

            DataTable dtJob = new DataTable();            
            dtJob.Columns.Add("AssemblySeq");
            dtJob.Columns.Add("OprSeq");
            dtJob.Columns.Add("OpCode");
            dtJob.Columns.Add("ResourceGrpID");
            dtJob.Columns.Add("ResourceID");
           
            foreach (DataRow row in dtJobOpData.Rows)
            {
                var dr = dtJob.NewRow();

                dr["AssemblySeq"] = row["AssemblySeq"].ToString();
                dr["OprSeq"] = row["OprSeq"].ToString();
                dr["OpCode"] = row["OpCode"].ToString();
                dr["ResourceGrpID"] = row["ResourceGrpID"].ToString();
                dr["ResourceID"] = row["ResourceID"].ToString();
                dtJob.Rows.Add(dr);
            }

            frmJOC.dgvActualOpr.DataSource = dtJob;

            frmJOC.dgvActualOpr.Columns["AssemblySeq"].Width = 50;            // AssemblySeq
            frmJOC.dgvActualOpr.Columns["AssemblySeq"].HeaderText = "AsmSeq";
            frmJOC.dgvActualOpr.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmJOC.dgvActualOpr.Columns["AssemblySeq"].DisplayIndex = 0;
            frmJOC.dgvActualOpr.Columns["OprSeq"].Width = 50;            // OprSeq
            frmJOC.dgvActualOpr.Columns["OprSeq"].HeaderText = "OprSeq";
            frmJOC.dgvActualOpr.Columns["OprSeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmJOC.dgvActualOpr.Columns["OprSeq"].DisplayIndex = 1;
            frmJOC.dgvActualOpr.Columns["OpCode"].Width = 70;           // OpCode
            frmJOC.dgvActualOpr.Columns["OpCode"].HeaderText = "OpCode";
            frmJOC.dgvActualOpr.Columns["OpCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmJOC.dgvActualOpr.Columns["OpCode"].DisplayIndex = 2;
            frmJOC.dgvActualOpr.Columns["ResourceGrpID"].Width = 95;           // ResourceGrpID
            frmJOC.dgvActualOpr.Columns["ResourceGrpID"].HeaderText = "ResourceGrpID";
            frmJOC.dgvActualOpr.Columns["ResourceGrpID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmJOC.dgvActualOpr.Columns["ResourceGrpID"].DisplayIndex = 3;
            frmJOC.dgvActualOpr.Columns["ResourceID"].Width = 95;           // ResourceID   
            frmJOC.dgvActualOpr.Columns["ResourceID"].HeaderText = "ResourceID";
            frmJOC.dgvActualOpr.Columns["ResourceID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;           
            frmJOC.dgvActualOpr.Columns["ResourceID"].DisplayIndex = 4;

            frmJOC.lbActualJobOpr.Text = "This job's actual operations and sub assemblies.";

            DataTable dtPartOpr = objMain.GetPartOperationData(partNumStr, revisionNumStr);

            DataTable dtNeedOpr = new DataTable();
            dtNeedOpr.Columns.Add("AssemblySeq");
            dtNeedOpr.Columns.Add("OprSeq");
            dtNeedOpr.Columns.Add("OpCode");
            dtNeedOpr.Columns.Add("ResourceGrpID");
            dtNeedOpr.Columns.Add("ResourceID");

            foreach (DataRow row in dtPartOpr.Rows)
            {
                if ( revTypeStr == "REV6" && row["OprSeq"].ToString() == "35")
                {
                    addOpr = false;
                }
                else
                {
                    addOpr = true;
                }

                if (addOpr == true)
                {
                    var dr2 = dtNeedOpr.NewRow();

                    dr2["AssemblySeq"] = "0";
                    dr2["OprSeq"] = row["OprSeq"].ToString();
                    dr2["OpCode"] = row["OpCode"].ToString();
                    dr2["ResourceGrpID"] = row["ResourceGrpID"].ToString();
                    dr2["ResourceID"] = row["ResourceID"].ToString();
                    dtNeedOpr.Rows.Add(dr2);
                }
            }

            //foreach(DataGridViewRow row in dgvBOM.Rows)
            foreach (SubAssembly asm in SubAsm)
            {
                            
                //if (row.Cells["AssemblySeq"].Value.ToString() != curAsmSeq)
                //{
                   // curAsmSeq = row.Cells["AssemblySeq"].Value.ToString();

                    //if (curAsmSeq != "0")
                    //{
                    //    parentPartNum = row.Cells["ParentPartNum"].Value.ToString();

                    //    if (parentPartNum.Contains("RFG"))
                    //    {
                    //        int blankPos = parentPartNum.IndexOf(" ");
                    //        if (blankPos > 0)
                    //        {
                    //            parentPartNum = parentPartNum.Substring(0, blankPos);
                    //        }
                    //    }
                    //    else if (parentPartNum.StartsWith("PNL"))
                    //    {
                    //        parLen = parentPartNum.Length;
                    //        eqPos = parentPartNum.IndexOf("=");
                    //        parentPartNum = parentPartNum.Substring((eqPos + 2), (parLen - (eqPos + 2)));
                    //    }

                    //    dtRev = objMain.GetPartRevisionNum(parentPartNum);

                    //    if (dtRev.Rows.Count > 0)
                    //    {
                    //        DataRow drRev = dtRev.Rows[0];
                    //        revisionNumStr = drRev["RevisionNum"].ToString();
                    //    }

                parentPartNum = asm.AssemblyPartNum;
                revisionNumStr = asm.AssemblyRevNum;
                curAsmSeq = asm.AsmSeq.ToString();
                dtPartOpr = objMain.GetPartOperationData(parentPartNum, revisionNumStr);

                foreach(DataRow drow in dtPartOpr.Rows)
                {
                    var dr2 = dtNeedOpr.NewRow();

                    dr2["AssemblySeq"] = curAsmSeq;                            
                    dr2["OprSeq"] = drow["OprSeq"].ToString();
                    dr2["OpCode"] = drow["OpCode"].ToString();
                    dr2["ResourceGrpID"] = drow["ResourceGrpID"].ToString();
                    dr2["ResourceID"] = drow["ResourceID"].ToString();

                    //if (curOprSeq != drow["OprSeq"].ToString())
                    //{
                        curOprSeq = drow["OprSeq"].ToString();
                        dtNeedOpr.Rows.Add(dr2);
                    //}
                }
                    //}
                //}                
            }

            frmJOC.dgvNeededOpr.DataSource = dtNeedOpr;

            frmJOC.dgvNeededOpr.Columns["AssemblySeq"].Width = 50;            // AssemblySeq
            frmJOC.dgvNeededOpr.Columns["AssemblySeq"].HeaderText = "AsmSeq";
            frmJOC.dgvNeededOpr.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmJOC.dgvNeededOpr.Columns["AssemblySeq"].DisplayIndex = 0;
            frmJOC.dgvNeededOpr.Columns["OprSeq"].Width = 50;            // OprSeq
            frmJOC.dgvNeededOpr.Columns["OprSeq"].HeaderText = "OprSeq";
            frmJOC.dgvNeededOpr.Columns["OprSeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmJOC.dgvNeededOpr.Columns["OprSeq"].DisplayIndex = 1;
            frmJOC.dgvNeededOpr.Columns["OpCode"].Width = 70;           // OpCode
            frmJOC.dgvNeededOpr.Columns["OpCode"].HeaderText = "OpCode";
            frmJOC.dgvNeededOpr.Columns["OpCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmJOC.dgvNeededOpr.Columns["OpCode"].DisplayIndex = 2;
            frmJOC.dgvNeededOpr.Columns["ResourceGrpID"].Width = 95;           // ResourceGrpID
            frmJOC.dgvNeededOpr.Columns["ResourceGrpID"].HeaderText = "ResourceGrpID";
            frmJOC.dgvNeededOpr.Columns["ResourceGrpID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmJOC.dgvNeededOpr.Columns["ResourceGrpID"].DisplayIndex = 3;
            frmJOC.dgvNeededOpr.Columns["ResourceID"].Width = 95;           // ResourceID   
            frmJOC.dgvNeededOpr.Columns["ResourceID"].HeaderText = "ResourceID";
            frmJOC.dgvNeededOpr.Columns["ResourceID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmJOC.dgvNeededOpr.Columns["ResourceID"].DisplayIndex = 4;

            frmJOC.lbNeededJobOpr.Text = "Job# " + jobNumStr + " needs the following operations and sub assemblies.";

            frmJOC.dgvActualOpr.BackgroundColor = Color.Green;
            frmJOC.lbMsg.Text = "Actual Job Operations are GOOD!"; 
            foreach (DataGridViewRow row in frmJOC.dgvNeededOpr.Rows)
            {
                curAsmSeq = row.Cells["AssemblySeq"].Value.ToString();
                curOprSeq = row.Cells["OprSeq"].Value.ToString();
                curOpCode = row.Cells["OpCode"].Value.ToString();
                curResourceGrpID = row.Cells["ResourceGrpID"].Value.ToString();
                curResourceID = row.Cells["ResourceID"].Value.ToString();
                missingAsmSeq = true;
                missingOprSeq = true;

                foreach (DataGridViewRow dgrv in frmJOC.dgvActualOpr.Rows)
                {                    
                    if (curAsmSeq == dgrv.Cells["AssemblySeq"].Value.ToString())
                    {
                        missingAsmSeq = false;
                        if (curOprSeq == dgrv.Cells["OprSeq"].Value.ToString())
                        {
                            missingOprSeq = false;
                            if (curOpCode == dgrv.Cells["OpCode"].Value.ToString() && curResourceGrpID == dgrv.Cells["ResourceGrpID"].Value.ToString() &&
                                curResourceID == dgrv.Cells["ResourceID"].Value.ToString())
                            {
                                jobFormatGood = true;
                                break;
                            }
                            else
                    
                            {
                                frmJOC.lbMsg.Text = "ERROR! - Actual AssemblySeq => " + curAsmSeq + " OprSeq => " + curOprSeq + " does not match needed job operations.";
                                frmJOC.dgvActualOpr.BackgroundColor = Color.Red;
                                row.DefaultCellStyle.BackColor = Color.Red;
                                jobFormatGood = false;
                                break;
                            }
                        }
                    }                  
                }  
             
                if (jobFormatGood == false)
                {
                    frmJOC.BackColor = Color.Red;
                    break;
                }
                else
                {
                    if (missingAsmSeq == true)
                    {
                        frmJOC.lbMsg.Text = "ERROR! - AssemblySeq => " + curAsmSeq + " is missing! Contact IT to resolve.";
                        frmJOC.dgvActualOpr.BackgroundColor = Color.Red;
                    }
                    else if (missingOprSeq == true)
                    {
                        frmJOC.lbMsg.Text = "ERROR! - AssemblySeq => " + curAsmSeq + " OprSeq => " + curOprSeq + " is missing! Contact IT to resolve.";
                        frmJOC.dgvActualOpr.BackgroundColor = Color.Red;
                    }
                    frmJOC.BackColor = Color.Green;
                }
            }

            frmJOC.ShowDialog();

        }


        private string validateJobOperationFormat(string jobNumStr, string partNumStr)
        {
            string revisionNumStr = "";
            string oprSeqStr = "";
            string asmSeqStr = "";
            string errors = "";

            int pos = 0;

            DataTable dtRev = objMain.GetPartRevisionNum(partNumStr);

            if (dtRev.Rows.Count > 0)
            {
                DataRow drRev = dtRev.Rows[0];
                revisionNumStr = drRev["RevisionNum"].ToString();

                DataTable dtPartOp = objMain.GetPartOpDtl(partNumStr, revisionNumStr);
                asmSeqStr = "0";

                foreach (DataRow row in dtPartOp.Rows)
                {
                    oprSeqStr = row["OprSeq"].ToString();

                    DataTable dtJobOp = objMain.GetJobOpDtlByJobNumAsmSeqOprSeq(jobNumStr, Int32.Parse(asmSeqStr), Int32.Parse(oprSeqStr));

                    if (dtJobOp.Rows.Count == 0)
                    {
                        errors += "PartNum - " + partNumStr + " AssembySeq - " + asmSeqStr + " - OprSeq - " + oprSeqStr + " does not exist.\n";
                    }
                }

                revisionNumStr = "";
                partNumStr = "OACONTROLPANEL";
                dtRev = objMain.GetPartRevisionNum(partNumStr);

                if (dtRev.Rows.Count > 0)
                {
                    DataRow drRevCP = dtRev.Rows[0];
                    revisionNumStr = drRevCP["RevisionNum"].ToString();
                }

                dtPartOp = objMain.GetPartOpDtl(partNumStr, revisionNumStr);
                asmSeqStr = "1";

                foreach (DataRow row in dtPartOp.Rows)
                {
                    oprSeqStr = row["OprSeq"].ToString();

                    DataTable dtJobOp = objMain.GetJobOpDtlByJobNumAsmSeqOprSeq(jobNumStr, Int32.Parse(asmSeqStr), Int32.Parse(oprSeqStr));

                    if (dtJobOp.Rows.Count == 0)
                    {
                        errors += "PartNum - " + partNumStr + " AssembySeq - " + asmSeqStr + " - OprSeq - " + oprSeqStr + " does not exist.\n";
                    }
                }

                foreach (string pnStr in TandemCompAsm)
                {
                    pos = pnStr.IndexOf("(");
                    if (pos > 0)
                    {
                        partNumStr = pnStr.Substring(0, (pos - 1));
                    }
                    else
                    {
                        partNumStr = pnStr.Substring(0, (pnStr.Length - 1));
                    }

                    asmSeqStr = pnStr.Substring((pnStr.Length - 1), 1);
                    revisionNumStr = "";

                    dtRev = objMain.GetPartRevisionNum(partNumStr);

                    dtPartOp = objMain.GetPartOpDtl(partNumStr, revisionNumStr);

                    foreach (DataRow row in dtPartOp.Rows)
                    {
                        oprSeqStr = row["OprSeq"].ToString();

                        DataTable dtJobOp = objMain.GetJobOpDtlByJobNumAsmSeqOprSeq(jobNumStr, Int32.Parse(asmSeqStr), Int32.Parse(oprSeqStr));

                        if (dtJobOp.Rows.Count == 0)
                        {
                            errors += "PartNum - " + partNumStr + " AssembySeq - " + asmSeqStr + " - OprSeq - " + oprSeqStr + " does not exist.\n";
                        }
                    }
                }
            }
            return errors;
        }       
        
    }
}