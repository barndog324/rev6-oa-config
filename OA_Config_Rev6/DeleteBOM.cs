﻿using Epicor.Mfg.Core;
using Epicor.Mfg.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace OA_Config_Rev6
{    
    class DeleteBOM
    {
        private Session epiSession;
        private string epicorServer;
        private string epicorSqlConnectionString;
       
        MainBO objMain = new MainBO();

        public DeleteBOM(string epicorServer, string epicorSqlConnectionString)
        {
            this.epicorServer = epicorServer;
            this.epicorSqlConnectionString = epicorSqlConnectionString;           
        }

        public string  DeleteBOM_FromEpicor(string jobNumStr)
        {
            string errorMsg = "";
            string ipJobHeadRowid = "";
            string opQuoteLineList = "";
            string partNum = "";
            string revisionNum = "";
            string sysRowID = "";
            string xrefPartNum = "";
            string xrefPartType = "";
            string vMsgText = "";
            string vMsgType = "";
            string basePartNum = "";
            string baseRevisionNum = "";
            string sourceFile = "";
            string sourceRev = "";
            string sourceJob = "";
            string sourceAltMethod = "";
            string sourcePart = "";

            int targetAsm = 0;
            int sourceAsm = 0;
            int sourceLine = 0;
            int sourceQuote = 0;

            bool vSubAvail;
            bool multipleMatch;
            bool useMethodForParts = false;
            bool resequence = true;
            bool jobEngineered = false;
            bool jobReleased = false;

            if (GlobalJob.EnvironmentStr.Contains("Test") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);
            }
            else
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);
            }

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);            

            try
            {
                JobEntryDataSet jeds = je.GetByID(jobNumStr);

                DataRow[] jhdr = jeds.JobHead.Select();

                if (jhdr[0]["JobEngineered"].ToString() == "True")
                {
                    jobEngineered = true;
                }

                if (jhdr[0]["JobReleased"].ToString() == "True")
                {
                    jobReleased = true;
                }

                if (jobEngineered) // If JobEngineered = True then un-engineer the job.
                {
                    try
                    {
                        je.ValidateJobNum(jobNumStr);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
                    }

                    if (errorMsg.Length == 0)
                    {
                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {

                            try
                            {
                                je.Update(jeds); //JobEntry dataset
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                            }
                        }
                    }                  
                }

                if (errorMsg.Length == 0)
                {
                    try
                    {
                        ipJobHeadRowid = jhdr[0]["RowIdent"].ToString();
                        je.DeleteAll(ipJobHeadRowid);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = ("ERROR occurred when deleting JobNum --> " + jobNumStr + " --> " + ex);
                    }

                    if (errorMsg.Length == 0)
                    {
                        try
                        {
                            je.GetByID(jobNumStr);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR occurred with GetByID for JobNum --> " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                je.GetLinkedQuoteLineList(jobNumStr, out opQuoteLineList);
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR occurred with GetLinkedQuoteLineList for JobNum --> " + ex);
                            }

                            if (errorMsg.Length == 0)
                            {
                                try
                                {
                                    partNum = jhdr[0]["PartNum"].ToString();
                                    revisionNum = jhdr[0]["RevisionNum"].ToString();
                                    targetAsm = (int)jeds.Tables["JobAsmbl"].Rows[0]["AssemblySeq"];
                                    je.PreGetDetails(partNum, revisionNum, sourceFile, jobNumStr, targetAsm, out vMsgText, out basePartNum, out baseRevisionNum);
                                }
                                catch (Exception ex)
                                {
                                    errorMsg = ("ERROR - PreGetDetails - Job# " + jobNumStr + " " + ex);
                                }

                                if (errorMsg.Length == 0)
                                {
                                    try
                                    {
                                        sourceFile = "Method";
                                        sourcePart = partNum;
                                        sourceRev = revisionNum;
                                        sourceAsm = 0;
                                        sourceLine = 0;
                                        sourceQuote = 0;
                                        useMethodForParts = false;
                                        resequence = true;
                                        je.GetDetails(jobNumStr, targetAsm, sourceFile, sourceQuote, sourceLine, sourceJob,
                                                      sourceAsm, sourcePart, sourceRev, sourceAltMethod, resequence, useMethodForParts);
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMsg = ("ERROR - GetDetails - Job# " + jobNumStr + " " + ex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - GetByID - Job# " + jobNumStr + " " + ex);
            }

            return errorMsg;
        }

    }
}
