﻿namespace OA_Config_Rev6
{
    partial class frmPickListCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDisplayRpt = new System.Windows.Forms.Button();
            this.dgvPickListJobs = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbOAU_Desc = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickListJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(325, 23);
            this.label2.TabIndex = 13;
            this.label2.Text = "L5PA - Electrical Panel assembly";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(12, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 23);
            this.label1.TabIndex = 12;
            this.label1.Text = "L5LS - Lineside Components";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(500, 60);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 40);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDisplayRpt
            // 
            this.btnDisplayRpt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplayRpt.ForeColor = System.Drawing.Color.Blue;
            this.btnDisplayRpt.Location = new System.Drawing.Point(500, 5);
            this.btnDisplayRpt.Name = "btnDisplayRpt";
            this.btnDisplayRpt.Size = new System.Drawing.Size(110, 40);
            this.btnDisplayRpt.TabIndex = 10;
            this.btnDisplayRpt.Text = "Display";
            this.btnDisplayRpt.UseVisualStyleBackColor = true;
            this.btnDisplayRpt.Click += new System.EventHandler(this.btnDisplayRpt_Click);
            // 
            // dgvPickListJobs
            // 
            this.dgvPickListJobs.AllowUserToAddRows = false;
            this.dgvPickListJobs.AllowUserToDeleteRows = false;
            this.dgvPickListJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPickListJobs.Location = new System.Drawing.Point(12, 130);
            this.dgvPickListJobs.Name = "dgvPickListJobs";
            this.dgvPickListJobs.Size = new System.Drawing.Size(599, 315);
            this.dgvPickListJobs.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Green;
            this.label8.Location = new System.Drawing.Point(12, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(325, 23);
            this.label8.TabIndex = 22;
            this.label8.Text = "L5RE - Refrigeration";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Green;
            this.label7.Location = new System.Drawing.Point(12, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(325, 23);
            this.label7.TabIndex = 24;
            this.label7.Text = "L5MS - Miscellaneous";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbOAU_Desc
            // 
            this.lbOAU_Desc.Enabled = false;
            this.lbOAU_Desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOAU_Desc.ForeColor = System.Drawing.Color.Red;
            this.lbOAU_Desc.Location = new System.Drawing.Point(370, 77);
            this.lbOAU_Desc.Name = "lbOAU_Desc";
            this.lbOAU_Desc.Size = new System.Drawing.Size(86, 23);
            this.lbOAU_Desc.TabIndex = 25;
            this.lbOAU_Desc.Text = "OAU_Desc";
            this.lbOAU_Desc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbOAU_Desc.Visible = false;
            // 
            // frmPickListCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 460);
            this.Controls.Add(this.lbOAU_Desc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDisplayRpt);
            this.Controls.Add(this.dgvPickListJobs);
            this.Name = "frmPickListCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Picklist Stations";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickListJobs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDisplayRpt;
        public System.Windows.Forms.DataGridView dgvPickListJobs;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lbOAU_Desc;
    }
}