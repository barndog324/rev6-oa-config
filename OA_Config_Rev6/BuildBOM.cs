﻿using Epicor.Mfg.Core;
using Epicor.Mfg.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace OA_Config_Rev6
{     
    class BuildBOM
    {
        private Session epiSession;
       
        private string epicorServer;
        private string epicorSqlConnectionString;       
       
        MainBO objMain = new MainBO();

        public BuildBOM(string epicorServer, string epicorSqlConnectionString)
        {
            this.epicorServer = epicorServer;
            this.epicorSqlConnectionString = epicorSqlConnectionString;
        }                   

        public string SavePart(string jobNumStr, string partNum, string revisionNum, decimal qtyPer, int assemblySeq, int relOp, 
                               string partCategory, string parentPartNum, int ruleBatchID, string costMethod, string partType,
                               string unitType, string partStatus, string rowMod)
        {
            string errorMsg = "";
            string stationLoc = "";           
            string sysRowID = "";
            string xrefPartNum = "";
            string xrefPartType = "";
            string vMsgText = "";
            string vMsgType = "";
            string ipPartNum = partNum;
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];  
           
            int oprSeq = 0;
            int jmIdx = 0;
            int parentAsm = 0;
            int curAsmSeq = 0;
            int priorAsmSeq = 0;

            decimal qtyPerDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool vSubAvail;
            bool multipleMatch;
            bool ipValidatePart = true;
            bool opPartChgCompleted;            

            if (envStr.Contains("Epicor905APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // Current Production port              
            }
            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // New Production port
            }                      
            else
            {
                //this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);   // Connect to Test2 epicor
                //this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9411", Session.LicenseType.Default);   // Connect to Test2 epicor
            }

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);                    

            try
            {
                JobEntryDataSet jeds = je.GetByID(jobNumStr);
                DataRow[] jhdr = jeds.JobHead.Select();

                if (jhdr[0]["JobEngineered"].ToString() == "True")
                {
                    jobEngineered = true;
                }

                if (jhdr[0]["JobReleased"].ToString() == "True")
                {
                    jobReleased = true;
                }

                if (jobEngineered == true || jobReleased == true) // If JobEngineered = True then un-engineer the job.
                {
                    try
                    {
                        je.ValidateJobNum(jobNumStr);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
                    }
                    if (errorMsg.Length == 0)
                    {
                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                je.Update(jeds); //JobEntry dataset
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                            }
                        }
                    }
                }

                if (errorMsg.Length == 0)
                {
                    if (rowMod == "ADD")
                    {
                        stationLoc = objMain.findStationLoc(relOp, unitType, "LT");

                        try
                        {
                            je.GetNewJobMtl(jeds, jobNumStr, assemblySeq);
                            jmIdx = jeds.Tables["JobMtl"].Rows.Count - 1;
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - GetNewJobMtl - Job# -->" + jobNumStr + " " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                ipValidatePart = true;
                                je.ChangeJobMtlPartNum(jeds, ipValidatePart, ref ipPartNum, sysRowID, xrefPartNum, xrefPartType,
                                                       out vMsgText, out vSubAvail, out vMsgType, out multipleMatch,
                                                       out opPartChgCompleted);
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - ChangeJobMtlPartNum - Job# -->" + jobNumStr + " " + ex);
                            }

                            if (errorMsg.Length == 0)
                            {
                                try
                                {
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["QtyPer"] = qtyPer;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["RelatedOperation"] = relOp;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character01"] = partCategory;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character02"] = partStatus;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character03"] = partType;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character04"] = parentPartNum;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character05"] = ruleBatchID.ToString();
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character06"] = stationLoc;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Checkbox01"] = 0;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Checkbox02"] = 0;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["RowMod"] = "ADD";

                                    je.ChangeJobMtlQtyPer(jeds);
                                }
                                catch (Exception ex)
                                {
                                    errorMsg = ("ERROR - ChangeJobMtlQtyPer - Job# -->" + jobNumStr + " " + ex);
                                }

                                if (errorMsg.Length == 0)
                                {
                                    try
                                    {
                                        je.Update(jeds); //JobEntry dataset                           
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                                    }
                                }
                            }
                        }
                    }
                    else  // Update 
                    {
                        try
                        {
                            jeds.Tables["JobMtl"].Rows[jmIdx]["QtyPer"] = qtyPer;                            
                            jeds.Tables["JobMtl"].Rows[jmIdx]["RowMod"] = "U";

                            je.ChangeJobMtlQtyPer(jeds);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - ChangeJobMtlQtyPer - Job# -->" + jobNumStr + " " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                je.Update(jeds); //JobEntry dataset                           
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
            }
            return errorMsg;
        }

        public string InsertKitParts(string jobNumStr, string newPartNum, string unitType)
        {
            string errorMsg = "";            
            string sysRowID = "";
            string xrefPartNum = "";
            string xrefPartType = "";
            string vMsgText = "";
            string vMsgType = "";
            string revNum = "";
            string ipPartNum = "";
            string partCategory = "";
            string partStatus = "";
            string partType = "";
            string parentPartNum = "";
            string ruleBatchID = "0";
            string stationLoc = "";
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];  

            int oprSeq = 0;
            int jmIdx = 0;
            int assemblySeq = 0;
            int parentAsm = 0;
            int curAsmSeq = 0;
            int priorAsmSeq = 0;
            int relOpInt = 0;

            decimal qtyPerDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool vSubAvail;
            bool multipleMatch;
            bool ipValidatePart = true;
            bool opPartChgCompleted;
                      
            if (envStr.Contains("Epicor905APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // Current Production port              
            }
            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // New Production port
            }           
            else
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);   // Connect to Test2 epicor
            }                          

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

            try
            {
                JobEntryDataSet jeds = je.GetByID(jobNumStr);
                DataRow[] jhdr = jeds.JobHead.Select();

                if (jhdr[0]["JobEngineered"].ToString() == "True")
                {
                    jobEngineered = true;
                }

                if (jhdr[0]["JobReleased"].ToString() == "True")
                {
                    jobReleased = true;
                }

                if (jobEngineered == true || jobReleased == true) // If JobEngineered = True then un-engineer the job.
                {
                    try
                    {
                        je.ValidateJobNum(jobNumStr);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
                    }
                    if (errorMsg.Length == 0)
                    {
                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                je.Update(jeds); //JobEntry dataset
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                            }
                        }
                    }
                }

                if (errorMsg.Length == 0)
                {
                    DataTable dtRev = objMain.GetPartRevisionNum(newPartNum);
                    if (dtRev.Rows.Count > 0)
                    {
                        DataRow drRev = dtRev.Rows[0];
                        revNum = drRev["RevisionNum"].ToString();
                    }

                    DataTable dtKit = objMain.GetPartMtlBOM(newPartNum, revNum, newPartNum);

                    foreach (DataRow drKit in dtKit.Rows)
                    {
                        ipPartNum = drKit["MtlPartNum"].ToString();
                        qtyPerDec = (decimal)drKit["QtyPer"];                        
                        partCategory = "SheetMetalKits";
                        partStatus = drKit["PartStatus"].ToString();
                        partType = "";
                        parentPartNum = newPartNum;

                        stationLoc = drKit["StationLoc"].ToString();
                        relOpInt = objMain.findRelatedOpByStatioLoc(stationLoc, unitType);

                        try
                        {
                            je.GetNewJobMtl(jeds, jobNumStr, assemblySeq);
                            jmIdx = jeds.Tables["JobMtl"].Rows.Count - 1;
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - GetNewJobMtl - Job# -->" + jobNumStr + " " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                ipValidatePart = true;
                                je.ChangeJobMtlPartNum(jeds, ipValidatePart, ref ipPartNum, sysRowID, xrefPartNum, xrefPartType,
                                                       out vMsgText, out vSubAvail, out vMsgType, out multipleMatch,
                                                       out opPartChgCompleted);
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - ChangeJobMtlPartNum - Job# -->" + jobNumStr + " " + ex);
                            }

                            if (errorMsg.Length == 0)
                            {
                                try
                                {
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["QtyPer"] = qtyPerDec;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["RelatedOperation"] = relOpInt;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character01"] = partCategory;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character02"] = partStatus;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character03"] = partType;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character04"] = parentPartNum;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character05"] = ruleBatchID.ToString();
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Character06"] = stationLoc;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Checkbox01"] = 0;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["Checkbox02"] = 0;
                                    jeds.Tables["JobMtl"].Rows[jmIdx]["RowMod"] = "ADD";

                                    je.ChangeJobMtlQtyPer(jeds);
                                }
                                catch (Exception ex)
                                {
                                    errorMsg = ("ERROR - ChangeJobMtlQtyPer - Job# -->" + jobNumStr + " " + ex);
                                }

                                if (errorMsg.Length == 0)
                                {
                                    try
                                    {
                                        je.Update(jeds); //JobEntry dataset                           
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - Inserting part number - " + ipPartNum + " --- Job# " + jobNumStr + " " + ex);
            }
            return errorMsg;
        }


        public string DeletePart(string jobNumStr, string partNum, int assemblySeq, int mtlSeq)
        {
            string errorMsg = "";                                 
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

            bool jobEngineered = false;
            bool jobReleased = false;

            if (envStr.Contains("Epicor905APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // Current Production port              
            }
            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // New Production port
            }           
            else
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);   // Connect to Test2 epicor
            }                                

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

            try
            {
                JobEntryDataSet jeds = je.GetByID(jobNumStr);
                DataRow[] jhdr = jeds.JobHead.Select();

                if (jhdr[0]["JobEngineered"].ToString() == "True")
                {
                    jobEngineered = true;
                }

                if (jhdr[0]["JobReleased"].ToString() == "True")
                {
                    jobReleased = true;
                }

                if (jobEngineered == true || jobReleased == true) // If JobEngineered = True then un-engineer the job.
                {
                    try
                    {
                        je.ValidateJobNum(jobNumStr);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
                    }
                    if (errorMsg.Length == 0)
                    {
                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                je.Update(jeds); //JobEntry dataset
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                            }
                        }
                    }
                }
               
                if (errorMsg.Length == 0)
                {
                    try
                    {                        
                        foreach(DataRow row in jeds.Tables["JobMtl"].Rows)
                        {
                            if (row["PartNum"].ToString() == partNum && (int)row["AssemblySeq"] == assemblySeq &&
                                (int)row["MtlSeq"] == mtlSeq)
                            {
                                row["RowMod"] = "DELETED";
                                row.Delete();
                            }                            
                        }
                       
                        je.Update(jeds); //JobEntry dataset                           
                    }
                    catch (Exception ex)
                    {
                        errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                    }                                                
                }
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - Deleting part number - " + partNum + " --- Job# " + jobNumStr + " " + ex);
            }           

            return errorMsg;
        }

        public string DeleteKitParts(string jobNumStr, string parentPartNum)
        {
            string errorMsg = "";
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            
            bool jobEngineered = false;
            bool jobReleased = false;

            if (envStr.Contains("Epicor905APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // Current Production port              
            }
            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // New Production port
            }          
            else
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);   // Connect to Test2 epicor
            }            

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

            try
            {
                JobEntryDataSet jeds = je.GetByID(jobNumStr);
                DataRow[] jhdr = jeds.JobHead.Select();

                if (jhdr[0]["JobEngineered"].ToString() == "True")
                {
                    jobEngineered = true;
                }

                if (jhdr[0]["JobReleased"].ToString() == "True")
                {
                    jobReleased = true;
                }

                if (jobEngineered == true || jobReleased == true) // If JobEngineered = True then un-engineer the job.
                {
                    try
                    {
                        je.ValidateJobNum(jobNumStr);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
                    }
                    if (errorMsg.Length == 0)
                    {
                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
                        }

                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                je.Update(jeds); //JobEntry dataset
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                            }
                        }
                    }
                }

                if (errorMsg.Length == 0)
                {
                    try
                    {                       
                        foreach (DataRow row in jeds.Tables["JobMtl"].Rows)
                        {
                            if (row["Character04"].ToString().Contains(parentPartNum) == true)
                            {
                                row["RowMod"] = "DELETED";
                                row.Delete();                                                             
                            }                            
                        }

                        je.Update(jeds); //JobEntry dataset                           
                    }
                    catch (Exception ex)
                    {
                        errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - Deleting Kit part number - " + parentPartNum + " --- Job# " + jobNumStr + " " + ex);
            }
            return errorMsg;
        }       

        public string SaveBOM(string jobNumStr, DataTable dtBOM)
        {                     
            string errorMsg = "";           
            string unitTypeStr = "";
            string unitPartNum = "";
            string curAsmPartNum = "";;
            string asmPartNum = "";
            string asmRevNum = "";           
            string revisionNum = "";           
            string ipPartNum = "";
            string stationLoc = "";
            string rowStationLoc = "";            
            string subAsmPart = "";
            string subAsmMtlPart = "";
            string runOutStr = "";
            string rev6LogicalOpr = "41,51,72,85";
            string prodLine = "";
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                       
            int assemblySeq = 0;
            int oprSeq = 0;
            int relOp = 0;       
            int jaIdx = 0;
            int jmIdx = 0;
            int parentAsm = 0;
            int curAsmSeq = 0;
            int priorAsmSeq = 0;
           
            byte buyToOrder = 0;
            byte runOut = 0;
            
            decimal qtyPerDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool firstError = true;
            bool firstInactive = true;
            bool inactivePartsFound = false;
            bool logicalOperation = false;
            //MessageBox.Show("MadeIt");      
      

            if (envStr.Contains("Epicor905APP") == true)
            {                
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // Current Production port              
            }
            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // New Production port
            }           
            else
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);   // Connect to Test2 epicor
            }            

            DataTable dtJobOpr = objMain.GetOA_JobOperations(jobNumStr);
            
            if (dtJobOpr.Rows.Count > 0)
            {                        
                JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

                try
                {
                    JobEntryDataSet jeds = je.GetByID(jobNumStr);
                    DataRow[] jhdr = jeds.JobHead.Select();

                    if (jhdr[0]["JobEngineered"].ToString() == "True")
                    {
                        jobEngineered = true;
                    }

                    if (jhdr[0]["JobReleased"].ToString() == "True")
                    {
                        jobReleased = true;
                    }

                    if (jobEngineered == true || jobReleased == true) // If JobEngineered = True then un-engineer the job.
                    {
                        try
                        {
                            je.ValidateJobNum(jobNumStr);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
                        }
                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                                je.ChangeJobHeadJobEngineered(jeds);
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
                            }

                            if (errorMsg.Length == 0)
                            {
                                try
                                {
                                    je.Update(jeds); //JobEntry dataset
                                }
                                catch (Exception ex)
                                {
                                    errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                                }
                            }
                        }                  
                    }                   

                    if (errorMsg.Length == 0)
                    {
                        unitPartNum = jhdr[0]["PartNum"].ToString();
                        unitTypeStr = unitPartNum.Substring(0, 4);

                        var bomResult = dtBOM.AsEnumerable().Distinct();
                        //var bomResult = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

                        foreach (DataRow dRow in bomResult)
                        {
                            ipPartNum = dRow["PartNum"].ToString();

                            try
                            {
                                runOutStr = dRow["RunOut"].ToString();
                                runOut = byte.Parse(runOutStr);
                            }
                            catch
                            {
                                MessageBox.Show("ERROR - PartNum => " + ipPartNum + " has a runout value problem");
                            }

                            if (runOut == 1)
                            {
                                if (firstError)
                                {
                                    firstError = false;
                                    errorMsg = "WARNING - Part Numbers shown as a RunOut part. This is a potential issue.";
                                }
                                errorMsg += "\nPartNum = " + ipPartNum;
                            }

                            if (dRow["PartStatus"].ToString() == "InActive")
                            {
                                if (firstInactive)
                                {
                                    firstInactive = false;
                                    inactivePartsFound = true;
                                    errorMsg = "ERROR - Part Numbers shown as an InActive part. \nThis needs to be resolved before you can move on!";
                                }
                                errorMsg += "\nPartNum = " + ipPartNum;
                            }
                        }

                        if (inactivePartsFound == false)
                        {
                            if (errorMsg.Length > 0)
                            {
                                DialogResult result1 = MessageBox.Show(errorMsg +
                                                                       " Press 'Yes' to continue saving BOM & 'No' to cancel!",
                                                                       "WARNING WARNING WARNING",
                                                                       MessageBoxButtons.YesNo,
                                                                       MessageBoxIcon.Exclamation);
                                if (result1 == DialogResult.No)
                                {
                                    return (errorMsg);
                                }
                            }

                            errorMsg = "";

                            var bomResult2 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

                            foreach (DataRow dRow in bomResult2)
                            {
                                ipPartNum = dRow["PartNum"].ToString();
                                relOp = (int)dRow["RelOp"];

                                if (unitTypeStr.Contains("OA") == true && relOp < 40)
                                {
                                    if (firstError)
                                    {
                                        firstError = false;
                                        errorMsg = "ERROR - Related operation must point to a main line operation!";
                                    }
                                    errorMsg += "\nPartNum = " + ipPartNum + " - RelatedOp = " + relOp.ToString();
                                }
                            }
                        }
                        //else
                        //{
                        //    MessageBox.Show("ERROR - Inactive parts");                      
                        //}
                    }

                    if (errorMsg.Length == 0)
                    {                                                
                        foreach (DataRow prow in dtJobOpr.Rows)
                        {
                            oprSeq = (int)prow["OprSeq"];
                            prodLine = prow["OprSeq"].ToString();

                            logicalOperation = false;

                            if ((unitPartNum.StartsWith("OANG") == true) && (rev6LogicalOpr.Contains(oprSeq.ToString()) == true))
                            {
                                logicalOperation = true;
                            }

                           if (logicalOperation == false)
                           {
                               stationLoc = objMain.findStationLoc(oprSeq, unitTypeStr, prodLine);

                                //Process main assembly parts
                                var results = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("StationLoc") == stationLoc && dr.Field<string>("ConfigurablePart") == "No"));

                                int resCount = results.Count();

                                if (resCount > 0)
                                {
                                    foreach (DataRow row in results)
                                    {
                                        ipPartNum = row["PartNum"].ToString();
                                        //assemblySeq = (int)row["AssemblySeq"];
                                        assemblySeq = 0;
                                        qtyPerDec = (decimal)row["ReqQty"];
                                        rowStationLoc = row["StationLoc"].ToString();
                                        buyToOrder = (byte)row["BuyToOrder"];

                                        if (jmIdx == 142)
                                        {
                                            string taco = "bell";
                                        }

                                        errorMsg = insertPartMtl(je, jobNumStr, jeds, ipPartNum, row, assemblySeq, oprSeq, jmIdx, buyToOrder);

                                        if (errorMsg.Length == 0)
                                        {
                                            ++jmIdx;
                                        }
                                        else
                                        {
                                            //break;
                                            return errorMsg;
                                        }
                                    }
                                }
                            }                      
                        }

                        //if (errorMsg.Length == 0)
                        //{
                        //    var results2 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "No" && dr.Field<int>("AssemblySeq") > 0));

                        //    foreach (DataRow row in results2)
                        //    {
                        //        ipPartNum = row["PartNum"].ToString();
                        //        revisionNum = row["RevisionNum"].ToString();

                        //        asmPartNum = ipPartNum;
                        //        assemblySeq = (int)row["AssemblySeq"];
                        //        subAsmPart = row["SubAssemblyPart"].ToString();
                        //        subAsmMtlPart = row["SubAsmMtlPart"].ToString();

                        //        if (asmPartNum != curAsmPartNum)
                        //        {
                        //            ++jaIdx;
                        //            curAsmSeq = jaIdx;
                        //            parentAsm = 0;
                        //            priorAsmSeq = 0;
                        //            oprSeq = 40;
                        //            qtyPerDec = 1;

                        //            DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
                        //            if (dtRev.Rows.Count > 0)
                        //            {
                        //                DataRow drRev = dtRev.Rows[0];
                        //                asmRevNum = drRev["RevisionNum"].ToString();
                        //            }

                        //            jaIdx = createSubAssembly(je, oprSeq, false, jobNumStr, jeds, asmPartNum, asmRevNum, 1,
                        //                                     jaIdx, parentAsm, curAsmSeq, priorAsmSeq, out errorMsg);
                        //            curAsmSeq = jaIdx;
                        //            curAsmPartNum = asmPartNum;

                        //            if (errorMsg.Length != 0)
                        //            {
                        //                break;
                        //            }
                        //        }
                        //    }
                        //}

                        //Process the Configurable sub-assemblies
                        if (errorMsg.Length == 0)
                        {
                            var results3 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

                            foreach (DataRow row in results3)
                            {
                                ipPartNum = row["PartNum"].ToString();
                                revisionNum = row["RevisionNum"].ToString();

                                asmPartNum = row["ParentPartNum"].ToString();
                                assemblySeq = (int)row["AssemblySeq"];
                                subAsmPart = row["SubAssemblyPart"].ToString();
                                subAsmMtlPart = row["SubAsmMtlPart"].ToString();
                                buyToOrder = (byte)row["BuyToOrder"];

                                if (asmPartNum != curAsmPartNum)
                                {
                                    ++jaIdx;
                                    curAsmSeq = jaIdx;
                                    parentAsm = 0;
                                    priorAsmSeq = 0;
                                    //oprSeq = 40;
                                    
                                    relOp = (int)row["RelOp"];
                                    qtyPerDec = 1;

                                    DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
                                    if (dtRev.Rows.Count > 0)
                                    {
                                        DataRow drRev = dtRev.Rows[0];
                                        asmRevNum = drRev["RevisionNum"].ToString();
                                    }

                                     jaIdx = createSubAssembly(je, relOp, true, jobNumStr, jeds, asmPartNum, asmRevNum, 1,
                                                             jaIdx, parentAsm, curAsmSeq, priorAsmSeq, out errorMsg);
                                    curAsmSeq = jaIdx;
                                    curAsmPartNum = asmPartNum;
                                }

                                if (errorMsg.Length == 0)
                                {
                                    ipPartNum = row["PartNum"].ToString();
                                    assemblySeq = (int)row["AssemblySeq"];
                                    qtyPerDec = (decimal)row["ReqQty"];
                                    rowStationLoc = row["StationLoc"].ToString();
                                    oprSeq = 10;
                                   
                                    errorMsg = insertPartMtl(je, jobNumStr, jeds, ipPartNum, row, curAsmSeq, oprSeq, jmIdx, buyToOrder);
                                }
                                else
                                {
                                    break;
                                }

                                if (errorMsg.Length == 0)
                                {
                                    ++jmIdx;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }                        
                    }                   
                }
                catch (Exception ex)
                {
                    errorMsg = ("ERROR - GetByID - Job# " + jobNumStr + " " + ex);
                }            
            }
            else
            {
                errorMsg = ("ERROR - This job has No operations setup in Epicor. Please contact Business Technology!");
            }
          
            return errorMsg;
        }

        public string SaveFullyIndentedBOM(string jobNumStr, DataTable dtBOM)
        {
            string errorMsg = "";
            string unitTypeStr = "";
            string unitPartNum = "";
            string curAsmPartNum = ""; ;
            string asmPartNum = "";
            string asmRevNum = "";
            string revisionNum = "";
            string ipPartNum = "";
            string stationLoc = "";
            string rowStationLoc = "";
            string subAsmPart = "";
            string subAsmMtlPart = "";
            string runOutStr = "";
            string prodLine = "";
            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

            int assemblySeq = 0;
            int oprSeq = 0;
            int relOp = 0;
            int jaIdx = 0;
            int jmIdx = 0;
            int parentAsm = 0;
            int curAsmSeq = 0;
            int priorAsmSeq = 0;

            byte buyToOrder = 0;
            byte runOut = 0;

            decimal qtyPerDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool firstError = true;
            bool firstInactive = true;
            bool inactivePartsFound = false;
            //MessageBox.Show("MadeIt");

            if (envStr.Contains("Epicor905APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // Current Production port              
            }
            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);  // New Production port
            }
            //else if (envStr.Contains("kccwvpkntcapp01") == true)
            //{
            //    ////this.epiSession = new Session("epiagent", "epiagent1", "HTTP://KCCWVPKNTCAPP01/KineticTest");    // Kinetic production
            //    ////this.epiSession = new Session("epiagent", "epiagent1", "HTTP://net.tcp.KCCWVPKNTCAPP01.kccinternational.local/KineticTest");
            //}
            else
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9431", Session.LicenseType.Default);   // Connect to Test2 epicor
            }            

            DataTable dtJobOpr = objMain.GetOA_JobOperations(jobNumStr);

            if (dtJobOpr.Rows.Count > 0)
            {
                JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

                try
                {
                    JobEntryDataSet jeds = je.GetByID(jobNumStr);
                    DataRow[] jhdr = jeds.JobHead.Select();

                    if (jhdr[0]["JobEngineered"].ToString() == "True")
                    {
                        jobEngineered = true;
                    }

                    if (jhdr[0]["JobReleased"].ToString() == "True")
                    {
                        jobReleased = true;
                    }

                    if (jobEngineered == true || jobReleased == true) // If JobEngineered = True then un-engineer the job.
                    {
                        try
                        {
                            je.ValidateJobNum(jobNumStr);
                        }
                        catch (Exception ex)
                        {
                            errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNumStr + " " + ex;
                        }
                        if (errorMsg.Length == 0)
                        {
                            try
                            {
                                jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                                je.ChangeJobHeadJobEngineered(jeds);
                            }
                            catch (Exception ex)
                            {
                                errorMsg = ("ERROR - ChangeJobHeadJobEngineered() JobNum --> " + jobNumStr + " --> " + ex);
                            }

                            if (errorMsg.Length == 0)
                            {
                                try
                                {
                                    je.Update(jeds); //JobEntry dataset
                                }
                                catch (Exception ex)
                                {
                                    errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
                                }
                            }
                        }
                    }

                    if (errorMsg.Length == 0)
                    {
                        unitPartNum = jhdr[0]["PartNum"].ToString();
                        unitTypeStr = unitPartNum.Substring(0, 4);

                        var bomResult = dtBOM.AsEnumerable().Distinct();
                        //var bomResult = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

                        foreach (DataRow dRow in bomResult)
                        {
                            ipPartNum = dRow["PartNum"].ToString();

                            try
                            {
                                runOutStr = dRow["RunOut"].ToString();
                                runOut = byte.Parse(runOutStr);
                            }
                            catch
                            {
                                MessageBox.Show("ERROR - PartNum => " + ipPartNum + " has a runout value problem");
                            }

                            if (runOut == 1)
                            {
                                if (firstError)
                                {
                                    firstError = false;
                                    errorMsg = "WARNING - Part Numbers shown as a RunOut part. This is a potential issue.";
                                }
                                errorMsg += "\nPartNum = " + ipPartNum;
                            }

                            if (dRow["PartStatus"].ToString() == "InActive")
                            {
                                if (firstInactive)
                                {
                                    firstInactive = false;
                                    inactivePartsFound = true;
                                    errorMsg = "ERROR - Part Numbers shown as an InActive part. \nThis needs to be resolved before you can move on!";
                                }
                                errorMsg += "\nPartNum = " + ipPartNum;
                            }
                        }

                        if (inactivePartsFound == false)
                        {
                            if (errorMsg.Length > 0)
                            {
                                DialogResult result1 = MessageBox.Show(errorMsg +
                                                                       " Press 'Yes' to continue saving BOM & 'No' to cancel!",
                                                                       "WARNING WARNING WARNING",
                                                                       MessageBoxButtons.YesNo,
                                                                       MessageBoxIcon.Exclamation);
                                if (result1 == DialogResult.No)
                                {
                                    return (errorMsg);
                                }
                            }

                            errorMsg = "";

                            var bomResult2 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

                            foreach (DataRow dRow in bomResult2)
                            {
                                ipPartNum = dRow["PartNum"].ToString();
                                relOp = (int)dRow["RelOp"];

                                if (unitTypeStr.Contains("OA") == true && relOp < 40)
                                {
                                    if (firstError)
                                    {
                                        firstError = false;
                                        errorMsg = "ERROR - Related operation must point to a main line operation!";
                                    }
                                    errorMsg += "\nPartNum = " + ipPartNum + " - RelatedOp = " + relOp.ToString();
                                }
                            }
                        }
                        //else
                        //{
                        //    MessageBox.Show("ERROR - Inactive parts");                      
                        //}
                    }

                    if (errorMsg.Length == 0)
                    {
                        foreach (DataRow prow in dtJobOpr.Rows)
                        {
                            oprSeq = (int)prow["OprSeq"];
                            prodLine = prow["ResourceID"].ToString();

                            stationLoc = objMain.findStationLoc(oprSeq, unitTypeStr, prodLine);

                            //Process main assembly parts
                            var results = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("StationLoc") == stationLoc && dr.Field<string>("ConfigurablePart") == "No"));

                            int resCount = results.Count();

                            if (resCount > 0)
                            {
                                foreach (DataRow row in results)
                                {
                                    ipPartNum = row["PartNum"].ToString();
                                    //assemblySeq = (int)row["AssemblySeq"];
                                    assemblySeq = 0;
                                    qtyPerDec = (decimal)row["ReqQty"];
                                    rowStationLoc = row["StationLoc"].ToString();
                                    buyToOrder = (byte)row["BuyToOrder"];

                                    if (ipPartNum.StartsWith("VME") || ipPartNum.StartsWith("RFGASM"))
                                    {
                                        ++jaIdx;
                                        curAsmSeq = jaIdx;
                                        parentAsm = 0;
                                        priorAsmSeq = 0;
                                        //oprSeq = 40;

                                        relOp = (int)row["RelOp"];
                                        qtyPerDec = 1;

                                        asmPartNum = ipPartNum;

                                        DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
                                        if (dtRev.Rows.Count > 0)
                                        {
                                            DataRow drRev = dtRev.Rows[0];
                                            asmRevNum = drRev["RevisionNum"].ToString();
                                        }

                                        jaIdx = createSubAssembly(je, relOp, true, jobNumStr, jeds, asmPartNum, asmRevNum, 1,
                                                                 jaIdx, parentAsm, curAsmSeq, priorAsmSeq, out errorMsg);
                                        curAsmSeq = jaIdx;
                                        curAsmPartNum = asmPartNum;
                                    }
                                    else
                                    {
                                        errorMsg = insertPartMtl(je, jobNumStr, jeds, ipPartNum, row, assemblySeq, oprSeq, jmIdx, buyToOrder);

                                        if (errorMsg.Length == 0)
                                        {
                                            ++jmIdx;
                                        }
                                        else
                                        {
                                            //break;
                                            return errorMsg;
                                        }
                                    }
                                }
                            }
                        }

                        //if (errorMsg.Length == 0)
                        //{
                        //    var results2 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "No" && dr.Field<int>("AssemblySeq") > 0));

                        //    foreach (DataRow row in results2)
                        //    {
                        //        ipPartNum = row["PartNum"].ToString();
                        //        revisionNum = row["RevisionNum"].ToString();

                        //        asmPartNum = ipPartNum;
                        //        assemblySeq = (int)row["AssemblySeq"];
                        //        subAsmPart = row["SubAssemblyPart"].ToString();
                        //        subAsmMtlPart = row["SubAsmMtlPart"].ToString();

                        //        if (asmPartNum != curAsmPartNum)
                        //        {
                        //            ++jaIdx;
                        //            curAsmSeq = jaIdx;
                        //            parentAsm = 0;
                        //            priorAsmSeq = 0;
                        //            oprSeq = 40;
                        //            qtyPerDec = 1;

                        //            DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
                        //            if (dtRev.Rows.Count > 0)
                        //            {
                        //                DataRow drRev = dtRev.Rows[0];
                        //                asmRevNum = drRev["RevisionNum"].ToString();
                        //            }

                        //            jaIdx = createSubAssembly(je, oprSeq, false, jobNumStr, jeds, asmPartNum, asmRevNum, 1,
                        //                                     jaIdx, parentAsm, curAsmSeq, priorAsmSeq, out errorMsg);
                        //            curAsmSeq = jaIdx;
                        //            curAsmPartNum = asmPartNum;

                        //            if (errorMsg.Length != 0)
                        //            {
                        //                break;
                        //            }
                        //        }
                        //    }
                        //}

                        //Process the Configurable sub-assemblies
                        if (errorMsg.Length == 0)
                        {
                            var results3 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ConfigurablePart") == "Yes")).Distinct();

                            foreach (DataRow row in results3)
                            {
                                ipPartNum = row["PartNum"].ToString();
                                revisionNum = row["RevisionNum"].ToString();

                                asmPartNum = row["ParentPartNum"].ToString();
                                assemblySeq = (int)row["AssemblySeq"];
                                subAsmPart = row["SubAssemblyPart"].ToString();
                                subAsmMtlPart = row["SubAsmMtlPart"].ToString();
                                buyToOrder = (byte)row["BuyToOrder"];

                                if (asmPartNum != curAsmPartNum)
                                {
                                    ++jaIdx;
                                    curAsmSeq = jaIdx;
                                    parentAsm = 0;
                                    priorAsmSeq = 0;
                                    //oprSeq = 40;

                                    relOp = (int)row["RelOp"];
                                    qtyPerDec = 1;

                                    DataTable dtRev = objMain.GetPartRevisionNum(asmPartNum);
                                    if (dtRev.Rows.Count > 0)
                                    {
                                        DataRow drRev = dtRev.Rows[0];
                                        asmRevNum = drRev["RevisionNum"].ToString();
                                    }

                                    jaIdx = createSubAssembly(je, relOp, true, jobNumStr, jeds, asmPartNum, asmRevNum, 1,
                                                             jaIdx, parentAsm, curAsmSeq, priorAsmSeq, out errorMsg);
                                    curAsmSeq = jaIdx;
                                    curAsmPartNum = asmPartNum;
                                }

                                if (errorMsg.Length == 0)
                                {
                                    ipPartNum = row["PartNum"].ToString();
                                    assemblySeq = (int)row["AssemblySeq"];
                                    qtyPerDec = (decimal)row["ReqQty"];
                                    rowStationLoc = row["StationLoc"].ToString();
                                    oprSeq = 10;

                                    errorMsg = insertPartMtl(je, jobNumStr, jeds, ipPartNum, row, curAsmSeq, oprSeq, jmIdx, buyToOrder);
                                }
                                else
                                {
                                    break;
                                }

                                if (errorMsg.Length == 0)
                                {
                                    ++jmIdx;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorMsg = ("ERROR - GetByID - Job# " + jobNumStr + " " + ex);
                }
            }
            else
            {
                errorMsg = ("ERROR - This job has No operations setup in Epicor. Please contact Business Technology!");
            }

            return errorMsg;
        }

        private string insertPartMtl(JobEntry je,  string jobNumStr, JobEntryDataSet jeds, string partNum, DataRow row, 
                                     int assemblySeq, int oprSeq, int jmIdx, byte buyToOrder)
        {            
            string errorMsg = "";            
            string sysRowID = "";
            string xrefPartNum = "";
            string xrefPartType = "";
            string vMsgText = "";
            string vMsgType = "";
            string ipPartNum = partNum;
             
            bool vSubAvail;
            bool multipleMatch;           
            bool ipValidatePart = true;
            bool opPartChgCompleted;

            try
            {
                je.GetNewJobMtl(jeds, jobNumStr, assemblySeq);                
            }
            catch (Exception ex)
            {
               errorMsg = ("ERROR - GetNewJobMtl - Job# -->" + jobNumStr + " " + ex);               
            }

            if (errorMsg.Length == 0)
            {
                try
                {
                    ipValidatePart = true;
                    je.ChangeJobMtlPartNum(jeds, ipValidatePart, ref ipPartNum, sysRowID, xrefPartNum, xrefPartType,
                                           out vMsgText, out vSubAvail, out vMsgType, out multipleMatch,
                                           out opPartChgCompleted);
                }
                catch (Exception ex)
                {
                    errorMsg = ("ERROR - ChangeJobMtlPartNum - Job# -->" + jobNumStr + " " + ex);
                }

                if (errorMsg.Length == 0)
                {
                    try
                    {
                        if (ipPartNum.Contains("VMECAB") == true || ipPartNum.Contains("VMEASM") == true)
                        {
                            if ((byte)row["NonStock"] == 1)
                            {
                                jeds.Tables["JobMtl"].Rows[jmIdx]["Direct"] = 1;
                            }
                            else
                            {
                                jeds.Tables["JobMtl"].Rows[jmIdx]["Direct"] = 0;
                            }
                        }
                        else
                        {
                            if ((byte)row["NonStock"] == 1)
                            {
                                jeds.Tables["JobMtl"].Rows[jmIdx]["BuyIt"] = 1;
                            }
                            else
                            {
                                jeds.Tables["JobMtl"].Rows[jmIdx]["BuyIt"] = 0;
                            }
                        }

                        jeds.Tables["JobMtl"].Rows[jmIdx]["PartNum"] = ipPartNum;
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Description"] = row["Description"].ToString(); ;
                        //jeds.Tables["JobMtl"].Rows[jmIdx]["BuyIt"] = (byte)row["BuyToOrder"];
                        jeds.Tables["JobMtl"].Rows[jmIdx]["QtyPer"] = (decimal)row["ReqQty"];
                        //jeds.Tables["JobMtl"].Rows[jmIdx]["RequiredQty"] = (decimal)row["ReqQty"];
                        jeds.Tables["JobMtl"].Rows[jmIdx]["RelatedOperation"] = oprSeq;
                        jeds.Tables["JobMtl"].Rows[jmIdx]["WarehouseCode"] = row["WhseCode"].ToString();
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Character01"] = row["PartCategory"].ToString();
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Character02"] = row["PartStatus"].ToString();
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Character03"] = row["PartType"].ToString();
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Character04"] = row["ParentPartNum"].ToString();
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Character05"] = row["RuleBatchID"].ToString();
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Character06"] = row["StationLoc"].ToString();
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Checkbox01"] = row["SubAssemblyPart"];
                        jeds.Tables["JobMtl"].Rows[jmIdx]["Checkbox02"] = row["SubAsmMtlPart"];
                        
                        je.ChangeJobMtlQtyPer(jeds);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = ("ERROR - ChangeJobMtlQtyPer - Job# -->" + jobNumStr + " " + ex);                       
                    }

                    if (errorMsg.Length == 0)
                    {
                        try
                        {
                            je.Update(jeds); //JobEntry dataset                           
                        }
                        catch (Exception ex)
                        {
                            errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);                            
                        }
                    }
                }
            }

            return errorMsg;         
        }

        private int createSubAssembly(JobEntry je, int oprSeq, bool getDetails, string jobNumStr, JobEntryDataSet jeds, string asmPartNum, 
                                      string revNum, decimal qtyPerDec, int jaIdx, int parentAsm, int curAsmSeq, int priorAsmSeq, 
                                      out string errorMsg)
        {
            string runOutWarning = "";
            string c_WarnLogTxt = "";            
            string asmPartRev = revNum;
            string sysRowID = "";
            string xrefPartNum = "";
            string xrefPartType = "";
            string vMsgText = "";
            string vMsgType = "";
            string sourceFile = "";
            string sourceRev = "";
            string sourceJob = "";
            string sourceAltMethod = "";
            string sourcePart = "";
            string basePartNum = "";
            string baseRevisionNum = "";
            string ipPartNum = "";
            string stationLoc = "";
            string rowStationLoc = "";

            int asmSeq = 0;          
            int targetAsm = 0;
            int sourceAsm = 0;
            int sourceLine = 0;
            int sourceQuote = 0;
            int rowCount = 0;            
            int jmIdx = 0;
            int iProposedRelatedOperation = 0;
            int rowIdx = curAsmSeq;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool skipXRefUpdate = true;
            bool vSubAvail;
            bool multipleMatch;
            bool useMethodForParts = false;
            bool resequence = true;
            bool ipValidatePart = true;
            bool opPartChgCompleted;

            errorMsg = "";

            try
            {
                jaIdx = jeds.Tables["JobAsmbl"].Rows.Count;
                je.InsertNewJobAsmbl(jeds, jobNumStr, parentAsm, 0, priorAsmSeq);  //JobEntry dataset, jobNum, (current assembly seq), bomLevel, priorAssemblySeq
                jaIdx = jeds.Tables["JobAsmbl"].Rows.Count;
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - InsertNewJobAsmbl - Job# " + jobNumStr + " " + ex);
            }

            try
            {
                je.CheckChangeJobAsmblParent(parentAsm, jeds); //ipNewParent, JobEntry dataset, 
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - CheckChangeJobAsmblParent - Job# " + jobNumStr + " " + ex);
            }

            try
            {
                je.ChangeJobAsmblOpr(oprSeq, jeds); //iProposedRelatedOperation, JobEntry dataset, 
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - ChangeJobAsmblOpr - Job# " + jobNumStr + " " + ex);
            }

            try
            {
                je.CheckPrePartInfo(jeds, ref asmPartNum, sysRowID, skipXRefUpdate, xrefPartNum, xrefPartType,
                                    out vMsgText, out vSubAvail, out vMsgType, out multipleMatch);
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - CheckPrePartInfo - Job# " + jobNumStr + " " + ex);
            }

            try
            {
                //if (curAsmSeq == 1)
                //{
                //    ++rowIdx;
                //}
                jeds.Tables["JobAsmbl"].Rows[rowIdx]["PartNum"] = asmPartNum;
                jeds.Tables["JobAsmbl"].Rows[rowIdx]["RevisionNum"] = asmPartRev;

                je.ChangeJobAsmblPartNum(jeds); //JobEntry dataset, 
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - ChangeJobAsmblPartNum - Job# " + jobNumStr + " " + ex);
            }

            try
            {
                je.Update(jeds); //JobEntry dataset
            }
            catch (Exception ex)
            {
                errorMsg = ("ERROR - Update - Job# " + jobNumStr + " " + ex);
            }

            targetAsm = (int)jeds.Tables["JobAsmbl"].Rows[rowIdx]["AssemblySeq"];

            if (getDetails == true)
            {
                try
                {                   
                    je.PreGetDetails(asmPartNum, asmPartRev, sourceFile, jobNumStr,
                                     targetAsm, out vMsgText, out basePartNum, out baseRevisionNum);
                }
                catch (Exception ex)
                {
                    errorMsg = ("ERROR - PreGetDetails - Job# " + jobNumStr + " " + ex);
                }

                try
                {
                    sourceFile = "Method";
                    sourcePart = asmPartNum;
                    sourceRev = asmPartRev;
                    sourceAsm = 0;
                    sourceLine = 0;
                    sourceQuote = 0;
                    useMethodForParts = false;
                    resequence = true;
                    je.GetDetails(jobNumStr, targetAsm, sourceFile, sourceQuote, sourceLine, sourceJob,
                                  sourceAsm, sourcePart, sourceRev, sourceAltMethod, resequence, useMethodForParts);
                }
                catch (Exception ex)
                {
                    errorMsg = ("ERROR - GetDetails - Job# " + jobNumStr + " = Assembly PartNum --> " + asmPartNum + " --> " + ex);
                }
            }

            return targetAsm;

        }        
    }
}
