﻿using Epicor.Mfg.Core;
using Epicor.Mfg.BO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmLineTranfer : Form
    {
        private string ToLine = "Empty";
        private string ToLoc = "Empty";
        private string SelectedSlot = "Empty";

        private string Slot1_StartTime = "";
        private string Slot2_StartTime = "";
        private string Slot3_StartTime = "";
        private string Slot4_StartTime = "";
        private string Slot5_StartTime = "";
        private string Slot1_EndTime = "";
        private string Slot2_EndTime = "";
        private string Slot3_EndTime = "";
        private string Slot4_EndTime = "";
        private string Slot5_EndTime = "";

        private DateTime StartDate = DateTime.Now;

        LineTransferBO objTran = new LineTransferBO();
        MainBO objMain = new MainBO();

        public frmLineTranfer()
        {
            InitializeComponent();
        }       

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string fromLoc = "";
            string fromLine = "";
            string toLoc = "";
            string toLine = "";           
            string jobNum = lbJobNum.Text;
            string partNum = lbModelNo.Text.Substring(0, 7);

            if (rbGrassToLine1.Checked == false && rbGrassToLine2.Checked == false && rbGrassToLine3.Checked == false && rbGrassToLine4.Checked == false &&
                rbTechToLineA.Checked == false && rbTechToLineB.Checked == false && rbTechToLineC.Checked == false && rbTechToLineD.Checked == false)
            {
                MessageBox.Show("ERROR = No To Line selection has been made!");
            }
            else if (rbGrassFromLine1.Checked && rbGrassToLine1.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if (rbGrassFromLine2.Checked && rbGrassToLine2.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if (rbGrassFromLine3.Checked && rbGrassToLine3.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if (rbGrassFromLine4.Checked && rbGrassToLine4.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if (rbTechFromLineA.Checked && rbTechToLineA.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if (rbTechFromLineB.Checked && rbTechToLineB.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if (rbTechFromLineC.Checked && rbTechToLineC.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if (rbTechFromLineD.Checked && rbTechToLineD.Checked)
            {
                MessageBox.Show("ERROR = From Line selected and To Line selected are the same!");
            }
            else if ((rbTechFromLineA.Checked || rbTechFromLineB.Checked || rbTechFromLineC.Checked || rbTechFromLineD.Checked) && 
                     (rbGrassToLine1.Checked || rbGrassToLine2.Checked || rbGrassToLine3.Checked || rbGrassToLine4.Checked))
            {
                MessageBox.Show("ERROR = Transferring a Job from Technology to Grassland must be handled manually within Epicor!");
            }            
            else
            {
                if (rbGrassFromLine1.Checked)
                {
                    fromLoc = "Grassland";
                    fromLine = "Line1";
                }
                else if (rbGrassFromLine2.Checked)
                {
                    fromLoc = "Grassland";
                    fromLine = "Line2";
                }
                else if (rbGrassFromLine3.Checked)
                {
                    fromLoc = "Grassland";
                    fromLine = "Line3";
                }
                else if (rbGrassFromLine4.Checked)
                {
                    fromLoc = "Grassland";
                    fromLine = "Line4";
                }
                else if (rbTechFromLineA.Checked)
                {
                    fromLoc = "Technology";
                    fromLine = "LineA";
                }
                else if (rbTechFromLineB.Checked)
                {
                    fromLoc = "Technology";
                    fromLine = "LineB";
                }
                else if (rbTechFromLineC.Checked)
                {
                    fromLoc = "Technology";
                    fromLine = "LineC";
                }
                else if (rbTechFromLineD.Checked)
                {
                    fromLoc = "Technology";
                    fromLine = "LineD";
                }

                if (rbGrassToLine1.Checked)
                {
                    toLoc = "Grassland";
                    toLine = "Line1";
                }
                else if (rbGrassToLine2.Checked)
                {
                    toLoc = "Grassland";
                    toLine = "Line2";
                }
                else if (rbGrassToLine3.Checked)
                {
                    toLoc = "Grassland";
                    toLine = "Line3";
                }
                else if (rbGrassToLine4.Checked)
                {
                    toLoc = "Grassland";
                    toLine = "Line4";
                }
                else if (rbTechToLineA.Checked)
                {
                    toLoc = "Technology";
                    toLine = "LineA";
                }
                else if (rbTechToLineB.Checked)
                {
                    toLoc = "Technology";
                    toLine = "LineB";
                }
                else if (rbTechToLineC.Checked)
                {
                    toLoc = "Technology";
                    toLine = "LineC";
                }
                else if (rbTechToLineD.Checked)
                {
                    toLoc = "Technology";
                    toLine = "LineD";
                }

                DialogResult result1 = MessageBox.Show("You are attempting to transfer Job# " + jobNum + " from " + fromLoc + " " + fromLine + 
                                                       "\nto " + toLoc + " " + toLine + ". If you wish to proceed, then press 'Yes' to Continue otherwise press 'No' to Cancel.",
                                                       "ATTENTION ATTENTION ATTENTION",
                                                       MessageBoxButtons.YesNo,
                                                       MessageBoxIcon.Exclamation);
                if (result1 == DialogResult.Yes)
                {
                    string epicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
                    string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;
                    Console.WriteLine(epicorServer);

                    TransferJob tj = new TransferJob(epicorServer, epicorSqlConnectionString);

                    if (fromLoc == "Grassland" && toLoc == "Grassland")
                    {
                        lbMessage.Text = "Under development.";                       
                        //processGrasslandToGrasslandLineTransfer(tj, jobNum, toLoc, toLine);                        
                    }
                    else if (fromLoc == "Technology" && toLoc == "Technology")
                    {
                        lbMessage.Text = "Reconfiguring operations from Technology " + fromLine + " to Technology " + toLine;
                        this.Refresh();
                        tj.ProcessJobTechnologyToTechnology(jobNum, partNum, fromLoc, fromLine, toLoc, toLine);
                        lbMessage.Text = "Complete!";
                    }
                    else if (fromLoc == "Grassland" && toLoc == "Technology")
                    {
                        lbMessage.Text = "Reconfiguring operations from Grassland " + fromLine + " to Technology " + toLine;
                        this.Refresh();
                        tj.ProcessJobGrasslandToTechnology(jobNum, partNum, fromLoc, fromLine, toLoc, toLine);
                        lbMessage.Text = "Complete!";
                    }                   
                }                    
            }
        }

        private void  processGrasslandToGrasslandLineTransfer(TransferJob tj, string jobNum, string toLoc, string toLine)
        {
            string slotStartTime = "";
            
            double prodHrs = 0;
           
            prodHrs = calcProdHrs(jobNum);
            lbMessage.Text = "Scheduling Job# " + jobNum;
            //if (SelectedSlot == "Slot3" && gbSlot3.Text == "Open Slot") 
            //{               
            //    Slot1_StartTime = "09:00";
            //    slotStartTime = "09:00";
            //    //tj.ScheduleJobToOpenSlot(jobNum, toLoc, toLine, slotStartTime, prodHrs, StartDate);
            //}
            //else if (SelectedSlot == "Slot2" && gbSlot2.Text == "Open Slot")
            //{
            //    Slot1_StartTime = "07:00";
            //    slotStartTime = "07:00";
            //    //tj.ScheduleJobToOpenSlot(jobNum, toLoc, toLine, slotStartTime, prodHrs, StartDate);
            //}
            //else if (SelectedSlot == "Slot1" && gbSlot1.Text == "Open Slot")
            //{
            //    Slot1_StartTime = "05:00";
            //    slotStartTime = "05:00";
            //    //tj.ScheduleJobToOpenSlot(jobNum, toLoc, toLine, slotStartTime, prodHrs, StartDate);
            //}
           
        }

        private double calcProdHrs(string jobNum)
        {
            string prodHrs = "0";
           
            int asmSeq = 0;

            double totProdHrs = 0;

            DataTable dtJob = objMain.GetOA_JobOperations(jobNum);

            foreach(DataRow dr in dtJob.Rows)  // Add all of the productions hours
            {
                asmSeq = Int32.Parse(dr["AssemblySeq"].ToString());

                if (asmSeq <= 2)
                {
                    prodHrs = dr["ProdStandard"].ToString();
                    totProdHrs += double.Parse(prodHrs);
                }
            }

            totProdHrs += 8; // This 8 hours is to account for the extra day of travel time given for the Electron control panel.
            return totProdHrs;
        }

        private void dtpToLineDate_ValueChanged(object sender, EventArgs e)
        {
            string jobNum = "";
            string startHour = "";
            string dueHour = "";
            string startDate = "";
        
            int rowIdx = 1;
            //StartDate = dtpToLineDate.Value;
            //if (ToLine != "Empty")
            //{
            //    startDate =  StartDate.ToShortDateString();
            //    gbLineSchd.Text = ToLine + " Schedule for " + startDate;
            //    DataTable dt = objTran.GetLineScheduleByDate(ToLine, StartDate);
            //    if (dt.Rows.Count == 0)
            //    {
            //        gbSlot1.Text = "Open Slot";
            //        lbSlot1StartHour.Text = "05:00";
            //        lbSlot1DueHour.Text = "??:??";
            //        gbSlot2.Text = "Open Slot";
            //        lbSlot2StartHour.Text = "07:00";
            //        lbSlot2DueHour.Text = "??:??";
            //        gbSlot3.Text = "Open Slot";
            //        lbSlot3StartHour.Text = "09:00";
            //        lbSlot3DueHour.Text = "??:??";
            //        gbSlot4.Text = "Open Slot";
            //        lbSlot4StartHour.Text = "11:00";
            //        lbSlot4DueHour.Text = "??:??";
            //        gbSlot5.Text = "Open Slot";
            //        lbSlot5StartHour.Text = "13:00";
            //        lbSlot5DueHour.Text = "??:??";
            //    }
            //    else
            //    {
            //        foreach (DataRow row in dt.Rows)
            //        {
            //            if (row["JobNum"] != null)
            //            {
            //                jobNum = row["JobNum"].ToString();
            //            }
            //            else
            //            {
            //                jobNum = "Open Slot";
            //            }

            //            if (row["StartHour"] != null)
            //            {
            //                startHour = row["StartHour"].ToString();
            //            }
            //            if (row["DueHour"] != null)
            //            {
            //                dueHour = row["DueHour"].ToString();
            //            }

            //            if (rowIdx == 1)
            //            {
            //                gbSlot1.Text = jobNum;
            //                if (jobNum != "EmptySlot")
            //                {
            //                    gbSlot1.Text = jobNum;
            //                    lbSlot1StartHour.Text = startHour;
            //                    Slot1_StartTime = startHour;
            //                    lbSlot1DueHour.Text = dueHour;
            //                    Slot1_EndTime = dueHour;
            //                }
            //            }
            //            else if (rowIdx == 2)
            //            {
            //                gbSlot2.Text = jobNum;
            //                if (jobNum != "EmptySlot")
            //                {
            //                    gbSlot2.Text = jobNum;
            //                    lbSlot2StartHour.Text = startHour;
            //                    Slot2_StartTime = startHour;
            //                    lbSlot2DueHour.Text = dueHour;
            //                    Slot2_EndTime = dueHour;
            //                }
            //            }
            //            else if (rowIdx == 3)
            //            {
            //                gbSlot3.Text = jobNum;

            //                if (jobNum != "EmptySlot")
            //                {
            //                    gbSlot3.Text = jobNum;
            //                    lbSlot3StartHour.Text = startHour;
            //                    Slot3_StartTime = startHour;
            //                    lbSlot3DueHour.Text = dueHour;
            //                    Slot3_EndTime = dueHour;
            //                }

            //            }
            //            else if (rowIdx == 4)
            //            {
            //                if (jobNum != "EmptySlot")
            //                {
            //                    gbSlot4.Text = jobNum;
            //                    lbSlot4StartHour.Text = startHour;
            //                    Slot4_StartTime = startHour;
            //                    lbSlot4DueHour.Text = dueHour;
            //                    Slot4_EndTime = dueHour;
            //                }
            //            }
            //            else if (rowIdx == 5)
            //            {
            //                if (jobNum != "EmptySlot")
            //                {
            //                    gbSlot5.Text = jobNum;
            //                    lbSlot5StartHour.Text = startHour;
            //                    Slot5_StartTime = startHour;
            //                    lbSlot5DueHour.Text = dueHour;
            //                    Slot5_EndTime = dueHour;
            //                }
            //            }
            //            ++rowIdx;
            //        }

            //        if ((rowIdx == 4) && (ToLoc == "Technology"))
            //        {
            //            gbSlot4.Visible = true;
            //        }
            //        ++rowIdx;
            //        if ((rowIdx == 5) && (ToLoc == "Technology"))
            //        {
            //            gbSlot5.Visible = true;
            //        }
            //        ++rowIdx;
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("ERROR - No To Line has been selected!");
            //}
        }

        private void rbGrassToLine1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine1.Checked == true)
            {
                ToLine = "Line1";
                ToLoc = "Grassland";
            }
        }

        private void rbGrassToLine2_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine2.Checked == true)
            {
                ToLine = "Line2";
                ToLoc = "Grassland";
            }
        }

        private void rbGrassToLine3_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine3.Checked == true)
            {
                ToLine = "Line3";
                ToLoc = "Grassland";
            }
        }

        private void rbGrassToLine4_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrassToLine4.Checked == true)
            {
                ToLine = "Line4";
                ToLoc = "Grassland";
            }
        }

        private void rbTechToLineA_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineA.Checked == true)
            {
                ToLine = "LineA";
                ToLoc = "Technology";
            }
        }

        private void rbTechToLineB_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineB.Checked == true)
            {
                ToLine = "Line B";
                ToLoc = "Technology";
            }
        }

        private void rbTechToLineC_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineC.Checked == true)
            {
                ToLine = "Line C";
                ToLoc = "Technology";
            }
        }

        private void rbTechToLineD_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTechToLineD.Checked == true)
            {
                ToLine = "Line D";
                ToLoc = "Technology";
            }
        }

        //private void rbSlot1_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbSlot1.Checked == true)
        //    {
        //        SelectedSlot = "Slot1";
        //        btnSubmit.Enabled = true;
        //    }
        //}

        //private void rbSlot2_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbSlot2.Checked == true)
        //    {
        //        SelectedSlot = "Slot2";
        //        btnSubmit.Enabled = true;
        //    }
        //}

        //private void rbSlot3_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbSlot3.Checked == true)
        //    {
        //        SelectedSlot = "Slot3";
        //        btnSubmit.Enabled = true;
        //    }
        //}

        //private void rbSlot4_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbSlot4.Checked == true)
        //    {
        //        SelectedSlot = "Slot4";
        //        btnSubmit.Enabled = true;
        //    }
        //}

        //private void rbSlot5_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbSlot5.Checked == true)
        //    {
        //        SelectedSlot = "Slot5";
        //        btnSubmit.Enabled = true;
        //    }
        //}
       
    }
}
