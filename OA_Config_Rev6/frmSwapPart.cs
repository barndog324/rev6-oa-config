﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace OA_Config_Rev6
{
    public partial class frmSwapPart : Form
    {
        MainBO objMain = new MainBO();

        public frmSwapPart()
        {
            InitializeComponent();
        }

        private void frmSwapPart_Load(object sender, EventArgs e)
        {
            if (lbMode.Text == "Swap")
            {
               foreach(DataGridViewRow row in dgvSwapList.Rows)
               {
                   row.Cells["ProcessRow"].Value = true;
               }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string errorMsg = "";

            foreach (DataGridViewRow row in dgvSwapList.Rows)
            {
                if (Convert.ToBoolean(row.Cells["ProcessRow"].Value) == true)
                {
                    if (row.Cells["OldPartNum"].Value.ToString().Length == 0 &&
                             row.Cells["NewPartNum"].Value.ToString().Length == 0)
                    {
                        errorMsg += ("ERROR - JobNum = " + row.Cells["JobNum"].Value.ToString() + " has neither an Old or New part number.\n");
                    }
                }
            }

            if (errorMsg.Length == 0)
            {
                foreach (DataGridViewRow row in dgvSwapList.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["ProcessRow"].Value) == true) // ProcessRow checkbox is checked
                    {
                        errorMsg = processKitSwap(row.Cells["JobNum"].Value.ToString(), 
                                                  row.Cells["UnitType"].Value.ToString(),
                                                  row.Cells["OldPartNum"].Value.ToString(), 
                                                  row.Cells["NewPartNum"].Value.ToString());
                        row.Cells["Results"].Value = errorMsg;
                        dgvSwapList.Refresh();
                    }                   
                }
            }
            else
            {
                MessageBox.Show(errorMsg);
            }
        }

        #region OtherMethods

        private string processKitSwap(string jobNum, string unitType, string oldPartNum, string newPartNum)
        {
            string epicorServer = "";
            string errorMsg = "";

            if (GlobalJob.DisplayMode == "Production")
            {
                epicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            }
            else if (GlobalJob.DisplayMode == "Debug")
            {
                epicorServer = ConfigurationManager.AppSettings["DevEpicorAppServer"];
            }
            else
            {
                epicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            }

            string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;

            Console.WriteLine(epicorServer);

            BuildBOM bb = new BuildBOM(epicorServer, epicorSqlConnectionString);

            if (oldPartNum.Length > 0)
            {
                errorMsg = bb.DeleteKitParts(jobNum, oldPartNum);
            }

            if (errorMsg.Length == 0)
            {
                if (oldPartNum.Length > 0)
                {
                    lbMsg.Text = oldPartNum + " - Successfully deleted!";
                }
                if (newPartNum.Length > 0)
                {
                    errorMsg = bb.InsertKitParts(jobNum, newPartNum, unitType);
                }
            }
            else
            {
                lbMsg.Text = "ERROR - Deleting Kit - PartNum = " + oldPartNum;
            }

            if (errorMsg.Length == 0)
            {
                errorMsg = "Process completed successfully!";
            }

            return errorMsg;                          
        }

        
        #endregion

       
    }
}
