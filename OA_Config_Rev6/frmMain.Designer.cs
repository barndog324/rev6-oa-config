﻿namespace OA_Config_Rev6
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnExit = new System.Windows.Forms.Button();
            this.msMainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchConfigureJobsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.findPartsOnJobsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removePartsOrderedFlagToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapPartsOnJobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oAModelNoConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oARev6ModelNoConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monitorModelNoConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mSPModelNoConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vikingModelNoConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.criticalComponentsPurchasePartsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partsByJobNumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobsByDateRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiPurchasePartsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partsByJobNumberToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newPicklistByStationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oAUBOMPicklistToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sheetMetalForecastingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sheetMetalByJobNumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sheetMetalByDateRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partDemandByDateRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobGoodToGoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.picklistByDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tandemCompressorJobsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tandemCompressorByJobNumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobHistoryByJobNumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sheetMetalSuperMarketPicklistToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refrigerationComponentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sheetMetalBendByJobNumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subAssemblyStationJobDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partAssemblyBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vMEAndRFGASMPartsByJobNumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eTLLabelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineTransferJobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scheduleJobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scheduleExtractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partMaintenanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rev5PartMainToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rev6PartMaintToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monitorPartMaintToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mSPPartMaintToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sheetMetalAllocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateJobQtysByReleaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMainReset = new System.Windows.Forms.Button();
            this.rbMainOpenJobs = new System.Windows.Forms.RadioButton();
            this.rbMainClosedJobs = new System.Windows.Forms.RadioButton();
            this.rbMainAllJobs = new System.Windows.Forms.RadioButton();
            this.txtMainSearchJobNum = new System.Windows.Forms.TextBox();
            this.lbMainStartAt = new System.Windows.Forms.Label();
            this.lbMainMsg = new System.Windows.Forms.Label();
            this.dgvMainUnitList = new System.Windows.Forms.DataGridView();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPageOA_Units = new System.Windows.Forms.TabPage();
            this.gbPartSearch = new System.Windows.Forms.GroupBox();
            this.btnExitSearch = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dgvPartSearch = new System.Windows.Forms.DataGridView();
            this.PartNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageMonitor = new System.Windows.Forms.TabPage();
            this.dgvMonitorMainList = new System.Windows.Forms.DataGridView();
            this.tabPageRRU = new System.Windows.Forms.TabPage();
            this.dgvRRU_MainList = new System.Windows.Forms.DataGridView();
            this.tabPageMSP = new System.Windows.Forms.TabPage();
            this.dgvMSP_MainList = new System.Windows.Forms.DataGridView();
            this.tabPageViking = new System.Windows.Forms.TabPage();
            this.dgvViking_MainList = new System.Windows.Forms.DataGridView();
            this.lbMainTitle = new System.Windows.Forms.Label();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.mixedAirPartMaintToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.msMainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainUnitList)).BeginInit();
            this.tabMain.SuspendLayout();
            this.tabPageOA_Units.SuspendLayout();
            this.gbPartSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartSearch)).BeginInit();
            this.tabPageMonitor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitorMainList)).BeginInit();
            this.tabPageRRU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRRU_MainList)).BeginInit();
            this.tabPageMSP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMSP_MainList)).BeginInit();
            this.tabPageViking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViking_MainList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1320, 71);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 33);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // msMainMenu
            // 
            this.msMainMenu.BackColor = System.Drawing.Color.LightSalmon;
            this.msMainMenu.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.displayToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.eTLLabelsToolStripMenuItem,
            this.lineTransferJobToolStripMenuItem,
            this.partMaintenanceToolStripMenuItem,
            this.sheetMetalAllocationToolStripMenuItem});
            this.msMainMenu.Location = new System.Drawing.Point(0, 0);
            this.msMainMenu.Name = "msMainMenu";
            this.msMainMenu.Size = new System.Drawing.Size(1452, 24);
            this.msMainMenu.TabIndex = 2;
            this.msMainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.batchConfigureJobsToolStripMenuItem1,
            this.findPartsOnJobsToolStripMenuItem,
            this.removePartsOrderedFlagToolStripMenuItem,
            this.swapPartsOnJobToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // batchConfigureJobsToolStripMenuItem1
            // 
            this.batchConfigureJobsToolStripMenuItem1.Name = "batchConfigureJobsToolStripMenuItem1";
            this.batchConfigureJobsToolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.batchConfigureJobsToolStripMenuItem1.Text = "BatchConfigureJobs";
            this.batchConfigureJobsToolStripMenuItem1.Click += new System.EventHandler(this.batchConfigureJobsToolStripMenuItem1_Click);
            // 
            // findPartsOnJobsToolStripMenuItem
            // 
            this.findPartsOnJobsToolStripMenuItem.Name = "findPartsOnJobsToolStripMenuItem";
            this.findPartsOnJobsToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.findPartsOnJobsToolStripMenuItem.Text = "FindPartsOnJobs";
            this.findPartsOnJobsToolStripMenuItem.Click += new System.EventHandler(this.findPartsOnJobsToolStripMenuItem_Click);
            // 
            // removePartsOrderedFlagToolStripMenuItem
            // 
            this.removePartsOrderedFlagToolStripMenuItem.Name = "removePartsOrderedFlagToolStripMenuItem";
            this.removePartsOrderedFlagToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.removePartsOrderedFlagToolStripMenuItem.Text = "RemovePartsOrderedFlag";
            this.removePartsOrderedFlagToolStripMenuItem.Click += new System.EventHandler(this.removePartsOrderedFlagToolStripMenuItem_Click);
            // 
            // swapPartsOnJobToolStripMenuItem
            // 
            this.swapPartsOnJobToolStripMenuItem.Name = "swapPartsOnJobToolStripMenuItem";
            this.swapPartsOnJobToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.swapPartsOnJobToolStripMenuItem.Text = "SwapPartsOnJob";
            this.swapPartsOnJobToolStripMenuItem.Click += new System.EventHandler(this.swapPartsOnJobToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oAModelNoConfigurationToolStripMenuItem,
            this.oARev6ModelNoConfigurationToolStripMenuItem,
            this.monitorModelNoConfigurationToolStripMenuItem,
            this.mSPModelNoConfigurationToolStripMenuItem,
            this.vikingModelNoConfigurationToolStripMenuItem});
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.displayToolStripMenuItem.Text = "Display";
            // 
            // oAModelNoConfigurationToolStripMenuItem
            // 
            this.oAModelNoConfigurationToolStripMenuItem.Name = "oAModelNoConfigurationToolStripMenuItem";
            this.oAModelNoConfigurationToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.oAModelNoConfigurationToolStripMenuItem.Text = "OA Model No Configuration";
            this.oAModelNoConfigurationToolStripMenuItem.Click += new System.EventHandler(this.oAModelNoConfigurationToolStripMenuItem_Click);
            // 
            // oARev6ModelNoConfigurationToolStripMenuItem
            // 
            this.oARev6ModelNoConfigurationToolStripMenuItem.Name = "oARev6ModelNoConfigurationToolStripMenuItem";
            this.oARev6ModelNoConfigurationToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.oARev6ModelNoConfigurationToolStripMenuItem.Text = "OA Rev6 ModelNo Configuration";
            this.oARev6ModelNoConfigurationToolStripMenuItem.Click += new System.EventHandler(this.oARev6ModelNoConfigurationToolStripMenuItem_Click);
            // 
            // monitorModelNoConfigurationToolStripMenuItem
            // 
            this.monitorModelNoConfigurationToolStripMenuItem.Name = "monitorModelNoConfigurationToolStripMenuItem";
            this.monitorModelNoConfigurationToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.monitorModelNoConfigurationToolStripMenuItem.Text = "Monitor ModelNo Configuration";
            this.monitorModelNoConfigurationToolStripMenuItem.Click += new System.EventHandler(this.monitorModelNoConfigurationToolStripMenuItem_Click);
            // 
            // mSPModelNoConfigurationToolStripMenuItem
            // 
            this.mSPModelNoConfigurationToolStripMenuItem.Name = "mSPModelNoConfigurationToolStripMenuItem";
            this.mSPModelNoConfigurationToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.mSPModelNoConfigurationToolStripMenuItem.Text = "MSP ModelNo Configuration";
            this.mSPModelNoConfigurationToolStripMenuItem.Click += new System.EventHandler(this.mSPModelNoConfigurationToolStripMenuItem_Click);
            // 
            // vikingModelNoConfigurationToolStripMenuItem
            // 
            this.vikingModelNoConfigurationToolStripMenuItem.Name = "vikingModelNoConfigurationToolStripMenuItem";
            this.vikingModelNoConfigurationToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.vikingModelNoConfigurationToolStripMenuItem.Text = "MixedAirUnit ModelNo Configuration";
            this.vikingModelNoConfigurationToolStripMenuItem.Click += new System.EventHandler(this.vikingModelNoConfigurationToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.criticalComponentsPurchasePartsToolStripMenuItem,
            this.multiPurchasePartsToolStripMenuItem,
            this.newPicklistByStationToolStripMenuItem,
            this.oAUBOMPicklistToolStripMenuItem,
            this.sheetMetalForecastingToolStripMenuItem,
            this.partDemandByDateRangeToolStripMenuItem,
            this.jobGoodToGoToolStripMenuItem,
            this.picklistByDateToolStripMenuItem,
            this.tandemCompressorJobsToolStripMenuItem,
            this.tandemCompressorByJobNumberToolStripMenuItem,
            this.jobHistoryByJobNumberToolStripMenuItem,
            this.sheetMetalSuperMarketPicklistToolStripMenuItem,
            this.refrigerationComponentsToolStripMenuItem,
            this.sheetMetalBendByJobNumberToolStripMenuItem,
            this.subAssemblyStationJobDataToolStripMenuItem,
            this.partAssemblyBOMToolStripMenuItem,
            this.vMEAndRFGASMPartsByJobNumToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // criticalComponentsPurchasePartsToolStripMenuItem
            // 
            this.criticalComponentsPurchasePartsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.partsByJobNumberToolStripMenuItem,
            this.jobsByDateRangeToolStripMenuItem});
            this.criticalComponentsPurchasePartsToolStripMenuItem.Name = "criticalComponentsPurchasePartsToolStripMenuItem";
            this.criticalComponentsPurchasePartsToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.criticalComponentsPurchasePartsToolStripMenuItem.Text = "Critical Components Purchase Parts";
            // 
            // partsByJobNumberToolStripMenuItem
            // 
            this.partsByJobNumberToolStripMenuItem.Name = "partsByJobNumberToolStripMenuItem";
            this.partsByJobNumberToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.partsByJobNumberToolStripMenuItem.Text = "Parts by Job Number";
            this.partsByJobNumberToolStripMenuItem.Click += new System.EventHandler(this.partsByJobNumberToolStripMenuItem_Click);
            // 
            // jobsByDateRangeToolStripMenuItem
            // 
            this.jobsByDateRangeToolStripMenuItem.Name = "jobsByDateRangeToolStripMenuItem";
            this.jobsByDateRangeToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.jobsByDateRangeToolStripMenuItem.Text = "Jobs by Date Range";
            this.jobsByDateRangeToolStripMenuItem.Click += new System.EventHandler(this.jobsByDateRangeToolStripMenuItem_Click);
            // 
            // multiPurchasePartsToolStripMenuItem
            // 
            this.multiPurchasePartsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.partsByJobNumberToolStripMenuItem1});
            this.multiPurchasePartsToolStripMenuItem.Name = "multiPurchasePartsToolStripMenuItem";
            this.multiPurchasePartsToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.multiPurchasePartsToolStripMenuItem.Text = "Multi Purchase Parts";
            // 
            // partsByJobNumberToolStripMenuItem1
            // 
            this.partsByJobNumberToolStripMenuItem1.Name = "partsByJobNumberToolStripMenuItem1";
            this.partsByJobNumberToolStripMenuItem1.Size = new System.Drawing.Size(190, 22);
            this.partsByJobNumberToolStripMenuItem1.Text = "Parts By Job Number";
            this.partsByJobNumberToolStripMenuItem1.Click += new System.EventHandler(this.partsByJobNumberToolStripMenuItem1_Click);
            // 
            // newPicklistByStationToolStripMenuItem
            // 
            this.newPicklistByStationToolStripMenuItem.Name = "newPicklistByStationToolStripMenuItem";
            this.newPicklistByStationToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.newPicklistByStationToolStripMenuItem.Text = "New Picklist By Station";
            this.newPicklistByStationToolStripMenuItem.Click += new System.EventHandler(this.newPicklistByStationToolStripMenuItem_Click);
            // 
            // oAUBOMPicklistToolStripMenuItem
            // 
            this.oAUBOMPicklistToolStripMenuItem.Name = "oAUBOMPicklistToolStripMenuItem";
            this.oAUBOMPicklistToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.oAUBOMPicklistToolStripMenuItem.Text = "OAU BOM Picklist";
            this.oAUBOMPicklistToolStripMenuItem.Click += new System.EventHandler(this.oAUBOMPicklistToolStripMenuItem_Click);
            // 
            // sheetMetalForecastingToolStripMenuItem
            // 
            this.sheetMetalForecastingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sheetMetalByJobNumberToolStripMenuItem,
            this.sheetMetalByDateRangeToolStripMenuItem});
            this.sheetMetalForecastingToolStripMenuItem.Name = "sheetMetalForecastingToolStripMenuItem";
            this.sheetMetalForecastingToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.sheetMetalForecastingToolStripMenuItem.Text = "Sheet Metal Forecasting";
            // 
            // sheetMetalByJobNumberToolStripMenuItem
            // 
            this.sheetMetalByJobNumberToolStripMenuItem.Name = "sheetMetalByJobNumberToolStripMenuItem";
            this.sheetMetalByJobNumberToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.sheetMetalByJobNumberToolStripMenuItem.Text = "Sheet Metal by Job Number";
            this.sheetMetalByJobNumberToolStripMenuItem.Click += new System.EventHandler(this.sheetMetalByJobNumberToolStripMenuItem_Click);
            // 
            // sheetMetalByDateRangeToolStripMenuItem
            // 
            this.sheetMetalByDateRangeToolStripMenuItem.Name = "sheetMetalByDateRangeToolStripMenuItem";
            this.sheetMetalByDateRangeToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.sheetMetalByDateRangeToolStripMenuItem.Text = "Sheet Metal by Date Range";
            this.sheetMetalByDateRangeToolStripMenuItem.Click += new System.EventHandler(this.sheetMetalByDateRangeToolStripMenuItem_Click);
            // 
            // partDemandByDateRangeToolStripMenuItem
            // 
            this.partDemandByDateRangeToolStripMenuItem.Name = "partDemandByDateRangeToolStripMenuItem";
            this.partDemandByDateRangeToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.partDemandByDateRangeToolStripMenuItem.Text = "Part Demand by Date Range";
            this.partDemandByDateRangeToolStripMenuItem.Click += new System.EventHandler(this.partDemandByDateRangeToolStripMenuItem_Click);
            // 
            // jobGoodToGoToolStripMenuItem
            // 
            this.jobGoodToGoToolStripMenuItem.Name = "jobGoodToGoToolStripMenuItem";
            this.jobGoodToGoToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.jobGoodToGoToolStripMenuItem.Text = "JobGoodToGo";
            this.jobGoodToGoToolStripMenuItem.Click += new System.EventHandler(this.jobGoodToGoToolStripMenuItem_Click);
            // 
            // picklistByDateToolStripMenuItem
            // 
            this.picklistByDateToolStripMenuItem.Name = "picklistByDateToolStripMenuItem";
            this.picklistByDateToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.picklistByDateToolStripMenuItem.Text = "Picklist By Date";
            this.picklistByDateToolStripMenuItem.Click += new System.EventHandler(this.picklistByDateToolStripMenuItem_Click);
            // 
            // tandemCompressorJobsToolStripMenuItem
            // 
            this.tandemCompressorJobsToolStripMenuItem.Name = "tandemCompressorJobsToolStripMenuItem";
            this.tandemCompressorJobsToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.tandemCompressorJobsToolStripMenuItem.Text = "Tandem Compressor Jobs";
            this.tandemCompressorJobsToolStripMenuItem.Click += new System.EventHandler(this.tandemCompressorJobsToolStripMenuItem_Click);
            // 
            // tandemCompressorByJobNumberToolStripMenuItem
            // 
            this.tandemCompressorByJobNumberToolStripMenuItem.Name = "tandemCompressorByJobNumberToolStripMenuItem";
            this.tandemCompressorByJobNumberToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.tandemCompressorByJobNumberToolStripMenuItem.Text = "Tandem Compressor By Job Number";
            this.tandemCompressorByJobNumberToolStripMenuItem.Click += new System.EventHandler(this.tandemCompressorByJobNumberToolStripMenuItem_Click);
            // 
            // jobHistoryByJobNumberToolStripMenuItem
            // 
            this.jobHistoryByJobNumberToolStripMenuItem.Name = "jobHistoryByJobNumberToolStripMenuItem";
            this.jobHistoryByJobNumberToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.jobHistoryByJobNumberToolStripMenuItem.Text = "Job History By Job Number";
            this.jobHistoryByJobNumberToolStripMenuItem.Click += new System.EventHandler(this.jobHistoryByJobNumberToolStripMenuItem_Click);
            // 
            // sheetMetalSuperMarketPicklistToolStripMenuItem
            // 
            this.sheetMetalSuperMarketPicklistToolStripMenuItem.Name = "sheetMetalSuperMarketPicklistToolStripMenuItem";
            this.sheetMetalSuperMarketPicklistToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.sheetMetalSuperMarketPicklistToolStripMenuItem.Text = "SheetMetal SuperMarket Picklist";
            this.sheetMetalSuperMarketPicklistToolStripMenuItem.Click += new System.EventHandler(this.sheetMetalSuperMarketPicklistToolStripMenuItem_Click);
            // 
            // refrigerationComponentsToolStripMenuItem
            // 
            this.refrigerationComponentsToolStripMenuItem.Name = "refrigerationComponentsToolStripMenuItem";
            this.refrigerationComponentsToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.refrigerationComponentsToolStripMenuItem.Text = "Refrigeration Components";
            this.refrigerationComponentsToolStripMenuItem.Click += new System.EventHandler(this.refrigerationComponentsToolStripMenuItem_Click);
            // 
            // sheetMetalBendByJobNumberToolStripMenuItem
            // 
            this.sheetMetalBendByJobNumberToolStripMenuItem.Name = "sheetMetalBendByJobNumberToolStripMenuItem";
            this.sheetMetalBendByJobNumberToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.sheetMetalBendByJobNumberToolStripMenuItem.Text = "SheetMetalBend by Job Number";
            this.sheetMetalBendByJobNumberToolStripMenuItem.Click += new System.EventHandler(this.sheetMetalBendByJobNumberToolStripMenuItem_Click);
            // 
            // subAssemblyStationJobDataToolStripMenuItem
            // 
            this.subAssemblyStationJobDataToolStripMenuItem.Name = "subAssemblyStationJobDataToolStripMenuItem";
            this.subAssemblyStationJobDataToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.subAssemblyStationJobDataToolStripMenuItem.Text = "SubAssembly Station Job Data";
            this.subAssemblyStationJobDataToolStripMenuItem.Click += new System.EventHandler(this.subAssemblyStationJobDataToolStripMenuItem_Click);
            // 
            // partAssemblyBOMToolStripMenuItem
            // 
            this.partAssemblyBOMToolStripMenuItem.Name = "partAssemblyBOMToolStripMenuItem";
            this.partAssemblyBOMToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.partAssemblyBOMToolStripMenuItem.Text = "PartAssemblyBOM";
            this.partAssemblyBOMToolStripMenuItem.Click += new System.EventHandler(this.partAssemblyBOMToolStripMenuItem_Click);
            // 
            // vMEAndRFGASMPartsByJobNumToolStripMenuItem
            // 
            this.vMEAndRFGASMPartsByJobNumToolStripMenuItem.Name = "vMEAndRFGASMPartsByJobNumToolStripMenuItem";
            this.vMEAndRFGASMPartsByJobNumToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.vMEAndRFGASMPartsByJobNumToolStripMenuItem.Text = "VME and RFGASM Parts by Job Num";
            this.vMEAndRFGASMPartsByJobNumToolStripMenuItem.Click += new System.EventHandler(this.vMEAndRFGASMPartsByJobNumToolStripMenuItem_Click);
            // 
            // eTLLabelsToolStripMenuItem
            // 
            this.eTLLabelsToolStripMenuItem.Name = "eTLLabelsToolStripMenuItem";
            this.eTLLabelsToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.eTLLabelsToolStripMenuItem.Text = "ETL Labels";
            this.eTLLabelsToolStripMenuItem.Click += new System.EventHandler(this.eTLLabelsToolStripMenuItem_Click);
            // 
            // lineTransferJobToolStripMenuItem
            // 
            this.lineTransferJobToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lineTransferToolStripMenuItem,
            this.scheduleJobToolStripMenuItem,
            this.scheduleExtractToolStripMenuItem});
            this.lineTransferJobToolStripMenuItem.Name = "lineTransferJobToolStripMenuItem";
            this.lineTransferJobToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.lineTransferJobToolStripMenuItem.Text = "Job Scheduling";
            // 
            // lineTransferToolStripMenuItem
            // 
            this.lineTransferToolStripMenuItem.Name = "lineTransferToolStripMenuItem";
            this.lineTransferToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.lineTransferToolStripMenuItem.Text = "Line Transfer";
            this.lineTransferToolStripMenuItem.Click += new System.EventHandler(this.lineTransferToolStripMenuItem_Click);
            // 
            // scheduleJobToolStripMenuItem
            // 
            this.scheduleJobToolStripMenuItem.Name = "scheduleJobToolStripMenuItem";
            this.scheduleJobToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.scheduleJobToolStripMenuItem.Text = "Batch Schedule Jobs";
            this.scheduleJobToolStripMenuItem.Click += new System.EventHandler(this.scheduleJobToolStripMenuItem_Click);
            // 
            // scheduleExtractToolStripMenuItem
            // 
            this.scheduleExtractToolStripMenuItem.Name = "scheduleExtractToolStripMenuItem";
            this.scheduleExtractToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.scheduleExtractToolStripMenuItem.Text = "ScheduleExtract";
            this.scheduleExtractToolStripMenuItem.Click += new System.EventHandler(this.scheduleExtractToolStripMenuItem_Click);
            // 
            // partMaintenanceToolStripMenuItem
            // 
            this.partMaintenanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rev5PartMainToolToolStripMenuItem,
            this.rev6PartMaintToolToolStripMenuItem,
            this.monitorPartMaintToolToolStripMenuItem,
            this.mSPPartMaintToolToolStripMenuItem,
            this.mixedAirPartMaintToolToolStripMenuItem});
            this.partMaintenanceToolStripMenuItem.Name = "partMaintenanceToolStripMenuItem";
            this.partMaintenanceToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.partMaintenanceToolStripMenuItem.Text = "Part Maintenance";
            // 
            // rev5PartMainToolToolStripMenuItem
            // 
            this.rev5PartMainToolToolStripMenuItem.Name = "rev5PartMainToolToolStripMenuItem";
            this.rev5PartMainToolToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.rev5PartMainToolToolStripMenuItem.Text = "Rev 5 Part Main Tool";
            this.rev5PartMainToolToolStripMenuItem.Click += new System.EventHandler(this.rev5PartMainToolToolStripMenuItem_Click);
            // 
            // rev6PartMaintToolToolStripMenuItem
            // 
            this.rev6PartMaintToolToolStripMenuItem.Name = "rev6PartMaintToolToolStripMenuItem";
            this.rev6PartMaintToolToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.rev6PartMaintToolToolStripMenuItem.Text = "Rev 6 Part Maint Tool";
            this.rev6PartMaintToolToolStripMenuItem.Click += new System.EventHandler(this.rev6PartMaintToolToolStripMenuItem_Click);
            // 
            // monitorPartMaintToolToolStripMenuItem
            // 
            this.monitorPartMaintToolToolStripMenuItem.Name = "monitorPartMaintToolToolStripMenuItem";
            this.monitorPartMaintToolToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.monitorPartMaintToolToolStripMenuItem.Text = "Monitor Part Maint Tool";
            this.monitorPartMaintToolToolStripMenuItem.Click += new System.EventHandler(this.monitorPartMaintToolToolStripMenuItem_Click);
            // 
            // mSPPartMaintToolToolStripMenuItem
            // 
            this.mSPPartMaintToolToolStripMenuItem.Name = "mSPPartMaintToolToolStripMenuItem";
            this.mSPPartMaintToolToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.mSPPartMaintToolToolStripMenuItem.Text = "MSP Part Maint Tool";
            this.mSPPartMaintToolToolStripMenuItem.Click += new System.EventHandler(this.mSPPartMaintToolToolStripMenuItem_Click);
            // 
            // sheetMetalAllocationToolStripMenuItem
            // 
            this.sheetMetalAllocationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateJobQtysByReleaseToolStripMenuItem});
            this.sheetMetalAllocationToolStripMenuItem.Name = "sheetMetalAllocationToolStripMenuItem";
            this.sheetMetalAllocationToolStripMenuItem.Size = new System.Drawing.Size(133, 20);
            this.sheetMetalAllocationToolStripMenuItem.Text = "Sheet Metal Release";
            // 
            // updateJobQtysByReleaseToolStripMenuItem
            // 
            this.updateJobQtysByReleaseToolStripMenuItem.Name = "updateJobQtysByReleaseToolStripMenuItem";
            this.updateJobQtysByReleaseToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.updateJobQtysByReleaseToolStripMenuItem.Text = "UpdateJobQtysByRelease";
            this.updateJobQtysByReleaseToolStripMenuItem.Click += new System.EventHandler(this.updateJobQtysByReleaseToolStripMenuItem_Click);
            // 
            // btnMainReset
            // 
            this.btnMainReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMainReset.ForeColor = System.Drawing.Color.Teal;
            this.btnMainReset.Location = new System.Drawing.Point(1320, 32);
            this.btnMainReset.Name = "btnMainReset";
            this.btnMainReset.Size = new System.Drawing.Size(105, 33);
            this.btnMainReset.TabIndex = 23;
            this.btnMainReset.Text = "Reset";
            this.btnMainReset.UseVisualStyleBackColor = true;
            this.btnMainReset.Click += new System.EventHandler(this.btnMainReset_Click);
            // 
            // rbMainOpenJobs
            // 
            this.rbMainOpenJobs.AutoSize = true;
            this.rbMainOpenJobs.Checked = true;
            this.rbMainOpenJobs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMainOpenJobs.ForeColor = System.Drawing.Color.Black;
            this.rbMainOpenJobs.Location = new System.Drawing.Point(809, 85);
            this.rbMainOpenJobs.Name = "rbMainOpenJobs";
            this.rbMainOpenJobs.Size = new System.Drawing.Size(92, 20);
            this.rbMainOpenJobs.TabIndex = 21;
            this.rbMainOpenJobs.TabStop = true;
            this.rbMainOpenJobs.Text = "Open Jobs";
            this.rbMainOpenJobs.UseVisualStyleBackColor = true;
            this.rbMainOpenJobs.CheckedChanged += new System.EventHandler(this.rbMainOpenJobs_CheckedChanged);
            // 
            // rbMainClosedJobs
            // 
            this.rbMainClosedJobs.AutoSize = true;
            this.rbMainClosedJobs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMainClosedJobs.ForeColor = System.Drawing.Color.Black;
            this.rbMainClosedJobs.Location = new System.Drawing.Point(905, 85);
            this.rbMainClosedJobs.Name = "rbMainClosedJobs";
            this.rbMainClosedJobs.Size = new System.Drawing.Size(102, 20);
            this.rbMainClosedJobs.TabIndex = 22;
            this.rbMainClosedJobs.Text = "Closed Jobs";
            this.rbMainClosedJobs.UseVisualStyleBackColor = true;
            this.rbMainClosedJobs.CheckedChanged += new System.EventHandler(this.rbMainClosedJobs_CheckedChanged);
            // 
            // rbMainAllJobs
            // 
            this.rbMainAllJobs.AutoSize = true;
            this.rbMainAllJobs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMainAllJobs.ForeColor = System.Drawing.Color.Black;
            this.rbMainAllJobs.Location = new System.Drawing.Point(1011, 85);
            this.rbMainAllJobs.Name = "rbMainAllJobs";
            this.rbMainAllJobs.Size = new System.Drawing.Size(74, 20);
            this.rbMainAllJobs.TabIndex = 20;
            this.rbMainAllJobs.Text = "All Jobs";
            this.rbMainAllJobs.UseVisualStyleBackColor = true;
            this.rbMainAllJobs.CheckedChanged += new System.EventHandler(this.rbMainAllJobs_CheckedChanged);
            // 
            // txtMainSearchJobNum
            // 
            this.txtMainSearchJobNum.BackColor = System.Drawing.Color.White;
            this.txtMainSearchJobNum.ForeColor = System.Drawing.Color.Blue;
            this.txtMainSearchJobNum.Location = new System.Drawing.Point(653, 85);
            this.txtMainSearchJobNum.Name = "txtMainSearchJobNum";
            this.txtMainSearchJobNum.Size = new System.Drawing.Size(90, 20);
            this.txtMainSearchJobNum.TabIndex = 18;
            this.txtMainSearchJobNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMainSearchJobNum_KeyDown);
            // 
            // lbMainStartAt
            // 
            this.lbMainStartAt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainStartAt.ForeColor = System.Drawing.Color.Black;
            this.lbMainStartAt.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbMainStartAt.Location = new System.Drawing.Point(487, 85);
            this.lbMainStartAt.Name = "lbMainStartAt";
            this.lbMainStartAt.Size = new System.Drawing.Size(156, 20);
            this.lbMainStartAt.TabIndex = 17;
            this.lbMainStartAt.Text = "Search Job Number:";
            this.lbMainStartAt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbMainMsg
            // 
            this.lbMainMsg.BackColor = System.Drawing.Color.LightSalmon;
            this.lbMainMsg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainMsg.Location = new System.Drawing.Point(19, 731);
            this.lbMainMsg.Name = "lbMainMsg";
            this.lbMainMsg.Size = new System.Drawing.Size(1433, 20);
            this.lbMainMsg.TabIndex = 24;
            this.lbMainMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvMainUnitList
            // 
            this.dgvMainUnitList.AllowUserToAddRows = false;
            this.dgvMainUnitList.AllowUserToDeleteRows = false;
            this.dgvMainUnitList.AllowUserToResizeRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.White;
            this.dgvMainUnitList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMainUnitList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvMainUnitList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMainUnitList.DefaultCellStyle = dataGridViewCellStyle23;
            this.dgvMainUnitList.Location = new System.Drawing.Point(6, 7);
            this.dgvMainUnitList.Name = "dgvMainUnitList";
            this.dgvMainUnitList.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMainUnitList.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvMainUnitList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMainUnitList.Size = new System.Drawing.Size(1416, 592);
            this.dgvMainUnitList.TabIndex = 0;
            this.dgvMainUnitList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMainUnitList_CellMouseDoubleClick);
            this.dgvMainUnitList.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMainUnitList_DataBindingComplete);
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPageOA_Units);
            this.tabMain.Controls.Add(this.tabPageMonitor);
            this.tabMain.Controls.Add(this.tabPageRRU);
            this.tabMain.Controls.Add(this.tabPageMSP);
            this.tabMain.Controls.Add(this.tabPageViking);
            this.tabMain.Location = new System.Drawing.Point(12, 110);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1436, 619);
            this.tabMain.TabIndex = 1;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabPageOA_Units
            // 
            this.tabPageOA_Units.Controls.Add(this.gbPartSearch);
            this.tabPageOA_Units.Controls.Add(this.dgvMainUnitList);
            this.tabPageOA_Units.Location = new System.Drawing.Point(4, 22);
            this.tabPageOA_Units.Name = "tabPageOA_Units";
            this.tabPageOA_Units.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOA_Units.Size = new System.Drawing.Size(1428, 593);
            this.tabPageOA_Units.TabIndex = 0;
            this.tabPageOA_Units.Text = "OAU";
            this.tabPageOA_Units.UseVisualStyleBackColor = true;
            // 
            // gbPartSearch
            // 
            this.gbPartSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gbPartSearch.Controls.Add(this.btnExitSearch);
            this.gbPartSearch.Controls.Add(this.btnSearch);
            this.gbPartSearch.Controls.Add(this.dgvPartSearch);
            this.gbPartSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPartSearch.ForeColor = System.Drawing.Color.Blue;
            this.gbPartSearch.Location = new System.Drawing.Point(39, 26);
            this.gbPartSearch.Name = "gbPartSearch";
            this.gbPartSearch.Size = new System.Drawing.Size(222, 274);
            this.gbPartSearch.TabIndex = 1;
            this.gbPartSearch.TabStop = false;
            this.gbPartSearch.Text = "Part Search";
            this.gbPartSearch.Visible = false;
            // 
            // btnExitSearch
            // 
            this.btnExitSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitSearch.ForeColor = System.Drawing.Color.Red;
            this.btnExitSearch.Location = new System.Drawing.Point(131, 241);
            this.btnExitSearch.Name = "btnExitSearch";
            this.btnExitSearch.Size = new System.Drawing.Size(85, 25);
            this.btnExitSearch.TabIndex = 264;
            this.btnExitSearch.Text = "Exit";
            this.btnExitSearch.UseVisualStyleBackColor = true;
            this.btnExitSearch.Click += new System.EventHandler(this.btnExitSearch_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Blue;
            this.btnSearch.Location = new System.Drawing.Point(7, 241);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 25);
            this.btnSearch.TabIndex = 263;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dgvPartSearch
            // 
            this.dgvPartSearch.AllowUserToDeleteRows = false;
            this.dgvPartSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PartNum});
            this.dgvPartSearch.Location = new System.Drawing.Point(7, 20);
            this.dgvPartSearch.MultiSelect = false;
            this.dgvPartSearch.Name = "dgvPartSearch";
            this.dgvPartSearch.Size = new System.Drawing.Size(209, 213);
            this.dgvPartSearch.TabIndex = 0;
            // 
            // PartNum
            // 
            this.PartNum.HeaderText = "Part Number";
            this.PartNum.Name = "PartNum";
            this.PartNum.Width = 160;
            // 
            // tabPageMonitor
            // 
            this.tabPageMonitor.Controls.Add(this.dgvMonitorMainList);
            this.tabPageMonitor.Location = new System.Drawing.Point(4, 22);
            this.tabPageMonitor.Name = "tabPageMonitor";
            this.tabPageMonitor.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMonitor.Size = new System.Drawing.Size(1428, 593);
            this.tabPageMonitor.TabIndex = 1;
            this.tabPageMonitor.Text = "Monitor";
            this.tabPageMonitor.UseVisualStyleBackColor = true;
            // 
            // dgvMonitorMainList
            // 
            this.dgvMonitorMainList.AllowUserToAddRows = false;
            this.dgvMonitorMainList.AllowUserToDeleteRows = false;
            this.dgvMonitorMainList.AllowUserToResizeRows = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.White;
            this.dgvMonitorMainList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMonitorMainList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.dgvMonitorMainList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMonitorMainList.DefaultCellStyle = dataGridViewCellStyle27;
            this.dgvMonitorMainList.Location = new System.Drawing.Point(6, 7);
            this.dgvMonitorMainList.Name = "dgvMonitorMainList";
            this.dgvMonitorMainList.ReadOnly = true;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMonitorMainList.RowHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgvMonitorMainList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonitorMainList.Size = new System.Drawing.Size(1430, 600);
            this.dgvMonitorMainList.TabIndex = 1;
            this.dgvMonitorMainList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMonitorMainList_CellMouseDoubleClick);
            // 
            // tabPageRRU
            // 
            this.tabPageRRU.Controls.Add(this.dgvRRU_MainList);
            this.tabPageRRU.Location = new System.Drawing.Point(4, 22);
            this.tabPageRRU.Name = "tabPageRRU";
            this.tabPageRRU.Size = new System.Drawing.Size(1428, 593);
            this.tabPageRRU.TabIndex = 2;
            this.tabPageRRU.Text = "RRU";
            this.tabPageRRU.UseVisualStyleBackColor = true;
            // 
            // dgvRRU_MainList
            // 
            this.dgvRRU_MainList.AllowUserToAddRows = false;
            this.dgvRRU_MainList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.White;
            this.dgvRRU_MainList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle29;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRRU_MainList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvRRU_MainList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRRU_MainList.DefaultCellStyle = dataGridViewCellStyle31;
            this.dgvRRU_MainList.Location = new System.Drawing.Point(6, 7);
            this.dgvRRU_MainList.Name = "dgvRRU_MainList";
            this.dgvRRU_MainList.ReadOnly = true;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRRU_MainList.RowHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.dgvRRU_MainList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRRU_MainList.Size = new System.Drawing.Size(1430, 600);
            this.dgvRRU_MainList.TabIndex = 1;
            this.dgvRRU_MainList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRRU_MainList_CellMouseDoubleClick);
            // 
            // tabPageMSP
            // 
            this.tabPageMSP.Controls.Add(this.dgvMSP_MainList);
            this.tabPageMSP.Location = new System.Drawing.Point(4, 22);
            this.tabPageMSP.Name = "tabPageMSP";
            this.tabPageMSP.Size = new System.Drawing.Size(1428, 593);
            this.tabPageMSP.TabIndex = 3;
            this.tabPageMSP.Text = "MSP";
            this.tabPageMSP.UseVisualStyleBackColor = true;
            // 
            // dgvMSP_MainList
            // 
            this.dgvMSP_MainList.AllowUserToAddRows = false;
            this.dgvMSP_MainList.AllowUserToDeleteRows = false;
            this.dgvMSP_MainList.AllowUserToResizeRows = false;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.White;
            this.dgvMSP_MainList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle33;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMSP_MainList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dgvMSP_MainList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMSP_MainList.DefaultCellStyle = dataGridViewCellStyle35;
            this.dgvMSP_MainList.Location = new System.Drawing.Point(6, 7);
            this.dgvMSP_MainList.Name = "dgvMSP_MainList";
            this.dgvMSP_MainList.ReadOnly = true;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMSP_MainList.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.dgvMSP_MainList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMSP_MainList.Size = new System.Drawing.Size(1430, 600);
            this.dgvMSP_MainList.TabIndex = 1;
            this.dgvMSP_MainList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMSP_MainList_CellMouseDoubleClick);
            // 
            // tabPageViking
            // 
            this.tabPageViking.Controls.Add(this.dgvViking_MainList);
            this.tabPageViking.Location = new System.Drawing.Point(4, 22);
            this.tabPageViking.Name = "tabPageViking";
            this.tabPageViking.Size = new System.Drawing.Size(1428, 593);
            this.tabPageViking.TabIndex = 4;
            this.tabPageViking.Text = "Mixed Air";
            this.tabPageViking.UseVisualStyleBackColor = true;
            // 
            // dgvViking_MainList
            // 
            this.dgvViking_MainList.AllowUserToAddRows = false;
            this.dgvViking_MainList.AllowUserToDeleteRows = false;
            this.dgvViking_MainList.AllowUserToResizeRows = false;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.White;
            this.dgvViking_MainList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle37;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvViking_MainList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.dgvViking_MainList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvViking_MainList.DefaultCellStyle = dataGridViewCellStyle39;
            this.dgvViking_MainList.Location = new System.Drawing.Point(7, 8);
            this.dgvViking_MainList.Name = "dgvViking_MainList";
            this.dgvViking_MainList.ReadOnly = true;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvViking_MainList.RowHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.dgvViking_MainList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvViking_MainList.Size = new System.Drawing.Size(1430, 600);
            this.dgvViking_MainList.TabIndex = 1;
            this.dgvViking_MainList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvViking_MainList_CellMouseDoubleClick);
            this.dgvViking_MainList.DoubleClick += new System.EventHandler(this.dgvViking_DoubleClick);
            // 
            // lbMainTitle
            // 
            this.lbMainTitle.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitle.Location = new System.Drawing.Point(489, 54);
            this.lbMainTitle.Name = "lbMainTitle";
            this.lbMainTitle.Size = new System.Drawing.Size(417, 25);
            this.lbMainTitle.TabIndex = 25;
            this.lbMainTitle.Text = "Title";
            this.lbMainTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Engravers MT", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.Location = new System.Drawing.Point(258, 22);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(897, 35);
            this.lbEnvironment.TabIndex = 26;
            this.lbEnvironment.Text = "Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mixedAirPartMaintToolToolStripMenuItem
            // 
            this.mixedAirPartMaintToolToolStripMenuItem.Name = "mixedAirPartMaintToolToolStripMenuItem";
            this.mixedAirPartMaintToolToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.mixedAirPartMaintToolToolStripMenuItem.Text = "MixedAir Part Maint Tool";
            this.mixedAirPartMaintToolToolStripMenuItem.Click += new System.EventHandler(this.mixedAirPartMaintToolToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1467, 749);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.lbMainTitle);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.lbMainMsg);
            this.Controls.Add(this.btnMainReset);
            this.Controls.Add(this.rbMainOpenJobs);
            this.Controls.Add(this.rbMainClosedJobs);
            this.Controls.Add(this.rbMainAllJobs);
            this.Controls.Add(this.txtMainSearchJobNum);
            this.Controls.Add(this.lbMainStartAt);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.msMainMenu);
            this.ForeColor = System.Drawing.Color.Blue;
            this.MainMenuStrip = this.msMainMenu;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "TOAU Main Screen Rev6";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.msMainMenu.ResumeLayout(false);
            this.msMainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainUnitList)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tabPageOA_Units.ResumeLayout(false);
            this.gbPartSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartSearch)).EndInit();
            this.tabPageMonitor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitorMainList)).EndInit();
            this.tabPageRRU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRRU_MainList)).EndInit();
            this.tabPageMSP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMSP_MainList)).EndInit();
            this.tabPageViking.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvViking_MainList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.MenuStrip msMainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Button btnMainReset;
        private System.Windows.Forms.RadioButton rbMainOpenJobs;
        private System.Windows.Forms.RadioButton rbMainClosedJobs;
        private System.Windows.Forms.RadioButton rbMainAllJobs;
        public System.Windows.Forms.TextBox txtMainSearchJobNum;
        private System.Windows.Forms.Label lbMainStartAt;
        private System.Windows.Forms.Label lbMainMsg;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem criticalComponentsPurchasePartsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partsByJobNumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobsByDateRangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eTLLabelsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiPurchasePartsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partsByJobNumberToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem oAUBOMPicklistToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sheetMetalForecastingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sheetMetalByJobNumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sheetMetalByDateRangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partDemandByDateRangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oAModelNoConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oARev6ModelNoConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobGoodToGoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removePartsOrderedFlagToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem picklistByDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tandemCompressorJobsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tandemCompressorByJobNumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobHistoryByJobNumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monitorModelNoConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineTransferJobToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineTransferToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scheduleJobToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partMaintenanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rev5PartMainToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rev6PartMaintToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scheduleExtractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sheetMetalSuperMarketPicklistToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refrigerationComponentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sheetMetalBendByJobNumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subAssemblyStationJobDataToolStripMenuItem;
        private System.Windows.Forms.DataGridView dgvMainUnitList;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPageOA_Units;
        private System.Windows.Forms.TabPage tabPageMonitor;
        private System.Windows.Forms.TabPage tabPageRRU;
        private System.Windows.Forms.TabPage tabPageMSP;
        private System.Windows.Forms.DataGridView dgvMonitorMainList;
        private System.Windows.Forms.DataGridView dgvRRU_MainList;
        private System.Windows.Forms.DataGridView dgvMSP_MainList;
        public System.Windows.Forms.Label lbMainTitle;
        private System.Windows.Forms.ToolStripMenuItem sheetMetalAllocationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateJobQtysByReleaseToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPageViking;
        private System.Windows.Forms.DataGridView dgvViking_MainList;
        private System.Windows.Forms.ToolStripMenuItem monitorPartMaintToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mSPPartMaintToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mSPModelNoConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partAssemblyBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newPicklistByStationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vikingModelNoConfigurationToolStripMenuItem;
        public System.Windows.Forms.Label lbEnvironment;
        private System.Windows.Forms.ToolStripMenuItem vMEAndRFGASMPartsByJobNumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem batchConfigureJobsToolStripMenuItem1;       
        private System.Windows.Forms.ToolStripMenuItem swapPartsOnJobToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findPartsOnJobsToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbPartSearch;
        private System.Windows.Forms.DataGridView dgvPartSearch;
        public System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartNum;
        public System.Windows.Forms.Button btnExitSearch;
        private System.Windows.Forms.ToolStripMenuItem mixedAirPartMaintToolToolStripMenuItem;
    }
}

