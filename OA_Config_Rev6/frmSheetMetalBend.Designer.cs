﻿namespace OA_Config_Rev6
{
    partial class frmSheetMetalBend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dtpRunDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtJobNums = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbCabType = new System.Windows.Forms.ComboBox();
            this.chkInsertSMR_Table = new System.Windows.Forms.CheckBox();
            this.radBtnLaser = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.radBtnS4 = new System.Windows.Forms.RadioButton();
            this.radBtnTurret = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(12, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select Date of Run";
            // 
            // dtpRunDate
            // 
            this.dtpRunDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpRunDate.Location = new System.Drawing.Point(15, 77);
            this.dtpRunDate.Name = "dtpRunDate";
            this.dtpRunDate.Size = new System.Drawing.Size(113, 20);
            this.dtpRunDate.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(12, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(313, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Input Job Numbers separated by a comma for this run.";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.Green;
            this.btnSubmit.Location = new System.Drawing.Point(147, 179);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(241, 179);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtJobNums
            // 
            this.txtJobNums.Location = new System.Drawing.Point(15, 130);
            this.txtJobNums.Name = "txtJobNums";
            this.txtJobNums.Size = new System.Drawing.Size(456, 20);
            this.txtJobNums.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Select Cabinet Type";
            // 
            // cbCabType
            // 
            this.cbCabType.FormattingEnabled = true;
            this.cbCabType.Items.AddRange(new object[] {
            "",
            "MON",
            "OAB",
            "OAD Rev5",
            "OAD Rev6",
            "OAG",
            "OAK",
            "OAN Rev5",
            "OAN Rev6"});
            this.cbCabType.Location = new System.Drawing.Point(15, 26);
            this.cbCabType.Name = "cbCabType";
            this.cbCabType.Size = new System.Drawing.Size(113, 21);
            this.cbCabType.TabIndex = 9;
            // 
            // chkInsertSMR_Table
            // 
            this.chkInsertSMR_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInsertSMR_Table.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.chkInsertSMR_Table.Location = new System.Drawing.Point(192, 77);
            this.chkInsertSMR_Table.Name = "chkInsertSMR_Table";
            this.chkInsertSMR_Table.Size = new System.Drawing.Size(279, 24);
            this.chkInsertSMR_Table.TabIndex = 10;
            this.chkInsertSMR_Table.Text = "Make Entry  in Sheet Metal Release Table";
            this.chkInsertSMR_Table.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkInsertSMR_Table.UseVisualStyleBackColor = true;
            // 
            // radBtnLaser
            // 
            this.radBtnLaser.AutoSize = true;
            this.radBtnLaser.Checked = true;
            this.radBtnLaser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBtnLaser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.radBtnLaser.Location = new System.Drawing.Point(192, 29);
            this.radBtnLaser.Name = "radBtnLaser";
            this.radBtnLaser.Size = new System.Drawing.Size(56, 17);
            this.radBtnLaser.TabIndex = 11;
            this.radBtnLaser.TabStop = true;
            this.radBtnLaser.Text = "Laser";
            this.radBtnLaser.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(189, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Cut Method";
            // 
            // radBtnS4
            // 
            this.radBtnS4.AutoSize = true;
            this.radBtnS4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBtnS4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.radBtnS4.Location = new System.Drawing.Point(301, 30);
            this.radBtnS4.Name = "radBtnS4";
            this.radBtnS4.Size = new System.Drawing.Size(40, 17);
            this.radBtnS4.TabIndex = 13;
            this.radBtnS4.TabStop = true;
            this.radBtnS4.Text = "S4";
            this.radBtnS4.UseVisualStyleBackColor = true;
            // 
            // radBtnTurret
            // 
            this.radBtnTurret.AutoSize = true;
            this.radBtnTurret.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBtnTurret.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.radBtnTurret.Location = new System.Drawing.Point(394, 30);
            this.radBtnTurret.Name = "radBtnTurret";
            this.radBtnTurret.Size = new System.Drawing.Size(59, 17);
            this.radBtnTurret.TabIndex = 14;
            this.radBtnTurret.TabStop = true;
            this.radBtnTurret.Text = "Turret";
            this.radBtnTurret.UseVisualStyleBackColor = true;
            // 
            // frmSheetMetalBend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 234);
            this.Controls.Add(this.radBtnTurret);
            this.Controls.Add(this.radBtnS4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.radBtnLaser);
            this.Controls.Add(this.chkInsertSMR_Table);
            this.Controls.Add(this.cbCabType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtJobNums);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpRunDate);
            this.Name = "frmSheetMetalBend";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSheetMetalBend";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpRunDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtJobNums;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbCabType;
        private System.Windows.Forms.CheckBox chkInsertSMR_Table;
        private System.Windows.Forms.RadioButton radBtnLaser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radBtnS4;
        private System.Windows.Forms.RadioButton radBtnTurret;
    }
}