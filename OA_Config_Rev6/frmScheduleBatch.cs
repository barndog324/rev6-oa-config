﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace OA_Config_Rev6
{
    public partial class frmScheduleBatch : Form
    {
        MainBO objMain = new MainBO();

        public frmScheduleBatch()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string endDateStr = "";
            string startHourStr = "";
            string jobNumStr = "";    
            string startDateStr = "";
            string reqDueDateStr = "";
            string dueDateStr = "1/1/1000";
            string dueHourStr = "";
            string dueTimeStr = "";
            string errorMsg = "";
            string prodHrsStr = "";

            int sepPos = -1;

            bool scheduleJobs = true;            

            double addProdHrs = 0;
            double prodHrsDbl = 0;

            DateTime calcEndDate = DateTime.Now;

            foreach(DataGridViewRow row in dgvJobList.Rows)
            {
                jobNumStr = row.Cells["JobNum"].Value.ToString();
                startDateStr = row.Cells["StartDate"].Value.ToString();
                startHourStr = row.Cells["StartHour"].Value.ToString();                
                reqDueDateStr = row.Cells["ReqDueDate"].Value.ToString();
                prodHrsStr = row.Cells["ProdHrs"].Value.ToString();

                try
                {
                    var startDate = Convert.ToDateTime(startDateStr);

                    if (startDate < DateTime.Today)
                    {
                        scheduleJobs = false;
                        MessageBox.Show("ERROR - Job Num -> " + jobNumStr + " has an Start Date value less than Today! Please fix and re-submit.");
                        break;
                    }
                }
                catch
                {
                    scheduleJobs = false;
                    MessageBox.Show("ERROR - Job Num -> " + jobNumStr + " has an invalid Start Date value! Please fix and re-submit.");
                    break;
                }

                try
                {
                    var startHour = Convert.ToDateTime(startHourStr);
                    int iStartHr = Int32.Parse(startHourStr.Substring(0, startHourStr.IndexOf(":")));

                    if (iStartHr < 6)
                    {
                        scheduleJobs = false;
                        MessageBox.Show("ERROR - Job Num -> " + jobNumStr + " has a Start Hour value before the shift starts at 6am! Please fix and re-submit.");
                        break;
                    }
                    else if (iStartHr > 14)
                    {
                        scheduleJobs = false;
                        MessageBox.Show("ERROR - Job Num -> " + jobNumStr + " has a Start Hour value after the shift ends at 3pm! Please fix and re-submit.");
                        break;
                    }
                }
                catch
                {
                    scheduleJobs = false;
                    MessageBox.Show("ERROR - Job Num -> " + jobNumStr + " has an invalid End Hour value! Please fix and re-submit.");
                    break;
                }

                try
                {
                    endDateStr = CalcDueDateAndTime(DateTime.Parse(startDateStr), startHourStr, prodHrsStr, out addProdHrs);
                    prodHrsDbl = double.Parse(prodHrsStr) + addProdHrs;
                    prodHrsStr = prodHrsDbl.ToString("N2");

                    startHourStr = ConvertTimeToDecimal(startHourStr);
                    sepPos = endDateStr.IndexOf("=");
                    if (sepPos > 0)
                    {
                        dueDateStr = endDateStr.Substring(0, sepPos);
                        dueHourStr = endDateStr.Substring((sepPos+1), 5);
                        dueTimeStr = ConvertTimeToDecimal(dueHourStr);                                                

                        if (DateTime.Parse(dueDateStr) > DateTime.Parse(reqDueDateStr))
                        {
                            DialogResult result1 = MessageBox.Show("WARNING - Job# " + jobNumStr + " has an Estimated Competion Date greater than the Required Due Date!  " +
                                                                  "Would you like to continue scheduling this job? Press 'Yes' to continue or 'No' to stop.",
                                                                  "WARNING WARNING WARNING",
                                                                  MessageBoxButtons.YesNo,
                                                                  MessageBoxIcon.Exclamation);

                            if (result1 == DialogResult.No)
                            {
                                scheduleJobs = false;
                                break;
                            }                           
                        }
                       
                        row.Cells["ProdHrs"].Value = prodHrsStr;
                        row.Cells["EndDate"].Value = dueDateStr;
                        row.Cells["EndHour"].Value = dueTimeStr;                       
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR - Job Num -> " + jobNumStr + " has an invalid End Hour value! Please fix and re-submit.");
                    break;
                }
            }

            if (scheduleJobs == true)
            {
                foreach (DataGridViewRow dgvr in dgvJobList.Rows)
                {
                    jobNumStr = dgvr.Cells["JobNum"].Value.ToString();
                    startDateStr = dgvr.Cells["StartDate"].Value.ToString();
                    startHourStr = dgvr.Cells["StartHour"].Value.ToString();
                    reqDueDateStr = dgvr.Cells["ReqDueDate"].Value.ToString();
                    prodHrsStr = dgvr.Cells["ProdHrs"].Value.ToString();
                    dueDateStr = dgvr.Cells["EndDate"].Value.ToString();
                    dueHourStr = dgvr.Cells["EndHour"].Value.ToString();

                    string epicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
                    string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;
                    Console.WriteLine(epicorServer);

                    ScheduleJob sj = new ScheduleJob(epicorServer, epicorSqlConnectionString);

                    lbMsg.Text = "Job# " + jobNumStr + " is currently being scheduled.";
                    this.Refresh();
                    errorMsg = sj.ScheduleJobByDateAndTime(jobNumStr, startDateStr, startHourStr, dueDateStr, dueHourStr, prodHrsStr);
                    dgvr.Cells["SchedulingResult"].Value = errorMsg;
                    //dgvr.Cells["SchedulingResult"].Value = "Testing email";
                }

                lbMsg.Text = "Batch job scheduling has completed. Please view the result column on this page to verify each job was successfully scheduled.";

                using (StreamWriter sw = new StreamWriter(@"\\EPICOR905APP\Apps\OAU\Rev6_OAU_Config\BatchJobScheduleResults.csv"))
                //using (StreamWriter sw = new StreamWriter(@"C:\Users\tonyt\Documents\BatchJobScheduleResults.csv"))
                {
                    sw.WriteLine("JobNum," + "StartDate," + "StartHour," + "ReqDueDate," + "EndDate," + "EndHour," + "Result");

                    foreach (DataGridViewRow dgvr in dgvJobList.Rows)
                    {
                        sw.WriteLine(dgvr.Cells["JobNum"].Value.ToString() + "," + dgvr.Cells["StartDate"].Value.ToString() + "," + dgvr.Cells["StartHour"].Value.ToString() + "," +
                                     dgvr.Cells["ReqDueDate"].Value.ToString() + "," + dgvr.Cells["EndDate"].Value.ToString() + "," + dgvr.Cells["EndHour"].Value.ToString() + "," +
                                     dgvr.Cells["SchedulingResult"].Value.ToString());
                    }
                }

                // Send email to the scheduling team.
                MailMessage mail = new MailMessage();
                mail.To.Add("jschroering@KCCMfg.com");
                mail.To.Add("lwolz@KCCMfg.com");
                mail.To.Add("kwiseman@KCCMfg.com");
                mail.To.Add("dhernandez@KCCMfg.com");
                //mail.To.Add("tthoman@KCCMfg.com");                

                mail.Subject = "Batch Job Schedule - " + DateTime.Now.ToShortDateString();

                mail.Body = "See Attached";

                System.Net.Mail.Attachment attachment;

                attachment = new System.Net.Mail.Attachment(@"\\EPICOR905APP\Apps\OAU\Rev6_OAU_Config\BatchJobScheduleResults.csv");
                //attachment = new System.Net.Mail.Attachment(@"C:\Users\tonyt\Documents\BatchJobScheduleResults.csv");
                mail.Attachments.Add(attachment);
                mail.IsBodyHtml = false;
                
                SmtpClient smtp = new SmtpClient();
                //smtp.Host = "smtp.gmail.com";
                //smtp.Port = 587;

                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("username@domain.com", "password");
                //smtp.EnableSsl = true;              

                smtp.Send(mail);
                this.Refresh();
            }                
            
        }

        private string CalcDueDateAndTime(DateTime startDate, string startTime, string prodHrs, out double addProdHrs)
        {
            string dueDateTime = "";
            string startHr = "";
            string startMin = "";
            string endTimeStr = "";
            string prodHrsStr = prodHrs.Substring(0, prodHrs.IndexOf("."));

            int pos = -1;
            int iProdHrs = int.Parse(prodHrsStr);
            int iProdDays = iProdHrs / 8;
            int endHour = 0;
            int addDays = 0;

            decimal dProdHrs = decimal.Parse(prodHrsStr);
            decimal dProdDays = dProdHrs / 8;

            DateTime endDate;

            var addHrs = iProdHrs % 8;

            addProdHrs = 0;

            pos = startTime.IndexOf(":");
            if (pos > 0)
            {
                startHr = startTime.Substring(0, pos);
                startMin = startTime.Substring((pos + 1), 2);
            }

            endDate = startDate.AddDays(iProdDays);
            endHour = Int32.Parse(startHr);
            endHour += (int)addHrs;

            if (endHour > 14) // If the end hour is greater than 2pm the add a day
            {
                endDate = endDate.AddDays(1);
                endHour = 5 + (endHour - 14);  // Add any hrs past 2pm to the Start of Day time of 05:00
            }
          
            if (endHour < 10)
            {
                endTimeStr = "0" + endHour.ToString() + ":" + startMin;
            }
            else
            {
                endTimeStr = endHour.ToString() + ":" + startMin;
            }

            addDays = EndDateCrossesWeekendOrHoliday(startDate, endDate);
            addProdHrs = addProdHrs + (addDays * 8);
            //if (EndDateCrossesWeekendOrHoliday(startDate, endDate) == tru)
            //{
            //    endDate = endDate.AddDays(2);
            //    addProdHrs = 16;                
            //}

            endDate = endDate.AddDays(addDays);
            dueDateTime = endDate.ToShortDateString() + "=" + endTimeStr;
            return dueDateTime;
        }

        private int EndDateCrossesWeekendOrHoliday(DateTime startDate, DateTime endDate)
        {
            int addDays = 0;           

            while (startDate < endDate)
            {
                startDate = startDate.AddDays(1);
                if (startDate.DayOfWeek == DayOfWeek.Saturday || startDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    ++addDays;
                    endDate = endDate.AddDays(1);
                }
                else if (objMain.GetProdCalDay(startDate) == true)
                {
                    ++addDays;
                    endDate = endDate.AddDays(1);
                }
            }

            return addDays;
        }

        private string ConvertTimeToDecimal(string inTime)
        {            
            string tempHr = "";
            string tempMin = "";
            string returnTimeStr = "00:00";

            int pos = -1;                       

            pos = inTime.IndexOf(":");

            if (pos > 0)
            {
                tempHr = inTime.Substring(0, pos);
                tempMin = inTime.Substring((pos + 1), 2);

                tempMin = (decimal.Parse(tempMin) / 60).ToString();
                returnTimeStr = (decimal.Parse(tempHr) + decimal.Parse(tempMin)).ToString();
            }

            return returnTimeStr;
        }
    }
}
