﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace OA_Config_Rev6
{
    static class GlobalJob
    {
        public static string JobNumber { get; set; }
        public static string ModelNo { get; set; }
        public static string ProgramMode {get; set;}
        public static string DisplayMode { get; set; }
        public static string EnvironmentStr { get; set; }
        public static Color EnvColor { get; set; }
    }
}
