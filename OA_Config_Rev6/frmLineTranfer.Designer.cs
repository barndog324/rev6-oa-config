﻿namespace OA_Config_Rev6
{
    partial class frmLineTranfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label25 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.lbCustName = new System.Windows.Forms.Label();
            this.gbFromLine = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbTechFromLineD = new System.Windows.Forms.RadioButton();
            this.rbTechFromLineC = new System.Windows.Forms.RadioButton();
            this.rbTechFromLineB = new System.Windows.Forms.RadioButton();
            this.rbTechFromLineA = new System.Windows.Forms.RadioButton();
            this.rbGrassFromLine4 = new System.Windows.Forms.RadioButton();
            this.rbGrassFromLine3 = new System.Windows.Forms.RadioButton();
            this.rbGrassFromLine2 = new System.Windows.Forms.RadioButton();
            this.rbGrassFromLine1 = new System.Windows.Forms.RadioButton();
            this.gbToLine = new System.Windows.Forms.GroupBox();
            this.rbTechToLineE = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rbTechToLineD = new System.Windows.Forms.RadioButton();
            this.rbTechToLineC = new System.Windows.Forms.RadioButton();
            this.rbTechToLineB = new System.Windows.Forms.RadioButton();
            this.rbTechToLineA = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine4 = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine3 = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine2 = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine1 = new System.Windows.Forms.RadioButton();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbMessage = new System.Windows.Forms.Label();
            this.gbFromLine.SuspendLayout();
            this.gbToLine.SuspendLayout();
            this.SuspendLayout();
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(164, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(125, 20);
            this.label25.TabIndex = 271;
            this.label25.Text = "Job #:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Black;
            this.lbJobNum.Location = new System.Drawing.Point(292, 9);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(116, 20);
            this.lbJobNum.TabIndex = 270;
            this.lbJobNum.Text = "??????-??-??";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Black;
            this.lbCustName.Location = new System.Drawing.Point(62, 37);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(441, 20);
            this.lbCustName.TabIndex = 272;
            this.lbCustName.Text = "Customer Name";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbFromLine
            // 
            this.gbFromLine.Controls.Add(this.label2);
            this.gbFromLine.Controls.Add(this.label1);
            this.gbFromLine.Controls.Add(this.rbTechFromLineD);
            this.gbFromLine.Controls.Add(this.rbTechFromLineC);
            this.gbFromLine.Controls.Add(this.rbTechFromLineB);
            this.gbFromLine.Controls.Add(this.rbTechFromLineA);
            this.gbFromLine.Controls.Add(this.rbGrassFromLine4);
            this.gbFromLine.Controls.Add(this.rbGrassFromLine3);
            this.gbFromLine.Controls.Add(this.rbGrassFromLine2);
            this.gbFromLine.Controls.Add(this.rbGrassFromLine1);
            this.gbFromLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbFromLine.ForeColor = System.Drawing.Color.Blue;
            this.gbFromLine.Location = new System.Drawing.Point(70, 114);
            this.gbFromLine.Name = "gbFromLine";
            this.gbFromLine.Size = new System.Drawing.Size(130, 300);
            this.gbFromLine.TabIndex = 274;
            this.gbFromLine.TabStop = false;
            this.gbFromLine.Text = "From Line";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Green;
            this.label2.Location = new System.Drawing.Point(28, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Technology";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(28, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Grassland";
            // 
            // rbTechFromLineD
            // 
            this.rbTechFromLineD.AutoSize = true;
            this.rbTechFromLineD.Enabled = false;
            this.rbTechFromLineD.Location = new System.Drawing.Point(31, 255);
            this.rbTechFromLineD.Name = "rbTechFromLineD";
            this.rbTechFromLineD.Size = new System.Drawing.Size(62, 17);
            this.rbTechFromLineD.TabIndex = 7;
            this.rbTechFromLineD.TabStop = true;
            this.rbTechFromLineD.Text = "Line D";
            this.rbTechFromLineD.UseVisualStyleBackColor = true;
            this.rbTechFromLineD.Visible = false;
            // 
            // rbTechFromLineC
            // 
            this.rbTechFromLineC.AutoSize = true;
            this.rbTechFromLineC.Location = new System.Drawing.Point(32, 230);
            this.rbTechFromLineC.Name = "rbTechFromLineC";
            this.rbTechFromLineC.Size = new System.Drawing.Size(61, 17);
            this.rbTechFromLineC.TabIndex = 6;
            this.rbTechFromLineC.TabStop = true;
            this.rbTechFromLineC.Text = "Line C";
            this.rbTechFromLineC.UseVisualStyleBackColor = true;
            // 
            // rbTechFromLineB
            // 
            this.rbTechFromLineB.AutoSize = true;
            this.rbTechFromLineB.Location = new System.Drawing.Point(32, 205);
            this.rbTechFromLineB.Name = "rbTechFromLineB";
            this.rbTechFromLineB.Size = new System.Drawing.Size(61, 17);
            this.rbTechFromLineB.TabIndex = 5;
            this.rbTechFromLineB.TabStop = true;
            this.rbTechFromLineB.Text = "Line B";
            this.rbTechFromLineB.UseVisualStyleBackColor = true;
            // 
            // rbTechFromLineA
            // 
            this.rbTechFromLineA.AutoSize = true;
            this.rbTechFromLineA.Location = new System.Drawing.Point(31, 180);
            this.rbTechFromLineA.Name = "rbTechFromLineA";
            this.rbTechFromLineA.Size = new System.Drawing.Size(61, 17);
            this.rbTechFromLineA.TabIndex = 4;
            this.rbTechFromLineA.TabStop = true;
            this.rbTechFromLineA.Text = "Line A";
            this.rbTechFromLineA.UseVisualStyleBackColor = true;
            // 
            // rbGrassFromLine4
            // 
            this.rbGrassFromLine4.AutoSize = true;
            this.rbGrassFromLine4.Location = new System.Drawing.Point(30, 125);
            this.rbGrassFromLine4.Name = "rbGrassFromLine4";
            this.rbGrassFromLine4.Size = new System.Drawing.Size(60, 17);
            this.rbGrassFromLine4.TabIndex = 3;
            this.rbGrassFromLine4.TabStop = true;
            this.rbGrassFromLine4.Text = "Line 4";
            this.rbGrassFromLine4.UseVisualStyleBackColor = true;
            // 
            // rbGrassFromLine3
            // 
            this.rbGrassFromLine3.AutoSize = true;
            this.rbGrassFromLine3.Location = new System.Drawing.Point(30, 100);
            this.rbGrassFromLine3.Name = "rbGrassFromLine3";
            this.rbGrassFromLine3.Size = new System.Drawing.Size(60, 17);
            this.rbGrassFromLine3.TabIndex = 2;
            this.rbGrassFromLine3.TabStop = true;
            this.rbGrassFromLine3.Text = "Line 3";
            this.rbGrassFromLine3.UseVisualStyleBackColor = true;
            // 
            // rbGrassFromLine2
            // 
            this.rbGrassFromLine2.AutoSize = true;
            this.rbGrassFromLine2.Location = new System.Drawing.Point(30, 75);
            this.rbGrassFromLine2.Name = "rbGrassFromLine2";
            this.rbGrassFromLine2.Size = new System.Drawing.Size(60, 17);
            this.rbGrassFromLine2.TabIndex = 1;
            this.rbGrassFromLine2.TabStop = true;
            this.rbGrassFromLine2.Text = "Line 2";
            this.rbGrassFromLine2.UseVisualStyleBackColor = true;
            // 
            // rbGrassFromLine1
            // 
            this.rbGrassFromLine1.AutoSize = true;
            this.rbGrassFromLine1.Location = new System.Drawing.Point(30, 50);
            this.rbGrassFromLine1.Name = "rbGrassFromLine1";
            this.rbGrassFromLine1.Size = new System.Drawing.Size(60, 17);
            this.rbGrassFromLine1.TabIndex = 0;
            this.rbGrassFromLine1.TabStop = true;
            this.rbGrassFromLine1.Text = "Line 1";
            this.rbGrassFromLine1.UseVisualStyleBackColor = true;
            // 
            // gbToLine
            // 
            this.gbToLine.Controls.Add(this.rbTechToLineE);
            this.gbToLine.Controls.Add(this.label3);
            this.gbToLine.Controls.Add(this.label4);
            this.gbToLine.Controls.Add(this.rbTechToLineD);
            this.gbToLine.Controls.Add(this.rbTechToLineC);
            this.gbToLine.Controls.Add(this.rbTechToLineB);
            this.gbToLine.Controls.Add(this.rbTechToLineA);
            this.gbToLine.Controls.Add(this.rbGrassToLine4);
            this.gbToLine.Controls.Add(this.rbGrassToLine3);
            this.gbToLine.Controls.Add(this.rbGrassToLine2);
            this.gbToLine.Controls.Add(this.rbGrassToLine1);
            this.gbToLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbToLine.ForeColor = System.Drawing.Color.Blue;
            this.gbToLine.Location = new System.Drawing.Point(363, 114);
            this.gbToLine.Name = "gbToLine";
            this.gbToLine.Size = new System.Drawing.Size(130, 300);
            this.gbToLine.TabIndex = 275;
            this.gbToLine.TabStop = false;
            this.gbToLine.Text = "To Line";
            // 
            // rbTechToLineE
            // 
            this.rbTechToLineE.AutoSize = true;
            this.rbTechToLineE.Enabled = false;
            this.rbTechToLineE.Location = new System.Drawing.Point(32, 277);
            this.rbTechToLineE.Name = "rbTechToLineE";
            this.rbTechToLineE.Size = new System.Drawing.Size(61, 17);
            this.rbTechToLineE.TabIndex = 20;
            this.rbTechToLineE.TabStop = true;
            this.rbTechToLineE.Text = "Line E";
            this.rbTechToLineE.UseVisualStyleBackColor = true;
            this.rbTechToLineE.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Green;
            this.label3.Location = new System.Drawing.Point(29, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Technology";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(29, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Grassland";
            // 
            // rbTechToLineD
            // 
            this.rbTechToLineD.AutoSize = true;
            this.rbTechToLineD.Enabled = false;
            this.rbTechToLineD.Location = new System.Drawing.Point(32, 254);
            this.rbTechToLineD.Name = "rbTechToLineD";
            this.rbTechToLineD.Size = new System.Drawing.Size(62, 17);
            this.rbTechToLineD.TabIndex = 17;
            this.rbTechToLineD.TabStop = true;
            this.rbTechToLineD.Text = "Line D";
            this.rbTechToLineD.UseVisualStyleBackColor = true;
            this.rbTechToLineD.Visible = false;
            this.rbTechToLineD.CheckedChanged += new System.EventHandler(this.rbTechToLineD_CheckedChanged);
            // 
            // rbTechToLineC
            // 
            this.rbTechToLineC.AutoSize = true;
            this.rbTechToLineC.Location = new System.Drawing.Point(33, 229);
            this.rbTechToLineC.Name = "rbTechToLineC";
            this.rbTechToLineC.Size = new System.Drawing.Size(61, 17);
            this.rbTechToLineC.TabIndex = 16;
            this.rbTechToLineC.TabStop = true;
            this.rbTechToLineC.Text = "Line C";
            this.rbTechToLineC.UseVisualStyleBackColor = true;
            this.rbTechToLineC.CheckedChanged += new System.EventHandler(this.rbTechToLineC_CheckedChanged);
            // 
            // rbTechToLineB
            // 
            this.rbTechToLineB.AutoSize = true;
            this.rbTechToLineB.Location = new System.Drawing.Point(33, 204);
            this.rbTechToLineB.Name = "rbTechToLineB";
            this.rbTechToLineB.Size = new System.Drawing.Size(61, 17);
            this.rbTechToLineB.TabIndex = 15;
            this.rbTechToLineB.TabStop = true;
            this.rbTechToLineB.Text = "Line B";
            this.rbTechToLineB.UseVisualStyleBackColor = true;
            this.rbTechToLineB.CheckedChanged += new System.EventHandler(this.rbTechToLineB_CheckedChanged);
            // 
            // rbTechToLineA
            // 
            this.rbTechToLineA.AutoSize = true;
            this.rbTechToLineA.Location = new System.Drawing.Point(32, 179);
            this.rbTechToLineA.Name = "rbTechToLineA";
            this.rbTechToLineA.Size = new System.Drawing.Size(61, 17);
            this.rbTechToLineA.TabIndex = 14;
            this.rbTechToLineA.TabStop = true;
            this.rbTechToLineA.Text = "Line A";
            this.rbTechToLineA.UseVisualStyleBackColor = true;
            this.rbTechToLineA.CheckedChanged += new System.EventHandler(this.rbTechToLineA_CheckedChanged);
            // 
            // rbGrassToLine4
            // 
            this.rbGrassToLine4.AutoSize = true;
            this.rbGrassToLine4.Location = new System.Drawing.Point(31, 124);
            this.rbGrassToLine4.Name = "rbGrassToLine4";
            this.rbGrassToLine4.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine4.TabIndex = 13;
            this.rbGrassToLine4.TabStop = true;
            this.rbGrassToLine4.Text = "Line 4";
            this.rbGrassToLine4.UseVisualStyleBackColor = true;
            this.rbGrassToLine4.CheckedChanged += new System.EventHandler(this.rbGrassToLine4_CheckedChanged);
            // 
            // rbGrassToLine3
            // 
            this.rbGrassToLine3.AutoSize = true;
            this.rbGrassToLine3.Location = new System.Drawing.Point(31, 99);
            this.rbGrassToLine3.Name = "rbGrassToLine3";
            this.rbGrassToLine3.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine3.TabIndex = 12;
            this.rbGrassToLine3.TabStop = true;
            this.rbGrassToLine3.Text = "Line 3";
            this.rbGrassToLine3.UseVisualStyleBackColor = true;
            this.rbGrassToLine3.CheckedChanged += new System.EventHandler(this.rbGrassToLine3_CheckedChanged);
            // 
            // rbGrassToLine2
            // 
            this.rbGrassToLine2.AutoSize = true;
            this.rbGrassToLine2.Location = new System.Drawing.Point(31, 74);
            this.rbGrassToLine2.Name = "rbGrassToLine2";
            this.rbGrassToLine2.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine2.TabIndex = 11;
            this.rbGrassToLine2.TabStop = true;
            this.rbGrassToLine2.Text = "Line 2";
            this.rbGrassToLine2.UseVisualStyleBackColor = true;
            this.rbGrassToLine2.CheckedChanged += new System.EventHandler(this.rbGrassToLine2_CheckedChanged);
            // 
            // rbGrassToLine1
            // 
            this.rbGrassToLine1.AutoSize = true;
            this.rbGrassToLine1.Location = new System.Drawing.Point(31, 49);
            this.rbGrassToLine1.Name = "rbGrassToLine1";
            this.rbGrassToLine1.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine1.TabIndex = 10;
            this.rbGrassToLine1.TabStop = true;
            this.rbGrassToLine1.Text = "Line 1";
            this.rbGrassToLine1.UseVisualStyleBackColor = true;
            this.rbGrassToLine1.CheckedChanged += new System.EventHandler(this.rbGrassToLine1_CheckedChanged);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.Green;
            this.btnSubmit.Location = new System.Drawing.Point(224, 354);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(115, 25);
            this.btnSubmit.TabIndex = 277;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(224, 389);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 25);
            this.button1.TabIndex = 278;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Black;
            this.lbModelNo.Location = new System.Drawing.Point(62, 68);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(441, 20);
            this.lbModelNo.TabIndex = 279;
            this.lbModelNo.Text = "ModelNo";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMessage
            // 
            this.lbMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lbMessage.ForeColor = System.Drawing.Color.Black;
            this.lbMessage.Location = new System.Drawing.Point(-2, 444);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(564, 23);
            this.lbMessage.TabIndex = 290;
            this.lbMessage.Text = "Message Line";
            this.lbMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmLineTranfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 468);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.gbToLine);
            this.Controls.Add(this.gbFromLine);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbJobNum);
            this.Name = "frmLineTranfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Line Tranfer Job";
            this.gbFromLine.ResumeLayout(false);
            this.gbFromLine.PerformLayout();
            this.gbToLine.ResumeLayout(false);
            this.gbToLine.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.Label lbCustName;
        private System.Windows.Forms.GroupBox gbFromLine;
        private System.Windows.Forms.GroupBox gbToLine;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbTechToLineD;
        private System.Windows.Forms.RadioButton rbTechToLineC;
        private System.Windows.Forms.RadioButton rbTechToLineB;
        private System.Windows.Forms.RadioButton rbTechToLineA;
        private System.Windows.Forms.RadioButton rbGrassToLine4;
        private System.Windows.Forms.RadioButton rbGrassToLine3;
        private System.Windows.Forms.RadioButton rbGrassToLine2;
        private System.Windows.Forms.RadioButton rbGrassToLine1;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.RadioButton rbGrassFromLine1;
        public System.Windows.Forms.RadioButton rbTechFromLineD;
        public System.Windows.Forms.RadioButton rbTechFromLineC;
        public System.Windows.Forms.RadioButton rbTechFromLineB;
        public System.Windows.Forms.RadioButton rbTechFromLineA;
        public System.Windows.Forms.RadioButton rbGrassFromLine4;
        public System.Windows.Forms.RadioButton rbGrassFromLine3;
        public System.Windows.Forms.RadioButton rbGrassFromLine2;
        public System.Windows.Forms.Label lbModelNo;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.RadioButton rbTechToLineE;
    }
}