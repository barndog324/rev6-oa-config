﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmSubAssemblyStations : Form
    {
        MainBO objMain = new MainBO();

        public frmSubAssemblyStations()
        {
            InitializeComponent();
        }

        private void btnDisplayRpt_Click(object sender, EventArgs e)
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string voltageStr = "";
            string mcaStr = "";
            string mopStr = "";
            string oaRev = "REV5";
            string secModStr = "No";
            string airflowConStr = "";
            string heatTypeStr = "";
            string fuelTypeStr = "";
            string supplyFanMotorHPStr = "";
            string supplyFanMotorTypeStr = "";
            string supplyFanWheelStr = "";
            string supplyFanFLAStr = "";
            string exhaustFanMotorHPStr = "";
            string exhaustFanMotorTypeStr = "";
            string exhaustFanWheelStr = "";
            string ervWheelStr = "";
            string ervInfoStr = "";
            string unitTypeStr = "";
            string elecOptionsStr = "";
            string subStationStr = "";
            string partTypeStr = "";
            string evapFanFlaStr = "";
            string pwrExhFlaStr = "";
            string modelTypeStr = "OAU";

            string wshpAuxSecInfo = "";
            string auxBox = "";
            string secModDigit8ValuesBDGH = "B,D,G,H";
            string secModDigit20Values0ABCDEFJK = "0,A,B,C,D,E,F,J,K";
            string secModDigit8ValuesACEF = "A,C,E,F";
            string secModDigit20ValuesGHLNQSTUVWYZ = "G,H,L,N,Q,S,T,U,V,W,Y,Z";     

            int mopInt = 0;          

            bool validOA_Job = false;
            bool dualPoint = false;

            if (modelNoStr.Substring(0, 3) == "OAB" || modelNoStr.Substring(0, 3) == "OAG")
            {
                unitTypeStr = "OALBG";
            }
            else if (modelNoStr.Substring(0, 3) == "OAD" || modelNoStr.Substring(0, 3) == "OAK" || modelNoStr.Substring(0, 3) == "OAN")
            {
                unitTypeStr = "OAU123DKN";
            }
            else
            {
                modelNoStr = "Viking";
            }

            DataTable dtEtl = objMain.GetEtlData(jobNumStr);

            DataRow row = null;

            if (dtEtl.Rows.Count > 0)
            {
                row = dtEtl.Rows[0];

                mcaStr = row["MinCKTAmp"].ToString();

                if (dualPoint == true)
                {
                    mcaStr += " / " + row["MinCKTAmp2"].ToString() + " -  Dualpoint";
                }

                mopStr = row["MFSMCB"].ToString();
                voltageStr = row["ElecRating"].ToString();

                if (fuelTypeStr != "" && fuelTypeStr.StartsWith("No") == false)
                {
                    heatTypeStr += " - " + fuelTypeStr;
                }

                if (row["FanEvapFLA"] != null)
                {
                    evapFanFlaStr = row["FanEvapFLA"].ToString();
                    evapFanFlaStr = " - FLA = " + evapFanFlaStr;
                }
                else
                {
                    evapFanFlaStr = "";
                }

                if (row["FanPwrExhFLA"] != null)
                {
                    pwrExhFlaStr = row["FanPwrExhFLA"].ToString();
                    pwrExhFlaStr = " - FLA = " + pwrExhFlaStr;
                }
                else
                {
                    pwrExhFlaStr = "";
                }
            }           

            if (modelNoStr.Length == 39)
            {
                oaRev = "REV5";
                validOA_Job = true;
              
                wshpAuxSecInfo = "";
                if (modelNoStr.Substring(3, 1) == "E" && (modelNoStr.Substring(13, 1) == "3" || modelNoStr.Substring(13, 1) == "8"))
                {
                    wshpAuxSecInfo = "Outdoor WaterSource Heat Pump";
                }
                else if (modelNoStr.Substring(3, 1) == "F" && (modelNoStr.Substring(13, 1) == "3" || modelNoStr.Substring(13, 1) == "8"))
                {
                    wshpAuxSecInfo = "Indoor WaterSource Heat Pump";
                }                

                auxBox = "NO Aux Box";
                secModStr = "NO Secondary Module";
                if (unitTypeStr == "OALBG")
                {                    
                    if (modelNoStr.Substring(26,1) != "0" && modelNoStr.Substring(26,1) != "X")
                    {
                        auxBox = "Aux Box = YES";
                    }
                    else if (modelNoStr.Substring(26, 1) == "9" && (modelNoStr.Substring(7, 1) != "B" || modelNoStr.Substring(7, 1) != "D"))
                    {
                        auxBox = "Aux Box = YES";
                    }
                }
                else
                {
                    if (secModDigit8ValuesBDGH.Contains(modelNoStr.Substring(7, 1)) == false && modelNoStr.Substring(31, 1) != "0" && modelNoStr.Substring(31, 1) != "X")
                    {
                        auxBox = "Aux Box = YES";
                    }
                    else if (secModDigit8ValuesBDGH.Contains(modelNoStr.Substring(7, 1)) == true &&modelNoStr.Substring(31, 1) != "0" && modelNoStr.Substring(31, 1) != "X")
                    {
                        auxBox = "Aux Box = YES";
                        secModStr = "Secondary Module = YES";
                    }
                    else if (secModDigit8ValuesBDGH.Contains(modelNoStr.Substring(7, 1)) == true && secModDigit20Values0ABCDEFJK.Contains(modelNoStr.Substring(19, 1)) && 
                             modelNoStr.Substring(31, 1) == "0")
                    {
                        secModStr = "Secondary Module = YES";
                    }
                    else if (secModDigit8ValuesACEF.Contains(modelNoStr.Substring(7, 1)) == true && secModDigit20ValuesGHLNQSTUVWYZ.Contains(modelNoStr.Substring(19, 1)) == true &&
                             modelNoStr.Substring(31, 1) == "0")
                    {
                        secModStr = "Secondary Module = YES";
                    }
                    else if (secModDigit8ValuesBDGH.Contains(modelNoStr.Substring(7, 1)) == true && secModDigit20ValuesGHLNQSTUVWYZ.Contains(modelNoStr.Substring(19, 1)) == true && 
                             modelNoStr.Substring(31, 1) == "0")
                    {
                        secModStr = "Secondary Module = YES";
                    }
                }

                if (wshpAuxSecInfo.Length == 0)
                {
                    wshpAuxSecInfo = auxBox + "  --  " + secModStr;
                }
                else
                {
                    wshpAuxSecInfo += "  --  " + auxBox + "  --  " + secModStr;
                }

                DataTable dtMod = objMain.GetRev5ModelNoDigitDesc(8, modelNoStr.Substring(7, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    airflowConStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(20, modelNoStr.Substring(7, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    heatTypeStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(21, modelNoStr.Substring(20, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    fuelTypeStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(18, modelNoStr.Substring(17, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];

                    supplyFanMotorHPStr = drMod["DigitDescription"].ToString() + evapFanFlaStr;
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(16, modelNoStr.Substring(15, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    supplyFanMotorTypeStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(17, modelNoStr.Substring(16, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    supplyFanWheelStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(29, modelNoStr.Substring(28, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];

                    exhaustFanMotorHPStr = drMod["DigitDescription"].ToString() + pwrExhFlaStr;
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(27, modelNoStr.Substring(26, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    exhaustFanMotorTypeStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(28, modelNoStr.Substring(27, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    exhaustFanWheelStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(31, modelNoStr.Substring(30, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    ervInfoStr = drMod["DigitDescription"].ToString();                   
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(32, modelNoStr.Substring(31, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    ervWheelStr = drMod["DigitDescription"].ToString();

                    if (ervInfoStr != "No ERV")
                    {
                        ervWheelStr += " - " + ervInfoStr;
                    }
                }

                dtMod = objMain.GetRev5ModelNoDigitDesc(36, modelNoStr.Substring(35, 1), "NA", unitTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    elecOptionsStr = drMod["DigitDescription"].ToString();

                    if (elecOptionsStr.Contains("Dual") == true)
                    {
                        dualPoint = true;
                    }
                }
            }
            else if (modelNoStr.Length == 69)
            {
                oaRev = "REV6";
                validOA_Job = true;

                if (modelNoStr.Substring(50, 1) == "A")
                {
                    wshpAuxSecInfo = "Outdoor WaterSource Heat Pump";
                }
                else if (modelNoStr.Substring(50, 1) == "B")
                {
                    wshpAuxSecInfo = "Indoor WaterSource Heat Pump";
                }
                else
                {
                    wshpAuxSecInfo = "";
                }

                DataTable dtMod = objMain.GetModelNumDesc(8, "NA", modelNoStr.Substring(7, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    airflowConStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetModelNumDesc(16, "NA", modelNoStr.Substring(15, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    heatTypeStr = drMod["DigitDescription"].ToString();
                    fuelTypeStr = "";
                }

                dtMod = objMain.GetModelNumDesc(21, "NA", modelNoStr.Substring(20, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                   
                    supplyFanMotorHPStr = drMod["DigitDescription"].ToString() + evapFanFlaStr;
                }

                dtMod = objMain.GetModelNumDesc(22, "NA", modelNoStr.Substring(21, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    supplyFanMotorTypeStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetModelNumDesc(2324, "NA", modelNoStr.Substring(22, 2), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    supplyFanWheelStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetModelNumDesc(25, "NA", modelNoStr.Substring(24, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];                   
                   
                    exhaustFanMotorHPStr = drMod["DigitDescription"].ToString() + pwrExhFlaStr;
                }

                dtMod = objMain.GetModelNumDesc(26, "NA", modelNoStr.Substring(25, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    exhaustFanMotorTypeStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetModelNumDesc(2728, "NA", modelNoStr.Substring(26, 2), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    exhaustFanWheelStr = drMod["DigitDescription"].ToString();
                }

                dtMod = objMain.GetModelNumDesc(34, "NA", modelNoStr.Substring(33, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    ervInfoStr = drMod["DigitDescription"].ToString();                    
                }

                dtMod = objMain.GetModelNumDesc(36, "NA", modelNoStr.Substring(35, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    ervWheelStr = drMod["DigitDescription"].ToString();
                    if (ervInfoStr != "No ERV")
                    {
                        ervWheelStr += " - " + ervInfoStr;
                    }
                }

                dtMod = objMain.GetModelNumDesc(41, "NA", modelNoStr.Substring(40, 1), modelTypeStr);
                if (dtMod.Rows.Count > 0)
                {
                    DataRow drMod = dtMod.Rows[0];
                    elecOptionsStr = drMod["DigitDescription"].ToString();
                }

                if (elecOptionsStr.Contains("Dual") == true)
                {
                    dualPoint = true;
                }
            }
            else
            {
                validOA_Job = false;
                MessageBox.Show("ERROR - The selected Job Number is for a unit other than a OA Rev 5 or Rev 6!");
            }

            if (validOA_Job)
            {                
                if (rbAllStations.Checked == true)
                {
                    subStationStr = "ALL";
                    partTypeStr = "";
                }
                else if (rbSubAsm1.Checked == true)
                {
                    subStationStr = "SubStat1";
                    partTypeStr = "";
                }
                else if (rbSubAsm2.Checked == true)
                {
                    subStationStr = "SubStat2";
                    partTypeStr = "";
                }
                else if (rbSubAsm3.Checked == true)
                {
                    subStationStr = "SubStat3";
                    partTypeStr = "ERV&Damper";
                }
                else if (rbSubAsm4.Checked == true)
                {
                    subStationStr = "SubStat4";
                    partTypeStr = "";
                }
                else if (rbSubAsm5.Checked == true)
                {
                    subStationStr = "SubStat5";
                    partTypeStr = "";
                }
                else if (rbSubAsm6.Checked == true)
                {
                    subStationStr = "SubStat6";
                    partTypeStr = "";
                }
                else if (rbSubAsm7.Checked == true)
                {
                    subStationStr = "SubStat7";
                    partTypeStr = "";
                }
                else if (rbSubAsm8.Checked == true)
                {
                    subStationStr = "SubStat8";
                    partTypeStr = "Rosenberg";
                }
                else if (rbSubAsm9.Checked == true)
                {
                    subStationStr = "SubStat9";
                    partTypeStr = "Breaker";
                }
                else if (rbSubAsm10.Checked == true)
                {
                    subStationStr = "SubStat10";
                    partTypeStr = "Motor";
                }
                else if (rbSubAsm11.Checked == true)
                {
                    subStationStr = "SubStat11";
                    partTypeStr = "Motor";
                }
                else if (rbSubAsm12.Checked == true)
                {
                    subStationStr = "SubStat12";
                    partTypeStr = "";
                }                

                frmPrintBOM frmPrint = new frmPrintBOM();

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNum";
                pdv1.Value = jobNumStr;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@OA_Rev";
                pdv2.Value = oaRev;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                ParameterField pf3 = new ParameterField();
                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                pf3.Name = "@ModelNo";
                pdv3.Value = modelNoStr;
                pf3.CurrentValues.Add(pdv3);

                paramFields.Add(pf3);

                ParameterField pf4 = new ParameterField();
                ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
                pf4.Name = "@MCA";
                pdv4.Value = mcaStr;
                pf4.CurrentValues.Add(pdv4);

                paramFields.Add(pf4);

                ParameterField pf5 = new ParameterField();
                ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
                pf5.Name = "@MOP";
                pdv5.Value = mopStr;
                pf5.CurrentValues.Add(pdv5);

                paramFields.Add(pf5);

                ParameterField pf6 = new ParameterField();
                ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
                pf6.Name = "@Voltage";
                pdv6.Value = voltageStr;
                pf6.CurrentValues.Add(pdv6);

                paramFields.Add(pf6);

                ParameterField pf7 = new ParameterField();
                ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
                pf7.Name = "@HeatType";
                pdv7.Value = heatTypeStr;
                pf7.CurrentValues.Add(pdv7);

                paramFields.Add(pf7);

                ParameterField pf8 = new ParameterField();
                ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
                pf8.Name = "@AirflowInfo";
                pdv8.Value = airflowConStr;
                pf8.CurrentValues.Add(pdv8);

                paramFields.Add(pf8);

                ParameterField pf9 = new ParameterField();
                ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
                pf9.Name = "@SupplyFanHP";
                pdv9.Value = supplyFanMotorHPStr;
                pf9.CurrentValues.Add(pdv9);

                paramFields.Add(pf9);

                ParameterField pf10 = new ParameterField();
                ParameterDiscreteValue pdv10 = new ParameterDiscreteValue();
                pf10.Name = "@SupplyFanMotorType";
                pdv10.Value = supplyFanMotorTypeStr;
                pf10.CurrentValues.Add(pdv10);

                paramFields.Add(pf10);

                ParameterField pf11 = new ParameterField();
                ParameterDiscreteValue pdv11 = new ParameterDiscreteValue();
                pf11.Name = "@SupplyFanWheelInfo";
                pdv11.Value = supplyFanWheelStr;
                pf11.CurrentValues.Add(pdv11);

                paramFields.Add(pf11);

                ParameterField pf12 = new ParameterField();
                ParameterDiscreteValue pdv12 = new ParameterDiscreteValue();
                pf12.Name = "@ExhaustFanHP";
                pdv12.Value = exhaustFanMotorHPStr;
                pf12.CurrentValues.Add(pdv12);

                paramFields.Add(pf12);

                ParameterField pf13 = new ParameterField();
                ParameterDiscreteValue pdv13 = new ParameterDiscreteValue();
                pf13.Name = "@ExhaustFanMotorType";
                pdv13.Value = exhaustFanMotorTypeStr;
                pf13.CurrentValues.Add(pdv13);

                paramFields.Add(pf13);

                ParameterField pf14 = new ParameterField();
                ParameterDiscreteValue pdv14 = new ParameterDiscreteValue();
                pf14.Name = "@ExhaustFanWheelInfo";
                pdv14.Value = exhaustFanWheelStr;
                pf14.CurrentValues.Add(pdv14);

                paramFields.Add(pf14);

                ParameterField pf15 = new ParameterField();
                ParameterDiscreteValue pdv15 = new ParameterDiscreteValue();
                pf15.Name = "@ERVWheelInfo";
                pdv15.Value = ervWheelStr;
                pf15.CurrentValues.Add(pdv15);

                paramFields.Add(pf15);

                ParameterField pf16 = new ParameterField();
                ParameterDiscreteValue pdv16 = new ParameterDiscreteValue();
                pf16.Name = "@ELecOptions";
                pdv16.Value = elecOptionsStr;
                pf16.CurrentValues.Add(pdv16);

                paramFields.Add(pf16);

                ParameterField pf17 = new ParameterField();
                ParameterDiscreteValue pdv17 = new ParameterDiscreteValue();
                pf17.Name = "@SubStation";
                pdv17.Value = subStationStr;
                pf17.CurrentValues.Add(pdv17);

                paramFields.Add(pf17);

                ParameterField pf18 = new ParameterField();
                ParameterDiscreteValue pdv18 = new ParameterDiscreteValue();
                pf18.Name = "@PartType";
                pdv18.Value = partTypeStr;
                pf18.CurrentValues.Add(pdv18);

                paramFields.Add(pf18);

                ParameterField pf19 = new ParameterField();
                ParameterDiscreteValue pdv19 = new ParameterDiscreteValue();
                pf19.Name = "@WshpAuxSec";
                pdv19.Value = wshpAuxSecInfo;
                pf19.CurrentValues.Add(pdv19);

                paramFields.Add(pf19);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\SubAssemblyLine.rpt";             
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\SubAssemblyLine.rpt";
#endif

                cryRpt.Load(objMain.ReportString);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

    }
}
