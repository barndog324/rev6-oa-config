﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OA_Config_Rev6
{
    public class SubAssembly
    {        
        public string AssemblyPartNum { get; set; }
        public string AssemblyRevNum { get; set; }
        public string AsmQty { get; set; }
        public string AsmUOM { get; set; }        
        public string ImmediateParent { get; set; }
        public int ParentAsmSeq { get; set; }
        public int AsmSeq { get; set; }
        public int Parent { get; set; }
        public int PriorPeer { get; set; }
        public int NextPeer { get; set; }
        public int Child { get; set; }
        public int BomSeq { get; set; }
        public int BomLevel { get; set; }
        public int ReqRefDes { get; set; }
        public string WeightUOM { get; set; }
        public bool ConfigurableSubAsm { get; set; }        

        //static List<SubAssembly> SubAsm = new List<SubAssembly>();

        public SubAssembly(string assemblyPartNum, string asmRevNum, string asmQty, string asmUOM, int asmSeq,
                           int parent, int priorPeer, int nextPeer, int child, int bomSeq, int bomLevel, int parentAsmSeq,
                           string immediateParent, int reqRefDes, string weightUOMStr, bool configurableSubAsm)
        {
            AssemblyPartNum = assemblyPartNum;
            AssemblyRevNum = asmRevNum;
            AsmQty = asmQty;
            AsmUOM = asmUOM;
            AsmSeq = asmSeq;
            Parent = parent;
            PriorPeer = priorPeer;
            NextPeer = nextPeer;
            Child = child;
            BomSeq = bomSeq;
            BomLevel = bomLevel;
            ParentAsmSeq = parentAsmSeq;
            ImmediateParent = immediateParent;
            ReqRefDes = reqRefDes;
            WeightUOM = weightUOMStr;
            ConfigurableSubAsm = configurableSubAsm;
        }
    }
}
