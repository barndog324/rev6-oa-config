﻿namespace OA_Config_Rev6
{
    partial class frmOldModelNoConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCustName = new System.Windows.Forms.Label();
            this.lbLastUpdateDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbCompleteDate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbProdStartDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbBOMCreationDate = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbShipDate = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lbOrderDate = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.groupBoxCabinet = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbTranePPS_Compatibility = new System.Windows.Forms.ComboBox();
            this.gbHotWaterInputs = new System.Windows.Forms.GroupBox();
            this.label209 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.txtEnteringTemp = new System.Windows.Forms.TextBox();
            this.txtFlowRate = new System.Windows.Forms.TextBox();
            this.cbHeatSourceSecondary = new System.Windows.Forms.ComboBox();
            this.cbHeatSourcePrimary = new System.Windows.Forms.ComboBox();
            this.cbMajorDesign = new System.Windows.Forms.ComboBox();
            this.label133 = new System.Windows.Forms.Label();
            this.cbMinorDesign = new System.Windows.Forms.ComboBox();
            this.label132 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.cbAltitude = new System.Windows.Forms.ComboBox();
            this.label158 = new System.Windows.Forms.Label();
            this.cbHailguard = new System.Windows.Forms.ComboBox();
            this.label157 = new System.Windows.Forms.Label();
            this.cbAirFlowMonitoring = new System.Windows.Forms.ComboBox();
            this.label156 = new System.Windows.Forms.Label();
            this.cbElectricalOptions = new System.Windows.Forms.ComboBox();
            this.label155 = new System.Windows.Forms.Label();
            this.cbSmokeDetector = new System.Windows.Forms.ComboBox();
            this.label154 = new System.Windows.Forms.Label();
            this.cbEconomizer = new System.Windows.Forms.ComboBox();
            this.label153 = new System.Windows.Forms.Label();
            this.cbERVSize = new System.Windows.Forms.ComboBox();
            this.label152 = new System.Windows.Forms.Label();
            this.cbERV_HRV = new System.Windows.Forms.ComboBox();
            this.label151 = new System.Windows.Forms.Label();
            this.cbPoweredExhaustFanMotorHP = new System.Windows.Forms.ComboBox();
            this.cbPoweredExhaustFanWheel = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbPoweredExhaustFanMotor = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbUnitControls = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbCorrosiveEnvPackage = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelInputsHeatCapacityLabel = new System.Windows.Forms.Label();
            this.cbFuelType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.cbIndoorFanMotorHP = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.cbIndoorFanWheel = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.cbIndoorFanMotor = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.cbRefridgerantCapacityControl = new System.Windows.Forms.ComboBox();
            this.label143 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbEvapType = new System.Windows.Forms.ComboBox();
            this.cbCoolingCapacity = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.cbFiltrationOptions = new System.Windows.Forms.ComboBox();
            this.label141 = new System.Windows.Forms.Label();
            this.cbCondenser = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.cbHotGasReheat = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbCompressor = new System.Windows.Forms.ComboBox();
            this.label139 = new System.Windows.Forms.Label();
            this.cbHeatType = new System.Windows.Forms.ComboBox();
            this.label138 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.cbVoltage = new System.Windows.Forms.ComboBox();
            this.label135 = new System.Windows.Forms.Label();
            this.cbCabinet = new System.Windows.Forms.ComboBox();
            this.label136 = new System.Windows.Forms.Label();
            this.btnCreateBOM = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtModelNo = new System.Windows.Forms.TextBox();
            this.lbEnterModelNo = new System.Windows.Forms.Label();
            this.btnParseModelNo = new System.Windows.Forms.Button();
            this.menuStripConfig = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteBOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbLastUpdateBy = new System.Windows.Forms.Label();
            this.lbBomCreateBy = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lbUnitType = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBoxCabinet.SuspendLayout();
            this.gbHotWaterInputs.SuspendLayout();
            this.menuStripConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Black;
            this.lbCustName.Location = new System.Drawing.Point(242, 100);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(525, 30);
            this.lbCustName.TabIndex = 465;
            this.lbCustName.Text = "CustomerName";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbLastUpdateDate
            // 
            this.lbLastUpdateDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateDate.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateDate.Location = new System.Drawing.Point(147, 66);
            this.lbLastUpdateDate.Name = "lbLastUpdateDate";
            this.lbLastUpdateDate.Size = new System.Drawing.Size(87, 20);
            this.lbLastUpdateDate.TabIndex = 410;
            this.lbLastUpdateDate.Text = "??/??/????";
            this.lbLastUpdateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(8, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 409;
            this.label3.Text = "Last Updated Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label129
            // 
            this.label129.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Black;
            this.label129.Location = new System.Drawing.Point(801, 183);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(10, 15);
            this.label129.TabIndex = 408;
            this.label129.Text = "9";
            // 
            // label130
            // 
            this.label130.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Black;
            this.label130.Location = new System.Drawing.Point(801, 168);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(10, 15);
            this.label130.TabIndex = 407;
            this.label130.Text = "3";
            // 
            // label131
            // 
            this.label131.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Black;
            this.label131.Location = new System.Drawing.Point(801, 160);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(10, 15);
            this.label131.TabIndex = 406;
            this.label131.Text = "^";
            // 
            // label127
            // 
            this.label127.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Black;
            this.label127.Location = new System.Drawing.Point(642, 183);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(10, 15);
            this.label127.TabIndex = 405;
            this.label127.Text = "9";
            // 
            // label128
            // 
            this.label128.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Black;
            this.label128.Location = new System.Drawing.Point(626, 183);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(10, 15);
            this.label128.TabIndex = 404;
            this.label128.Text = "8";
            // 
            // label119
            // 
            this.label119.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(785, 183);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(10, 15);
            this.label119.TabIndex = 403;
            this.label119.Text = "8";
            // 
            // label120
            // 
            this.label120.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(769, 183);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(10, 15);
            this.label120.TabIndex = 402;
            this.label120.Text = "7";
            // 
            // label121
            // 
            this.label121.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(753, 183);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(10, 15);
            this.label121.TabIndex = 401;
            this.label121.Text = "6";
            // 
            // label122
            // 
            this.label122.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Black;
            this.label122.Location = new System.Drawing.Point(738, 183);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(10, 15);
            this.label122.TabIndex = 400;
            this.label122.Text = "5";
            // 
            // label123
            // 
            this.label123.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Black;
            this.label123.Location = new System.Drawing.Point(722, 183);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(10, 15);
            this.label123.TabIndex = 399;
            this.label123.Text = "4";
            // 
            // label124
            // 
            this.label124.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.Black;
            this.label124.Location = new System.Drawing.Point(705, 183);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(10, 15);
            this.label124.TabIndex = 398;
            this.label124.Text = "3";
            // 
            // label125
            // 
            this.label125.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.Black;
            this.label125.Location = new System.Drawing.Point(688, 183);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(10, 15);
            this.label125.TabIndex = 397;
            this.label125.Text = "2";
            // 
            // label126
            // 
            this.label126.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Black;
            this.label126.Location = new System.Drawing.Point(673, 183);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(10, 15);
            this.label126.TabIndex = 396;
            this.label126.Text = "1";
            // 
            // label111
            // 
            this.label111.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(610, 183);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(10, 15);
            this.label111.TabIndex = 395;
            this.label111.Text = "7";
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Black;
            this.label112.Location = new System.Drawing.Point(595, 183);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(10, 15);
            this.label112.TabIndex = 394;
            this.label112.Text = "6";
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(578, 183);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(10, 15);
            this.label113.TabIndex = 393;
            this.label113.Text = "5";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(562, 183);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(10, 15);
            this.label114.TabIndex = 392;
            this.label114.Text = "4";
            // 
            // label115
            // 
            this.label115.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(546, 183);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(10, 15);
            this.label115.TabIndex = 391;
            this.label115.Text = "3";
            // 
            // label116
            // 
            this.label116.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Black;
            this.label116.Location = new System.Drawing.Point(529, 183);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(10, 15);
            this.label116.TabIndex = 390;
            this.label116.Text = "2";
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(512, 183);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(10, 15);
            this.label117.TabIndex = 389;
            this.label117.Text = "1";
            // 
            // label118
            // 
            this.label118.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(497, 183);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(10, 15);
            this.label118.TabIndex = 388;
            this.label118.Text = "0";
            // 
            // label103
            // 
            this.label103.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(465, 183);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(10, 15);
            this.label103.TabIndex = 387;
            this.label103.Text = "8";
            // 
            // label104
            // 
            this.label104.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(450, 183);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(10, 15);
            this.label104.TabIndex = 386;
            this.label104.Text = "7";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(433, 183);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(10, 15);
            this.label105.TabIndex = 385;
            this.label105.Text = "6";
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(417, 183);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(10, 15);
            this.label106.TabIndex = 384;
            this.label106.Text = "5";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(400, 183);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(10, 15);
            this.label107.TabIndex = 383;
            this.label107.Text = "4";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(384, 183);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(10, 15);
            this.label108.TabIndex = 382;
            this.label108.Text = "3";
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Black;
            this.label109.Location = new System.Drawing.Point(367, 183);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(10, 15);
            this.label109.TabIndex = 381;
            this.label109.Text = "2";
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(352, 183);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(10, 15);
            this.label110.TabIndex = 380;
            this.label110.Text = "1";
            // 
            // label102
            // 
            this.label102.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(321, 168);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(10, 15);
            this.label102.TabIndex = 379;
            this.label102.Text = "9";
            // 
            // label100
            // 
            this.label100.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(785, 168);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(10, 15);
            this.label100.TabIndex = 378;
            this.label100.Text = "3";
            // 
            // label101
            // 
            this.label101.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(769, 168);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(10, 15);
            this.label101.TabIndex = 377;
            this.label101.Text = "3";
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(753, 168);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(10, 15);
            this.label96.TabIndex = 376;
            this.label96.Text = "3";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(738, 168);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(10, 15);
            this.label97.TabIndex = 375;
            this.label97.Text = "3";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(722, 168);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(10, 15);
            this.label98.TabIndex = 374;
            this.label98.Text = "3";
            // 
            // label99
            // 
            this.label99.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(705, 168);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(10, 15);
            this.label99.TabIndex = 373;
            this.label99.Text = "3";
            // 
            // label92
            // 
            this.label92.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(688, 168);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(10, 15);
            this.label92.TabIndex = 372;
            this.label92.Text = "3";
            // 
            // label93
            // 
            this.label93.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(673, 168);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(10, 15);
            this.label93.TabIndex = 371;
            this.label93.Text = "3";
            // 
            // label94
            // 
            this.label94.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(642, 168);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(10, 15);
            this.label94.TabIndex = 370;
            this.label94.Text = "2";
            // 
            // label95
            // 
            this.label95.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(626, 168);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(10, 15);
            this.label95.TabIndex = 369;
            this.label95.Text = "2";
            // 
            // label88
            // 
            this.label88.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(610, 168);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(10, 15);
            this.label88.TabIndex = 368;
            this.label88.Text = "2";
            // 
            // label89
            // 
            this.label89.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(595, 168);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(10, 15);
            this.label89.TabIndex = 367;
            this.label89.Text = "2";
            // 
            // label90
            // 
            this.label90.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(578, 168);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(10, 15);
            this.label90.TabIndex = 366;
            this.label90.Text = "2";
            // 
            // label91
            // 
            this.label91.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(562, 168);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(10, 15);
            this.label91.TabIndex = 365;
            this.label91.Text = "2";
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(546, 168);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(10, 15);
            this.label84.TabIndex = 364;
            this.label84.Text = "2";
            // 
            // label85
            // 
            this.label85.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(529, 168);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(10, 15);
            this.label85.TabIndex = 363;
            this.label85.Text = "2";
            // 
            // label86
            // 
            this.label86.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(512, 168);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(10, 15);
            this.label86.TabIndex = 362;
            this.label86.Text = "2";
            // 
            // label87
            // 
            this.label87.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(497, 168);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(10, 15);
            this.label87.TabIndex = 361;
            this.label87.Text = "2";
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(465, 168);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(10, 15);
            this.label80.TabIndex = 360;
            this.label80.Text = "1";
            // 
            // label81
            // 
            this.label81.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(450, 168);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(10, 15);
            this.label81.TabIndex = 359;
            this.label81.Text = "1";
            // 
            // label82
            // 
            this.label82.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(433, 168);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(10, 15);
            this.label82.TabIndex = 358;
            this.label82.Text = "1";
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(417, 168);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(10, 15);
            this.label83.TabIndex = 357;
            this.label83.Text = "1";
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(400, 168);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(10, 15);
            this.label76.TabIndex = 356;
            this.label76.Text = "1";
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(384, 168);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(10, 15);
            this.label77.TabIndex = 355;
            this.label77.Text = "1";
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(367, 168);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(10, 15);
            this.label78.TabIndex = 354;
            this.label78.Text = "1";
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(352, 168);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(10, 15);
            this.label79.TabIndex = 353;
            this.label79.Text = "1";
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(305, 168);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(10, 15);
            this.label72.TabIndex = 352;
            this.label72.Text = "8";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(289, 168);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(10, 15);
            this.label73.TabIndex = 351;
            this.label73.Text = "7";
            // 
            // label74
            // 
            this.label74.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(272, 168);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(10, 15);
            this.label74.TabIndex = 350;
            this.label74.Text = "6";
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(256, 168);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(10, 15);
            this.label75.TabIndex = 349;
            this.label75.Text = "5";
            // 
            // label70
            // 
            this.label70.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(241, 168);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(10, 15);
            this.label70.TabIndex = 348;
            this.label70.Text = "4";
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(225, 168);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(10, 15);
            this.label71.TabIndex = 347;
            this.label71.Text = "3";
            // 
            // label69
            // 
            this.label69.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(208, 168);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(10, 15);
            this.label69.TabIndex = 346;
            this.label69.Text = "2";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(193, 168);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(10, 15);
            this.label68.TabIndex = 345;
            this.label68.Text = "1";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(321, 160);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(10, 15);
            this.label67.TabIndex = 344;
            this.label67.Text = "^";
            // 
            // label66
            // 
            this.label66.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(642, 160);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(10, 15);
            this.label66.TabIndex = 343;
            this.label66.Text = "^";
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(626, 160);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(10, 15);
            this.label65.TabIndex = 342;
            this.label65.Text = "^";
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(785, 160);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(10, 15);
            this.label61.TabIndex = 341;
            this.label61.Text = "^";
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(769, 160);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(10, 15);
            this.label62.TabIndex = 340;
            this.label62.Text = "^";
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(753, 160);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(10, 15);
            this.label63.TabIndex = 339;
            this.label63.Text = "^";
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(738, 160);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(10, 15);
            this.label64.TabIndex = 338;
            this.label64.Text = "^";
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(722, 160);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(10, 15);
            this.label57.TabIndex = 337;
            this.label57.Text = "^";
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(705, 160);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(10, 15);
            this.label58.TabIndex = 336;
            this.label58.Text = "^";
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(689, 160);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(10, 15);
            this.label59.TabIndex = 335;
            this.label59.Text = "^";
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(674, 160);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(10, 15);
            this.label60.TabIndex = 334;
            this.label60.Text = "^";
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(610, 160);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(10, 15);
            this.label53.TabIndex = 333;
            this.label53.Text = "^";
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(595, 160);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(10, 15);
            this.label54.TabIndex = 332;
            this.label54.Text = "^";
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(578, 160);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(10, 15);
            this.label55.TabIndex = 331;
            this.label55.Text = "^";
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(563, 160);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(10, 15);
            this.label56.TabIndex = 330;
            this.label56.Text = "^";
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(546, 160);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(10, 15);
            this.label49.TabIndex = 329;
            this.label49.Text = "^";
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(529, 160);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(10, 15);
            this.label50.TabIndex = 328;
            this.label50.Text = "^";
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(513, 160);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(10, 15);
            this.label51.TabIndex = 327;
            this.label51.Text = "^";
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(497, 160);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(10, 15);
            this.label52.TabIndex = 326;
            this.label52.Text = "^";
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(465, 160);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(10, 15);
            this.label45.TabIndex = 325;
            this.label45.Text = "^";
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(450, 160);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(10, 15);
            this.label46.TabIndex = 324;
            this.label46.Text = "^";
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(434, 160);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(10, 15);
            this.label47.TabIndex = 323;
            this.label47.Text = "^";
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(418, 160);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(10, 15);
            this.label48.TabIndex = 322;
            this.label48.Text = "^";
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(400, 160);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(10, 15);
            this.label41.TabIndex = 321;
            this.label41.Text = "^";
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(384, 160);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(10, 15);
            this.label42.TabIndex = 320;
            this.label42.Text = "^";
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(368, 160);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(10, 15);
            this.label43.TabIndex = 319;
            this.label43.Text = "^";
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(353, 160);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(10, 15);
            this.label44.TabIndex = 318;
            this.label44.Text = "^";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(306, 160);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(10, 15);
            this.label37.TabIndex = 317;
            this.label37.Text = "^";
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(289, 160);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(10, 15);
            this.label38.TabIndex = 316;
            this.label38.Text = "^";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(273, 160);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(10, 15);
            this.label39.TabIndex = 315;
            this.label39.Text = "^";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(257, 160);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(10, 15);
            this.label40.TabIndex = 314;
            this.label40.Text = "^";
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(241, 160);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(10, 15);
            this.label35.TabIndex = 313;
            this.label35.Text = "^";
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(225, 160);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(10, 15);
            this.label36.TabIndex = 312;
            this.label36.Text = "^";
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(208, 160);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(10, 15);
            this.label34.TabIndex = 311;
            this.label34.Text = "^";
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(193, 160);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(10, 15);
            this.label33.TabIndex = 310;
            this.label33.Text = "^";
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Black;
            this.lbModelNo.Location = new System.Drawing.Point(187, 134);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(638, 30);
            this.lbModelNo.TabIndex = 309;
            this.lbModelNo.Text = "123456789-12345678-01234567890123456789\r\n";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCompleteDate
            // 
            this.lbCompleteDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCompleteDate.ForeColor = System.Drawing.Color.Black;
            this.lbCompleteDate.Location = new System.Drawing.Point(889, 47);
            this.lbCompleteDate.Name = "lbCompleteDate";
            this.lbCompleteDate.Size = new System.Drawing.Size(94, 20);
            this.lbCompleteDate.TabIndex = 308;
            this.lbCompleteDate.Text = "??/??/????";
            this.lbCompleteDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(771, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.TabIndex = 307;
            this.label4.Text = "Complete Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbProdStartDate
            // 
            this.lbProdStartDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProdStartDate.ForeColor = System.Drawing.Color.Black;
            this.lbProdStartDate.Location = new System.Drawing.Point(889, 27);
            this.lbProdStartDate.Name = "lbProdStartDate";
            this.lbProdStartDate.Size = new System.Drawing.Size(94, 20);
            this.lbProdStartDate.TabIndex = 306;
            this.lbProdStartDate.Text = "??/??/????";
            this.lbProdStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(771, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 305;
            this.label2.Text = "Prod Start Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbBOMCreationDate
            // 
            this.lbBOMCreationDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBOMCreationDate.ForeColor = System.Drawing.Color.Black;
            this.lbBOMCreationDate.Location = new System.Drawing.Point(147, 27);
            this.lbBOMCreationDate.Name = "lbBOMCreationDate";
            this.lbBOMCreationDate.Size = new System.Drawing.Size(87, 20);
            this.lbBOMCreationDate.TabIndex = 304;
            this.lbBOMCreationDate.Text = "??/??/????";
            this.lbBOMCreationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Teal;
            this.label27.Location = new System.Drawing.Point(8, 27);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(140, 20);
            this.label27.TabIndex = 303;
            this.label27.Text = "BOM Creation Date:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbShipDate
            // 
            this.lbShipDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShipDate.ForeColor = System.Drawing.Color.Black;
            this.lbShipDate.Location = new System.Drawing.Point(889, 67);
            this.lbShipDate.Name = "lbShipDate";
            this.lbShipDate.Size = new System.Drawing.Size(94, 20);
            this.lbShipDate.TabIndex = 302;
            this.lbShipDate.Text = "??/??/????";
            this.lbShipDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Teal;
            this.label32.Location = new System.Drawing.Point(749, 67);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 20);
            this.label32.TabIndex = 301;
            this.label32.Text = "Ship Date:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbOrderDate
            // 
            this.lbOrderDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderDate.ForeColor = System.Drawing.Color.Black;
            this.lbOrderDate.Location = new System.Drawing.Point(888, 87);
            this.lbOrderDate.Name = "lbOrderDate";
            this.lbOrderDate.Size = new System.Drawing.Size(100, 20);
            this.lbOrderDate.TabIndex = 300;
            this.lbOrderDate.Text = "??/??/????";
            this.lbOrderDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Teal;
            this.label26.Location = new System.Drawing.Point(749, 87);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 20);
            this.label26.TabIndex = 299;
            this.label26.Text = "Order Date:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(326, 62);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(140, 35);
            this.label25.TabIndex = 298;
            this.label25.Text = "Job #:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Tahoma", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Black;
            this.lbJobNum.Location = new System.Drawing.Point(465, 62);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(187, 35);
            this.lbJobNum.TabIndex = 297;
            this.lbJobNum.Text = "??????-??-??";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Engravers MT", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.Blue;
            this.lbEnvironment.Location = new System.Drawing.Point(242, 27);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(525, 30);
            this.lbEnvironment.TabIndex = 296;
            this.lbEnvironment.Text = "Environment";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxCabinet
            // 
            this.groupBoxCabinet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBoxCabinet.Controls.Add(this.label15);
            this.groupBoxCabinet.Controls.Add(this.cbTranePPS_Compatibility);
            this.groupBoxCabinet.Controls.Add(this.gbHotWaterInputs);
            this.groupBoxCabinet.Controls.Add(this.cbHeatSourceSecondary);
            this.groupBoxCabinet.Controls.Add(this.cbHeatSourcePrimary);
            this.groupBoxCabinet.Controls.Add(this.cbMajorDesign);
            this.groupBoxCabinet.Controls.Add(this.label133);
            this.groupBoxCabinet.Controls.Add(this.cbMinorDesign);
            this.groupBoxCabinet.Controls.Add(this.label132);
            this.groupBoxCabinet.Controls.Add(this.label159);
            this.groupBoxCabinet.Controls.Add(this.cbAltitude);
            this.groupBoxCabinet.Controls.Add(this.label158);
            this.groupBoxCabinet.Controls.Add(this.cbHailguard);
            this.groupBoxCabinet.Controls.Add(this.label157);
            this.groupBoxCabinet.Controls.Add(this.cbAirFlowMonitoring);
            this.groupBoxCabinet.Controls.Add(this.label156);
            this.groupBoxCabinet.Controls.Add(this.cbElectricalOptions);
            this.groupBoxCabinet.Controls.Add(this.label155);
            this.groupBoxCabinet.Controls.Add(this.cbSmokeDetector);
            this.groupBoxCabinet.Controls.Add(this.label154);
            this.groupBoxCabinet.Controls.Add(this.cbEconomizer);
            this.groupBoxCabinet.Controls.Add(this.label153);
            this.groupBoxCabinet.Controls.Add(this.cbERVSize);
            this.groupBoxCabinet.Controls.Add(this.label152);
            this.groupBoxCabinet.Controls.Add(this.cbERV_HRV);
            this.groupBoxCabinet.Controls.Add(this.label151);
            this.groupBoxCabinet.Controls.Add(this.cbPoweredExhaustFanMotorHP);
            this.groupBoxCabinet.Controls.Add(this.cbPoweredExhaustFanWheel);
            this.groupBoxCabinet.Controls.Add(this.label14);
            this.groupBoxCabinet.Controls.Add(this.cbPoweredExhaustFanMotor);
            this.groupBoxCabinet.Controls.Add(this.label12);
            this.groupBoxCabinet.Controls.Add(this.cbUnitControls);
            this.groupBoxCabinet.Controls.Add(this.label11);
            this.groupBoxCabinet.Controls.Add(this.cbCorrosiveEnvPackage);
            this.groupBoxCabinet.Controls.Add(this.label10);
            this.groupBoxCabinet.Controls.Add(this.label9);
            this.groupBoxCabinet.Controls.Add(this.labelInputsHeatCapacityLabel);
            this.groupBoxCabinet.Controls.Add(this.cbFuelType);
            this.groupBoxCabinet.Controls.Add(this.label8);
            this.groupBoxCabinet.Controls.Add(this.label147);
            this.groupBoxCabinet.Controls.Add(this.cbIndoorFanMotorHP);
            this.groupBoxCabinet.Controls.Add(this.label7);
            this.groupBoxCabinet.Controls.Add(this.label146);
            this.groupBoxCabinet.Controls.Add(this.cbIndoorFanWheel);
            this.groupBoxCabinet.Controls.Add(this.label6);
            this.groupBoxCabinet.Controls.Add(this.label145);
            this.groupBoxCabinet.Controls.Add(this.cbIndoorFanMotor);
            this.groupBoxCabinet.Controls.Add(this.label5);
            this.groupBoxCabinet.Controls.Add(this.label144);
            this.groupBoxCabinet.Controls.Add(this.cbRefridgerantCapacityControl);
            this.groupBoxCabinet.Controls.Add(this.label143);
            this.groupBoxCabinet.Controls.Add(this.label1);
            this.groupBoxCabinet.Controls.Add(this.label13);
            this.groupBoxCabinet.Controls.Add(this.cbEvapType);
            this.groupBoxCabinet.Controls.Add(this.cbCoolingCapacity);
            this.groupBoxCabinet.Controls.Add(this.label18);
            this.groupBoxCabinet.Controls.Add(this.label142);
            this.groupBoxCabinet.Controls.Add(this.cbFiltrationOptions);
            this.groupBoxCabinet.Controls.Add(this.label141);
            this.groupBoxCabinet.Controls.Add(this.cbCondenser);
            this.groupBoxCabinet.Controls.Add(this.label21);
            this.groupBoxCabinet.Controls.Add(this.label140);
            this.groupBoxCabinet.Controls.Add(this.cbHotGasReheat);
            this.groupBoxCabinet.Controls.Add(this.label20);
            this.groupBoxCabinet.Controls.Add(this.cbCompressor);
            this.groupBoxCabinet.Controls.Add(this.label139);
            this.groupBoxCabinet.Controls.Add(this.cbHeatType);
            this.groupBoxCabinet.Controls.Add(this.label138);
            this.groupBoxCabinet.Controls.Add(this.label19);
            this.groupBoxCabinet.Controls.Add(this.label134);
            this.groupBoxCabinet.Controls.Add(this.label137);
            this.groupBoxCabinet.Controls.Add(this.cbVoltage);
            this.groupBoxCabinet.Controls.Add(this.label135);
            this.groupBoxCabinet.Controls.Add(this.cbCabinet);
            this.groupBoxCabinet.Controls.Add(this.label136);
            this.groupBoxCabinet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxCabinet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBoxCabinet.Location = new System.Drawing.Point(12, 210);
            this.groupBoxCabinet.Name = "groupBoxCabinet";
            this.groupBoxCabinet.Size = new System.Drawing.Size(974, 504);
            this.groupBoxCabinet.TabIndex = 466;
            this.groupBoxCabinet.TabStop = false;
            this.groupBoxCabinet.Text = "Inputs";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(488, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(253, 20);
            this.label15.TabIndex = 118;
            this.label15.Text = "Trane PPS Compatibility (30)";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbTranePPS_Compatibility
            // 
            this.cbTranePPS_Compatibility.BackColor = System.Drawing.Color.White;
            this.cbTranePPS_Compatibility.DropDownWidth = 300;
            this.cbTranePPS_Compatibility.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTranePPS_Compatibility.ForeColor = System.Drawing.Color.Black;
            this.cbTranePPS_Compatibility.FormattingEnabled = true;
            this.cbTranePPS_Compatibility.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbTranePPS_Compatibility.Location = new System.Drawing.Point(747, 122);
            this.cbTranePPS_Compatibility.Name = "cbTranePPS_Compatibility";
            this.cbTranePPS_Compatibility.Size = new System.Drawing.Size(214, 21);
            this.cbTranePPS_Compatibility.TabIndex = 119;
            // 
            // gbHotWaterInputs
            // 
            this.gbHotWaterInputs.Controls.Add(this.label209);
            this.gbHotWaterInputs.Controls.Add(this.label208);
            this.gbHotWaterInputs.Controls.Add(this.txtEnteringTemp);
            this.gbHotWaterInputs.Controls.Add(this.txtFlowRate);
            this.gbHotWaterInputs.Location = new System.Drawing.Point(729, 382);
            this.gbHotWaterInputs.Name = "gbHotWaterInputs";
            this.gbHotWaterInputs.Size = new System.Drawing.Size(231, 89);
            this.gbHotWaterInputs.TabIndex = 117;
            this.gbHotWaterInputs.TabStop = false;
            this.gbHotWaterInputs.Text = "Hot Water Inputs";
            this.gbHotWaterInputs.Visible = false;
            // 
            // label209
            // 
            this.label209.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label209.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label209.Location = new System.Drawing.Point(12, 54);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(101, 20);
            this.label209.TabIndex = 100;
            this.label209.Text = "Entering Temp:";
            this.label209.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label208
            // 
            this.label208.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label208.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label208.Location = new System.Drawing.Point(12, 22);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(101, 20);
            this.label208.TabIndex = 99;
            this.label208.Text = "Flow Rate:";
            this.label208.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEnteringTemp
            // 
            this.txtEnteringTemp.Location = new System.Drawing.Point(120, 54);
            this.txtEnteringTemp.Name = "txtEnteringTemp";
            this.txtEnteringTemp.Size = new System.Drawing.Size(100, 21);
            this.txtEnteringTemp.TabIndex = 1;
            // 
            // txtFlowRate
            // 
            this.txtFlowRate.Location = new System.Drawing.Point(120, 20);
            this.txtFlowRate.Name = "txtFlowRate";
            this.txtFlowRate.Size = new System.Drawing.Size(100, 21);
            this.txtFlowRate.TabIndex = 0;
            // 
            // cbHeatSourceSecondary
            // 
            this.cbHeatSourceSecondary.BackColor = System.Drawing.Color.White;
            this.cbHeatSourceSecondary.DropDownWidth = 300;
            this.cbHeatSourceSecondary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatSourceSecondary.ForeColor = System.Drawing.Color.Black;
            this.cbHeatSourceSecondary.FormattingEnabled = true;
            this.cbHeatSourceSecondary.Location = new System.Drawing.Point(269, 438);
            this.cbHeatSourceSecondary.Name = "cbHeatSourceSecondary";
            this.cbHeatSourceSecondary.Size = new System.Drawing.Size(213, 21);
            this.cbHeatSourceSecondary.TabIndex = 81;
            // 
            // cbHeatSourcePrimary
            // 
            this.cbHeatSourcePrimary.BackColor = System.Drawing.Color.White;
            this.cbHeatSourcePrimary.DropDownWidth = 300;
            this.cbHeatSourcePrimary.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatSourcePrimary.ForeColor = System.Drawing.Color.Black;
            this.cbHeatSourcePrimary.FormattingEnabled = true;
            this.cbHeatSourcePrimary.Location = new System.Drawing.Point(269, 411);
            this.cbHeatSourcePrimary.Name = "cbHeatSourcePrimary";
            this.cbHeatSourcePrimary.Size = new System.Drawing.Size(210, 21);
            this.cbHeatSourcePrimary.TabIndex = 80;
            // 
            // cbMajorDesign
            // 
            this.cbMajorDesign.BackColor = System.Drawing.Color.White;
            this.cbMajorDesign.DropDownWidth = 300;
            this.cbMajorDesign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMajorDesign.ForeColor = System.Drawing.Color.Black;
            this.cbMajorDesign.FormattingEnabled = true;
            this.cbMajorDesign.Items.AddRange(new object[] {
            "Rev4",
            "Rev5",
            "Heat Pump"});
            this.cbMajorDesign.Location = new System.Drawing.Point(269, 43);
            this.cbMajorDesign.Name = "cbMajorDesign";
            this.cbMajorDesign.Size = new System.Drawing.Size(210, 21);
            this.cbMajorDesign.TabIndex = 78;
            // 
            // label133
            // 
            this.label133.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.Location = new System.Drawing.Point(7, 43);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(253, 20);
            this.label133.TabIndex = 77;
            this.label133.Text = "Major Design (4)";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbMinorDesign
            // 
            this.cbMinorDesign.BackColor = System.Drawing.Color.White;
            this.cbMinorDesign.DropDownWidth = 300;
            this.cbMinorDesign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMinorDesign.ForeColor = System.Drawing.Color.Black;
            this.cbMinorDesign.FormattingEnabled = true;
            this.cbMinorDesign.Items.AddRange(new object[] {
            "Vertical Discharge/Vertical Return",
            "Vertical Discharge/ Horizontal Return",
            "Horizontal Discharge/ Vertical Return",
            "Horizontal Discharge/ Horizontal Return"});
            this.cbMinorDesign.Location = new System.Drawing.Point(269, 95);
            this.cbMinorDesign.Name = "cbMinorDesign";
            this.cbMinorDesign.Size = new System.Drawing.Size(210, 21);
            this.cbMinorDesign.TabIndex = 76;
            // 
            // label132
            // 
            this.label132.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(7, 95);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(253, 20);
            this.label132.TabIndex = 75;
            this.label132.Text = "Minor Design  (8)";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label159
            // 
            this.label159.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(488, 356);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(253, 20);
            this.label159.TabIndex = 73;
            this.label159.Text = "Altitude (39)";
            this.label159.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAltitude
            // 
            this.cbAltitude.BackColor = System.Drawing.Color.White;
            this.cbAltitude.DropDownWidth = 300;
            this.cbAltitude.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAltitude.ForeColor = System.Drawing.Color.Black;
            this.cbAltitude.FormattingEnabled = true;
            this.cbAltitude.Items.AddRange(new object[] {
            "Sea Level  to 1,000 feet",
            "1,001 to 2,000 feet",
            "2,001 to 3,000 feet",
            "3,001 to 4,000 feet",
            "4,001 to 5,000 feet",
            "5,001 to 6,000 feet",
            "6,001 to 7,000 feet",
            "Above 7,000 feet"});
            this.cbAltitude.Location = new System.Drawing.Point(747, 356);
            this.cbAltitude.Name = "cbAltitude";
            this.cbAltitude.Size = new System.Drawing.Size(214, 21);
            this.cbAltitude.TabIndex = 74;
            // 
            // label158
            // 
            this.label158.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.Location = new System.Drawing.Point(488, 329);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(253, 20);
            this.label158.TabIndex = 71;
            this.label158.Text = "Hailguards (38)";
            this.label158.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHailguard
            // 
            this.cbHailguard.BackColor = System.Drawing.Color.White;
            this.cbHailguard.DropDownWidth = 300;
            this.cbHailguard.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHailguard.ForeColor = System.Drawing.Color.Black;
            this.cbHailguard.FormattingEnabled = true;
            this.cbHailguard.Items.AddRange(new object[] {
            "No Hailguards",
            "Hailguards"});
            this.cbHailguard.Location = new System.Drawing.Point(747, 329);
            this.cbHailguard.Name = "cbHailguard";
            this.cbHailguard.Size = new System.Drawing.Size(214, 21);
            this.cbHailguard.TabIndex = 72;
            // 
            // label157
            // 
            this.label157.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.Location = new System.Drawing.Point(488, 303);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(253, 20);
            this.label157.TabIndex = 69;
            this.label157.Text = "Air Flow Monitoring (37)";
            this.label157.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAirFlowMonitoring
            // 
            this.cbAirFlowMonitoring.BackColor = System.Drawing.Color.White;
            this.cbAirFlowMonitoring.DropDownWidth = 300;
            this.cbAirFlowMonitoring.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAirFlowMonitoring.ForeColor = System.Drawing.Color.Black;
            this.cbAirFlowMonitoring.FormattingEnabled = true;
            this.cbAirFlowMonitoring.Items.AddRange(new object[] {
            "No Airflow Monitoring",
            "Airflow Monitoring - IFM Fan Piezo Ring",
            "Airflow Monitoring - IFM with Display",
            "Airflow Monitoring - PE with Display",
            "Airflow Monitoring - Outdoor Air",
            "Airflow Monitoring - Outdoor Air with Display"});
            this.cbAirFlowMonitoring.Location = new System.Drawing.Point(747, 303);
            this.cbAirFlowMonitoring.Name = "cbAirFlowMonitoring";
            this.cbAirFlowMonitoring.Size = new System.Drawing.Size(214, 21);
            this.cbAirFlowMonitoring.TabIndex = 70;
            // 
            // label156
            // 
            this.label156.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(488, 277);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(253, 20);
            this.label156.TabIndex = 67;
            this.label156.Text = "Electrical Options (36)";
            this.label156.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbElectricalOptions
            // 
            this.cbElectricalOptions.BackColor = System.Drawing.Color.White;
            this.cbElectricalOptions.DropDownWidth = 300;
            this.cbElectricalOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbElectricalOptions.ForeColor = System.Drawing.Color.Black;
            this.cbElectricalOptions.FormattingEnabled = true;
            this.cbElectricalOptions.Items.AddRange(new object[] {
            "Non-Fused Disconnect",
            "Fused Disconnect Switch",
            "Non-Fused Disconnect w/Convenience Outlet",
            "Fused Disconnect Switch w/Convenience Outlet",
            "Dual Point Power"});
            this.cbElectricalOptions.Location = new System.Drawing.Point(747, 277);
            this.cbElectricalOptions.Name = "cbElectricalOptions";
            this.cbElectricalOptions.Size = new System.Drawing.Size(214, 21);
            this.cbElectricalOptions.TabIndex = 68;
            // 
            // label155
            // 
            this.label155.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.Location = new System.Drawing.Point(488, 251);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(253, 20);
            this.label155.TabIndex = 65;
            this.label155.Text = "Smoke Detector - Factory Installed (35)";
            this.label155.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSmokeDetector
            // 
            this.cbSmokeDetector.BackColor = System.Drawing.Color.White;
            this.cbSmokeDetector.DropDownWidth = 300;
            this.cbSmokeDetector.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSmokeDetector.ForeColor = System.Drawing.Color.Black;
            this.cbSmokeDetector.FormattingEnabled = true;
            this.cbSmokeDetector.Items.AddRange(new object[] {
            "No Smoke Detector",
            "Supply Smoke Detector",
            "Return Smoke Detector",
            "Supply & Return Detector"});
            this.cbSmokeDetector.Location = new System.Drawing.Point(747, 251);
            this.cbSmokeDetector.Name = "cbSmokeDetector";
            this.cbSmokeDetector.Size = new System.Drawing.Size(214, 21);
            this.cbSmokeDetector.TabIndex = 66;
            // 
            // label154
            // 
            this.label154.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.Location = new System.Drawing.Point(488, 199);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(253, 20);
            this.label154.TabIndex = 63;
            this.label154.Text = "Economizer (33)";
            this.label154.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEconomizer
            // 
            this.cbEconomizer.BackColor = System.Drawing.Color.White;
            this.cbEconomizer.DropDownWidth = 300;
            this.cbEconomizer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEconomizer.ForeColor = System.Drawing.Color.Black;
            this.cbEconomizer.FormattingEnabled = true;
            this.cbEconomizer.Items.AddRange(new object[] {
            "100 % OA 2-Position Damper",
            "100 % OA 2-Position Damper w/RA 2-Position Damper",
            "Modulating Mixed Air Damper"});
            this.cbEconomizer.Location = new System.Drawing.Point(747, 199);
            this.cbEconomizer.Name = "cbEconomizer";
            this.cbEconomizer.Size = new System.Drawing.Size(214, 21);
            this.cbEconomizer.TabIndex = 64;
            // 
            // label153
            // 
            this.label153.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.Location = new System.Drawing.Point(488, 173);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(253, 20);
            this.label153.TabIndex = 61;
            this.label153.Text = "ERV Size (32)";
            this.label153.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbERVSize
            // 
            this.cbERVSize.BackColor = System.Drawing.Color.White;
            this.cbERVSize.DropDownWidth = 300;
            this.cbERVSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbERVSize.ForeColor = System.Drawing.Color.Black;
            this.cbERVSize.FormattingEnabled = true;
            this.cbERVSize.Items.AddRange(new object[] {
            "No ERV",
            "3014C",
            "3622C",
            "4136C",
            "4634C",
            "5856C",
            "6488C",
            "6876C",
            "74122C"});
            this.cbERVSize.Location = new System.Drawing.Point(747, 173);
            this.cbERVSize.Name = "cbERVSize";
            this.cbERVSize.Size = new System.Drawing.Size(214, 21);
            this.cbERVSize.TabIndex = 62;
            // 
            // label152
            // 
            this.label152.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.Location = new System.Drawing.Point(488, 147);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(253, 20);
            this.label152.TabIndex = 59;
            this.label152.Text = "ERV/HRV (Requires Powered Exhaust) (31)";
            this.label152.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbERV_HRV
            // 
            this.cbERV_HRV.BackColor = System.Drawing.Color.White;
            this.cbERV_HRV.DropDownWidth = 300;
            this.cbERV_HRV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbERV_HRV.ForeColor = System.Drawing.Color.Black;
            this.cbERV_HRV.FormattingEnabled = true;
            this.cbERV_HRV.Items.AddRange(new object[] {
            "No ERV",
            "ERV-Composite Contruction",
            "ERV-Composite Contruction with Frost Protection",
            "ERV-Composite Contruction with Bypass",
            "ERV-Composite Contruction with Frost Protection & Bypass",
            "ERV-Aluminum Contruction",
            "ERV-Aluminum Contruction with Frost Protection",
            "ERV-Aluminum Contruction with Bypass",
            "ERV-Aluminum Contruction with Frost Protection & Bypass"});
            this.cbERV_HRV.Location = new System.Drawing.Point(747, 147);
            this.cbERV_HRV.Name = "cbERV_HRV";
            this.cbERV_HRV.Size = new System.Drawing.Size(214, 21);
            this.cbERV_HRV.TabIndex = 60;
            // 
            // label151
            // 
            this.label151.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.Location = new System.Drawing.Point(488, 95);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(253, 20);
            this.label151.TabIndex = 57;
            this.label151.Text = "Powered Exhaust Fan Motor HP (29)";
            this.label151.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPoweredExhaustFanMotorHP
            // 
            this.cbPoweredExhaustFanMotorHP.BackColor = System.Drawing.Color.White;
            this.cbPoweredExhaustFanMotorHP.DropDownWidth = 300;
            this.cbPoweredExhaustFanMotorHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPoweredExhaustFanMotorHP.ForeColor = System.Drawing.Color.Black;
            this.cbPoweredExhaustFanMotorHP.FormattingEnabled = true;
            this.cbPoweredExhaustFanMotorHP.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbPoweredExhaustFanMotorHP.Location = new System.Drawing.Point(747, 95);
            this.cbPoweredExhaustFanMotorHP.Name = "cbPoweredExhaustFanMotorHP";
            this.cbPoweredExhaustFanMotorHP.Size = new System.Drawing.Size(214, 21);
            this.cbPoweredExhaustFanMotorHP.TabIndex = 58;
            // 
            // cbPoweredExhaustFanWheel
            // 
            this.cbPoweredExhaustFanWheel.BackColor = System.Drawing.Color.White;
            this.cbPoweredExhaustFanWheel.DropDownWidth = 300;
            this.cbPoweredExhaustFanWheel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPoweredExhaustFanWheel.ForeColor = System.Drawing.Color.Black;
            this.cbPoweredExhaustFanWheel.FormattingEnabled = true;
            this.cbPoweredExhaustFanWheel.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbPoweredExhaustFanWheel.Location = new System.Drawing.Point(747, 69);
            this.cbPoweredExhaustFanWheel.Name = "cbPoweredExhaustFanWheel";
            this.cbPoweredExhaustFanWheel.Size = new System.Drawing.Size(214, 21);
            this.cbPoweredExhaustFanWheel.TabIndex = 56;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(488, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(253, 20);
            this.label14.TabIndex = 55;
            this.label14.Text = "Powered Exhaust Fan Wheel (28)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPoweredExhaustFanMotor
            // 
            this.cbPoweredExhaustFanMotor.BackColor = System.Drawing.Color.White;
            this.cbPoweredExhaustFanMotor.DropDownWidth = 300;
            this.cbPoweredExhaustFanMotor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPoweredExhaustFanMotor.ForeColor = System.Drawing.Color.Black;
            this.cbPoweredExhaustFanMotor.FormattingEnabled = true;
            this.cbPoweredExhaustFanMotor.Items.AddRange(new object[] {
            "No Powered Exhaust",
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbPoweredExhaustFanMotor.Location = new System.Drawing.Point(747, 43);
            this.cbPoweredExhaustFanMotor.Name = "cbPoweredExhaustFanMotor";
            this.cbPoweredExhaustFanMotor.Size = new System.Drawing.Size(214, 21);
            this.cbPoweredExhaustFanMotor.TabIndex = 54;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(488, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(253, 20);
            this.label12.TabIndex = 53;
            this.label12.Text = "Powered Exhaust Fan Motor (PFM) (27)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitControls
            // 
            this.cbUnitControls.BackColor = System.Drawing.Color.White;
            this.cbUnitControls.DropDownWidth = 300;
            this.cbUnitControls.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUnitControls.ForeColor = System.Drawing.Color.Black;
            this.cbUnitControls.FormattingEnabled = true;
            this.cbUnitControls.Items.AddRange(new object[] {
            "Non DDC - Electromechanical",
            "Trane - Outdoor Air Control w/LON Read-Write w/Display",
            "Trane - Space Control w/LON Read-Write w/Display",
            "Trane - Outdoor Air Control w/BACNET (No Display)",
            "Trane - Space Control w/BACNET  (No Display)",
            "Trane - Discharge Air Control w/BACNET  (No Display)",
            "Trane - Outdoor Air Control w/BACNET  w/Display",
            "Trane - Space Control w/BACNET w/Display",
            "Trane - Discharge Air Control w/BACNET w/Display"});
            this.cbUnitControls.Location = new System.Drawing.Point(747, 17);
            this.cbUnitControls.Name = "cbUnitControls";
            this.cbUnitControls.Size = new System.Drawing.Size(214, 21);
            this.cbUnitControls.TabIndex = 52;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(488, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(253, 20);
            this.label11.TabIndex = 51;
            this.label11.Text = "Unit Controls (25,26)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCorrosiveEnvPackage
            // 
            this.cbCorrosiveEnvPackage.BackColor = System.Drawing.Color.White;
            this.cbCorrosiveEnvPackage.DropDownWidth = 300;
            this.cbCorrosiveEnvPackage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCorrosiveEnvPackage.ForeColor = System.Drawing.Color.Black;
            this.cbCorrosiveEnvPackage.FormattingEnabled = true;
            this.cbCorrosiveEnvPackage.Items.AddRange(new object[] {
            "No Corrosive Package",
            "S/S Cabinet, Basepan, Eco Coated Coils",
            "S/S Cabinet, Basepan",
            "S/S Basepan, Eco Coated Coils",
            "S/S Coil Casing",
            "S/S Interior Casing",
            "Eco Coated Coils"});
            this.cbCorrosiveEnvPackage.Location = new System.Drawing.Point(269, 465);
            this.cbCorrosiveEnvPackage.Name = "cbCorrosiveEnvPackage";
            this.cbCorrosiveEnvPackage.Size = new System.Drawing.Size(214, 21);
            this.cbCorrosiveEnvPackage.TabIndex = 50;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 465);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(253, 20);
            this.label10.TabIndex = 49;
            this.label10.Text = "Corrosive Environment Package (24)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 438);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(253, 23);
            this.label9.TabIndex = 44;
            this.label9.Text = "Heat Capacity-Secondary Heat Source (23)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelInputsHeatCapacityLabel
            // 
            this.labelInputsHeatCapacityLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInputsHeatCapacityLabel.Location = new System.Drawing.Point(7, 411);
            this.labelInputsHeatCapacityLabel.Name = "labelInputsHeatCapacityLabel";
            this.labelInputsHeatCapacityLabel.Size = new System.Drawing.Size(253, 23);
            this.labelInputsHeatCapacityLabel.TabIndex = 40;
            this.labelInputsHeatCapacityLabel.Text = "Heat Capacity-Primary Heat Source (22)";
            this.labelInputsHeatCapacityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbFuelType
            // 
            this.cbFuelType.BackColor = System.Drawing.Color.White;
            this.cbFuelType.DropDownWidth = 300;
            this.cbFuelType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFuelType.ForeColor = System.Drawing.Color.Black;
            this.cbFuelType.FormattingEnabled = true;
            this.cbFuelType.Items.AddRange(new object[] {
            "No Heat",
            "Natural Gas",
            "Propane",
            "Electric - Open Coil",
            "Electric - Sheathed Coil",
            "Hot Water",
            "Steam"});
            this.cbFuelType.Location = new System.Drawing.Point(269, 383);
            this.cbFuelType.Name = "cbFuelType";
            this.cbFuelType.Size = new System.Drawing.Size(210, 21);
            this.cbFuelType.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 383);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(253, 23);
            this.label8.TabIndex = 38;
            this.label8.Text = "Fuel Type (21)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label147
            // 
            this.label147.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.Location = new System.Drawing.Point(7, 330);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(253, 20);
            this.label147.TabIndex = 36;
            this.label147.Text = "Indoor Fan Motor HP (18)";
            this.label147.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbIndoorFanMotorHP
            // 
            this.cbIndoorFanMotorHP.BackColor = System.Drawing.Color.White;
            this.cbIndoorFanMotorHP.DropDownWidth = 300;
            this.cbIndoorFanMotorHP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIndoorFanMotorHP.ForeColor = System.Drawing.Color.Black;
            this.cbIndoorFanMotorHP.FormattingEnabled = true;
            this.cbIndoorFanMotorHP.Items.AddRange(new object[] {
            "1/2 HP - 1800 RPM",
            "1/2 HP - 3600 RPM",
            "3/4 HP - 1800 RPM",
            "3/4 HP - 3600 RPM",
            "1 HP - 1800 RPM",
            "1 HP - 3600 RPM",
            "1.5 HP - 1800 RPM",
            "1.5 HP - 3600 RPM",
            "2 HP - 1800 RPM",
            "2 HP - 3600 RPM",
            "3 HP - 1800 RPM",
            "3 HP - 3600 RPM",
            "5 HP - 1800 RPM",
            "5 HP - 3600 RPM",
            "7.5 HP - 1800 RPM",
            "7.5 HP - 3600 RPM",
            "10 HP - 1800 RPM",
            "10 HP - 3600 RPM",
            "15 HP - 1800 RPM",
            "15 HP - 3600 RPM"});
            this.cbIndoorFanMotorHP.Location = new System.Drawing.Point(269, 330);
            this.cbIndoorFanMotorHP.Name = "cbIndoorFanMotorHP";
            this.cbIndoorFanMotorHP.Size = new System.Drawing.Size(210, 21);
            this.cbIndoorFanMotorHP.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 330);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(253, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Indoor Fan Motor HP (18)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label146
            // 
            this.label146.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.Location = new System.Drawing.Point(7, 304);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(253, 20);
            this.label146.TabIndex = 34;
            this.label146.Text = "Indoor Fan Wheel (17)";
            this.label146.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbIndoorFanWheel
            // 
            this.cbIndoorFanWheel.BackColor = System.Drawing.Color.White;
            this.cbIndoorFanWheel.DropDownWidth = 300;
            this.cbIndoorFanWheel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIndoorFanWheel.ForeColor = System.Drawing.Color.Black;
            this.cbIndoorFanWheel.FormattingEnabled = true;
            this.cbIndoorFanWheel.Items.AddRange(new object[] {
            "122",
            "122.6",
            "150",
            "150.6",
            "165",
            "165.6",
            "182",
            "182.6",
            "200",
            "200.6",
            "182 X 2",
            "182.6 X 2"});
            this.cbIndoorFanWheel.Location = new System.Drawing.Point(269, 304);
            this.cbIndoorFanWheel.Name = "cbIndoorFanWheel";
            this.cbIndoorFanWheel.Size = new System.Drawing.Size(210, 21);
            this.cbIndoorFanWheel.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 304);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(253, 20);
            this.label6.TabIndex = 34;
            this.label6.Text = "Indoor Fan Wheel (17)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label145
            // 
            this.label145.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(7, 278);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(253, 20);
            this.label145.TabIndex = 32;
            this.label145.Text = "Indoor Fan Motor (IFM) (16)";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbIndoorFanMotor
            // 
            this.cbIndoorFanMotor.BackColor = System.Drawing.Color.White;
            this.cbIndoorFanMotor.DropDownWidth = 300;
            this.cbIndoorFanMotor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIndoorFanMotor.ForeColor = System.Drawing.Color.Black;
            this.cbIndoorFanMotor.FormattingEnabled = true;
            this.cbIndoorFanMotor.Items.AddRange(new object[] {
            "Direct Drive w/VFD",
            "Direct Drive (VFD by others)",
            "Belt Drive",
            "Belt Drive w/VFD",
            "Special Motor Option"});
            this.cbIndoorFanMotor.Location = new System.Drawing.Point(269, 278);
            this.cbIndoorFanMotor.Name = "cbIndoorFanMotor";
            this.cbIndoorFanMotor.Size = new System.Drawing.Size(210, 21);
            this.cbIndoorFanMotor.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(253, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Indoor Fan Motor (IFM) (16)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(7, 252);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(253, 20);
            this.label144.TabIndex = 30;
            this.label144.Text = "Refrigerant Capacity Control (15)";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbRefridgerantCapacityControl
            // 
            this.cbRefridgerantCapacityControl.BackColor = System.Drawing.Color.White;
            this.cbRefridgerantCapacityControl.DropDownWidth = 300;
            this.cbRefridgerantCapacityControl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRefridgerantCapacityControl.ForeColor = System.Drawing.Color.Black;
            this.cbRefridgerantCapacityControl.FormattingEnabled = true;
            this.cbRefridgerantCapacityControl.Items.AddRange(new object[] {
            "No RCC Valve",
            "RCC Valve on 1st Circuit",
            "RCC Valve on 1st Circuit & 2nd Circuit",
            "ERCC Valve on 1st Circuit",
            "ERCC Valve on 1st Circuit & 2nd Circuit",
            "HGBP Valve on 1st Circuit",
            "HGBP Valve on 1st Circuit & 2nd Circuit"});
            this.cbRefridgerantCapacityControl.Location = new System.Drawing.Point(269, 252);
            this.cbRefridgerantCapacityControl.Name = "cbRefridgerantCapacityControl";
            this.cbRefridgerantCapacityControl.Size = new System.Drawing.Size(210, 21);
            this.cbRefridgerantCapacityControl.TabIndex = 31;
            // 
            // label143
            // 
            this.label143.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(7, 148);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(253, 20);
            this.label143.TabIndex = 29;
            this.label143.Text = "Evaporator Type (11)";
            this.label143.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 252);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 20);
            this.label1.TabIndex = 30;
            this.label1.Text = "Refrigerant Capacity Control (15)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(7, 148);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(253, 20);
            this.label13.TabIndex = 29;
            this.label13.Text = "Evaporator Type (11)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbEvapType
            // 
            this.cbEvapType.BackColor = System.Drawing.Color.White;
            this.cbEvapType.DropDownWidth = 300;
            this.cbEvapType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEvapType.ForeColor = System.Drawing.Color.Black;
            this.cbEvapType.FormattingEnabled = true;
            this.cbEvapType.Items.AddRange(new object[] {
            "No Cooling",
            "DX 3-Row",
            "DX 4-Row",
            "DX 4-Row Interlaced",
            "DX 6-Row Interlaced",
            "DX 8-Row",
            "Glycol/Chilled Water Coil"});
            this.cbEvapType.Location = new System.Drawing.Point(269, 148);
            this.cbEvapType.Name = "cbEvapType";
            this.cbEvapType.Size = new System.Drawing.Size(210, 21);
            this.cbEvapType.TabIndex = 28;
            // 
            // cbCoolingCapacity
            // 
            this.cbCoolingCapacity.BackColor = System.Drawing.Color.White;
            this.cbCoolingCapacity.DropDownWidth = 300;
            this.cbCoolingCapacity.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCoolingCapacity.ForeColor = System.Drawing.Color.Black;
            this.cbCoolingCapacity.FormattingEnabled = true;
            this.cbCoolingCapacity.Items.AddRange(new object[] {
            "No Cooling",
            "5 Tons High Efficiency",
            "6 Tons High Efficiency",
            "7 Tons High Efficiency",
            "8 Tons High Efficiency",
            "10 Tons High Efficiency",
            "12 Tons High Efficiency",
            "15 Tons High Efficiency",
            "17 Tons High Efficiency",
            "20 Tons High Efficiency",
            "22 Tons High Efficiency",
            "25 Tons High Efficiency",
            "30 Tons High Efficiency",
            "35 Tons High Efficiency",
            "40 Tons High Efficiency",
            "45 Tons High Efficiency",
            "50 Tons High Efficiency",
            "54 Tons High Efficiency"});
            this.cbCoolingCapacity.Location = new System.Drawing.Point(269, 69);
            this.cbCoolingCapacity.Name = "cbCoolingCapacity";
            this.cbCoolingCapacity.Size = new System.Drawing.Size(210, 21);
            this.cbCoolingCapacity.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(7, 69);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(253, 20);
            this.label18.TabIndex = 25;
            this.label18.Text = "Normal Gross Cooling Capacity (567)";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label142
            // 
            this.label142.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.Location = new System.Drawing.Point(488, 224);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(253, 20);
            this.label142.TabIndex = 21;
            this.label142.Text = "Filtration Options (34)";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbFiltrationOptions
            // 
            this.cbFiltrationOptions.BackColor = System.Drawing.Color.White;
            this.cbFiltrationOptions.DropDownWidth = 300;
            this.cbFiltrationOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltrationOptions.ForeColor = System.Drawing.Color.Black;
            this.cbFiltrationOptions.FormattingEnabled = true;
            this.cbFiltrationOptions.Items.AddRange(new object[] {
            "Aluminum Mesh Intake Filters (ALM)",
            "MERV-8, 30%, and ALM",
            "MERV-13, 80%, and ALM",
            "MERV-14, 95%, and ALM",
            "Aluminum Mesh Intake Filters (ALM), w/UVC",
            "MERV-8, 30%, and ALM, w/UVC",
            "MERV-13, 80%, and ALM, w/UVC",
            "MERV-14, 95%, and ALM, w/UVC",
            "Aluminum Mesh Intake Filters (ALM), w/UVC & Electrostatic Filters",
            "MERV-8, 30%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-13, 80%, and ALM, w/UVC & Electrostatic Filters",
            "MERV-14, 95%, and ALM, w/UVC & Electrostatic Filters",
            "Aluminum Mesh Intake Filters (ALM) & Electrostatic Filters",
            "MERV-8, 30%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM & Electrostatic Filters",
            "MERV-14, 95%, and ALM & Electrostatic Filters",
            "MERV-13, 80%, and ALM w/TCACS",
            "MERV-14, 95%, and ALM w/TCACS",
            "Special Filter Options"});
            this.cbFiltrationOptions.Location = new System.Drawing.Point(747, 224);
            this.cbFiltrationOptions.Name = "cbFiltrationOptions";
            this.cbFiltrationOptions.Size = new System.Drawing.Size(214, 21);
            this.cbFiltrationOptions.TabIndex = 22;
            // 
            // label141
            // 
            this.label141.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.Location = new System.Drawing.Point(7, 226);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(253, 20);
            this.label141.TabIndex = 17;
            this.label141.Text = "Condenser (14)";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCondenser
            // 
            this.cbCondenser.BackColor = System.Drawing.Color.White;
            this.cbCondenser.DropDownWidth = 300;
            this.cbCondenser.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCondenser.ForeColor = System.Drawing.Color.Black;
            this.cbCondenser.FormattingEnabled = true;
            this.cbCondenser.Items.AddRange(new object[] {
            "No Condenser",
            "Air Cooled",
            "Air Cooled w/Head Pressure on/off control",
            "Water Source Heat Pump",
            "Air Cooled Fin & Tube w/ Head Pressure Variable Speed",
            "Air Cooled Micro Channel",
            "Air Cooled Micro Channel w/Head Pressure on/off control",
            "Air Cooled Micro Channel Variable Speed",
            "Water Cooled DX Condenser Copper/Nickel"});
            this.cbCondenser.Location = new System.Drawing.Point(269, 226);
            this.cbCondenser.Name = "cbCondenser";
            this.cbCondenser.Size = new System.Drawing.Size(210, 21);
            this.cbCondenser.TabIndex = 18;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 226);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(253, 20);
            this.label21.TabIndex = 17;
            this.label21.Text = "Condenser (14)";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label140
            // 
            this.label140.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.Location = new System.Drawing.Point(7, 174);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(253, 20);
            this.label140.TabIndex = 15;
            this.label140.Text = "Hot Gas Reheat (12)";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHotGasReheat
            // 
            this.cbHotGasReheat.BackColor = System.Drawing.Color.White;
            this.cbHotGasReheat.DropDownWidth = 300;
            this.cbHotGasReheat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHotGasReheat.ForeColor = System.Drawing.Color.Black;
            this.cbHotGasReheat.FormattingEnabled = true;
            this.cbHotGasReheat.Items.AddRange(new object[] {
            "No HGRH",
            "Modulating",
            "On/Off"});
            this.cbHotGasReheat.Location = new System.Drawing.Point(269, 174);
            this.cbHotGasReheat.Name = "cbHotGasReheat";
            this.cbHotGasReheat.Size = new System.Drawing.Size(210, 21);
            this.cbHotGasReheat.TabIndex = 16;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(7, 174);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(253, 20);
            this.label20.TabIndex = 15;
            this.label20.Text = "Hot Gas Reheat (12)";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCompressor
            // 
            this.cbCompressor.BackColor = System.Drawing.Color.White;
            this.cbCompressor.DropDownWidth = 300;
            this.cbCompressor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCompressor.ForeColor = System.Drawing.Color.Black;
            this.cbCompressor.FormattingEnabled = true;
            this.cbCompressor.Items.AddRange(new object[] {
            "No Compressor",
            "Scroll Compressor",
            "Digital Scroll-1st Circuit Only",
            "Digital Scroll-1st Circuit & 2nd Circuit",
            "Variable Speed Scroll"});
            this.cbCompressor.Location = new System.Drawing.Point(269, 200);
            this.cbCompressor.Name = "cbCompressor";
            this.cbCompressor.Size = new System.Drawing.Size(210, 21);
            this.cbCompressor.TabIndex = 14;
            // 
            // label139
            // 
            this.label139.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.Location = new System.Drawing.Point(7, 200);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(253, 20);
            this.label139.TabIndex = 13;
            this.label139.Text = "Compressor (13)";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbHeatType
            // 
            this.cbHeatType.BackColor = System.Drawing.Color.White;
            this.cbHeatType.DropDownWidth = 300;
            this.cbHeatType.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHeatType.ForeColor = System.Drawing.Color.Black;
            this.cbHeatType.FormattingEnabled = true;
            this.cbHeatType.Items.AddRange(new object[] {
            "No Heat",
            "Indirect - Fired (IF)",
            "Direct - Fired (DF)",
            "Electric - 2 Stage",
            "Electric - SCR Modulating",
            "Dual Fuel (PRI-DF/SEC-IF)",
            "Dual Fuel (PRI-DF/SEC-ELEC)",
            "Dual Fuel (PRI-IF/SEC-ELEC)",
            "Dual Fuel (PRI-ELEC/SEC-ELEC)",
            "Hot Water",
            "Steam"});
            this.cbHeatType.Location = new System.Drawing.Point(269, 356);
            this.cbHeatType.Name = "cbHeatType";
            this.cbHeatType.Size = new System.Drawing.Size(210, 21);
            this.cbHeatType.TabIndex = 14;
            // 
            // label138
            // 
            this.label138.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.Location = new System.Drawing.Point(7, 356);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(253, 23);
            this.label138.TabIndex = 13;
            this.label138.Text = "Heat Type (20)";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 200);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(253, 20);
            this.label19.TabIndex = 13;
            this.label19.Text = "Compressor (13)";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label134
            // 
            this.label134.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(7, 356);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(253, 23);
            this.label134.TabIndex = 13;
            this.label134.Text = "Heat Type (20)";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label137
            // 
            this.label137.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.Location = new System.Drawing.Point(7, 122);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(253, 20);
            this.label137.TabIndex = 11;
            this.label137.Text = "Voltage/Phase (9)";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbVoltage
            // 
            this.cbVoltage.BackColor = System.Drawing.Color.White;
            this.cbVoltage.DropDownWidth = 300;
            this.cbVoltage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVoltage.ForeColor = System.Drawing.Color.Black;
            this.cbVoltage.FormattingEnabled = true;
            this.cbVoltage.Items.AddRange(new object[] {
            "115/60/1",
            "208-230/60/1",
            "208-230/60/3",
            "460/60/3",
            "575/60/3"});
            this.cbVoltage.Location = new System.Drawing.Point(269, 122);
            this.cbVoltage.Name = "cbVoltage";
            this.cbVoltage.Size = new System.Drawing.Size(210, 21);
            this.cbVoltage.TabIndex = 12;
            // 
            // label135
            // 
            this.label135.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.Location = new System.Drawing.Point(7, 122);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(253, 20);
            this.label135.TabIndex = 11;
            this.label135.Text = "Voltage/Phase (9)";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCabinet
            // 
            this.cbCabinet.BackColor = System.Drawing.Color.White;
            this.cbCabinet.DropDownWidth = 300;
            this.cbCabinet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCabinet.ForeColor = System.Drawing.Color.Black;
            this.cbCabinet.FormattingEnabled = true;
            this.cbCabinet.Items.AddRange(new object[] {
            "625 - 3,600 cfm",
            "1,500 - 9,000 cfm",
            "3,500 - 13,500 cfm"});
            this.cbCabinet.Location = new System.Drawing.Point(269, 17);
            this.cbCabinet.Name = "cbCabinet";
            this.cbCabinet.Size = new System.Drawing.Size(210, 21);
            this.cbCabinet.TabIndex = 1;
            // 
            // label136
            // 
            this.label136.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(7, 17);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(253, 20);
            this.label136.TabIndex = 0;
            this.label136.Text = "Cabinet (3)";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCreateBOM
            // 
            this.btnCreateBOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateBOM.ForeColor = System.Drawing.Color.Teal;
            this.btnCreateBOM.Location = new System.Drawing.Point(760, 720);
            this.btnCreateBOM.Name = "btnCreateBOM";
            this.btnCreateBOM.Size = new System.Drawing.Size(100, 25);
            this.btnCreateBOM.TabIndex = 469;
            this.btnCreateBOM.Text = "Create BOM";
            this.btnCreateBOM.UseVisualStyleBackColor = true;
            this.btnCreateBOM.Click += new System.EventHandler(this.btnCreateBOM_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Blue;
            this.btnPrint.Location = new System.Drawing.Point(632, 720);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 25);
            this.btnPrint.TabIndex = 468;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(886, 720);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 25);
            this.btnExit.TabIndex = 467;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtModelNo
            // 
            this.txtModelNo.Location = new System.Drawing.Point(131, 722);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(297, 20);
            this.txtModelNo.TabIndex = 470;
            this.txtModelNo.Visible = false;
            this.txtModelNo.TextChanged += new System.EventHandler(this.txtModelNo_TextChanged);
            // 
            // lbEnterModelNo
            // 
            this.lbEnterModelNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnterModelNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbEnterModelNo.Location = new System.Drawing.Point(12, 721);
            this.lbEnterModelNo.Name = "lbEnterModelNo";
            this.lbEnterModelNo.Size = new System.Drawing.Size(112, 23);
            this.lbEnterModelNo.TabIndex = 471;
            this.lbEnterModelNo.Text = "Enter ModelNo: ";
            this.lbEnterModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbEnterModelNo.Visible = false;
            this.lbEnterModelNo.Click += new System.EventHandler(this.lbEnterModelNo_Click);
            // 
            // btnParseModelNo
            // 
            this.btnParseModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParseModelNo.ForeColor = System.Drawing.Color.Blue;
            this.btnParseModelNo.Location = new System.Drawing.Point(439, 720);
            this.btnParseModelNo.Name = "btnParseModelNo";
            this.btnParseModelNo.Size = new System.Drawing.Size(100, 25);
            this.btnParseModelNo.TabIndex = 472;
            this.btnParseModelNo.Text = "Parse";
            this.btnParseModelNo.UseVisualStyleBackColor = true;
            this.btnParseModelNo.Visible = false;
            this.btnParseModelNo.Click += new System.EventHandler(this.btnParseModelNo_Click);
            // 
            // menuStripConfig
            // 
            this.menuStripConfig.BackColor = System.Drawing.Color.LightSalmon;
            this.menuStripConfig.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripConfig.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripConfig.Location = new System.Drawing.Point(0, 0);
            this.menuStripConfig.Name = "menuStripConfig";
            this.menuStripConfig.Size = new System.Drawing.Size(1000, 24);
            this.menuStripConfig.TabIndex = 473;
            this.menuStripConfig.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createBOMToolStripMenuItem,
            this.deleteBOMToolStripMenuItem,
            this.printToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createBOMToolStripMenuItem
            // 
            this.createBOMToolStripMenuItem.Name = "createBOMToolStripMenuItem";
            this.createBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.createBOMToolStripMenuItem.Text = "Create BOM";
            this.createBOMToolStripMenuItem.Click += new System.EventHandler(this.createBOMToolStripMenuItem_Click);
            // 
            // deleteBOMToolStripMenuItem
            // 
            this.deleteBOMToolStripMenuItem.Name = "deleteBOMToolStripMenuItem";
            this.deleteBOMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteBOMToolStripMenuItem.Text = "Delete BOM";
            this.deleteBOMToolStripMenuItem.Click += new System.EventHandler(this.deleteBOMToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // lbLastUpdateBy
            // 
            this.lbLastUpdateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastUpdateBy.ForeColor = System.Drawing.Color.Black;
            this.lbLastUpdateBy.Location = new System.Drawing.Point(147, 87);
            this.lbLastUpdateBy.Name = "lbLastUpdateBy";
            this.lbLastUpdateBy.Size = new System.Drawing.Size(134, 20);
            this.lbLastUpdateBy.TabIndex = 475;
            this.lbLastUpdateBy.Text = "??/??/????";
            this.lbLastUpdateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbLastUpdateBy.Click += new System.EventHandler(this.lbLastUpdateBy_Click);
            // 
            // lbBomCreateBy
            // 
            this.lbBomCreateBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBomCreateBy.ForeColor = System.Drawing.Color.Black;
            this.lbBomCreateBy.Location = new System.Drawing.Point(147, 47);
            this.lbBomCreateBy.Name = "lbBomCreateBy";
            this.lbBomCreateBy.Size = new System.Drawing.Size(134, 20);
            this.lbBomCreateBy.TabIndex = 474;
            this.lbBomCreateBy.Text = "??/??/????";
            this.lbBomCreateBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(658, 183);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(10, 15);
            this.label16.TabIndex = 478;
            this.label16.Text = "0";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(658, 168);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(10, 15);
            this.label17.TabIndex = 477;
            this.label17.Text = "3";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(658, 160);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(10, 15);
            this.label22.TabIndex = 476;
            this.label22.Text = "^";
            // 
            // lbUnitType
            // 
            this.lbUnitType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUnitType.ForeColor = System.Drawing.Color.Black;
            this.lbUnitType.Location = new System.Drawing.Point(872, 134);
            this.lbUnitType.Name = "lbUnitType";
            this.lbUnitType.Size = new System.Drawing.Size(100, 20);
            this.lbUnitType.TabIndex = 479;
            this.lbUnitType.Text = "UnitType";
            this.lbUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbUnitType.Visible = false;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Teal;
            this.label23.Location = new System.Drawing.Point(8, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(140, 20);
            this.label23.TabIndex = 480;
            this.label23.Text = "BOM Creation By:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Teal;
            this.label24.Location = new System.Drawing.Point(8, 87);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(140, 20);
            this.label24.TabIndex = 481;
            this.label24.Text = "Last Updated By:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmOldModelNoConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1000, 749);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.lbUnitType);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.lbLastUpdateBy);
            this.Controls.Add(this.lbBomCreateBy);
            this.Controls.Add(this.menuStripConfig);
            this.Controls.Add(this.btnParseModelNo);
            this.Controls.Add(this.lbEnterModelNo);
            this.Controls.Add(this.txtModelNo);
            this.Controls.Add(this.btnCreateBOM);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.groupBoxCabinet);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.lbLastUpdateDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label129);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.lbCompleteDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbProdStartDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbBOMCreationDate);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lbShipDate);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.lbOrderDate);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbJobNum);
            this.Controls.Add(this.lbEnvironment);
            this.Name = "frmOldModelNoConfig";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Old ModelNo Configuration";
            this.groupBoxCabinet.ResumeLayout(false);
            this.gbHotWaterInputs.ResumeLayout(false);
            this.gbHotWaterInputs.PerformLayout();
            this.menuStripConfig.ResumeLayout(false);
            this.menuStripConfig.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbCustName;
        public System.Windows.Forms.Label lbLastUpdateDate;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label129;
        public System.Windows.Forms.Label label130;
        public System.Windows.Forms.Label label131;
        public System.Windows.Forms.Label label127;
        public System.Windows.Forms.Label label128;
        public System.Windows.Forms.Label label119;
        public System.Windows.Forms.Label label120;
        public System.Windows.Forms.Label label121;
        public System.Windows.Forms.Label label122;
        public System.Windows.Forms.Label label123;
        public System.Windows.Forms.Label label124;
        public System.Windows.Forms.Label label125;
        public System.Windows.Forms.Label label126;
        public System.Windows.Forms.Label label111;
        public System.Windows.Forms.Label label112;
        public System.Windows.Forms.Label label113;
        public System.Windows.Forms.Label label114;
        public System.Windows.Forms.Label label115;
        public System.Windows.Forms.Label label116;
        public System.Windows.Forms.Label label117;
        public System.Windows.Forms.Label label118;
        public System.Windows.Forms.Label label103;
        public System.Windows.Forms.Label label104;
        public System.Windows.Forms.Label label105;
        public System.Windows.Forms.Label label106;
        public System.Windows.Forms.Label label107;
        public System.Windows.Forms.Label label108;
        public System.Windows.Forms.Label label109;
        public System.Windows.Forms.Label label110;
        public System.Windows.Forms.Label label102;
        public System.Windows.Forms.Label label100;
        public System.Windows.Forms.Label label101;
        public System.Windows.Forms.Label label96;
        public System.Windows.Forms.Label label97;
        public System.Windows.Forms.Label label98;
        public System.Windows.Forms.Label label99;
        public System.Windows.Forms.Label label92;
        public System.Windows.Forms.Label label93;
        public System.Windows.Forms.Label label94;
        public System.Windows.Forms.Label label95;
        public System.Windows.Forms.Label label88;
        public System.Windows.Forms.Label label89;
        public System.Windows.Forms.Label label90;
        public System.Windows.Forms.Label label91;
        public System.Windows.Forms.Label label84;
        public System.Windows.Forms.Label label85;
        public System.Windows.Forms.Label label86;
        public System.Windows.Forms.Label label87;
        public System.Windows.Forms.Label label80;
        public System.Windows.Forms.Label label81;
        public System.Windows.Forms.Label label82;
        public System.Windows.Forms.Label label83;
        public System.Windows.Forms.Label label76;
        public System.Windows.Forms.Label label77;
        public System.Windows.Forms.Label label78;
        public System.Windows.Forms.Label label79;
        public System.Windows.Forms.Label label72;
        public System.Windows.Forms.Label label73;
        public System.Windows.Forms.Label label74;
        public System.Windows.Forms.Label label75;
        public System.Windows.Forms.Label label70;
        public System.Windows.Forms.Label label71;
        public System.Windows.Forms.Label label69;
        public System.Windows.Forms.Label label68;
        public System.Windows.Forms.Label label67;
        public System.Windows.Forms.Label label66;
        public System.Windows.Forms.Label label65;
        public System.Windows.Forms.Label label61;
        public System.Windows.Forms.Label label62;
        public System.Windows.Forms.Label label63;
        public System.Windows.Forms.Label label64;
        public System.Windows.Forms.Label label57;
        public System.Windows.Forms.Label label58;
        public System.Windows.Forms.Label label59;
        public System.Windows.Forms.Label label60;
        public System.Windows.Forms.Label label53;
        public System.Windows.Forms.Label label54;
        public System.Windows.Forms.Label label55;
        public System.Windows.Forms.Label label56;
        public System.Windows.Forms.Label label49;
        public System.Windows.Forms.Label label50;
        public System.Windows.Forms.Label label51;
        public System.Windows.Forms.Label label52;
        public System.Windows.Forms.Label label45;
        public System.Windows.Forms.Label label46;
        public System.Windows.Forms.Label label47;
        public System.Windows.Forms.Label label48;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label lbModelNo;
        public System.Windows.Forms.Label lbCompleteDate;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lbProdStartDate;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lbBOMCreationDate;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label lbShipDate;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label lbOrderDate;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.Label lbEnvironment;
        public System.Windows.Forms.GroupBox groupBoxCabinet;
        public System.Windows.Forms.ComboBox cbHeatSourceSecondary;
        public System.Windows.Forms.ComboBox cbHeatSourcePrimary;
        public System.Windows.Forms.ComboBox cbMajorDesign;
        private System.Windows.Forms.Label label133;
        public System.Windows.Forms.ComboBox cbMinorDesign;
        private System.Windows.Forms.Label label132;
        public System.Windows.Forms.ComboBox cbAltitude;
        public System.Windows.Forms.ComboBox cbHailguard;
        public System.Windows.Forms.ComboBox cbAirFlowMonitoring;
        public System.Windows.Forms.ComboBox cbElectricalOptions;
        public System.Windows.Forms.ComboBox cbSmokeDetector;
        public System.Windows.Forms.ComboBox cbEconomizer;
        public System.Windows.Forms.ComboBox cbERVSize;
        public System.Windows.Forms.ComboBox cbERV_HRV;
        public System.Windows.Forms.ComboBox cbPoweredExhaustFanMotorHP;
        public System.Windows.Forms.ComboBox cbPoweredExhaustFanWheel;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.ComboBox cbPoweredExhaustFanMotor;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.ComboBox cbUnitControls;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.ComboBox cbCorrosiveEnvPackage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelInputsHeatCapacityLabel;
        public System.Windows.Forms.ComboBox cbFuelType;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox cbIndoorFanMotorHP;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cbIndoorFanWheel;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cbIndoorFanMotor;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox cbRefridgerantCapacityControl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.ComboBox cbEvapType;
        public System.Windows.Forms.ComboBox cbCoolingCapacity;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.ComboBox cbFiltrationOptions;
        public System.Windows.Forms.ComboBox cbCondenser;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.ComboBox cbHotGasReheat;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox cbCompressor;
        public System.Windows.Forms.ComboBox cbHeatType;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label134;
        public System.Windows.Forms.ComboBox cbVoltage;
        private System.Windows.Forms.Label label135;
        public System.Windows.Forms.ComboBox cbCabinet;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Button btnCreateBOM;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.TextBox txtModelNo;
        public System.Windows.Forms.Label lbEnterModelNo;
        public System.Windows.Forms.Button btnParseModelNo;
        private System.Windows.Forms.MenuStrip menuStripConfig;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteBOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.GroupBox gbHotWaterInputs;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label137;
        public System.Windows.Forms.TextBox txtEnteringTemp;
        public System.Windows.Forms.TextBox txtFlowRate;
        public System.Windows.Forms.Label lbLastUpdateBy;
        public System.Windows.Forms.Label lbBomCreateBy;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.ComboBox cbTranePPS_Compatibility;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label lbUnitType;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label24;
    }
}