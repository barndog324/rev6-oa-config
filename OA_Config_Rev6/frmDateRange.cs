﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmDateRange : Form
    {
        private frmMain m_parent;

        public frmDateRange(frmMain frmMain)
        {
            m_parent = frmMain;
            InitializeComponent();
        }

        private void btnDatePickExit_Click(object sender, EventArgs e)
        {
            m_parent.StartDate = DateTime.Today;
            m_parent.EndDate = DateTime.Today;
            this.Close();
        }

        private void btnDatePickOK_Click(object sender, EventArgs e)
        {
            frmMain frmMn = new frmMain();
            m_parent.StartDate = this.dtpDatePickStartDate.Value;
            m_parent.EndDate = this.dtpDatePickEndDate.Value;
            this.Close();
        }       
    }
}
