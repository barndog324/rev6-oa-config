﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace OA_Config_Rev6
{
    public partial class frmMain : Form
    {
        MainBO objMain = new MainBO();
        LineTransferBO objTran = new LineTransferBO();

        public class JobInfo
        {
            public string JobNum { get; set; }
            public string ModelNo { get; set; }            
        }        

        public bool gJobNumberStopEvent { get; set; }
        public bool gContinue { get; set; }

        public string jobNumberStr = "";
        public string CurrentTabStr { get; set; }
        public string EnvironmentStr { get; set; }

        public int CurrentTabIdx { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public frmMain()
        {
            InitializeComponent();

//            string envStr = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
           
//            if (envStr.Contains("Epicor905APP") == true)
//            {
//                GlobalJob.EnvColor = Color.Snow;
//                GlobalJob.EnvironmentStr = "Production Environment";
//                GlobalJob.DisplayMode = "Production";
//                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=EPICOR905SQL\\EPICOR905;Initial Catalog=KCC;Integrated Security=True";
//                config.Save(ConfigurationSaveMode.Modified);

//                ConfigurationManager.RefreshSection("connectionStrings");
//            }
//            else if (envStr.Contains("KCCWVPEPIC9APP") == true)
//            {
//                GlobalJob.EnvColor = Color.Snow;
//                GlobalJob.EnvironmentStr = "New Production Environment";
//                GlobalJob.DisplayMode = "Production";
//                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=kccwvpepic9sql;Initial Catalog=KCC;Integrated Security=True";
//                config.Save(ConfigurationSaveMode.Modified);

//                ConfigurationManager.RefreshSection("connectionStrings");
//            }            
//            else if (envStr.Contains("kccwvpkntcapp01") == true)
//            {
//                GlobalJob.EnvColor = Color.Snow;
//                GlobalJob.EnvironmentStr = "Kinetic Production Environment";
//                GlobalJob.DisplayMode = "Production";

//                 Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=kccwvpkntcsql01;Initial Catalog=KCC;Integrated Security=True";
//                config.Save(ConfigurationSaveMode.Modified);

//                ConfigurationManager.RefreshSection("connectionStrings");
//            }

//#if DEBUG
//            try           // If in the debug environment then set the Database connection string to the test database.
//            {
//                GlobalJob.EnvColor = Color.LightCoral;
//                GlobalJob.EnvironmentStr = "Development Environment";
//                GlobalJob.DisplayMode = "Debug";
//                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
//                config.Save(ConfigurationSaveMode.Modified);

//                ConfigurationManager.RefreshSection("connectionStrings");
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
//            }
//#if TEST2
//            try           // If in the debug environment then set the Database connection string to the test database.
//            {
//                GlobalJob.EnvColor = Color.SpringGreen;
//                GlobalJob.EnvironmentStr = "Test 2 Environment";
//                GlobalJob.DisplayMode = "Test";
//                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
//                config.Save(ConfigurationSaveMode.Modified);

//                ConfigurationManager.RefreshSection("connectionStrings");
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
//            }
//#endif
//#endif
            //GlobalJob.JobNumber = "320289-2-1";
            //GlobalJob.ModelNo = "HAEA015C3-C0G20AF00-C1AC00000-11F000050-A00A00A00-AA1000000-000000000";     

            //Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            //int w = Width >= screen.Width ? screen.Width : (screen.Width + Width) / 2;
            //int h = Height >= screen.Height ? screen.Height : (screen.Height + Height) / 2;
            //this.Location = new Point((screen.Width - w) / 2, (screen.Height - h) / 2);
            //this.Size = new Size(w, h);

            lbMainTitle.Text = "Outdoor Air Units";
            lbMainTitle.ForeColor = Color.Black;
            lbEnvironment.Text = GlobalJob.EnvironmentStr;
            this.BackColor = GlobalJob.EnvColor;

            if (GlobalJob.JobNumber != "")
            {
                //MessageBox.Show("lbEnvironment.Text => " + GlobalJob.EnvironmentStr); 
                LoadModelNoConfigScreen(GlobalJob.JobNumber, GlobalJob.ModelNo);               
                if (gContinue == false)
                {
                    GlobalJob.ProgramMode = "ExitProgram";
                }
            }
            else
            {
                DataTable dt = objMain.GetOA_ConfigMainList("", 0, "OA"); // Retrieve All Open OA Jobs
                PopulateMainDataGrid(dt, dgvMainUnitList);

                dt = objMain.GetOA_ConfigMainList("", 0, "MON"); // Retrieve All Open Monitor Jobs
                PopulateMainDataGrid(dt, dgvMonitorMainList);

                dt = objMain.GetOA_ConfigMainList("", 0, "RRU"); // Retrieve All Open RRU Jobs
                PopulateMainDataGrid(dt, dgvRRU_MainList);

                dt = objMain.GetOA_ConfigMainList("", 0, "MSP"); // Retrieve All Open MSP Jobs
                PopulateMainDataGrid(dt, dgvMSP_MainList);

                dt = objMain.GetOA_ConfigMainList("", 0, "HA"); // Retrieve All Open Viking Jobs
                PopulateMainDataGrid(dt, dgvViking_MainList);

                CurrentTabStr = "OAU";
                CurrentTabIdx = 0; //Default to OA Units                

                gJobNumberStopEvent = false;

                this.lbMainMsg.Text = "Double click on the desired row to configure Job.";
            }
        }    

        #region Events

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (GlobalJob.ProgramMode == "ExitProgram")
            {
                Application.Exit();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMainReset_Click(object sender, EventArgs e)
        {
            txtMainSearchJobNum.Text = "";
            DataTable dt = objMain.GetOA_ConfigMainList("", 0, "OA"); // Retrieve All Open Jobs
            PopulateMainDataGrid(dt, dgvMainUnitList);
            gJobNumberStopEvent = false;
        }

        private void txtMainSearchJobNum_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";

            DataGridView dgvSelectedGrid = null;

            if (CurrentTabIdx == 0)
            {
                dgvSelectedGrid = dgvMainUnitList;
            }
            else if (CurrentTabIdx == 1)
            {
                dgvSelectedGrid = dgvMonitorMainList;
            }
            else if (CurrentTabIdx == 2)
            {
                dgvSelectedGrid = dgvRRU_MainList;
            }
            else if (CurrentTabIdx == 3)
            {
                dgvSelectedGrid = dgvMSP_MainList;
            }

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else if ((e.KeyValue > 47 && e.KeyValue < 58) || (e.KeyValue > 95 && e.KeyValue < 106) ||
                     (e.KeyValue == 8))
            {
                try
                {
                    // Because the key value has not been added to the StartAt textbox
                    // at this point it must be added in order to search properly.
                    tmpStr = this.txtMainSearchJobNum.Text + convertKeyValue(e.KeyValue);
                    if (tmpStr.Length > 2)
                    {                       
                        gJobNumberStopEvent = true;

                        if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                        {
                            tmpStr = tmpStr.Substring(1, (tmpStr.Length - 1));
                        }

                        int rowIdx = 0;
                        foreach (DataGridViewRow dgr in dgvSelectedGrid.Rows)
                        {
                            if (dgr.Cells["JobNum"].Value != null)
                            {
                                if (dgr.Cells["JobNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["JobNum"].Value.ToString() == tmpStr)
                                {
                                    rowIdx = dgr.Index;
                                    dgvSelectedGrid.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                    dgvSelectedGrid.Rows[rowIdx].Selected = true;
                                    dgvSelectedGrid.FirstDisplayedScrollingRowIndex = rowIdx;
                                    //dgvMainUnitList.Focus();
                                    break;
                                }
                            }
                        }                                   
                    }
                }
                catch (Exception d)
                {
                    MessageBox.Show("ERROR - Searching data grid. " + d);
                }
            }
            else //Any other KeyValue will trigger an Error Message.
            {
                tmpStr = this.txtMainSearchJobNum.Text;
                MessageBox.Show("ERROR - Numeric values only.");
                this.txtMainSearchJobNum.Text = tmpStr;
                this.txtMainSearchJobNum.SelectionStart = tmpStr.Length;
            }
        }

        private void dgvMainUnitList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string partsOrdered = "";

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                partsOrdered = (row.Cells["PartsOrdered"].Value ?? string.Empty).ToString();
                if (partsOrdered == "True") // If partsOrdered equals true (1) then paint row
                {
                    colorCodeCells(dgvMainUnitList, row, Color.LightGreen, Color.Black);
                }
                else
                {
                    colorCodeCells(dgvMainUnitList, row, Color.White, Color.Black);
                }
            }
            //dgvMainUnitList.ClearSelection();
        }

        private void rbMainOpenJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbMainOpenJobs.Checked == true)
            {
                DataTable dt = objMain.GetOA_ConfigMainList("", 0, "OA"); // Retrieve All Open OA Jobs
                PopulateMainDataGrid(dt, dgvMainUnitList);

                dt = objMain.GetOA_ConfigMainList("", 0, "MON"); 
                PopulateMainDataGrid(dt, dgvMonitorMainList);

                dt = objMain.GetOA_ConfigMainList("", 0, "RRU"); 
                PopulateMainDataGrid(dt, dgvRRU_MainList);

                dt = objMain.GetOA_ConfigMainList("", 0, "MSP"); 
                PopulateMainDataGrid(dt, dgvMSP_MainList);

                dt = objMain.GetOA_ConfigMainList("", 0, "HA"); 
                PopulateMainDataGrid(dt, dgvViking_MainList);
                gJobNumberStopEvent = false;
            }
        }

        private void rbMainClosedJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbMainClosedJobs.Checked == true)
            {
                DataTable dt = objMain.GetOA_ConfigMainList("", 1, "OA"); // Retrieve All Closed Jobs
                PopulateMainDataGrid(dt, dgvMainUnitList);              

                dt = objMain.GetOA_ConfigMainList("", 1, "MON"); 
                PopulateMainDataGrid(dt, dgvMonitorMainList);

                dt = objMain.GetOA_ConfigMainList("", 1, "RRU"); 
                PopulateMainDataGrid(dt, dgvRRU_MainList);

                dt = objMain.GetOA_ConfigMainList("", 1, "MSP"); 
                PopulateMainDataGrid(dt, dgvMSP_MainList);

                dt = objMain.GetOA_ConfigMainList("", 1, "HA"); 
                PopulateMainDataGrid(dt, dgvViking_MainList);
                gJobNumberStopEvent = false;
            }
        }

        private void rbMainAllJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbMainAllJobs.Checked == true)
            {
                DataTable dt = objMain.GetOA_ConfigMainList("", 2, "OA"); // Retrieve All Open & Closed Jobs
                PopulateMainDataGrid(dt, dgvMainUnitList);               

                dt = objMain.GetOA_ConfigMainList("", 2, "MON"); 
                PopulateMainDataGrid(dt, dgvMonitorMainList);

                dt = objMain.GetOA_ConfigMainList("", 2, "RRU"); 
                PopulateMainDataGrid(dt, dgvRRU_MainList);

                dt = objMain.GetOA_ConfigMainList("", 2, "MSP"); 
                PopulateMainDataGrid(dt, dgvMSP_MainList);

                dt = objMain.GetOA_ConfigMainList("", 2, "HA");
                PopulateMainDataGrid(dt, dgvViking_MainList);
                gJobNumberStopEvent = false;
            }
        }
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void removePartsOrderedFlagToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DateTime partsOrderRemovedDate = DateTime.Now;
            DateTime partsOrderedDate = DateTime.Now;

            string jobNumLst = "";
            string orderedByStr = "";
            string partsOrderRemovedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            bool firstJob = true;

            frmPrintBOM frmPrint = new frmPrintBOM();

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    if (firstJob)
                    {
                        jobNumLst += row.Cells["JobNum"].Value.ToString();
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
                    }
                }
            }

            if (jobNumLst != "")
            {
                objMain.UpdateOA_SchedulePartsOrdered(jobNumLst, 0, partsOrderedDate, orderedByStr, partsOrderRemovedDate, partsOrderRemovedByStr);
            }
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            int tabIdx = tabMain.SelectedIndex;

            switch (tabIdx)
            {
                case 0:
                    lbMainTitle.Text = "Outdoor Air Units";
                    lbMainTitle.ForeColor = Color.Black;
                    CurrentTabStr = "OAU";
                    break;
                case 1:
                    lbMainTitle.Text = "Monitor Units";
                    lbMainTitle.ForeColor = Color.Blue;
                    CurrentTabStr = "Monitor";
                    break;
                case 2:
                    lbMainTitle.Text = "RRU Units";
                    lbMainTitle.ForeColor = Color.Red;
                    CurrentTabStr = "RRU";
                    break;
                case 3:
                    lbMainTitle.Text = "MSP Units";
                    lbMainTitle.ForeColor = Color.Green;
                    CurrentTabStr = "MSP";
                    break;
                case 4:
                    lbMainTitle.Text = "Mixed Air Units";
                    lbMainTitle.ForeColor = Color.Purple;
                    CurrentTabStr = "MixedAir";
                    break;
                default:
                    break;
            }

            CurrentTabIdx = tabIdx;
        }

        private void dgvMainUnitList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {           
            gridDoubleClick(dgvMainUnitList, dgvMainUnitList.CurrentRow.Index, "OAU");
        }

        private void dgvMonitorMainList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            gridDoubleClick(dgvMonitorMainList, dgvMonitorMainList.CurrentRow.Index, "MON");
        }

        private void dgvRRU_MainList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            gridDoubleClick(dgvRRU_MainList, dgvRRU_MainList.CurrentRow.Index, "RRU");
        }

        private void dgvMSP_MainList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            gridDoubleClick(dgvMSP_MainList, dgvMSP_MainList.CurrentRow.Index, "MSP");
        }

        private void dgvViking_MainList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            gridDoubleClick(dgvViking_MainList, dgvViking_MainList.CurrentRow.Index, "VKG");
        }       
       

        private void dgvViking_DoubleClick(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Other Methods
        private void gridDoubleClick(DataGridView dgvUnit, int rowIdx, string unitType)
        {
            string orderNumStr = "";
            string orderLineStr = "";
            string releaseNumStr = "";
            string customerNameStr = "";
            string modelNoStr;
            string unitDescStr = "";
            string bomCreationDateStr;
            string completeDateStr;
            string lastUpdateDateStr;
            string orderDateStr;
            string prodStartDateStr;
            string shipDateStr;
            string jobNumStr;
            string partNumStr = "";
            string lastModByStr = "";
            string bomCreateByStr = "";

            int dashOnePos;
            int dashTwoPos;
            int slashPos;

            bool modelNoPresent = false;

            if (rowIdx == -1)
            {
                MessageBox.Show("WARNING -- No Job Number has been selected or you Double Clicked on a Column Header!");
            }
            else
            {
                jobNumStr = dgvUnit.Rows[rowIdx].Cells["JobNum"].Value.ToString();

                dashOnePos = jobNumStr.IndexOf('-');
                dashTwoPos = jobNumStr.LastIndexOf('-');

                if ((dashOnePos != -1) && (dashTwoPos != -1))
                {
                    orderLineStr = jobNumStr.Substring((dashOnePos + 1), (dashTwoPos - (dashOnePos + 1)));
                    releaseNumStr = jobNumStr.Substring((dashTwoPos + 1), (jobNumStr.Length - (dashTwoPos + 1)));
                    orderNumStr = jobNumStr.Substring(0, dashOnePos);
                }

                customerNameStr = dgvUnit.Rows[rowIdx].Cells["CustomerName"].Value.ToString();
                partNumStr = dgvUnit.Rows[rowIdx].Cells["PartNum"].Value.ToString();
                modelNoStr = dgvUnit.Rows[rowIdx].Cells["ModelNo"].Value.ToString();

                DataTable dt = objMain.GetOA_JobDataByJobNum(jobNumStr);

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    unitDescStr = dr["PartDescription"].ToString();
                }

                prodStartDateStr = dgvUnit.Rows[rowIdx].Cells["StartDate"].Value.ToString();
                completeDateStr = dgvUnit.Rows[rowIdx].Cells["CompleteDate"].Value.ToString();
                orderDateStr = dgvUnit.Rows[rowIdx].Cells["OrderDate"].Value.ToString();
                shipDateStr = dgvUnit.Rows[rowIdx].Cells["ShipDate"].Value.ToString();
                bomCreationDateStr = dgvUnit.Rows[rowIdx].Cells["BOMDate"].Value.ToString();
                lastUpdateDateStr = dgvUnit.Rows[rowIdx].Cells["LastUpdate"].Value.ToString();
                bomCreateByStr = dgvUnit.Rows[rowIdx].Cells["CreatedBy"].Value.ToString();
                slashPos = bomCreateByStr.IndexOf("\\");
                if (slashPos > 0)
                {
                    bomCreateByStr = "- " + bomCreateByStr.Substring((slashPos + 1), (bomCreateByStr.Length - (slashPos + 1)));
                }
                lastModByStr = dgvUnit.Rows[rowIdx].Cells["LastModifiedBy"].Value.ToString();
                slashPos = lastModByStr.IndexOf("\\");
                if (slashPos > 0)
                {
                    lastModByStr = "- " + lastModByStr.Substring((slashPos + 1), (lastModByStr.Length - (slashPos + 1)));
                }

                if (unitType == "OAU")
                {
                    if ((modelNoStr.Length == 69) && (modelNoStr.IndexOf(" ") == -1))
                    {
                        parseModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr, bomCreationDateStr, 
                                                      bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr, true, "OAU");
                    }
                    else if ((modelNoStr.Length == 39) && (modelNoStr.IndexOf(" ") == -1))
                    {
                        parseOldModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr,
                                                      bomCreationDateStr, bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr);
                    }
                    else
                    {
                        MessageBox.Show("ERROR - Check OAU ModelNo length, it must be either 39 or 69 digits long!");
                    }
                }
                else if (unitType == "MON") 
                {
                    if ((modelNoStr.Length == 43) && (modelNoStr.IndexOf(" ") == -1))
                    { 
                        parseMonitorModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr,
                                                    bomCreationDateStr, bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr);
                    }
                    else
                    {
                        MessageBox.Show("ERROR - Check Monitor ModelNo length, it must be 43 digits long!");
                    }
                }
                else if (unitType == "RRU")
                {
                    DisplayRRU_BOM();
                }
                else if (unitType == "MSP")
                {
                    if (modelNoStr.Length == 69)
                    {
                        modelNoPresent = true;

                        parseMSP_ModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr,
                                                          bomCreationDateStr, bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr, modelNoPresent);
                    }
                    else
                    {
                        MessageBox.Show("ERROR - Check MSP ModelNo length, it must be 69 digits long!");
                    }
                }
                else if (unitType == "VKG")
                {
                    if ((modelNoStr.Length == 69) && (modelNoStr.IndexOf(" ") == -1))
                    {
                        parseModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr, bomCreationDateStr, bomCreateByStr, 
                                                      lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr, true, "VKG");
                    }
                    else
                    {
                        MessageBox.Show("ERROR - Check Viking ModelNo length, it must be 69 digits long!");
                    }
                }
                else
                {
                    MessageBox.Show("ERROR - the job selected does NOT contain a valid ModelNo! " +
                        "A valid 39 or 43 or 69 digit ModelNo is required!");
                }
            }
        }

        private void LoadModelNoConfigScreen(string jobNumStr, string modelNoStr)
        {            
            string orderNumStr = "";
            string orderLineStr = "";
            string releaseNumStr = "";
            string customerNameStr = "";            
            string unitDescStr = "";
            string bomCreationDateStr = "";
            string completeDateStr = ""; 
            string lastUpdateDateStr = ""; 
            string orderDateStr = ""; 
            string prodStartDateStr = ""; 
            string shipDateStr = "";
            string partNumStr = "";
            string lastModByStr = "";         
            string bomCreateByStr = "";
            string partDescStr = "";
            string unitTypeStr = "";

            int dashOnePos;
            int dashTwoPos;
            int slashPos;

            bool displayConfig = true;

            dashOnePos = jobNumStr.IndexOf('-');
            dashTwoPos = jobNumStr.LastIndexOf('-');

            if ((dashOnePos != -1) && (dashTwoPos != -1))
            {
                orderLineStr = jobNumStr.Substring((dashOnePos + 1), (dashTwoPos - (dashOnePos + 1)));
                releaseNumStr = jobNumStr.Substring((dashTwoPos + 1), (jobNumStr.Length - (dashTwoPos + 1)));
                orderNumStr = jobNumStr.Substring(0, dashOnePos);
            }

            DataTable dtJob = objMain.GetOA_JobDataByJobNum(jobNumStr);
            
            if (dtJob.Rows.Count > 0)
            {                
                DataRow drJob = dtJob.Rows[0];                
                customerNameStr = drJob["CustomerName"].ToString();
                partNumStr = drJob["PartNum"].ToString();
                unitDescStr = drJob["PartDescription"].ToString();
                prodStartDateStr = drJob["StartDate"].ToString();
                completeDateStr = drJob["CompDate"].ToString();
                orderDateStr = drJob["OrderDate"].ToString();
                shipDateStr = drJob["ShipDate"].ToString();
                bomCreationDateStr = drJob["BOMCreateDate"].ToString();                
                bomCreateByStr = drJob["CreatedBy"].ToString();
                slashPos = bomCreateByStr.IndexOf("\\");
                if (slashPos > 0)
                {
                    bomCreateByStr = bomCreateByStr.Substring((slashPos + 1), (bomCreateByStr.Length - (slashPos+1)));
                }
                lastUpdateDateStr = drJob["LastModDate"].ToString();
                lastModByStr = drJob["LastModifiedBy"].ToString();
                slashPos = lastModByStr.IndexOf("\\");
                if (slashPos > 0)
                {
                    lastModByStr = lastModByStr.Substring((slashPos + 1), (lastModByStr.Length - (slashPos+1)));
                }
            }
            
            if (partNumStr.Length > 7)
            {
                partNumStr = partNumStr.Substring(0, 7);
            }
            
            if (partNumStr != GlobalJob.ModelNo.Substring(0, 7))
            {
                DialogResult result1 = MessageBox.Show("The Epicor part number " +partNumStr + " for Job# " + jobNumStr + 
                                                       " does NOT match the 1st digits of the ModelNo " + GlobalJob.ModelNo.Substring(0, 7) + "  Press 'Yes' to continue or 'No' to Exit",
                                                       "WARNING PartNum Mis-Match",
                                                       MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    gContinue = true;
                    displayConfig = true;
                }
                else
                {
                    displayConfig = false;
                     gContinue = false;
                }
            }
            
            if (displayConfig == true)
            {
                 //DataTable dt = objMain.GetModelNumDesc(567, "NA", modelNoStr.Substring(4, 3));

                 //if (dt.Rows.Count > 0)
                 //{
                 //    DataRow dr = dt.Rows[0];
                 //    unitDescStr = dr["DigitDescription"].ToString();
                 //}
                
                 if ((modelNoStr.Length == 69) && (modelNoStr.IndexOf(" ") == -1))
                 {
                     if (modelNoStr.Substring(0,2) == "OA")
                     {
                         unitTypeStr = "OAU";
                     }
                     else if (modelNoStr.Substring(0, 2) == "HA")
                     {
                         unitTypeStr = "Viking";
                     }
                     else
                     {
                         unitTypeStr = "MSP";
                     }

                     if (unitTypeStr == "OAU" || unitTypeStr == "Viking")
                     {
                         parseModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr,
                                                       bomCreationDateStr, bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr, true, unitTypeStr);
                     }
                     else
                     {
                          parseMSP_ModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr, bomCreationDateStr, 
                                                            bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr, true);
                     }
                 }
                 else if ((modelNoStr.Length == 39) && (modelNoStr.IndexOf(" ") == -1))
                 {
                 
                     parseOldModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr,
                                                     bomCreationDateStr, bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr);
                 }
                 else if ((modelNoStr.Length == 43) && (modelNoStr.IndexOf(" ") == -1))
                 {

                     parseMonitorModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr,
                                                     bomCreationDateStr, bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr);
                 }                
                 else
                 {
                     MessageBox.Show("ERROR - the job selected does NOT contain a valid ModelNo! " +
                         "A valid 39 or 69 digit ModelNo is required!");
                 }                    
            }  
            else
            {
                MessageBox.Show("ERROR - this job contains a PartNum different than the first 7 digits of the ModelNo " +
                                "and the user has chosen to abort configuration.");
            }
        }

        private void PopulateMainDataGrid(DataTable dt, DataGridView dgvGrid)
        {

            dgvGrid.DataSource = dt;

            dgvGrid.Columns["Line"].Width = 50;            // Line
            dgvGrid.Columns["Line"].HeaderText = "Schd\nLine";
            dgvGrid.Columns["Line"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvGrid.Columns["Line"].DisplayIndex = 0;
            dgvGrid.Columns["JobNum"].Width = 80;            // JobNum
            dgvGrid.Columns["JobNum"].HeaderText = "JobNum";
            dgvGrid.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvGrid.Columns["JobNum"].DisplayIndex = 1;
            dgvGrid.Columns["CustomerName"].Width = 180;           // CustomerName
            dgvGrid.Columns["CustomerName"].HeaderText = "Customer Name";
            dgvGrid.Columns["CustomerName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvGrid.Columns["CustomerName"].DisplayIndex = 2;
            dgvGrid.Columns["PartNum"].Width = 75;           // Part Number
            dgvGrid.Columns["PartNum"].HeaderText = "Part Num";
            dgvGrid.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvGrid.Columns["PartNum"].DisplayIndex = 3;
            dgvGrid.Columns["ModelNo"].Width = 285;            // Model No
            dgvGrid.Columns["ModelNo"].HeaderText = "Model No";
            dgvGrid.Columns["ModelNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvGrid.Columns["ModelNo"].DisplayIndex = 4;
            dgvGrid.Columns["StartDate"].Width = 75;           // Start Date   
            dgvGrid.Columns["StartDate"].HeaderText = "Start Date";
            dgvGrid.Columns["StartDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrid.Columns["StartDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvGrid.Columns["StartDate"].DisplayIndex = 5;
            dgvGrid.Columns["CompleteDate"].Width = 75;            // Complete Date         
            dgvGrid.Columns["CompleteDate"].HeaderText = "Comp Date";
            dgvGrid.Columns["CompleteDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrid.Columns["CompleteDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvGrid.Columns["CompleteDate"].DisplayIndex = 6;
            dgvGrid.Columns["OrderDate"].Width = 75;            // Order Date
            dgvGrid.Columns["OrderDate"].HeaderText = "Order Date";
            dgvGrid.Columns["OrderDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrid.Columns["OrderDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvGrid.Columns["OrderDate"].DisplayIndex = 7;
            dgvGrid.Columns["ShipDate"].Width = 75;            // Ship Date
            dgvGrid.Columns["ShipDate"].HeaderText = "Ship Date";
            dgvGrid.Columns["ShipDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrid.Columns["ShipDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvGrid.Columns["ShipDate"].DisplayIndex = 8;
            dgvGrid.Columns["BOMDate"].Width = 75;            // BOM Date
            dgvGrid.Columns["BOMDate"].HeaderText = "BOM Date";
            dgvGrid.Columns["BOMDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrid.Columns["BOMDate"].DisplayIndex = 9;
            dgvGrid.Columns["LastUpdate"].Width = 75;            // Last Update
            dgvGrid.Columns["LastUpdate"].HeaderText = "Last Update";
            dgvGrid.Columns["LastUpdate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrid.Columns["LastUpdate"].DisplayIndex = 10;
            dgvGrid.Columns["JobClosed"].Width = 155;            // Status
            dgvGrid.Columns["JobClosed"].HeaderText = "Job Status/\nCur Opr Start Time";
            dgvGrid.Columns["JobClosed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvGrid.Columns["JobClosed"].DisplayIndex = 11;
            dgvGrid.Columns["PartsOrderedDate"].Width = 75;            // Date parts order was placed.
            dgvGrid.Columns["PartsOrderedDate"].HeaderText = "Parts Ordered\nDate";
            dgvGrid.Columns["PartsOrderedDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrid.Columns["PartsOrderedDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvGrid.Columns["PartsOrderedDate"].DisplayIndex = 12;
            dgvGrid.Columns["PartsOrdered"].Visible = false;
            dgvGrid.Columns["CreatedBy"].Visible = false;
            dgvGrid.Columns["LastModifiedBy"].Visible = false;         

        }

        private string convertKeyValue(int keyValue)
        {
            // This function converts the integer KeyValue passed into it to its string equivalent.

            if ((keyValue == 48) || (keyValue == 96))
            {
                return "0";
            }
            else if ((keyValue == 49) || (keyValue == 97))
            {
                return "1";
            }
            else if ((keyValue == 50) || (keyValue == 98))
            {
                return "2";
            }
            else if ((keyValue == 51) || (keyValue == 99))
            {
                return "3";
            }
            else if ((keyValue == 52) || (keyValue == 100))
            {
                return "4";
            }
            else if ((keyValue == 53) || (keyValue == 101))
            {
                return "5";
            }
            else if ((keyValue == 54) || (keyValue == 102))
            {
                return "6";
            }
            else if ((keyValue == 55) || (keyValue == 103))
            {
                return "7";
            }
            else if ((keyValue == 56) || (keyValue == 104))
            {
                return "8";
            }
            else if ((keyValue == 57) || (keyValue == 105))
            {
                return "9";
            }

            return "";
        }

        private void parseModelNoDisplayConfigForm(string modelNoStr, string jobNumStr, string custName, string unitDescStr, string orderDateStr, string shipDateStr,
                                                   string bomCreationDateStr, string bomCreateByStr, string lastUpdateDateStr, string lastUpdateByStr,  string prodStartDateStr, 
                                                   string completeDateStr, bool modelNoValuesPresent, string unitTypeStr)
        {
            string digitValStr = "";            
            string heatingTypeStr = "NA";            
           
            frmModelNoConfig frmConfig = new frmModelNoConfig();                       
            
            frmConfig.lbModelType.Text = unitTypeStr;
            frmConfig.lbEnvironment.Text = GlobalJob.EnvironmentStr;           
            frmConfig.BackColor = GlobalJob.EnvColor;
           
            //frmConfig.labelConfigModelNo.Text = modelNoStr;    
            
            frmConfig.lbJobNum.Text = jobNumStr;
            frmConfig.lbOrderDate.Text = orderDateStr;
            frmConfig.lbShipDate.Text = shipDateStr;
            frmConfig.lbBOMCreationDate.Text = bomCreationDateStr;
            frmConfig.lbBomCreateBy.Text = bomCreateByStr;
            frmConfig.lbLastUpdateDate.Text = lastUpdateDateStr;
            frmConfig.lbLastUpdateBy.Text = lastUpdateByStr;
            frmConfig.lbProdStartDate.Text = prodStartDateStr;
            frmConfig.lbCompleteDate.Text = completeDateStr;
            //frmConfig.Text = unitDescStr;
            frmConfig.lbModelNo.Text = modelNoStr;
            frmConfig.lbCustName.Text = custName; 

            // Digit 3 - Cabinet Size
                                          
            DataTable dt = objMain.GetModelNumDesc(3, "NA", null, unitTypeStr);
            frmConfig.cbCabinet.DataSource = dt;
            frmConfig.cbCabinet.DisplayMember = "DigitDescription";
            frmConfig.cbCabinet.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(2, 1); 
                frmConfig.cbCabinet.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 4 - Major Design
           
            dt = objMain.GetModelNumDesc(4, heatingTypeStr, null, unitTypeStr);
            frmConfig.cbMajorDesign.DataSource = dt;
            frmConfig.cbMajorDesign.DisplayMember = "DigitDescription";
            frmConfig.cbMajorDesign.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(3, 1);
                frmConfig.cbMajorDesign.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 567 - Cooling Capacity               
            
            dt = objMain.GetModelNumDesc(567, "NA", null, unitTypeStr);
            frmConfig.cbCoolingCapacity.DataSource = dt;
            frmConfig.cbCoolingCapacity.DisplayMember = "DigitDescription";
            frmConfig.cbCoolingCapacity.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(4, 3);
                frmConfig.cbCoolingCapacity.SelectedIndex = GetRowIndex(dt, digitValStr);            
            }
            
            // Digit 8 - Airflow Configuration               
            
            dt = objMain.GetModelNumDesc(8, "NA", null, unitTypeStr);
            frmConfig.cbAirflowConfig.DataSource = dt;
            frmConfig.cbAirflowConfig.DisplayMember = "DigitDescription";
            frmConfig.cbAirflowConfig.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(7, 1);
                frmConfig.cbAirflowConfig.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 9 - Voltage
            
            dt = objMain.GetModelNumDesc(9, "NA", null, unitTypeStr);
            frmConfig.cbVoltage.DataSource = dt;
            frmConfig.cbVoltage.DisplayMember = "DigitDescription";
            frmConfig.cbVoltage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(8, 1);
                frmConfig.cbVoltage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 11 - Indoor Coil Type
            
            dt = objMain.GetModelNumDesc(11, "NA", null, unitTypeStr);
            frmConfig.cbIndoorCoilType.DataSource = dt;
            frmConfig.cbIndoorCoilType.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorCoilType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(10, 1);
                frmConfig.cbIndoorCoilType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 12 - Hot Gas Reheat
            
            dt = objMain.GetModelNumDesc(12, "NA", null, unitTypeStr);
            frmConfig.cbHotGasReheat.DataSource = dt;
            frmConfig.cbHotGasReheat.DisplayMember = "DigitDescription";
            frmConfig.cbHotGasReheat.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(11, 1);
                frmConfig.cbHotGasReheat.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 13 - Compressor
            
            dt = objMain.GetModelNumDesc(13, "NA", null, unitTypeStr);
            frmConfig.cbCompressor.DataSource = dt;
            frmConfig.cbCompressor.DisplayMember = "DigitDescription";
            frmConfig.cbCompressor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(12, 1);
                frmConfig.cbCompressor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 14 - Outdoor Coil Type
            
            dt = objMain.GetModelNumDesc(14, "NA", null, unitTypeStr);
            frmConfig.cbOutdoorCoilType.DataSource = dt;
            frmConfig.cbOutdoorCoilType.DisplayMember = "DigitDescription";
            frmConfig.cbOutdoorCoilType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(13, 1);
                frmConfig.cbOutdoorCoilType.SelectedIndex = GetRowIndex(dt, digitValStr);
                string tempDesc = frmConfig.cbOutdoorCoilType.Text;
                if (tempDesc.Contains("ASHP") || tempDesc.Contains("WSHP"))
                {
                    frmConfig.lbUnitType.Text = "HeatPump";
                }
                else
                {
                    frmConfig.lbUnitType.Text = "Standard";
                }
            }
            
            // Digit 15 - Refridgerant Capacity Control
            
            dt = objMain.GetModelNumDesc(15, "NA", null, unitTypeStr);
            frmConfig.cbRefridgerantCapacityControl.DataSource = dt;
            frmConfig.cbRefridgerantCapacityControl.DisplayMember = "DigitDescription";
            frmConfig.cbRefridgerantCapacityControl.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(14, 1);
                frmConfig.cbRefridgerantCapacityControl.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 16 - Heat Type Primary
            
            dt = objMain.GetModelNumDesc(16, "NA", null, unitTypeStr);
            frmConfig.cbHeatTypePrimary.DataSource = dt;
            frmConfig.cbHeatTypePrimary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatTypePrimary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(15, 1);
                frmConfig.cbHeatTypePrimary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            if (digitValStr == "A" || digitValStr == "B" || digitValStr == "C" || digitValStr == "D" || digitValStr == "E" || digitValStr == "F")
            {
                heatingTypeStr = "IF";
            }
            else if (digitValStr == "G")
            {
                heatingTypeStr = "HOTWATER";
            }
            else if (digitValStr == "H" || digitValStr == "J")
            {
                heatingTypeStr = "ELEC";
            }
            else
            {
                heatingTypeStr = "IF";
            }
            // Digit 17 - Heat Capacity Primary
            
            dt = objMain.GetModelNumDesc(17, heatingTypeStr, null, unitTypeStr);
            frmConfig.cbHeatCapacityPrimary.DataSource = dt;
            frmConfig.cbHeatCapacityPrimary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatCapacityPrimary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(16, 1);
                frmConfig.cbHeatCapacityPrimary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 18 - Heat Type Secondary
            
            dt = objMain.GetModelNumDesc(18, "NA", null, unitTypeStr);
            frmConfig.cbHeatTypeSecondary.DataSource = dt;
            frmConfig.cbHeatTypeSecondary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatTypeSecondary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(17, 1);
                frmConfig.cbHeatTypeSecondary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            if (digitValStr == "1" || digitValStr == "2")
            {
                heatingTypeStr = "DF";
            }
            else if (digitValStr == "3")
            {
                heatingTypeStr = "HOTWATER";
            }
            else if (digitValStr == "4" || digitValStr == "5")
            {
                heatingTypeStr = "ELEC";
            }
            else
            {
                heatingTypeStr = "NA";
            }

            // Digit 19 - Heat Capacity Secondary           

            dt = objMain.GetModelNumDesc(19, heatingTypeStr, null, unitTypeStr);
            frmConfig.cbHeatCapacitySecondary.DataSource = dt;
            frmConfig.cbHeatCapacitySecondary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatCapacitySecondary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(18, 1);
                frmConfig.cbHeatCapacitySecondary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 21 - Supply Fan Motor
           
            dt = objMain.GetModelNumDesc(21, "NA", null, unitTypeStr);
            frmConfig.cbSupplyFanMotor.DataSource = dt;
            frmConfig.cbSupplyFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanMotor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(20, 1);
                frmConfig.cbSupplyFanMotor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 22 - Supply Fan Motor Type
            
            dt = objMain.GetModelNumDesc(22, "NA", null, unitTypeStr);
            frmConfig.cbSupplyFanMotorType.DataSource = dt;
            frmConfig.cbSupplyFanMotorType.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanMotorType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(21, 1);
                frmConfig.cbSupplyFanMotorType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 23 & 24 - Supply Fan Wheel Diameter
            
            dt = objMain.GetModelNumDesc(2324, "NA", null, unitTypeStr);
            frmConfig.cbSupplyFanWheelDiameter.DataSource = dt;
            frmConfig.cbSupplyFanWheelDiameter.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanWheelDiameter.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(22, 2);
                frmConfig.cbSupplyFanWheelDiameter.SelectedIndex = GetRowIndex(dt, digitValStr);
            }            

            // Digit 25 - Exhaust Fan Motor
            
            dt = objMain.GetModelNumDesc(25, "NA", null, unitTypeStr);
            frmConfig.cbExhaustFanMotor.DataSource = dt;
            frmConfig.cbExhaustFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanMotor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(24, 1);
                frmConfig.cbExhaustFanMotor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 26 - Exhaust Fan Motor Type
            
            dt = objMain.GetModelNumDesc(26, "NA", null, unitTypeStr);
            frmConfig.cbExhaustFanMotorType.DataSource = dt;
            frmConfig.cbExhaustFanMotorType.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanMotorType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(25, 1);
                frmConfig.cbExhaustFanMotorType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 27 & 28 - Exhaust Fan Wheel Diameter
            
            dt = objMain.GetModelNumDesc(2728, "NA", null, unitTypeStr);
            frmConfig.cbExhaustFanWheelDiamater.DataSource = dt;
            frmConfig.cbExhaustFanWheelDiamater.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanWheelDiamater.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(26, 2);
                frmConfig.cbExhaustFanWheelDiamater.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 29 - Exhaust Fan Piezo Ring
            
            dt = objMain.GetModelNumDesc(29, "NA", null, unitTypeStr);
            frmConfig.cbFanPiezoRing.DataSource = dt;
            frmConfig.cbFanPiezoRing.DisplayMember = "DigitDescription";
            frmConfig.cbFanPiezoRing.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(28, 1);
                frmConfig.cbFanPiezoRing.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 31 - Unit Controls
            
            dt = objMain.GetModelNumDesc(31, "NA", null, unitTypeStr);
            frmConfig.cbUnitControls.DataSource = dt;
            frmConfig.cbUnitControls.DisplayMember = "DigitDescription";
            frmConfig.cbUnitControls.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(30, 1);
                frmConfig.cbUnitControls.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 32 - Build Interface
            
            dt = objMain.GetModelNumDesc(32, "NA", null, unitTypeStr);
            frmConfig.cbBuildingInterface.DataSource = dt;
            frmConfig.cbBuildingInterface.DisplayMember = "DigitDescription";
            frmConfig.cbBuildingInterface.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(31, 1);
                frmConfig.cbBuildingInterface.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 33 - Filter Options
            
            dt = objMain.GetModelNumDesc(33, "NA", null, unitTypeStr);
            frmConfig.cbFilterOptions.DataSource = dt;
            frmConfig.cbFilterOptions.DisplayMember = "DigitDescription";
            frmConfig.cbFilterOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(32, 1);
                frmConfig.cbFilterOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 34 - ERV-Composite Construction with Bypass
            
            dt = objMain.GetModelNumDesc(34, "NA", null, unitTypeStr);
            frmConfig.cbEnergyRecovery.DataSource = dt;
            frmConfig.cbEnergyRecovery.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyRecovery.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(33, 1);
                frmConfig.cbEnergyRecovery.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 35 - Energy Recovery Wheel Options
            
            dt = objMain.GetModelNumDesc(35, "NA", null, unitTypeStr);
            frmConfig.cbEnergyRecoveryWheelOptions.DataSource = dt;
            frmConfig.cbEnergyRecoveryWheelOptions.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyRecoveryWheelOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(34, 1);
                frmConfig.cbEnergyRecoveryWheelOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 36 - Energy Recovery Wheel Size
            
            dt = objMain.GetModelNumDesc(36, "NA", null, unitTypeStr);
            frmConfig.cbEnergyWheelRecoverySize.DataSource = dt;
            frmConfig.cbEnergyWheelRecoverySize.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyWheelRecoverySize.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(35, 1);
                frmConfig.cbEnergyWheelRecoverySize.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 37 ERV Rotation Sensor
            
            dt = objMain.GetModelNumDesc(37, "NA", null, unitTypeStr);
            frmConfig.cbERV_RotationSensor.DataSource = dt;
            frmConfig.cbERV_RotationSensor.DisplayMember = "DigitDescription";
            frmConfig.cbERV_RotationSensor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(36, 1);
                frmConfig.cbERV_RotationSensor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 38 - Damper Options
            
            dt = objMain.GetModelNumDesc(38, "NA", null, unitTypeStr);
            frmConfig.cbDamperOptions.DataSource = dt;
            frmConfig.cbDamperOptions.DisplayMember = "DigitDescription";
            frmConfig.cbDamperOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(37, 1);
                frmConfig.cbDamperOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 39 - Exhaust Dampers 
            
            dt = objMain.GetModelNumDesc(39, "NA", null, unitTypeStr);
            frmConfig.cbExhaustDampers.DataSource = dt;
            frmConfig.cbExhaustDampers.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustDampers.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(38, 1);
                frmConfig.cbExhaustDampers.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 41 - Electrical Options
            
            dt = objMain.GetModelNumDesc(41, "NA", null, unitTypeStr);
            frmConfig.cbElectricalOptions.DataSource = dt;
            frmConfig.cbElectricalOptions.DisplayMember = "DigitDescription";
            frmConfig.cbElectricalOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(40, 1);
                frmConfig.cbElectricalOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 42 - Corrosive Environment Package
            
            dt = objMain.GetModelNumDesc(42, "NA", null, unitTypeStr);
            frmConfig.cbCorrisiveEnvironmentPackage.DataSource = dt;
            frmConfig.cbCorrisiveEnvironmentPackage.DisplayMember = "DigitDescription";
            frmConfig.cbCorrisiveEnvironmentPackage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(41, 1);
                frmConfig.cbCorrisiveEnvironmentPackage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 43 - Outdoor Air Monitoring
            
            dt = objMain.GetModelNumDesc(43, "NA", null, unitTypeStr);
            frmConfig.cbOutdoorAirMonitoring.DataSource = dt;
            frmConfig.cbOutdoorAirMonitoring.DisplayMember = "DigitDescription";
            frmConfig.cbOutdoorAirMonitoring.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(42, 1);
                frmConfig.cbOutdoorAirMonitoring.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 44 - Condenser Fan Options
           
            dt = objMain.GetModelNumDesc(44, "NA", null, unitTypeStr);
            frmConfig.cbCondenserFanOptions.DataSource = dt;
            frmConfig.cbCondenserFanOptions.DisplayMember = "DigitDescription";
            frmConfig.cbCondenserFanOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(43, 1);
                frmConfig.cbCondenserFanOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 45 - Compressor Sound Blankets 
           
            dt = objMain.GetModelNumDesc(45, "NA", null, unitTypeStr);
            frmConfig.cbSoundAttenuationPackage.DataSource = dt;
            frmConfig.cbSoundAttenuationPackage.DisplayMember = "DigitDescription";
            frmConfig.cbSoundAttenuationPackage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(44, 1);
                frmConfig.cbSoundAttenuationPackage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 46 - Smoke Detector 
            
            dt = objMain.GetModelNumDesc(46, "NA", null, unitTypeStr);
            frmConfig.cbSmokeDetector.DataSource = dt;
            frmConfig.cbSmokeDetector.DisplayMember = "DigitDescription";
            frmConfig.cbSmokeDetector.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(45, 1);
                frmConfig.cbSmokeDetector.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 47 - Hailguards
            
            dt = objMain.GetModelNumDesc(47, "NA", null, unitTypeStr);
            frmConfig.cbHailguards.DataSource = dt;
            frmConfig.cbHailguards.DisplayMember = "DigitDescription";
            frmConfig.cbHailguards.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(46, 1);
                frmConfig.cbHailguards.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 48 - Service Lights
            
            dt = objMain.GetModelNumDesc(48, "NA", null, unitTypeStr);
            frmConfig.cbServiceLights.DataSource = dt;
            frmConfig.cbServiceLights.DisplayMember = "DigitDescription";
            frmConfig.cbServiceLights.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(47, 1);
                frmConfig.cbServiceLights.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 49 - UV Lights 
            
            dt = objMain.GetModelNumDesc(49, "NA", null, unitTypeStr);
            frmConfig.cbUV_Lights.DataSource = dt;
            frmConfig.cbUV_Lights.DisplayMember = "DigitDescription";
            frmConfig.cbUV_Lights.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(48, 1);
                frmConfig.cbUV_Lights.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 51 - Installation
            
            dt = objMain.GetModelNumDesc(51, "NA", null, unitTypeStr);
            frmConfig.cbInstallation.DataSource = dt;
            frmConfig.cbInstallation.DisplayMember = "DigitDescription";
            frmConfig.cbInstallation.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(50, 1);
                frmConfig.cbInstallation.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 52 - Convenience Outlet 
            
            dt = objMain.GetModelNumDesc(52, "NA", null, unitTypeStr);
            frmConfig.cbConvenienceOutlet.DataSource = dt;
            frmConfig.cbConvenienceOutlet.DisplayMember = "DigitDescription";
            frmConfig.cbConvenienceOutlet.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(51, 1);
                frmConfig.cbConvenienceOutlet.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 53 - Controls Display 
            
            dt = objMain.GetModelNumDesc(53, "NA", null, unitTypeStr);
            frmConfig.cbControlsDisplay.DataSource = dt;
            frmConfig.cbControlsDisplay.DisplayMember = "DigitDescription";
            frmConfig.cbControlsDisplay.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(52, 1);
                frmConfig.cbControlsDisplay.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 54 - Reliatel 
            
            dt = objMain.GetModelNumDesc(54, "NA", null, unitTypeStr);
            frmConfig.cbCoolingControls.DataSource = dt;
            frmConfig.cbCoolingControls.DisplayMember = "DigitDescription";
            frmConfig.cbCoolingControls.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(53, 1);
                frmConfig.cbCoolingControls.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 55 - Face and Bypass Evap 
            
            dt = objMain.GetModelNumDesc(55, "NA", null, unitTypeStr);
            frmConfig.cbFaceAndBypassEvap.DataSource = dt;
            frmConfig.cbFaceAndBypassEvap.DisplayMember = "DigitDescription";
            frmConfig.cbFaceAndBypassEvap.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(54, 1);
                frmConfig.cbFaceAndBypassEvap.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 56 - Thermostat 
            
            dt = objMain.GetModelNumDesc(56, "NA", null, unitTypeStr);
            frmConfig.cbThermostat.DataSource = dt;
            frmConfig.cbThermostat.DisplayMember = "DigitDescription";
            frmConfig.cbThermostat.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(55, 1);
                frmConfig.cbThermostat.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 57 - Altitude 
            
            dt = objMain.GetModelNumDesc(57, "NA", null, unitTypeStr);
            frmConfig.cbAltitude.DataSource = dt;
            frmConfig.cbAltitude.DisplayMember = "DigitDescription";
            frmConfig.cbAltitude.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(56, 1);
                frmConfig.cbAltitude.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
            
            // Digit 58 - Condensate Overflow Switch 
            
            dt = objMain.GetModelNumDesc(58, "NA", null, unitTypeStr);
            frmConfig.cbCondensateOverflowSwitch.DataSource = dt;
            frmConfig.cbCondensateOverflowSwitch.DisplayMember = "DigitDescription";
            frmConfig.cbCondensateOverflowSwitch.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(57, 1);
                frmConfig.cbCondensateOverflowSwitch.SelectedIndex = GetRowIndex(dt, digitValStr);
            }            

            // Digit 59 - Freezstat 
            
            dt = objMain.GetModelNumDesc(59, "NA", null, unitTypeStr);
            frmConfig.cbFreezstat.DataSource = dt;
            frmConfig.cbFreezstat.DisplayMember = "DigitDescription";
            frmConfig.cbFreezstat.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(58, 1);
                frmConfig.cbFreezstat.SelectedIndex = GetRowIndex(dt, digitValStr);
            }            

            // Digit 61 - Chilled Water Type
            
            dt = objMain.GetModelNumDesc(61, "NA", null, unitTypeStr);
            frmConfig.cbOutdoorCoilFluidType.DataSource = dt;
            frmConfig.cbOutdoorCoilFluidType.DisplayMember = "DigitDescription";
            frmConfig.cbOutdoorCoilFluidType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(60, 1);
                frmConfig.cbOutdoorCoilFluidType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 62 - Minumum Damper Leakage

            dt = objMain.GetModelNumDesc(62, "NA", null, unitTypeStr);
            frmConfig.cbMinimumDamperLeakage.DataSource = dt;
            frmConfig.cbMinimumDamperLeakage.DisplayMember = "DigitDescription";
            frmConfig.cbMinimumDamperLeakage.ValueMember = "DigitVal";

            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(61, 1);
                frmConfig.cbMinimumDamperLeakage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            if (unitTypeStr.Contains("OA") == true)
            {
                frmConfig.Text = "Outdoor Air Unit --> " + modelNoStr;
            }
            else
            {
                frmConfig.Text = "Mixed Air Unit --> " + modelNoStr;

                // Digit 63/64 - MUC600 Hardware Template

                frmConfig.lbDigit63Desc.Text = "UC600 Hardware Template (63/64)";
                dt = objMain.GetModelNumDesc(6364, "NA", null, unitTypeStr);
                frmConfig.cbDigit63.DataSource = dt;
                frmConfig.cbDigit63.DisplayMember = "DigitDescription";
                frmConfig.cbDigit63.ValueMember = "DigitVal";
                if (modelNoValuesPresent)
                {
                    digitValStr = modelNoStr.Substring(62, 2);
                    frmConfig.cbDigit63.SelectedIndex = GetRowIndex(dt, digitValStr);
                }
                frmConfig.cbDigit63.Enabled = true;

                // Digit 65 - Electric Heat Capacity - Primary

                frmConfig.lbDigit64Desc.Text = "Electric Heat Capacity - Primary (65)";
                dt = objMain.GetModelNumDesc(65, "ELEC", null, unitTypeStr);
                frmConfig.cbDigit64.DataSource = dt;
                frmConfig.cbDigit64.DisplayMember = "DigitDescription";
                frmConfig.cbDigit64.ValueMember = "DigitVal";
                if (modelNoValuesPresent)
                {
                    digitValStr = modelNoStr.Substring(64, 1);
                    frmConfig.cbDigit64.SelectedIndex = GetRowIndex(dt, digitValStr);
                }
                frmConfig.cbDigit64.Enabled = true;

                // Digit 66 - Hot Water Coil Design - Primary

                frmConfig.lbDigit65Desc.Text = "Hot Water Coil Design - Primary (66)";
                dt = objMain.GetModelNumDesc(66, "NA", null, unitTypeStr);
                frmConfig.cbDigit65.DataSource = dt;
                frmConfig.cbDigit65.DisplayMember = "DigitDescription";
                frmConfig.cbDigit65.ValueMember = "DigitVal";
                if (modelNoValuesPresent)
                {
                    digitValStr = modelNoStr.Substring(65, 1);
                    frmConfig.cbDigit65.SelectedIndex = GetRowIndex(dt, digitValStr);
                }
                frmConfig.cbDigit65.Enabled = true;

                // Digit 67 - Gas Heat Capacity - Secondary

                frmConfig.lbDigit66Desc.Text = "Gas Heat Capacity - Secondary (67)";
                dt = objMain.GetModelNumDesc(67, "NA", null, unitTypeStr);
                frmConfig.cbDigit66.DataSource = dt;
                frmConfig.cbDigit66.DisplayMember = "DigitDescription";
                frmConfig.cbDigit66.ValueMember = "DigitVal";
                if (modelNoValuesPresent)
                {
                    digitValStr = modelNoStr.Substring(66, 1);
                    frmConfig.cbDigit66.SelectedIndex = GetRowIndex(dt, digitValStr);
                }
                frmConfig.cbDigit66.Enabled = true;

                frmConfig.lbDigit67Desc.Text = "Future Use (68)";
                frmConfig.lbDigit68Desc.Text = "Future Use (69)";

                frmConfig.lbDigit69Desc.Visible = false;
                frmConfig.cbDigit69.Visible = false;

            }

            if (modelNoValuesPresent == false)
            {
                frmConfig.btnParseModelNo.Visible = true;
                frmConfig.lbEnterModelNo.Visible = true;
                frmConfig.lbModelNo.Text = "";
                frmConfig.txtModelNo.Visible = true;
                frmConfig.txtModelNo.Focus();
                frmConfig.txtModelNo.Select();
            }
            frmConfig.ShowDialog();

           
        }

        private void parseOldModelNoDisplayConfigForm(string modelNoStr, string jobNumStr, string custName, string unitDescStr, string orderDateStr, string shipDateStr,
                                                      string bomCreationDateStr, string bomCreateByStr, string lastUpdateDateStr, string lastUpdateByStr, string prodStartDateStr, string completeDateStr)
        {
            string digitValStr = "";
            string heatingTypeStr = "NA";
            string secHeatTypeStr = "NA";
            string oauTypeCode = "OAU123DKN";

            frmOldModelNoConfig frmConfig = new frmOldModelNoConfig();

            frmConfig.Text = modelNoStr;

            if (modelNoStr.Substring(0, 3) == "OAB" || modelNoStr.Substring(0, 3) == "OAG")
            {
                oauTypeCode = "OALBG";
            }                

            //frmConfig.labelConfigModelNo.Text = modelNoStr;    

            frmConfig.lbJobNum.Text = jobNumStr;
            frmConfig.lbOrderDate.Text = orderDateStr;
            frmConfig.lbShipDate.Text = shipDateStr;
            frmConfig.lbBOMCreationDate.Text = bomCreationDateStr;
            frmConfig.lbBomCreateBy.Text = bomCreateByStr;
            frmConfig.lbLastUpdateDate.Text = lastUpdateDateStr;
            frmConfig.lbLastUpdateBy.Text = lastUpdateByStr;
            frmConfig.lbProdStartDate.Text = prodStartDateStr;
            frmConfig.lbCompleteDate.Text = completeDateStr;           
            frmConfig.lbModelNo.Text = modelNoStr;
            frmConfig.lbCustName.Text = custName;
            frmConfig.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmConfig.BackColor = GlobalJob.EnvColor;

            // Digit 3 - Cabinet Size
            digitValStr = modelNoStr.Substring(2, 1);
            DataTable dt = objMain.GetOAU_ModelNoValues(3, "NA", oauTypeCode);
            frmConfig.cbCabinet.DataSource = dt;
            frmConfig.cbCabinet.DisplayMember = "DigitDescription";
            frmConfig.cbCabinet.ValueMember = "DigitVal";
            frmConfig.cbCabinet.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 4 - Major Design
            digitValStr = modelNoStr.Substring(3, 1);
            dt = objMain.GetOAU_ModelNoValues(4, "NA", oauTypeCode);
            frmConfig.cbMajorDesign.DataSource = dt;
            frmConfig.cbMajorDesign.DisplayMember = "DigitDescription";
            frmConfig.cbMajorDesign.ValueMember = "DigitVal";
            frmConfig.cbMajorDesign.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 567 - Cooling Capacity               
            digitValStr = modelNoStr.Substring(4, 3);
            dt = objMain.GetOAU_ModelNoValues(567, "NA", oauTypeCode);
            frmConfig.cbCoolingCapacity.DataSource = dt;
            frmConfig.cbCoolingCapacity.DisplayMember = "DigitDescription";
            frmConfig.cbCoolingCapacity.ValueMember = "DigitVal";
            frmConfig.cbCoolingCapacity.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 8 - Minor Design Sequence               
            digitValStr = modelNoStr.Substring(7, 1);
            dt = objMain.GetOAU_ModelNoValues(8, "NA", oauTypeCode);
            frmConfig.cbMinorDesign.DataSource = dt;
            frmConfig.cbMinorDesign.DisplayMember = "DigitDescription";
            frmConfig.cbMinorDesign.ValueMember = "DigitVal";
            frmConfig.cbMinorDesign.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 9 - Voltage
            digitValStr = modelNoStr.Substring(8, 1);
            dt = objMain.GetOAU_ModelNoValues(9, "NA", oauTypeCode);
            frmConfig.cbVoltage.DataSource = dt;
            frmConfig.cbVoltage.DisplayMember = "DigitDescription";
            frmConfig.cbVoltage.ValueMember = "DigitVal";
            frmConfig.cbVoltage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 11 - Indoor Coil Type
            digitValStr = modelNoStr.Substring(10, 1);
            dt = objMain.GetOAU_ModelNoValues(11, "NA", oauTypeCode);
            frmConfig.cbEvapType.DataSource = dt;
            frmConfig.cbEvapType.DisplayMember = "DigitDescription";
            frmConfig.cbEvapType.ValueMember = "DigitVal";
            frmConfig.cbEvapType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 12 - Hot Gas Reheat
            digitValStr = modelNoStr.Substring(11, 1);
            dt = objMain.GetOAU_ModelNoValues(12, "NA", oauTypeCode);
            frmConfig.cbHotGasReheat.DataSource = dt;
            frmConfig.cbHotGasReheat.DisplayMember = "DigitDescription";
            frmConfig.cbHotGasReheat.ValueMember = "DigitVal";
            frmConfig.cbHotGasReheat.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 13 - Compressor
            digitValStr = modelNoStr.Substring(12, 1);
            dt = objMain.GetOAU_ModelNoValues(13, "NA", oauTypeCode);
            frmConfig.cbCompressor.DataSource = dt;
            frmConfig.cbCompressor.DisplayMember = "DigitDescription";
            frmConfig.cbCompressor.ValueMember = "DigitVal";
            frmConfig.cbCompressor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 14 - Outdoor Coil Type
            digitValStr = modelNoStr.Substring(13, 1);
            dt = objMain.GetOAU_ModelNoValues(14, "NA", oauTypeCode);
            frmConfig.cbCondenser.DataSource = dt;
            frmConfig.cbCondenser.DisplayMember = "DigitDescription";
            frmConfig.cbCondenser.ValueMember = "DigitVal";
            frmConfig.cbCondenser.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
           
            if ((modelNoStr.Substring(13, 1) == "3" || modelNoStr.Substring(13, 1) == "8") || (modelNoStr.Substring(3, 1) == "E"))
            {
                frmConfig.lbUnitType.Text = "HeatPump";
            }
            else
            {
                frmConfig.lbUnitType.Text = "Standard";
            }

            // Digit 15 - Refridgerant Capacity Control
            digitValStr = modelNoStr.Substring(14, 1);
            dt = objMain.GetOAU_ModelNoValues(15, "NA", oauTypeCode);
            frmConfig.cbRefridgerantCapacityControl.DataSource = dt;
            frmConfig.cbRefridgerantCapacityControl.DisplayMember = "DigitDescription";
            frmConfig.cbRefridgerantCapacityControl.ValueMember = "DigitVal";
            frmConfig.cbRefridgerantCapacityControl.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 16 - Indoor Fan Motor
            digitValStr = modelNoStr.Substring(15, 1);
            dt = objMain.GetOAU_ModelNoValues(16, "NA", oauTypeCode);
            frmConfig.cbIndoorFanMotor.DataSource = dt;
            frmConfig.cbIndoorFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorFanMotor.ValueMember = "DigitVal";
            frmConfig.cbIndoorFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);           

            // Digit 17 -Indoor Fan Wheel Size
            digitValStr = modelNoStr.Substring(16, 1);
            dt = objMain.GetOAU_ModelNoValues(17, "NA", oauTypeCode);
            frmConfig.cbIndoorFanWheel.DataSource = dt;
            frmConfig.cbIndoorFanWheel.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorFanWheel.ValueMember = "DigitVal";
            frmConfig.cbIndoorFanWheel.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 18 - Indoor Fan Motor HP
            digitValStr = modelNoStr.Substring(17, 1);
            dt = objMain.GetOAU_ModelNoValues(18, "NA", oauTypeCode);
            frmConfig.cbIndoorFanMotorHP.DataSource = dt;
            frmConfig.cbIndoorFanMotorHP.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorFanMotorHP.ValueMember = "DigitVal";
            frmConfig.cbIndoorFanMotorHP.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            digitValStr = modelNoStr.Substring(19, 1);
            if (digitValStr == "A" || digitValStr == "E" || digitValStr == "G" || digitValStr == "T")
            {
                heatingTypeStr = "IF";
                if (digitValStr == "E")
                {
                    secHeatTypeStr = "DF";
                }
                else if (digitValStr == "G" || digitValStr == "T")
                {
                    secHeatTypeStr = "ELEC";
                }               
            }
            else if (digitValStr == "C" || digitValStr == "D" || digitValStr == "F" || digitValStr == "H" ||
                     digitValStr == "M" || digitValStr == "N")
            {
                heatingTypeStr = "ELEC";
                if (digitValStr == "F" || digitValStr == "M")
                {
                    secHeatTypeStr = "DF";
                }
                else if (digitValStr == "H" || digitValStr == "W")
                {
                    secHeatTypeStr = "ELEC";
                }               
            }
            else if (digitValStr == "J" || digitValStr == "P" || digitValStr == "Q")
            {
                heatingTypeStr = "HOTWATER";

                DataTable dtWater = objMain.GetEtlData(jobNumStr);

                if (dtWater.Rows.Count > 0)
                {
                    DataRow drWater = dtWater.Rows[0];
                    frmConfig.txtFlowRate.Text = drWater["FlowRate"].ToString();
                    frmConfig.txtEnteringTemp.Text = drWater["EnteringTemp"].ToString();                                                                            
                }
                frmConfig.gbHotWaterInputs.Visible = true; 

                if (digitValStr == "P")
                {
                    secHeatTypeStr = "DF";
                }
                else if (digitValStr == "Q" || digitValStr == "Y")
                {
                    secHeatTypeStr = "ELEC";
                }         
            }
            else if (digitValStr == "K" || digitValStr == "R" || digitValStr == "S")
            {
                heatingTypeStr = "STEAM";
                if (digitValStr == "R")
                {
                    secHeatTypeStr = "DF";
                }
                else if (digitValStr == "S" || digitValStr == "Z")
                {
                    secHeatTypeStr = "ELEC";
                }      
            }

            // Digit 20 - Heat Type
           
            dt = objMain.GetOAU_ModelNoValues(20, "NA", oauTypeCode);
            frmConfig.cbHeatType.DataSource = dt;
            frmConfig.cbHeatType.DisplayMember = "DigitDescription";
            frmConfig.cbHeatType.ValueMember = "DigitVal";
            frmConfig.cbHeatType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);           

            // Digit 21 - Fuel Type
            digitValStr = modelNoStr.Substring(20, 1);
            dt = objMain.GetOAU_ModelNoValues(21, "NA", oauTypeCode);
            frmConfig.cbFuelType.DataSource = dt;
            frmConfig.cbFuelType.DisplayMember = "DigitDescription";
            frmConfig.cbFuelType.ValueMember = "DigitVal";
            frmConfig.cbFuelType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            //Digit 22 - Heat Source Primary
            digitValStr = modelNoStr.Substring(21, 1);
            dt = objMain.GetOAU_ModelNoValues(22, heatingTypeStr, oauTypeCode);
            frmConfig.cbHeatSourcePrimary.DataSource = dt;
            frmConfig.cbHeatSourcePrimary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatSourcePrimary.ValueMember = "DigitVal";
            frmConfig.cbHeatSourcePrimary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);     

            // Digit 23 - Heat Source Secondary
            digitValStr = modelNoStr.Substring(22, 1);
            dt = objMain.GetOAU_ModelNoValues(23, secHeatTypeStr, oauTypeCode);
            frmConfig.cbHeatSourceSecondary.DataSource = dt;
            frmConfig.cbHeatSourceSecondary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatSourceSecondary.ValueMember = "DigitVal";
            frmConfig.cbHeatSourceSecondary.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 24 - Corrosive Env Package
            digitValStr = modelNoStr.Substring(23, 1);
            dt = objMain.GetOAU_ModelNoValues(24, "NA", oauTypeCode);
            frmConfig.cbCorrosiveEnvPackage.DataSource = dt;
            frmConfig.cbCorrosiveEnvPackage.DisplayMember = "DigitDescription";
            frmConfig.cbCorrosiveEnvPackage.ValueMember = "DigitVal";
            frmConfig.cbCorrosiveEnvPackage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 25,26 - Unit Controls
            digitValStr = modelNoStr.Substring(24, 2);
            dt = objMain.GetOAU_ModelNoValues(2526, "NA", oauTypeCode);
            frmConfig.cbUnitControls.DataSource = dt;
            frmConfig.cbUnitControls.DisplayMember = "DigitDescription";
            frmConfig.cbUnitControls.ValueMember = "DigitVal";
            frmConfig.cbUnitControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 27 - Powered Exhaust Fan Motor
            digitValStr = modelNoStr.Substring(26, 1);
            dt = objMain.GetOAU_ModelNoValues(27, "NA", oauTypeCode);
            frmConfig.cbPoweredExhaustFanMotor.DataSource = dt;
            frmConfig.cbPoweredExhaustFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbPoweredExhaustFanMotor.ValueMember = "DigitVal";
            frmConfig.cbPoweredExhaustFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 28 - Powered Exhaust Fan Wheel
            digitValStr = modelNoStr.Substring(27, 1);
            dt = objMain.GetOAU_ModelNoValues(28, "NA", oauTypeCode);
            frmConfig.cbPoweredExhaustFanWheel.DataSource = dt;
            frmConfig.cbPoweredExhaustFanWheel.DisplayMember = "DigitDescription";
            frmConfig.cbPoweredExhaustFanWheel.ValueMember = "DigitVal";
            frmConfig.cbPoweredExhaustFanWheel.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 29 - Powered Exhaust Fan Motor HP
            digitValStr = modelNoStr.Substring(28, 1);
            dt = objMain.GetOAU_ModelNoValues(29, "NA", oauTypeCode);
            frmConfig.cbPoweredExhaustFanMotorHP.DataSource = dt;
            frmConfig.cbPoweredExhaustFanMotorHP.DisplayMember = "DigitDescription";
            frmConfig.cbPoweredExhaustFanMotorHP.ValueMember = "DigitVal";
            frmConfig.cbPoweredExhaustFanMotorHP.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 30 - Trane PPS Compatibility
            digitValStr = modelNoStr.Substring(29, 1);
            dt = objMain.GetOAU_ModelNoValues(30, "NA", oauTypeCode);
            frmConfig.cbTranePPS_Compatibility.DataSource = dt;
            frmConfig.cbTranePPS_Compatibility.DisplayMember = "DigitDescription";
            frmConfig.cbTranePPS_Compatibility.ValueMember = "DigitVal";
            frmConfig.cbTranePPS_Compatibility.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 31 - ERV_HRV
            digitValStr = modelNoStr.Substring(30, 1);
            dt = objMain.GetOAU_ModelNoValues(31, "NA", oauTypeCode);
            frmConfig.cbERV_HRV.DataSource = dt;
            frmConfig.cbERV_HRV.DisplayMember = "DigitDescription";
            frmConfig.cbERV_HRV.ValueMember = "DigitVal";
            frmConfig.cbERV_HRV.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 32 - ERV Size
            digitValStr = modelNoStr.Substring(31, 1);
            dt = objMain.GetOAU_ModelNoValues(32, "NA", oauTypeCode);
            frmConfig.cbERVSize.DataSource = dt;
            frmConfig.cbERVSize.DisplayMember = "DigitDescription";
            frmConfig.cbERVSize.ValueMember = "DigitVal";
            frmConfig.cbERVSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 33 - Economizer
            digitValStr = modelNoStr.Substring(32, 1);
            dt = objMain.GetOAU_ModelNoValues(33, "NA", oauTypeCode);
            frmConfig.cbEconomizer.DataSource = dt;
            frmConfig.cbEconomizer.DisplayMember = "DigitDescription";
            frmConfig.cbEconomizer.ValueMember = "DigitVal";
            frmConfig.cbEconomizer.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 34 - Filtration Options
            digitValStr = modelNoStr.Substring(33, 1);
            dt = objMain.GetOAU_ModelNoValues(34, "NA", oauTypeCode);
            frmConfig.cbFiltrationOptions.DataSource = dt;
            frmConfig.cbFiltrationOptions.DisplayMember = "DigitDescription";
            frmConfig.cbFiltrationOptions.ValueMember = "DigitVal";
            frmConfig.cbFiltrationOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 35 - Energy Recovery Wheel Size
            digitValStr = modelNoStr.Substring(34, 1);
            dt = objMain.GetOAU_ModelNoValues(35, "NA", oauTypeCode);
            frmConfig.cbSmokeDetector.DataSource = dt;
            frmConfig.cbSmokeDetector.DisplayMember = "DigitDescription";
            frmConfig.cbSmokeDetector.ValueMember = "DigitVal";
            frmConfig.cbSmokeDetector.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 36 - Electrical Options
            digitValStr = modelNoStr.Substring(35, 1);
            dt = objMain.GetOAU_ModelNoValues(36, "NA", oauTypeCode);
            frmConfig.cbElectricalOptions.DataSource = dt;
            frmConfig.cbElectricalOptions.DisplayMember = "DigitDescription";
            frmConfig.cbElectricalOptions.ValueMember = "DigitVal";
            frmConfig.cbElectricalOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 37 - Damper Options
            digitValStr = modelNoStr.Substring(36, 1);
            dt = objMain.GetOAU_ModelNoValues(37, "NA", oauTypeCode);
            frmConfig.cbAirFlowMonitoring.DataSource = dt;
            frmConfig.cbAirFlowMonitoring.DisplayMember = "DigitDescription";
            frmConfig.cbAirFlowMonitoring.ValueMember = "DigitVal";
            frmConfig.cbAirFlowMonitoring.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 38 - Hailguard 
            digitValStr = modelNoStr.Substring(37, 1);
            dt = objMain.GetOAU_ModelNoValues(38, "NA", oauTypeCode);
            frmConfig.cbHailguard.DataSource = dt;
            frmConfig.cbHailguard.DisplayMember = "DigitDescription";
            frmConfig.cbHailguard.ValueMember = "DigitVal";
            frmConfig.cbHailguard.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 39 - Altitude 
            digitValStr = modelNoStr.Substring(38, 1);
            dt = objMain.GetOAU_ModelNoValues(39, "NA", oauTypeCode);
            frmConfig.cbAltitude.DataSource = dt;
            frmConfig.cbAltitude.DisplayMember = "DigitDescription";
            frmConfig.cbAltitude.ValueMember = "DigitVal";
            frmConfig.cbAltitude.SelectedIndex = objMain.GetRowIndex(dt, digitValStr); 

            frmConfig.ShowDialog();

        }

        private void parseMonitorModelNoDisplayConfigForm(string modelNoStr, string jobNumStr, string custName, 
                                                          string unitDescStr, string orderDateStr, string shipDateStr,
                                                          string bomCreationDateStr, string bomCreateByStr, 
                                                          string lastUpdateDateStr, string lastUpdateByStr, 
                                                          string prodStartDateStr, string completeDateStr)
        {
            string digitValStr = "";
            string heatingTypeStr = "NA";
            string secHeatTypeStr = "NA";

            bool modelNoPresent = true;

            frmMonitorModelNoConfig frmConfig = new frmMonitorModelNoConfig();

            frmConfig.Text = modelNoStr;
          
            //frmConfig.labelConfigModelNo.Text = modelNoStr;    

            frmConfig.btnParseModelNo.Visible = true;
            frmConfig.lbEnterModelNo.Visible = true;
            frmConfig.lbModelNo.Text = "";
            frmConfig.txtModelNo.Visible = true;
            frmConfig.txtModelNo.Focus();
            frmConfig.txtModelNo.Select();

            frmConfig.lbJobNum.Text = jobNumStr;
            frmConfig.lbOrderDate.Text = orderDateStr;
            frmConfig.lbShipDate.Text = shipDateStr;
            frmConfig.lbBOMCreationDate.Text = bomCreationDateStr;
            frmConfig.lbBomCreateBy.Text = bomCreateByStr;
            frmConfig.lbLastUpdateDate.Text = lastUpdateDateStr;
            frmConfig.lbLastUpdateBy.Text = lastUpdateByStr;
            frmConfig.lbProdStartDate.Text = prodStartDateStr;
            frmConfig.lbCompleteDate.Text = completeDateStr;            
            frmConfig.lbModelNo.Text = modelNoStr;
            frmConfig.lbCustName.Text = custName;
            frmConfig.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmConfig.BackColor = GlobalJob.EnvColor;

            if (modelNoStr.Length == 0)
            {
                modelNoPresent = false;
            }
            
            // Digit 1 - Unit Type
            
            DataTable dt = objMain.MonitorGetModelNoValues(1, "NA");
            frmConfig.cbUnitType.DataSource = dt;
            frmConfig.cbUnitType.DisplayMember = "DigitDescription";
            frmConfig.cbUnitType.ValueMember = "DigitVal";

            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(0, 1);
                frmConfig.cbUnitType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                if (digitValStr == "T")
                {
                    heatingTypeStr = "ELEC";
                }
                else
                {
                    heatingTypeStr = "GE";
                }
            }
            
            // Digit 2 - Efficiency            
            dt = objMain.MonitorGetModelNoValues(2, "NA");
            frmConfig.cbEfficiency.DataSource = dt;
            frmConfig.cbEfficiency.DisplayMember = "DigitDescription";
            frmConfig.cbEfficiency.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(1, 1);
                frmConfig.cbEfficiency.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 3 - Airflow Configuration     
            
            dt = objMain.MonitorGetModelNoValues(3, "NA");
            frmConfig.cbAirflowConfiguration.DataSource = dt;
            frmConfig.cbAirflowConfiguration.DisplayMember = "DigitDescription";
            frmConfig.cbAirflowConfiguration.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(2, 1);
                frmConfig.cbAirflowConfiguration.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 456 - CoolingCapacity 
            
            dt = objMain.MonitorGetModelNoValues(456, "NA");
            frmConfig.cbCoolingCapacity.DataSource = dt;
            frmConfig.cbCoolingCapacity.DisplayMember = "DigitDescription";
            frmConfig.cbCoolingCapacity.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(3, 3);
                frmConfig.cbCoolingCapacity.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 7 - MajorDesignSequence
           
            dt = objMain.MonitorGetModelNoValues(7, "NA");
            frmConfig.cbMajorDesignSequence.DataSource = dt;
            frmConfig.cbMajorDesignSequence.DisplayMember = "DigitDescription";
            frmConfig.cbMajorDesignSequence.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(6, 1);
                frmConfig.cbMajorDesignSequence.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 8 - Voltage
            
            dt = objMain.MonitorGetModelNoValues(8, "NA");
            frmConfig.cbVoltage.DataSource = dt;
            frmConfig.cbVoltage.DisplayMember = "DigitDescription";
            frmConfig.cbVoltage.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(7, 1);
                frmConfig.cbVoltage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 9 - UnitControls
            
            dt = objMain.MonitorGetModelNoValues(9, "NA");
            frmConfig.cbUnitControls.DataSource = dt;
            frmConfig.cbUnitControls.DisplayMember = "DigitDescription";
            frmConfig.cbUnitControls.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(8, 1);
                frmConfig.cbUnitControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 10 - HeatingCapacity
           
            dt = objMain.MonitorGetModelNoValues(10, heatingTypeStr);
            frmConfig.cbHeatingCapacity.DataSource = dt;
            frmConfig.cbHeatingCapacity.DisplayMember = "DigitDescription";
            frmConfig.cbHeatingCapacity.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(9, 1);
                frmConfig.cbHeatingCapacity.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 11 - cbMinorDesignSequence
            
            dt = objMain.MonitorGetModelNoValues(11, "NA");
            frmConfig.cbMinorDesignSequence.DataSource = dt;
            frmConfig.cbMinorDesignSequence.DisplayMember = "DigitDescription";
            frmConfig.cbMinorDesignSequence.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(10, 1);
                frmConfig.cbMinorDesignSequence.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 12,13 - ServiceSequence
            
            dt = objMain.MonitorGetModelNoValues(1213, "NA");
            frmConfig.cbServiceSequence.DataSource = dt;
            frmConfig.cbServiceSequence.DisplayMember = "DigitDescription";
            frmConfig.cbServiceSequence.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(11, 2);
                frmConfig.cbServiceSequence.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 14 - Fresh Air Selection
            
            dt = objMain.MonitorGetModelNoValues(14, "NA");
            frmConfig.cbFreshAirSelection.DataSource = dt;
            frmConfig.cbFreshAirSelection.DisplayMember = "DigitDescription";
            frmConfig.cbFreshAirSelection.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(13, 1);
                frmConfig.cbFreshAirSelection.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);               
            }            

            // Digit 15 -SupplyFanMotor
            
            dt = objMain.MonitorGetModelNoValues(15, "NA");
            frmConfig.cbSupplyFanMotor.DataSource = dt;
            frmConfig.cbSupplyFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanMotor.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(14, 1);
                frmConfig.cbSupplyFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 16 - HingedServiceAccessFilters
            
            dt = objMain.MonitorGetModelNoValues(16, "NA");
            frmConfig.cbHingedServiceAccessFilters.DataSource = dt;
            frmConfig.cbHingedServiceAccessFilters.DisplayMember = "DigitDescription";
            frmConfig.cbHingedServiceAccessFilters.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(15, 1);
                frmConfig.cbHingedServiceAccessFilters.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 17 - CondenserCoilProtection
            
            dt = objMain.MonitorGetModelNoValues(17, "NA");
            frmConfig.cbCondenserCoilProtection.DataSource = dt;
            frmConfig.cbCondenserCoilProtection.DisplayMember = "DigitDescription";
            frmConfig.cbCondenserCoilProtection.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(16, 1);
                frmConfig.cbCondenserCoilProtection.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 18 - ThruBaseProvisions
            
            dt = objMain.MonitorGetModelNoValues(18, heatingTypeStr);
            frmConfig.cbThruBaseProvisions.DataSource = dt;
            frmConfig.cbThruBaseProvisions.DisplayMember = "DigitDescription";
            frmConfig.cbThruBaseProvisions.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(17, 1);
                frmConfig.cbThruBaseProvisions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 19 - DisconnectSwitch
            
            dt = objMain.MonitorGetModelNoValues(19, "NA");
            frmConfig.cbDisconnectSwitch.DataSource = dt;
            frmConfig.cbDisconnectSwitch.DisplayMember = "DigitDescription";
            frmConfig.cbDisconnectSwitch.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(18, 1);
                frmConfig.cbDisconnectSwitch.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 20 - ConvenienceOutletOptions
            
            dt = objMain.MonitorGetModelNoValues(20, "NA");
            frmConfig.cbConvenienceOutletOptions.DataSource = dt;
            frmConfig.cbConvenienceOutletOptions.DisplayMember = "DigitDescription";
            frmConfig.cbConvenienceOutletOptions.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(19, 1);
                frmConfig.cbConvenienceOutletOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 21 - CommunicationsOptions
           
            dt = objMain.MonitorGetModelNoValues(21, "NA");
            frmConfig.cbCommunicationsOptions.DataSource = dt;
            frmConfig.cbCommunicationsOptions.DisplayMember = "DigitDescription";
            frmConfig.cbCommunicationsOptions.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(20, 1);
                frmConfig.cbCommunicationsOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            //Digit 22 - RefrigerationSystemOptions
            
            dt = objMain.MonitorGetModelNoValues(22, "NA");
            frmConfig.cbRefrigerationSystemOptions.DataSource = dt;
            frmConfig.cbRefrigerationSystemOptions.DisplayMember = "DigitDescription";
            frmConfig.cbRefrigerationSystemOptions.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(21, 1);
                frmConfig.cbRefrigerationSystemOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 23 - RefrigerationControls
            
            dt = objMain.MonitorGetModelNoValues(23, "NA");
            frmConfig.cbRefrigerationControls.DataSource = dt;
            frmConfig.cbRefrigerationControls.DisplayMember = "DigitDescription";
            frmConfig.cbRefrigerationControls.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(22, 1);
                frmConfig.cbRefrigerationControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 24 - SmokeDetector
            
            dt = objMain.MonitorGetModelNoValues(24, "NA");
            frmConfig.cbSmokeDetector.DataSource = dt;
            frmConfig.cbSmokeDetector.DisplayMember = "DigitDescription";
            frmConfig.cbSmokeDetector.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(23, 1);
                frmConfig.cbSmokeDetector.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 25- TraneSystemMonitoringControls
            
            dt = objMain.MonitorGetModelNoValues(25, "NA");
            frmConfig.cbTraneSystemMonitoringControls.DataSource = dt;
            frmConfig.cbTraneSystemMonitoringControls.DisplayMember = "DigitDescription";
            frmConfig.cbTraneSystemMonitoringControls.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(24, 1);
                frmConfig.cbTraneSystemMonitoringControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 26 - SystemMonitoringControls
            
            dt = objMain.MonitorGetModelNoValues(26, "NA");
            frmConfig.cbSystemMonitoringControls.DataSource = dt;
            frmConfig.cbSystemMonitoringControls.DisplayMember = "DigitDescription";
            frmConfig.cbSystemMonitoringControls.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(25, 1);
                frmConfig.cbSystemMonitoringControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 27 - UnitHardwareEnhancements
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(26, 1);
            }
            dt = objMain.MonitorGetModelNoValues(27, "NA");
            frmConfig.cbUnitHardwareEnhancements.DataSource = dt;
            frmConfig.cbUnitHardwareEnhancements.DisplayMember = "DigitDescription";
            frmConfig.cbUnitHardwareEnhancements.ValueMember = "DigitVal";
            frmConfig.cbUnitHardwareEnhancements.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 28 - Powered Exhaust Fan Wheel
            
            dt = objMain.MonitorGetModelNoValues(28, "NA");
            frmConfig.cbShortCircuitCurrentRating.DataSource = dt;
            frmConfig.cbShortCircuitCurrentRating.DisplayMember = "DigitDescription";
            frmConfig.cbShortCircuitCurrentRating.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(27, 1);
                frmConfig.cbShortCircuitCurrentRating.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 29,30 - Powered Exhaust Fan Motor HP
            //digitValStr = modelNoStr.Substring(28, 1);
            //dt = objMain.MonitorGetModelNoValues(29, "NA", oauTypeCode);
            //frmConfig.cbPoweredExhaustFanMotorHP.DataSource = dt;
            //frmConfig.cbPoweredExhaustFanMotorHP.DisplayMember = "DigitDescription";
            //frmConfig.cbPoweredExhaustFanMotorHP.ValueMember = "DigitVal";
            //frmConfig.cbPoweredExhaustFanMotorHP.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

            // Digit 31 - AdvancedUnitControls
            
            dt = objMain.MonitorGetModelNoValues(31, "NA");
            frmConfig.cbAdvancedUnitControls.DataSource = dt;
            frmConfig.cbAdvancedUnitControls.DisplayMember = "DigitDescription";
            frmConfig.cbAdvancedUnitControls.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(30, 1);
                frmConfig.cbAdvancedUnitControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 32 - PreHeaterOption
            
            dt = objMain.MonitorGetModelNoValues(32, "NA");
            frmConfig.cbPreHeaterOption.DataSource = dt;
            frmConfig.cbPreHeaterOption.DisplayMember = "DigitDescription";
            frmConfig.cbPreHeaterOption.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(31, 1);
                frmConfig.cbPreHeaterOption.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 33 - ExhaustFanMotor
           
            dt = objMain.MonitorGetModelNoValues(33, "NA");
            frmConfig.cbExhaustFanMotor.DataSource = dt;
            frmConfig.cbExhaustFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanMotor.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(32, 1);
                frmConfig.cbExhaustFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 34 - ExhaustFanMotorType
           
            dt = objMain.MonitorGetModelNoValues(34, "NA");
            frmConfig.cbExhaustFanMotorType.DataSource = dt;
            frmConfig.cbExhaustFanMotorType.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanMotorType.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(33, 1);
                frmConfig.cbExhaustFanMotorType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 35 - ExhaustFanSize
            
            dt = objMain.MonitorGetModelNoValues(35, "NA");
            frmConfig.cbExhaustFanSize.DataSource = dt;
            frmConfig.cbExhaustFanSize.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanSize.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(34, 1);
                frmConfig.cbExhaustFanSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 36 - ExhaustDampers
            
            dt = objMain.MonitorGetModelNoValues(36, "NA");
            frmConfig.cbExhaustDampers.DataSource = dt;
            frmConfig.cbExhaustDampers.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustDampers.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(35, 1);
                frmConfig.cbExhaustDampers.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 37 - EnergyRecoveryWheel
            
            dt = objMain.MonitorGetModelNoValues(37, "NA");
            frmConfig.cbEnergyRecoveryWheel.DataSource = dt;
            frmConfig.cbEnergyRecoveryWheel.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyRecoveryWheel.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(36, 1);
                frmConfig.cbEnergyRecoveryWheel.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 38 - EnergyRecoveryOptions 
            
            dt = objMain.MonitorGetModelNoValues(38, "NA");
            frmConfig.cbEnergyRecoveryOptions.DataSource = dt;
            frmConfig.cbEnergyRecoveryOptions.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyRecoveryOptions.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(37, 1);
                frmConfig.cbEnergyRecoveryOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 39 - SupplyDampers 
            
            dt = objMain.MonitorGetModelNoValues(39, "NA");
            frmConfig.cbSupplyDampers.DataSource = dt;
            frmConfig.cbSupplyDampers.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyDampers.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(38, 1);
                frmConfig.cbSupplyDampers.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 40 - SmokeDectectorOutdoorAirMon 
            
            dt = objMain.MonitorGetModelNoValues(40, "NA");
            frmConfig.cbSmokeDectectorOutdoorAirMon.DataSource = dt;
            frmConfig.cbSmokeDectectorOutdoorAirMon.DisplayMember = "DigitDescription";
            frmConfig.cbSmokeDectectorOutdoorAirMon.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(39, 1);
                frmConfig.cbSmokeDectectorOutdoorAirMon.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 41 - Pulley - Driver 
            
            dt = objMain.MonitorGetModelNoValues(41, "NA");
            frmConfig.cbPulleyDriver.DataSource = dt;
            frmConfig.cbPulleyDriver.DisplayMember = "DigitDescription";
            frmConfig.cbPulleyDriver.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(40, 1);
                frmConfig.cbPulleyDriver.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }            

            // Digit 42 - Belt 
           
            dt = objMain.MonitorGetModelNoValues(42, "NA");
            frmConfig.cbBelt.DataSource = dt;
            frmConfig.cbBelt.DisplayMember = "DigitDescription";
            frmConfig.cbBelt.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(41, 1);
                frmConfig.cbBelt.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }
            
            // Digit 43 - Pulley - Fan 
            
            dt = objMain.MonitorGetModelNoValues(43, "NA");
            frmConfig.cbPulleyFan.DataSource = dt;
            frmConfig.cbPulleyFan.DisplayMember = "DigitDescription";
            frmConfig.cbPulleyFan.ValueMember = "DigitVal";
            if (modelNoPresent)
            {
                digitValStr = modelNoStr.Substring(42, 1);
                frmConfig.cbPulleyFan.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);
            }

            frmConfig.menuStripConfig.Visible = true;
            frmConfig.ShowDialog();

        }

        private void parseMSP_ModelNoDisplayConfigForm(string modelNoStr, string jobNumStr, string custName, string unitDescStr, string orderDateStr, string shipDateStr,
                                                       string bomCreationDateStr, string bomCreateByStr, string lastUpdateDateStr, string lastUpdateByStr, string prodStartDateStr,
                                                       string completeDateStr, bool modelNoValuesPresent)
        {
            string digitValStr = "";
            string heatingTypeStr = "NA";
            string unitTypeStr = "MSP";           

            frmMSP_ModelNoConfig frmConfig = new frmMSP_ModelNoConfig();

            frmConfig.Text = modelNoStr;

            //frmConfig.labelConfigModelNo.Text = modelNoStr;    

            frmConfig.lbJobNum.Text = jobNumStr;
            frmConfig.lbOrderDate.Text = orderDateStr;
            frmConfig.lbShipDate.Text = shipDateStr;
            frmConfig.lbBOMCreationDate.Text = bomCreationDateStr;
            frmConfig.lbBomCreateBy.Text = bomCreateByStr;
            frmConfig.lbLastUpdateDate.Text = lastUpdateDateStr;
            frmConfig.lbLastUpdateBy.Text = lastUpdateByStr;
            frmConfig.lbProdStartDate.Text = prodStartDateStr;
            frmConfig.lbCompleteDate.Text = completeDateStr;
            //frmConfig.Text = unitDescStr;
            frmConfig.lbModelNo.Text = modelNoStr;
            frmConfig.lbCustName.Text = custName;
            frmConfig.lbEnvironment.Text = GlobalJob.EnvironmentStr;           
            frmConfig.BackColor = GlobalJob.EnvColor;

            // Digit 1 - UnitType

            DataTable dt = objMain.MSP_GetModelNoValues(1, "NA");
            frmConfig.cbUnitType.DataSource = dt;
            frmConfig.cbUnitType.DisplayMember = "DigitDescription";
            frmConfig.cbUnitType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(0, 1);
                frmConfig.cbUnitType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 2 - CabinetDesign

            dt = objMain.MSP_GetModelNoValues(2, heatingTypeStr);
            frmConfig.cbCabinetDesign.DataSource = dt;
            frmConfig.cbCabinetDesign.DisplayMember = "DigitDescription";
            frmConfig.cbCabinetDesign.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(1, 1);
                frmConfig.cbCabinetDesign.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 3456 - Model Size               

            dt = objMain.MSP_GetModelNoValues(3456, "NA");
            frmConfig.cbModelSize.DataSource = dt;
            frmConfig.cbModelSize.DisplayMember = "DigitDescription";
            frmConfig.cbModelSize.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(2, 4);
                frmConfig.cbModelSize.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 7 - Voltage               

            dt = objMain.MSP_GetModelNoValues(7, "NA");
            frmConfig.cbVoltage.DataSource = dt;
            frmConfig.cbVoltage.DisplayMember = "DigitDescription";
            frmConfig.cbVoltage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(6, 1);
                frmConfig.cbVoltage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 8 - MajorDesignSeq

            dt = objMain.MSP_GetModelNoValues(8, "NA");
            frmConfig.cbMajorDesignSeq.DataSource = dt;
            frmConfig.cbMajorDesignSeq.DisplayMember = "DigitDescription";
            frmConfig.cbMajorDesignSeq.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(7, 1);
                frmConfig.cbMajorDesignSeq.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 11 - CoilType

            dt = objMain.MSP_GetModelNoValues(11, "NA");
            frmConfig.cbCoilType.DataSource = dt;
            frmConfig.cbCoilType.DisplayMember = "DigitDescription";
            frmConfig.cbCoilType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(10, 1);
                frmConfig.cbCoilType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 12 - CoilSize

            dt = objMain.MSP_GetModelNoValues(12, "NA");
            frmConfig.cbCoilSize.DataSource = dt;
            frmConfig.cbCoilSize.DisplayMember = "DigitDescription";
            frmConfig.cbCoilSize.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(11, 1);
                frmConfig.cbCoilSize.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 13 - MSP_CoolingCoilFluidType

            dt = objMain.MSP_GetModelNoValues(13, "NA");
            frmConfig.cbMSP_CoolingCoilFluidType.DataSource = dt;
            frmConfig.cbMSP_CoolingCoilFluidType.DisplayMember = "DigitDescription";
            frmConfig.cbMSP_CoolingCoilFluidType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(12, 1);
                frmConfig.cbMSP_CoolingCoilFluidType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 14 - PostCoolingCoilType

            dt = objMain.MSP_GetModelNoValues(14, "NA");
            frmConfig.cbPostCoolingCoilType.DataSource = dt;
            frmConfig.cbPostCoolingCoilType.DisplayMember = "DigitDescription";
            frmConfig.cbPostCoolingCoilType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(13, 1);
                frmConfig.cbPostCoolingCoilType.SelectedIndex = GetRowIndex(dt, digitValStr);                              
            }

            // Digit 15 - PostCoolingCoilSize

            dt = objMain.MSP_GetModelNoValues(15, "NA");
            frmConfig.cbPostCoolingCoilSize.DataSource = dt;
            frmConfig.cbPostCoolingCoilSize.DisplayMember = "DigitDescription";
            frmConfig.cbPostCoolingCoilSize.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(14, 1);
                frmConfig.cbPostCoolingCoilSize.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 16 - PostHeatingCoilType

            dt = objMain.MSP_GetModelNoValues(16, "NA");
            frmConfig.cbPostHeatingCoilType.DataSource = dt;
            frmConfig.cbPostHeatingCoilType.DisplayMember = "DigitDescription";
            frmConfig.cbPostHeatingCoilType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(15, 1);
                frmConfig.cbPostHeatingCoilType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
                      
            // Digit 17 - PostHeatingCoilSize

            dt = objMain.MSP_GetModelNoValues(17, heatingTypeStr);
            frmConfig.cbPostHeatingCoilSize.DataSource = dt;
            frmConfig.cbPostHeatingCoilSize.DisplayMember = "DigitDescription";
            frmConfig.cbPostHeatingCoilSize.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(16, 1);
                frmConfig.cbPostHeatingCoilSize.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 18 HeatingCoilFluidType

            dt = objMain.MSP_GetModelNoValues(18, "NA");
            frmConfig.cbHeatingCoilFluidType.DataSource = dt;
            frmConfig.cbHeatingCoilFluidType.DisplayMember = "DigitDescription";
            frmConfig.cbHeatingCoilFluidType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(17, 1);
                frmConfig.cbHeatingCoilFluidType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 19 - HeatExchangerType           

            dt = objMain.MSP_GetModelNoValues(19, heatingTypeStr);
            frmConfig.cbHeatExchangerType.DataSource = dt;
            frmConfig.cbHeatExchangerType.DisplayMember = "DigitDescription";
            frmConfig.cbHeatExchangerType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(18, 1);
                frmConfig.cbHeatExchangerType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 21 - CabinetConstuctIndoor

            dt = objMain.MSP_GetModelNoValues(21, "NA");
            frmConfig.cbCabinetConstuctIndoor.DataSource = dt;
            frmConfig.cbCabinetConstuctIndoor.DisplayMember = "DigitDescription";
            frmConfig.cbCabinetConstuctIndoor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(20, 1);
                frmConfig.cbCabinetConstuctIndoor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 22 - CabinetConstructOutdoor

            dt = objMain.MSP_GetModelNoValues(22, "NA");
            frmConfig.cbCabinetConstructOutdoor.DataSource = dt;
            frmConfig.cbCabinetConstructOutdoor.DisplayMember = "DigitDescription";
            frmConfig.cbCabinetConstructOutdoor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(21, 1);
                frmConfig.cbCabinetConstructOutdoor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 23 - CoilCorrisionPackage

            dt = objMain.MSP_GetModelNoValues(23, "NA");
            frmConfig.cbCoilCorrisionPackage.DataSource = dt;
            frmConfig.cbCoilCorrisionPackage.DisplayMember = "DigitDescription";
            frmConfig.cbCoilCorrisionPackage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(22, 1);
                frmConfig.cbCoilCorrisionPackage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 24 - SupplyFanWheel

            dt = objMain.MSP_GetModelNoValues(24, "NA");
            frmConfig.cbSupplyFanWheel.DataSource = dt;
            frmConfig.cbSupplyFanWheel.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanWheel.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(23, 1);
                frmConfig.cbSupplyFanWheel.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 25 - SupplyFanMotorType

            dt = objMain.MSP_GetModelNoValues(25, "NA");
            frmConfig.cbSupplyFanMotorType.DataSource = dt;
            frmConfig.cbSupplyFanMotorType.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanMotorType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(24, 1);
                frmConfig.cbSupplyFanMotorType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 26 - SupplyFanMotorSize

            dt = objMain.MSP_GetModelNoValues(26, "NA");
            frmConfig.cbSupplyFanMotorSize.DataSource = dt;
            frmConfig.cbSupplyFanMotorSize.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanMotorSize.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(25, 1);
                frmConfig.cbSupplyFanMotorSize.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 27 - FilterOptions

            dt = objMain.MSP_GetModelNoValues(27, "NA");
            frmConfig.cbFilterOptions.DataSource = dt;
            frmConfig.cbFilterOptions.DisplayMember = "DigitDescription";
            frmConfig.cbFilterOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(26, 1);
                frmConfig.cbFilterOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 28 - BypassDamper

            dt = objMain.MSP_GetModelNoValues(28, "NA");
            frmConfig.cbBypassDamper.DataSource = dt;
            frmConfig.cbBypassDamper.DisplayMember = "DigitDescription";
            frmConfig.cbBypassDamper.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(27, 1);
                frmConfig.cbBypassDamper.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 29 - Altitude

            dt = objMain.MSP_GetModelNoValues(29, "NA");
            frmConfig.cbAltitude.DataSource = dt;
            frmConfig.cbAltitude.DisplayMember = "DigitDescription";
            frmConfig.cbAltitude.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(28, 1);
                frmConfig.cbAltitude.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 31 - AirflowMonitoring

            dt = objMain.MSP_GetModelNoValues(31, "NA");
            frmConfig.cbAirflowMonitoring.DataSource = dt;
            frmConfig.cbAirflowMonitoring.DisplayMember = "DigitDescription";
            frmConfig.cbAirflowMonitoring.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(30, 1);
                frmConfig.cbAirflowMonitoring.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 32 - Accessories

            dt = objMain.MSP_GetModelNoValues(32, "NA");
            frmConfig.cbAccessories.DataSource = dt;
            frmConfig.cbAccessories.DisplayMember = "DigitDescription";
            frmConfig.cbAccessories.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(31, 1);
                frmConfig.cbAccessories.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 33 - SmokeDetector

            dt = objMain.MSP_GetModelNoValues(33, "NA");
            frmConfig.cbSmokeDetector.DataSource = dt;
            frmConfig.cbSmokeDetector.DisplayMember = "DigitDescription";
            frmConfig.cbSmokeDetector.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(32, 1);
                frmConfig.cbSmokeDetector.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 34 - CondensateOverflowSwitch

            dt = objMain.MSP_GetModelNoValues(34, "NA");
            frmConfig.cbCondensateOverflowSwitch.DataSource = dt;
            frmConfig.cbCondensateOverflowSwitch.DisplayMember = "DigitDescription";
            frmConfig.cbCondensateOverflowSwitch.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(33, 1);
                frmConfig.cbCondensateOverflowSwitch.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 35 Controls

            dt = objMain.MSP_GetModelNoValues(35, "NA");
            frmConfig.cbControls.DataSource = dt;
            frmConfig.cbControls.DisplayMember = "DigitDescription";
            frmConfig.cbControls.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(34, 1);
                frmConfig.cbControls.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 36 - ControlsDisplay

            dt = objMain.MSP_GetModelNoValues(36, "NA");
            frmConfig.cbControlsDisplay.DataSource = dt;
            frmConfig.cbControlsDisplay.DisplayMember = "DigitDescription";
            frmConfig.cbControlsDisplay.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(35, 1);
                frmConfig.cbControlsDisplay.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 37 - ElectricalOptions 

            dt = objMain.MSP_GetModelNoValues(37, "NA");
            frmConfig.cbElectricalOptions.DataSource = dt;
            frmConfig.cbElectricalOptions.DisplayMember = "DigitDescription";
            frmConfig.cbElectricalOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(36, 1);
                frmConfig.cbElectricalOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 38 - ConvenienceOutlet

            dt = objMain.MSP_GetModelNoValues(38, "NA");
            frmConfig.cbConvenienceOutlet.DataSource = dt;
            frmConfig.cbConvenienceOutlet.DisplayMember = "DigitDescription";
            frmConfig.cbConvenienceOutlet.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(37, 1);
                frmConfig.cbConvenienceOutlet.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 39 - ServiceLights

            dt = objMain.MSP_GetModelNoValues(39, "NA");
            frmConfig.cbServiceLights.DataSource = dt;
            frmConfig.cbServiceLights.DisplayMember = "DigitDescription";
            frmConfig.cbServiceLights.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(38, 1);
                frmConfig.cbServiceLights.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 41 - Warranty

            dt = objMain.MSP_GetModelNoValues(41, "NA");
            frmConfig.cbWarranty.DataSource = dt;
            frmConfig.cbWarranty.DisplayMember = "DigitDescription";
            frmConfig.cbWarranty.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(40, 1);
                frmConfig.cbWarranty.SelectedIndex = GetRowIndex(dt, digitValStr);
            }           

            if (modelNoValuesPresent == false)
            {
                frmConfig.btnParseModelNo.Visible = true;
                frmConfig.lbEnterModelNo.Visible = true;
                frmConfig.lbModelNo.Text = "";
                frmConfig.txtModelNo.Visible = true;
                frmConfig.txtModelNo.Focus();
                frmConfig.txtModelNo.Select();
            }
            frmConfig.ShowDialog();
        }        
      
        private int GetRowIndex(DataTable dt, string digitVal)
        {
            int retRowIdx = 0;

            foreach(DataRow dr in dt.Rows)
            {
                if (digitVal == dr["DigitVal"].ToString())
                {
                    retRowIdx = Int32.Parse(dr["RowIndex"].ToString()) - 1;
                    break;
                }
            }

            if (retRowIdx == -1)
            {
                retRowIdx = 0;
            }

            return retRowIdx;
        }

        private void colorCodeCells(DataGridView grd, DataGridViewRow row, Color backGround, Color foreGround)
        {
            DataGridViewCellStyle style = new DataGridViewCellStyle();

            for (int x = 0; x < 14; ++x)
            {
                style.BackColor = backGround;
                style.ForeColor = foreGround;
                row.Cells[x].Style = style;
            }
        }
        
        #endregion                     

        #region Reports
        private void partsByJobNumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";

            bool firstJob = true;


            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    if (firstJob)
                    {
                        jobNumLst = row.Cells["JobNum"].Value.ToString();
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
                    }
                }
            }

            frmSelectCategory frmSel = new frmSelectCategory();
            frmSel.lbJobNumListHidden.Text = jobNumLst;

            frmSel.ShowDialog();
        }

        private void jobsByDateRangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";
            string jobNumStr = "";
            string jobNumPremLst = "";
            string jobCompleteStr = "";
            string jobCreateDateStr = "";
            string noModelNoLst = "";
            string dateRangeJobNumLst = "";
            string modelNoStr = "";
            string tempDate;        

            DateTime StartDateDt;
            DateTime EndDateDt;
            DateTime jobStartDate;

            bool firstJob = true;
            bool firstJobPrem = true;
            bool firstNoModelNo = true;
            
            frmDateRange frmDR = new frmDateRange(this);
            frmDR.ShowDialog();

            if ((StartDate != null && EndDate != null))
            {
                tempDate = StartDate.ToShortDateString();
                StartDateDt = Convert.ToDateTime(tempDate);

                tempDate = EndDate.ToShortDateString();
                EndDateDt = Convert.ToDateTime(tempDate);

                DataTable dtDateRangeJobLst = objMain.GetOAU_JobsByDateRange(StartDateDt.Date, EndDateDt.Date);

                frmPrintBOM frmPrint = new frmPrintBOM();

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                foreach (DataRow row in dtDateRangeJobLst.Rows)
                {
                    jobNumStr = row["JobNum"].ToString();
                    jobStartDate = Convert.ToDateTime(row["StartDate"].ToString());
                    jobCompleteStr = row["JobComplete"].ToString();
                    jobCreateDateStr = row["CreateDate"].ToString();
                    modelNoStr = row["ModelNo"].ToString();

                    if (jobCompleteStr != "2" && !jobCreateDateStr.Contains("1/1/1900"))  // 2 indicates Job does not exist in Epicor and 1900 date means no Prelim Bom Exist
                    {
                        objMain.DeleteFromOA_PremliminaryBOM(jobNumStr);
                    }

                    if ((jobCompleteStr != "2") && (jobStartDate >= StartDateDt && jobStartDate <= EndDateDt))
                    {
                        if (firstJob)
                        {
                            jobNumLst = jobNumStr;
                            firstJob = false;
                        }
                        else
                        {
                            jobNumLst += ", " + jobNumStr;
                        }
                    }
                    else
                    {
                        if (modelNoStr.Length == 39 || modelNoStr.Length == 69)
                        {
                            if (jobCompleteStr == "2" && jobCreateDateStr.Contains("1/1/1900"))
                            {
                                objMain.GetCritCompParts_InsertOA_PremBOM(jobNumStr, modelNoStr);
                            }

                            if (jobStartDate >= StartDateDt && jobStartDate <= EndDateDt)
                            {
                                if (firstJobPrem)
                                {
                                    jobNumPremLst = jobNumStr;
                                    firstJobPrem = false;
                                }
                                else
                                {
                                    jobNumPremLst += ", " + jobNumStr;
                                }
                            }
                        }
                        else
                        {
                            if (jobStartDate >= StartDateDt && jobStartDate <= EndDateDt)
                            {
                                if (firstNoModelNo)
                                {
                                    noModelNoLst = jobNumStr;
                                    firstNoModelNo = false;
                                }
                                else
                                {
                                    noModelNoLst += ", " + jobNumStr;
                                }
                            }
                        }
                    }
                }

                dateRangeJobNumLst = "Epicor BOMs: " + jobNumLst + " Prelim BOMs: " + jobNumPremLst + " No ModelNo: " + noModelNoLst;

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@StartDate";
                pdv1.Value = StartDateDt;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@EndDate";
                pdv2.Value = EndDateDt;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                ParameterField pf3 = new ParameterField();
                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                pf3.Name = "@JobNumList";
                pdv3.Value = dateRangeJobNumLst;
                pf3.CurrentValues.Add(pdv3);

                paramFields.Add(pf3);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];      
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\CriticalCompPartsByDateRange.rpt";  
                //cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompPartsByDateRange.rpt");
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\CriticalCompPartsByDateRange.rpt"; 
#endif                            
                 
                cryRpt.Load(objMain.ReportString);
                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            else
            {
                MessageBox.Show("WARNING - No Start and/or End dates selected!");
            }
        }

        private void partsByJobNumberToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DateTime partsOrderRemovedDate = DateTime.Now;
            DateTime partsOrderedDate = DateTime.Now;

            string jobNumLst = "";
            string orderedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string partsOrderRemovedByStr = "";           

            bool firstJob = true;

            DataGridView dgvSelectedGrid = null;

            if (CurrentTabIdx == 0)
            {
                dgvSelectedGrid = dgvMainUnitList;
            }
            else if (CurrentTabIdx == 1)
            {
                dgvSelectedGrid = dgvMonitorMainList;
            }
            else if (CurrentTabIdx == 2)
            {
                dgvSelectedGrid = dgvRRU_MainList;
            }
            else if (CurrentTabIdx == 3)
            {
                dgvSelectedGrid = dgvMSP_MainList;
            }
            else if (CurrentTabIdx == 4)
            {
                dgvSelectedGrid = dgvViking_MainList;
            }

            frmPrintBOM frmPrint = new frmPrintBOM();

            foreach (DataGridViewRow row in dgvSelectedGrid.Rows)
            {
                if (row.Selected == true)
                {
                    if (firstJob)
                    {
                        jobNumLst += row.Cells["JobNum"].Value.ToString();
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
                    }
                }
            }

            if (jobNumLst != "")
            {
                objMain.UpdateOA_SchedulePartsOrdered(jobNumLst, 1, partsOrderedDate, orderedByStr, partsOrderRemovedDate, partsOrderRemovedByStr);
            }

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNumLst";
            pdv1.Value = jobNumLst;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\PurchasePartsByPartNum.rpt";
            cryRpt.Load(objMain.ReportString);

            //cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\PurchasePartsByPartNum.rpt");
            //cryRpt.Load(@"\\KCCWVPEPIC9APP\Apps\OAU\OAUConfiguratorcrystalreports\PurchasePartsByPartNum.rpt");

            //cryRpt.Load(@"F:\Projects\OAUConfig\PurchasePartsByPartNum.rpt");

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }

        private void oAUBOMPicklistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string oauDesc = "";

            bool firstJob = true;

            int selectedRowCount = 0;

            frmPrintBOM frmPrint = new frmPrintBOM();

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum");

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    var dr = dt.NewRow();
                    dr["JobNum"] = row.Cells["JobNum"].Value.ToString();
                    dt.Rows.Add(dr);
                    oauDesc = row.Cells["ModelNo"].Value.ToString();
                    ++selectedRowCount;
                }
            }

            if (selectedRowCount == 0)
            {
                //MessageBox.Show("ERROR! => You have selected job numbers. This report can only handle a single Job Number at a time.");
                MessageBox.Show("ERROR! => No Jobs have been selected! This report requires at least 1 Job to be selected from the list.");
            }
            else
            {
                frmPickListCategory frmPLC = new frmPickListCategory();
                frmPLC.dgvPickListJobs.DataSource = dt;

                DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
                checkColumn.Name = "L5LS";
                checkColumn.HeaderText = "L5LS";
                checkColumn.Width = 75;
                checkColumn.ReadOnly = false;
                checkColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;                        
                frmPLC.dgvPickListJobs.Columns.Add(checkColumn);

                DataGridViewCheckBoxColumn checkColumnOFS1 = new DataGridViewCheckBoxColumn();
                checkColumnOFS1.Name = "L5MS";
                checkColumnOFS1.HeaderText = "L5MS";
                checkColumnOFS1.Width = 75;
                checkColumnOFS1.ReadOnly = false;
                checkColumnOFS1.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumnOFS1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;                        
                frmPLC.dgvPickListJobs.Columns.Add(checkColumnOFS1);

                DataGridViewCheckBoxColumn checkColumn1 = new DataGridViewCheckBoxColumn();
                checkColumn1.Name = "L5PA";
                checkColumn1.HeaderText = "L5PA";
                checkColumn1.Width = 75;
                checkColumn1.ReadOnly = false;
                checkColumn1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;                        
                frmPLC.dgvPickListJobs.Columns.Add(checkColumn1);

                DataGridViewCheckBoxColumn checkColumnOFS2 = new DataGridViewCheckBoxColumn();
                checkColumnOFS2.Name = "L5RE";
                checkColumnOFS2.HeaderText = "L5RE";
                checkColumnOFS2.Width = 75;
                checkColumnOFS2.ReadOnly = false;
                checkColumnOFS2.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumnOFS2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmPLC.dgvPickListJobs.Columns.Add(checkColumnOFS2);

                //DataGridViewCheckBoxColumn checkColumnTCS = new DataGridViewCheckBoxColumn();
                //checkColumnTCS.Name = "TandemStn";
                //checkColumnTCS.HeaderText = "Tandem Comp Stn";
                //checkColumnTCS.Width = 105;
                //checkColumnTCS.ReadOnly = true;
                //checkColumnTCS.DefaultCellStyle.BackColor = Color.OrangeRed;
                ////checkColumn1.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumnTCS);

                //DataGridViewCheckBoxColumn checkColumn2 = new DataGridViewCheckBoxColumn();
                //checkColumn2.Name = "Station3";
                //checkColumn2.HeaderText = "Station 3";
                //checkColumn2.Width = 55;
                //checkColumn2.ReadOnly = false;
                //checkColumn2.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumn2);

                //DataGridViewCheckBoxColumn checkColumnOFS3 = new DataGridViewCheckBoxColumn();
                //checkColumnOFS3.Name = "OffsiteStn3";
                //checkColumnOFS3.HeaderText = "Offsite Stn 3";
                //checkColumnOFS3.Width = 75;
                //checkColumnOFS3.ReadOnly = true;
                //checkColumnOFS3.DefaultCellStyle.BackColor = Color.LightGreen;
                ////checkColumn1.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumnOFS3);

                //DataGridViewCheckBoxColumn checkColumn3 = new DataGridViewCheckBoxColumn();
                //checkColumn3.Name = "Station4";
                //checkColumn3.HeaderText = "Station 4";
                //checkColumn3.Width = 55;
                //checkColumn3.ReadOnly = false;
                ////checkColumn3.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumn3);

                //DataGridViewCheckBoxColumn checkColumnOFS4 = new DataGridViewCheckBoxColumn();
                //checkColumnOFS4.Name = "OffsiteStn4";
                //checkColumnOFS4.HeaderText = "Offsite Stn 4";
                //checkColumnOFS4.Width = 75;
                //checkColumnOFS4.ReadOnly = false;
                //checkColumnOFS4.DefaultCellStyle.BackColor = Color.LightGreen;
                ////checkColumn1.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumnOFS4);

                //DataGridViewCheckBoxColumn checkColumn4 = new DataGridViewCheckBoxColumn();
                //checkColumn4.Name = "Station5";
                //checkColumn4.HeaderText = "Station 5";
                //checkColumn4.Width = 55;
                //checkColumn4.ReadOnly = false;
                ////checkColumn4.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumn4);

                //DataGridViewCheckBoxColumn checkColumnOFS5 = new DataGridViewCheckBoxColumn();
                //checkColumnOFS5.Name = "OffsiteStn5";
                //checkColumnOFS5.HeaderText = "Offsite Stn 5";
                //checkColumnOFS5.Width = 75;
                //checkColumnOFS5.ReadOnly = false;
                //checkColumnOFS5.DefaultCellStyle.BackColor = Color.LightGreen;
                ////checkColumn1.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumnOFS5);

                //DataGridViewCheckBoxColumn checkColumn7 = new DataGridViewCheckBoxColumn();
                //checkColumn7.Name = "Station7";
                //checkColumn7.HeaderText = "Station 7";
                //checkColumn7.Width = 55;
                //checkColumn7.ReadOnly = false;
                ////checkColumn5.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumn7);

                //DataGridViewCheckBoxColumn checkColumnOFS7 = new DataGridViewCheckBoxColumn();
                //checkColumnOFS7.Name = "OffsiteStn7";
                //checkColumnOFS7.HeaderText = "Offsite Stn 7";
                //checkColumnOFS7.Width = 75;
                //checkColumnOFS7.ReadOnly = false;
                //checkColumnOFS7.DefaultCellStyle.BackColor = Color.LightGreen;
                ////checkColumn1.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                //frmPLC.dgvPickListJobs.Columns.Add(checkColumnOFS7);

                frmPLC.lbOAU_Desc.Text = oauDesc;

                frmPLC.ShowDialog();
            }
        }


        private void sheetMetalByJobNumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";
         
            bool firstJob = true;

            frmPrintBOM frmPrint = new frmPrintBOM();

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    if (firstJob)
                    {
                        jobNumLst = row.Cells["JobNum"].Value.ToString();
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
                    }
                }
            }

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNumLst";
            pdv1.Value = jobNumLst;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\SheetMetalNeededByJobNum.rpt";           
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\SheetMetalNeededByJobNum.rpt";            
#endif            

            cryRpt.Load(objMain.ReportString);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }

        private void sheetMetalByDateRangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";
            string jobNumStr = "";
            string jobCompleteStr = "";
            string jobCreateDateStr = "";
            string dateRangeJobNumLst = "";
            string modelNoStr = "";
            string tempDate;

            DateTime StartDateDt;
            DateTime EndDateDt;
            DateTime jobStartDate;

            bool firstJob = true;
           
            frmDateRange frmDR = new frmDateRange(this);
            frmDR.ShowDialog();


            if ((StartDate != null && EndDate != null))
            {
                tempDate = StartDate.ToShortDateString();
                StartDateDt = Convert.ToDateTime(tempDate);

                tempDate = EndDate.ToShortDateString();
                EndDateDt = Convert.ToDateTime(tempDate);

                DataTable dtDateRangeJobLst = objMain.GetOAU_JobsByDateRange(StartDateDt.Date, EndDateDt.Date);

                frmPrintBOM frmPrint = new frmPrintBOM();

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                foreach (DataRow row in dtDateRangeJobLst.Rows)
                {
                    jobNumStr = row["JobNum"].ToString();
                    jobStartDate = Convert.ToDateTime(row["StartDate"].ToString());
                    jobCompleteStr = row["JobComplete"].ToString();
                    jobCreateDateStr = row["CreateDate"].ToString();
                    modelNoStr = row["ModelNo"].ToString();

                    if (firstJob)
                    {
                        jobNumLst = jobNumStr;
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += ", " + jobNumStr;
                    }

                }

                dateRangeJobNumLst = jobNumLst;

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@StartDate";
                pdv1.Value = StartDateDt;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@EndDate";
                pdv2.Value = EndDateDt;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                ParameterField pf3 = new ParameterField();
                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                pf3.Name = "@JobNumList";
                pdv3.Value = dateRangeJobNumLst;
                pf3.CurrentValues.Add(pdv3);

                paramFields.Add(pf3);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\SheetMetalNeededByDateRange.rpt";                  
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\SheetMetalNeededByDateRange.rpt";
#endif

                cryRpt.Load(objMain.ReportString);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            else
            {
                MessageBox.Show("WARNING - No Start and/or End dates selected!");
            }
        }

        private void partDemandByDateRangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPartDemand frmPD = new frmPartDemand();

            frmPD.ShowDialog();
        }

        private void picklistByDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPrePicklist frmPick = new frmPrePicklist();
            frmPick.ShowDialog();
        }

        private void tandemCompressorJobsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DateTime startDate = DateTime.Now.AddDays(1);
            DateTime endDate;

            frmTandemComp frmTand = new frmTandemComp();

            if (startDate.DayOfWeek == DayOfWeek.Monday)
            {
                endDate = startDate.AddDays(5);
            }
            else if (startDate.DayOfWeek == DayOfWeek.Tuesday || startDate.DayOfWeek == DayOfWeek.Wednesday || startDate.DayOfWeek == DayOfWeek.Thursday ||
                     startDate.DayOfWeek == DayOfWeek.Friday)
            {
                endDate = startDate.AddDays(6);
            }
            else if (startDate.DayOfWeek == DayOfWeek.Saturday)
            {
                startDate = DateTime.Now.AddDays(2);
                endDate = startDate.AddDays(5);
            }
            else
            {
                startDate = DateTime.Now.AddDays(1);
                endDate = startDate.AddDays(5);
            }

            frmTand.dtpStartDate.Value = startDate;
            frmTand.dtpEndDate.Value = endDate;

            frmTand.ShowDialog();
        }

        private void tandemCompressorByJobNumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";

            bool firstJob = true;

            frmPrintBOM frmPrint = new frmPrintBOM();

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    if (firstJob)
                    {
                        jobNumLst += row.Cells["JobNum"].Value.ToString();
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
                    }
                }
            }

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNumList";
            pdv1.Value = jobNumLst;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\TandemAsmByJobNumber.rpt";                
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\TandemAsmByJobNumber.rpt";
#endif

            cryRpt.Load(objMain.ReportString);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }


        private void jobHistoryByJobNumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";

            bool firstJob = true;

            frmPrintBOM frmPrint = new frmPrintBOM();

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    if (firstJob)
                    {
                        jobNumLst += row.Cells["JobNum"].Value.ToString();
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
                    }
                }
            }

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNumList";
            pdv1.Value = jobNumLst;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\JobMtlHistoryByJobNum.rpt";                   
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\JobMtlHistoryByJobNum.rpt";
#endif

            cryRpt.Load(objMain.ReportString);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }

        private void jobGoodToGoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumStr = dgvMainUnitList.SelectedRows[0].Cells["JobNum"].Value.ToString();
            string jobNameStr = dgvMainUnitList.SelectedRows[0].Cells["CustomerName"].Value.ToString();
            //string startDateStr = "";

            DateTime origStartDate = DateTime.Now;

            if (dgvMainUnitList.SelectedRows[0].Cells["StartDate"].Value != null)
            {
                try
                {
                    origStartDate = Convert.ToDateTime(dgvMainUnitList.SelectedRows[0].Cells["StartDate"].Value.ToString());
                }
                catch (Exception ex)
                {

                }
            }

            frmPrintBOM frmPrint = new frmPrintBOM();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@JobName";
            pdv2.Value = jobNameStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@StartDate";
            pdv3.Value = origStartDate;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\JobGoodToGo.rpt";                 
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\JobGoodToGo.rpt";
#endif

            cryRpt.Load(objMain.ReportString);

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }

        private void sheetMetalSuperMarketPicklistToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            bool firstJob = true;

            int selectedRowCount = 0;

            frmPrintBOM frmPrint = new frmPrintBOM();

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum");

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    var dr = dt.NewRow();
                    dr["JobNum"] = row.Cells["JobNum"].Value.ToString();
                    dt.Rows.Add(dr);                   
                    ++selectedRowCount;
                }
            }

            if (selectedRowCount == 0)
            {
                //MessageBox.Show("ERROR! => You have selected job numbers. This report can only handle a single Job Number at a time.");
                MessageBox.Show("ERROR! => No Jobs have been selected! This report requires at least 1 Job to be selected from the list.");
            }
            else
            {

                frmSM_SuperMktStationList frmSub = new frmSM_SuperMktStationList();
                frmSub.dgvSuperMktJobs.DataSource = dt;

                DataGridViewCheckBoxColumn checkColumnAll = new DataGridViewCheckBoxColumn();
                checkColumnAll.Name = "AllStations";
                checkColumnAll.HeaderText = "All\nStations";
                checkColumnAll.Width = 70;
                checkColumnAll.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumnAll);              


                 DataGridViewCheckBoxColumn checkColumnWeld = new DataGridViewCheckBoxColumn();
                checkColumnWeld.Name = "WeldingStation";
                checkColumnWeld.HeaderText = "Weld";
                checkColumnWeld.Width = 50;
                checkColumnWeld.ReadOnly = false;                            
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumnWeld);              

                DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
                checkColumn.Name = "Station1";
                checkColumn.HeaderText = "ASY 1";
                checkColumn.Width = 50;
                checkColumn.ReadOnly = false;                            
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn);               

                DataGridViewCheckBoxColumn checkColumn1 = new DataGridViewCheckBoxColumn();
                checkColumn1.Name = "Station2";
                checkColumn1.HeaderText = "ASY 2";
                checkColumn1.Width = 50;
                checkColumn1.ReadOnly = false;                           
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn1);                             

                DataGridViewCheckBoxColumn checkColumn2 = new DataGridViewCheckBoxColumn();
                checkColumn2.Name = "Station3";
                checkColumn2.HeaderText = "ASY 3";
                checkColumn2.Width = 50;
                checkColumn2.ReadOnly = false;                           
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn2);               

                DataGridViewCheckBoxColumn checkColumn3 = new DataGridViewCheckBoxColumn();
                checkColumn3.Name = "Station4";
                checkColumn3.HeaderText = "ASY 4";
                checkColumn3.Width = 50;
                checkColumn3.ReadOnly = false;                          
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn3);
               
                DataGridViewCheckBoxColumn checkColumn4 = new DataGridViewCheckBoxColumn();
                checkColumn4.Name = "Station5";
                checkColumn4.HeaderText = "ASY 5";
                checkColumn4.Width = 50;
                checkColumn4.ReadOnly = false;                          
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn4);              

                DataGridViewCheckBoxColumn checkColumn6 = new DataGridViewCheckBoxColumn();
                checkColumn6.Name = "Station6";
                checkColumn6.HeaderText = "ASY 6";
                checkColumn6.Width = 50;
                checkColumn6.ReadOnly = false;                  
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn6);

                DataGridViewCheckBoxColumn checkColumn9A = new DataGridViewCheckBoxColumn();
                checkColumn9A.Name = "Station9";
                checkColumn9A.HeaderText = "ASY 9";
                checkColumn9A.Width = 50;
                checkColumn9A.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn9A);

                DataGridViewCheckBoxColumn checkColumn10A = new DataGridViewCheckBoxColumn();
                checkColumn10A.Name = "Station10";
                checkColumn10A.HeaderText = "ASY 10";
                checkColumn10A.Width = 50;
                checkColumn10A.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn10A);

                DataGridViewCheckBoxColumn checkColumn7 = new DataGridViewCheckBoxColumn();
                checkColumn7.Name = "SubStation1";
                checkColumn7.HeaderText = "SA 1";
                checkColumn7.Width = 50;
                checkColumn7.ReadOnly = false;                       
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn7);

                DataGridViewCheckBoxColumn checkColumn8 = new DataGridViewCheckBoxColumn();
                checkColumn8.Name = "SubStation2";
                checkColumn8.HeaderText = "SA 2";
                checkColumn8.Width = 50;
                checkColumn8.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn8);

                DataGridViewCheckBoxColumn checkColumn9 = new DataGridViewCheckBoxColumn();
                checkColumn9.Name = "SubStation3";
                checkColumn9.HeaderText = "SA 3";
                checkColumn9.Width = 50;
                checkColumn9.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn9);

                DataGridViewCheckBoxColumn checkColumn10 = new DataGridViewCheckBoxColumn();
                checkColumn10.Name = "SubStation4";
                checkColumn10.HeaderText = "SA 4";
                checkColumn10.Width = 50;
                checkColumn10.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn10);

                DataGridViewCheckBoxColumn checkColumn11 = new DataGridViewCheckBoxColumn();
                checkColumn11.Name = "SubStation5";
                checkColumn11.HeaderText = "SA 5";
                checkColumn11.Width = 50;
                checkColumn11.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn11);

                DataGridViewCheckBoxColumn checkColumn12 = new DataGridViewCheckBoxColumn();
                checkColumn12.Name = "SubStation6";
                checkColumn12.HeaderText = "SA 6";
                checkColumn12.Width = 50;
                checkColumn12.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn12);

                DataGridViewCheckBoxColumn checkColumn13 = new DataGridViewCheckBoxColumn();
                checkColumn13.Name = "SubStation7";
                checkColumn13.HeaderText = "SA 7";
                checkColumn13.Width = 50;
                checkColumn13.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn13);

                DataGridViewCheckBoxColumn checkColumn14 = new DataGridViewCheckBoxColumn();
                checkColumn14.Name = "SubStation8";
                checkColumn14.HeaderText = "SA 8";
                checkColumn14.Width = 50;
                checkColumn14.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn14);

                DataGridViewCheckBoxColumn checkColumn15 = new DataGridViewCheckBoxColumn();
                checkColumn15.Name = "SubStation9";
                checkColumn15.HeaderText = "SA 9";
                checkColumn15.Width = 50;
                checkColumn15.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn15);

                DataGridViewCheckBoxColumn checkColumn16 = new DataGridViewCheckBoxColumn();
                checkColumn16.Name = "SubStation10";
                checkColumn16.HeaderText = "SA 10";
                checkColumn16.Width = 50;
                checkColumn16.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn16);

                DataGridViewCheckBoxColumn checkColumn17 = new DataGridViewCheckBoxColumn();
                checkColumn17.Name = "SubStation11";
                checkColumn17.HeaderText = "SA 11";
                checkColumn17.Width = 50;
                checkColumn17.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn17);

                DataGridViewCheckBoxColumn checkColumn18 = new DataGridViewCheckBoxColumn();
                checkColumn18.Name = "SubStation12";
                checkColumn18.HeaderText = "SA 12";
                checkColumn18.Width = 50;
                checkColumn18.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn18);    

                DataGridViewCheckBoxColumn checkColumn19 = new DataGridViewCheckBoxColumn();
                checkColumn19.Name = "SubStation13";
                checkColumn19.HeaderText = "SA 13";
                checkColumn19.Width = 50;
                checkColumn19.ReadOnly = false;
                frmSub.dgvSuperMktJobs.Columns.Add(checkColumn19);    

                frmSub.ShowDialog();
            }
        }

         private void refrigerationComponentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIdx = dgvMainUnitList.CurrentRow.Index;

            string jobNum = dgvMainUnitList.Rows[rowIdx].Cells["JobNum"].Value.ToString();
            string modelNo = dgvMainUnitList.Rows[rowIdx].Cells["ModelNo"].Value.ToString();
            string oaRev = "REV5";

            bool validOA_Job = false;
           
            if (modelNo.Length == 39)
            {
                oaRev = "REV5";
                validOA_Job = true;
            }
            else if (modelNo.Length == 69)
            {
                oaRev = "REV6";
                validOA_Job = true;
            }
            else
            {
                validOA_Job = false;
                MessageBox.Show("ERROR - The selected Job Number is for a unit other than a OA Rev 5 or Rev 6!");
            }

            if (validOA_Job)
            {
                frmPrintBOM frmPrint = new frmPrintBOM();

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNum";
                pdv1.Value = jobNum;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@OA_Rev";
                pdv2.Value = oaRev;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);            

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\RefrigerationComponent.rpt";             
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\RefrigerationComponent.rpt";
#endif

                cryRpt.Load(objMain.ReportString);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            
        }

         private void sheetMetalBendByJobNumberToolStripMenuItem_Click(object sender, EventArgs e)
         {
             frmSheetMetalBend frmSht = new frmSheetMetalBend();
             frmSht.ShowDialog();
//             string jobNumLst = "";
//             string cabType = "";

//             int jobCount = 0;

//             bool firstJob = true;

//             frmPrintBOM frmPrint = new frmPrintBOM();

//             foreach (DataGridViewRow row in dgvMainUnitList.Rows)
//             {
//                 if (row.Selected == true)
//                 {
//                     ++jobCount;
//                     if (firstJob)
//                     {
//                         jobNumLst += row.Cells["JobNum"].Value.ToString();
//                         cabType = row.Cells["ModelNo"].Value.ToString().Substring(0, 3);
//                         firstJob = false;
//                     }
//                     else
//                     {
//                         jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
//                     }
//                     if (cabType != row.Cells["ModelNo"].Value.ToString().Substring(0, 3))
//                     {
//                         MessageBox.Show("ERROR - Different Cabinet Types selected!");
//                     }
//                 }
//             }

//             if (jobCount < 1)
//             {
//                 jobNumLst = null;
//             }


//             using (frmModalDateDialog frmDate = new frmModalDateDialog())
//             {
//                 if (frmDate.ShowDialog() == System.Windows.Forms.DialogResult.OK)
//                 {
//                     cabType += " of " + frmDate.RunDate;
//                 }
//             }

//             ReportDocument cryRpt = new ReportDocument();

//             ParameterValues curPV = new ParameterValues();

//             ParameterFields paramFields = new ParameterFields();

//             ParameterField pf1 = new ParameterField();
//             ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
//             pf1.Name = "@JobList";
//             pdv1.Value = jobNumLst;
//             pf1.CurrentValues.Add(pdv1);

//             paramFields.Add(pf1);

//             ParameterField pf2 = new ParameterField();
//             ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
//             pf2.Name = "@CabinetType";
//             pdv2.Value = cabType;
//             pf2.CurrentValues.Add(pdv2);

//             paramFields.Add(pf2);            

//             frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

//#if DEBUG
//            cryRpt.Load(@"\C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OA_Config_Rev6\OA_Config_Rev6\Reports\OAU_SheetMetalBendReport.rpt");
//#else
//             cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\OAU_SheetMetalBendReport.rpt");
//#endif

//             frmPrint.crystalReportViewer1.ReportSource = cryRpt;
//             frmPrint.ShowDialog();
//             frmPrint.crystalReportViewer1.Refresh();
         }

        #endregion                                            

        #region ETL Label Tool
        private void eTLLabelsToolStripMenuItem_Click(object sender, EventArgs e)
        {

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                     
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];            
#endif
            objMain.ProgPath = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\EtlLabel\\setup.exe";            
            System.Diagnostics.Process.Start(objMain.ProgPath);           
        }
        #endregion

        #region Rev 5 Part Maintenance tool
        private void rev5PartMainToolToolStripMenuItem_Click(object sender, EventArgs e)
        {

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                     
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];            
#endif
            objMain.ProgPath = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAU_PartMaintenanceTool\\setup.exe";            
            System.Diagnostics.Process.Start(objMain.ProgPath);                

        }
        #endregion

        #region Rev 6 Part Maintenance tool
        private void rev6PartMaintToolToolStripMenuItem_Click(object sender, EventArgs e)
        {            

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                     
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
#endif
            objMain.ProgPath = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OA_Rev6PartMaintenanceTool\\setup.exe";
            System.Diagnostics.Process.Start(objMain.ProgPath);                

        }
        #endregion

        #region ModelNo Configuration
        private void oAModelNoConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOldModelNoConfig frmConfig = new frmOldModelNoConfig();

            frmConfig.btnParseModelNo.Visible = true;
            frmConfig.lbEnterModelNo.Visible = true;
            frmConfig.lbModelNo.Text = "";
            frmConfig.txtModelNo.Visible = true;
            frmConfig.txtModelNo.Focus();
            frmConfig.txtModelNo.Select();

            frmConfig.ShowDialog();
        }

        private void oARev6ModelNoConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {           
            string customerNameStr = "";
            string modelNoStr = "";
            string unitDescStr = "";
            string bomCreationDateStr = "";
            string completeDateStr = "";
            string lastUpdateDateStr = "";
            string orderDateStr = "";
            string prodStartDateStr = "";
            string shipDateStr = "";
            string jobNumStr = "";
            string lastUpdByStr = "";
            string bomCreateByStr = "";
            string unitTypeStr = "OAU";

            parseModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr, bomCreationDateStr,
                                          bomCreateByStr, lastUpdateDateStr, lastUpdByStr, prodStartDateStr, completeDateStr, false, unitTypeStr);           
        }

        private void monitorModelNoConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMonitorModelNoConfig frmMon = new frmMonitorModelNoConfig();

            frmMon.btnParseModelNo.Visible = true;
            frmMon.lbEnterModelNo.Visible = true;
            frmMon.lbModelNo.Text = "";
            frmMon.txtModelNo.Visible = true;
            frmMon.txtModelNo.Focus();
            frmMon.txtModelNo.Select();

            frmMon.ShowDialog();

            //string customerNameStr = "";
            //string modelNoStr = ""; ;
            //string unitDescStr = "";
            //string bomCreationDateStr = "";
            //string completeDateStr = "";
            //string lastUpdateDateStr = "";
            //string orderDateStr = "";
            //string prodStartDateStr = "";
            //string shipDateStr = "";
            //string jobNumStr = "";
            //string lastModByStr = "";
            //string bomCreateByStr = "";

            //parseMonitorModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr,
            //                                        bomCreationDateStr, bomCreateByStr, lastUpdateDateStr, lastModByStr, prodStartDateStr, completeDateStr);
        }

        #endregion       

        #region RRU_DisplayBOM
        private void DisplayRRU_BOM()
        {
            string jobNumStr = "";
            string modelNoStr = "";
            string mcaStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "";
            string circuit2Charge = "";
            string partNumStr = "";
            string revisionNumStr = "";
            string custNameStr = "";

            //int rowIdx = dgvMainUnitList.CurrentRow.Index;

            frmDisplayBOM frmBOM = new frmDisplayBOM();

            jobNumStr = dgvMainUnitList.CurrentRow.Cells["JobNum"].Value.ToString();
            modelNoStr = dgvMainUnitList.CurrentRow.Cells["ModelNo"].Value.ToString();
            custNameStr = dgvMainUnitList.CurrentRow.Cells["CustomerName"].Value.ToString();

            frmBOM.lbVoltage.Text = "";

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "REV5");
            if (dtBOM.Rows.Count > 0) // Display Existing BOM found.
            {
                //mcaStr = objMain.CalculateAmpacityValues(dtBOM, modelNoStr, out mopStr, out rawMopStr, "REV6");
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = false;
                frmBOM.btnSave.Enabled = false;

                //DataTable dtETL = objMain.GetEtlData(jobNumStr);
                //if (dtETL.Rows.Count > 0)
                //{
                //    DataRow drETL = dtETL.Rows[0];
                //    circuit1Charge = drETL["Circuit1Charge"].ToString();
                //    circuit2Charge = drETL["Circuit2Charge"].ToString();
                //}


                dtBOM.DefaultView.Sort = "AssemblySeq";
                dtBOM = dtBOM.DefaultView.ToTable();
                frmBOM.dgvBOM.DataSource = dtBOM;
                frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
                frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
                frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
                frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
                frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
                frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
                frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
                frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
                frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
                frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
                frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
                frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
                frmBOM.dgvBOM.Columns["Description"].Width = 300;
                frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
                frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
                frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
                frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
                frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
                frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
                frmBOM.dgvBOM.Columns["UOM"].Width = 50;
                frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
                frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
                frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
                frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
                frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
                frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
                frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
                frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
                frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
                frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
                frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
                frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
                frmBOM.dgvBOM.Columns["Status"].Width = 75;
                frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
                frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
                frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
                frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
                frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
                frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
                frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
                frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
                frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
                frmBOM.dgvBOM.Columns["PartType"].Visible = false;
                frmBOM.dgvBOM.Columns["BuyToOrder"].Visible = false;
                frmBOM.dgvBOM.Columns["SubAssemblyPart"].Visible = false;
                frmBOM.dgvBOM.Columns["SubAsmMtlPart"].Visible = false;
                frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;
                frmBOM.dgvBOM.Columns["StationLoc"].Visible = false;
                frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;    
            }

            frmBOM.Text = "Outdoor Air Bill of Materials";
            //frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            //frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            //frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            //frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            //frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = dgvMainUnitList.CurrentRow.Cells["OrderDate"].Value.ToString();
            frmBOM.lbProdStartDate.Text = dgvMainUnitList.CurrentRow.Cells["StartDate"].Value.ToString();
            frmBOM.lbShipDate.Text = dgvMainUnitList.CurrentRow.Cells["ShipDate"].Value.ToString();
            frmBOM.lbJobNum.Text = jobNumStr;

            DataTable dt = objMain.MonitorGetModelNoDigitDesc(456, modelNoStr.Substring(3,3), "NA");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];               
                frmBOM.lbEnvironment.Text = dr["DigitDescription"].ToString();
            }

            frmBOM.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmBOM.BackColor = GlobalJob.EnvColor;
            frmBOM.lbCustName.Text = dgvMainUnitList.CurrentRow.Cells["CustomerName"].Value.ToString();
            frmBOM.lbModelNo.Text = dgvMainUnitList.CurrentRow.Cells["ModelNo"].Value.ToString();
            frmBOM.lbMCA.Text = mcaStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbUnitType.Text = modelNoStr.Substring(0,3);
            frmBOM.lbCir1Chrg.Text = circuit1Charge + " lbs";
            frmBOM.lbCir2Chrg.Text = circuit2Charge + " lbs";

            frmBOM.ShowDialog();
        }
        #endregion

        #region JobScheduling
        private void lineTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNum = "";
            string lineNum = "";

            int selRow = -1;
            int selCnt = 0;

            frmLineTranfer frmLT = new frmLineTranfer();

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    ++selCnt;
                }
            }

            if (selCnt > 1)
            {
                MessageBox.Show("ERROR = You can only select a single job when transferring lines!");
            }
            else
            {
                selRow = dgvMainUnitList.SelectedRows[0].Index;
                jobNum = dgvMainUnitList.Rows[selRow].Cells["JobNum"].Value.ToString();
                frmLT.lbJobNum.Text = jobNum;
                frmLT.lbCustName.Text = dgvMainUnitList.Rows[selRow].Cells["CustomerName"].Value.ToString();
                frmLT.lbModelNo.Text = dgvMainUnitList.Rows[selRow].Cells["ModelNo"].Value.ToString();

                DataTable dtJob = objTran.GetJobSchedulingDataByJobNum(jobNum);
                if (dtJob.Rows.Count > 0)
                {
                    //DataRow drJob = dtJob.Rows[0];
                    //frmLT.lbFromLineStartDate.Text = drJob["StartDate"].ToString();
                    //if (drJob["StartHour"] != null)
                    //{
                    //    frmLT.lbFromLineStartHour.Text = drJob["StartHour"].ToString();
                    //}

                    //frmLT.lbFromLineDueDate.Text = drJob["DueDate"].ToString();
                    //if (drJob["DueHour"] != null)
                    //{
                    //    frmLT.lbFromLineDueHour.Text = drJob["DueHour"].ToString();
                    //}
                }

                DataTable dt = objTran.GetProductionLineByJobNum(jobNum, 40);

                foreach (DataRow row in dt.Rows)
                {
                    if (row["ResourceID"] != DBNull.Value)
                    {
                        lineNum = row["ResourceID"].ToString();
                        if (lineNum.Substring(0, 2) == "L1")
                        {
                            frmLT.rbGrassFromLine1.Checked = true;
                            break;
                        }
                        else if (lineNum.Substring(0, 2) == "L2")
                        {
                            frmLT.rbGrassFromLine2.Checked = true;
                            break;
                        }
                        else if (lineNum.Substring(0, 2) == "L3")
                        {
                            frmLT.rbGrassFromLine3.Checked = true;
                            break;
                        }
                        else if (lineNum.Substring(0, 2) == "L4")
                        {
                            frmLT.rbGrassFromLine4.Checked = true;
                            break;
                        }
                        else if (lineNum.Substring(0, 2) == "LA")
                        {
                            frmLT.rbTechFromLineA.Checked = true;
                            break;
                        }
                        else if (lineNum.Substring(0, 2) == "LB")
                        {
                            frmLT.rbTechFromLineB.Checked = true;
                            break;
                        }
                        else if (lineNum.Substring(0, 2) == "LC")
                        {
                            frmLT.rbTechFromLineC.Checked = true;
                            break;
                        }
                        else if (lineNum.Substring(0, 2) == "LD")
                        {
                            frmLT.rbTechFromLineD.Checked = true;
                            break;
                        }
                    }
                }
                frmLT.ShowDialog();
            }
        }
        #endregion        

        private void scheduleJobToolStripMenuItem_Click(object sender, EventArgs e)
        {
            decimal totProdHrsDec = 0;

            string jobNum = "";
            string oprSeq = "";
            string asmSeq = "";
            string prodStd = "";
            string errorMsg = "";
            string startDate = "";
            string reqDueDate = "";

            bool errorFound = false;

            DateTime shipDateDT = DateTime.Now;
            DateTime reqDueDateDT = DateTime.Now;
            
            frmScheduleBatch frmSch = new frmScheduleBatch();

            DataTable dtJobs = new DataTable();
            dtJobs.Columns.Add("JobNum");
            dtJobs.Columns.Add("PartNum");
            dtJobs.Columns.Add("ProdLine");
            dtJobs.Columns.Add("ProdHrs");
            dtJobs.Columns.Add("ReqDueDate");
            dtJobs.Columns.Add("ShipDate");
            dtJobs.Columns.Add("StartDate");
            dtJobs.Columns.Add("StartHour");
            dtJobs.Columns.Add("EndDate");
            dtJobs.Columns.Add("EndHour");
            dtJobs.Columns.Add("SchedulingResult");

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {
                    totProdHrsDec = 0;
                    jobNum = row.Cells["JobNum"].Value.ToString();

                    DataTable dtJobHead = objMain.GetOA_JobHeadDataByJobNum(jobNum);
                    if (dtJobHead.Rows.Count > 0)
                    {

                        DataRow drJobHead = dtJobHead.Rows[0];

                        reqDueDate = drJobHead["ReqDueDate"].ToString();
                        reqDueDateDT = DateTime.Parse(reqDueDate);
                    }
                    else
                    {
                        errorMsg = "ERROR - Job# " + jobNum + " Does NOT exist in Epicor. This must be resolved before it can be scheduled.";
                        break;
                    }                 

                    DataTable dtJobOpDtl = objMain.GetJobOpDtlByJobNumAsmSeqOprSeq(jobNum, 0, 40);
                    if (dtJobOpDtl.Rows.Count > 0)
                    {
                        DataRow drJob = dtJobOpDtl.Rows[0];
                        
                       
                        if (drJob["StartHour"].ToString() != "0.00")
                        {                            
                            errorMsg = "ERROR - Job# " + jobNum + " is already scheduled in Epicor. This must be resolved before it can be scheduled.";
                        }

                        DataTable dtMtl = objMain.GetExistingBOMPartListByJobNum(jobNum, "");

                        if (dtMtl.Rows.Count == 0)
                        {
                             errorMsg = "ERROR - Job Num -> " + jobNum + " has NOT been configured!";
                            break;
                        }

                        DataTable dtJobOp = objMain.GetOA_JobOperations(jobNum);

                        if (dtJobOp.Rows.Count > 0)
                        {
                            foreach (DataRow drJobRow in dtJobOp.Rows)
                            {
                                asmSeq = drJobRow["AssemblySeq"].ToString();
                                oprSeq = drJobRow["OprSeq"].ToString();

                                if (asmSeq == "0")
                                {
                                    if (oprSeq != "10" && oprSeq != "20" && oprSeq != "35" && oprSeq != "36")
                                    {
                                        prodStd = drJobRow["ProdStandard"].ToString();
                                        totProdHrsDec += decimal.Parse(prodStd);
                                    }
                                }
                                //else if (asmSeq == "1")
                                //{
                                //    if (oprSeq == "10")
                                //    {
                                //        totProdHrsDec += 8;
                                //    }
                                //}
                                //else if (asmSeq == "2")
                                //{
                                //    if (oprSeq == "10")
                                //    {
                                //        totProdHrsDec += 1;
                                //    }
                                //}
                            }

                            var dr = dtJobs.NewRow();
                            dr["JobNum"] = jobNum;
                            dr["PartNum"] = row.Cells["PartNum"].Value.ToString();
                            dr["ProdLine"] = row.Cells["Line"].Value.ToString();
                            dr["ProdHrs"] = totProdHrsDec.ToString("N2");
                            dr["ReqDueDate"] = reqDueDateDT.ToShortDateString();
                            shipDateDT = (DateTime)row.Cells["ShipDate"].Value;
                            dr["ShipDate"] = shipDateDT.ToShortDateString();
                            dr["StartDate"] = "mm/dd/yyyy";
                            dr["StartHour"] = "hh:mm";
                            dr["EndDate"] = "";
                            dr["EndHour"] = "";
                            dr["SchedulingResult"] = "";
                            dtJobs.Rows.Add(dr);
                        }
                        else
                        {
                            errorMsg = "ERROR - Job# " + jobNum + " Does NOT have Job Operations. This must be resolved before it can be scheduled.";
                            break;
                        }
                    }
                    else
                    {
                        errorMsg = "ERROR - Job# " + jobNum + " Does NOT have Job Operations. This must be resolved before it can be scheduled.";     
                        break;
                    }                   

                }
            }

            if (errorMsg.Length == 0)
            {
                frmSch.dgvJobList.DataSource = dtJobs;

                frmSch.dgvJobList.Columns["ProdLine"].Width = 55;            // ProdLine
                frmSch.dgvJobList.Columns["ProdLine"].HeaderText = "ProdLine";
                frmSch.dgvJobList.Columns["ProdLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSch.dgvJobList.Columns["ProdLine"].ReadOnly = true;
                frmSch.dgvJobList.Columns["ProdLine"].DisplayIndex = 0;
                frmSch.dgvJobList.Columns["JobNum"].Width = 85;            // JobNum
                frmSch.dgvJobList.Columns["JobNum"].HeaderText = "JobNum";
                frmSch.dgvJobList.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSch.dgvJobList.Columns["JobNum"].ReadOnly = true;
                frmSch.dgvJobList.Columns["JobNum"].DisplayIndex = 1;
                frmSch.dgvJobList.Columns["PartNum"].Width = 75;           // PartNum
                frmSch.dgvJobList.Columns["PartNum"].HeaderText = "PartNum";
                frmSch.dgvJobList.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSch.dgvJobList.Columns["PartNum"].ReadOnly = true;
                frmSch.dgvJobList.Columns["PartNum"].DisplayIndex = 2;
                frmSch.dgvJobList.Columns["ProdHrs"].Width = 55;           // ProdHrs
                frmSch.dgvJobList.Columns["ProdHrs"].HeaderText = "ProdHrs";
                frmSch.dgvJobList.Columns["ProdHrs"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                frmSch.dgvJobList.Columns["ProdHrs"].ReadOnly = true;
                frmSch.dgvJobList.Columns["ProdHrs"].DefaultCellStyle.Format = "N2";
                frmSch.dgvJobList.Columns["ProdHrs"].DisplayIndex = 3;
                frmSch.dgvJobList.Columns["ReqDueDate"].Width = 80;            //ReqDueDate
                frmSch.dgvJobList.Columns["ReqDueDate"].HeaderText = "ReqDueDate";
                frmSch.dgvJobList.Columns["ReqDueDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmSch.dgvJobList.Columns["ReqDueDate"].ReadOnly = true;
                frmSch.dgvJobList.Columns["ReqDueDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
                frmSch.dgvJobList.Columns["ReqDueDate"].DisplayIndex = 4;
                frmSch.dgvJobList.Columns["ShipDate"].Width = 80;           // ShipDate   
                frmSch.dgvJobList.Columns["ShipDate"].HeaderText = "ShipDate";
                frmSch.dgvJobList.Columns["ShipDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmSch.dgvJobList.Columns["ShipDate"].ReadOnly = true;
                frmSch.dgvJobList.Columns["ShipDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
                frmSch.dgvJobList.Columns["ShipDate"].DisplayIndex = 5;
                frmSch.dgvJobList.Columns["StartDate"].Width = 80;            // StartDate         
                frmSch.dgvJobList.Columns["StartDate"].HeaderText = "StartDate";
                frmSch.dgvJobList.Columns["StartDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmSch.dgvJobList.Columns["StartDate"].DisplayIndex = 6;
                frmSch.dgvJobList.Columns["StartHour"].Width = 75;            // StartHour         
                frmSch.dgvJobList.Columns["StartHour"].HeaderText = "StartHour";
                frmSch.dgvJobList.Columns["StartHour"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmSch.dgvJobList.Columns["StartHour"].DisplayIndex = 7;
                frmSch.dgvJobList.Columns["SchedulingResult"].Width = 300;            // SchedulingResult         
                frmSch.dgvJobList.Columns["SchedulingResult"].HeaderText = "SchedulingResult";
                frmSch.dgvJobList.Columns["SchedulingResult"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSch.dgvJobList.Columns["SchedulingResult"].ReadOnly = true;
                frmSch.dgvJobList.Columns["SchedulingResult"].DisplayIndex = 8;
                frmSch.dgvJobList.Columns["EndDate"].Visible = false;
                frmSch.dgvJobList.Columns["EndHour"].Visible = false;      

                frmSch.lbMsg.Text = "Please input a valid 'StartDate' (mm/dd/yyyy) and 'StartHour' (hh:mm) for each of the selected jobs.";
                frmSch.ShowDialog();
            }
            else
            {
                MessageBox.Show(errorMsg);                
            }
        }

        private void scheduleExtractToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                     
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
#endif
            objMain.ProgPath = @"\\" + objMain.EpicorServer + "\\Apps\\Scripts\\OA_ScheduleExtract\\OA_ScheduleExtract_Console.exe";
            System.Diagnostics.Process.Start(objMain.ProgPath);                    
        }

        private void subAssemblyStationJobDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIdx = dgvMainUnitList.CurrentRow.Index;
            int selectedRowCount = 0;

            foreach (DataGridViewRow row in dgvMainUnitList.Rows)
            {
                if (row.Selected == true)
                {                   
                    ++selectedRowCount;
                }
            }

            if (selectedRowCount == 0)
            {
                //MessageBox.Show("ERROR! => You have selected job numbers. This report can only handle a single Job Number at a time.");
                MessageBox.Show("ERROR! => No Jobs have been selected! This report requires at least 1 Job to be selected from the list.");
            }
            else if (selectedRowCount > 1)
            { 
                MessageBox.Show("ERROR! => You have selected multiple job numbers. This report is setup to onlt handle 1 Job Number at a time.");
            }
            else
            {
                frmSubAssemblyStations frmSub = new frmSubAssemblyStations();

                frmSub.lbJobNum.Text = dgvMainUnitList.Rows[rowIdx].Cells["JobNum"].Value.ToString();
                frmSub.lbModelNo.Text = dgvMainUnitList.Rows[rowIdx].Cells["ModelNo"].Value.ToString();
                frmSub.lbCustName.Text = dgvMainUnitList.Rows[rowIdx].Cells["CustomerName"].Value.ToString();    
                frmSub.ShowDialog();
            }                                 
        }
       
        private void updateJobQtysByReleaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSheetMetalRelease frmSMR = new frmSheetMetalRelease();

            DataTable dtSMR = objMain.GetSheetMetalReleases();
            frmSMR.dgvSheetMetalRelease.DataSource = dtSMR;

            frmSMR.dgvSheetMetalRelease.Columns["ReleaseID"].Width = 125;
            frmSMR.dgvSheetMetalRelease.Columns["ReleaseID"].HeaderText = "ReleaseID";
            frmSMR.dgvSheetMetalRelease.Columns["ReleaseID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSMR.dgvSheetMetalRelease.Columns["ReleaseID"].DisplayIndex = 0;
            frmSMR.dgvSheetMetalRelease.Columns["CabinetType"].Width = 75;
            frmSMR.dgvSheetMetalRelease.Columns["CabinetType"].HeaderText = "CabinetType";
            frmSMR.dgvSheetMetalRelease.Columns["CabinetType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSMR.dgvSheetMetalRelease.Columns["CabinetType"].DisplayIndex = 1;
            frmSMR.dgvSheetMetalRelease.Columns["JobNumList"].Width = 600;
            frmSMR.dgvSheetMetalRelease.Columns["JobNumList"].HeaderText = "JobNumList";
            frmSMR.dgvSheetMetalRelease.Columns["JobNumList"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSMR.dgvSheetMetalRelease.Columns["JobNumList"].DisplayIndex = 2;
            frmSMR.dgvSheetMetalRelease.Columns["CreateBy"].Width = 80;
            frmSMR.dgvSheetMetalRelease.Columns["CreateBy"].HeaderText = "CreateBy";
            frmSMR.dgvSheetMetalRelease.Columns["CreateBy"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSMR.dgvSheetMetalRelease.Columns["CreateBy"].DisplayIndex = 3;
            frmSMR.dgvSheetMetalRelease.Columns["CreateDate"].Width = 125;
            frmSMR.dgvSheetMetalRelease.Columns["CreateDate"].HeaderText = "CreateDate";
            frmSMR.dgvSheetMetalRelease.Columns["CreateDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSMR.dgvSheetMetalRelease.Columns["CreateDate"].DisplayIndex = 4;       
     
            frmSMR.lbMainMsg.Text = "Double click on the desired release to update Metal usage";

            frmSMR.ShowDialog();
        }

        private void monitorPartMaintToolToolStripMenuItem_Click(object sender, EventArgs e)
        {

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                     
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
#endif
            objMain.ProgPath = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\MonitorPartMainenanceTool\\setup.exe";
            System.Diagnostics.Process.Start(objMain.ProgPath); 
           
        }
     
        private void mSPModelNoConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string customerNameStr = "";
            string modelNoStr = ""; ;
            string unitDescStr = "";
            string bomCreationDateStr = "";
            string completeDateStr = "";
            string lastUpdateDateStr = "";
            string orderDateStr = "";
            string prodStartDateStr = "";
            string shipDateStr = "";
            string jobNumStr = "";
            string lastUpdByStr = "";
            string bomCreateByStr = "";

            parseMSP_ModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr, bomCreationDateStr,
                                              bomCreateByStr, lastUpdateDateStr, lastUpdByStr, prodStartDateStr, completeDateStr, false);         
        }

        private void partAssemblyBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //frmPartAsmBOM frmAsm = new frmPartAsmBOM();

            //DataTable dt = objMain.GetParentPartNumbers();

            //frmAsm.cbParentPartNum.DataSource = dt;
            //frmAsm.cbParentPartNum.DisplayMember = "PartNumDesc";
            //frmAsm.cbParentPartNum.ValueMember = "PartNum";

            //foreach ()


        }

        private void newPicklistByStationToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            if (CurrentTabStr == "OAU")
            {
                displayNewPicklistForm(dgvMainUnitList);
            }
            else
            {
                displayNewPicklistForm(dgvViking_MainList);
            }
           
        }

        private void displayNewPicklistForm(DataGridView dgvMain)
        {
            string oauDesc = "";

            bool firstJob = true;

            int selectedRowCount = 0;

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum");

            foreach (DataGridViewRow row in dgvMain.Rows)
            {
                if (row.Selected == true)
                {
                    var dr = dt.NewRow();
                    dr["JobNum"] = row.Cells["JobNum"].Value.ToString();
                    dt.Rows.Add(dr);
                    oauDesc = row.Cells["ModelNo"].Value.ToString();
                    ++selectedRowCount;
                }
            }

            if (selectedRowCount == 0)
            {
                //MessageBox.Show("ERROR! => You have selected job numbers. This report can only handle a single Job Number at a time.");
                MessageBox.Show("ERROR! => No Jobs have been selected! This report requires at least 1 Job to be selected from the list.");
            }
            else
            {
                frmPicklistStations frmSTA = new frmPicklistStations();
                frmSTA.dgvPickListJobs.DataSource = dt;

                DataGridViewCheckBoxColumn checkColumnA = new DataGridViewCheckBoxColumn();
                checkColumnA.Name = "ALLSTAT";
                checkColumnA.HeaderText = "All\nStations";
                checkColumnA.Width = 50;
                checkColumnA.ReadOnly = false;
                checkColumnA.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmSTA.dgvPickListJobs.Columns.Add(checkColumnA);          

                DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
                checkColumn.Name = "ASY1";
                checkColumn.HeaderText = "ASY 1";
                checkColumn.Width = 45;
                checkColumn.ReadOnly = false;
                checkColumn.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn);               

                DataGridViewCheckBoxColumn checkColumn1 = new DataGridViewCheckBoxColumn();
                checkColumn1.Name = "ASY2";
                checkColumn1.HeaderText = "ASY 2";
                checkColumn1.Width = 45;
                checkColumn1.ReadOnly = false;
                checkColumn1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn1);

                DataGridViewCheckBoxColumn checkColumn2 = new DataGridViewCheckBoxColumn();
                checkColumn2.Name = "ASY3";
                checkColumn2.HeaderText = "ASY 3";
                checkColumn2.Width = 45;
                checkColumn2.ReadOnly = false;
                checkColumn2.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumn2.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn2);

                DataGridViewCheckBoxColumn checkColumn3 = new DataGridViewCheckBoxColumn();
                checkColumn3.Name = "ASY4";
                checkColumn3.HeaderText = "ASY 4";
                checkColumn3.Width = 45;
                checkColumn3.ReadOnly = false;
                checkColumn3.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn3);

                DataGridViewCheckBoxColumn checkColumn4 = new DataGridViewCheckBoxColumn();
                checkColumn4.Name = "ASY5";
                checkColumn4.HeaderText = "ASY 5";
                checkColumn4.Width = 45;
                checkColumn4.ReadOnly = false;
                checkColumn4.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumn4.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn4);

                DataGridViewCheckBoxColumn checkColumn5 = new DataGridViewCheckBoxColumn();
                checkColumn5.Name = "ASY6";
                checkColumn5.HeaderText = "ASY 6";
                checkColumn5.Width = 45;
                checkColumn5.ReadOnly = false;
                checkColumn5.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn5);

                DataGridViewCheckBoxColumn checkColumn6 = new DataGridViewCheckBoxColumn();
                checkColumn6.Name = "ASY7";
                checkColumn6.HeaderText = "ASY 7";
                checkColumn6.Width = 45;
                checkColumn6.ReadOnly = false;
                checkColumn6.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumn6.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn6);

                DataGridViewCheckBoxColumn checkColumn7 = new DataGridViewCheckBoxColumn();
                checkColumn7.Name = "ASY8";
                checkColumn7.HeaderText = "ASY 8";
                checkColumn7.Width = 45;
                checkColumn7.ReadOnly = false;
                checkColumn7.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn7);

                DataGridViewCheckBoxColumn checkColumn8 = new DataGridViewCheckBoxColumn();
                checkColumn8.Name = "ASY9";
                checkColumn8.HeaderText = "ASY 9";
                checkColumn8.Width = 45;
                checkColumn8.ReadOnly = false;
                checkColumn8.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumn8.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn8);

                DataGridViewCheckBoxColumn checkColumn9 = new DataGridViewCheckBoxColumn();
                checkColumn9.Name = "ASY10";
                checkColumn9.HeaderText = "ASY 10";
                checkColumn9.Width = 50;
                checkColumn9.ReadOnly = false;
                checkColumn9.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn9);

                DataGridViewCheckBoxColumn checkColumn10 = new DataGridViewCheckBoxColumn();
                checkColumn10.Name = "TEST";
                checkColumn10.HeaderText = "TEST";
                checkColumn10.Width = 50;
                checkColumn10.ReadOnly = false;
                checkColumn10.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumn10.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn10);

                DataGridViewCheckBoxColumn checkColumn10A = new DataGridViewCheckBoxColumn();
                checkColumn10A.Name = "CLOSE";
                checkColumn10A.HeaderText = "AUDIT\nCLOSE";
                checkColumn10A.Width = 50;
                checkColumn10A.ReadOnly = false;
                checkColumn10A.DefaultCellStyle.BackColor = Color.LightGreen;
                checkColumn10A.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values           
                frmSTA.dgvPickListJobs.Columns.Add(checkColumn10A);

                //DataGridViewCheckBoxColumn checkColumn11 = new DataGridViewCheckBoxColumn();
                //checkColumn11.Name = "SA01";
                //checkColumn11.HeaderText = "SA 01";
                //checkColumn11.Width = 45;
                //checkColumn11.ReadOnly = false;
                //checkColumn11.DefaultCellStyle.BackColor = Color.LightBlue;
                //checkColumn11.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn11);

                //DataGridViewCheckBoxColumn checkColumn12 = new DataGridViewCheckBoxColumn();
                //checkColumn12.Name = "SA02";
                //checkColumn12.HeaderText = "SA 02";
                //checkColumn12.Width = 45;
                //checkColumn12.ReadOnly = false;
                //checkColumn12.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn12);

                //DataGridViewCheckBoxColumn checkColumn13 = new DataGridViewCheckBoxColumn();
                //checkColumn13.Name = "SA03";
                //checkColumn13.HeaderText = "SA 03";
                //checkColumn13.Width = 45;
                //checkColumn13.ReadOnly = false;
                //checkColumn13.DefaultCellStyle.BackColor = Color.LightBlue;
                //checkColumn13.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn13);

                //DataGridViewCheckBoxColumn checkColumn14 = new DataGridViewCheckBoxColumn();
                //checkColumn14.Name = "SA04";
                //checkColumn14.HeaderText = "SA 04";
                //checkColumn14.Width = 45;
                //checkColumn14.ReadOnly = false;
                //checkColumn14.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn14);

                //DataGridViewCheckBoxColumn checkColumn15 = new DataGridViewCheckBoxColumn();
                //checkColumn15.Name = "SA05";
                //checkColumn15.HeaderText = "SA 05";
                //checkColumn15.Width = 45;
                //checkColumn15.ReadOnly = false;
                //checkColumn15.DefaultCellStyle.BackColor = Color.LightBlue;
                //checkColumn15.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn15);

                //DataGridViewCheckBoxColumn checkColumn16 = new DataGridViewCheckBoxColumn();
                //checkColumn16.Name = "SA06";
                //checkColumn16.HeaderText = "SA 06";
                //checkColumn16.Width = 45;
                //checkColumn16.ReadOnly = false;
                //checkColumn16.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn16);

                //DataGridViewCheckBoxColumn checkColumn17 = new DataGridViewCheckBoxColumn();
                //checkColumn17.Name = "SA07";
                //checkColumn17.HeaderText = "SA 07";
                //checkColumn17.Width = 45;
                //checkColumn17.ReadOnly = false;
                //checkColumn17.DefaultCellStyle.BackColor = Color.LightBlue;
                //checkColumn17.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn17);

                //DataGridViewCheckBoxColumn checkColumn18 = new DataGridViewCheckBoxColumn();
                //checkColumn18.Name = "SA08";
                //checkColumn18.HeaderText = "SA 08";
                //checkColumn18.Width = 45;
                //checkColumn18.ReadOnly = false;
                //checkColumn18.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn18);

                //DataGridViewCheckBoxColumn checkColumn19 = new DataGridViewCheckBoxColumn();
                //checkColumn19.Name = "SA09";
                //checkColumn19.HeaderText = "SA 09";
                //checkColumn19.Width = 45;
                //checkColumn19.ReadOnly = false;
                //checkColumn19.DefaultCellStyle.BackColor = Color.LightBlue;
                //checkColumn19.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn19);

                //DataGridViewCheckBoxColumn checkColumn20 = new DataGridViewCheckBoxColumn();
                //checkColumn20.Name = "SA10";
                //checkColumn20.HeaderText = "SA 10";
                //checkColumn20.Width = 45;
                //checkColumn20.ReadOnly = false;
                //checkColumn20.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn20);

                //DataGridViewCheckBoxColumn checkColumn21 = new DataGridViewCheckBoxColumn();
                //checkColumn21.Name = "SA11";
                //checkColumn21.HeaderText = "SA 11";
                //checkColumn21.Width = 45;
                //checkColumn21.ReadOnly = false;
                //checkColumn21.DefaultCellStyle.BackColor = Color.LightBlue;
                //checkColumn21.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn21);

                //DataGridViewCheckBoxColumn checkColumn22 = new DataGridViewCheckBoxColumn();
                //checkColumn22.Name = "SA12";
                //checkColumn22.HeaderText = "SA 12";
                //checkColumn22.Width = 45;
                //checkColumn22.ReadOnly = false;
                //checkColumn22.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn22);

                //DataGridViewCheckBoxColumn checkColumn23 = new DataGridViewCheckBoxColumn();
                //checkColumn23.Name = "SA13";
                //checkColumn23.HeaderText = "SA 13";
                //checkColumn23.Width = 45;
                //checkColumn23.ReadOnly = false;
                //checkColumn23.DefaultCellStyle.BackColor = Color.LightBlue;
                //checkColumn23.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //frmSTA.dgvPickListJobs.Columns.Add(checkColumn23);

                frmSTA.ShowDialog();
            }
        }

        private void vikingModelNoConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string customerNameStr = "";
            string modelNoStr = "";
            string unitDescStr = "";
            string bomCreationDateStr = "";
            string completeDateStr = "";
            string lastUpdateDateStr = "";
            string orderDateStr = "";
            string prodStartDateStr = "";
            string shipDateStr = "";
            string jobNumStr = "";
            string lastUpdByStr = "";
            string bomCreateByStr = "";
            string unitTypeStr = "VKG";

            parseModelNoDisplayConfigForm(modelNoStr, jobNumStr, customerNameStr, unitDescStr, orderDateStr, shipDateStr, bomCreationDateStr,
                                          bomCreateByStr, lastUpdateDateStr, lastUpdByStr, prodStartDateStr, completeDateStr, false, unitTypeStr);
        }

        private void vMEAndRFGASMPartsByJobNumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";                      

            bool firstJob = true;

            DataGridView dgvSelectedGrid = null;

            if (CurrentTabIdx == 0)
            {
                dgvSelectedGrid = dgvMainUnitList;
            }
            else if (CurrentTabIdx == 1)
            {
                dgvSelectedGrid = dgvMonitorMainList;
            }
            else if (CurrentTabIdx == 2)
            {
                dgvSelectedGrid = dgvRRU_MainList;
            }
            else if (CurrentTabIdx == 3)
            {
                dgvSelectedGrid = dgvMSP_MainList;
            }
            else if (CurrentTabIdx == 4)
            {
                dgvSelectedGrid = dgvViking_MainList;
            }

            frmPrintBOM frmPrint = new frmPrintBOM();

            foreach (DataGridViewRow row in dgvSelectedGrid.Rows)
            {
                if (row.Selected == true)
                {
                    if (firstJob)
                    {
                        jobNumLst += row.Cells["JobNum"].Value.ToString();
                        firstJob = false;
                    }
                    else
                    {
                        jobNumLst += "," + row.Cells["JobNum"].Value.ToString();
                    }
                }
            }

            if (jobNumLst != "")
            {               
                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNumLst";
                pdv1.Value = jobNumLst;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
                objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                
                //cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAUConfiguratorcrystalreports\VME_and_RFGASM_PartsMultiJob.rpt");
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
#endif
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\VME_and_RFGASM_PartsMultiJob.rpt";
                cryRpt.Load(objMain.ReportString);                

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            else
            {
                MessageBox.Show("ERROR - No jobs have been selected!");
            }
        }

        private void batchConfigureJobsToolStripMenuItem1_Click(object sender, EventArgs e)
        {           
            string modelNo = "";
            string errorMsg = "";
            string unitType = "";
           
            bool firstJob = true;
           
            DataGridView dgvSelectedGrid = null;

            //if (CurrentTabIdx == 0)
            //{
            //    dgvSelectedGrid = dgvMainUnitList;
            //}
            //else if (CurrentTabIdx == 1)
            //{
            //    MessageBox.Show("ERROR - Batch configuration not available for Monitor jobs!");
            //}
            //else if (CurrentTabIdx == 2)
            //{
            //    MessageBox.Show("ERROR - Batch configuration not available for RRU jobs!");
            //}
            //else if (CurrentTabIdx == 3)
            //{
            //    MessageBox.Show("ERROR - Batch configuration not available for MSP jobs!");
            //}
            //else if (CurrentTabIdx == 4)
            //{
            //    dgvSelectedGrid = dgvViking_MainList;
            //}
            
            DataTable dtSelectedJobs = new DataTable();
            dtSelectedJobs.Columns.Add("JobNum");
            dtSelectedJobs.Columns.Add("ModelNo");
            dtSelectedJobs.Columns.Add("UnitType");
            dtSelectedJobs.Columns.Add("Results");

            for (int x = 0; x < 2; ++x)
            {
                if (x == 0)
                {
                    dgvSelectedGrid = dgvMainUnitList;
                }
                else if (x == 1)
                {
                    dgvSelectedGrid = dgvViking_MainList;
                }

                foreach (DataGridViewRow row in dgvSelectedGrid.Rows)
                {
                    if (row.Selected == true)
                    {
                        if (row.Cells["ModelNo"].Value.ToString().Length != 39 && row.Cells["ModelNo"].Value.ToString().Length != 69)
                        {
                            if (firstJob == true)
                            {
                                firstJob = false;
                                errorMsg = "ERROR: The following jobs do not have a valid ModelNo:\n";
                                errorMsg += row.Cells["JobNum"].Value.ToString() + "\n";
                            }
                            else
                            {
                                errorMsg += row.Cells["JobNum"].Value.ToString() + "\n";
                            }
                        }
                        else
                        {                           
                            var dr = dtSelectedJobs.NewRow();
                            dr["JobNum"] = row.Cells["JobNum"].Value.ToString();
                            modelNo = row.Cells["ModelNo"].Value.ToString();
                            dr["ModelNo"] = modelNo;

                            if (modelNo.Length == 39)
                            {
                                unitType = "REV5";
                            }
                            else
                            {
                                if (modelNo.StartsWith("OA") == true)
                                {
                                    unitType = "REV6";
                                }
                                else
                                {
                                    unitType = "VKG";
                                }
                            }

                            dr["UnitType"] = unitType;
                            dr["Results"] = "";
                            dtSelectedJobs.Rows.Add(dr);
                        }
                    }
                }               
            }

            if (errorMsg.Length > 0)
            {
                MessageBox.Show(errorMsg);
            }
            else
            {
                frmBatchConfig frmBC = new frmBatchConfig();

                frmBC.dgvMainUnitList.DataSource = dtSelectedJobs;
                frmBC.dgvMainUnitList.Columns["JobNum"].Width = 80;            // JobNum
                frmBC.dgvMainUnitList.Columns["JobNum"].HeaderText = "JobNum";
                frmBC.dgvMainUnitList.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBC.dgvMainUnitList.Columns["JobNum"].DisplayIndex = 0;
                frmBC.dgvMainUnitList.Columns["ModelNo"].Width = 480;            // ModelNo
                frmBC.dgvMainUnitList.Columns["ModelNo"].HeaderText = "JobNum";
                frmBC.dgvMainUnitList.Columns["ModelNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBC.dgvMainUnitList.Columns["ModelNo"].DisplayIndex = 1;
                frmBC.dgvMainUnitList.Columns["UnitType"].Width = 75;           // UnitType
                frmBC.dgvMainUnitList.Columns["UnitType"].HeaderText = "Customer Name";
                frmBC.dgvMainUnitList.Columns["UnitType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBC.dgvMainUnitList.Columns["UnitType"].DisplayIndex = 2;
                frmBC.dgvMainUnitList.Columns["Results"].Width = 200;           // UnitType
                frmBC.dgvMainUnitList.Columns["Results"].HeaderText = "Configure Results";
                frmBC.dgvMainUnitList.Columns["Results"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmBC.dgvMainUnitList.Columns["Results"].DisplayIndex = 3;

                frmBC.lbEnvironment.Text = GlobalJob.EnvironmentStr;
                frmBC.BackColor = GlobalJob.EnvColor;
                frmBC.Show();               
            }
        }       

        #region SaveAPOB
        private void saveAllPartsOnBOM(string jobNumStr, string modelNoStr)
        {
            decimal totMaterialCostDec = 0;

            int jobDelSuccessInt = 0;
            int dash1Pos = 0;
            int dash2Pos = 0;

            bool saveBOMBool = true;

            string laborErrors = "";
            string oprSeq = "";
            string partNumStr = "";
            string revTypeStr = "REV5";

            string orderNumStr = "";
            string orderLineStr = "";
            string unitTypeStr = modelNoStr.Substring(0, 3);
            string msp_UnitTypes = "DD,DH,DU,DV";
            string lastUpdatedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string errorMsg = "";


            // Check each Part in the BOM to make sure it doesn't have a Zero cost.
//            foreach (DataGridViewRow dr in dgvBOM.Rows)
//            {
//                string unitCost = dr.Cells["UnitCost"].Value.ToString();
//                if (decimal.Parse(unitCost) == 0)
//                {
//                    if (!dr.Cells["PartNum"].Value.ToString().Contains("OAWARRANTY"))
//                    {
//                        // This was added to allow for zero cost in the development env but not prod.
//#if RELEASE
//                            createBOMBool = false;
//                            MessageBox.Show("ERROR -- Part# --> " + individualPartNumStr + " - has a zero value unit cost! You must resolve this " +
//                                            "before you can save this BOM");
//                            break;
//#endif
//                    }
//                }
//            }

//            dash1Pos = jobNumStr.IndexOf("-");
//            dash2Pos = jobNumStr.LastIndexOf("-");

//            if (dash1Pos > 0 && dash2Pos > 0)
//            {
//                orderNumStr = jobNumStr.Substring(0, dash1Pos);
//                orderLineStr = jobNumStr.Substring((dash1Pos + 1), (dash2Pos - (dash1Pos + 1)));
//            }
//            else
//            {
//                orderNumStr = jobNumStr;
//                orderLineStr = "0";
//            }

//            DataTable dtOrder = objMain.GetOrderDetail(orderNumStr, orderLineStr);
//            if (dtOrder.Rows.Count > 0)
//            {
//                DataRow drOrder = dtOrder.Rows[0];
//                partNumStr = drOrder["PartNum"].ToString();
//            }
//            else
//            {
//                if ((modelNoStr.Length == 69) && (msp_UnitTypes.Contains(modelNoStr.Substring(0, 2))))
//                {
//                    partNumStr = modelNoStr.Substring(0, 6);
//                }
//                else
//                {
//                    partNumStr = modelNoStr.Substring(0, 7);
//                }
//            }

//            if (modelNoStr.Substring(0, 2) == "OA")
//            {
//                if (modelNoStr.Length == 69)
//                {
//                    revTypeStr = "REV6";
//                }
//                else if (modelNoStr.Length == 39)
//                {
//                    revTypeStr = "REV5";
//                }
//            }
//            else if (modelNoStr.Substring(0, 2) == "HA")
//            {
//                revTypeStr = "VKG";
//            }
//            else if ((modelNoStr.Length == 69) && (msp_UnitTypes.Contains(modelNoStr.Substring(0, 2))))
//            {
//                revTypeStr = "MSP";
//            }
//            else if (partNumStr.Substring(0, 3) == "MON")
//            {
//                revTypeStr = "MON";
//            }

//            DataTable dtJobMtl = objMain.GetExistingBOMPartListByJobNum(jobNumStr, revTypeStr);

//            DataTable dtOauBom = objMain.GetOAU_BOMsData(jobNumStr);

//            DataTable dtLabor = objMain.GetLaborDetailDataByJobNum(jobNumStr);

//            if (dtJobMtl.Rows.Count == 0)
//            {
//                saveBOMBool = true;

//                if (dtOauBom.Rows.Count == 0)
//                {
//                    objMain.InsertOAU_BOMsData(jobNumStr, lastUpdatedByStr);   // Insert entry for Job into the R6_OA_BOMs
//                }
//                else
//                {
//                    objMain.UpdateOAU_BOMsData(jobNumStr, lastUpdatedByStr);
//                }

//                //lbMsg.Text = "Writing Job# " + jobNumStr + " to KCC.dbo.OAU_BOMs table!";
//                this.Refresh();
//            }
//            else
//            {
//                saveBOMBool = false;
//            }

//            if ((saveBOMBool == true) && (jobDelSuccessInt == 0))
//            {

//                //lbMsg.Text = "Writing Job# " + jobNumStr + " to epicor905.dbo.JobMtl table!";
//                this.Refresh();

//                int troubleCount = 0;

//                if (troubleCount == 10)
//                {
//                    MessageBox.Show("WARNING - Database Issue, database is running extremely slow, contact IT!");
//                }
//                else
//                {
//                    //errorMsg = saveBomDataIntoEpicor(jobNumStr, revTypeStr);                   

//                    //if (errorMsg.Length == 0)
//                    //{
//                    //    lbMsg.Text = "Adding Job# " + jobNumStr + " to KCC.dbo.R6_OA_EtlOAU table!";
//                    //    this.Refresh();
//                    //    addETLStickerData(jobNumStr, revTypeStr);

//                    //    ////validateJobOperations(jobNumStr, partNumStr, revTypeStr);

                       
//                    //}
//                }
//            }
        }

        private void swapPartsOnJobToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string modelNo = "";
            string errorMsg = "";
            string unitType = "";

            int rowIdx;

            bool firstJob = true;

            frmSwapPart frmSP = new frmSwapPart();

            DataGridView dgvSelectedGrid = null;          

            DataTable dtJobs = new DataTable();
            dtJobs.Columns.Add("JobNum");
            dtJobs.Columns.Add("ModelNo");
            dtJobs.Columns.Add("UnitType");
            dtJobs.Columns.Add("OldPartNum");
            dtJobs.Columns.Add("NewPartNum");           
            dtJobs.Columns.Add("Results");
           
            for (int x = 0; x < 2; ++x)
            {
                if (x == 0)
                {
                    dgvSelectedGrid = dgvMainUnitList;
                }
                else if (x == 1)
                {
                    dgvSelectedGrid = dgvViking_MainList;
                }

                foreach (DataGridViewRow row in dgvSelectedGrid.Rows)
                {                   
                    if (row.Selected == true)
                    {
                       modelNo = row.Cells["ModelNo"].Value.ToString();                           

                       if (modelNo.Length == 39)
                       {
                           unitType = "REV5";
                       }
                       else  if (modelNo.Length == 69)                            
                       {
                           if (modelNo.StartsWith("OA") == true)
                           {
                               unitType = "REV6";
                           }
                           else
                           {
                               unitType = "VKG";
                           }
                       }
                       else
                       {                          
                            errorMsg = "ERROR: This job do not have a valid ModelNo: ";
                            errorMsg += row.Cells["JobNum"].Value.ToString() + "\n";                           
                       }                                             

                       DataTable dtJobMtl = objMain.GetExistingBOMPartListByJobNum(row.Cells["JobNum"].Value.ToString(), unitType);

                       if (dtJobMtl.Rows.Count > 0)
                       {
                           var dr = dtJobs.NewRow();
                           dr["JobNum"] = row.Cells["JobNum"].Value.ToString();
                           dr["ModelNo"] = modelNo;
                           dr["UnitType"] = unitType;
                           dr["OldPartNum"] = "";
                           dr["NewPartNum"] = "";                          
                           dr["Results"] = "";
                           dtJobs.Rows.Add(dr);                                                                              
                       }
                       else
                       {
                           errorMsg = "ERROR: This job has not been configured: ";
                           errorMsg += row.Cells["JobNum"].Value.ToString() + "\n";    
                       }                       
                    }
                }
            }

            if (errorMsg.Length > 0)
            {
                MessageBox.Show(errorMsg);
            }
            else
            {
                frmSP.dgvSwapList.DataSource = dtJobs;              

                frmSP.dgvSwapList.Columns["JobNum"].Width = 75;            // JobNum
                frmSP.dgvSwapList.Columns["JobNum"].HeaderText = "JobNum";
                frmSP.dgvSwapList.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSP.dgvSwapList.Columns["JobNum"].ReadOnly = true;
                frmSP.dgvSwapList.Columns["JobNum"].DisplayIndex = 1;
                frmSP.dgvSwapList.Columns["ModelNo"].Width = 450;            // ModelNo
                frmSP.dgvSwapList.Columns["ModelNo"].HeaderText = "ModelNo";
                frmSP.dgvSwapList.Columns["ModelNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSP.dgvSwapList.Columns["ModelNo"].ReadOnly = true;
                frmSP.dgvSwapList.Columns["ModelNo"].DisplayIndex = 2;
                frmSP.dgvSwapList.Columns["UnitType"].Width = 50;           // UnitType
                frmSP.dgvSwapList.Columns["UnitType"].HeaderText = "UnitType";
                frmSP.dgvSwapList.Columns["UnitType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSP.dgvSwapList.Columns["UnitType"].ReadOnly = true;
                frmSP.dgvSwapList.Columns["UnitType"].DisplayIndex = 3;
                frmSP.dgvSwapList.Columns["OldPartNum"].Width = 130;           // OldPartNum
                frmSP.dgvSwapList.Columns["OldPartNum"].HeaderText = "OldPartNum";
                frmSP.dgvSwapList.Columns["OldPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSP.dgvSwapList.Columns["OldPartNum"].ReadOnly = false;
                frmSP.dgvSwapList.Columns["OldPartNum"].DisplayIndex = 4;
                frmSP.dgvSwapList.Columns["NewPartNum"].Width = 130;            //NewPartNum
                frmSP.dgvSwapList.Columns["NewPartNum"].HeaderText = "NewPartNum";
                frmSP.dgvSwapList.Columns["NewPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSP.dgvSwapList.Columns["NewPartNum"].ReadOnly = false;
                frmSP.dgvSwapList.Columns["NewPartNum"].DisplayIndex = 5;

                DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();  // PorcessRow Checkbox
                checkColumn.Name = "ProcessRow";
                checkColumn.HeaderText = "ProcessRow";
                checkColumn.Width = 75;
                checkColumn.ReadOnly = false;
                checkColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                checkColumn.TrueValue = true;
                frmSP.dgvSwapList.Columns.Add(checkColumn);
                frmSP.dgvSwapList.Columns["ProcessRow"].DisplayIndex = 0;                                       

                frmSP.dgvSwapList.Columns["Results"].Width = 240;            //Results
                frmSP.dgvSwapList.Columns["Results"].HeaderText = "Results";
                frmSP.dgvSwapList.Columns["Results"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                frmSP.dgvSwapList.Columns["Results"].DisplayIndex = 6;

                frmSP.lbMsg.Text = "Please check the Process Row checkbox for each job you want processed.";
                frmSP.lbMode.Text = "Swap";
                frmSP.lbEnvironment.Text = GlobalJob.EnvironmentStr;
                frmSP.BackColor = GlobalJob.EnvColor;
                frmSP.Show();
            }   
        }                 

        #endregion


        #region PartSearch

        private void findPartsOnJobsToolStripMenuItem_Click(object sender, EventArgs e)
        {                     
            btnSearch.Enabled = true;
            btnExitSearch.Enabled = true;
            gbPartSearch.BringToFront();
            gbPartSearch.Visible = true;            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string partNum = "";            

            DataTable dtJobs = new DataTable();

            DataTable dtResults = new DataTable();
            //dtResults.Columns.Add("ProcessRow");
            dtResults.Columns.Add("JobNum");
            dtResults.Columns.Add("ModelNo");
            dtResults.Columns.Add("UnitType");
            dtResults.Columns.Add("OldPartNum");
            dtResults.Columns.Add("NewPartNum");
            dtResults.Columns.Add("Results");

            foreach(DataGridViewRow row in dgvPartSearch.Rows)
            {
                if (row.Cells["PartNum"].Value != null)
                {
                    partNum = row.Cells["PartNum"].Value.ToString();

                    dtJobs = objMain.GetJobNumsByPartSearch(partNum.ToUpper());

                    foreach (DataRow dr in dtJobs.Rows)
                    {
                        var drow = dtResults.NewRow();
                        //drow["ProcessRow"] = false;
                        drow["JobNum"] = dr["JobNum"].ToString();
                        drow["ModelNo"] = dr["ModelNo"].ToString();
                        drow["UnitType"] = dr["UnitType"].ToString();
                        drow["OldPartNum"] = partNum.ToUpper();
                        drow["NewPartNum"] = "";
                        drow["Results"] = "";
                        dtResults.Rows.Add(drow);
                    }
                }
            }

            frmSwapPart frmSP = new frmSwapPart();

            frmSP.dgvSwapList.DataSource = dtResults;          

            frmSP.dgvSwapList.Columns["JobNum"].Width = 75;            // JobNum
            frmSP.dgvSwapList.Columns["JobNum"].HeaderText = "JobNum";
            frmSP.dgvSwapList.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSP.dgvSwapList.Columns["JobNum"].ReadOnly = true;
            frmSP.dgvSwapList.Columns["JobNum"].DisplayIndex = 1;
            frmSP.dgvSwapList.Columns["ModelNo"].Width = 450;            // ModelNo
            frmSP.dgvSwapList.Columns["ModelNo"].HeaderText = "ModelNo";
            frmSP.dgvSwapList.Columns["ModelNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSP.dgvSwapList.Columns["ModelNo"].ReadOnly = true;
            frmSP.dgvSwapList.Columns["ModelNo"].DisplayIndex = 2;
            frmSP.dgvSwapList.Columns["UnitType"].Width = 50;           // UnitType
            frmSP.dgvSwapList.Columns["UnitType"].HeaderText = "UnitType";
            frmSP.dgvSwapList.Columns["UnitType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSP.dgvSwapList.Columns["UnitType"].ReadOnly = true;
            frmSP.dgvSwapList.Columns["UnitType"].DisplayIndex = 3;
            frmSP.dgvSwapList.Columns["OldPartNum"].Width = 150;           // OldPartNum
            frmSP.dgvSwapList.Columns["OldPartNum"].HeaderText = "OldPartNum";
            frmSP.dgvSwapList.Columns["OldPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSP.dgvSwapList.Columns["OldPartNum"].ReadOnly = true;
            frmSP.dgvSwapList.Columns["OldPartNum"].DisplayIndex = 4;
            frmSP.dgvSwapList.Columns["NewPartNum"].Width = 150;            //NewPartNum
            frmSP.dgvSwapList.Columns["NewPartNum"].HeaderText = "NewPartNum";
            frmSP.dgvSwapList.Columns["NewPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSP.dgvSwapList.Columns["NewPartNum"].ReadOnly = false;
            frmSP.dgvSwapList.Columns["NewPartNum"].DisplayIndex = 5;

            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();  // PorcessRow Checkbox
            checkColumn.Name = "ProcessRow";
            checkColumn.HeaderText = "ProcessRow";
            checkColumn.Width = 75;
            checkColumn.ReadOnly = false;
            checkColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmSP.dgvSwapList.Columns.Add(checkColumn);
            frmSP.dgvSwapList.Columns["ProcessRow"].DisplayIndex = 0;                    

            frmSP.dgvSwapList.Columns["Results"].Width = 200;            //Results
            frmSP.dgvSwapList.Columns["Results"].HeaderText = "Results";
            frmSP.dgvSwapList.Columns["Results"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSP.dgvSwapList.Columns["Results"].ReadOnly = true;
            frmSP.dgvSwapList.Columns["Results"].DisplayIndex = 6;

            frmSP.lbMsg.Text = "Please check the Process Row checkbox for each job you want processed.";

            frmSP.lbMode.Text = "FindKit";
            frmSP.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmSP.BackColor = GlobalJob.EnvColor;
            frmSP.ShowDialog();
            gbPartSearch.Visible = false;
        }

        private void btnExitSearch_Click(object sender, EventArgs e)
        {
            gbPartSearch.Visible = false;
            tabMain.Enabled = true;
        }
        #endregion      

        private void mSPPartMaintToolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                     
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
#endif
            objMain.ProgPath = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\MSP_PartMainenanceTool\\setup.exe";
            System.Diagnostics.Process.Start(objMain.ProgPath); 
        }

        private void mixedAirPartMaintToolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];                     
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
#endif
            objMain.ProgPath = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\MixedAirPartMainenanceTool\\setup.exe";
            System.Diagnostics.Process.Start(objMain.ProgPath); 
        }

    }
}
