﻿using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmPickListCategory : Form
    {
        public frmPickListCategory()
        {
            InitializeComponent();
        }

        private void btnDisplayRpt_Click(object sender, EventArgs e)
        {
            bool stationChecked = false;
            //bool station1Checked = false;
            //bool station2Checked = false;
            //bool station3Checked = false;
            //bool station4Checked = false;
            //bool station5Checked = false;
            //bool station7Checked = false;

            //bool offsiteStn1Checked = false;
            //bool offsiteStn2Checked = false;
            //bool offsiteStn3Checked = false;
            //bool offsiteStn4Checked = false;
            //bool offsiteStn5Checked = false;
            //bool offsiteStn7Checked = false;

            bool tandemStnChecked = false;

            int reportID = 0;

            string retReportID = "";
            string jobNumStr = "";
            string jobNumList = "";
            string stationLst = "";
            string stationNumLst = "";
            string oauDescStr = lbOAU_Desc.Text;

            //OAU_DataSets dsCRData = new OAU_DataSets();

            MainBO objMain = new MainBO();

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum", typeof(string)).MaxLength = 14;
            dt.Columns.Add("StationList", typeof(string)).MaxLength = 200;
            dt.Columns.Add("StationNumList", typeof(string)).MaxLength = 50;

            foreach (DataGridViewRow row in dgvPickListJobs.Rows)
            {
                jobNumStr = row.Cells["JobNum"].Value.ToString();  // Job Number cell
                jobNumList += jobNumStr + ", ";

                stationLst = "";
                stationNumLst = "";

                var dr = dt.NewRow();
                dr["JobNum"] = jobNumStr;

                if (Convert.ToBoolean(row.Cells[0].Value) == true) // L5LS checkbox cell
                {
                    stationChecked = true;
                    //station1Checked = true;
                    stationLst += "L5LS";
                    stationNumLst += "1";
                }

                if (Convert.ToBoolean(row.Cells[1].Value) == true) // L5MS checkbox cell
                {
                    //station2Checked = true;
                    if (stationChecked == true)
                    {
                        stationLst += ",";
                        stationNumLst += ",";
                    }
                    stationChecked = true;
                    stationLst += "L5MS";
                    stationNumLst += "2";
                }

                if (Convert.ToBoolean(row.Cells[2].Value) == true)  // L5PA checkbox cell
                {
                    //station3Checked = true;
                    if (stationChecked == true)
                    {
                        stationLst += ",";
                        stationNumLst += ",";
                    }
                    stationChecked = true;
                    stationLst += "L5PA";
                    stationNumLst += "3";
                }

                if (Convert.ToBoolean(row.Cells[3].Value) == true) // L5RE checkbox cell
                {
                    //station4Checked = true;
                    if (stationChecked == true)
                    {
                        stationLst += ",";
                        stationNumLst += ",";
                    }
                    stationChecked = true;
                    stationLst += "L5RE";
                    stationNumLst += "4";
                }

                //if (Convert.ToBoolean(row.Cells[4].Value) == true) // Tandem Compressor Station checkbox cell - not currently being used
                //{
                //    tandemStnChecked = true;
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "TandemComp,Tandem Comp Accessories";
                //    stationNumLst += "2";
                //}

                //if (Convert.ToBoolean(row.Cells[5].Value) == true)   // Station 3 checkbox cell
                //{
                //    station3Checked = true;
                //    //if (tandemStnChecked == true)
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "Coil";
                //    stationNumLst += "3";
                //}

                //if (Convert.ToBoolean(row.Cells[6].Value) == true) // Offsite Station 3 checkbox cell
                //{
                //    offsiteStn3Checked = true;
                //    if (station3Checked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "";
                //    stationNumLst += "3";
                //}

                //if (Convert.ToBoolean(row.Cells[7].Value) == true)  // Station 4 checkbox cell
                //{
                //    station4Checked = true;
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "Refrigeration";
                //    stationNumLst += "4";
                //}

                //if (Convert.ToBoolean(row.Cells[8].Value) == true) // Offsite Station 4 checkbox cell
                //{
                //    offsiteStn4Checked = true;
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "Off-Site Refrig";
                //    stationNumLst += "4";
                //}

                //if (Convert.ToBoolean(row.Cells[9].Value) == true)   // Station 5 checkbox cell
                //{
                //    station5Checked = true;
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "Electrical,Motor";
                //    stationNumLst += "5";
                //}

                //if (Convert.ToBoolean(row.Cells[10].Value) == true) // Offsite Station 5 checkbox cell
                //{
                //    offsiteStn5Checked = true;
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "Off-Site Elec,Off-Site Mtr,Wheels,Dampers,Pre-heat";
                //    stationNumLst += "5";
                //}

                //if (Convert.ToBoolean(row.Cells[11].Value) == true)   // Station 7 checkbox cell
                //{
                //    station7Checked = true;
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "Miscellaneous";
                //    stationNumLst += "7";
                //}

                //if (Convert.ToBoolean(row.Cells[12].Value) == true) // Offsite Station 7 checkbox cell
                //{
                //    offsiteStn7Checked = true;
                //    if (stationChecked == true)
                //    {
                //        stationLst += ",";
                //        stationNumLst += ",";
                //    }
                //    stationChecked = true;
                //    stationLst += "Filters,Off-Site Misc";
                //    stationNumLst += "7";
                //}

                if (stationChecked == false)
                {
                    MessageBox.Show("ERROR - No Stations selected for Job # " + jobNumStr);
                    break;
                }
                else
                {
                    stationChecked = false;
                    //station1Checked = false;
                    //station2Checked = false;
                    //station3Checked = false;
                    //station4Checked = false;
                    //station5Checked = false;
                    //station7Checked = false;
                    //offsiteStn1Checked = false;
                    //offsiteStn2Checked = false;
                    //offsiteStn3Checked = false;
                    //offsiteStn4Checked = false;
                    //offsiteStn5Checked = false;
                    //offsiteStn7Checked = false;
                    //tandemStnChecked = false;
                    dr["StationList"] = stationLst.Substring(0, stationLst.Length);
                    dr["StationNumList"] = stationNumLst.Substring(0, stationNumLst.Length);
                    dt.Rows.Add(dr);
                    //stationLst = "";
                    //stationNumLst = "";
                }
            }

            if (jobNumList.Length > 0)
            {
                jobNumList = jobNumList.Substring(0, (jobNumList.Length - 2));
            }

            if (dt.Rows.Count > 0)
            {
                retReportID = objMain.Insert_OAU_PickListHistory(dt, System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString());
            }

            if (retReportID != "" && retReportID != null)
            {
                reportID = int.Parse(retReportID);

                ReportDocument cryRpt = new ReportDocument();
                frmPrintBOM frmPrint = new frmPrintBOM();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@ReportID";
                pdv1.Value = reportID;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@JobNum";
                pdv2.Value = jobNumList;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                ParameterField pf3 = new ParameterField();
                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                pf3.Name = "@PickListCategories";
                pdv3.Value = stationLst.Substring(0, stationLst.Length);
                pf3.CurrentValues.Add(pdv3);

                paramFields.Add(pf3);

                ParameterField pf4 = new ParameterField();
                ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
                pf4.Name = "@OAU_Desc";
                pdv4.Value = oauDescStr;
                pf4.CurrentValues.Add(pdv4);

                paramFields.Add(pf4);

                ParameterField pf5 = new ParameterField();
                ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
                pf5.Name = "@StationNumList";
                pdv5.Value = stationNumLst;
                pf5.CurrentValues.Add(pdv5);

                paramFields.Add(pf5);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUPickList\\OAUPickListv5.rpt";             
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUPickList\\OAUPickListv5.rpt";
#endif

                cryRpt.Load(objMain.ReportString);
                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            else
            {
                MessageBox.Show("WARNING: Required Report ID is missing. Please contact IT to resolve.");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }       
    }
}
