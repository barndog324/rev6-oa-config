﻿using Epicor.Mfg.Core;
using Epicor.Mfg.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    class TransferJob
    {
        private Session epiSession;
        private string epicorServer;
        private string epicorSqlConnectionString;

        LineTransferBO objTran = new LineTransferBO();
        MainBO objMain = new MainBO();

        // Init
        public TransferJob(string epicorServer, string epicorSqlConnectionString)
        {
            this.epicorServer = epicorServer;
            this.epicorSqlConnectionString = epicorSqlConnectionString;
        }

        public void ScheduleJobToOpenSlot(string jobNum, string toLoc, string toLine, string slotStrartTime, DateTime slotStartDate)
        {
            string reqDueDateStr = "";
            string prodQtyStr = "";
            string endDateStr = "";
            string dueDateStr = "1/1/1000";
            string dueHourStr = "";
            string startDateStr = "1/1/1000";
            string oprStartDate = "";
            string oprStartHour = "";
            string oprEndDate = "";
            string oprEndHour = "";
            string vMessage = "";
            string plantLine = "";
            string resourceGrpID = "";
            string resourceID = "";
            string opDtlDesc = "";
            string runOutWarning = "";
            string c_WarnLogTxt = "";

            int oprSeq = 0;
            int rowIdx = 0;

            DateTime reqDueDate = DateTime.Now; // These date variable are set to today's date only for initialization purposes.
            //DateTime startDate = DateTime.Now;
            DateTime dueDate = DateTime.Now;

            decimal prodQtyDec = 0;

            double totalPrdHrs = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool opChangeDescription = false;
            bool l_finished = false;

            this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);
            ScheduleEngine se = new ScheduleEngine(this.epiSession.ConnectionPool);

            JobEntryDataSet jeds = je.GetByID(jobNum);
            DataRow[] jhdr = jeds.JobHead.Select();

            if (jhdr[0]["ProdQty"] != null)
            {
                prodQtyStr = jhdr[0]["ProdQty"].ToString();
                prodQtyDec = decimal.Parse(prodQtyStr);
            }

            if (jhdr[0]["ReqDueDate"] != null)
            {
                reqDueDateStr = jhdr[0]["ReqDueDate"].ToString();
                reqDueDate = Convert.ToDateTime(reqDueDateStr);
            }

            if (jhdr[0]["JobEngineered"].ToString() == "True")
            {
                jobEngineered = true;
            }

            if (jhdr[0]["JobReleased"].ToString() == "True")
            {
                jobReleased = true;
            }

            if (jobEngineered) // If JobEngineered = True then un-engineer the job.
            {
                try
                {
                    je.ValidateJobNum(jobNum);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ValidateJobNum() -- " + ex);
                }

                try
                {
                    DataTable dtJobOps = objMain.GetOA_JobOperations(jobNum);
                    if (dtJobOps.Rows.Count > 0)
                    {
                        totalPrdHrs = CalcPrdHours(dtJobOps);

                        try
                        {
                            int sepPos = 0;
                            endDateStr = CalcDueDateAndTime(slotStartDate, slotStrartTime, totalPrdHrs);

                            sepPos = endDateStr.IndexOf("=");
                            if (sepPos > 0)
                            {
                                dueDateStr = endDateStr.Substring(0, sepPos);
                                dueHourStr = endDateStr.Substring((sepPos + 1), (endDateStr.Length - (sepPos + 1)));
                                ScheduleEngineDataSet seds = new ScheduleEngineDataSet();
                                foreach (DataRow row in dtJobOps.Rows)
                                {
                                    DataRow sedr = seds.ScheduleEngine.NewRow();

                                    sedr["Company"] = "KCC";
                                    sedr["JobNum"] = jobNum;
                                    sedr["AssemblySeq"] = Int32.Parse(row["AssemblySeq"].ToString());
                                    sedr["OprSeq"] = Int32.Parse(row["OprSeq"].ToString());
                                    sedr["OpDtlSeq"] = Int32.Parse(row["OpDtlSeq"].ToString());
                                    sedr["StartDate"] = dueDateStr;
                                    sedr["StartTime"] = Int32.Parse(dueHourStr);
                                    sedr["EndDate"] = dueDateStr;
                                    sedr["EndTime"] = Int32.Parse(dueHourStr);
                                    sedr["WhatIf"] = 0;
                                    sedr["Finite"] = 0;                                    
                                    sedr["SchedTypeCode"] = "AA";
                                    sedr["ScheduleDirection"] = "F";
                                    seds.ScheduleEngine.Rows.Add(sedr);
                                }

                                //string schTypCodes = "";
                                //string schOpTypCodes = "";
                                //se.GetSchedTypeCodes(out schTypCodes, out schOpTypCodes);

                                //seds.Tables["ScheduleEngine"].Rows[0]["ScheduleDirection"] = "B";
                                //seds.Tables["ScheduleEngine"].Rows[0]["SchedTypeCode"] = "JA";
                                se.MoveJobItem(seds, out l_finished, out c_WarnLogTxt);
                            }
                            else
                            {
                                MessageBox.Show("ERROR - Invalid Due Date and/or Hour");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - MoveJobItem() -- " + ex);
                        }

                        try
                        {
                            jeds = je.GetByID(jobNum);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetByID() -- " + ex);
                        }
                    }                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOA_JobOperations -- " + ex);
                }

                

            }

            plantLine = "GRSLINE" + toLine.Substring((toLine.Length - 1), 1);

            
        }

        public void ProcessJobGrasslandToGrasslandOpenSlot(string jobNum, string toLoc, string toLine, string slotStrartTime, double prodHrs, DateTime slotStartDate)
        {
            string reqDueDateStr = "";
            string prodQtyStr = "";
            string endDateStr = "";
            string dueDateStr = "1/1/1000";
            string dueHourStr = "";
            string startDateStr = "1/1/1000";
            string vMessage = "";
            string plantLine = "";
            string resourceGrpID = "";
            string resourceID = "";
            string opDtlDesc = "";
            string runOutWarning = "";
            string c_WarnLogTxt = "";

            int oprSeq = 0;
            int rowIdx = 0;

            DateTime reqDueDate = DateTime.Now; // These date variable are set to today's date only for initialization purposes.
            //DateTime startDate = DateTime.Now;
            DateTime dueDate = DateTime.Now;

            decimal prodQtyDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool opChangeDescription = false;
            bool l_finished = false;

            this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);
            ScheduleEngine se = new ScheduleEngine(this.epiSession.ConnectionPool);

            JobEntryDataSet jeds = je.GetByID(jobNum);
            DataRow[] jhdr = jeds.JobHead.Select();            

            if (jhdr[0]["ProdQty"] != null)
            {
                prodQtyStr = jhdr[0]["ProdQty"].ToString();
                prodQtyDec = decimal.Parse(prodQtyStr);
            }

            if (jhdr[0]["ReqDueDate"] != null)
            {
                reqDueDateStr = jhdr[0]["ReqDueDate"].ToString();
                reqDueDate = Convert.ToDateTime(reqDueDateStr);
            }            
    
            if (jhdr[0]["JobEngineered"].ToString() == "True")
            {
                jobEngineered = true;
            }

            if (jhdr[0]["JobReleased"].ToString() == "True")
            {
                jobReleased = true;
            }

            if (jobEngineered) // If JobEngineered = True then un-engineer the job.
            {
                try
                {
                    je.ValidateJobNum(jobNum);                    
                }
                catch(Exception ex)
                {
                    MessageBox.Show("ERROR - ValidateJobNum() -- " + ex);
                }

                try
                {
                    int sepPos = 0;
                    endDateStr = CalcDueDateAndTime(slotStartDate, slotStrartTime, prodHrs);

                    sepPos = endDateStr.IndexOf("=");
                    if (sepPos > 0)
                    {
                        dueDateStr = endDateStr.Substring(0, sepPos);
                        dueHourStr = endDateStr.Substring((sepPos + 1), (endDateStr.Length - (sepPos + 1)));
                        ScheduleEngineDataSet seds = new ScheduleEngineDataSet();
                        DataRow sedr = seds.ScheduleEngine.NewRow();
                        sedr["Company"] = "KCC";
                        sedr["JobNum"] = jobNum;
                        sedr["EndDate"] = dueDateStr;
                        sedr["EndTime"] = Int32.Parse(dueHourStr);
                        sedr["AssemblySeq"] = 0;
                        seds.ScheduleEngine.Rows.Add(sedr);
                        se.MoveJobItem(seds, out l_finished, out c_WarnLogTxt);
                    }
                    else
                    {
                        MessageBox.Show("ERROR - Invalid Due Date and/or Hour");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - MoveJobItem() -- " + ex);
                }

                try
                {
                    jeds = je.GetByID(jobNum);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetByID() -- " + ex);
                }
                           
            }

            plantLine = "GRSLINE" + toLine.Substring((toLine.Length - 1), 1);
            
            try
            {
                try
                {
                    oprSeq = 40;

                    DataTable dt40 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt40.Rows.Count > 0)
                    {
                        DataRow dr40 = dt40.Rows[0];
                        resourceID = dr40["ResourceID"].ToString();
                        opDtlDesc = dr40["OpDtlDesc"].ToString();
                        resourceGrpID = dr40["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach(DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "40")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }          
                 
                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 40;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }                                 
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 40 Issues -- " + ex);
            }

            try
            {
                try
                {
                    oprSeq = 50;

                    DataTable dt50 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt50.Rows.Count > 0)
                    {
                        DataRow dr50 = dt50.Rows[0];
                        resourceID = dr50["ResourceID"].ToString();
                        opDtlDesc = dr50["OpDtlDesc"].ToString();
                        resourceGrpID = dr50["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "50")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }

                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 50;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 50 Issues -- " + ex);
            }

            try
            {
                try
                {
                    oprSeq = 60;

                    DataTable dt60 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt60.Rows.Count > 0)
                    {
                        DataRow dr60 = dt60.Rows[0];
                        resourceID = dr60["ResourceID"].ToString();
                        opDtlDesc = dr60["OpDtlDesc"].ToString();
                        resourceGrpID = dr60["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "60")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }

                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 60;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 60 Issues -- " + ex);
            }

            try
            {
                try
                {
                    oprSeq = 70;

                    DataTable dt70 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt70.Rows.Count > 0)
                    {
                        DataRow dr70 = dt70.Rows[0];
                        resourceID = dr70["ResourceID"].ToString();
                        opDtlDesc = dr70["OpDtlDesc"].ToString();
                        resourceGrpID = dr70["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "70")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }

                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 70;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 70 Issues -- " + ex);
            }                                    

            if (jobReleased)
            {
                try
                {
                    je.CheckResourcePlants(jobNum, out runOutWarning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                }
        
                try
                {
                    jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;
                    jeds.Tables["JobHead"].Rows[0]["JobReleased"] = true;  
                    je.ChangeJobHeadJobEngineered(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                }

                try
                {
                    //je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                }

                try
                {
                    je.CheckForChanges("KCC", jobNum, false, false, true, false, false, false, false, false, false, out opChangeDescription);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                }

                try
                {
                    jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                    je.Update(jeds);
                    MessageBox.Show("Line Transfer Complete!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update -- " + ex);
                }            
            }
            else if (jobEngineered)
            {
                jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;

                try
                {
                    je.CheckResourcePlants(jobNum, out runOutWarning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                }

                try
                {                    
                    je.ChangeJobHeadJobEngineered(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                }

                try
                {
                    //je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                }

                try
                {                    
                    je.CheckForChanges("KCC", jobNum, true, false, true, false, false, false, false, false, false, out opChangeDescription);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                }

                try
                {
                    jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                    je.Update(jeds);
                    MessageBox.Show("Line Transfer Complete!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update -- " + ex);
                }            
            }
            else
            {
                MessageBox.Show("Line Transfer Complete!");
            }
        }

        public void ProcessJobGrasslandToGrassland(string jobNum, string toLoc, string toLine, string jobStrartTime, decimal prodHrs, DateTime startDate)
        {
            string reqDueDateStr = "";
            string prodQtyStr = "";
            string dueDateStr = "1/1/1000";
            string startDateStr = "1/1/1000";
            string vMessage = "";
            string plantLine = "";
            string resourceGrpID = "";
            string resourceID = "";
            string opDtlDesc = "";
            string runOutWarning = "";

            int oprSeq = 0;
            int rowIdx = 0;

            DateTime reqDueDate = DateTime.Now; // These date variable are set to today's date only for initialization purposes.
            //DateTime startDate = DateTime.Now;
            DateTime dueDate = DateTime.Now;

            decimal prodQtyDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool opChangeDescription = false;

            this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

            JobEntryDataSet jeds = je.GetByID(jobNum);
            DataRow[] jhdr = jeds.JobHead.Select();

            if (jhdr[0]["ProdQty"] != null)
            {
                prodQtyStr = jhdr[0]["ProdQty"].ToString();
                prodQtyDec = decimal.Parse(prodQtyStr);
            }

            if (jhdr[0]["ReqDueDate"] != null)
            {
                reqDueDateStr = jhdr[0]["ReqDueDate"].ToString();
                reqDueDate = Convert.ToDateTime(reqDueDateStr);
            }

            if (jhdr[0]["StartDate"] != DBNull.Value)
            {
                startDateStr = jhdr[0]["StartDate"].ToString();
            }
            startDate = Convert.ToDateTime(startDateStr);

            if (jhdr[0]["DueDate"] != DBNull.Value)
            {
                dueDateStr = jhdr[0]["DueDate"].ToString();
            }
            dueDate = Convert.ToDateTime(dueDateStr);

            if (jhdr[0]["JobEngineered"].ToString() == "True")
            {
                jobEngineered = true;
            }

            if (jhdr[0]["JobReleased"].ToString() == "True")
            {
                jobReleased = true;
            }

            if (jobEngineered) // If JobEngineered = True then un-engineer the job.
            {
                try
                {
                    jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                    je.ChangeJobHeadJobEngineered(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                }

                try
                {
                    je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                }

                try
                {
                    je.CheckForChanges("KCC", jobNum, false, false, true, false, false, false, false, false, false, out opChangeDescription);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update -- " + ex);
                }
            }

            plantLine = "GRSLINE" + toLine.Substring((toLine.Length - 1), 1);

            try
            {
                try
                {
                    oprSeq = 40;

                    DataTable dt40 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt40.Rows.Count > 0)
                    {
                        DataRow dr40 = dt40.Rows[0];
                        resourceID = dr40["ResourceID"].ToString();
                        opDtlDesc = dr40["OpDtlDesc"].ToString();
                        resourceGrpID = dr40["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "40")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }

                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 40;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 40 Issues -- " + ex);
            }

            try
            {
                try
                {
                    oprSeq = 50;

                    DataTable dt50 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt50.Rows.Count > 0)
                    {
                        DataRow dr50 = dt50.Rows[0];
                        resourceID = dr50["ResourceID"].ToString();
                        opDtlDesc = dr50["OpDtlDesc"].ToString();
                        resourceGrpID = dr50["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "50")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }

                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 50;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 50 Issues -- " + ex);
            }

            try
            {
                try
                {
                    oprSeq = 60;

                    DataTable dt60 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt60.Rows.Count > 0)
                    {
                        DataRow dr60 = dt60.Rows[0];
                        resourceID = dr60["ResourceID"].ToString();
                        opDtlDesc = dr60["OpDtlDesc"].ToString();
                        resourceGrpID = dr60["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "60")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }

                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 60;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 60 Issues -- " + ex);
            }

            try
            {
                try
                {
                    oprSeq = 70;

                    DataTable dt70 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    if (dt70.Rows.Count > 0)
                    {
                        DataRow dr70 = dt70.Rows[0];
                        resourceID = dr70["ResourceID"].ToString();
                        opDtlDesc = dr70["OpDtlDesc"].ToString();
                        resourceGrpID = dr70["ResourceGrpID"].ToString();

                        rowIdx = 0;
                        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                        {
                            if (row["OprSeq"].ToString() == "70")
                            {
                                break;
                            }
                            ++rowIdx;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                }

                try
                {
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceGrpID"] = resourceGrpID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["ResourceID"] = resourceID;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OpDtlDesc"] = opDtlDesc;
                    jeds.Tables["JobOpDtl"].Rows[rowIdx]["OprSeq"] = 70;

                    je.ChangeJobOpDtlResourceID(resourceID, jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                }

                try
                {
                    je.Update(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update() -- " + ex);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR - Operation 70 Issues -- " + ex);
            }

            if (jobReleased)
            {
                try
                {
                    je.CheckResourcePlants(jobNum, out runOutWarning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                }

                try
                {
                    jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;
                    jeds.Tables["JobHead"].Rows[0]["JobReleased"] = true;
                    je.ChangeJobHeadJobEngineered(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                }

                try
                {
                    je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                }

                try
                {
                    je.CheckForChanges("KCC", jobNum, false, false, true, false, false, false, false, false, false, out opChangeDescription);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                }

                try
                {
                    jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                    je.Update(jeds);
                    MessageBox.Show("Line Transfer Complete!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update -- " + ex);
                }
            }
            else if (jobEngineered)
            {
                jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;

                try
                {
                    je.CheckResourcePlants(jobNum, out runOutWarning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                }

                try
                {
                    je.ChangeJobHeadJobEngineered(jeds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                }

                try
                {
                    je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                }

                try
                {
                    je.CheckForChanges("KCC", jobNum, true, false, true, false, false, false, false, false, false, out opChangeDescription);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                }

                try
                {
                    jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                    je.Update(jeds);
                    MessageBox.Show("Line Transfer Complete!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Update -- " + ex);
                }
            }
            else
            {
                MessageBox.Show("Line Transfer Complete!");
            }
        }
        
        public void ProcessJobTechnologyToTechnology(string jobNum, string partNum, string fromLoc, string fromLine, string toLoc, string toLine)
        {
            string reqDueDateStr = "";
            string prodQtyStr = "";
            string dueDateStr = "1/1/1000";
            string startDateStr = "1/1/1000";
            string vMessage = "";
            string plantLine = "";
            string resourceGrpID = "";
            string resourceID = "";
            string opCode = "";
            string opDesc = "";
            string opDtlDesc = "";
            string runOutWarning = "";
            string refreshMessage = "";
            string relatedOp = "";
            string oprSequence = "";
            string laborTrans = "";
            string laborEntryMethod = "";
            string EstProdHours = "";
            string prodStd = "";
            string prodBurRate = "";
            string prodLabRate = "";

            int oprSeq = 0;
            int oprRowIdx = 0;
            int opDtlRowIdx = 0;
            int rowIdx = 0;
            int asmSeq = 0;

            DateTime reqDueDate = DateTime.Now; // These date variable are set to today's date only for initialization purposes.
            DateTime startDate = DateTime.Now;
            DateTime dueDate = DateTime.Now;

            decimal prodQtyDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool opChangeDescription = false;
            bool operationExist = false;
            bool epicorSessionExist = true;

            try
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);
            }
            catch (Exception ex)
            {
                epicorSessionExist = false;
            }

            if (epicorSessionExist == true)
            {
                if (RelavantLaborTransactionsExist(jobNum, "Technology", out laborTrans) == false)
                {
                    DataTable dtJobOps = objMain.GetOA_JobOperations(jobNum);

                    JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

                    JobEntryDataSet jeds = je.GetByID(jobNum);
                    DataRow[] jhdr = jeds.JobHead.Select();

                    if (jhdr[0]["ProdQty"] != null)
                    {
                        prodQtyStr = jhdr[0]["ProdQty"].ToString();
                        prodQtyDec = decimal.Parse(prodQtyStr);
                    }

                    if (jhdr[0]["ReqDueDate"] != null)
                    {
                        reqDueDateStr = jhdr[0]["ReqDueDate"].ToString();
                        reqDueDate = Convert.ToDateTime(reqDueDateStr);
                    }

                    if (jhdr[0]["StartDate"] != DBNull.Value)
                    {
                        startDateStr = jhdr[0]["StartDate"].ToString();
                    }
                    startDate = Convert.ToDateTime(startDateStr);

                    if (jhdr[0]["DueDate"] != DBNull.Value)
                    {
                        dueDateStr = jhdr[0]["DueDate"].ToString();
                    }
                    dueDate = Convert.ToDateTime(dueDateStr);

                    if (jhdr[0]["JobEngineered"].ToString() == "True")
                    {
                        jobEngineered = true;
                    }

                    if (jhdr[0]["JobReleased"].ToString() == "True")
                    {
                        jobReleased = true;
                    }

                    if (jobEngineered) // If JobEngineered = True then un-engineer the job.
                    {
                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                        }

                        try
                        {
                            je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                        }

                        try
                        {
                            je.CheckForChanges("KCC", jobNum, false, false, true, false, false, false, false, false, false, out opChangeDescription);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update -- " + ex);
                        }

                        if (startDateStr != "1/1/1000")  //Check to see if the Job has a Scheduled Start Date. If the StartDate is equal to "1/1/1000" then it has not been scheduled.
                        {
                            try
                            {
                                je.RemoveFromSchedule(jobNum);
                                jeds = je.GetByID(jobNum);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - RemoveFromSchedule -- " + ex);
                            }
                        }
                    }

                    plantLine = partNum.Substring(0, 3) + "LINE" + toLine.Substring((toLine.Length - 1), 1);

                    #region Operation 30
                    
                    #endregion

                    #region Operation 35/36
                    //try
                    //{
                    //    try
                    //    {
                    //        rowIdx = 0;
                    //        foreach (DataRow row in jeds.Tables["jobMtl"].Rows)
                    //        {
                    //            relatedOp = row["RelatedOperation"].ToString();

                    //            if (relatedOp == "35")
                    //            {
                    //                jeds.Tables["jobMtl"].Rows[rowIdx]["RowMod"] = "U";
                    //                je.ChangeJobMtlRelatedOperation(0, jeds);

                    //                je.Update(jeds);

                    //            }
                    //            ++rowIdx;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        MessageBox.Show("ERROR - ChangeJobMtlRelatedOperation() -- " + ex);
                    //    }

                    //    try
                    //    {
                    //        rowIdx = 0;
                    //        foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                    //        {
                    //            if (row["OprSeq"].ToString() == "35")
                    //            {
                    //                jeds.Tables["JobOpDtl"].Rows[rowIdx].Delete();
                    //                break;
                    //            }
                    //            ++rowIdx;
                    //        }

                    //        rowIdx = 0;
                    //        foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                    //        {
                    //            oprSequence = row["OprSeq"].ToString();

                    //            if (oprSequence == "35")
                    //            {
                    //                jeds.Tables["JobOper"].Rows[rowIdx].Delete();
                    //                je.Update(jeds);
                    //                break;
                    //            }
                    //            ++rowIdx;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        MessageBox.Show("ERROR - Update() -- " + ex);
                    //    }

                    //    try
                    //    {
                    //        oprSeq = 36;
                    //        DataTable dt36 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                    //        if (dt36.Rows.Count > 0)
                    //        {
                    //            DataRow dr36 = dt36.Rows[0];
                    //            resourceID = dr36["ResourceID"].ToString();
                    //            opCode = dr36["OpCode"].ToString();
                    //            opDesc = dr36["OpDesc"].ToString();
                    //            opDtlDesc = dr36["OpDtlDesc"].ToString();
                    //            resourceGrpID = dr36["ResourceGrpID"].ToString();
                    //            laborEntryMethod = dr36["LaborEntryMethod"].ToString();
                    //            EstProdHours = dr36["EstProdHours"].ToString();
                    //            prodStd = dr36["ProdStandard"].ToString();
                    //            prodLabRate = dr36["ProdLabRate"].ToString();
                    //            prodBurRate = dr36["ProdBurRate"].ToString();
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                    //    }

                    //    if (OperationExist(dtJobOps, "36") == false)
                    //    {
                    //        try
                    //        {
                    //            je.GetNewJobOper(jeds, jobNum, asmSeq);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            je.CheckJobOperOprSeq(jobNum, asmSeq, 36, out vMessage);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;


                    //            jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 36;
                    //            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                    //            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                    //            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                    //            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                    //            je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                    //            je.ChangeJobOperOpStdID(jeds);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            je.ChangeJobOperLaborEntryMethod("Q", jeds);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            je.Update(jeds);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - Update() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                    //            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                    //            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                    //            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                    //            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                    //            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                    //            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                    //            //jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 36;

                    //            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            je.Update(jeds);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - Update() -- " + ex);
                    //        }

                    //        try
                    //        {
                    //            rowIdx = 0;
                    //            foreach (DataRow row in jeds.Tables["jobMtl"].Rows)
                    //            {
                    //                relatedOp = row["RelatedOperation"].ToString();

                    //                if (relatedOp == "0")
                    //                {
                    //                    jeds.Tables["jobMtl"].Rows[rowIdx]["RowMod"] = "U";
                    //                    je.ChangeJobMtlRelatedOperation(36, jeds);

                    //                    je.Update(jeds);
                    //                }
                    //                ++rowIdx;
                    //            }
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show("ERROR - ChangeJobMtlRelatedOperation() -- " + ex);
                    //        }
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show("ERROR - Operation 36 Issues -- " + ex);
                    //}
                    #endregion

                    #region Operation 40
                    try
                    {
                        try
                        {
                            oprSeq = 40;

                            DataTable dt40 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt40.Rows.Count > 0)
                            {
                                DataRow dr40 = dt40.Rows[0];
                                resourceID = dr40["ResourceID"].ToString();
                                opCode = dr40["OpCode"].ToString();
                                opDesc = dr40["OpDesc"].ToString();
                                opDtlDesc = dr40["OpDtlDesc"].ToString();
                                resourceGrpID = dr40["ResourceGrpID"].ToString();
                                laborEntryMethod = dr40["LaborEntryMethod"].ToString();
                                EstProdHours = dr40["EstProdHours"].ToString();
                                prodStd = dr40["ProdStandard"].ToString();
                                prodLabRate = dr40["ProdLabRate"].ToString();
                                prodBurRate = dr40["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "40")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "40")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 40;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 40 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 41
                    try
                    {
                        oprSeq = 41;
                        DataTable dt41 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt41.Rows.Count > 0)
                        {
                            DataRow dr41 = dt41.Rows[0];
                            resourceID = dr41["ResourceID"].ToString();
                            opCode = dr41["OpCode"].ToString();
                            opDesc = dr41["OpDesc"].ToString();
                            opDtlDesc = dr41["OpDtlDesc"].ToString();
                            resourceGrpID = dr41["ResourceGrpID"].ToString();
                            laborEntryMethod = dr41["LaborEntryMethod"].ToString();
                            EstProdHours = dr41["EstProdHours"].ToString();
                            prodStd = dr41["ProdStandard"].ToString();
                            prodLabRate = dr41["ProdLabRate"].ToString();
                            prodBurRate = dr41["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "41") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 41, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 41;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;

                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 41;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {                                
                                if (row["OprSeq"].ToString() == "41")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "41")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 41;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 41 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 50
                    try
                    {
                        try
                        {
                            oprSeq = 50;

                            DataTable dt50 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt50.Rows.Count > 0)
                            {
                                DataRow dr50 = dt50.Rows[0];
                                opCode = dr50["OpCode"].ToString();
                                opDesc = dr50["OpDesc"].ToString();
                                resourceID = dr50["ResourceID"].ToString();
                                opDtlDesc = dr50["OpDtlDesc"].ToString();
                                resourceGrpID = dr50["ResourceGrpID"].ToString();
                                laborEntryMethod = dr50["LaborEntryMethod"].ToString();
                                EstProdHours = dr50["EstProdHours"].ToString();
                                prodStd = dr50["ProdStandard"].ToString();
                                prodLabRate = dr50["ProdLabRate"].ToString();
                                prodBurRate = dr50["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "50")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "50")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 50;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 50 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 51
                    try
                    {
                        oprSeq = 51;
                        DataTable dt51 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt51.Rows.Count > 0)
                        {
                            DataRow dr51 = dt51.Rows[0];
                            resourceID = dr51["ResourceID"].ToString();
                            opCode = dr51["OpCode"].ToString();
                            opDtlDesc = dr51["OpDtlDesc"].ToString();
                            resourceGrpID = dr51["ResourceGrpID"].ToString();
                            laborEntryMethod = dr51["LaborEntryMethod"].ToString();
                            EstProdHours = dr51["EstProdHours"].ToString();
                            prodStd = dr51["ProdStandard"].ToString();
                            prodLabRate = dr51["ProdLabRate"].ToString();
                            prodBurRate = dr51["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "51") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 51, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 51;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;

                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 51;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "51")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "51")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 51;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 51 Issues -- " + ex);
                    }
                    #endregion

                    #region Operarion 55
                    try
                    {
                        try
                        {
                            oprSeq = 55;

                            DataTable dt55 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt55.Rows.Count > 0)
                            {
                                DataRow dr55 = dt55.Rows[0];
                                opCode = dr55["OpCode"].ToString();
                                opDesc = dr55["OpDesc"].ToString();
                                resourceID = dr55["ResourceID"].ToString();
                                opDtlDesc = dr55["OpDtlDesc"].ToString();
                                resourceGrpID = dr55["ResourceGrpID"].ToString();
                                laborEntryMethod = dr55["LaborEntryMethod"].ToString();
                                EstProdHours = dr55["EstProdHours"].ToString();
                                prodStd = dr55["ProdStandard"].ToString();
                                prodLabRate = dr55["ProdLabRate"].ToString();
                                prodBurRate = dr55["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "55")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "55")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 55;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 55 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 60
                    try
                    {
                        try
                        {
                            oprSeq = 60;

                            DataTable dt60 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt60.Rows.Count > 0)
                            {
                                DataRow dr60 = dt60.Rows[0];
                                opCode = dr60["OpCode"].ToString();
                                opDesc = dr60["OpDesc"].ToString();
                                resourceID = dr60["ResourceID"].ToString();
                                opDtlDesc = dr60["OpDtlDesc"].ToString();
                                resourceGrpID = dr60["ResourceGrpID"].ToString();
                                laborEntryMethod = dr60["LaborEntryMethod"].ToString();
                                EstProdHours = dr60["EstProdHours"].ToString();
                                prodStd = dr60["ProdStandard"].ToString();
                                prodLabRate = dr60["ProdLabRate"].ToString();
                                prodBurRate = dr60["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "60")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "60")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 60;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 60 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 65
                    try
                    {
                        try
                        {
                            oprSeq = 65;

                            DataTable dt65 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt65.Rows.Count > 0)
                            {
                                DataRow dr65 = dt65.Rows[0];
                                opCode = dr65["OpCode"].ToString();
                                opDesc = dr65["OpDesc"].ToString();
                                resourceID = dr65["ResourceID"].ToString();
                                opDtlDesc = dr65["OpDtlDesc"].ToString();
                                resourceGrpID = dr65["ResourceGrpID"].ToString();
                                laborEntryMethod = dr65["LaborEntryMethod"].ToString();
                                EstProdHours = dr65["EstProdHours"].ToString();
                                prodStd = dr65["ProdStandard"].ToString();
                                prodLabRate = dr65["ProdLabRate"].ToString();
                                prodBurRate = dr65["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "65")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "65")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 65;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 65 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 70
                    try
                    {
                        try
                        {
                            oprSeq = 70;

                            DataTable dt70 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt70.Rows.Count > 0)
                            {
                                DataRow dr70 = dt70.Rows[0];
                                opCode = dr70["OpCode"].ToString();
                                opDesc = dr70["OpDesc"].ToString();
                                resourceID = dr70["ResourceID"].ToString();
                                opDtlDesc = dr70["OpDtlDesc"].ToString();
                                resourceGrpID = dr70["ResourceGrpID"].ToString();
                                laborEntryMethod = dr70["LaborEntryMethod"].ToString();
                                EstProdHours = dr70["EstProdHours"].ToString();
                                prodStd = dr70["ProdStandard"].ToString();
                                prodLabRate = dr70["ProdLabRate"].ToString();
                                prodBurRate = dr70["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "70")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "70")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 70;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 70 Issues -- " + ex);
                    }
                    #endregion


                    #region Operation 71
                    try
                    {
                        oprSeq = 71;
                        DataTable dt71 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt71.Rows.Count > 0)
                        {
                            DataRow dr71 = dt71.Rows[0];
                            resourceID = dr71["ResourceID"].ToString();
                            opCode = dr71["OpCode"].ToString();
                            opDesc = dr71["OpDesc"].ToString();
                            opDtlDesc = dr71["OpDtlDesc"].ToString();
                            resourceGrpID = dr71["ResourceGrpID"].ToString();
                            laborEntryMethod = dr71["LaborEntryMethod"].ToString();
                            EstProdHours = dr71["EstProdHours"].ToString();
                            prodStd = dr71["ProdStandard"].ToString();
                            prodLabRate = dr71["ProdLabRate"].ToString();
                            prodBurRate = dr71["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "71") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 71, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 71;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 71;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "71")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "71")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 71;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 71 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 80
                    try
                    {
                        oprSeq = 80;
                        DataTable dt80 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt80.Rows.Count > 0)
                        {
                            DataRow dr80 = dt80.Rows[0];
                            resourceID = dr80["ResourceID"].ToString();
                            opCode = dr80["OpCode"].ToString();
                            opDesc = dr80["OpDesc"].ToString();
                            opDtlDesc = dr80["OpDtlDesc"].ToString();
                            resourceGrpID = dr80["ResourceGrpID"].ToString();
                            laborEntryMethod = dr80["LaborEntryMethod"].ToString();
                            EstProdHours = dr80["EstProdHours"].ToString();
                            prodStd = dr80["ProdStandard"].ToString();
                            prodLabRate = dr80["ProdLabRate"].ToString();
                            prodBurRate = dr80["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "80") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 80, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 80;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 80;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "80")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "80")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 80;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 80 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 90
                    try
                    {
                        oprSeq = 90;

                        DataTable dt90 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt90.Rows.Count > 0)
                        {
                            DataRow dr90 = dt90.Rows[0];
                            resourceID = dr90["ResourceID"].ToString();
                            opCode = dr90["OpCode"].ToString();
                            opDesc = dr90["OpDesc"].ToString();
                            opDtlDesc = dr90["OpDtlDesc"].ToString();
                            resourceGrpID = dr90["ResourceGrpID"].ToString();
                            laborEntryMethod = dr90["LaborEntryMethod"].ToString();
                            EstProdHours = dr90["EstProdHours"].ToString();
                            prodStd = dr90["ProdStandard"].ToString();
                            prodLabRate = dr90["ProdLabRate"].ToString();
                            prodBurRate = dr90["ProdBurRate"].ToString();
                        }


                        if (OperationExist(dtJobOps, "90") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 90, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 90;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 90;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "90")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "90")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 90;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 80 Issues -- " + ex);
                    }                    
                    #endregion

                    if (jobReleased)
                    {
                        try
                        {
                            je.CheckResourcePlants(jobNum, out runOutWarning);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;
                            jeds.Tables["JobHead"].Rows[0]["JobReleased"] = true;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                        }

                        try
                        {
                            je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                        }

                        try
                        {
                            je.CheckForChanges("KCC", jobNum, false, false, true, false, false, false, false, false, false, out opChangeDescription);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                            je.Update(jeds);
                            MessageBox.Show("Line Transfer Complete!");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update -- " + ex);
                        }
                    }
                    else if (jobEngineered)
                    {
                        jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;

                        try
                        {
                            je.CheckResourcePlants(jobNum, out runOutWarning);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                        }

                        try
                        {
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                        }

                        try
                        {
                            je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                        }

                        try
                        {
                            je.CheckForChanges("KCC", jobNum, true, false, true, false, false, false, false, false, false, out opChangeDescription);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                            je.Update(jeds);
                            MessageBox.Show("Line Transfer Complete!");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update -- " + ex);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Line Transfer Complete!");
                    }
                }
                else
                {
                    MessageBox.Show("ERROR - Transfer cannot take place. Labor transactions exist for operation/s: " + laborTrans);
                }            
            }
            else
            {
                MessageBox.Show("WARNING - No Epicor License is available at this time. Please try again later");
            }

        }

        public void ProcessJobGrasslandToTechnology(string jobNum, string partNum, string fromLoc, string fromLine, string toLoc, string toLine)
        {
            string reqDueDateStr = "";
            string prodQtyStr = "";
            string dueDateStr = "1/1/1000";
            string startDateStr = "1/1/1000";
            string vMessage = "";
            string plantLine = "";
            string resourceGrpID = "";
            string resourceID = "";
            string opCode = "";
            string opDesc = "";
            string opDtlDesc = "";
            string runOutWarning = "";
            string refreshMessage = "";            
            string relatedOp = "";
            string oprSequence = "";
            string laborTrans = "";
            string laborEntryMethod = "";
            string EstProdHours = "";
            string prodStd = "";
            string prodBurRate = "";
            string prodLabRate = "";

            int oprSeq = 0;
            int oprRowIdx = 0;
            int opDtlRowIdx = 0;
            int rowIdx = 0;
            int asmSeq = 0;

            DateTime reqDueDate = DateTime.Now; // These date variable are set to today's date only for initialization purposes.
            DateTime startDate = DateTime.Now;
            DateTime dueDate = DateTime.Now;

            decimal prodQtyDec = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool opChangeDescription = false;
            bool operationExist = false;
            bool epicorSeesionFound = true;

            try
            {
                this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);
            }
            catch (Exception ex)
            {
                epicorSeesionFound = false;
            }

            if (epicorSeesionFound = true)
            {
                if (RelavantLaborTransactionsExist(jobNum, "Technology", out laborTrans) == false)
                {
                    DataTable dtJobOps = objMain.GetOA_JobOperations(jobNum);

                    JobEntry je = new JobEntry(this.epiSession.ConnectionPool);

                    JobEntryDataSet jeds = je.GetByID(jobNum);
                    DataRow[] jhdr = jeds.JobHead.Select();

                    if (jhdr[0]["ProdQty"] != null)
                    {
                        prodQtyStr = jhdr[0]["ProdQty"].ToString();
                        prodQtyDec = decimal.Parse(prodQtyStr);
                    }

                    if (jhdr[0]["ReqDueDate"] != null)
                    {
                        reqDueDateStr = jhdr[0]["ReqDueDate"].ToString();
                        reqDueDate = Convert.ToDateTime(reqDueDateStr);
                    }

                    if (jhdr[0]["StartDate"] != DBNull.Value)
                    {
                        startDateStr = jhdr[0]["StartDate"].ToString();
                    }
                    startDate = Convert.ToDateTime(startDateStr);

                    if (jhdr[0]["DueDate"] != DBNull.Value)
                    {
                        dueDateStr = jhdr[0]["DueDate"].ToString();
                    }
                    dueDate = Convert.ToDateTime(dueDateStr);

                    if (jhdr[0]["JobEngineered"].ToString() == "True")
                    {
                        jobEngineered = true;
                    }

                    if (jhdr[0]["JobReleased"].ToString() == "True")
                    {
                        jobReleased = true;
                    }

                    if (jobEngineered) // If JobEngineered = True then un-engineer the job.
                    {
                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = false;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                        }

                        try
                        {
                            je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                        }

                        try
                        {
                            je.CheckForChanges("KCC", jobNum, false, false, true, false, false, false, false, false, false, out opChangeDescription);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update -- " + ex);
                        }

                        if (startDateStr != "1/1/1000")  //Check to see if the Job has a Scheduled Start Date. If the StartDate is equal to "1/1/1000" then it has not been scheduled.
                        {
                            try
                            {
                                je.RemoveFromSchedule(jobNum);
                                jeds = je.GetByID(jobNum);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - RemoveFromSchedule -- " + ex);
                            }
                        }
                    }

                    plantLine = partNum.Substring(0, 3) + "LINE" + toLine.Substring((toLine.Length - 1), 1);

                    #region Operation 30
                    try
                    {
                        try
                        {
                            oprSeq = 30;

                            DataTable dt30 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt30.Rows.Count > 0)
                            {
                                DataRow dr30 = dt30.Rows[0];
                                resourceID = dr30["ResourceID"].ToString();
                                opCode = dr30["OpCode"].ToString();
                                opDesc = dr30["OpDesc"].ToString();
                                opDtlDesc = dr30["OpDtlDesc"].ToString();
                                resourceGrpID = dr30["ResourceGrpID"].ToString();
                                laborEntryMethod = dr30["LaborEntryMethod"].ToString();
                                EstProdHours = dr30["EstProdHours"].ToString();
                                prodStd = dr30["ProdStandard"].ToString();
                                prodLabRate = dr30["ProdLabRate"].ToString();
                                prodBurRate = dr30["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "30")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "30")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 30;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 30 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 35/36
                    try
                    {
                        try
                        {
                            rowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobMtl"].Rows)
                            {
                                relatedOp = row["RelatedOperation"].ToString();

                                if (relatedOp == "35")
                                {
                                    jeds.Tables["jobMtl"].Rows[rowIdx]["RowMod"] = "U";
                                    je.ChangeJobMtlRelatedOperation(0, jeds);

                                    je.Update(jeds);

                                }
                                ++rowIdx;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobMtlRelatedOperation() -- " + ex);
                        }

                        try
                        {
                            rowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "35")
                                {
                                    jeds.Tables["JobOpDtl"].Rows[rowIdx].Delete();
                                    break;
                                }
                                ++rowIdx;
                            }

                            rowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                            {
                                oprSequence = row["OprSeq"].ToString();

                                if (oprSequence == "35")
                                {
                                    jeds.Tables["JobOper"].Rows[rowIdx].Delete();
                                    je.Update(jeds);
                                    break;
                                }
                                ++rowIdx;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }

                        try
                        {
                            oprSeq = 36;
                            DataTable dt36 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt36.Rows.Count > 0)
                            {
                                DataRow dr36 = dt36.Rows[0];
                                resourceID = dr36["ResourceID"].ToString();
                                opCode = dr36["OpCode"].ToString();
                                opDesc = dr36["OpDesc"].ToString();
                                opDtlDesc = dr36["OpDtlDesc"].ToString();
                                resourceGrpID = dr36["ResourceGrpID"].ToString();
                                laborEntryMethod = dr36["LaborEntryMethod"].ToString();
                                EstProdHours = dr36["EstProdHours"].ToString();
                                prodStd = dr36["ProdStandard"].ToString();
                                prodLabRate = dr36["ProdLabRate"].ToString();
                                prodBurRate = dr36["ProdBurRate"].ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        if (OperationExist(dtJobOps, "36") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 36, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;


                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 36;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                //jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 36;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }

                            try
                            {
                                rowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobMtl"].Rows)
                                {
                                    relatedOp = row["RelatedOperation"].ToString();

                                    if (relatedOp == "0")
                                    {
                                        jeds.Tables["jobMtl"].Rows[rowIdx]["RowMod"] = "U";
                                        je.ChangeJobMtlRelatedOperation(36, jeds);

                                        je.Update(jeds);
                                    }
                                    ++rowIdx;
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobMtlRelatedOperation() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 36 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 40
                    try
                    {
                        try
                        {
                            oprSeq = 40;

                            DataTable dt40 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt40.Rows.Count > 0)
                            {
                                DataRow dr40 = dt40.Rows[0];
                                resourceID = dr40["ResourceID"].ToString();
                                opCode = dr40["OpCode"].ToString();
                                opDesc = dr40["OpDesc"].ToString();
                                opDtlDesc = dr40["OpDtlDesc"].ToString();
                                resourceGrpID = dr40["ResourceGrpID"].ToString();
                                laborEntryMethod = dr40["LaborEntryMethod"].ToString();
                                EstProdHours = dr40["EstProdHours"].ToString();
                                prodStd = dr40["ProdStandard"].ToString();
                                prodLabRate = dr40["ProdLabRate"].ToString();
                                prodBurRate = dr40["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "40")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "40")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 40;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 40 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 41
                    try
                    {
                        oprSeq = 41;
                        DataTable dt41 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt41.Rows.Count > 0)
                        {
                            DataRow dr41 = dt41.Rows[0];
                            resourceID = dr41["ResourceID"].ToString();
                            opCode = dr41["OpCode"].ToString();
                            opDesc = dr41["OpDesc"].ToString();
                            opDtlDesc = dr41["OpDtlDesc"].ToString();
                            resourceGrpID = dr41["ResourceGrpID"].ToString();
                            laborEntryMethod = dr41["LaborEntryMethod"].ToString();
                            EstProdHours = dr41["EstProdHours"].ToString();
                            prodStd = dr41["ProdStandard"].ToString();
                            prodLabRate = dr41["ProdLabRate"].ToString();
                            prodBurRate = dr41["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "41") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 41, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 41;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;

                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 41;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "41")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "41")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 41;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 41 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 50
                    try
                    {
                        try
                        {
                            oprSeq = 50;

                            DataTable dt50 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt50.Rows.Count > 0)
                            {
                                DataRow dr50 = dt50.Rows[0];
                                opCode = dr50["OpCode"].ToString();
                                opDesc = dr50["OpDesc"].ToString();
                                resourceID = dr50["ResourceID"].ToString();
                                opDtlDesc = dr50["OpDtlDesc"].ToString();
                                resourceGrpID = dr50["ResourceGrpID"].ToString();
                                laborEntryMethod = dr50["LaborEntryMethod"].ToString();
                                EstProdHours = dr50["EstProdHours"].ToString();
                                prodStd = dr50["ProdStandard"].ToString();
                                prodLabRate = dr50["ProdLabRate"].ToString();
                                prodBurRate = dr50["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "50")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "50")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 50;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 50 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 51
                    try
                    {
                        oprSeq = 51;
                        DataTable dt51 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt51.Rows.Count > 0)
                        {
                            DataRow dr51 = dt51.Rows[0];
                            resourceID = dr51["ResourceID"].ToString();
                            opCode = dr51["OpCode"].ToString();
                            opDtlDesc = dr51["OpDtlDesc"].ToString();
                            resourceGrpID = dr51["ResourceGrpID"].ToString();
                            laborEntryMethod = dr51["LaborEntryMethod"].ToString();
                            EstProdHours = dr51["EstProdHours"].ToString();
                            prodStd = dr51["ProdStandard"].ToString();
                            prodLabRate = dr51["ProdLabRate"].ToString();
                            prodBurRate = dr51["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "51") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 51, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 51;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;

                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 51;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "51")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "51")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 51;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 51 Issues -- " + ex);
                    }
                    #endregion

                    #region Operarion 55
                    try
                    {
                        try
                        {
                            oprSeq = 55;

                            DataTable dt55 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt55.Rows.Count > 0)
                            {
                                DataRow dr55 = dt55.Rows[0];
                                opCode = dr55["OpCode"].ToString();
                                opDesc = dr55["OpDesc"].ToString();
                                resourceID = dr55["ResourceID"].ToString();
                                opDtlDesc = dr55["OpDtlDesc"].ToString();
                                resourceGrpID = dr55["ResourceGrpID"].ToString();
                                laborEntryMethod = dr55["LaborEntryMethod"].ToString();
                                EstProdHours = dr55["EstProdHours"].ToString();
                                prodStd = dr55["ProdStandard"].ToString();
                                prodLabRate = dr55["ProdLabRate"].ToString();
                                prodBurRate = dr55["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "55")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "55")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 55;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 55 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 60
                    try
                    {
                        try
                        {
                            oprSeq = 60;

                            DataTable dt60 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt60.Rows.Count > 0)
                            {
                                DataRow dr60 = dt60.Rows[0];
                                opCode = dr60["OpCode"].ToString();
                                opDesc = dr60["OpDesc"].ToString();
                                resourceID = dr60["ResourceID"].ToString();
                                opDtlDesc = dr60["OpDtlDesc"].ToString();
                                resourceGrpID = dr60["ResourceGrpID"].ToString();
                                laborEntryMethod = dr60["LaborEntryMethod"].ToString();
                                EstProdHours = dr60["EstProdHours"].ToString();
                                prodStd = dr60["ProdStandard"].ToString();
                                prodLabRate = dr60["ProdLabRate"].ToString();
                                prodBurRate = dr60["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "60")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "60")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 60;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 60 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 65
                    try
                    {
                        try
                        {
                            oprSeq = 65;

                            DataTable dt65 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt65.Rows.Count > 0)
                            {
                                DataRow dr65 = dt65.Rows[0];
                                opCode = dr65["OpCode"].ToString();
                                opDesc = dr65["OpDesc"].ToString();
                                resourceID = dr65["ResourceID"].ToString();
                                opDtlDesc = dr65["OpDtlDesc"].ToString();
                                resourceGrpID = dr65["ResourceGrpID"].ToString();
                                laborEntryMethod = dr65["LaborEntryMethod"].ToString();
                                EstProdHours = dr65["EstProdHours"].ToString();
                                prodStd = dr65["ProdStandard"].ToString();
                                prodLabRate = dr65["ProdLabRate"].ToString();
                                prodBurRate = dr65["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "65")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "65")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 65;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 65 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 70
                    try
                    {
                        try
                        {
                            oprSeq = 70;

                            DataTable dt70 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                            if (dt70.Rows.Count > 0)
                            {
                                DataRow dr70 = dt70.Rows[0];
                                opCode = dr70["OpCode"].ToString();
                                opDesc = dr70["OpDesc"].ToString();
                                resourceID = dr70["ResourceID"].ToString();
                                opDtlDesc = dr70["OpDtlDesc"].ToString();
                                resourceGrpID = dr70["ResourceGrpID"].ToString();
                                laborEntryMethod = dr70["LaborEntryMethod"].ToString();
                                EstProdHours = dr70["EstProdHours"].ToString();
                                prodStd = dr70["ProdStandard"].ToString();
                                prodLabRate = dr70["ProdLabRate"].ToString();
                                prodBurRate = dr70["ProdBurRate"].ToString();

                                oprRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOper"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "70")
                                    {
                                        break;
                                    }
                                    ++oprRowIdx;
                                }

                                opDtlRowIdx = 0;
                                foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                                {
                                    if (row["OprSeq"].ToString() == "70")
                                    {
                                        break;
                                    }
                                    ++opDtlRowIdx;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - GetOprDataByPlantLineAndOpr() -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                            jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 70;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                            jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                            je.ChangeJobOpDtlResourceID(resourceID, jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                        }

                        try
                        {
                            je.Update(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update() -- " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Operation 70 Issues -- " + ex);
                    }
                    #endregion


                    #region Operation 71
                    try
                    {
                        oprSeq = 71;
                        DataTable dt71 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt71.Rows.Count > 0)
                        {
                            DataRow dr71 = dt71.Rows[0];
                            resourceID = dr71["ResourceID"].ToString();
                            opCode = dr71["OpCode"].ToString();
                            opDesc = dr71["OpDesc"].ToString();
                            opDtlDesc = dr71["OpDtlDesc"].ToString();
                            resourceGrpID = dr71["ResourceGrpID"].ToString();
                            laborEntryMethod = dr71["LaborEntryMethod"].ToString();
                            EstProdHours = dr71["EstProdHours"].ToString();
                            prodStd = dr71["ProdStandard"].ToString();
                            prodLabRate = dr71["ProdLabRate"].ToString();
                            prodBurRate = dr71["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "71") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 71, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 71;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 71;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "71")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "71")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 71;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 71 Issues -- " + ex);
                    }
                    #endregion

                    #region Operation 80
                    try
                    {
                        oprSeq = 80;
                        DataTable dt80 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt80.Rows.Count > 0)
                        {
                            DataRow dr80 = dt80.Rows[0];
                            resourceID = dr80["ResourceID"].ToString();
                            opCode = dr80["OpCode"].ToString();
                            opDesc = dr80["OpDesc"].ToString();
                            opDtlDesc = dr80["OpDtlDesc"].ToString();
                            resourceGrpID = dr80["ResourceGrpID"].ToString();
                            laborEntryMethod = dr80["LaborEntryMethod"].ToString();
                            EstProdHours = dr80["EstProdHours"].ToString();
                            prodStd = dr80["ProdStandard"].ToString();
                            prodLabRate = dr80["ProdLabRate"].ToString();
                            prodBurRate = dr80["ProdBurRate"].ToString();
                        }

                        if (OperationExist(dtJobOps, "80") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 80, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 80;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 80;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "80")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "80")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 80;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 80 Issues -- " + ex);
                    }                   
                    #endregion

                    #region Operation 90
                    try
                    {
                        oprSeq = 90;

                        DataTable dt90 = objTran.GetOprDataByPlantLineAndOpr(plantLine, oprSeq);

                        if (dt90.Rows.Count > 0)
                        {
                            DataRow dr90 = dt90.Rows[0];
                            resourceID = dr90["ResourceID"].ToString();
                            opCode = dr90["OpCode"].ToString();
                            opDesc = dr90["OpDesc"].ToString();
                            opDtlDesc = dr90["OpDtlDesc"].ToString();
                            resourceGrpID = dr90["ResourceGrpID"].ToString();
                            laborEntryMethod = dr90["LaborEntryMethod"].ToString();
                            EstProdHours = dr90["EstProdHours"].ToString();
                            prodStd = dr90["ProdStandard"].ToString();
                            prodLabRate = dr90["ProdLabRate"].ToString();
                            prodBurRate = dr90["ProdBurRate"].ToString();
                        }


                        if (OperationExist(dtJobOps, "90") == false)
                        {
                            try
                            {
                                je.GetNewJobOper(jeds, jobNum, asmSeq);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - GetNewJobOper() -- " + ex);
                            }

                            try
                            {
                                je.CheckJobOperOprSeq(jobNum, asmSeq, 90, out vMessage);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - CheckJobOperOprSeq() -- " + ex);
                            }

                            try
                            {
                                oprRowIdx = jeds.Tables["JobOper"].Rows.Count - 1;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OprSeq"] = 90;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCodeOpDesc"] = opDtlDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;

                                je.ChangeJobOperOpCode(opCode, out refreshMessage, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpCode() -- " + ex);
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpStdID"] = " ";
                                je.ChangeJobOperOpStdID(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperOpStdID() -- " + ex);
                            }

                            try
                            {
                                je.ChangeJobOperLaborEntryMethod("Q", jeds);
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOperLaborEntryMethod() -- " + ex);
                            }

                            try
                            {
                                opDtlRowIdx = jeds.Tables["JobOpDtl"].Rows.Count - 1;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["RowMod"] = "U";
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 90;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;
                                je.ChangeJobOpDtlResourceGrpID(resourceGrpID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceGrpID() -- " + ex);
                            }


                            try
                            {
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                        else
                        {
                            oprRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "90")
                                {
                                    break;
                                }
                                ++oprRowIdx;
                            }

                            opDtlRowIdx = 0;
                            foreach (DataRow row in jeds.Tables["jobOpDtl"].Rows)
                            {
                                if (row["OprSeq"].ToString() == "90")
                                {
                                    break;
                                }
                                ++opDtlRowIdx;
                            }

                            try
                            {
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpCode"] = opCode;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["OpDesc"] = opDesc;
                                jeds.Tables["JobOper"].Rows[oprRowIdx]["LaborEntryMethod"] = laborEntryMethod;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceGrpID"] = resourceGrpID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ResourceID"] = resourceID;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OpDtlDesc"] = opDtlDesc;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["OprSeq"] = 90;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["EstProdHours"] = EstProdHours;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdStandard"] = prodStd;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdLabRate"] = prodLabRate;
                                jeds.Tables["JobOpDtl"].Rows[opDtlRowIdx]["ProdBurRate"] = prodBurRate;

                                je.ChangeJobOpDtlResourceID(resourceID, jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - ChangeJobOpDtlResourceID() -- " + ex);
                            }

                            try
                            {
                                je.Update(jeds);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Update() -- " + ex);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Adding Operation 80 Issues -- " + ex);
                    }                    
                    #endregion

                    if (jobReleased)
                    {
                        try
                        {
                            je.CheckResourcePlants(jobNum, out runOutWarning);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;
                            jeds.Tables["JobHead"].Rows[0]["JobReleased"] = true;
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                        }

                        try
                        {
                            je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                        }

                        try
                        {
                            je.CheckForChanges("KCC", jobNum, false, false, true, false, false, false, false, false, false, out opChangeDescription);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                            je.Update(jeds);
                            MessageBox.Show("Line Transfer Complete!");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update -- " + ex);
                        }
                    }
                    else if (jobEngineered)
                    {
                        jeds.Tables["JobHead"].Rows[0]["JobEngineered"] = true;

                        try
                        {
                            je.CheckResourcePlants(jobNum, out runOutWarning);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckResourcePlants -- " + ex);
                        }

                        try
                        {
                            je.ChangeJobHeadJobEngineered(jeds);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - ChangeJobHeadJobEngineered -- " + ex);
                        }

                        try
                        {
                            je.CheckToReschedule("KCC", jobNum, reqDueDate, prodQtyDec, dueDate, startDate, false, out vMessage);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckToReschedule -- " + ex);
                        }

                        try
                        {
                            je.CheckForChanges("KCC", jobNum, true, false, true, false, false, false, false, false, false, out opChangeDescription);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - CheckForChanges -- " + ex);
                        }

                        try
                        {
                            jeds.Tables["JobHead"].Rows[0]["ChangeDescription"] = "System Job Line Change.";
                            je.Update(jeds);
                            MessageBox.Show("Line Transfer Complete!");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Update -- " + ex);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Line Transfer Complete!");
                    }
                }
                else
                {
                    MessageBox.Show("ERROR - Transfer cannot take place. Labor transactions exist for operation/s: " + laborTrans);
                }
            }
            else
            {
                MessageBox.Show("WARNING - No Epicor License is available at this time. Please try again later");
            }
        }

        private bool OperationExist(DataTable dtJobOps, string oprSeq)
        {
            bool oprExist = false;

            foreach (DataRow row in dtJobOps.Rows)
            {
                if (oprSeq == row["OprSeq"].ToString())
                {
                    oprExist = true;
                    break;
                }
            }
            return oprExist;
        }

        private bool RelavantLaborTransactionsExist(string jobNum, string plantLoc, out string laborTrans)
        {
            bool laborTransExist = false;
            bool opr30Exist = false;
            bool opr40Exist = false;
            bool opr50Exist = false;
            bool opr60Exist = false;
            bool opr70Exist = false;

            laborTrans = "";

            DataTable dt = objMain.GetLaborDetailDataByJobNum(jobNum);

            foreach(DataRow row in dt.Rows)
            {
                if (row["OprSeq"].ToString() == "30")
                {
                    if (plantLoc == "Technology")
                    {
                        laborTransExist = true;
                        if (opr30Exist == false)
                        {
                            laborTrans = "30";
                            opr30Exist = true;
                        }
                    }
                }
                if (row["OprSeq"].ToString() == "40")
                {                   
                    laborTransExist = true;
                    if (opr40Exist == false)
                    {
                        if (laborTrans != "")
                        {
                            laborTrans += ", ";
                        }
                        laborTrans += "40";
                        opr40Exist = true;
                    }
                }
                if (row["OprSeq"].ToString() == "50")
                {
                    laborTransExist = true;
                    if (opr50Exist == false)
                    {
                        if (laborTrans != "")
                        {
                            laborTrans += ", ";
                        }
                        laborTrans += "50";
                        opr50Exist = true;
                    }
                }
                if (row["OprSeq"].ToString() == "60")
                {
                    laborTransExist = true;
                    if (opr60Exist == false)
                    {
                        if (laborTrans != "")
                        {
                            laborTrans += ", ";
                        }
                        laborTrans += "60";
                        opr60Exist = true;
                    }
                }
                if (row["OprSeq"].ToString() == "70")
                {
                    laborTransExist = true;
                    if (opr70Exist == false)
                    {
                        if (laborTrans != "")
                        {
                            laborTrans += ", ";
                        }
                        laborTrans += "70";
                        opr70Exist = true;
                    }
                }
            }

            return laborTransExist;
        }

        private string CalcDueDateAndTime(DateTime startDate, string startTime, double prodHrs)
        {
            string dueDateTime = "";            
            string startHr = startTime.Substring(0, startTime.IndexOf(":"));           

            int iProdDays = (int) prodHrs / 8;
            int endHour = 0;

            double dProdDays = (double) iProdDays;
            
            DateTime endDate;
            
            var addHrs = prodHrs % 8;

            endDate = startDate.AddDays(dProdDays);
            endHour = Int32.Parse(startHr);
            endHour += (int) addHrs;           
           
            if (endHour > 14) // If the end hour is greater than 2pm the add a day
            {
                endDate = endDate.AddDays(1);
                endHour = 5 + (endHour - 14);  // Add any hrs past 2pm to the Start of Day time of 05:00
            }

            if (endDate.DayOfWeek == DayOfWeek.Saturday || endDate.DayOfWeek == DayOfWeek.Sunday)
            {
                endDate = endDate.AddDays(2);
            }

            dueDateTime = endDate.ToShortDateString() + "=" + endHour.ToString();
            return dueDateTime;
        }

        private double CalcPrdHours(DataTable dtJobOprs)
        {
            double totalProdHrs = 0;

            foreach(DataRow row in dtJobOprs.Rows)
            {
                if (row["AssemblySeq"].ToString() != "1")
                {
                    totalProdHrs += double.Parse(row["ProdStandard"].ToString());
                }
                else
                {
                    totalProdHrs += 8;
                }                
            }

            return totalProdHrs;
        }

        private void CalcOprStartAndEnd(int asmSeq, int oprSeq, string unitEndDate, string unitEndHour, out string oprStartDate, out string oprStartHour, out string oprEndDate, out string oprEndHour)
        {
           if (asmSeq == 0)
           {
               oprStartDate = unitEndDate;
               oprStartHour = unitEndHour;
               oprEndDate = unitEndDate;
               oprEndHour = unitEndHour;
           }
           else if (asmSeq == 1)
           {
               oprStartDate = unitEndDate;
               oprStartHour = unitEndHour;
               oprEndDate = unitEndDate;
               oprEndHour = unitEndHour;
           }
           else
           {
               oprStartDate = unitEndDate;
               oprStartHour = unitEndHour;
               oprEndDate = unitEndDate;
               oprEndHour = unitEndHour;
           }

        }

        private JobEntryDataSet AddNewOprToJobOper(JobEntryDataSet jeds, string jobNum, int asmSeq, int oprSeq)
        {
            string partNum = jeds.Tables["JobHead"].Rows[0]["PartNum"].ToString();
            string partDesc = jeds.Tables["JobHead"].Rows[0]["PartDescription"].ToString();
            string partRev = jeds.Tables["JobHead"].Rows[0]["RevisionNum"].ToString();

            DataRow dr = jeds.Tables["JobOper"].NewRow();
            dr["Company"] = "KCC";
            dr["JobComplete"] = false;
            dr["OpComplete"] = false;
            dr["JobNum"] = jobNum;
            dr["AssemblySeq"] = asmSeq;
            dr["OprSeq"] = oprSeq;
            dr["OpCode"] = " ";
            dr["OpStdID"] = " ";
            dr["EstSetHours"] = 0;
            dr["EstProdHours"] = 0;
            dr["ProdStandard"] = 0;
            dr["StdFormat"] = "HP";
            dr["StdBasis"] = "E";
            dr["OpsPerPart"] = 0;
            dr["QtyPer"] = 1;
            dr["QueStartDate"] = DBNull.Value;
            dr["QueStartHour"] = 0;
            dr["StartDate"] = DBNull.Value;
            dr["StartHour"] = 0;
            dr["DueDate"] = DBNull.Value;
            dr["DueHour"] = 0;
            dr["MoveDueDate"] = DBNull.Value;
            dr["MoveDueHour"] = 0;
            dr["ProdLabRate"] = 0;
            dr["SetupLabRate"] = 0;
            dr["ProdBurRate"] = 0;
            dr["SetupBurRate"] = 0;
            dr["AddedOper"] = false;
            dr["Machines"] = 1;
            dr["SetUpCrewSize"] = 1;
            dr["ProdCrewSize"] = 1;
            dr["ProdComplete"] = false;
            dr["SetupComplete"] = false;
            dr["ActProdHours"] = 0;
            dr["ActProdRwkHours"] = 0;
            dr["ActSetupHours"] = 0;
            dr["ActSetupRwkHours"] = 0;
            dr["QtyCompleted"] = 0;
            dr["SetupPctComplete"] = 0;
            dr["EstScrap"] = 0;
            dr["EstScrapType"] = "%";
            dr["SubContract"] = false;
            dr["IUM"] = "EA";
            dr["EstUnitCost"] = 0;
            dr["DaysOut"] = 0;
            dr["PartNum"] = partNum;
            dr["Description"] = partDesc;
            dr["VendorNum"] = 0;
            dr["PurPoint"] = " ";
            dr["CommentText"] = " ";
            dr["SchedRelation"] = "FS";
            dr["RunQty"] = 1;
            dr["WIName"] = " ";
            dr["WIMachines"] = 1;
            dr["WIQueStartDate"] = DBNull.Value;
            dr["WIQueStartHour"] = 0;
            dr["WIStartDate"] = DBNull.Value;
            dr["WIStartHour"] = 0;
            dr["WIDueDate"] = DBNull.Value;
            dr["WIDueHour"] = 0;
            dr["WIMoveDueDate"] = DBNull.Value;
            dr["WIMoveDueHour"] = 0;
            dr["WIHoursPerMachine"] = 8;
            dr["WILoadDate"] = DBNull.Value;
            dr["WILoadHour"] = 0;
            dr["ActBurCost"] = 0;
            dr["ActLabCost"] = 0;
            dr["ReworkBurCost"] = 0;
            dr["ReworkLabCost"] = 0;
            dr["MiscAmt"] = 0;
            dr["HoursPerMachine"] = 8;
            dr["LoadDate"] = DBNull.Value;
            dr["LoadHour"] = 0;
            dr["ReloadNum"] = 0;
            dr["SndAlrtCmpl"] = false;
            dr["RcvInspectionReq"] = false;
            dr["JobEngineered"] = false;
            dr["EstSetHoursPerMch"] = 0;
            dr["RevisionNum"] = partRev;
            dr["AutoReceiptDate"] = DBNull.Value;
            dr["LastLaborDate"] = DBNull.Value;
            dr["CallNum"] = 0;
            dr["CallLine"] = 0;            
            dr["LaborRate"] = 0;
            dr["BillableLaborRate"] = 0;
            dr["DocLaborRate"] = 0;
            dr["DocBillableLaborRate"] = 0;
            dr["Billable"] = false;
            dr["UnitPrice"] = 0;
            dr["BillableUnitPrice"] = 0;
            dr["DocBillableUnitPrice"] = 0;
            dr["DocUnitPrice"] = 0;
            dr["LaborEntryMethod"] = "T";
            dr["PricePerCode"] = "E";
            dr["FAQty"] = 0;
            dr["QtyStagedToDate"] = 0;
            dr["RFQNeeded"] = false;
            dr["RFQVendQuotes"] = 0;
            dr["RFQNum"] = 0;
            dr["RFQLine"] = 0;
            dr["RFQStat"] = " ";
            dr["SetupGroup"] = "KYVIC02";
            dr["RestoreFlag"] = " ";
            dr["AnalysisCode"] = " ";
            dr["PrimarySetupOpDtl"] = 0;
            dr["PrimaryProdOpDtl"] = 0;
            dr["OpDesc"] = " ";
            dr["Character01"] = " ";
            dr["Character02"] = " ";
            dr["Character03"] = " ";
            dr["Character04"] = " ";
            dr["Character05"] = " ";
            dr["Character06"] = " ";
            dr["Character07"] = " ";
            dr["Character08"] = " ";
            dr["Character09"] = " ";
            dr["Character10"] = " ";
            dr["Number01"] = 0;
            dr["Number02"] = 0;
            dr["Number03"] = 0;
            dr["Number04"] = 0;
            dr["Number05"] = 0;
            dr["Number06"] = 0;
            dr["Number07"] = 0;
            dr["Number08"] = 0;
            dr["Number09"] = 0;
            dr["Number10"] = 0;
            dr["Date01"] = DBNull.Value;
            dr["Date02"] = DBNull.Value;
            dr["Date03"] = DBNull.Value;
            dr["Date04"] = DBNull.Value;
            dr["Date05"] = DBNull.Value;
            dr["CheckBox01"] = false;
            dr["CheckBox02"] = false;
            dr["CheckBox03"] = false;
            dr["CheckBox04"] = false;
            dr["CheckBox05"] = false;
            dr["KitDate"] = DBNull.Value;
            dr["Number11"] = 0;
            dr["Number12"] = 0;
            dr["Number13"] = 0;
            dr["Number14"] = 0;
            dr["Number15"] = 0;
            dr["Number16"] = 0;
            dr["Number17"] = 0;
            dr["Number18"] = 0;
            dr["Number19"] = 0;
            dr["Number20"] = 0;
            dr["Date06"] = DBNull.Value;
            dr["Date07"] = DBNull.Value;
            dr["Date08"] = DBNull.Value;
            dr["Date09"] = DBNull.Value;
            dr["Date10"] = DBNull.Value;
            dr["Date11"] = DBNull.Value;
            dr["Date12"] = DBNull.Value;
            dr["Date13"] = DBNull.Value;
            dr["Date14"] = DBNull.Value;
            dr["Date15"] = DBNull.Value;
            dr["Date16"] = DBNull.Value;
            dr["Date17"] = DBNull.Value;
            dr["Date18"] = DBNull.Value;
            dr["Date19"] = DBNull.Value;
            dr["Date20"] = DBNull.Value;
            dr["CheckBox06"] = false;
            dr["CheckBox07"] = false;
            dr["CheckBox08"] = false;
            dr["CheckBox09"] = false;
            dr["CheckBox10"] = false;
            dr["CheckBox11"] = false;
            dr["CheckBox12"] = false;
            dr["CheckBox13"] = false;
            dr["CheckBox14"] = false;
            dr["CheckBox15"] = false;
            dr["CheckBox16"] = false;
            dr["CheckBox17"] = false;
            dr["CheckBox18"] = false;
            dr["CheckBox19"] = false;
            dr["CheckBox20"] = false;
            dr["ShortChar01"] = " ";
            dr["ShortChar02"] = " ";
            dr["ShortChar03"] = " ";
            dr["ShortChar04"] = " ";
            dr["ShortChar05"] = " ";
            dr["ShortChar06"] = " ";
            dr["ShortChar07"] = " ";
            dr["ShortChar08"] = " ";
            dr["ShortChar09"] = " ";
            dr["ShortChar10"] = " ";
            dr["GlbRFQ"] = false;
            dr["BookedUnitCost"] = 0;
            dr["RecalcExpProdYld"] = false;
            dr["UserMapData"] = " ";
            dr["RoughCutSched"] = false;
            dr["SchedComment"] = " ";
            dr["Rpt1BillableLaborRate"] = 0;
            dr["Rpt2BillableLaborRate"] = 0;
            dr["Rpt3BillableLaborRate"] = 0;
            dr["Rpt1BillableUnitPrice"] = 0;
            dr["Rpt2BillableUnitPrice"] = 0;
            dr["Rpt3BillableUnitPrice"] = 0;
            dr["Rpt1LaborRate"] = 0;
            dr["Rpt2LaborRate"] = 0;
            dr["Rpt3LaborRate"] = 0;
            dr["Rpt1UnitPrice"] = 0;
            dr["Rpt2UnitPrice"] = 0;
            dr["Rpt3UnitPrice"] = 0;
            dr["SNRequiredOpr"] = false;
            dr["SNRequiredSubConShip"] = false;
            dr["SysRowID"] = " ";
            dr["SysRevID"] = 0;
            dr["Weight"] = 0;
            dr["WeightUOM"] = " ";
            dr["BitFlag"] = 0;
            dr["SendAheadType"] = "HOURS";
            dr["SendAheadOffset"] = 0;
            dr["PrjRoleList"] = " ";
            dr["TearDwnEndDate"] = DBNull.Value;
            dr["TearDwnEndHour"] = 0;
            dr["WITearDwnEndDate"] = DBNull.Value;
            dr["WITearDwnEndHour"] = 0;
            dr["UseAllRoles"] = true;
            dr["AssetPartNum"] = " ";
            dr["SerialNumber"] = " ";
            dr["ProductionQty"] = 1;
            dr["ScrapQty"] = 0;
            dr["EstLabHours"] = 0;
            dr["DisplayServLaborRate"] = 0;
            dr["DisplayServAmt"] = 0;
            dr["DocDisplayServLaborRate"] = 0;
            dr["DocDisplayServAmt"] = 0;
            dr["DisplayExtPrice"] = 0;
            dr["FinalOpr"] = false;
            dr["AutoReceive"] = false;
            dr["DisplayUnitPrice"] = 0;
            dr["CurrencySwitch"] = false;
            dr["DocDisplayUnitPrice"] = 0;
            dr["DocDisplayExtPrice"] = 0;
            dr["ShowStatusIcon"] = "Normal";
            dr["CurrSymbol"] = " ";
            dr["EnableSndAlrtCmpl"] = false;
            dr["EnableAutoReceive"] = true;
            dr["OpStdDescription"] = " ";
            dr["PrimaryResourceGrpID"] = " ";
            dr["PrimaryResourceGrpDesc"] = " ";
            dr["LaborEntryMethodDesc"] = " ";
            dr["RefreshResources"] = false;
            dr["StdFormatDescription"] = " ";
            dr["StdBasisDescription"] = " ";
            dr["PrimaryProdOpDtlDesc"] = " ";
            dr["PrimarySetupOpDtlDesc"] = " ";
            dr["ActSubCost"] = 0;
            dr["EstBurdenCost"] = 0;
            dr["EstLaborCost"] = 0;
            dr["EstSubCost"] = 0;
            dr["AllowJobCosts"] = " ";
            dr["LoadHrs"] = 0;
            dr["IsBaseCurrency"] = false;
            dr["EnableSNRequiredOpr"] = true;
            dr["CurrencyCode"] = " ";
            dr["Rpt1DisplayServLaborRate"] = 0;
            dr["Rpt2DisplayServLaborRate"] = 0;
            dr["Rpt3DisplayServLaborRate"] = 0;
            dr["Rpt1DisplayServAmt"] = 0;
            dr["Rpt2DisplayServAmt"] = 0;
            dr["Rpt3DisplayServAmt"] = 0;
            dr["Rpt1DisplayUnitPrice"] = 0;
            dr["Rpt2DisplayUnitPrice"] = 0;
            dr["Rpt3DisplayUnitPrice"] = 0;
            dr["Rpt1DisplayExtPrice"] = 0;
            dr["Rpt2DisplayExtPrice"] = 0;
            dr["Rpt3DisplayExtPrice"] = 0;
            dr["EnableSNReqSubConShip"] = false;
            dr["DspIUM"] = "EA";
            dr["AnalysisCdDescription"] = " ";
            dr["AssemblySeqDescription"] = " ";
            dr["CallLineLineDesc"] = " ";
            dr["JobNumPartDescription"] = " ";
            dr["OpCodeOpDesc"] = " ";
            dr["PartNumSalesUM"] = " ";
            dr["PartNumTrackLots"] = false;
            dr["PartNumPartDescription"] = " ";
            dr["PartNumTrackSerialNum"] = false;
            dr["PartNumSellingFactor"] = 1;
            dr["PartNumPricePerCode"] = "E";
            dr["PartNumTrackDimension"] = false;
            dr["PartNumIUM"] = " ";
            dr["RFQLineLineDesc"] = " ";
            dr["SetupGroupDescription"] = " ";
            dr["VendorNumName"] = " ";
            dr["VendorNumAddress3"] = " ";
            dr["VendorNumVendorID"] = " ";
            dr["VendorNumCurrencyCode"] = " ";
            dr["VendorNumState"] = " ";
            dr["VendorNumAddress1"] = " ";
            dr["VendorNumCity"] = " ";
            dr["VendorNumZIP"] = " ";
            dr["VendorNumDefaultFOB"] = " ";
            dr["VendorNumAddress2"] = " ";
            dr["VendorNumTermsCode"] = " ";
            dr["VendorNumCountry"] = " ";
            dr["VendorPPAddress2"] = " ";
            dr["VendorPPCountry"] = " ";
            dr["VendorPPState"] = " ";
            dr["VendorPPAddress3"] = " ";
            dr["VendorPPName"] = " ";
            dr["VendorPPZip"] = " ";
            dr["VendorPPCity"] = " ";
            dr["VendorPPAddress1"] = " ";
            dr["VendorPPPrimPCon"] = 0;
            dr["RowIdent"] = " ";
            dr["RowMod"] = "A";
            dr["DBRowIdent"] = DBNull.Value;
            jeds.Tables["JobOper"].Rows.Add(dr);

            return jeds;
        }
    }
}


