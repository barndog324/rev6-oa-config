﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_Config_Rev6
{
    public class MainDTO
    {
        public string EpicorServer { get; set; }
        public string ReportString { get; set; }
        public string ProgPath { get; set; }

        // ETL Label Values
        public string Circuit1Charge { get; set; }
        public string Circuit2Charge { get; set; }

        public string Comp1LRA { get; set; }
        public string Comp2LRA { get; set; }
        public string Comp3LRA { get; set; }
        public string Comp4LRA { get; set; }
        public string Comp5LRA { get; set; }
        public string Comp6LRA { get; set; }

        public string Comp1Phase { get; set; }
        public string Comp2Phase { get; set; }
        public string Comp3Phase { get; set; }
        public string Comp4Phase { get; set; }
        public string Comp5Phase { get; set; }
        public string Comp6Phase { get; set; }

        public string Comp1Qty { get; set; }
        public string Comp2Qty { get; set; }
        public string Comp3Qty { get; set; }
        public string Comp4Qty { get; set; }
        public string Comp5Qty { get; set; }
        public string Comp6Qty { get; set; }

        public string Comp1RLA_Volts { get; set; }
        public string Comp2RLA_Volts { get; set; }
        public string Comp3RLA_Volts { get; set; }
        public string Comp4RLA_Volts { get; set; }
        public string Comp5RLA_Volts { get; set; }
        public string Comp6RLA_Volts { get; set; }
       
        public string DF_StaticPressure { get; set; }
        public string DF_MaxHtgInputBTUH { get; set; }
        public string DF_MinHeatingInput { get; set; }
        public string DF_MinPressureDrop { get; set; }
        public string DF_MaxPressureDrop { get; set; }
        public string DF_ManifoldPressure { get; set; }
        public string DF_CutoutTemp { get; set; }
        public string DF_TempRise { get; set; }
        public string DF_MaxGasPressure { get; set; }
        public string DF_MinGasPressure { get; set; }

        public string Digit1 { get; set; }
        public string Digit2 { get; set; }
        public string Digit456 { get; set; }
        public string Digit7 { get; set; }
        public string Digit8 { get; set; }
        public string Digit10 { get; set; }
        public string Digit22 { get; set; }
        public string Digit32 { get; set; }
        public string Digit33 { get; set; }
        public string Digit37 { get; set; }

        public byte DualPointPower { get; set; }
        public string ElecRating { get; set; }
        public string EnteringTemp { get; set; }

        public string FanCondQty { get; set; }
        public string FanErvQty { get; set; }
        public string FanEvapQty { get; set; }        
        public string FanPwrExhQty { get; set; }

        public string FanCondPhase { get; set; }
        public string FanErvPhase { get; set; }
        public string FanEvapPhase { get; set; }
        public string FanPwrExhPhase { get; set; }

        public string FanCondFLA { get; set; }
        public string FanErvFLA { get; set; }
        public string FanEvapFLA { get; set; }
        public string FanPwrExhFLA { get; set; }

        public string FanCondHP { get; set; }
        public string FanErvHP { get; set; }
        public string FanEvapHP { get; set; }
        public string FanPwrExhHP { get; set; }

        public string FlowRate { get; set; }
        public string French { get; set; }
        public string FuelType { get; set; }

        public string HeatingType { get; set; }
        public string HeatingInputElectric { get; set; }
        
        public string IN_MaxHtgInputBTUH { get; set; }
        public string IN_HeatingOutputBTUH { get; set; }
        public string IN_MinInputBTU { get; set; }
        public string IN_MaxExt { get; set; }
        public string IN_TempRise { get; set; }
        public string IN_MaxOutAirTemp { get; set; }
        public string IN_MaxGasPressure { get; set; }
        public string IN_MinGasPressure { get; set; }
        public string IN_ManifoldPressure { get; set; }

        public string JobNum { get; set; }

        public string MfgDate { get; set; }
        public string MfsMcb { get; set; }
        public string MfsMcb2 { get; set; }
        public string MinCKTAmp { get; set; }
        public string MinCKTAmp2 { get; set; }
        public string ModelNoVerifiedBy { get; set; }
        public string ModelNo { get; set; }
        public string MOP { get; set; }

        public string OperatingPressure { get; set; }
        public string OperVoltage { get; set; }
        public int OrderNum { get; set; }
        public int OrderLine { get; set; }
       
        public string RawMOP { get; set; }
        public int ReleaseNum { get; set; }
        public string RuleHeadID { get; set; }

        public string SecondaryHtgInput { get; set; }
        public string SerialNo { get; set; }

        public string TestPressureHigh { get; set; }
        public string TestPressureLow { get; set; }

        public string UnitType { get; set; }
        public string UnitDesign { get; set; }

        public string Voltage { get; set; }

        public string WaterGlycol { get; set; }
        public string WhseCode { get; set; }

        #region Monitor Fields
        public string MaxOutletTemp { get; set; }

        public string PreHeatFLA { get; set; }

        public string Comp1StdRLA { get; set; }
        public string Comp1StdQty { get; set; }
        public string Comp1StdPhase { get; set; }
       
        public string Comp2StdRLA { get; set; }
        public string Comp2StdQty { get; set; }
        public string Comp2StdPhase { get; set; }

        public string Comp3StdRLA { get; set; }
        public string Comp3StdQty { get; set; }
        public string Comp3StdPhase { get; set; }

        public string Comp1HighRLA { get; set; }
        public string Comp1HighQty { get; set; }
        public string Comp1HighPhase { get; set; }

        public string Comp2HighRLA { get; set; }
        public string Comp2HighQty { get; set; }
        public string Comp2HighPhase { get; set; }

        public string ElectricHeatFLA { get; set; }

        public string DD_SupplyFanStdQty { get; set; }
        public string DD_SupplyFanStdPhase { get; set; }
        public string DD_SupplyFanStdFLA { get; set; }
        public string DD_SupplyFanStdHP { get; set; }

        public string DD_SupplyFanOvrQty { get; set; }
        public string DD_SupplyFanOvrPhase { get; set; }
        public string DD_SupplyFanOvrFLA { get; set; }
        public string DD_SupplyFanOvrHP { get; set; }

        public string BeltSupplyFanStdQty { get; set; }
        public string BeltSupplyFanStdPhase { get; set; }
        public string BeltSupplyFanStdFLA { get; set; }
        public string BeltSupplyFanStdHP { get; set; }

        public string BeltSupplyFanOvrQty { get; set; }
        public string BeltSupplyFanOvrPhase { get; set; }
        public string BeltSupplyFanOvrFLA { get; set; }
        public string BeltSupplyFanOvrHP { get; set; }

        public string CondenserFanStdQty { get; set; }
        public string CondenserFanStdPhase { get; set; }
        public string CondenserFanStdFLA { get; set; }
        public string CondenserFanStdHP { get; set; }

        public string CondenserFanHighQty { get; set; }
        public string CondenserFanHighPhase { get; set; }
        public string CondenserFanHighFLA { get; set; }
        public string CondenserFanHighHP { get; set; }

        public string ExhaustFanQty { get; set; }
        public string ExhaustFanPhase { get; set; }
        public string ExhaustFanFLA { get; set; }
        public string ExhaustFanHP { get; set; }

        public string SemcoWheelQty { get; set; }
        public string SemcoWheelPhase { get; set; }
        public string SemcoWheelFLA { get; set; }
        public string SemcoWheelHP { get; set; }

        public string AirXchangeWheelQty { get; set; }
        public string AirXchangeWheelPhase { get; set; }
        public string AirXchangeWheelFLA { get; set; }
        public string AirXchangeWheelHP { get; set; }

        #endregion

        #region MSP Properties
        public string AmbientTemp { get; set; }
        public string PartNum { get; set; }
        public string Refrigerant { get; set; }

        public string MSPDesignation { get; set; }
        public string MSPFanFilter { get; set; }

        public string MSPFanFLA { get; set; }
        public string MSPFanQty { get; set; }
        public string MSPFanAirVolume { get; set; }
        public string MSPFanExternalSP { get; set; }
        public string MSPFanTotalSP { get; set; }
        public string MSPFanVoltage { get; set; }
        public string MSPFanDraw { get; set; }
        public string MSPFanPower { get; set; }

        public string Dehumidifier_BTUH { get; set; }
        public string Dehumidifier_EWT { get; set; }
        public string Dehumidifier_LWT { get; set; }
        public string Dehumidifier_GPM { get; set; }
        public string Dehumidifier_PD { get; set; }
        public string Dehumidifier_Glycol { get; set; }
        public string Dehumidifier_EAT { get; set; }
        public string Dehumidifier_LAT { get; set; }
        public string Dehumidifier_SST { get; set; }
        public string Dehumidifier_CAPACITY { get; set; }

        public string PostCooling_BTUH { get; set; }
        public string PostCooling_EWT { get; set; }
        public string PostCooling_LWT { get; set; }
        public string PostCooling_GPM { get; set; }
        public string PostCooling_PD { get; set; }
        public string PostCooling_Glycol { get; set; }
        public string PostCooling_EAT { get; set; }
        public string PostCooling_LAT { get; set; }
        public string PostCooling_SST { get; set; }

        public string PostHeating_BTUH { get; set; }
        public string PostHeating_EWT { get; set; }
        public string PostHeating_LWT { get; set; }
        public string PostHeating_GPM { get; set; }
        public string PostHeating_PD { get; set; }
        public string PostHeating_Glycol { get; set; }
        public string PostHeating_EAT { get; set; }
        public string PostHeating_LAT { get; set; }

        #endregion

    }
}
