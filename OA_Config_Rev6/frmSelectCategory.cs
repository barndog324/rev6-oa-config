﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmSelectCategory : Form
    {
        public frmSelectCategory()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            string jobNumLst = this.lbJobNumListHidden.Text;
            string categoryList = "";

            if (cbCoil.Checked == true)
            {
                categoryList = "Coil,";
            }

            if (cbCompressor.Checked == true)
            {
                categoryList += "Compressor,DigitalScroll,DigitalScrollTandem,";
            }

            if (cbDampers.Checked == true)
            {
                categoryList += "Dampers,";
            }

            if (cbExtHeater.Checked == true)
            {
                categoryList += "External Heater,";
            }

            if (cbFurnaceHeater.Checked == true)
            {
                categoryList += "Furnace/Heater,";
            }

            if (cbMotor.Checked == true)
            {
                categoryList += "Motor,";
            }

            if (cbPreHeat.Checked == true)
            {
                categoryList += "Pre-Heat,";
            }

            if (cbWheels.Checked == true)
            {
                categoryList += "Wheels,";
            }

            if (categoryList.Length == 0)
            {
                MessageBox.Show("ERROR - No Categories have been selected. You must select at least 1 category.");
            }
            else
            {
                categoryList = categoryList.Substring(0, (categoryList.Length - 1));

                frmPrintBOM frmPrint = new frmPrintBOM();

                ReportDocument cryRpt = new ReportDocument();

                ParameterValues curPV = new ParameterValues();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobNumLst";
                pdv1.Value = jobNumLst;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                ParameterField pf2 = new ParameterField();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pf2.Name = "@CategoryList";
                pdv2.Value = categoryList;
                pf2.CurrentValues.Add(pdv2);

                paramFields.Add(pf2);

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG
                //cryRpt.Load(@"\\EPICOR905TEST\Apps\OAU\OAU.Configurator\crystalreports\CriticalCompPartsByJobNum.rpt");
                cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompPartsByJobNum.rpt");
#else
            cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompPartsByJobNum.rpt");
#endif
                //cryRpt.Load(@"F:\Projects\OAU_ScheduleExtract\OAU_ScheduleExtract\CriticalCompPartsByPartNum.rpt");

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();

                this.Close();
            }
        }
    }
}
