﻿namespace OA_Config_Rev6
{
    partial class frmPartAsmBOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lbPartNum = new System.Windows.Forms.Label();
            this.cbParentPartNum = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(994, 434);
            this.dataGridView1.TabIndex = 0;
            // 
            // lbPartNum
            // 
            this.lbPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPartNum.ForeColor = System.Drawing.Color.Black;
            this.lbPartNum.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbPartNum.Location = new System.Drawing.Point(281, 11);
            this.lbPartNum.Name = "lbPartNum";
            this.lbPartNum.Size = new System.Drawing.Size(156, 20);
            this.lbPartNum.TabIndex = 18;
            this.lbPartNum.Text = "Parent Part Num:";
            this.lbPartNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbParentPartNum
            // 
            this.cbParentPartNum.FormattingEnabled = true;
            this.cbParentPartNum.Location = new System.Drawing.Point(443, 10);
            this.cbParentPartNum.Name = "cbParentPartNum";
            this.cbParentPartNum.Size = new System.Drawing.Size(280, 21);
            this.cbParentPartNum.TabIndex = 19;
            // 
            // frmPartAsmBOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 488);
            this.Controls.Add(this.cbParentPartNum);
            this.Controls.Add(this.lbPartNum);
            this.Controls.Add(this.dataGridView1);
            this.Name = "frmPartAsmBOM";
            this.Text = "frmPartAsmBOM";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbPartNum;
        public System.Windows.Forms.ComboBox cbParentPartNum;
    }
}