﻿namespace OA_Config_Rev6
{
    partial class frmSubAssemblyStations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDisplayRpt = new System.Windows.Forms.Button();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.rbSubAsm1 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm2 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm4 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm3 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm6 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm5 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm12 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm11 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm10 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm9 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm8 = new System.Windows.Forms.RadioButton();
            this.rbSubAsm7 = new System.Windows.Forms.RadioButton();
            this.lbCustName = new System.Windows.Forms.Label();
            this.rbAllStations = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(528, 292);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 40);
            this.btnClose.TabIndex = 45;
            this.btnClose.Text = "Cancel";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDisplayRpt
            // 
            this.btnDisplayRpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplayRpt.ForeColor = System.Drawing.Color.Green;
            this.btnDisplayRpt.Location = new System.Drawing.Point(358, 292);
            this.btnDisplayRpt.Name = "btnDisplayRpt";
            this.btnDisplayRpt.Size = new System.Drawing.Size(110, 40);
            this.btnDisplayRpt.TabIndex = 44;
            this.btnDisplayRpt.Text = "Display";
            this.btnDisplayRpt.UseVisualStyleBackColor = true;
            this.btnDisplayRpt.Click += new System.EventHandler(this.btnDisplayRpt_Click);
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Blue;
            this.lbModelNo.Location = new System.Drawing.Point(13, 39);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(1002, 23);
            this.lbModelNo.TabIndex = 61;
            this.lbModelNo.Text = "ModelNo";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Blue;
            this.lbJobNum.Location = new System.Drawing.Point(506, 12);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(204, 23);
            this.lbJobNum.TabIndex = 62;
            this.lbJobNum.Text = "JobNum";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(392, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 23);
            this.label12.TabIndex = 63;
            this.label12.Text = "Job Number:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbSubAsm1
            // 
            this.rbSubAsm1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm1.ForeColor = System.Drawing.Color.Blue;
            this.rbSubAsm1.Location = new System.Drawing.Point(103, 133);
            this.rbSubAsm1.Name = "rbSubAsm1";
            this.rbSubAsm1.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm1.TabIndex = 64;
            this.rbSubAsm1.TabStop = true;
            this.rbSubAsm1.Text = "Sub Assembly 1 = Refrigeration Kits";
            this.rbSubAsm1.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm2
            // 
            this.rbSubAsm2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm2.ForeColor = System.Drawing.Color.Green;
            this.rbSubAsm2.Location = new System.Drawing.Point(103, 158);
            this.rbSubAsm2.Name = "rbSubAsm2";
            this.rbSubAsm2.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm2.TabIndex = 65;
            this.rbSubAsm2.TabStop = true;
            this.rbSubAsm2.Text = "Sub Assembly 2 = Aux Box Station 1 (Sheet Metal)";
            this.rbSubAsm2.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm4
            // 
            this.rbSubAsm4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm4.ForeColor = System.Drawing.Color.Green;
            this.rbSubAsm4.Location = new System.Drawing.Point(103, 208);
            this.rbSubAsm4.Name = "rbSubAsm4";
            this.rbSubAsm4.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm4.TabIndex = 67;
            this.rbSubAsm4.TabStop = true;
            this.rbSubAsm4.Text = "Sub Assembly 4 = Wiring Harness & Electrical Kits";
            this.rbSubAsm4.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm3
            // 
            this.rbSubAsm3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm3.ForeColor = System.Drawing.Color.Blue;
            this.rbSubAsm3.Location = new System.Drawing.Point(103, 183);
            this.rbSubAsm3.Name = "rbSubAsm3";
            this.rbSubAsm3.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm3.TabIndex = 66;
            this.rbSubAsm3.TabStop = true;
            this.rbSubAsm3.Text = "Sub Assembly 3 = Aux Box Station 2 (Damper, ERVs && Wiring";
            this.rbSubAsm3.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm6
            // 
            this.rbSubAsm6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm6.ForeColor = System.Drawing.Color.Green;
            this.rbSubAsm6.Location = new System.Drawing.Point(103, 258);
            this.rbSubAsm6.Name = "rbSubAsm6";
            this.rbSubAsm6.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm6.TabIndex = 69;
            this.rbSubAsm6.TabStop = true;
            this.rbSubAsm6.Text = "Sub Assembly 6 = Station 8 Door Assembly";
            this.rbSubAsm6.UseVisualStyleBackColor = true;
            this.rbSubAsm6.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rbSubAsm5
            // 
            this.rbSubAsm5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm5.ForeColor = System.Drawing.Color.Blue;
            this.rbSubAsm5.Location = new System.Drawing.Point(103, 233);
            this.rbSubAsm5.Name = "rbSubAsm5";
            this.rbSubAsm5.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm5.TabIndex = 68;
            this.rbSubAsm5.TabStop = true;
            this.rbSubAsm5.Text = "Sub Assembly 5 = Front Panels && Heater Assembly";
            this.rbSubAsm5.UseVisualStyleBackColor = true;
            this.rbSubAsm5.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // rbSubAsm12
            // 
            this.rbSubAsm12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm12.ForeColor = System.Drawing.Color.Green;
            this.rbSubAsm12.Location = new System.Drawing.Point(543, 258);
            this.rbSubAsm12.Name = "rbSubAsm12";
            this.rbSubAsm12.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm12.TabIndex = 75;
            this.rbSubAsm12.TabStop = true;
            this.rbSubAsm12.Text = "Sub Assembly 12 = Electrical Control Board Sub-Assembly";
            this.rbSubAsm12.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm11
            // 
            this.rbSubAsm11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm11.ForeColor = System.Drawing.Color.Blue;
            this.rbSubAsm11.Location = new System.Drawing.Point(543, 233);
            this.rbSubAsm11.Name = "rbSubAsm11";
            this.rbSubAsm11.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm11.TabIndex = 74;
            this.rbSubAsm11.TabStop = true;
            this.rbSubAsm11.Text = "Sub Assembly 11 = Condenser Fans";
            this.rbSubAsm11.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm10
            // 
            this.rbSubAsm10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm10.ForeColor = System.Drawing.Color.Green;
            this.rbSubAsm10.Location = new System.Drawing.Point(543, 208);
            this.rbSubAsm10.Name = "rbSubAsm10";
            this.rbSubAsm10.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm10.TabIndex = 73;
            this.rbSubAsm10.TabStop = true;
            this.rbSubAsm10.Text = "Sub Assembly 10 = Fan Motor Assembly";
            this.rbSubAsm10.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm9
            // 
            this.rbSubAsm9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm9.ForeColor = System.Drawing.Color.Blue;
            this.rbSubAsm9.Location = new System.Drawing.Point(543, 183);
            this.rbSubAsm9.Name = "rbSubAsm9";
            this.rbSubAsm9.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm9.TabIndex = 72;
            this.rbSubAsm9.TabStop = true;
            this.rbSubAsm9.Text = "Sub Assembly 9 = Breakers && Transformers";
            this.rbSubAsm9.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm8
            // 
            this.rbSubAsm8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm8.ForeColor = System.Drawing.Color.Green;
            this.rbSubAsm8.Location = new System.Drawing.Point(543, 158);
            this.rbSubAsm8.Name = "rbSubAsm8";
            this.rbSubAsm8.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm8.TabIndex = 71;
            this.rbSubAsm8.TabStop = true;
            this.rbSubAsm8.Text = "Sub Assembly 8 = Rosenburg Sub-Assembly";
            this.rbSubAsm8.UseVisualStyleBackColor = true;
            // 
            // rbSubAsm7
            // 
            this.rbSubAsm7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSubAsm7.ForeColor = System.Drawing.Color.Blue;
            this.rbSubAsm7.Location = new System.Drawing.Point(543, 133);
            this.rbSubAsm7.Name = "rbSubAsm7";
            this.rbSubAsm7.Size = new System.Drawing.Size(400, 20);
            this.rbSubAsm7.TabIndex = 70;
            this.rbSubAsm7.TabStop = true;
            this.rbSubAsm7.Text = "Sub Assembly 7 = Front Door Assembly";
            this.rbSubAsm7.UseVisualStyleBackColor = true;
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Blue;
            this.lbCustName.Location = new System.Drawing.Point(138, 66);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(739, 23);
            this.lbCustName.TabIndex = 76;
            this.lbCustName.Text = "Customer Name";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rbAllStations
            // 
            this.rbAllStations.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAllStations.ForeColor = System.Drawing.Color.Black;
            this.rbAllStations.Location = new System.Drawing.Point(455, 108);
            this.rbAllStations.Name = "rbAllStations";
            this.rbAllStations.Size = new System.Drawing.Size(108, 20);
            this.rbAllStations.TabIndex = 77;
            this.rbAllStations.TabStop = true;
            this.rbAllStations.Text = "All Stations";
            this.rbAllStations.UseVisualStyleBackColor = true;
            // 
            // frmSubAssemblyStations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 353);
            this.Controls.Add(this.rbAllStations);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.rbSubAsm12);
            this.Controls.Add(this.rbSubAsm11);
            this.Controls.Add(this.rbSubAsm10);
            this.Controls.Add(this.rbSubAsm9);
            this.Controls.Add(this.rbSubAsm8);
            this.Controls.Add(this.rbSubAsm7);
            this.Controls.Add(this.rbSubAsm6);
            this.Controls.Add(this.rbSubAsm5);
            this.Controls.Add(this.rbSubAsm4);
            this.Controls.Add(this.rbSubAsm3);
            this.Controls.Add(this.rbSubAsm2);
            this.Controls.Add(this.rbSubAsm1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lbJobNum);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDisplayRpt);
            this.Name = "frmSubAssemblyStations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sub Assembly Stations Report";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDisplayRpt;
        public System.Windows.Forms.Label lbModelNo;
        public System.Windows.Forms.Label lbJobNum;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rbSubAsm1;
        private System.Windows.Forms.RadioButton rbSubAsm2;
        private System.Windows.Forms.RadioButton rbSubAsm4;
        private System.Windows.Forms.RadioButton rbSubAsm3;
        private System.Windows.Forms.RadioButton rbSubAsm6;
        private System.Windows.Forms.RadioButton rbSubAsm5;
        private System.Windows.Forms.RadioButton rbSubAsm12;
        private System.Windows.Forms.RadioButton rbSubAsm11;
        private System.Windows.Forms.RadioButton rbSubAsm10;
        private System.Windows.Forms.RadioButton rbSubAsm9;
        private System.Windows.Forms.RadioButton rbSubAsm8;
        private System.Windows.Forms.RadioButton rbSubAsm7;
        public System.Windows.Forms.Label lbCustName;
        private System.Windows.Forms.RadioButton rbAllStations;
    }
}