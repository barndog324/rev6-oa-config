﻿namespace OA_Config_Rev6
{
    partial class frmPicklistStations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPickListJobs = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDisplayRpt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickListJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPickListJobs
            // 
            this.dgvPickListJobs.AllowUserToAddRows = false;
            this.dgvPickListJobs.AllowUserToDeleteRows = false;
            this.dgvPickListJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPickListJobs.Location = new System.Drawing.Point(12, 61);
            this.dgvPickListJobs.Name = "dgvPickListJobs";
            this.dgvPickListJobs.Size = new System.Drawing.Size(1280, 318);
            this.dgvPickListJobs.TabIndex = 10;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(1172, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 40);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDisplayRpt
            // 
            this.btnDisplayRpt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplayRpt.ForeColor = System.Drawing.Color.Blue;
            this.btnDisplayRpt.Location = new System.Drawing.Point(1037, 11);
            this.btnDisplayRpt.Name = "btnDisplayRpt";
            this.btnDisplayRpt.Size = new System.Drawing.Size(110, 40);
            this.btnDisplayRpt.TabIndex = 12;
            this.btnDisplayRpt.Text = "Display";
            this.btnDisplayRpt.UseVisualStyleBackColor = true;
            this.btnDisplayRpt.Click += new System.EventHandler(this.btnDisplayRpt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(497, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 31);
            this.label1.TabIndex = 14;
            this.label1.Text = "Picklist by Station";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmPicklistStations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 391);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDisplayRpt);
            this.Controls.Add(this.dgvPickListJobs);
            this.Name = "frmPicklistStations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPicklistStations";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickListJobs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dgvPickListJobs;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDisplayRpt;
        private System.Windows.Forms.Label label1;
    }
}