﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmSheetMetalBend : Form
    {
        MainBO objMain = new MainBO();
        public frmSheetMetalBend()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string jobNumLst = "";            
            string cabType = "";
            string modelNo = "";
            string dupJobsStr = "";
            string foldMethod = "";

            int lastPos = -1;

            bool rev6ModelNoFound = false;
            bool rev5ModelNoFound = false;
            bool multipleCabTypes = false;
            bool processJobs = true;
            bool errorFound = false;

            if (cbCabType.SelectedIndex == 0)
            {
                MessageBox.Show("ERROR - No Cabinet Type has been selected!");
            }
            else
            {              
                jobNumLst = txtJobNums.Text;

                if (jobNumLst != "")
                {
                    if (jobNumLst.Contains(",") == false)
                    {
                        MessageBox.Show("ERROR - All Job Numbers must be separated by commas!");
                        errorFound = true;
                    }

                    lastPos = jobNumLst.LastIndexOf(',') + 1;
                    if (lastPos != jobNumLst.Length)
                    {
                        MessageBox.Show("ERROR - Job List must end with a comma!");
                        errorFound = true;
                    }

                    if (cbCabType.SelectedIndex < 1)
                    {
                        MessageBox.Show("ERROR - Cabinet Type is a required field, please one from the drop down box!");
                        errorFound = true;
                    }

                    if (errorFound == false)
                    {
                        string[] jobNums = jobNumLst.Split(',');

                        foreach (string jobNum in jobNums)
                        {
                            if (jobNum != "")
                            {
                                DataTable dtJob = objMain.GetOA_JobHeadDataByJobNum(jobNum);
                                if (dtJob.Rows.Count > 0)
                                {
                                    DataRow drJob = dtJob.Rows[0];
                                    modelNo = drJob["PartDescription"].ToString();

                                    if (modelNo.Length == 39)
                                    {
                                        rev5ModelNoFound = true;
                                    }
                                    else if (modelNo.Length == 69)
                                    {
                                        rev6ModelNoFound = true;
                                    }

                                    if (modelNo.Substring(0, 3) != cbCabType.Text.Substring(0, 3))
                                    {
                                        multipleCabTypes = true;
                                    }
                                }
                                else
                                {
                                    errorFound = true;
                                    MessageBox.Show("ERROR - Job number: " + jobNum + " does Not exist. Please check the job number list you inputted.");
                                    break;
                                }
                            }
                        }

                        if (errorFound == false)
                        {
                            if (rev5ModelNoFound == true && rev6ModelNoFound == true)
                            {
                                DialogResult result1 = MessageBox.Show("WARNING - The selected jobs contain both Rev 5 & Rev 6 ModelNo. " +
                                                                       "Do you wish to continue with this report 'Yes' or 'No'",
                                                                       "WARNING WARNING WARNING",
                                                                       MessageBoxButtons.YesNo,
                                                                       MessageBoxIcon.Exclamation);

                                if (result1 == DialogResult.No)
                                {
                                    processJobs = false;
                                }
                            }

                            if (multipleCabTypes == true)
                            {
                                DialogResult result1 = MessageBox.Show("WARNING - The selected jobs contain different cabinet types. " +
                                                                       "Do you wish to continue with this report 'Yes' or 'No'",
                                                                       "WARNING WARNING WARNING",
                                                                       MessageBoxButtons.YesNo,
                                                                       MessageBoxIcon.Exclamation);

                                if (result1 == DialogResult.No)
                                {
                                    processJobs = false;
                                }
                            }


                            if (processJobs == true)
                            {
                                if (radBtnLaser.Checked == true)
                                {
                                    foldMethod = "LASER";
                                }
                                else if (radBtnS4.Checked == true)
                                {
                                    foldMethod = "S4";
                                }
                                else if (radBtnTurret.Checked == true)
                                {
                                    foldMethod = "TURRET";
                                }

                                cabType = cbCabType.Text + " of " + dtpRunDate.Value.ToShortDateString();

                                frmPrintBOM frmPrint = new frmPrintBOM();

                                ReportDocument cryRpt = new ReportDocument();

                                ParameterValues curPV = new ParameterValues();

                                ParameterFields paramFields = new ParameterFields();

                                ParameterField pf1 = new ParameterField();
                                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                                pf1.Name = "@JobList";
                                pdv1.Value = jobNumLst;
                                pf1.CurrentValues.Add(pdv1);

                                paramFields.Add(pf1);

                                ParameterField pf2 = new ParameterField();
                                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                                pf2.Name = "@CabinetType";
                                pdv2.Value = cabType;
                                pf2.CurrentValues.Add(pdv2);

                                paramFields.Add(pf2);

                                ParameterField pf3 = new ParameterField();
                                ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
                                pf3.Name = "@FoldMethod";
                                pdv3.Value = foldMethod;
                                pf3.CurrentValues.Add(pdv3);

                                paramFields.Add(pf3);

                                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;


#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\OAU_SheetMetalBendReport.rpt";             
#else
                            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\OAU_SheetMetalBendReport.rpt";
#endif

                            cryRpt.Load(objMain.ReportString);
                                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                                frmPrint.Show();
                                frmPrint.crystalReportViewer1.Refresh();

                                if (chkInsertSMR_Table.Checked == true)
                                {
                                    long releaseID = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmss"));
                                    string createBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                                    string[] jobNumList = txtJobNums.Text.Split(',');
                                    DateTime releaseDate = dtpRunDate.Value;
                                    DateTime createDate = DateTime.Now;


                                    foreach (string jobStr in jobNumList)
                                    {
                                        DataTable dtSMR = objMain.GetSheetMetalReleaseByJobNum(jobStr);

                                        if (dtSMR.Rows.Count > 0)
                                        {
                                            dupJobsStr += jobStr + ",";
                                        }
                                    }

                                    if (dupJobsStr.Length > 0)
                                    {
                                        MessageBox.Show("ERROR -- The following Jobs have already been accounted for in an another Release: " + dupJobsStr);
                                    }
                                    else
                                    {
                                        try
                                        {
                                            objMain.InsertSheetMetalRelease(releaseID, jobNumLst, cbCabType.Text, releaseDate, createBy, createDate, createBy, createDate);
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show("ERROR - When trying to Insert into R6_OS_SheetMetalRelease table - " + ex);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("ERROR - No jobs have been inputted!");
                }
            }
        }
    }
}
