﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmSheetMetalRelease : Form
    {
        MainBO objMain = new MainBO();

        public frmSheetMetalRelease()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       

        private void dgvSheetMetalRelease_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvSheetMetalRelease.CurrentRow.Index;

            string jobNumList = dgvSheetMetalRelease.Rows[rowIdx].Cells["JobNumList"].Value.ToString();
            string releaseIdStr = dgvSheetMetalRelease.Rows[rowIdx].Cells["ReleaseID"].Value.ToString();

            DataTable dtMetal = objMain.GetSheetMetalReleaseMetal(jobNumList);

            frmSheetMetalUsage frmSMU = new frmSheetMetalUsage();

            frmSMU.lbJobNumList.Text = jobNumList;
            frmSMU.lbReleaseID.Text = releaseIdStr;

            frmSMU.dgvMetalUsage.DataSource = dtMetal;

            frmSMU.dgvMetalUsage.Columns["PartNum"].Width = 175;
            frmSMU.dgvMetalUsage.Columns["PartNum"].HeaderText = "PartNum";
            frmSMU.dgvMetalUsage.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSMU.dgvMetalUsage.Columns["PartNum"].DisplayIndex = 0;
            frmSMU.dgvMetalUsage.Columns["Description"].Width = 425;
            frmSMU.dgvMetalUsage.Columns["Description"].HeaderText = "Description";
            frmSMU.dgvMetalUsage.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmSMU.dgvMetalUsage.Columns["Description"].DisplayIndex = 1;
            frmSMU.dgvMetalUsage.Columns["EstimatedQtyUsed"].Width = 85;
            frmSMU.dgvMetalUsage.Columns["EstimatedQtyUsed"].HeaderText = "Estimated\nSheets Needed";
            frmSMU.dgvMetalUsage.Columns["EstimatedQtyUsed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmSMU.dgvMetalUsage.Columns["EstimatedQtyUsed"].DisplayIndex = 2;
            frmSMU.dgvMetalUsage.Columns["ActualQtyUsed"].Width = 85;
            frmSMU.dgvMetalUsage.Columns["ActualQtyUsed"].HeaderText = "Actual\nSheets Used";
            frmSMU.dgvMetalUsage.Columns["ActualQtyUsed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmSMU.dgvMetalUsage.Columns["ActualQtyUsed"].DisplayIndex = 3;
            frmSMU.dgvMetalUsage.Columns["TransType"].Visible = false;
  
            frmSMU.lbMainMsg.Text = "Enter actaul sheets used per part number in Actual Sheets Used column and press 'Submit'";
            
            frmSMU.ShowDialog();
            RedisplaySheetMetalReleases();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            RedisplaySheetMetalReleases();
        }

        private void RedisplaySheetMetalReleases()
        {
            DataTable dtSMR = objMain.GetSheetMetalReleases();
            dgvSheetMetalRelease.DataSource = dtSMR;

            dgvSheetMetalRelease.Columns["ReleaseID"].Width = 125;
            dgvSheetMetalRelease.Columns["ReleaseID"].HeaderText = "ReleaseID";
            dgvSheetMetalRelease.Columns["ReleaseID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSheetMetalRelease.Columns["ReleaseID"].DisplayIndex = 0;
            dgvSheetMetalRelease.Columns["CabinetType"].Width = 75;
            dgvSheetMetalRelease.Columns["CabinetType"].HeaderText = "CabinetType";
            dgvSheetMetalRelease.Columns["CabinetType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSheetMetalRelease.Columns["CabinetType"].DisplayIndex = 1;
            dgvSheetMetalRelease.Columns["JobNumList"].Width = 600;
            dgvSheetMetalRelease.Columns["JobNumList"].HeaderText = "JobNumList";
            dgvSheetMetalRelease.Columns["JobNumList"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSheetMetalRelease.Columns["JobNumList"].DisplayIndex = 2;
            dgvSheetMetalRelease.Columns["CreateBy"].Width = 80;
            dgvSheetMetalRelease.Columns["CreateBy"].HeaderText = "CreateBy";
            dgvSheetMetalRelease.Columns["CreateBy"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSheetMetalRelease.Columns["CreateBy"].DisplayIndex = 3;
            dgvSheetMetalRelease.Columns["CreateDate"].Width = 125;
            dgvSheetMetalRelease.Columns["CreateDate"].HeaderText = "CreateDate";
            dgvSheetMetalRelease.Columns["CreateDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSheetMetalRelease.Columns["CreateDate"].DisplayIndex = 4;

            lbMainMsg.Text = "Double click on the desired release to update Metal usage";

           this.Refresh();
        }
    }
}
