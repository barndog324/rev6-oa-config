﻿using Epicor.Mfg.Core;
using Epicor.Mfg.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    class ScheduleJob
    {
        private Session epiSession;
        private string epicorServer;
        private string epicorSqlConnectionString;

        LineTransferBO objTran = new LineTransferBO();
        MainBO objMain = new MainBO();

        // Init
        public ScheduleJob(string epicorServer, string epicorSqlConnectionString)
        {
            this.epicorServer = epicorServer;
            this.epicorSqlConnectionString = epicorSqlConnectionString;
        }

        public string ScheduleJobByDateAndTime(string jobNum, string startDateStr, string startHourStr, string endDateStr, string endHourStr, string prodHrsStr)
        {
            string reqDueDateStr = "";
            string prodQtyStr = "";
            //string endDateStr = "";
            string dueDateStr = "1/1/1000";
            string dueHourStr = "";
            //string startDateStr = "1/1/1000";
            //string startHourStr = "";
            string oprStartDate = "";
            string oprStartHour = "";
            string oprStartMin = "";
            string oprEndDate = "";
            string oprEndHour = "";
            string oprEndMin = "";
            string vMessage = "";
            string plantLine = "";
            string resourceGrpID = "";
            string resourceID = "";
            string opDtlDesc = "";
            string runOutWarning = "";
            string c_WarnLogTxt = "";
            string errorMsg = "Job# " + jobNum + " scheduled successfully";

            int asmSeq = 0;
            int oprSeq = 0;
            int rowIdx = 0;
            int oprEndTimeInt = 0;
            int oprStartTimeInt = 0;

            DateTime reqDueDate = DateTime.Now; // These date variable are set to today's date only for initialization purposes.
            //DateTime startDate = DateTime.Now;
            DateTime dueDate = DateTime.Now;
            //DateTime startHour = DateTime.Now;

            decimal prodQtyDec = 0;         

            double totalPrdHrs = 0;

            bool jobEngineered = false;
            bool jobReleased = false;
            bool opChangeDescription = false;
            bool l_finished = false;

            this.epiSession = new Session("epiagent", "epiagent", "AppServerDC://" + this.epicorServer + ":9401", Session.LicenseType.Default);

            JobEntry je = new JobEntry(this.epiSession.ConnectionPool);
            ScheduleEngine se = new ScheduleEngine(this.epiSession.ConnectionPool);

            try
            {
                JobEntryDataSet jeds = je.GetByID(jobNum);
                DataRow[] jhdr = jeds.JobHead.Select();

                if (jhdr[0]["ProdQty"] != null)
                {
                    prodQtyStr = jhdr[0]["ProdQty"].ToString();
                    prodQtyDec = decimal.Parse(prodQtyStr);
                }

                if (jhdr[0]["ReqDueDate"] != null)
                {
                    reqDueDateStr = jhdr[0]["ReqDueDate"].ToString();
                    reqDueDate = Convert.ToDateTime(reqDueDateStr);
                }

                if (jhdr[0]["JobEngineered"].ToString() == "True")
                {
                    jobEngineered = true;
                }

                if (jhdr[0]["JobReleased"].ToString() == "True")
                {
                    jobReleased = true;
                }

                if (jobEngineered) // If JobEngineered = True then un-engineer the job.
                {
                    try
                    {
                        je.ValidateJobNum(jobNum);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "ERROR - ValidateJobNum() - Job# " + jobNum + " " + ex;                          
                    }

                    try
                    {                        
                        totalPrdHrs = double.Parse(prodHrsStr);

                            try
                            {
                                                                                                                           
                                ScheduleEngineDataSet seds = new ScheduleEngineDataSet();
                                CalcOprStartAndEndInteger(startDateStr, startHourStr, out oprStartDate, out oprStartHour, out oprStartTimeInt,
                                                          out oprEndDate, out oprEndHour, out oprEndTimeInt, (decimal)totalPrdHrs);


                                // OprSeq & OpDtlSeq are both set to 0 to allo Epicor to handle the sceduling of each individual operation & sub-assembly    
                                DataRow sedr = seds.ScheduleEngine.NewRow();
                                sedr["Company"] = "KCC";
                                sedr["JobNum"] = jobNum;
                                sedr["AssemblySeq"] = 0;                                
                                sedr["OprSeq"] = 0;
                                sedr["OpDtlSeq"] = 0; 
                                sedr["StartDate"] = oprStartDate;     // Using forward scheduling input the actual start date of the job                                       
                                sedr["StartTime"] = oprStartTimeInt;  // Input the start time as seconds (time * 3600)
                                sedr["EndDate"] = oprEndDate;         // Input the actual end date of the job 
                                sedr["EndTime"] = oprEndTimeInt;      // Input the end time as seconds (time * 3600)
                                sedr["WhatIf"] = 0;
                                sedr["Finite"] = 0;
                                sedr["SchedTypeCode"] = "JA";         //  JA = Job - All Operations
                                sedr["ScheduleDirection"] = "F";      // Forward scheduling.
                                seds.ScheduleEngine.Rows.Add(sedr);
                                                                                      
                                se.MoveJobItem(seds, out l_finished, out c_WarnLogTxt);                               
                            }
                            catch (Exception ex)
                            {
                                errorMsg = "ERROR - (MoveJobItem()) - Job# " + jobNum + " " + ex;                               
                            }

                            try
                            {
                                jeds = je.GetByID(jobNum);
                            }
                            catch (Exception ex)
                            {
                                errorMsg = "ERROR - GetByID() - Job# " + jobNum + " " + ex;                                
                            }
                        //}
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "ERROR - GetOA_JobOperations() - Job# " + jobNum + " " + ex;                        
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = "ERROR - GetByID - Job# " + jobNum + " " + ex;                
            }

            return errorMsg;
        }

        private double CalcPrdHours(DataTable dtJobOprs)
        {
            double totalProdHrs = 0;

            foreach (DataRow row in dtJobOprs.Rows)
            {
                if (row["AssemblySeq"].ToString() == "0")
                {
                    totalProdHrs += double.Parse(row["ProdStandard"].ToString());
                }
                //else
                //{
                //    totalProdHrs += 8;
                //}
            }

            return totalProdHrs;
        }

        private void CalcOprStartAndEndInteger(string prvOprEndDate, string prvOprEndHour, out string oprStartDate, out string oprStartHour,
                                               out int oprStartTimeInt, out string oprEndDate, out string oprEndHour, out int oprEndTimeInt, decimal prodHrs)
        {
            string startHr = "";
            string startMin = "";
            string endHr = "";
            string endMin = "";

            int chPos = -1;
            int decPos = -1;
            int iProdDays = 0;

            decimal startTime = 0;

            double oprEndTime = 0;
            double endTimeDbl = 0;

            DateTime tempDate = DateTime.Now;

            oprStartDate = prvOprEndDate;
            oprStartHour = prvOprEndHour;
            oprEndDate = prvOprEndDate;  // Starting point before adding ProdHrs
            oprEndHour = prvOprEndHour;  // Starting point before adding ProdHrs

            chPos = oprStartHour.IndexOf(":");

            if (chPos > 0)
            {
                startHr = oprStartHour.Substring(0, chPos);
                startMin = oprStartHour.Substring((chPos + 1), 2);
                startTime = decimal.Parse(startHr) + (decimal.Parse(startMin) / 60);
                oprStartTimeInt = int.Parse(startHr) * 3600 + int.Parse(startMin) * 60;
            }
            else
            {
                oprStartTimeInt = 5 * 3600; // default to 5am.
                startTime = 5;
            }

            if (prodHrs > 8)
            {
                iProdDays = (int)prodHrs / 8;
                tempDate = DateTime.Parse(oprStartDate).AddDays(iProdDays);
                prodHrs = prodHrs - (iProdDays * 8);
            }
            else
            {
                tempDate = DateTime.Parse(oprEndDate);
            }

            oprEndTime = (double)(startTime + prodHrs);

            if (oprEndTime > 15) // Shift end hour is 3pm or 15:00
            {
                endTimeDbl = (double)(5 + (oprEndTime - 14));  // Daily start hour is 5am; Add the hour greater than 14 to the start time of 5.                                       

                if (tempDate.AddDays(1).DayOfWeek == DayOfWeek.Saturday)
                {
                    oprEndDate = tempDate.AddDays(3).ToShortDateString();
                }
                else
                {
                    oprEndDate = tempDate.AddDays(1).ToShortDateString();
                }
            }
            else
            {
                endTimeDbl = oprEndTime;
                oprEndDate = tempDate.ToShortDateString();
            }
            
            oprEndHour = endTimeDbl.ToString("N2");

            TimeSpan ts = TimeSpan.FromHours(double.Parse(oprEndHour));
            oprEndHour = ts.ToString("hh\\:mm");
            
            oprEndTime = endTimeDbl * 3600;

            oprEndTimeInt = (int)oprEndTime * 1;
        }        
    }
}
