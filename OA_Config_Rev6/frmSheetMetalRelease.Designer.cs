﻿namespace OA_Config_Rev6
{
    partial class frmSheetMetalRelease
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.dgvSheetMetalRelease = new System.Windows.Forms.DataGridView();
            this.lbMainMsg = new System.Windows.Forms.Label();
            this.lbMainTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSheetMetalRelease)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(995, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.Green;
            this.btnReset.Location = new System.Drawing.Point(901, 12);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // dgvSheetMetalRelease
            // 
            this.dgvSheetMetalRelease.AllowUserToAddRows = false;
            this.dgvSheetMetalRelease.AllowUserToDeleteRows = false;
            this.dgvSheetMetalRelease.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSheetMetalRelease.Location = new System.Drawing.Point(13, 53);
            this.dgvSheetMetalRelease.MultiSelect = false;
            this.dgvSheetMetalRelease.Name = "dgvSheetMetalRelease";
            this.dgvSheetMetalRelease.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSheetMetalRelease.Size = new System.Drawing.Size(1057, 503);
            this.dgvSheetMetalRelease.TabIndex = 8;
            this.dgvSheetMetalRelease.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSheetMetalRelease_CellMouseDoubleClick);
            // 
            // lbMainMsg
            // 
            this.lbMainMsg.BackColor = System.Drawing.Color.LightSalmon;
            this.lbMainMsg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainMsg.Location = new System.Drawing.Point(12, 561);
            this.lbMainMsg.Name = "lbMainMsg";
            this.lbMainMsg.Size = new System.Drawing.Size(1058, 20);
            this.lbMainMsg.TabIndex = 25;
            this.lbMainMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMainTitle
            // 
            this.lbMainTitle.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitle.Location = new System.Drawing.Point(303, 5);
            this.lbMainTitle.Name = "lbMainTitle";
            this.lbMainTitle.Size = new System.Drawing.Size(417, 39);
            this.lbMainTitle.TabIndex = 26;
            this.lbMainTitle.Text = "Sheet Metal Releases";
            this.lbMainTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmSheetMetalRelease
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 585);
            this.Controls.Add(this.lbMainTitle);
            this.Controls.Add(this.lbMainMsg);
            this.Controls.Add(this.dgvSheetMetalRelease);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnReset);
            this.Name = "frmSheetMetalRelease";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SheetMetalRelease";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSheetMetalRelease)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnReset;
        public System.Windows.Forms.DataGridView dgvSheetMetalRelease;
        public System.Windows.Forms.Label lbMainTitle;
        public System.Windows.Forms.Label lbMainMsg;
    }
}