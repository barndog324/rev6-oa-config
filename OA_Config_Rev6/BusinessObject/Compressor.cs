﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_Config_Rev6
{
    public class Compressor
    {
        public string CircuitPos { get; set; }

        public string ImmedParent { get; set; }  

        public string LRA { get; set; }

        public string PartNum { get; set; }
        public string ParentPartNum { get; set; }

        public string Qty { get; set; }

        public string RLA { get; set; }
        public string RuleBatchID { get; set; }
        public string RuleHeadID { get; set; }

        public string Type { get; set; }

        public Compressor(string circuitPos, string partNum, string parentPartNum, string immedParent, string qty, 
                          string rla, string lra, string type, string ruleHeadID, string ruleBatchID)
        {
            this.CircuitPos = circuitPos;
            this.PartNum = partNum;
            this.ParentPartNum = parentPartNum;
            this.ImmedParent = immedParent;
            this.LRA = lra;
            this.Qty = qty;
            this.RLA = rla;
            this.Type = type;
            this.RuleHeadID = ruleHeadID;
            this.RuleBatchID = ruleBatchID;           
        }
    }
}
