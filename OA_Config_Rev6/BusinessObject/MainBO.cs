﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Configuration;

namespace OA_Config_Rev6
{
    public class MainBO : MainDTO
    {
        #region Main SQL 
        public DataTable GetOA_ConfigMainList(string jobNum, int jobStatus, string unitType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {                    
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOA_ConfigMainList", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNumSearchStr", jobNum);
                        cmd.Parameters.AddWithValue("@JobStatus", jobStatus);
                        cmd.Parameters.AddWithValue("@UnitType", unitType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOA_JobHeadDataByJobNum(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOA_JobHeadDataByJobNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetTandemCompElecData(string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetTandemCompElecData", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public bool GetProdCalDay(DateTime workDate)
        {
            bool dateFound = false;
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetProdCalDay", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WorkDate", workDate);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {
                                    dateFound = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dateFound;
        }

        public DataTable GetPartDetail(string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartDetail", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartBOM(string partNum, string revisionNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartBOM", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@RevisionNum", revisionNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetJobNumsByPartSearch(string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetJobNumsByPartSearch", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        
        public DataTable GetOrderDetail(string orderNum, string orderLine)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOrderDetail", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OrderNum", Int32.Parse(orderNum));
                        cmd.Parameters.AddWithValue("@OrderLine", Int32.Parse(orderLine));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetLaborDetailDataByJobNum(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetLaborDetailDataByJobNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


        public DataTable GetOA_ScheduleData(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOA_ScheduleData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetRefrigerationComponentData(string cabinetSize, string coolingCap, string unitType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetRefrigerationComponentData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CabinetSize", cabinetSize);
                        cmd.Parameters.AddWithValue("@CoolingCapacity", coolingCap);
                        cmd.Parameters.AddWithValue("@UnitType", unitType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartRulesByRuleHeadID_AndRuleBatchID(string partNum, string ruleBatchID, string unitTypeStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                if (unitTypeStr == "REV6")
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesByRuleHeadID_AndRuleBatchID", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PartNum", partNum);
                            cmd.Parameters.AddWithValue("@RuleBatchID", ruleBatchID);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                else if (unitTypeStr == "VKG")
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetPartRulesByPartNumAndRuleBatchID", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PartNum", partNum);
                            cmd.Parameters.AddWithValue("@RuleBatchID", ruleBatchID);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartRulesByRuleHeadID_AndRuleBatchID", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PartNum", partNum);
                            cmd.Parameters.AddWithValue("@RuleBatchID", ruleBatchID);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetLineSize(string digit3, string digit4, string coolingCap, string digit11)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetLineSizeList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Digit3", digit3);
                        cmd.Parameters.AddWithValue("@Digit4", digit4);
                        cmd.Parameters.AddWithValue("@CoolingCap", coolingCap);
                        cmd.Parameters.AddWithValue("@Digit11", digit11);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MonitorGetOrderDetails(int orderNum, int orderLine)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetOrderDetails", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OrderNum", orderNum);
                        cmd.Parameters.AddWithValue("@OrderLine", orderLine);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MonitorGetModelNoDigitDesc(int digitNo, string digitVal, string heatType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetModelNoDigitDesc", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNo", digitNo);
                        cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartOprAndOpDtlData(string partNumStr, string revNumStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartOprAndOpDtlData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNumStr);
                        cmd.Parameters.AddWithValue("@RevNum", revNumStr);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetOA_JobOperations(string jobNumStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOA_JobOperations", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetJobOpDtlByJobNumAsmSeqOprSeq(string jobNumStr, int assemblySeq, int oprSeq)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetJobOpDtlByJobNumAsmSeqOprSeq", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        cmd.Parameters.AddWithValue("@AssemblySeq", assemblySeq);
                        cmd.Parameters.AddWithValue("@OprSeq", oprSeq);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetOA_JobDataByJobNum(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOA_JobDataByJobNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetTandemCompAssemblyMtls(string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetTandemCompAssemblyMtls", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        #endregion

        #region OA_Schedule Sql
        public void UpdateOA_SchedulePartsAdded(string jobNumStr, DateTime partsAddedDate, string partsAddedByStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateOA_SchedulePartsAdded", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", jobNumStr);
                        command.Parameters.AddWithValue("@PartsAddedDate", partsAddedDate);
                        command.Parameters.AddWithValue("@PartsAddedBy", partsAddedByStr);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOA_SchedulePartsOrdered(string jobNumStr, int partsOrdered, DateTime partsOrderedDate, string orderedByStr, DateTime partsOrderRemovedDate, string partsOrderRemovedByStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateOA_SchedulePartsOrdered", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNumList", jobNumStr);
                        command.Parameters.AddWithValue("@PartsOrdered", partsOrdered);
                        command.Parameters.AddWithValue("@PartsOrderedDate", partsOrderedDate);
                        command.Parameters.AddWithValue("@OrderedBy", orderedByStr);
                        command.Parameters.AddWithValue("@PartsOrderRemovedDate", partsOrderRemovedDate);
                        command.Parameters.AddWithValue("@PartsOrderRemovedBy", partsOrderRemovedByStr);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetOA_SchedulePartsOrdered(string jobNumStr)
        {
            string partsOrdered = "False";
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOA_SchedulePartsOrdered", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                    DataRow dr = dt.Rows[0];
                                    partsOrdered = dr["PartsOrdered"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return partsOrdered;
        }

        public DataTable GetOAU_PartListByJobNum(string jobNumStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_PartListByJobNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        #endregion

        #region OAU_BOMs Sql
        public DataTable GetOAU_BOMsData(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOAU_BOMsData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertOAU_BOMsData(string jobNum, string createdBy)
        {            
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertOAU_BOMsData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                        connection.Open();
                        cmd.ExecuteNonQuery();                       
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
       

        public void UpdateOAU_BOMsData(string jobNum, string lastModBy)
        {           
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_UpdateOAU_BOMsData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);                                               
                        cmd.Parameters.AddWithValue("@LastModBy", lastModBy);
                        connection.Open();
                        cmd.ExecuteNonQuery();        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public void DeleteOAU_BOMsData(string jobNum, string deletedBy)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_DeleteOAU_BOMsData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);                       
                        cmd.Parameters.AddWithValue("@DeletedBy", deletedBy);
                        connection.Open();
                        cmd.ExecuteNonQuery();   
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
        #endregion

        #region ModelNo Sql
        public DataTable GetModelNumDesc(int digit, string heatType, string digitVal, string unitTypeStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                if (unitTypeStr == "OAU")
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetModelNumDesc", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@DigitNum", digit);
                            cmd.Parameters.AddWithValue("@HeatType", heatType);
                            cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetModelNumDesc", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@DigitNum", digit);
                            cmd.Parameters.AddWithValue("@HeatType", "NA");
                            cmd.Parameters.AddWithValue("@DigitVal", digitVal);

                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MSP_GetModelNumDesc(int digit, string heatType, string digitVal)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetModelNumDesc", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", digit);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetRev5ModelNoDigitDesc(int digitNo, string digitVal, string heatType, string OAUTypeCode)
        {
            DataTable dt = new DataTable();
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetRev5ModelNoDigitDesc", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNo", digitNo);
                        cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        cmd.Parameters.AddWithValue("@OAUTypeCode", OAUTypeCode);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOAU_ModelNoValues(int digit, string heatType, string oauTypeCode)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_ModelNoValues", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", digit);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        cmd.Parameters.AddWithValue("@OAUTypeCode", oauTypeCode);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MonitorGetModelNoValues(int digit, string heatType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetModelNoValues", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", digit);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MSP_GetModelNoValues(int digit, string heatType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetModelNoValues", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", digit);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region BOM Data Sql
        public DataTable GetTandemAsmFromJobAsmbl(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetTandemAsmFromJobAsmbl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartListByModelNo(string modelNo, string revType)
        {
            DataTable dt = new DataTable();

            if (revType == "VKG")
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetPartListByModelNo_StatLoc", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 120;
                            cmd.Parameters.AddWithValue("@ModelNo", modelNo);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else if (revType == "REV6")
            {
                try
                {                    
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;
                    
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartListByModelNo_StatLoc", connection))
                        //using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartListByModelNo_v2", connection)) //All Parts on the BOM stored proc
                        //using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartListByModelNo", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 120;
                            cmd.Parameters.AddWithValue("@ModelNo", modelNo);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else 
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        //using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartListByModelNo", connection))
                        using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartListByModelNo_StatLoc", connection)) //All Parts on the BOM stored proc
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 120;
                            cmd.Parameters.AddWithValue("@ModelNo", modelNo);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return dt;
        }

        public DataTable MonitorGetPartListByModelNo(string modelNo)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetPartListByModelNo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ModelNo", modelNo);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MSP_GetPartListByModelNo(string modelNo)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetPartListByModelNo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ModelNo", modelNo);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetExistingBOMPartListByJobNum(string jobNum, string revType)
        {
            DataTable dt = new DataTable();

            if (revType == "VKG")
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetExistingBOMPartListByJobNum", connection))
                        {
                            cmd.CommandTimeout = 120;
                            cmd.CommandType = CommandType.StoredProcedure;                           
                            cmd.Parameters.AddWithValue("@JobNum", jobNum);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else if (revType == "REV6")
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        //using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetExistingBOMPartListByJobNum", connection))
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetExistingBOMPartListByJobNum_Statloc", connection))
                        {
                            cmd.CommandTimeout = 120;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 60;
                            cmd.Parameters.AddWithValue("@JobNum", jobNum);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else if (revType == "MON")
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {                       
                        using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetExistingBOMPartListByJobNum", connection))
                        {
                            cmd.CommandTimeout = 120;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 60;
                            cmd.Parameters.AddWithValue("@JobNum", jobNum);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        //using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetExistingBOMPartListByJobNum", connection))
                        using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetExistingBOMPartListByJobNum_StatLoc", connection))
                        {
                            cmd.CommandTimeout = 120;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@JobNum", jobNum);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return dt;
        }

        public DataTable GetRev5ExistingBOMPartListByJobNum(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetRev5ExistingBOMPartListByJobNum", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCompressorDataDtl(string partNum, int voltage)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompressorDataDtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@Voltage", voltage);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCompDetailData(string partNumStr, int voltageInt, int phaseInt)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetCompressorDetailData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNumStr);
                        cmd.Parameters.AddWithValue("@Voltage", voltageInt);
                        cmd.Parameters.AddWithValue("@Phase", phaseInt);    
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCompressorChargeData(string unitModel, string coolingCapacity, string digit11, string digit13, string digit14)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompressorChargeData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UnitModel", unitModel);
                        cmd.Parameters.AddWithValue("@CoolingCap", coolingCapacity);
                        cmd.Parameters.AddWithValue("@Digit11", digit11);
                        cmd.Parameters.AddWithValue("@Digit13", digit13);
                        cmd.Parameters.AddWithValue("@Digit14", digit14);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetFurnaceDataDTL(string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetFurnaceDataDTL", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetMotorDataDTL(int voltage, decimal hertz, int phase, string revNum, string partNum)
        {
            DataTable dt = new DataTable();

            if (revNum == "REV6")
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetMotorDataDTL", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PartNum", partNum);
                            cmd.Parameters.AddWithValue("@Voltage", voltage);
                            cmd.Parameters.AddWithValue("@Hertz", hertz);
                            cmd.Parameters.AddWithValue("@Phase", phase);                           
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetMotorDataDTL", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PartNum", partNum);
                            cmd.Parameters.AddWithValue("@Voltage", voltage);
                            cmd.Parameters.AddWithValue("@Hertz", hertz);
                            cmd.Parameters.AddWithValue("@Phase", phase);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return dt;
        }

         public DataTable MonitorGetCompAndMotorData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetCompAndMotorData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Digit1", Digit1);
                        cmd.Parameters.AddWithValue("@Digit2", Digit2);
                        cmd.Parameters.AddWithValue("@Digit456", Digit456);
                        cmd.Parameters.AddWithValue("@Digit7", Digit7);
                        cmd.Parameters.AddWithValue("@Digit8", Digit8);
                        cmd.Parameters.AddWithValue("@Digit10", Digit10);
                        cmd.Parameters.AddWithValue("@Digit22", Digit22);
                        cmd.Parameters.AddWithValue("@Digit32", Digit32);
                        cmd.Parameters.AddWithValue("@Digit33", Digit33);

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartRevisionNum(string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRevisionNum", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPanelAsmBOM(string partNum, string revisionNum, int subAsmWithPartNums)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPanelAsmBOM", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@RevisionNum", revisionNum);
                        cmd.Parameters.AddWithValue("@SubAsmWithPartNums", subAsmWithPartNums);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartMtlBOM(string partNum, string revisionNum, string parentPartNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartMtlBOM", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@RevisionNum", revisionNum);
                        cmd.Parameters.AddWithValue("@ParentPartNum", parentPartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOA_JobAssemblys(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOA_JobAssemblys", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }       

        public DataTable GetPartOpDtl(string partNum, string revisionNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartOpDtl", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@RevisionNum", revisionNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartOperationData(string partNum, string revisionNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetPartOperations", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@RevisionNum", revisionNum);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetResourceGroupData(string resourceGrpId)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetResourceGroupData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ResourceGrpID", resourceGrpId);                      
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetAssemblyOperationData(string unitType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetAssemblyOperations", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UnitType", unitType);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }       

        public DataTable GetOrderHedData(int orderNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOrderHedData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OrderNum", orderNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOAU_PartNumbers(string partNum, string unitDesign)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOAU_PartNumbers", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@UnitDesign", unitDesign);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetParentPartNumbers()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetParentPartNums", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                     
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartCategoryHead(string revNum)
        {
            DataTable dt = new DataTable();

            if (revNum == "REV5")
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetPartCategoryHead", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PullOrder", -1);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartCategoryHead", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PullOrder", -1);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return dt;
        }

        public DataTable GetPartNumbersByCategory(string category)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetPartNumbersByCategory", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Category", category);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetMetalPartNumbers()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetMetalPartNumbers", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartAsmCost(string partNum, string revNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartAsmCost", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@RevisionNum", revNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetMotorData(int ruleHeadID)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetMotorData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadID", ruleHeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartConditionsByRuleHeadID(int ruleHeadID, bool equation)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetPartConditionsByRuleHeadID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadID", ruleHeadID);
                        cmd.Parameters.AddWithValue("@Equation", equation);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetAssemblyQtyAndUOM(string partNum, string mtlPartNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetAssemblyQtyAndUOM", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@MtlPartNum", mtlPartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartRulesByRuleHeadIdAndRuleBatchId(int ruleHeadId, int ruleBatchId)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetPartRulesByRuleHeadIdAndRuleBatchId", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadId", ruleHeadId);
                        cmd.Parameters.AddWithValue("@RuleBatchId", ruleBatchId);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartRulesByDigit(int digit)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_GetPartRulesByDigit", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNo", digit);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOAU_BreakerByModelNo(string unitTypeStr, string breakerSizeStr, string digit41ValStr, string oaRev)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                if (oaRev == "REV6" || oaRev == "VKG")
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOAU_BreakerByModelNo", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@UnitType", unitTypeStr);
                            cmd.Parameters.AddWithValue("@BreakerSize", breakerSizeStr);
                            cmd.Parameters.AddWithValue("@Digit41Val", digit41ValStr);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetOAU_BreakerByModelNo", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@UnitType", unitTypeStr);
                            cmd.Parameters.AddWithValue("@BreakerSize", breakerSizeStr);
                            cmd.Parameters.AddWithValue("@Digit36Val", digit41ValStr);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable MSP_GetBreakerByModelNo(string breakerSizeStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;
               
                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_GetBreakerByModelNo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       
                        cmd.Parameters.AddWithValue("@BreakerSize", breakerSizeStr);
                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartRulesHead(string partNumber)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNumber);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSheetMetalReleaseByJobNum(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetSheetMetalReleaseByJobNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSheetMetalReleases()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetSheetMetalReleases", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSheetMetalReleaseMetal(string jobNumList)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetSheetMetalReleaseMetal", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNumList", jobNumList);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetJobMtlPartDetail(string jobNum, string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetJobMtlPartDetail", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSheetMetalQtyByPart(string jobNumList, string partNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetSheetMetalQtyByPart", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNumList", jobNumList);
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void UpdateJobMtlPartQty(long releaseID, string jobNum, string partNum, string estQtyUsed, string actQty, string modBy, string progressRecID)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateJobMtlPartQty", connection))
                    {
                        command.CommandTimeout = 120;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ReleaseID", releaseID);   
                        command.Parameters.AddWithValue("@JobNum", jobNum);                       
                        command.Parameters.AddWithValue("@PartNum", partNum);
                        command.Parameters.AddWithValue("@EstimatedQtyUsed", Decimal.Parse(estQtyUsed));
                        command.Parameters.AddWithValue("@ActualQtyUsed", Decimal.Parse(actQty));                       
                        command.Parameters.AddWithValue("@ModBy", modBy);
                        command.Parameters.AddWithValue("@ProgressRecid", progressRecID);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSheetMetalRelease(long releaseID)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateSheetMetalRelease", connection))
                    {
                        command.CommandTimeout = 120;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ReleaseID", releaseID);                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertJobOpDtl(string jobNum, string assemblySeq, string oprSeq, string opDtlSeq, string resourceGrpID, string resourceID,
                                   string estProdHours, string prodStandard, string stdFormat, string stdBasis, string opsPerPart, string prodLabRate,
                                   string prodBurRate, string opDtlDesc, string prodCrewSize, string setupCrewSize, string guid)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_ROA_InsertJobOpDtl", connection))
                    {
                        command.CommandTimeout = 120;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", jobNum);
                        command.Parameters.AddWithValue("@AssemblySeq", Int32.Parse(assemblySeq));
                        command.Parameters.AddWithValue("@OprSeq", Int32.Parse(oprSeq));
                        command.Parameters.AddWithValue("@OpDtlSeq", Int32.Parse(opDtlSeq));
                        command.Parameters.AddWithValue("@ResourceGrpID", resourceGrpID);
                        command.Parameters.AddWithValue("@ResourceID", resourceID);
                        command.Parameters.AddWithValue("@EstProdHours", Decimal.Parse(estProdHours));
                        command.Parameters.AddWithValue("@ProdStandard", Decimal.Parse(prodStandard));
                        command.Parameters.AddWithValue("@StdFormat", stdFormat);
                        command.Parameters.AddWithValue("@StdBasis", stdBasis);
                        command.Parameters.AddWithValue("@OpsPerPart", Int32.Parse(opsPerPart));
                        command.Parameters.AddWithValue("@ProdLabRate", Decimal.Parse(prodLabRate));
                        command.Parameters.AddWithValue("@ProdBurRate", Decimal.Parse(prodBurRate));
                        command.Parameters.AddWithValue("@ProdCrewSize", Decimal.Parse(prodCrewSize));
                        command.Parameters.AddWithValue("@SetupCrewSize", Decimal.Parse(setupCrewSize));
                        command.Parameters.AddWithValue("@OpDtlDesc", opDtlDesc);
                        command.Parameters.AddWithValue("@SysRowId", guid);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertJobOper(string jobNum, string revNum, string assemblySeq, string oprSeq, string opCode, string estProdHours, string prodStandard, string stdFormat,
                                  string stdBasis, string opsPerPart, string qtyPer, string prodLabRate, string prodBurRate, string laborEntryMethod, string opDesc, string guid)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_ROA_InsertJobOper", connection))
                    {
                        command.CommandTimeout = 120;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", jobNum);
                        command.Parameters.AddWithValue("@RevisionNum", revNum);
                        command.Parameters.AddWithValue("@AssemblySeq", Int32.Parse(assemblySeq));
                        command.Parameters.AddWithValue("@OprSeq", Int32.Parse(oprSeq));
                        command.Parameters.AddWithValue("@OpCode", opCode);
                        command.Parameters.AddWithValue("@EstProdHours", Decimal.Parse(estProdHours));
                        command.Parameters.AddWithValue("@ProdStandard", Decimal.Parse(prodStandard));
                        command.Parameters.AddWithValue("@StdFormat", stdFormat);
                        command.Parameters.AddWithValue("@StdBasis", stdBasis);
                        command.Parameters.AddWithValue("@OpsPerPart", Int32.Parse(opsPerPart));
                        command.Parameters.AddWithValue("@QtyPer", Decimal.Parse(qtyPer));
                        command.Parameters.AddWithValue("@ProdLabRate", Decimal.Parse(prodLabRate));
                        command.Parameters.AddWithValue("@ProdBurRate", Decimal.Parse(prodBurRate));
                        command.Parameters.AddWithValue("@LaborEntryMethod", laborEntryMethod);
                        command.Parameters.AddWithValue("@OpDesc", opDesc);
                        command.Parameters.AddWithValue("@SysRowId", guid);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertBOM_DataIntoJobMtl(DataTable dt)
        {                       
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_Insert_MaterialJobMtl", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@MtlTbl", dt);
                        connection.Open();
                        cmd.ExecuteNonQuery();                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public void Insert_SinglePartJobMtl(string jobNumStr, int seqNumInt, string partNumStr, string partDescStr, decimal partQtyDec, string iumStr, decimal unitOfCostDec, 
                                            decimal totalCostDec, string partAssemblyStr, string partTypeStr, string whseCodeStr, int relOpInt, string statusStr, string revNumStr, 
                                            string costMethodStr, string guidIdStr, int partQtyInt, int assemblySeqInt, string parentPartNumStr, int ruleHeadID, int ruleBatchID, string modByStr)
        {
            bool subAssemblyPart = false;

            try
            {
                if (assemblySeqInt > 1)
                {
                    subAssemblyPart = true;
                }

                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_Insert_SinglePartJobMtl", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        cmd.Parameters.AddWithValue("@AssemblySeq", assemblySeqInt);
                        cmd.Parameters.AddWithValue("@SeqNum", seqNumInt);
                        cmd.Parameters.AddWithValue("@PartNum", partNumStr);
                        cmd.Parameters.AddWithValue("@PartDescription", partDescStr);
                        cmd.Parameters.AddWithValue("@PartQtyDec", partQtyDec);
                        cmd.Parameters.AddWithValue("@IUM", iumStr);
                        cmd.Parameters.AddWithValue("@UnitOfCost", unitOfCostDec);
                        cmd.Parameters.AddWithValue("@TotalCost", totalCostDec);         
                        cmd.Parameters.AddWithValue("@PartAssembly", partAssemblyStr);   // Character01
                        cmd.Parameters.AddWithValue("@PartType", partTypeStr);           // Character03
                        cmd.Parameters.AddWithValue("@ParentPartNum", parentPartNumStr); // Character04
                        cmd.Parameters.AddWithValue("@WarehouseCode", whseCodeStr);
                        cmd.Parameters.AddWithValue("@RelatedOperation", relOpInt);
                        cmd.Parameters.AddWithValue("@Status", statusStr);               // Character02
                        cmd.Parameters.AddWithValue("@RevisionNumber", revNumStr);
                        cmd.Parameters.AddWithValue("@CostMethod", costMethodStr);                       
                        cmd.Parameters.AddWithValue("@GuidID", guidIdStr);
                        cmd.Parameters.AddWithValue("@PartQtyInt", partQtyInt);
                        cmd.Parameters.AddWithValue("@SubAssemblyPart", subAssemblyPart);
                        //cmd.Parameters.AddWithValue("@RuleHeadID", ruleHeadID);
                        //cmd.Parameters.AddWithValue("@RuleBatchID", ruleBatchID);
                        cmd.Parameters.AddWithValue("@ModBy", modByStr);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPartJobMtl(string jobNumStr, string partNumStr, string partDescStr, string partQtyStr, decimal unitOfCostDec)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertPartJobMtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        cmd.Parameters.AddWithValue("@AssemblySeq", 0);
                        cmd.Parameters.AddWithValue("@PartNum", partNumStr);
                        cmd.Parameters.AddWithValue("@PartDescription", partDescStr);
                        cmd.Parameters.AddWithValue("@PartQty", partQtyStr);
                        cmd.Parameters.AddWithValue("@UnitOfCost", unitOfCostDec);
                        cmd.Parameters.AddWithValue("@GuidID", Guid.NewGuid().ToString());
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateJobMtl(string jobNumStr, string partNum, decimal requiredQty, string motorType, int progressRecid, string modBy)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_UpdateJobMtl", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@RequiredQty", requiredQty);
                        cmd.Parameters.AddWithValue("@MotorType", motorType);
                        cmd.Parameters.AddWithValue("@ModBy", modBy);
                        cmd.Parameters.AddWithValue("@ProgressRecid", progressRecid);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertSheetMetalRelease(long releaseID, string jobNumStr, string unitTypeStr, DateTime releaseDate, string createBy, 
                                            DateTime createDate, string modBy, DateTime modDate)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertSheetMetalRelease", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReleaseID", releaseID);
                        cmd.Parameters.AddWithValue("@JobNumList", jobNumStr);
                        cmd.Parameters.AddWithValue("@UnitType", unitTypeStr);
                        cmd.Parameters.AddWithValue("@ReleaseDate", releaseDate);                        
                        cmd.Parameters.AddWithValue("@CreateBy", createBy);
                        cmd.Parameters.AddWithValue("@CreateDate", createDate);
                        cmd.Parameters.AddWithValue("@ModifiedBy", modBy);
                        cmd.Parameters.AddWithValue("@ModifiedDate", modDate);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertJobAssembly(string jobNum, int assemblySeq, string partNum, string description, string revisionNum, decimal quantity, 
                                      string uomStr, string whseCode, int parent, int priorPeer, int nextPeer, int child, int bomSeq, int bomLevel,
                                      string laborCost, string burdenCost, string materialCost, string prodHours, string relOp,int reqRefDes, 
                                      int bitFlag, string wgtUOMStr, string guid)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_ROA_InsertJobAssembly", connection))
                    {
                        command.CommandTimeout = 120;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", jobNum);
                        command.Parameters.AddWithValue("@AssemblySeq", assemblySeq);
                        command.Parameters.AddWithValue("@PartNum", partNum);
                        command.Parameters.AddWithValue("@Description", description);
                        command.Parameters.AddWithValue("@RevisionNum", revisionNum);
                        command.Parameters.AddWithValue("@Quantity", quantity);
                        command.Parameters.AddWithValue("@UOM", uomStr);
                        command.Parameters.AddWithValue("@WhseCode", whseCode);
                        command.Parameters.AddWithValue("@Parent", parent);
                        command.Parameters.AddWithValue("@PriorPeer", priorPeer);
                        command.Parameters.AddWithValue("@NextPeer", nextPeer);
                        command.Parameters.AddWithValue("@Child", child);
                        command.Parameters.AddWithValue("@BomSeq", bomSeq);
                        command.Parameters.AddWithValue("@BomLevel", bomLevel);
                        command.Parameters.AddWithValue("@TLE_LaborCost", Decimal.Parse(laborCost));
                        command.Parameters.AddWithValue("@TLE_BurdenCost", Decimal.Parse(burdenCost));
                        command.Parameters.AddWithValue("@TLE_MaterialCost", Decimal.Parse(materialCost));
                        command.Parameters.AddWithValue("@TLE_ProdHours", Decimal.Parse(prodHours));
                        command.Parameters.AddWithValue("@RelatedOperation", Int32.Parse(relOp));
                        command.Parameters.AddWithValue("@ReqRefDes", reqRefDes);
                        command.Parameters.AddWithValue("@BitFlag", bitFlag);
                        command.Parameters.AddWithValue("@WgtUOM", wgtUOMStr);
                        command.Parameters.AddWithValue("@SysRowId", guid);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateJobAsmbl(string jobNumStr, int assemblySeq, decimal tlaProdsHours, decimal tleLaborCost, decimal tleBurdenCost, decimal tleMaterialCost, decimal tleProdHours )
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_UpdateJobAsmbl", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        cmd.Parameters.AddWithValue("@AssemblySeq", assemblySeq);
                        cmd.Parameters.AddWithValue("@TLAProdHours", tlaProdsHours);
                        cmd.Parameters.AddWithValue("@TLELaborCost", tleLaborCost);
                        cmd.Parameters.AddWithValue("@TLEBurdenCost", tleBurdenCost);
                        cmd.Parameters.AddWithValue("@TLEMaterialCost", tleMaterialCost);
                        cmd.Parameters.AddWithValue("@TLEProdHours", tleProdHours);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteJobOpDtl(string jobNum)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_DeleteJobOpDtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);                        
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CopyJobMtlToHistory(string jobNum, string modBy)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_CopyJobMtlToHistory", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        cmd.Parameters.AddWithValue("@ModBy", modBy);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteJobMtl(string jobNum)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_DeleteJobMtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeletePartNumFromJobMtl(string jobNum, string partNum, long progressRecid, string modBy)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_DeletePartNumFromJobMtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@ModBy", modBy);
                        cmd.Parameters.AddWithValue("@ProgressRecid", progressRecid);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteJobOper(string jobNum)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_DeleteJobOper", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void removeReqQtysFromDemandPartQtyAndPartWhseTables(string jobNumStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdateRemoveQtysPartQtyPartWhse", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", jobNumStr);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteSecondaryAssemblySeqFromJobAsmbl(string jobNum)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_ROA_DeleteSecondaryAssemblySeqFromJobAsmbl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int deleteJobFromDatabase(string jobNumStr, string userName, string unitType)
        {
            string orderNumStr;
            string orderLineStr;
            string releaseNumStr;

            int jobDeletedSuccessfullyInt = 0;
            //int dash1PosInt = jobNumStr.IndexOf('-');
            //int dash2PosInt = jobNumStr.LastIndexOf('-');            

            //if (dash1PosInt > 0 && dash1PosInt > 0)
            //{                
            //    orderNumStr = jobNumStr.Substring(0, dash1PosInt);
            //    orderLineStr = jobNumStr.Substring((dash1PosInt + 1), (dash2PosInt - (dash1PosInt + 1)));
            //    releaseNumStr = jobNumStr.Substring((dash2PosInt + 1), (jobNumStr.Length - (dash2PosInt + 1)));
            //}
            //else
            //{
            //    orderNumStr = jobNumStr;
            //    orderLineStr = "0";
            //    releaseNumStr = "0";
            //}
            

            // Delete job from OAU_BOMs table & update demand quantities 

            try
            {
                removeReqQtysFromDemandPartQtyAndPartWhseTables(jobNumStr);
                DeleteOAU_BOMsData(jobNumStr, userName);
            }
            catch (Exception f)
            {
                MessageBox.Show("Error deleting job# " + jobNumStr + " from OAU_BOMs Table - " + f);
                jobDeletedSuccessfullyInt = 1;
            }

            try
            {
                if (unitType == "MON")
                {
                    MonitorDeleteEtlData(jobNumStr);
                }
                else
                {
                    DeleteEtlOAUData(jobNumStr);
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error deleting job# " + jobNumStr + " from R6_OA_EtlOAU Table - " + f);
                jobDeletedSuccessfullyInt = 1;
            }

            try
            {
                CopyJobMtlToHistory(jobNumStr, userName);                
            }
            catch (Exception f)
            {
                MessageBox.Show("Error copying job# " + jobNumStr + " from jobmtl Table to JobMtlHistory Table - " + f);
                jobDeletedSuccessfullyInt = 2;
            }

            try
            {               
                DeleteJobMtl(jobNumStr);
            }
            catch (Exception f)
            {
                MessageBox.Show("Error deleting job# " + jobNumStr + " from jobmtl Table - " + f);
                jobDeletedSuccessfullyInt = 2;
            }

            try
            {
                DeleteJobOpDtl(jobNumStr);
            }
            catch (Exception f)
            {
                MessageBox.Show("Error deleting job# " + jobNumStr + " from jobopdtl Table - " + f);
                jobDeletedSuccessfullyInt = 3;
            }

            try
            {
                DeleteJobOper(jobNumStr);
            }
            catch (Exception f)
            {
                MessageBox.Show("Error deleting job# " + jobNumStr + " from joboper Table - " + f);
                jobDeletedSuccessfullyInt = 4;
            }

            try
            {
                DeleteSecondaryAssemblySeqFromJobAsmbl(jobNumStr);
            }
            catch (Exception f)
            {
                MessageBox.Show("Error deleting JobAsmbl# " + jobNumStr + " AssemblySeq 1 & 2 - " + f);
                jobDeletedSuccessfullyInt = 8;
            }

            Thread.Sleep(10000); //Sleep 5 seconds to give deletions time to complete
            return jobDeletedSuccessfullyInt;
        }

        #endregion

        #region ETL Sticker Sql         
        public DataTable GetMonitorEtlData(string jobNumStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetEtlData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetEtlData(string jobNum)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetEtlData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);                               
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetEtlJobHeadData(string jobNumStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetEtlJobHeadData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void DeleteEtlJobHeadData(string jobNumStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_DeleteEtlJobHeadData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        

        public void DeleteEtlOAUData(string jobNumStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_DeleteEtlOAUData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MonitorDeleteEtlData(string jobNumStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorDeleteEtlData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertEtlJobHeadData(string jobNumStr, string custName, string orderNum, string orderLine,
                                         string releaseNum, string unitType, string endConsumerName)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertEtlJobHeadData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        cmd.Parameters.AddWithValue("@CustName", custName);
                        cmd.Parameters.AddWithValue("@OrderNum", orderNum);
                        cmd.Parameters.AddWithValue("@OrderLine", orderLine);
                        cmd.Parameters.AddWithValue("@ReleaseNum", releaseNum);
                        cmd.Parameters.AddWithValue("@UnitType", unitType);
                        cmd.Parameters.AddWithValue("@EndConsumerName", endConsumerName);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertETL_Oau()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertETLOau", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);                       
                        cmd.Parameters.AddWithValue("@OrderNum", OrderNum);
                        cmd.Parameters.AddWithValue("@OrderLine", OrderLine);
                        cmd.Parameters.AddWithValue("@ReleaseNum", ReleaseNum);
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                        cmd.Parameters.AddWithValue("@SerialNo", SerialNo);
                        cmd.Parameters.AddWithValue("@MfgDate", MfgDate);
                        cmd.Parameters.AddWithValue("@UnitType", UnitType);
                        cmd.Parameters.AddWithValue("@ElecRating", ElecRating);
                        cmd.Parameters.AddWithValue("@OperVoltage", OperVoltage);
                        cmd.Parameters.AddWithValue("@DualPointPower", DualPointPower);
                        cmd.Parameters.AddWithValue("@MinCKTAmp", MinCKTAmp);
                        cmd.Parameters.AddWithValue("@MinCKTAmp2", MinCKTAmp2);
                        cmd.Parameters.AddWithValue("@MfsMcb", MfsMcb);
                        cmd.Parameters.AddWithValue("@MfsMcb2", MfsMcb2);
                        cmd.Parameters.AddWithValue("@MOP", MOP);
                        cmd.Parameters.AddWithValue("@HeatingType", HeatingType);
                        cmd.Parameters.AddWithValue("@Voltage", Voltage);
                        cmd.Parameters.AddWithValue("@TestPressureHigh", TestPressureHigh);
                        cmd.Parameters.AddWithValue("@TestPressureLow", TestPressureLow);
                        cmd.Parameters.AddWithValue("@SecondaryHtgInput", SecondaryHtgInput);
                        cmd.Parameters.AddWithValue("@HeatingInputElectric", HeatingInputElectric);
                        cmd.Parameters.AddWithValue("@FuelType", FuelType);
                        cmd.Parameters.AddWithValue("@Comp1Qty", Comp1Qty);
                        cmd.Parameters.AddWithValue("@Comp2Qty", Comp2Qty);
                        cmd.Parameters.AddWithValue("@Comp3Qty", Comp3Qty);
                        cmd.Parameters.AddWithValue("@Comp4Qty", Comp4Qty);
                        cmd.Parameters.AddWithValue("@Comp5Qty", Comp5Qty);
                        cmd.Parameters.AddWithValue("@Comp6Qty", Comp6Qty);
                        cmd.Parameters.AddWithValue("@Comp1Phase", Comp1Phase);
                        cmd.Parameters.AddWithValue("@Comp2Phase", Comp2Phase);
                        cmd.Parameters.AddWithValue("@Comp3Phase", Comp3Phase);
                        cmd.Parameters.AddWithValue("@Comp4Phase", Comp4Phase);
                        cmd.Parameters.AddWithValue("@Comp5Phase", Comp5Phase);
                        cmd.Parameters.AddWithValue("@Comp6Phase", Comp6Phase);
                        cmd.Parameters.AddWithValue("@Comp1RLA_Volts", Comp1RLA_Volts);
                        cmd.Parameters.AddWithValue("@Comp2RLA_Volts", Comp2RLA_Volts);
                        cmd.Parameters.AddWithValue("@Comp3RLA_Volts", Comp3RLA_Volts);
                        cmd.Parameters.AddWithValue("@Comp4RLA_Volts", Comp4RLA_Volts);
                        cmd.Parameters.AddWithValue("@Comp5RLA_Volts", Comp5RLA_Volts);
                        cmd.Parameters.AddWithValue("@Comp6RLA_Volts", Comp6RLA_Volts);
                        cmd.Parameters.AddWithValue("@Comp1LRA", Comp1LRA);
                        cmd.Parameters.AddWithValue("@Comp2LRA", Comp2LRA);
                        cmd.Parameters.AddWithValue("@Comp3LRA", Comp3LRA);
                        cmd.Parameters.AddWithValue("@Comp4LRA", Comp4LRA);
                        cmd.Parameters.AddWithValue("@Comp5LRA", Comp5LRA);
                        cmd.Parameters.AddWithValue("@Comp6LRA", Comp6LRA);
                        cmd.Parameters.AddWithValue("@Circuit1Charge", Circuit1Charge);
                        cmd.Parameters.AddWithValue("@Circuit2Charge", Circuit2Charge);
                        cmd.Parameters.AddWithValue("@FanCondQty", FanCondQty);
                        cmd.Parameters.AddWithValue("@FanEvapQty", FanEvapQty);
                        cmd.Parameters.AddWithValue("@FanErvQty", FanErvQty);
                        cmd.Parameters.AddWithValue("@FanPwrExhQty", FanPwrExhQty);
                        cmd.Parameters.AddWithValue("@FanCondPhase", FanCondPhase);
                        cmd.Parameters.AddWithValue("@FanEvapPhase", FanEvapPhase);
                        cmd.Parameters.AddWithValue("@FanErvPhase", FanErvPhase);
                        cmd.Parameters.AddWithValue("@FanPwrExhPhase", FanPwrExhPhase);
                        cmd.Parameters.AddWithValue("@FanCondFLA", FanCondFLA);
                        cmd.Parameters.AddWithValue("@FanEvapFLA", FanEvapFLA);
                        cmd.Parameters.AddWithValue("@FanErvFLA", FanErvFLA);
                        cmd.Parameters.AddWithValue("@FanPwrExhFLA", FanPwrExhFLA);
                        cmd.Parameters.AddWithValue("@FanCondHP", FanCondHP);
                        cmd.Parameters.AddWithValue("@FanEvapHP", FanEvapHP);
                        cmd.Parameters.AddWithValue("@FanErvHP", FanErvHP);
                        cmd.Parameters.AddWithValue("@FanPwrExhHP", FanPwrExhHP);
                        cmd.Parameters.AddWithValue("@FlowRate", FlowRate);
                        cmd.Parameters.AddWithValue("@EnteringTemp", EnteringTemp);
                        cmd.Parameters.AddWithValue("@OperatingPressure", OperatingPressure);
                        cmd.Parameters.AddWithValue("@WaterGlycol", WaterGlycol);
                        cmd.Parameters.AddWithValue("@DF_StaticPressure", DF_StaticPressure);
                        cmd.Parameters.AddWithValue("@DF_MaxHtgInputBTUH", DF_MaxHtgInputBTUH);
                        cmd.Parameters.AddWithValue("@DF_MinHeatingInput", DF_MinHeatingInput);
                        cmd.Parameters.AddWithValue("@DF_MinPressureDrop", DF_MinPressureDrop);
                        cmd.Parameters.AddWithValue("@DF_MaxPressureDrop", DF_MaxPressureDrop);
                        cmd.Parameters.AddWithValue("@DF_ManifoldPressure", DF_ManifoldPressure);
                        cmd.Parameters.AddWithValue("@DF_MinGasPressure", DF_MinGasPressure);
                        cmd.Parameters.AddWithValue("@DF_MaxGasPressure", DF_MaxGasPressure);
                        cmd.Parameters.AddWithValue("@DF_CutoutTemp", DF_CutoutTemp);
                        cmd.Parameters.AddWithValue("@DF_TempRise", DF_TempRise);
                        cmd.Parameters.AddWithValue("@IN_MaxHtgInputBTUH", IN_MaxHtgInputBTUH);
                        cmd.Parameters.AddWithValue("@IN_HeatingOutputBTUH", IN_HeatingOutputBTUH);
                        cmd.Parameters.AddWithValue("@IN_MinInputBTU", IN_MinInputBTU);
                        cmd.Parameters.AddWithValue("@IN_MaxExt", IN_MaxExt);
                        cmd.Parameters.AddWithValue("@IN_TempRise", IN_TempRise);
                        cmd.Parameters.AddWithValue("@IN_MaxOutAirTemp", IN_MaxOutAirTemp);
                        cmd.Parameters.AddWithValue("@IN_MaxGasPressure", IN_MaxGasPressure);
                        cmd.Parameters.AddWithValue("@IN_MinGasPressure", IN_MinGasPressure);
                        cmd.Parameters.AddWithValue("@IN_ManifoldPressure", IN_ManifoldPressure);
                        cmd.Parameters.AddWithValue("@ModelNoVerifiedBy", ModelNoVerifiedBy);
                        cmd.Parameters.AddWithValue("@French", French);
                        cmd.Parameters.AddWithValue("@WhseCode", WhseCode);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MonitorInsertEtl()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorInsertEtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@OrderNum", OrderNum);
                        cmd.Parameters.AddWithValue("@OrderLine", OrderLine);
                        cmd.Parameters.AddWithValue("@ReleaseNum", ReleaseNum);
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                        cmd.Parameters.AddWithValue("@SerialNo", SerialNo);
                        cmd.Parameters.AddWithValue("@MfgDate", MfgDate);                       
                        cmd.Parameters.AddWithValue("@ElecRating", ElecRating);
                        cmd.Parameters.AddWithValue("@OperVoltage", OperVoltage);                       
                        cmd.Parameters.AddWithValue("@MinCKTAmp", MinCKTAmp);                        
                        cmd.Parameters.AddWithValue("@MOP", MfsMcb);                       
                        cmd.Parameters.AddWithValue("@RawMOP", RawMOP);
                        cmd.Parameters.AddWithValue("@HeatingType", HeatingType);
                        cmd.Parameters.AddWithValue("@Voltage", Voltage);
                        cmd.Parameters.AddWithValue("@ElectricHeatFLA", ElectricHeatFLA);
                        cmd.Parameters.AddWithValue("@PreHeatFLA", PreHeatFLA);
                        cmd.Parameters.AddWithValue("@FuelType", FuelType);
                        cmd.Parameters.AddWithValue("@MaxOutletTemp", MaxOutletTemp);                       
                        cmd.Parameters.AddWithValue("@Circuit1Charge", Circuit1Charge);
                        cmd.Parameters.AddWithValue("@Circuit2Charge", Circuit2Charge);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdQty", DD_SupplyFanStdQty);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdPhase", DD_SupplyFanStdPhase);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdFLA", DD_SupplyFanStdFLA);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanStdHP", DD_SupplyFanStdHP);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrQty", DD_SupplyFanOvrQty);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrPhase", DD_SupplyFanOvrPhase);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrFLA", DD_SupplyFanOvrFLA);
                        cmd.Parameters.AddWithValue("@DD_SupplyFanOvrHP", DD_SupplyFanOvrHP);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdQty", BeltSupplyFanStdQty);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdPhase", BeltSupplyFanStdPhase);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdFLA", BeltSupplyFanStdFLA);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanStdHP", BeltSupplyFanStdHP);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrQty", BeltSupplyFanOvrQty);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrPhase", BeltSupplyFanOvrPhase);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrFLA", BeltSupplyFanOvrFLA);
                        cmd.Parameters.AddWithValue("@BeltSupplyFanOvrHP", BeltSupplyFanOvrHP);
                        cmd.Parameters.AddWithValue("@CondenserFanStdQty", CondenserFanStdQty);
                        cmd.Parameters.AddWithValue("@CondenserFanStdPhase", CondenserFanStdPhase);
                        cmd.Parameters.AddWithValue("@CondenserFanStdFLA", CondenserFanStdFLA);
                        cmd.Parameters.AddWithValue("@CondenserFanStdHP", CondenserFanStdHP);
                        cmd.Parameters.AddWithValue("@CondenserFanHighQty", CondenserFanHighQty);
                        cmd.Parameters.AddWithValue("@CondenserFanHighPhase", CondenserFanHighPhase);
                        cmd.Parameters.AddWithValue("@CondenserFanHighFLA", CondenserFanHighFLA);
                        cmd.Parameters.AddWithValue("@CondenserFanHighHP", CondenserFanHighHP);
                        cmd.Parameters.AddWithValue("@Comp1StdRLA", Comp1StdRLA);
                        cmd.Parameters.AddWithValue("@Comp1StdPhase", Comp1StdPhase);
                        cmd.Parameters.AddWithValue("@Comp1StdQty", Comp1StdQty);
                        cmd.Parameters.AddWithValue("@Comp2StdRLA", Comp2StdRLA);
                        cmd.Parameters.AddWithValue("@Comp2StdPhase", Comp2StdPhase);
                        cmd.Parameters.AddWithValue("@Comp2StdQty", Comp2StdQty);
                        cmd.Parameters.AddWithValue("@Comp3StdRLA", Comp3StdRLA);
                        cmd.Parameters.AddWithValue("@Comp3StdPhase", Comp3StdPhase);
                        cmd.Parameters.AddWithValue("@Comp3StdQty", Comp3StdQty);
                        cmd.Parameters.AddWithValue("@Comp1HighRLA", Comp1HighRLA);
                        cmd.Parameters.AddWithValue("@Comp1HighPhase", Comp1HighPhase);
                        cmd.Parameters.AddWithValue("@Comp1HighQty", Comp1HighQty);
                        cmd.Parameters.AddWithValue("@Comp2HighRLA", Comp2HighRLA);
                        cmd.Parameters.AddWithValue("@Comp2HighPhase", Comp2HighPhase);
                        cmd.Parameters.AddWithValue("@Comp2HighQty", Comp2HighQty);
                        cmd.Parameters.AddWithValue("@ExhaustFanQty", ExhaustFanQty);
                        cmd.Parameters.AddWithValue("@ExhaustFanPhase", ExhaustFanPhase);
                        cmd.Parameters.AddWithValue("@ExhaustFanFLA", ExhaustFanFLA);
                        cmd.Parameters.AddWithValue("@ExhaustFanHP", ExhaustFanHP);
                        cmd.Parameters.AddWithValue("@SemcoWheelQty", SemcoWheelQty);
                        cmd.Parameters.AddWithValue("@SemcoWheelPhase", SemcoWheelPhase);
                        cmd.Parameters.AddWithValue("@SemcoWheelFLA", SemcoWheelFLA);
                        cmd.Parameters.AddWithValue("@SemcoWheelHP", SemcoWheelHP);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelQty", AirXchangeWheelQty);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelPhase", AirXchangeWheelPhase);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelFLA", AirXchangeWheelFLA);
                        cmd.Parameters.AddWithValue("@AirXchangeWheelHP", AirXchangeWheelHP);                      
                        cmd.Parameters.AddWithValue("@ModelNoVerifiedBy", ModelNoVerifiedBy);
                        cmd.Parameters.AddWithValue("@French", French);                        
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MSP_InsertEtl()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MSP_InsertEtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@OrderNum", OrderNum);
                        cmd.Parameters.AddWithValue("@OrderLine", OrderLine);
                        cmd.Parameters.AddWithValue("@ReleaseNum", ReleaseNum);
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo); 
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@SerialNum", SerialNo);
                        cmd.Parameters.AddWithValue("@MfgDate", null);
                        cmd.Parameters.AddWithValue("@Designation", MSPDesignation);
                        cmd.Parameters.AddWithValue("@Filter", MSPFanFilter);
                        cmd.Parameters.AddWithValue("@Voltage", MSPFanVoltage);
                        cmd.Parameters.AddWithValue("@Power", MSPFanPower);
                        cmd.Parameters.AddWithValue("@Draw", MSPFanDraw);

                        cmd.Parameters.AddWithValue("@FanQty", int.Parse(MSPFanQty));
                        cmd.Parameters.AddWithValue("@FanAirVolume", MSPFanAirVolume);
                        cmd.Parameters.AddWithValue("@FanExternalSP", MSPFanExternalSP);
                        cmd.Parameters.AddWithValue("@FanTotalSP", MSPFanTotalSP);
                        cmd.Parameters.AddWithValue("@AmbientTemp", AmbientTemp);
                        cmd.Parameters.AddWithValue("@Refrigerant", Refrigerant);

                        cmd.Parameters.AddWithValue("@Dehumidifier_BTUH", Dehumidifier_BTUH);
                        cmd.Parameters.AddWithValue("@Dehumidifier_SST", Dehumidifier_SST);
                        cmd.Parameters.AddWithValue("@Dehumidifier_EWT", Dehumidifier_EWT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_LWT", Dehumidifier_LWT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_GPM", Dehumidifier_GPM);
                        cmd.Parameters.AddWithValue("@Dehumidifier_PD", Dehumidifier_PD);
                        cmd.Parameters.AddWithValue("@Dehumidifier_Glycol", Dehumidifier_Glycol);
                        cmd.Parameters.AddWithValue("@Dehumidifier_EAT", Dehumidifier_EAT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_LAT", Dehumidifier_LAT);
                        cmd.Parameters.AddWithValue("@Dehumidifier_CAPACITY", Dehumidifier_CAPACITY);

                        cmd.Parameters.AddWithValue("@PostCooling_BTUH", PostCooling_BTUH);
                        cmd.Parameters.AddWithValue("@PostCooling_SST", PostCooling_SST);
                        cmd.Parameters.AddWithValue("@PostCooling_EWT", PostCooling_EWT);
                        cmd.Parameters.AddWithValue("@PostCooling_LWT", PostCooling_LWT);
                        cmd.Parameters.AddWithValue("@PostCooling_GPM", PostCooling_GPM);
                        cmd.Parameters.AddWithValue("@PostCooling_PD", PostCooling_PD);
                        cmd.Parameters.AddWithValue("@PostCooling_Glycol", PostCooling_Glycol);
                        cmd.Parameters.AddWithValue("@PostCooling_EAT", PostCooling_EAT);
                        cmd.Parameters.AddWithValue("@PostCooling_LAT", PostCooling_LAT);

                        cmd.Parameters.AddWithValue("@PostHeating_BTUH", PostHeating_BTUH);
                        cmd.Parameters.AddWithValue("@PostHeating_EWT", PostHeating_EWT);
                        cmd.Parameters.AddWithValue("@PostHeating_LWT", PostHeating_LWT);
                        cmd.Parameters.AddWithValue("@PostHeating_GPM", PostHeating_GPM);
                        cmd.Parameters.AddWithValue("@PostHeating_PD", PostHeating_PD);
                        cmd.Parameters.AddWithValue("@PostHeating_Glycol", PostHeating_Glycol);
                        cmd.Parameters.AddWithValue("@PostHeating_EAT", PostHeating_EAT);
                        cmd.Parameters.AddWithValue("@PostHeating_LAT", PostHeating_LAT);

                        cmd.Parameters.AddWithValue("@VerifiedBy", ModelNoVerifiedBy);
                        cmd.Parameters.AddWithValue("@French", French);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetModelNumDigitDesc(int digit, string heatType, string digitVal, string unitType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;
                if (unitType.ToUpper() == "REV6")
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetModelNumDesc", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@DigitNum", digit);
                            cmd.Parameters.AddWithValue("@HeatType", heatType);
                            cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.VKG_GetModelNumDesc", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@DigitNum", digit);
                            cmd.Parameters.AddWithValue("@HeatType", heatType);
                            cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        
        #endregion

        #region Compressor and Motor Data Sql
        public DataTable GetCompCircuitChargeData(string digit3, string digit4, string digit567, string digit9, string digit11, string digit12,  string digit13, string digit14)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetCompCircuitChargeData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Digit3", digit3);
                        cmd.Parameters.AddWithValue("@Digit4", digit4);
                        cmd.Parameters.AddWithValue("@Digit567", digit567);
                        cmd.Parameters.AddWithValue("@Digit9", digit9);
                        cmd.Parameters.AddWithValue("@Digit11", digit11);
                        cmd.Parameters.AddWithValue("@Digit12", digit12);
                        cmd.Parameters.AddWithValue("@Digit13", digit13);
                        cmd.Parameters.AddWithValue("@Digit14", digit14);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetRev5CompCircuitChargeData(string digit3, string digit4, string digit567,
                                                  string digit9, string digit13, string digit14)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetRev5CompCircuitChargeData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Digit3", digit3);
                        cmd.Parameters.AddWithValue("@Digit4", digit4);
                        cmd.Parameters.AddWithValue("@Digit567", digit567);
                        cmd.Parameters.AddWithValue("@Digit9", digit9);
                        cmd.Parameters.AddWithValue("@Digit13", digit13);
                        cmd.Parameters.AddWithValue("@Digit14", digit14);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        //public DataTable GetCompAndMotorData(string digit1, string digit2, string digit456, string digit7, string digit8, string digit10, string digit22, string digit32, string digit33)
        //{
        //    DataTable dt = new DataTable();

        //    try
        //    {
        //        var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

        //        using (var connection = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetCompAndMotorData", connection))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@Digit1", digit1);
        //                cmd.Parameters.AddWithValue("@Digit2", digit2);
        //                cmd.Parameters.AddWithValue("@Digit456", digit456);
        //                cmd.Parameters.AddWithValue("@Digit7", digit7);
        //                cmd.Parameters.AddWithValue("@Digit8", digit8);
        //                cmd.Parameters.AddWithValue("@Digit10", digit10);
        //                cmd.Parameters.AddWithValue("@Digit22", digit22);
        //                cmd.Parameters.AddWithValue("@Digit32", digit32);
        //                cmd.Parameters.AddWithValue("@Digit33", digit33);    
        //                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        //                {
        //                    if (da.SelectCommand.Connection.State != ConnectionState.Open)
        //                    {
        //                        da.Fill(dt);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return dt;
        //}

        public DataTable MonitorGetMotorDataDTL()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.MonitorGetMotorDataDTL", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        cmd.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Report Sql
        public void DeleteFromOA_PremliminaryBOM(string jobNumStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.DeleteFromOA_PremliminaryBOM", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", jobNumStr);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetOAU_JobsByDateRange(DateTime StartDate, DateTime EndDate)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_JobsByDateRange", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@StartDate", StartDate);
                        cmd.Parameters.AddWithValue("@EndDate", EndDate);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public void GetCritCompParts_InsertOA_PremBOM(string jobNumStr, string modelNoStr)
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.GetCritCompParts_InsertOA_PremBOM", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@JobNum", jobNumStr);
                        command.Parameters.AddWithValue("@ModelNo", modelNoStr);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Insert_OAU_PickListHistory(DataTable dt, string createBy)
        {
            string reportID = "1";
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.Insert_OAU_PickListHistory", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PickListTbl", dt);
                        command.Parameters.AddWithValue("@CreateBy", createBy);
                        command.Parameters.Add("@ReportID", SqlDbType.BigInt);
                        command.Parameters["@ReportID"].Direction = ParameterDirection.Output;
                        connection.Open();
                        command.ExecuteNonQuery();

                        reportID = command.Parameters["@ReportID"].Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportID;
        }
        #endregion

        #region Other Methods
        public DataTable AddRefrigAndBreakerAndWarranty(DataTable dtBOM, string modelNoStr, string mcaStr, string mopStr, 
                                         string unitType, out string circuit1ChargeRetVal, out string circuit2ChargeRetVal)
        {            
            string unitPriceStr = "";
            string ruleHeadIdStr = "";
            string digit3 = modelNoStr.Substring(2, 1);
            string digit4 = modelNoStr.Substring(3, 1);
            string digit567 = modelNoStr.Substring(4, 3);
            string digit9 = modelNoStr.Substring(8, 1);
            string digit11 = modelNoStr.Substring(10, 1);
            string digit12 = modelNoStr.Substring(11, 1);
            string digit13 = modelNoStr.Substring(12, 1);
            string digit14 = modelNoStr.Substring(13, 1);
            string circuit1ChargeStr = "0";
            string circuit2ChargeStr = "0";
            string stationLoc = "";

            int seqNumInt = 0;
            int relOp = 0;

            DataTable dtCCD = GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow drComp = dtCCD.Rows[0];

                if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = drComp["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(drComp["Circuit1_Charge"].ToString()) + Decimal.Parse(drComp["Circuit1_ReheatCharge"].ToString())).ToString();
                }

                if (drComp["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = drComp["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(drComp["Circuit2_Charge"].ToString()) + Decimal.Parse(drComp["Circuit2_ReheatCharge"].ToString())).ToString();
                    }
                }
            }
            else
            {
                if (modelNoStr.StartsWith("OA") == true)
                {
                    digit13 = "0";
                }

                DataTable dtChrg = GetCompressorChargeData(modelNoStr.Substring(0, 3), modelNoStr.Substring(4, 3),
                                                  modelNoStr.Substring(10, 1), digit13, modelNoStr.Substring(13, 1));

                if (dtChrg.Rows.Count > 0)
                {
                    DataRow drChrg = dtChrg.Rows[0];

                    if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit1ChargeStr = drChrg["Circuit1"].ToString();
                    }
                    else
                    {
                        circuit1ChargeStr = (Decimal.Parse(drChrg["Circuit1"].ToString()) + Decimal.Parse(drChrg["Circuit1_Reheat"].ToString())).ToString();
                    }

                    circuit2ChargeStr = drChrg["Circuit2"].ToString();
                }
            }

            circuit1ChargeRetVal = circuit1ChargeStr;
            circuit2ChargeRetVal = circuit2ChargeStr;

            DataTable dtPart = GetPartDetail("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                unitPriceStr = drPart["UnitCost"].ToString();
            }

            dtPart = GetPartRulesHead("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                ruleHeadIdStr = drPart["ID"].ToString();
            }

            var dr = dtBOM.NewRow();
            dr["PartCategory"] = "Refrigeration";
            dr["AssemblySeq"] = 0;
            seqNumInt = GetNextSeqNum("0", dtBOM);
            dr["SeqNo"] = seqNumInt;
            dr["PartNum"] = "VCPRFG-0400";
            dr["Description"] = "Tank, 100lb, R-410a";

            string partQtyStr = (decimal.Parse(circuit1ChargeStr) + decimal.Parse(circuit2ChargeStr)).ToString();

            decimal partQtyDec = 0;

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
                if (partQtyDec == 0)
                {
                    partQtyDec = 1;
                }
            }

            //dr["QtyPer"] = partQtyDec;
            dr["ReqQty"] = partQtyDec;
            dr["UOM"] = "LB";
            dr["UnitCost"] = decimal.Parse(unitPriceStr);
            dr["WhseCode"] = "LWH5";
            dr["RelOp"] = 60;
            stationLoc = findStationLoc(60, unitType, "LT");
            dr["PartStatus"] = "Active";
            dr["RevisionNum"] = "";
            dr["CostMethod"] = "A";
            dr["PartType"] = "";
            dr["BuyToOrder"] = (byte)0;
            dr["NonStock"] = (byte)1;
            dr["RunOut"] = (byte)0;
            dr["Status"] = "BackFlush";
            dr["PROGRESS_RECID"] = 0;
            dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr["RuleBatchID"] = 0;
            dr["ParentPartNum"] = "";
            dr["StationLoc"] = stationLoc;
            dr["ConfigurablePart"] = "No";
            dr["SubAssemblyPart"] = 0;
            dr["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr);

            if (Int32.Parse(mopStr) < 100)
            {
                string tempMopStr = "0" + mopStr;
                mopStr = tempMopStr;
            }

            if (unitType == "VKG")
            {
                dtPart = GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(40, 1), unitType);
            }
            else if (unitType == "REV6")
            {
                dtPart = GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(40, 1), unitType);
            }
            else
            {
                dtPart = GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(36, 1), unitType);
            }

            if (dtPart.Rows.Count > 0)
            {
                var dr1 = dtBOM.NewRow();

                DataRow drPart = dtPart.Rows[0];

                dr1["PartCategory"] = drPart["PartCategory"].ToString();
                dr1["AssemblySeq"] = 0;
                dr1["SeqNo"] = seqNumInt + 10;
                dr1["PartNum"] = drPart["PartNum"].ToString();
                dr1["Description"] = drPart["PartDescription"].ToString();

                partQtyStr = drPart["ReqQty"].ToString();

                if (partQtyStr == "")
                {
                    partQtyDec = 0;
                }
                else
                {
                    partQtyDec = decimal.Parse(partQtyStr);
                }

                //dr1["QtyPer"] = partQtyDec;
                dr1["ReqQty"] = partQtyDec;
                dr1["UOM"] = drPart["UOM"].ToString();
                unitPriceStr = drPart["UnitPrice"].ToString();
                dr1["UnitCost"] = decimal.Parse(unitPriceStr);
                dr1["WhseCode"] = "LWH5";
                relOp = Int32.Parse(drPart["RelOp"].ToString());
                dr1["RelOp"] = relOp;
                stationLoc = findStationLoc(relOp, unitType, "LT");
                dr1["PartStatus"] = "Active";
                dr1["RevisionNum"] = drPart["RevisionNum"].ToString();
                dr1["CostMethod"] = drPart["CostMethod"].ToString();
                dr1["PartType"] = drPart["PartType"].ToString();
                dr1["BuyToOrder"] = (byte)1;
                dr1["NonStock"] = (byte)1;
                dr1["RunOut"] = (byte)0;
                dr1["PROGRESS_RECID"] = 0;
                ruleHeadIdStr = drPart["RuleHeadId"].ToString();
                dr1["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                dr1["RuleBatchID"] = 0;
                dr1["ParentPartNum"] = "";
                dr1["StationLoc"] = stationLoc;
                dr1["ConfigurablePart"] = "No";
                dr1["SubAssemblyPart"] = 0;
                dr1["SubAsmMtlPart"] = 0;

                dtBOM.Rows.Add(dr1);
            }

            var dr2 = dtBOM.NewRow();

            dr2["PartCategory"] = "Warranty";
            dr2["AssemblySeq"] = 0;
            seqNumInt = 9998;
            dr2["SeqNo"] = seqNumInt.ToString();
            dr2["PartNum"] = "OAWARRANTYSTD";
            dr2["Description"] = "Warranty Part Entry";

            partQtyStr = "1.00";

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            //dr2["QtyPer"] = partQtyDec;
            dr2["ReqQty"] = partQtyDec;
            dr2["UOM"] = "";
            //unitPriceStr = drPart["UnitPrice"].ToString();
            //dr2["UnitCost"] = decimal.Parse(unitPriceStr);
            dr2["UnitCost"] = 0;
            dr2["WhseCode"] = "LWH5";
            dr2["RelOp"] = 90;
            stationLoc = findStationLoc(90, unitType, "LT");
            dr2["PartStatus"] = "Active";
            dr2["RevisionNum"] = "";
            dr2["CostMethod"] = "A";
            dr2["PartType"] = "Warranty";
            dr2["BuyToOrder"] = (byte)0;
            dr2["RunOut"] = (byte)0;
            dr2["NonStock"] = (byte)0;
            dr2["Status"] = "BackFlush";
            dr2["PROGRESS_RECID"] = 0;
            ruleHeadIdStr = "0";
            dr2["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr2["RuleBatchID"] = 0;
            dr2["ParentPartNum"] = "";
            dr2["StationLoc"] = stationLoc;
            dr2["ConfigurablePart"] = "No";
            dr2["SubAssemblyPart"] = 0;
            dr2["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr2);

            var dr3 = dtBOM.NewRow();

            dr3["PartCategory"] = "Warranty";
            dr3["AssemblySeq"] = 0;
            seqNumInt = 9999;
            dr3["SeqNo"] = seqNumInt.ToString();
            dr3["PartNum"] = "OAWARRANTYCOMP";
            dr3["Description"] = "Warranty Part Entry";

            partQtyStr = "1.00";

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            //dr3["QtyPer"] = partQtyDec;
            dr3["ReqQty"] = partQtyDec;
            dr3["UOM"] = "";
            //unitPriceStr = drPart["UnitPrice"].ToString();
            //dr3["UnitCost"] = decimal.Parse(unitPriceStr);
            dr3["UnitCost"] = 0;
            dr3["WhseCode"] = "LWH5";
            dr3["RelOp"] = 90;
            dr3["PartStatus"] = "Active";
            dr3["RevisionNum"] = "";
            dr3["CostMethod"] = "A";
            dr3["PartType"] = "Warranty";
            dr3["BuyToOrder"] = (byte)0;
            dr3["RunOut"] = (byte)0;
            dr3["NonStock"] = (byte)0;
            dr3["Status"] = "BackFlush";
            dr3["PROGRESS_RECID"] = 0;
            ruleHeadIdStr = "0";
            dr3["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr3["RuleBatchID"] = 0;
            dr3["ParentPartNum"] = "";
            dr3["StationLoc"] = stationLoc;
            dr3["ConfigurablePart"] = "No";
            dr3["SubAssemblyPart"] = 0;
            dr3["SubAsmMtlPart"] = 0;

            dtBOM.Rows.Add(dr3);

            return dtBOM;
        }

        public void SaveBOM_StatLoc(DataTable dtBOM, string jobNumStr, string modelNoStr, string unitTypeStr)
        {           
            string lastUpdatedByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            string errorMsg = "";                      
           
            DataTable dtOauBom = GetOAU_BOMsData(jobNumStr);
           
            if (dtOauBom.Rows.Count == 0)
            {
                InsertOAU_BOMsData(jobNumStr, lastUpdatedByStr);   // Insert entry for Job into the R6_OA_BOMs
            }
            else
            {
                UpdateOAU_BOMsData(jobNumStr, lastUpdatedByStr);   // Update entry for Job into the R6_OA_BOMs
            }

            errorMsg = SaveBomDataIntoEpicor(dtBOM, jobNumStr, unitTypeStr);
                    
            if (errorMsg.Length == 0)
            {
                AddETLStickerData(dtBOM, jobNumStr, unitTypeStr, modelNoStr);                
            }                
            
        }

        private string SaveBomDataIntoEpicor(DataTable dtBOM, string jobNumStr, string unitTypeStr)
        {
            string errorMsg = "";
            string epicorServer = "";           

            if (GlobalJob.DisplayMode == "Production")
            {
                epicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            }
            else if (GlobalJob.DisplayMode == "Debug")
            {
                epicorServer = ConfigurationManager.AppSettings["DevEpicorAppServer"];
            }
            else
            {
                epicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            }

            string epicorSqlConnectionString = ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.epicor905ConnectionString"].ConnectionString;

            Console.WriteLine(epicorServer);


            BuildBOM bb = new BuildBOM(epicorServer, epicorSqlConnectionString);

            //MessageBox.Show("epicorServer = " + epicorServer + "  epicorSqlConnectionString = " + epicorSqlConnectionString);            

            errorMsg = bb.SaveBOM(jobNumStr, dtBOM);
           
            return errorMsg;
        }

        private void AddETLStickerData(DataTable dtBOM, string jobNumStr, string unitTypeStr, string modelNoStr)
        {
            string orderNumStr;
            string orderLineStr;           
            string releaseNumStr;
            string custNameStr = "";           

            int dash1PosInt = jobNumStr.IndexOf('-');
            int dash2PosInt = jobNumStr.LastIndexOf('-');

            if (dash1PosInt > 0 && dash1PosInt > 0)
            {
                orderNumStr = jobNumStr.Substring(0, dash1PosInt);
                orderLineStr = jobNumStr.Substring((dash1PosInt + 1), (dash2PosInt - (dash1PosInt + 1)));
                releaseNumStr = jobNumStr.Substring((dash2PosInt + 1), (jobNumStr.Length - (dash2PosInt + 1)));
            }
            else
            {
                orderNumStr = jobNumStr;
                orderLineStr = "0";
                releaseNumStr = "0";
            }           

            //if (unitTypeStr == "MON") // Use Monitor Etl method.
            //{
            //    writeEtlMonitorStickerData(jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNumStr);
            //}
            //else if (unitTypeStr == "MSP")  // Use MSP Etl table.
            //{
            //    writeMSP_EtlStickerData(jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNumStr);
            //    unitTypeStr = "MSP"; // This is changed for the ETL Label program to be able to recognize the unit type.
            //}
            if (unitTypeStr == "REV6")  // Use Rev 6 Etl table.
            {
                WriteRev6EtlOAUStickerData(dtBOM, jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNoStr);
                unitTypeStr = "OAU"; // This is changed for the ETL Label program to be able to recognize the unit type.
            }
            else if (unitTypeStr == "VKG")  // Use Rev 6 Etl table.
            {
                WriteRev6EtlOAUStickerData(dtBOM, jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNoStr);
            }
            else
            {
                WriteRev5EtlOAUStickerData(dtBOM, jobNumStr, orderNumStr, orderLineStr, releaseNumStr, modelNoStr);
                unitTypeStr = "OAU"; // This is changed for the ETL Label program to be able to recognize the unit type.
            }

            DataTable dtEtl = GetEtlJobHeadData(jobNumStr);

            if (dtEtl.Rows.Count > 0)
            {
                DeleteEtlJobHeadData(jobNumStr);
            }

            DataTable dtOrd = GetOrderHedData(Int32.Parse(orderNumStr));
            if (dtOrd.Rows.Count > 0)
            {
                DataRow drOrd = dtOrd.Rows[0];
                custNameStr = drOrd["Name"].ToString();
            }

            InsertEtlJobHeadData(jobNumStr, custNameStr, orderNumStr, orderLineStr, releaseNumStr,
                                 unitTypeStr.Substring(0, 3), custNameStr);
        }

        private void WriteRev5EtlOAUStickerData(DataTable dtBOM, string jobNumStr, string orderNumStr, string orderLineStr, string releaseNumStr, string modelNumStr)
        {
            string partNumStr;
            string parentPartNumStr = "";
            string partCategoryStr;
            string motorTypeStr;
            string voltageStr = "";
            string unitTypeStr;
            string compQtyStr = "";
            string compTypeStr = "";
            string compLRAStr = "";
            string compRLA_VoltsStr = "";
            string comp1RLA_VoltsStr = "";
            string comp2RLA_VoltsStr = "";
            string comp3RLA_VoltsStr = "";
            string comp4RLA_VoltsStr = "";
            string comp5RLA_VoltsStr = "";
            string comp6RLA_VoltsStr = "";
            string comp1QtyStr = "";
            string comp2QtyStr = "";
            string comp3QtyStr = "";
            string comp4QtyStr = "";
            string comp5QtyStr = "";
            string comp6QtyStr = "";
            string comp1PhaseStr = "";
            string comp2PhaseStr = "";
            string comp3PhaseStr = "";
            string comp4PhaseStr = "";
            string comp5PhaseStr = "";
            string comp6PhaseStr = "";
            string comp1LRAStr = "";
            string comp2LRAStr = "";
            string comp3LRAStr = "";
            string comp4LRAStr = "";
            string comp5LRAStr = "";
            string comp6LRAStr = "";
            string circuit1ChargeStr = "";
            string circuit2ChargeStr = "";
            string fanCondQtyStr = "";
            string fanCondPHStr = "";
            string fanCondFLAStr = "";
            string fanCondFLAVoltStr = "";
            string fanCondHPStr = "";
            string fanEvapQtyStr = "";
            string fanEvapPHStr = "";
            string fanEvapFLAStr = "";
            string fanEvapFLAVoltStr = "";
            string fanEvapHPStr = "";
            string fanERVQtyStr = "";
            string fanERVPHStr = "";
            string fanERVFLAStr = "";
            string fanERVFLAVoltStr = "";
            string fanERVHPStr = "";
            string fanPWRExhQtyStr = "";
            string fanPWRExhPHStr = "";
            string fanPWRExhFLAStr = "";
            string fanPWRExhFLAVoltStr = "";
            string fanPWRExhHPStr = "";
            string operatingPressureStr = "";
            string waterGlycol = "";
            string df_StaticPressureStr = "";
            string df_MaxHtgInputBTUHStr = "";
            string df_MinHeatingInputStr = "";
            string df_TempRiseStr = "";
            string df_MaxGasPressureStr = "";
            string df_MinGasPressureStr = "";
            string df_MaxPressureDropStr = "";
            string df_MinPressureDropStr = "";
            string df_ManifoldPressureStr = "";
            string df_CutOutTempStr = "";
            string if_MaxHtgInputBTUHStr = "";
            string if_HtgOutputBTUHStr = "";
            string if_MinInputBTUStr = "";
            string if_MaxEntStr = "";
            string if_TempRiseStr = "";
            string if_MaxGasPressureStr = "";
            string if_MinGasPressureStr = "";
            string if_ManifoldPressureStr = "";
            string maxOutletAirTempStr = "";
            string secHtgInputStr = "0";
            string preHtgInputStr = "0";
            string primaryHtgInputStr = "0";
            string minCKTAmpStr = "";
            string mfsMCBStr = "";
            string mcaStr = "0";
            string mopStr = "N/A";
            string elecRatingStr = "";
            string operatingVoltsStr = "";
            string testPresHighStr = "";
            string testPresLowStr = "";
            string mbhStr = "0";
            string qtyStr;
            string heatingInputElecStr = "0";
            string kwStr;
            string elecMcaStr = "0";
            string elecMopStr = "0";
            string elecRawMopStr = "0";
            string coolingMcaStr = "0";
            string coolingMopStr = "0";
            string coolingRawMopStr = "0";
            string fansOnlyMcaStr = "0";
            string fansOnlyFLAStr = "0";
            string fansOnlyRawMopStr = "0";
            string nonElecPrimNoSecFlaStr = "0";
            string nonElecPrimNoSecMcaStr = "0";
            string nonElecPrimNoSecMopStr = "0";
            string nonElecPrimWithSecFlaStr = "0";
            string nonElecPrimWithSecMcaStr = "0";
            string nonElecPrimWithSecMopStr = "0";
            string heatPumpNoSecFlaStr = "0";
            string heatPumpNoSecMcaStr = "0";
            string heatPumpNoSecMopStr = "0";
            string heatPumpWithSecFlaStr = "0";
            string heatPumpWithSecMcaStr = "0";
            string heatPumpWithSecMopStr = "0";
            string tmp_MinCKTAmpStr = "";
            string tmp_MFSMCBStr = "";
            string rawMopStr = "";
            string mfgDateStr = "";
            string currentPartTypeStr = "";
            string flowRateStr = "";
            string enteringTempStr = "";
            string heatTypeStr = "";
            string ruleHeadID = "";
            string ruleBatchID = "";
            string serialNoStr = "";
            string fuelType = "";
            string whseCode = "";
            string revisionNum = "";

            //todo: needs to be looked into
            string minCKTAmpStr2 = "";
            string mfsMCBStr2 = "";

            int voltageInt = 0;
            int evapFanCntInt = 0;
            int pwrExhFanCntInt = 0;
            int phaseInt = 0;
            int primKwInt = 0;
            int secKwInt = 0;

            decimal tempDec;
            decimal hertzDec = 0;

            double tempDbl;
            double primHtgInputElecDbl = 0;
            double secHtgInputElecDbl = 0;
            double preHtgInputElecDbl = 0;

            bool calcElecHeatMCA = false;
            bool heatPumpExist = false;
            bool airSrcHeatPumpExist = false;
            bool secHeatExist = false;
            bool secElecHeatExist = false;
            bool nonElecPrimNoSecHeat = false;
            bool nonElecPrimWithSecHeat = false;

            DataTable dtModel = new DataTable();

            DateTime mfgDateDate;

            Byte dualPointPower = 0;

            serialNoStr = "OA" + orderNumStr + "-" + orderLineStr + "-" + releaseNumStr;
            unitTypeStr = modelNumStr.Substring(2, 1);
            mfgDateStr = DateTime.Today.ToShortDateString();
            mfgDateDate = Convert.ToDateTime(mfgDateStr);

            switch (modelNumStr.Substring(8, 1)) //Voltage 
            {
                case "1":
                    voltageInt = 115;
                    hertzDec = 60;
                    phaseInt = 1;
                    elecRatingStr = "115/60/1";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "2":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "208/60/1";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "3":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "208/60/3";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "4":
                    voltageInt = 460;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "460/60/3";
                    operatingVoltsStr = "414 / 506";
                    break;
                case "5":
                    voltageInt = 575;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "575/60/3";
                    operatingVoltsStr = "517 / 633";
                    break;
                default:
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "208/60/3";
                    operatingVoltsStr = "187 / 253";
                    break;
            }

            voltageStr = voltageInt.ToString();
            testPresHighStr = "446";
            testPresLowStr = "238";

            if (unitTypeStr == "L" || unitTypeStr == "M" || unitTypeStr == "B" || unitTypeStr == "G")
            {
                unitTypeStr = "OALBG";
            }
            else
            {
                unitTypeStr = "OAU123DKN";
            }

            // Digit 36 Dual Point Power
            if (unitTypeStr == "OALBG")
            {
                if (modelNumStr.Substring(35, 1) == "E" || modelNumStr.Substring(35, 1) == "F")
                {
                    dualPointPower = 1;
                }
                else
                {
                    dualPointPower = 0;
                }
            }
            else
            {
                if (modelNumStr.Substring(35, 1) == "D" || modelNumStr.Substring(35, 1) == "E")
                {
                    dualPointPower = 1;
                }
                else
                {
                    dualPointPower = 0;
                }
            }

            if (modelNumStr.Substring(22, 1) != "0")
            {
                secHeatExist = true;
            }


            // If Heat Pump then account  for PreHeat in MCA & MOP
            if (modelNumStr.Substring(3, 1) == "E" || modelNumStr.Substring(3, 1) == "F")
            {
                heatPumpExist = true;
                preHtgInputStr = "0";

                if ((modelNumStr.Substring(3, 1) == "E") &&
                       (modelNumStr.Substring(13, 1) == "1" || modelNumStr.Substring(3, 1) == "2" || modelNumStr.Substring(3, 1) == "4"))
                {
                    airSrcHeatPumpExist = true;
                }

                if (modelNumStr.Substring(19, 1) == "G" || modelNumStr.Substring(19, 1) == "H" || modelNumStr.Substring(19, 1) == "L" ||
                    modelNumStr.Substring(19, 1) == "N" || modelNumStr.Substring(19, 1) == "Q" || modelNumStr.Substring(19, 1) == "S" ||
                    modelNumStr.Substring(19, 1) == "T" || modelNumStr.Substring(19, 1) == "U" || modelNumStr.Substring(19, 1) == "V" ||
                    modelNumStr.Substring(19, 1) == "W" || modelNumStr.Substring(19, 1) == "Y" || modelNumStr.Substring(19, 1) == "Z")
                {
                    secHeatExist = true;
                    secElecHeatExist = true;
                    dtModel = GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");
                        primKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        preHtgInputElecDbl = tempDbl;

                        heatingInputElecStr = preHtgInputElecDbl.ToString("f2");
                        calcElecHeatMCA = true;
                    }
                }
            }

            if (modelNumStr.Substring(19, 1) == "A" || modelNumStr.Substring(19, 1) == "E" ||
                modelNumStr.Substring(19, 1) == "G" || modelNumStr.Substring(19, 1) == "T")  // Indirect Fired (IF) & DuelFuel IF/????
            {
                try
                {
                    heatTypeStr = "Indirect";

                    dtModel = GetRev5ModelNoDigitDesc(22, modelNumStr.Substring(21, 1), "NA", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        mbhStr = drModel["DigitDescription"].ToString();
                        mbhStr = mbhStr.Substring(0, (mbhStr.IndexOf("MBH") - 1));
                        if_MaxHtgInputBTUHStr = mbhStr + "000";
                        tempDec = decimal.Parse(if_MaxHtgInputBTUHStr) * 0.8m;
                        if_HtgOutputBTUHStr = tempDec.ToString("f0");
                        if_MinInputBTUStr = GetMinInputBTU(mbhStr, modelNumStr.Substring(2, 1));
                    }

                    if_MaxEntStr = "2.0";
                    if_TempRiseStr = "10 TO 100 Deg F";
                    maxOutletAirTempStr = "125 Deg F";
                    if_MaxGasPressureStr = "14";
                    if (modelNumStr.Substring(20, 1) == "2") // if Fuel Type is Liquid Propane
                    {
                        if_MinGasPressureStr = "11";
                        if_ManifoldPressureStr = "10";
                    }
                    else
                    {
                        if_MinGasPressureStr = "7";
                        if_ManifoldPressureStr = "3.5";
                    }

                    if (voltageInt == 208 || voltageInt == 230 || voltageInt == 240)
                    {
                        primaryHtgInputStr = "6.25";
                    }
                    else if (voltageInt == 460)
                    {
                        primaryHtgInputStr = "3.13";
                    }
                    else if (voltageInt == 575)
                    {
                        primaryHtgInputStr = "2.5";
                    }

                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 22 - " + f);
                }

                try
                {
                    // Get fuel type
                    dtModel = GetRev5ModelNoDigitDesc(21, modelNumStr.Substring(20, 1), "NA", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];
                        fuelType = drModel["DigitDescription"].ToString(); // Stores fuel type
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 20 - " + f);
                }

                //try
                //{
                //    if (modelNumStr.Substring(19, 1) == "G" || modelNumStr.Substring(19, 1) == "T")
                //    {
                //        dtModel = GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);

                //        if (dtModel.Rows.Count > 0)
                //        {
                //            DataRow drModel = dtModel.Rows[0];
                //            kwStr = drModel["DigitDescription"].ToString();

                //            kwStr = stripKwFromDescription(kwStr);

                //            kwStr = kwStr + "000";
                //            tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                //            tempDbl = tempDbl / Math.Sqrt(3);
                //            secHtgInputElecDbl = tempDbl;

                //            secHtgInputStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                //        }
                //        else
                //        {
                //            secHtgInputStr = "0";
                //        }
                //    }
                //}
                //catch (Exception f)
                //{
                //    MessageBox.Show("Error Reading ModelNumDesc Table for digit 23 - " + f);
                //}
            }
            else if (modelNumStr.Substring(19, 1) == "C" || modelNumStr.Substring(19, 1) == "D") // Electric, Electric SCR 
            {
                //heatingInputElecStr = (((kW * 1000) / voltage) / square root of 3)
                heatTypeStr = "ELEC";
                calcElecHeatMCA = true;
                try
                {
                    dtModel = GetRev5ModelNoDigitDesc(22, modelNumStr.Substring(21, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");
                        primKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        heatingInputElecStr = tempDbl.ToString("f2");
                        primHtgInputElecDbl = tempDbl;
                        maxOutletAirTempStr = "90 Deg F";
                    }
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 22 - " + f);
                }
            }
            else if (modelNumStr.Substring(19, 1) == "B")  // Direct Fired
            {
                heatTypeStr = "Direct";

                df_TempRiseStr = "100 deg";
                df_MaxGasPressureStr = "14\"";
                df_MinHeatingInputStr = "22,000";

                df_MaxPressureDropStr = "";
                df_MinPressureDropStr = "";
                df_StaticPressureStr = "";
                df_CutOutTempStr = "";

                if (modelNumStr.Substring(20, 1) == "1") // Natural Gas
                {
                    df_MinGasPressureStr = "7\"";
                    df_ManifoldPressureStr = "3.5\"";
                }
                else   // Liquid Propane
                {
                    df_MinGasPressureStr = "10\"";
                    df_ManifoldPressureStr = "2\"";
                }

                if (modelNumStr.Substring(22, 1) == "A")
                {
                    df_MaxHtgInputBTUHStr = "330,000";
                }
                else if (modelNumStr.Substring(22, 1) == "B")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                }
                else if (modelNumStr.Substring(22, 1) == "C")
                {
                    df_MaxHtgInputBTUHStr = "600,000";
                }
                else if (modelNumStr.Substring(22, 1) == "D")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                    df_MinHeatingInputStr = "40,000";
                }
                else if (modelNumStr.Substring(22, 1) == "E")
                {
                    df_MaxHtgInputBTUHStr = "900,000";
                    df_MinHeatingInputStr = "40,000";
                }
            }
            else if (modelNumStr.Substring(19, 1) == "H" || modelNumStr.Substring(19, 1) == "N" ||
                     modelNumStr.Substring(19, 1) == "U" || modelNumStr.Substring(19, 1) == "W")  // Dual Heat Type Electric\Elctric
            {
                //heatingInputElecStr = (((kW * 1000) / voltage) / square root of 3)
                heatTypeStr = "ELEC";
                calcElecHeatMCA = true;
                secHeatExist = true;
                secElecHeatExist = true;
                nonElecPrimNoSecHeat = false;
                nonElecPrimWithSecHeat = false;

                try
                {
                    dtModel = GetRev5ModelNoDigitDesc(22, modelNumStr.Substring(21, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");
                        primKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        primHtgInputElecDbl = tempDbl;
                        maxOutletAirTempStr = "90 Deg F";
                    }

                    dtModel = GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");
                        secKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        secHtgInputElecDbl = tempDbl;
                        secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                    }

                    //heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");

                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 23 - " + f);
                }
            }
            else if (modelNumStr.Substring(19, 1) == "J" || modelNumStr.Substring(19, 1) == "P" ||
                     modelNumStr.Substring(19, 1) == "Q" || modelNumStr.Substring(19, 1) == "Y")  // Hot Water
            {
                heatTypeStr = "HotWater";
                flowRateStr = "??";
                enteringTempStr = "??";

                if (modelNumStr.Substring(19, 1) == "Q" || modelNumStr.Substring(19, 1) == "Y")
                {
                    secHeatExist = true;
                    secElecHeatExist = true;
                }
            }
            else if (modelNumStr.Substring(19, 1) == "K" || modelNumStr.Substring(19, 1) == "R" ||
                     modelNumStr.Substring(19, 1) == "S" || modelNumStr.Substring(19, 1) == "Z")  // Steam
            {
                heatTypeStr = "Steam";
            }

            if (heatTypeStr != "ELEC")
            {
                if (modelNumStr.Substring(22, 1) == "0")
                {
                    nonElecPrimNoSecHeat = true;
                }
                else
                {
                    secHeatExist = true;
                    secElecHeatExist = true;
                    calcElecHeatMCA = true;
                    nonElecPrimWithSecHeat = true;

                    dtModel = GetRev5ModelNoDigitDesc(23, modelNumStr.Substring(22, 1), "ELEC", unitTypeStr);

                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel;
                        drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");
                        secKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                        tempDbl = tempDbl / Math.Sqrt(3);
                        secHtgInputElecDbl = tempDbl;
                        secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                    }
                }
            }

            // Determine the number of Compressor for this unit and record the compressor data.              

            List<Compressor> CompList = new List<Compressor>();
            compQtyStr = "0";
            compTypeStr = "";

            int compNum = 0;
            string taco = "";

            foreach (DataRow drBOM in dtBOM.Rows)
            {              
                ruleHeadID = "0";                

                partCategoryStr = drBOM["PartCategory"].ToString();
                partNumStr = drBOM["PartNum"].ToString();
                revisionNum = drBOM["RevisionNum"].ToString();

                if (partCategoryStr != "")
                {
                    currentPartTypeStr = partCategoryStr;
                }

                motorTypeStr = drBOM["PartType"].ToString();

                if ((currentPartTypeStr == "Compressor") || (currentPartTypeStr.Contains("DigitalScroll")) || (currentPartTypeStr.Contains("TandemComp")))
                {
                    if (partNumStr.Contains("RFGASM") == true)
                    {
                        string rfgQtyStr = drBOM["ReqQty"].ToString();
                        string immedParent = partNumStr;

                        DataTable dtComp = GetPartBOM(partNumStr, revisionNum);

                        foreach (DataRow drComp in dtComp.Rows)
                        {
                            partNumStr = drComp["MtlPartNum"].ToString();
                            qtyStr = drComp["QtyPer"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                compQtyStr = qtyStr;
                            }

                            decimal compQtyPerDec = decimal.Parse(compQtyStr) * decimal.Parse(rfgQtyStr);
                            compQtyStr = compQtyPerDec.ToString("f0");
                            compTypeStr = currentPartTypeStr;
                            getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, "REV5", ruleHeadID);

                            if (decimal.Parse(compRLA_VoltsStr) > 0)
                            {
                                CompList.Add(new Compressor("", partNumStr, parentPartNumStr, immedParent, compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                            }
                        }
                    }
                    else
                    {
                        qtyStr = drBOM["ReqQty"].ToString();
                        if (qtyStr.IndexOf(".") > 0)
                        {
                            compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        }
                        else
                        {
                            compQtyStr = qtyStr;
                        }
                        compTypeStr = currentPartTypeStr;
                        getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, "REV5", ruleHeadID);

                        if (decimal.Parse(compRLA_VoltsStr) > 0)
                        {
                            CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                        }
                    }                         
                }
                else if (motorTypeStr == "Condensor")
                {
                    qtyStr = drBOM["ReqQty"].ToString();
                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanCondQtyStr = qtyStr;
                    }

                    getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanCondFLAStr, out fanCondHPStr, partNumStr);

                    if (fanCondFLAStr != "0")
                    {
                        fanCondFLAVoltStr = fanCondFLAStr + "-" + voltageStr;
                        fanCondPHStr = phaseInt.ToString();
                    }
                }
                else if (motorTypeStr == "Indoor FAN" || motorTypeStr == "SupplyFan")
                {
                    qtyStr = drBOM["ReqQty"].ToString();
                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanEvapQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanEvapQtyStr = qtyStr;
                    }

                    getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanEvapFLAStr, out fanEvapHPStr, partNumStr);

                    if (fanEvapFLAStr != "0")
                    {
                        fanEvapFLAVoltStr = fanEvapFLAStr + "-" + voltageStr;
                        fanEvapPHStr = phaseInt.ToString();
                    }

                    ++evapFanCntInt;
                }
                else if (motorTypeStr == "Powered Exhaust" || motorTypeStr == "PoweredExhaust")
                {
                    qtyStr = drBOM["ReqQty"].ToString();
                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanPWRExhQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanPWRExhQtyStr = qtyStr;
                    }

                    getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanPWRExhFLAStr, out fanPWRExhHPStr, partNumStr);

                    if (fanPWRExhFLAStr != "0")
                    {
                        fanPWRExhFLAVoltStr = fanPWRExhFLAStr + "-" + voltageStr;
                        fanPWRExhPHStr = phaseInt.ToString();
                    }

                    ++pwrExhFanCntInt;
                }
                else if (motorTypeStr == "ERV")
                {
                    qtyStr = drBOM["ReqQty"].ToString();

                    if (qtyStr.IndexOf(".") > 0)
                    {
                        fanERVQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                    }
                    else
                    {
                        fanERVQtyStr = qtyStr;
                    }

                    getFLA_Data(Int32.Parse(ruleHeadID), voltageStr, out fanERVFLAStr, out fanERVHPStr, partNumStr);

                    if (fanERVFLAStr != "0")
                    {
                        fanERVFLAVoltStr = fanERVFLAStr + "-" + voltageStr;
                        fanERVPHStr = phaseInt.ToString();
                    }
                }               
            }

            mcaStr = "0";
            rawMopStr = "NA";

            calcAmpacityValuesFansOnly(out fansOnlyFLAStr, out fansOnlyMcaStr, out fansOnlyRawMopStr, fanEvapFLAStr,
                                               fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr,
                                               voltageInt.ToString());

            if (nonElecPrimNoSecHeat == true) // If the unit has non electric primary heat and no secondary heat then add the additional values to MCA & MOP
            {                                 // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                nonElecPrimNoSecMcaStr = (double.Parse(fansOnlyMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                nonElecPrimNoSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            }
            else if (nonElecPrimWithSecHeat == true)  // If the unit has non electric primart heat and has secondary elec heat then add the additional values to MCA & MOP
            {                                         // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                if (secKwInt >= 50)
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + secHtgInputElecDbl).ToString("f1");
                }
                else
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + (secHtgInputElecDbl * 1.25)).ToString("f1");
                }
                nonElecPrimWithSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                //nonElecPrimWithSecMopStr = ((1.25 * (double.Parse(fansOnlyRawMopStr)) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl)).ToString("f2");
            }

            if (modelNumStr.Substring(4, 3) != "000") // if the unit has cooling then calculate the cooling MCA & MOP
            {                                    // B. DX Cool (Horizon & Viking)
                calcAmpacityValues(out coolingMcaStr, out coolingMopStr, out coolingRawMopStr, CompList, fanCondFLAStr,
                                           fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr,
                                           fanPWRExhFLAStr, fanPWRExhQtyStr, voltageInt.ToString());
            }

            if (heatPumpExist == true) // if the unit has a heat pump and has secondary electric heating then add the additional values to MCA & MOP
            {                          // E. DX Heat (Air or Water Source Heat Pump) Normal Operation (Horizon)
                if (airSrcHeatPumpExist == true)  // F.	DX Heat (Air Source Heat Pump) Defrost Mode Operation (Horizon)
                {
                    if (nonElecPrimWithSecHeat == true) // If the unit has non electric primary heat and has secondary heat then add the additional values to MCA & MOP
                    {
                        if (secElecHeatExist == false) //Mode F-a
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                        }
                        else // Secondary heat exist   Mode F-b
                        {
                            if (secKwInt >= 50)
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                            }
                            else
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                            }
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                        }
                    }
                    else if (heatTypeStr == "ELEC") // Mode F-c Elec primary heat; no secondary elec heat
                    {
                        if (primKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString();
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * double.Parse(primaryHtgInputStr))).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                    }
                }
                else
                {
                    if (secElecHeatExist == true) //Mode E-a
                    {
                        if (secKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + secHtgInputElecDbl).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + secHtgInputElecDbl).ToString("f1");
                    }
                    // If no secondary elec heat then just use the coolingMcaStr & coolingRawMopStr values
                    // Mode E-b
                }
                // Else if no secondary elec heat the coolingMcaStr doesn't change from what was calculate earlier.
            }

            if (calcElecHeatMCA == true) // if the unit contains some type of electric heating
            {                            // C.	Primary Heat: Electric (Viking and Horizon)
                calcElecAmpacityValues(out elecMcaStr, out elecRawMopStr, primKwInt, secKwInt, fanEvapFLAStr,
                                               fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr,
                                               voltageInt.ToString(), primHtgInputElecDbl, secHtgInputElecDbl);
            }

            // determine which MCA value is the largest 
            if (double.Parse(fansOnlyMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(elecMcaStr) &&
                double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = fansOnlyMcaStr;
            }
            else if (double.Parse(coolingMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = coolingMcaStr;
            }
            else if (double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = nonElecPrimNoSecMcaStr;
            }
            else if (double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr))
            {
                mcaStr = nonElecPrimWithSecMcaStr;
            }
            else
            {
                mcaStr = elecMcaStr;
            }

            // determine which MOP value is the largest 
            if (double.Parse(fansOnlyRawMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(elecRawMopStr) &&
               double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = fansOnlyRawMopStr;
            }
            else if (double.Parse(coolingRawMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = coolingRawMopStr;
            }
            else if (double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = nonElecPrimNoSecMopStr;
            }
            else if (double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(nonElecPrimNoSecMopStr))
            {
                rawMopStr = nonElecPrimWithSecMopStr;
            }
            else
            {
                rawMopStr = elecRawMopStr;
            }

            // Do the rounding for both electrical MCA & non electrical MCA
            if (mcaStr != "0")
            {
                elecMopStr = roundElecMOP(double.Parse(mcaStr)); // Electrical MCA rounding
            }
            else
            {
                elecMopStr = "0";
            }
            mopStr = roundMOP(double.Parse(rawMopStr)); // Non-Electrical MCA rounding

            if (double.Parse(elecMopStr) > double.Parse(mopStr)) // Use the larger of the 2 values
            {
                mopStr = elecMopStr;
            }           

            string digit3 = modelNumStr.Substring(2, 1);
            string digit4 = modelNumStr.Substring(3, 1);
            string digit567 = modelNumStr.Substring(4, 3);
            string digit9 = modelNumStr.Substring(8, 1);
            string digit11 = modelNumStr.Substring(10, 1);
            string digit12 = modelNumStr.Substring(11, 1);
            string digit13 = modelNumStr.Substring(12, 1);
            string digit14 = modelNumStr.Substring(13, 1);

            DataTable dtCCD = GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow dr = dtCCD.Rows[0];

                comp1QtyStr = "1";
                comp1RLA_VoltsStr = dr["Comp1_1_RLA"].ToString() + "-" + voltageStr;
                comp1LRAStr = dr["Comp1_1_LRA"].ToString();
                comp1PhaseStr = "3";

                if (dr["Circuit1_2_Compressor"].ToString() != "NA")
                {
                    comp2QtyStr = "1";
                    comp2RLA_VoltsStr = dr["Comp1_2_RLA"].ToString() + "-" + voltageStr;
                    comp2LRAStr = dr["Comp1_2_LRA"].ToString();
                    comp2PhaseStr = "3";
                }

                if (dr["Circuit2_1_Compressor"].ToString() != "NA")
                {
                    comp4QtyStr = "1";
                    comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    comp4LRAStr = dr["Comp2_1_LRA"].ToString();
                    comp4PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp2QtyStr = "1";
                    //    comp2RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp2LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp2PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                }

                if (dr["Circuit2_2_Compressor"].ToString() != "NA")
                {
                    comp5QtyStr = "1";
                    comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    comp5LRAStr = dr["Comp2_2_LRA"].ToString();
                    comp5PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                }

                if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = dr["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(dr["Circuit1_Charge"].ToString()) + Decimal.Parse(dr["Circuit1_ReheatCharge"].ToString())).ToString();
                }

                if (dr["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = dr["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(dr["Circuit2_Charge"].ToString()) + Decimal.Parse(dr["Circuit2_ReheatCharge"].ToString())).ToString();
                    }
                }
            }            

            JobNum = jobNumStr;
            OrderNum = Int32.Parse(orderNumStr);
            OrderLine = Int32.Parse(orderLineStr);
            ReleaseNum = Int32.Parse(releaseNumStr);
            ModelNo = modelNumStr;
            SerialNo = serialNoStr;
            MfgDate = mfgDateStr;
            UnitType = unitTypeStr;
            ElecRating = elecRatingStr;
            OperVoltage = operatingVoltsStr;
            DualPointPower = dualPointPower;
            MinCKTAmp = mcaStr;
            MinCKTAmp2 = minCKTAmpStr2;
            MfsMcb = mopStr;
            MfsMcb2 = mfsMCBStr2;
            MOP = rawMopStr;
            HeatingType = heatTypeStr;
            Voltage = voltageStr;
            TestPressureHigh = testPresHighStr;
            TestPressureLow = testPresLowStr;
            SecondaryHtgInput = secHtgInputStr;
            HeatingInputElectric = heatingInputElecStr;
            FuelType = fuelType;
            Comp1Qty = comp1QtyStr;
            Comp2Qty = comp2QtyStr;
            Comp3Qty = comp3QtyStr;
            Comp4Qty = comp4QtyStr;
            Comp5Qty = comp5QtyStr;
            Comp6Qty = comp6QtyStr;
            Comp1Phase = comp1PhaseStr;
            Comp2Phase = comp2PhaseStr;
            Comp3Phase = comp3PhaseStr;
            Comp4Phase = comp4PhaseStr;
            Comp5Phase = comp5PhaseStr;
            Comp6Phase = comp6PhaseStr;
            Comp1RLA_Volts = comp1RLA_VoltsStr;
            Comp2RLA_Volts = comp2RLA_VoltsStr;
            Comp3RLA_Volts = comp3RLA_VoltsStr;
            Comp4RLA_Volts = comp4RLA_VoltsStr;
            Comp5RLA_Volts = comp5RLA_VoltsStr;
            Comp6RLA_Volts = comp6RLA_VoltsStr;
            Comp1LRA = comp1LRAStr;
            Comp2LRA = comp2LRAStr;
            Comp3LRA = comp3LRAStr;
            Comp4LRA = comp4LRAStr;
            Comp5LRA = comp5LRAStr;
            Comp6LRA = comp6LRAStr;
            Circuit1Charge = circuit1ChargeStr;
            Circuit2Charge = circuit2ChargeStr;
            FanCondQty = fanCondQtyStr;
            FanEvapQty = fanEvapQtyStr;
            FanErvQty = fanERVQtyStr;
            FanPwrExhQty = fanPWRExhQtyStr;
            FanCondPhase = fanCondPHStr;
            FanEvapPhase = fanEvapPHStr;
            FanErvPhase = fanERVPHStr;
            FanPwrExhPhase = fanPWRExhPHStr;
            FanCondFLA = fanCondFLAVoltStr;
            FanEvapFLA = fanEvapFLAVoltStr;
            FanErvFLA = fanERVFLAVoltStr;
            FanPwrExhFLA = fanPWRExhFLAVoltStr;
            FanCondHP = fanCondHPStr;
            FanEvapHP = fanEvapHPStr;
            FanErvHP = fanERVHPStr;
            FanPwrExhHP = fanPWRExhHPStr;
            FlowRate = flowRateStr;
            EnteringTemp = enteringTempStr;
            OperatingPressure = operatingPressureStr;
            WaterGlycol = waterGlycol;
            DF_StaticPressure = df_StaticPressureStr;
            DF_MaxHtgInputBTUH = df_MaxHtgInputBTUHStr;
            DF_MinHeatingInput = df_MinHeatingInputStr;
            DF_MinPressureDrop = df_MinPressureDropStr;
            DF_MaxPressureDrop = df_MaxPressureDropStr;
            DF_ManifoldPressure = df_ManifoldPressureStr;
            DF_CutoutTemp = df_CutOutTempStr;
            DF_MinGasPressure = df_MinGasPressureStr;
            DF_MaxGasPressure = df_MaxGasPressureStr;
            DF_TempRise = df_TempRiseStr;
            IN_MaxHtgInputBTUH = if_MaxHtgInputBTUHStr;
            IN_HeatingOutputBTUH = if_HtgOutputBTUHStr;
            IN_MinInputBTU = if_MinInputBTUStr;
            IN_MaxExt = if_MaxEntStr;
            IN_TempRise = if_TempRiseStr;
            IN_MaxOutAirTemp = maxOutletAirTempStr;
            IN_MaxGasPressure = if_MaxGasPressureStr;
            IN_MinGasPressure = if_MinGasPressureStr;
            IN_ManifoldPressure = if_ManifoldPressureStr;
            ModelNoVerifiedBy = "OAU_Config";
            French = "French";
            WhseCode = "LWH5";

            try
            {
                InsertETL_Oau();
            }
            catch (Exception f)
            {
                MessageBox.Show("Error writing to the R6_OA_EtlOAU Table - " + f);
            }
        }


        private void WriteRev6EtlOAUStickerData(DataTable dtBOM,string jobNumStr, string orderNumStr, string orderLineStr, 
                                                string releaseNumStr, string modelNumStr)
        {
            string partNumStr;
            string parentPartNumStr;
            string partCategoryStr;
            string motorTypeStr;
            string voltageStr = "";
            string unitTypeStr;
            string compQtyStr = "";
            string compTypeStr = "";
            string compLRAStr = "";
            string compRLA_VoltsStr = "";
            string comp1RLA_VoltsStr = "";
            string comp2RLA_VoltsStr = "";
            string comp3RLA_VoltsStr = "";
            string comp4RLA_VoltsStr = "";
            string comp5RLA_VoltsStr = "";
            string comp6RLA_VoltsStr = "";
            string comp1QtyStr = "";
            string comp2QtyStr = "";
            string comp3QtyStr = "";
            string comp4QtyStr = "";
            string comp5QtyStr = "";
            string comp6QtyStr = "";
            string comp1PhaseStr = "";
            string comp2PhaseStr = "";
            string comp3PhaseStr = "";
            string comp4PhaseStr = "";
            string comp5PhaseStr = "";
            string comp6PhaseStr = "";
            string comp1LRAStr = "";
            string comp2LRAStr = "";
            string comp3LRAStr = "";
            string comp4LRAStr = "";
            string comp5LRAStr = "";
            string comp6LRAStr = "";
            string circuit1ChargeStr = "";
            string circuit2ChargeStr = "";
            string fanCondQtyStr = "0";
            string fanCondPHStr = "";
            string fanCondFLAStr = "0";
            string fanCondFLAVoltStr = "";
            string fanCondHPStr = "";
            string fanEvapQtyStr = "0";
            string fanEvapPHStr = "";
            string fanEvapFLAStr = "0";
            string fanEvapFLAVoltStr = "";
            string fanEvapHPStr = "";
            string fanERVQtyStr = "0";
            string fanERVPHStr = "";
            string fanERVFLAStr = "0";
            string fanERVFLAVoltStr = "";
            string fanERVHPStr = "";
            string fanPWRExhQtyStr = "0";
            string fanPWRExhPHStr = "";
            string fanPWRExhFLAStr = "0";
            string fanPWRExhFLAVoltStr = "";
            string fanPWRExhHPStr = "";
            string operatingPressureStr = "";
            string waterGlycol = "";
            string df_StaticPressureStr = "";
            string df_MaxHtgInputBTUHStr = "";
            string df_MinHeatingInputStr = "";
            string df_TempRiseStr = "";
            string df_MaxGasPressureStr = "";
            string df_MinGasPressureStr = "";
            string df_MaxPressureDropStr = "";
            string df_MinPressureDropStr = "";
            string df_ManifoldPressureStr = "";
            string df_CutOutTempStr = "";
            string if_MaxHtgInputBTUHStr = "";
            string if_HtgOutputBTUHStr = "";
            string if_MinInputBTUStr = "";
            string if_MaxEntStr = "";
            string if_TempRiseStr = "";
            string if_MaxGasPressureStr = "";
            string if_MinGasPressureStr = "";
            string if_ManifoldPressureStr = "";
            string maxOutletAirTempStr = "";
            string secHtgInputStr = "";
            string elecMcaStr = "0";
            string elecMopStr = "0";
            string elecRawMopStr = "0";
            string coolingMcaStr = "0";
            string coolingMopStr = "0";
            string coolingRawMopStr = "0";
            string fansOnlyMcaStr = "0";
            string fansOnlyFLAStr = "0";
            string fansOnlyRawMopStr = "0";
            string nonElecPrimNoSecFlaStr = "0";
            string nonElecPrimNoSecMcaStr = "0";
            string nonElecPrimNoSecMopStr = "0";
            string nonElecPrimWithSecFlaStr = "0";
            string nonElecPrimWithSecMcaStr = "0";
            string nonElecPrimWithSecMopStr = "0";
            string heatPumpNoSecFlaStr = "0";
            string heatPumpNoSecMcaStr = "0";
            string heatPumpNoSecMopStr = "0";
            string heatPumpWithSecFlaStr = "0";
            string heatPumpWithSecMcaStr = "0";
            string heatPumpWithSecMopStr = "0";
            string mcaStr = "0";
            string minCKTAmpStr = "";
            string mfsMCBStr = "";
            string mopStr = "N/A";
            string elecRatingStr = "";
            string operatingVoltsStr = "";
            string testPresHighStr = "";
            string testPresLowStr = "";
            string mbhStr = "0";
            string qtyStr;
            string heatingInputElecStr = "0";
            string kwStr = "0";
            string tmp_MinCKTAmpStr = "";
            string tmp_MFSMCBStr = "";
            string rawMopStr = "";
            string mfgDateStr = "";
            string currentPartTypeStr = "";
            string flowRateStr = "";
            string enteringTempStr = "";
            string heatTypeStr = "";
            string ruleHeadID = "";
            string ruleBatchID = "";
            string serialNoStr = "";
            string fuelType = "";
            string primaryHtgInputStr = "0";
            string revisionNum = "";
            string revType = "REV6";

            //todo: needs to be looked into
            string minCKTAmpStr2 = "";
            string mfsMCBStr2 = "";


            int voltageInt = 0;
            int evapFanCntInt = 0;
            int pwrExhFanCntInt = 0;
            int phaseInt = 0;
            int primKwInt = 0;
            int secKwInt = 0;

            decimal tempDec;
            decimal hertzDec = 0;

            double tempDbl;
            double primHtgInputElecDbl = 0;
            double secHtgInputElecDbl = 0;

            bool calcElecHeatMCA = false;
            //bool calcCoolMCA = false;
            bool heatPumpExist = false;
            bool secHeatExist = false;
            bool airSrcHeatPumpExist = false;
            bool secElecHeatExist = false;
            bool nonElecPrimNoSecHeat = false;
            bool nonElecPrimWithSecHeat = false;

            DateTime mfgDateDate;

            Byte dualPointPower = 0;

            serialNoStr = "OA" + orderNumStr + "-" + orderLineStr + "-" + releaseNumStr;

            unitTypeStr = modelNumStr.Substring(0, 3);

            if (unitTypeStr.StartsWith("OA") == true)
            {
                revType = "REV6";
            }
            else
            {
                revType = "VKG";
            }

            mfgDateStr = DateTime.Today.ToShortDateString();
            mfgDateDate = Convert.ToDateTime(mfgDateStr);

            switch (modelNumStr.Substring(8, 1)) //Voltage 
            {
                case "1":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "208-230/60/3";
                    operatingVoltsStr = "187 / 253";
                    break;
                case "2":
                    voltageInt = 230;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "230-240/60/3";
                    break;
                case "3":
                    voltageInt = 460;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "460/60/3";
                    operatingVoltsStr = "414 / 506";
                    break;
                case "4":
                    voltageInt = 575;
                    hertzDec = 60;
                    phaseInt = 3;
                    elecRatingStr = "575/60/3";
                    operatingVoltsStr = "517 / 633";
                    break;
                case "5":
                    voltageInt = 115;
                    hertzDec = 60;
                    phaseInt = 1;
                    break;
                case "6":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 1;
                    break;
                case "7":
                    voltageInt = 208;
                    hertzDec = 50;
                    phaseInt = 3;
                    break;
                case "8":
                    voltageInt = 380;
                    hertzDec = 50;
                    phaseInt = 3;
                    break;
                case "9":
                    voltageInt = 208;
                    hertzDec = 50;
                    phaseInt = 1;
                    break;
                default:
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
            }

            voltageStr = voltageInt.ToString();
            testPresHighStr = "446";
            testPresLowStr = "238";

            // If Digit 41 equal G, H, or J then unit has Dual Point Power
            if (modelNumStr.Substring(40, 1) == "G" || modelNumStr.Substring(40, 1) == "H" || modelNumStr.Substring(40, 1) == "J")
            {
                dualPointPower = 1;
            }
            else
            {
                dualPointPower = 0;
            }

            if (modelNumStr.Substring(15, 1) == "A" || modelNumStr.Substring(15, 1) == "B" ||
                modelNumStr.Substring(15, 1) == "C" || modelNumStr.Substring(15, 1) == "D" ||
                modelNumStr.Substring(15, 1) == "E" || modelNumStr.Substring(15, 1) == "F" ||
                modelNumStr.Substring(15, 1) == "L" || modelNumStr.Substring(15, 1) == "M")  // Indirect Fired (IF)
            {
                if (voltageInt == 208 || voltageInt == 230 || voltageInt == 240)
                {
                    primaryHtgInputStr = "6.25";
                }
                else if (voltageInt == 460)
                {
                    primaryHtgInputStr = "3.13";
                }
                else if (voltageInt == 575)
                {
                    primaryHtgInputStr = "2.5";
                }

                try
                {
                    if (modelNumStr.Substring(15, 1) == "A" || modelNumStr.Substring(15, 1) == "B" || modelNumStr.Substring(15, 1) == "C" || modelNumStr.Substring(15, 1) == "L")
                    {
                        fuelType = "NaturalGas";
                        if_MinGasPressureStr = "7";
                        if_ManifoldPressureStr = "3.5";
                        fuelType = "Natural Gas"; // Stores fuel type
                    }
                    else
                    {
                        fuelType = "LiquidPropane";
                        if_MinGasPressureStr = "11";
                        if_ManifoldPressureStr = "10";
                        fuelType = "Liquid Propane"; // Stores fuel type
                    }
                    heatTypeStr = "Indirect";

                    if (unitTypeStr.StartsWith("OA") == true)
                    {
                        DataTable dtModel = GetModelNumDigitDesc(17, "IF", modelNumStr.Substring(16, 1), "REV6");

                        if (dtModel.Rows.Count > 0) // Calculate MBH 
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            mbhStr = drModel["DigitDescription"].ToString();
                            mbhStr = mbhStr.Substring(0, (mbhStr.IndexOf("MBH") - 1));
                            if_MaxHtgInputBTUHStr = mbhStr + "000";
                            tempDec = decimal.Parse(if_MaxHtgInputBTUHStr) * 0.8m;
                            if_HtgOutputBTUHStr = tempDec.ToString("f0");
                            if_MinInputBTUStr = GetMinInputBTU(mbhStr, modelNumStr.Substring(2, 1));
                            if_TempRiseStr = "10 TO 100 Deg F";
                        }
                    }
                    else
                    {
                        DataTable dtModel = GetModelNumDigitDesc(17, "NA", modelNumStr.Substring(16, 1), "VKG");

                        if (dtModel.Rows.Count > 0) // Calculate MBH 
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            mbhStr = drModel["DigitDescription"].ToString();
                            mbhStr = mbhStr.Substring(0, (mbhStr.IndexOf("MBH") - 1));
                            if_MaxHtgInputBTUHStr = mbhStr + "000";
                            tempDec = decimal.Parse(if_MaxHtgInputBTUHStr) * 0.8m;
                            if_HtgOutputBTUHStr = tempDec.ToString("f0");
                            if_MinInputBTUStr = GetMinInputBTU(mbhStr, modelNumStr.Substring(2, 1));
                        }
                        if_TempRiseStr = "10 TO 85 Deg F";
                        if (modelNumStr.Substring(15, 1) == "L")
                        {
                            if_TempRiseStr = "10 TO 50 Deg F";
                            if (mbhStr == "200")
                            {
                                if_MaxHtgInputBTUHStr = "162,000";
                                if_HtgOutputBTUHStr = "110,000";
                            }
                            else if (mbhStr == "300")
                            {
                                if_MaxHtgInputBTUHStr = "243,000";
                                if_HtgOutputBTUHStr = "165,000";
                            }
                            else if (mbhStr == "400")
                            {
                                if_MaxHtgInputBTUHStr = "324,000";
                                if_HtgOutputBTUHStr = "220,000";
                            }
                        }
                    }

                    if_MaxEntStr = "2.0";
                    maxOutletAirTempStr = "125 Deg F";
                    if_MaxGasPressureStr = "14";                   
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 17 - " + f);
                }               
            }
            //else if (modelNumStr.Substring(15, 1) == "H" || modelNumStr.Substring(15, 1) == "J" ) // If Digit 16 (Primary Heat Type) Equals Electric-Staged or Electric-SCR Modulating 
            //{
            //    //heatingInputElecStr = (((kW * 1000) / voltage) / square root of 3)
            //    heatTypeStr = "Electric";
            //    try
            //    {
            //        DataTable dtModel = objMain.GetModelNumDesc(17, "ELEC", modelNumStr.Substring(16, 1));

            //        if (dtModel.Rows.Count > 0) // Calculate MBH 
            //        {
            //            DataRow drModel;
            //            drModel = dtModel.Rows[0];

            //            kwStr = drModel["DigitDescription"].ToString();
            //            kwStr = objMain.stripKwFromDescription(kwStr);

            //            kwStr = kwStr + "000";
            //            tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
            //            tempDbl = tempDbl / Math.Sqrt(3);
            //            heatingInputElecStr = tempDbl.ToString("f2");
            //            maxOutletAirTempStr = "90 Deg F";
            //        }
            //    }
            //    catch (Exception f)
            //    {
            //        MessageBox.Show("Error Reading ModelNumDesc Table for digit 17 - " + f);
            //    }
            //}
            else if ((modelNumStr.Substring(15, 1) == "0") &&
                     (modelNumStr.Substring(17, 1) == "1" || modelNumStr.Substring(17, 1) == "2")) // Direct Fired
            {
                heatTypeStr = "Direct";

                df_TempRiseStr = "100 deg";
                df_MaxGasPressureStr = "14\"";
                df_MinHeatingInputStr = "22,000";

                df_MaxPressureDropStr = "";
                df_MinPressureDropStr = "";
                df_StaticPressureStr = "";
                df_CutOutTempStr = "";

                if (modelNumStr.Substring(20, 1) == "1") // Natural Gas
                {
                    df_MinGasPressureStr = "7\"";
                    df_ManifoldPressureStr = "3.5\"";
                }
                else   // Liquid Propane
                {
                    df_MinGasPressureStr = "10\"";
                    df_ManifoldPressureStr = "2\"";
                }

                if (modelNumStr.Substring(22, 1) == "A")
                {
                    df_MaxHtgInputBTUHStr = "330,000";
                }
                else if (modelNumStr.Substring(22, 1) == "B")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                }
                else if (modelNumStr.Substring(22, 1) == "C")
                {
                    df_MaxHtgInputBTUHStr = "600,000";
                }
                else if (modelNumStr.Substring(22, 1) == "D")
                {
                    df_MaxHtgInputBTUHStr = "400,000";
                    df_MinHeatingInputStr = "40,000";
                }
                else if (modelNumStr.Substring(22, 1) == "E")
                {
                    df_MaxHtgInputBTUHStr = "900,000";
                    df_MinHeatingInputStr = "40,000";
                }
            }
            else if (modelNumStr.Substring(15, 1) == "H" || modelNumStr.Substring(15, 1) == "J")  // Electric
            {
                heatTypeStr = "Electric";
                calcElecHeatMCA = true;
                try
                {
                    DataTable dtModel = new DataTable();
                    if (revType == "REV6")
                    {
                        dtModel = GetModelNumDigitDesc(17, "ELEC", modelNumStr.Substring(16, 1), "REV6"); // OA uses digit 17
                    }
                    else
                    {
                        dtModel = GetModelNumDigitDesc(65, "NA", modelNumStr.Substring(64, 1), "VKG"); // Viking uses digit 65
                    }

                    if (dtModel.Rows.Count > 0) // Calculate Heating electric input value.
                    {
                        DataRow drModel;
                        drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, revType);
                        primKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                        tempDbl = tempDbl / Math.Sqrt(3);
                        primHtgInputElecDbl = tempDbl;
                        heatingInputElecStr = tempDbl.ToString("f1");
                        maxOutletAirTempStr = "90 Deg F";
                    }

                    //if (modelNumStr.Substring(17, 1) == "4" || modelNumStr.Substring(17, 1) == "5")
                    //{
                    //    secHeatExist = true;
                    //    secElecHeatExist = true;
                    //    dtModel = objMain.GetModelNumDigitDesc(19, "ELEC", modelNumStr.Substring(18, 1));
                    //    if (dtModel.Rows.Count > 0)
                    //    {
                    //        DataRow drModel;
                    //        drModel = dtModel.Rows[0];

                    //        kwStr = drModel["DigitDescription"].ToString();
                    //        kwStr = objMain.stripKwFromDescription(kwStr);

                    //        kwStr = kwStr + "000";
                    //        tempDbl = double.Parse(kwStr) / double.Parse(voltageStr);
                    //        tempDbl = tempDbl / Math.Sqrt(3);
                    //        secHtgInputElecDbl = tempDbl;
                    //        secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                    //    }
                    //}
                    //heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                }
                catch (Exception f)
                {
                    MessageBox.Show("Error Reading ModelNumDesc Table for digit 19 - " + f);
                }
            }
            else if (modelNumStr.Substring(15, 1) == "G")  // Hot Water
            {
                heatTypeStr = "HotWater";
                flowRateStr = "??";
                enteringTempStr = "??";
            }
            else if (modelNumStr.Substring(15, 1) == "K")  // Steam
            {
                heatTypeStr = "Steam";
            }

            if (heatTypeStr != "Electric")  // Potential bug - always assumes secondary heat type is electric
            {
                if (modelNumStr.Substring(17, 1) == "4" || modelNumStr.Substring(17, 1) == "5")
                {
                    nonElecPrimWithSecHeat = true;
                }
                else if (modelNumStr.Substring(17, 1) == "0")
                {
                    nonElecPrimNoSecHeat = true;
                }
            }

            if (modelNumStr.Substring(13, 1) == "5" || modelNumStr.Substring(13, 1) == "6" ||
                modelNumStr.Substring(13, 1) == "7" || modelNumStr.Substring(13, 1) == "8")
            {
                heatPumpExist = true;

                if (modelNumStr.Substring(13, 1) == "5" || modelNumStr.Substring(13, 1) == "6")
                {
                    airSrcHeatPumpExist = true;
                }
            }

            if (modelNumStr.Substring(17, 1) != "0")
            {
                secHeatExist = true;
                if (modelNumStr.Substring(17, 1) == "4" || modelNumStr.Substring(17, 1) == "5")
                {
                    secElecHeatExist = true;

                    DataTable dtModel = GetModelNumDigitDesc(19, "ELEC", modelNumStr.Substring(18, 1), revType);
                    if (dtModel.Rows.Count > 0)
                    {
                        DataRow drModel;
                        drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, revType);
                        secKwInt = Int32.Parse(kwStr);

                        kwStr = kwStr + "000";

                        tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                        tempDbl = tempDbl / Math.Sqrt(3);
                        secHtgInputElecDbl = tempDbl;
                        secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                    }
                }
                heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                calcElecHeatMCA = true;
            }

            // Determine the number of Compressor for this unit and record the compressor data.              

            List<Compressor> CompList = new List<Compressor>();

            foreach (DataRow drBOM in dtBOM.Rows)
            {
                ruleHeadID = "0";
                ruleBatchID = drBOM["RuleBatchID"].ToString();
                partCategoryStr = drBOM["PartCategory"].ToString();
                partNumStr = drBOM["PartNum"].ToString();
                parentPartNumStr = drBOM["ParentPartNum"].ToString();
                revisionNum = drBOM["RevisionNum"].ToString();

                if (ruleHeadID == "") // Safeguard in case RuleHeadID is null or blank
                {
                    ruleHeadID = "0";
                }

                if (ruleBatchID == "")  // Safeguard in case RuleBatchID is null or blank
                {
                    ruleBatchID = "0";
                }

                //if (partNumStr.StartsWith("VELMTR") == true)
                //{
                //    string taco = "bell";
                //}

                if (partCategoryStr != "")
                {
                    currentPartTypeStr = partCategoryStr;
                }

                motorTypeStr = drBOM["PartType"].ToString();

                if ((currentPartTypeStr == "Compressor") || (currentPartTypeStr.Contains("DigitalScroll")) || (currentPartTypeStr.Contains("TandemComp")))
                {
                    if (partNumStr.Contains("RFGASM") == true)
                    {
                        string immedParent = partNumStr;
                        DataTable dtComp = GetPartBOM(partNumStr, revisionNum);

                        foreach (DataRow drComp in dtComp.Rows)
                        {
                            partNumStr = drComp["MtlPartNum"].ToString();
                            qtyStr = drComp["QtyPer"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                compQtyStr = qtyStr;
                            }
                            compTypeStr = partCategoryStr;
                            getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, "REV6", ruleHeadID);

                            if (decimal.Parse(compRLA_VoltsStr) > 0)
                            {
                                CompList.Add(new Compressor("", partNumStr, parentPartNumStr, immedParent, compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                            }
                        }
                    }
                    else
                    {
                        getCompRLA_Data(partNumStr, voltageStr, out compRLA_VoltsStr, out compLRAStr, "REV6", "");

                        if (decimal.Parse(compRLA_VoltsStr) > 0)
                        {
                            parentPartNumStr = drBOM["ParentPartNum"].ToString();
                            qtyStr = drBOM["ReqQty"].ToString();
                            compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            compTypeStr = "Compressor";
                            if (Int32.Parse(compQtyStr) > 1) // Checking to see if qty is greater than 1, If true then account for additional compressors.
                            {
                                int compQtyInt = Int32.Parse(compQtyStr);
                                compQtyStr = "1";
                                for (int x = 0; x < compQtyInt; ++x)
                                {
                                    CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                                }
                            }
                            else
                            {
                                CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                            }
                        }
                    }
                }
                else if (motorTypeStr == "Condensor")
                {
                    DataTable dtMotor = GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = drBOM["ReqQty"].ToString();
                        if (fanCondQtyStr == "0")
                        {
                            fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        }
                        else
                        {
                            int fanQty = Int32.Parse(fanCondQtyStr);
                            fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            fanQty += Int32.Parse(fanCondQtyStr);
                            fanCondQtyStr = fanQty.ToString();
                        }
                        DataRow dr = dtMotor.Rows[0];
                        fanCondFLAStr = dr["FLA"].ToString();
                        fanCondHPStr = dr["HP"].ToString();
                        fanCondFLAVoltStr = fanCondFLAStr + "-" + voltageStr;
                        fanCondPHStr = phaseInt.ToString();
                    }
                }
                else if (motorTypeStr == "SupplyFan" || motorTypeStr == "Indoor FAN")
                {
                    DataTable dtMotor = GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = drBOM["ReqQty"].ToString();
                        fanEvapQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        DataRow dr = dtMotor.Rows[0];
                        fanEvapFLAStr = dr["FLA"].ToString();
                        fanEvapHPStr = dr["HP"].ToString();
                        fanEvapFLAVoltStr = fanEvapFLAStr + "-" + voltageStr;
                        fanEvapPHStr = phaseInt.ToString();
                    }

                    ++evapFanCntInt;
                }
                else if (motorTypeStr == "PoweredExhaust" || motorTypeStr == "Powered Exhaust")
                {
                    DataTable dtMotor = GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = drBOM["ReqQty"].ToString();
                        fanPWRExhQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        DataRow dr = dtMotor.Rows[0];
                        fanPWRExhFLAStr = dr["FLA"].ToString();
                        fanPWRExhHPStr = dr["HP"].ToString();
                        fanPWRExhFLAVoltStr = fanPWRExhFLAStr + "-" + voltageStr;
                        fanPWRExhPHStr = phaseInt.ToString();
                    }

                    ++pwrExhFanCntInt;
                }
                else if (motorTypeStr == "ERV")
                {
                    DataTable dtMotor = GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);
                    if (dtMotor.Rows.Count > 0)
                    {
                        qtyStr = drBOM["ReqQty"].ToString();
                        fanERVQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        DataRow dr = dtMotor.Rows[0];
                        fanERVFLAStr = dr["FLA"].ToString();
                        fanERVHPStr = dr["HP"].ToString();
                        fanERVFLAVoltStr = fanERVFLAStr + "-" + voltageStr;
                        fanERVPHStr = phaseInt.ToString();
                    }
                }
            }

            if ((heatTypeStr == "IF" && modelNumStr.Substring(4, 3) == "000") || (nonElecPrimWithSecHeat == true))
            {
                int mbhInt = Int32.Parse(mbhStr);

                if (mbhInt > 0)
                {
                    if (mbhInt < 225)
                    {
                        primaryHtgInputStr = "1.69";
                    }
                    else if (mbhInt < 425)
                    {
                        primaryHtgInputStr = "3.3";
                    }
                    else if (mbhInt < 825)
                    {
                        primaryHtgInputStr = "6.14";
                    }
                    else if (mbhInt < 1225)
                    {
                        primaryHtgInputStr = "10";
                    }
                }
                else
                {
                    primaryHtgInputStr = "0";
                }
            }          

            mcaStr = "0";

            // Calculate the different modes that the unit can operate in.
            // Fans are calculated for every unit.

            calcAmpacityValuesFansOnly(out fansOnlyFLAStr, out fansOnlyMcaStr, out fansOnlyRawMopStr, fanEvapFLAStr,
                                       fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr,
                                       voltageInt.ToString());

            if (nonElecPrimNoSecHeat == true) // If the unit has non electric primary heat and no secondary heat then add the additional values to MCA & MOP
            {                                 // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                nonElecPrimNoSecMcaStr = (double.Parse(fansOnlyMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                nonElecPrimNoSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
            }
            else if (nonElecPrimWithSecHeat == true)  // If the unit has non electric primart heat and has secondary elec heat then add the additional values to MCA & MOP
            {                                         // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                if (secKwInt >= 50)
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + secHtgInputElecDbl).ToString("f1");
                }
                else
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + (secHtgInputElecDbl * 1.25)).ToString("f1");
                }

                nonElecPrimWithSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
            }

            if (modelNumStr.Substring(4, 3) != "000") // if the unit has cooling then calculate the cooling MCA & MOP
            {                                    // B. DX Cool (Horizon & Viking)
                calcAmpacityValues(out coolingMcaStr, out coolingMopStr, out coolingRawMopStr, CompList, fanCondFLAStr, 
                                   fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr,
                                   fanPWRExhQtyStr, voltageInt.ToString());
            }

            if (heatPumpExist == true) // if the unit has a heat pump and has secondary electric heating then add the additional values to MCA & MOP
            {                          // E. DX Heat (Air or Water Source Heat Pump) Normal Operation (Horizon)
                if (airSrcHeatPumpExist == true)  // F.	DX Heat (Air Source Heat Pump) Defrost Mode Operation (Horizon)
                {
                    if (nonElecPrimWithSecHeat == true) // If the unit has non electric primary heat and has secondary heat then add the additional values to MCA & MOP
                    {
                        if (secElecHeatExist == false) //Mode F-a
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                        }
                        else // Secondary heat exist   Mode F-b
                        {
                            if (secKwInt >= 50)
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                            }
                            else
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                            }
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                        }
                    }
                    else if (heatTypeStr == "Electric") // Mode F-c Elec primary heat; no secondary elec heat
                    {
                        if (primKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * double.Parse(primaryHtgInputStr))).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                    }
                }
                else
                {
                    if (secElecHeatExist == true) //Mode E-a
                    {
                        if (secKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + secHtgInputElecDbl).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + secHtgInputElecDbl).ToString("f1");
                    }
                    // If no secondary elec heat then just use the coolingMcaStr & coolingRawMopStr values
                    // Mode E-b
                }
                // Else if no secondary elec heat the coolingMcaStr doesn't change from what was calculate earlier.
            }

            if (calcElecHeatMCA == true) // if the unit contains some type of electric heating
            {                            // C.	Primary Heat: Electric (Viking and Horizon)
                calcElecAmpacityValues(out elecMcaStr, out elecRawMopStr, primKwInt, secKwInt, fanEvapFLAStr,
                                       fanEvapQtyStr, fanERVFLAStr, fanERVQtyStr, fanPWRExhFLAStr, fanPWRExhQtyStr,
                                       voltageInt.ToString(), primHtgInputElecDbl, secHtgInputElecDbl);
            }                     

            // determine which MCA value is the largest 
            if (double.Parse(fansOnlyMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(elecMcaStr) &&
                double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = fansOnlyMcaStr;
            }
            else if (double.Parse(coolingMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = coolingMcaStr;
            }
            else if (double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = nonElecPrimNoSecMcaStr;
            }
            else if (double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr))
            {
                mcaStr = nonElecPrimWithSecMcaStr;
            }
            else
            {
                mcaStr = elecMcaStr;
            }

            minCKTAmpStr = mcaStr;

            // determine which MOP value is the largest 
            if (double.Parse(fansOnlyRawMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(elecRawMopStr) &&
               double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = fansOnlyRawMopStr;
            }
            else if (double.Parse(coolingRawMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = coolingRawMopStr;
            }
            else if (double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = nonElecPrimNoSecMopStr;
            }
            else if (double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(nonElecPrimNoSecMopStr))
            {
                rawMopStr = nonElecPrimWithSecMopStr;
            }
            else
            {
                rawMopStr = elecRawMopStr;
            }

            // Do the rounding for both electrical MCA & non electrical MCA
            if (mcaStr != "0")
            {
                elecMopStr = roundElecMOP(double.Parse(mcaStr)); // Electrical MCA rounding
            }
            else
            {
                elecMopStr = "0";
            }

            mopStr = roundMOP(double.Parse(rawMopStr)); // Non-Electrical MCA rounding

            if (double.Parse(elecMopStr) > double.Parse(mopStr)) // Use the larger of the 2 values
            {
                mopStr = elecMopStr;
            }

            mfsMCBStr = mopStr;           

            string digit3 = modelNumStr.Substring(2, 1);
            string digit4 = modelNumStr.Substring(3, 1);
            string digit567 = modelNumStr.Substring(4, 3);
            string digit9 = modelNumStr.Substring(8, 1);
            string digit11 = modelNumStr.Substring(10, 1);
            string digit12 = modelNumStr.Substring(11, 1);
            string digit13 = modelNumStr.Substring(12, 1);
            string digit14 = modelNumStr.Substring(13, 1);

            DataTable dtCCD = GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow dr = dtCCD.Rows[0];

                comp1QtyStr = "1";
                //comp1RLA_VoltsStr = dr["Comp1_1_RLA"].ToString() + "-" + voltageStr;
                comp1RLA_VoltsStr = dr["Comp1_1_RLA"].ToString();
                comp1LRAStr = dr["Comp1_1_LRA"].ToString();
                comp1PhaseStr = "3";

                if (dr["Circuit1_2_Compressor"].ToString() != "NA")
                {
                    comp2QtyStr = "1";
                    //comp2RLA_VoltsStr = dr["Comp1_2_RLA"].ToString() + "-" + voltageStr;
                    comp2RLA_VoltsStr = dr["Comp1_2_RLA"].ToString();
                    comp2LRAStr = dr["Comp1_2_LRA"].ToString();
                    comp2PhaseStr = "3";
                }

                if (dr["Circuit1_3_Compressor"].ToString() != "NA")
                {
                    comp3QtyStr = "1";
                    //comp3RLA_VoltsStr = dr["Comp1_2_RLA"].ToString() + "-" + voltageStr;
                    comp3RLA_VoltsStr = dr["Comp1_3_RLA"].ToString();
                    comp3LRAStr = dr["Comp1_3_LRA"].ToString();
                    comp3PhaseStr = "3";
                }

                if (dr["Circuit2_1_Compressor"].ToString() != "NA")
                {
                    comp4QtyStr = "1";
                    //comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString();
                    comp4LRAStr = dr["Comp2_1_LRA"].ToString();
                    comp4PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp2QtyStr = "1";
                    //    comp2RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp2LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp2PhaseStr = "3";
                    //}
                    //else if (dr["Circuit1_3_Compressor"].ToString() == "NA")
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_1_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_1_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                }

                if (dr["Circuit2_2_Compressor"].ToString() != "NA")
                {
                    comp5QtyStr = "1";
                    //comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString();
                    comp5LRAStr = dr["Comp2_2_LRA"].ToString();
                    comp5PhaseStr = "3";
                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp3QtyStr = "1";
                    //    comp3RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp3LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp3PhaseStr = "3";
                    //}
                    //else if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp5QtyStr = "1";
                    //    comp5RLA_VoltsStr = dr["Comp2_2_RLA"].ToString() + "-" + voltageStr;
                    //    comp5LRAStr = dr["Comp2_2_LRA"].ToString();
                    //    comp5PhaseStr = "3";
                    //}
                }

                if (dr["Circuit2_3_Compressor"].ToString() != "NA")
                {
                    comp6QtyStr = "1";
                    //comp6RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    comp6RLA_VoltsStr = dr["Comp2_3_RLA"].ToString();
                    comp6LRAStr = dr["Comp2_3_LRA"].ToString();
                    comp6PhaseStr = "3";

                    //if (dr["Circuit1_2_Compressor"].ToString() == "NA")
                    //{
                    //    comp4QtyStr = "1";
                    //    comp4RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    //    comp4LRAStr = dr["Comp2_3_LRA"].ToString();
                    //    comp4PhaseStr = "3";
                    //}
                    //else if (dr["Circuit1_3_Compressor"].ToString() == "NA")
                    //{
                    //    comp5QtyStr = "1";
                    //    comp5RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    //    comp5LRAStr = dr["Comp2_3_LRA"].ToString();
                    //    comp5PhaseStr = "3";
                    //}
                    //else
                    //{
                    //    comp6QtyStr = "1";
                    //    comp6RLA_VoltsStr = dr["Comp2_3_RLA"].ToString() + "-" + voltageStr;
                    //    comp6LRAStr = dr["Comp2_3_LRA"].ToString();
                    //    comp6PhaseStr = "3";
                    //}
                }

                if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = dr["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(dr["Circuit1_Charge"].ToString()) + Decimal.Parse(dr["Circuit1_ReheatCharge"].ToString())).ToString();
                }

                if (dr["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = dr["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(dr["Circuit2_Charge"].ToString()) + Decimal.Parse(dr["Circuit2_ReheatCharge"].ToString())).ToString();
                    }
                }
            }
            else
            {
                int cir1CompNum = 1;
                int cir2CompNum = 1;
                foreach (var comp in CompList)
                {
                    //parentPartNumStr = comp.ParentPartNum.Substring(0, 11);
                    if (revType == "VKG")
                    {
                        partNumStr = comp.ImmedParent;
                    }
                    else
                    {
                        if (comp.ParentPartNum.StartsWith("RFGASM") == true)
                        {
                            int parenPos = comp.ParentPartNum.IndexOf("(");
                            int partLen = 0;

                            if (parenPos > 0)
                            {
                                partLen = parenPos - 1;
                            }
                            else
                            {
                                partLen = comp.ParentPartNum.Length;
                            }
                            partNumStr = comp.ParentPartNum.Substring(0, partLen);
                        }
                        else
                        {
                            partNumStr = comp.PartNum;
                        }
                    }

                    parentPartNumStr = comp.ParentPartNum;
                    ruleBatchID = comp.RuleBatchID;

                    DataTable dtComp = GetPartRulesByRuleHeadID_AndRuleBatchID(partNumStr, ruleBatchID, revType);

                    if (dtComp.Rows.Count > 0)
                    {
                        DataRow row = dtComp.Rows[0];

                        if (row["Circuit1"].ToString() == "True" && row["Circuit2"].ToString() == "True")
                        {
                            if (cir1CompNum > 1)
                            {
                                comp.CircuitPos = "Circuit2_" + cir1CompNum++.ToString();
                            }
                            else
                            {
                                comp.CircuitPos = "Circuit1_" + cir1CompNum++.ToString();
                            }
                        }
                        else if (row["Circuit1"].ToString() == "True")
                        {
                            comp.CircuitPos = "Circuit1_" + cir1CompNum++.ToString();
                        }
                        else if (row["Circuit2"].ToString() == "True")
                        {
                            comp.CircuitPos = "Circuit2_" + cir2CompNum++.ToString();
                        }
                    }
                    else
                    {
                        if (cir1CompNum < 4)
                        {
                            comp.CircuitPos = "Circuit1";
                            ++cir1CompNum;
                        }
                        else
                        {
                            comp.CircuitPos = "Circuit2";
                            ++cir1CompNum;
                        }

                    }
                }

                decimal largestCir1CompRLA = 0;
                decimal largestCir2CompRLA = 0;
                cir1CompNum = 1;
                cir2CompNum = 1;

                foreach (var comp in CompList)
                {
                    if (comp.CircuitPos.Contains("Circuit1"))
                    {
                        if (decimal.Parse(comp.RLA) > largestCir1CompRLA)
                        {
                            largestCir1CompRLA = decimal.Parse(comp.RLA);
                            if (cir1CompNum > 1)
                            {
                                if (cir1CompNum > 2)
                                {
                                    comp3RLA_VoltsStr = comp2RLA_VoltsStr;
                                    comp3LRAStr = comp2LRAStr;
                                    comp3QtyStr = comp2QtyStr;
                                }
                                comp2RLA_VoltsStr = comp1RLA_VoltsStr;
                                comp2LRAStr = comp1LRAStr;
                                comp2QtyStr = comp1QtyStr;
                            }
                            comp1RLA_VoltsStr = comp.RLA;
                            comp1LRAStr = comp.LRA;
                            comp1QtyStr = comp.Qty;
                        }
                        else
                        {
                            if (cir1CompNum > 2)
                            {
                                if (decimal.Parse(comp.RLA) > decimal.Parse(comp2RLA_VoltsStr))
                                {
                                    comp3RLA_VoltsStr = comp2RLA_VoltsStr;
                                    comp3LRAStr = comp2LRAStr;
                                    comp3QtyStr = comp2QtyStr;
                                    comp2RLA_VoltsStr = comp.RLA;
                                    comp2LRAStr = comp.LRA;
                                    comp2QtyStr = comp.Qty;
                                }
                                else
                                {
                                    comp3RLA_VoltsStr = comp.RLA;
                                    comp3LRAStr = comp.LRA;
                                    comp3QtyStr = comp.Qty;
                                }
                            }
                            else
                            {
                                comp2RLA_VoltsStr = comp.RLA;
                                comp2LRAStr = comp.LRA;
                                comp2QtyStr = comp.Qty;
                            }
                        }
                        ++cir1CompNum;
                    }
                    else if (comp.CircuitPos.Contains("Circuit2"))
                    {
                        if (decimal.Parse(comp.RLA) > largestCir2CompRLA)
                        {
                            largestCir2CompRLA = decimal.Parse(comp.RLA);
                            if (cir2CompNum > 1)
                            {
                                if (cir2CompNum > 2)
                                {
                                    comp6RLA_VoltsStr = comp5RLA_VoltsStr;
                                    comp6LRAStr = comp5LRAStr;
                                    comp6QtyStr = comp5QtyStr;
                                }
                                comp5RLA_VoltsStr = comp4RLA_VoltsStr;
                                comp5LRAStr = comp4LRAStr;
                                comp5QtyStr = comp4QtyStr;
                            }
                            comp4RLA_VoltsStr = comp.RLA;
                            comp4LRAStr = comp.LRA;
                            comp4QtyStr = comp.Qty;
                        }
                        else
                        {
                            if (cir2CompNum > 2)
                            {
                                if (decimal.Parse(comp.RLA) > decimal.Parse(comp5RLA_VoltsStr))
                                {
                                    comp6RLA_VoltsStr = comp5RLA_VoltsStr;
                                    comp6LRAStr = comp5LRAStr;
                                    comp5RLA_VoltsStr = comp.RLA;
                                    comp5LRAStr = comp.LRA;
                                }
                                else
                                {
                                    comp6RLA_VoltsStr = comp.RLA;
                                    comp6LRAStr = comp.LRA; ;
                                }
                            }
                            else
                            {
                                comp5RLA_VoltsStr = comp.RLA;
                                comp5LRAStr = comp.LRA;
                            }
                        }
                        ++cir2CompNum;
                    }
                }
                DataTable dtChrg = GetCompressorChargeData(unitTypeStr, modelNumStr.Substring(4, 3), 
                                                           modelNumStr.Substring(10, 1), "0", modelNumStr.Substring(13, 1));
                if (dtChrg.Rows.Count > 0)
                {
                    DataRow drChrg = dtChrg.Rows[0];

                    if ((modelNumStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit1ChargeStr = drChrg["Circuit1"].ToString();
                    }
                    else
                    {
                        circuit1ChargeStr = (Decimal.Parse(drChrg["Circuit1"].ToString()) + Decimal.Parse(drChrg["Circuit1_Reheat"].ToString())).ToString();
                    }

                    circuit2ChargeStr = drChrg["Circuit2"].ToString();
                }
            }

            if (comp1RLA_VoltsStr != "")
            {
                comp1RLA_VoltsStr += "-" + voltageStr;
                //comp1QtyStr = "1";
                comp1PhaseStr = "3";
            }

            if (comp2RLA_VoltsStr != "")
            {
                comp2RLA_VoltsStr += "-" + voltageStr;
                //comp2QtyStr = "1";
                comp2PhaseStr = "3";
            }

            if (comp3RLA_VoltsStr != "")
            {
                comp3RLA_VoltsStr += "-" + voltageStr;
                //comp3QtyStr = "1";
                comp3PhaseStr = "3";
            }

            if (comp4RLA_VoltsStr != "")
            {
                comp4RLA_VoltsStr += "-" + voltageStr;
                //comp4QtyStr = "1";
                comp4PhaseStr = "3";
            }

            if (comp5RLA_VoltsStr != "")
            {
                comp5RLA_VoltsStr += "-" + voltageStr;
                //comp5QtyStr = "1";
                comp5PhaseStr = "3";
            }

            if (comp6RLA_VoltsStr != "")
            {
                comp6RLA_VoltsStr += "-" + voltageStr;
                //comp6QtyStr = "1";
                comp6PhaseStr = "3";
            }            

            JobNum = jobNumStr;
            OrderNum = Int32.Parse(orderNumStr);
            OrderLine = Int32.Parse(orderLineStr);
            ReleaseNum = Int32.Parse(releaseNumStr);
            ModelNo = modelNumStr;
            SerialNo = serialNoStr;
            MfgDate = mfgDateStr;
            UnitType = unitTypeStr;
            ElecRating = elecRatingStr;
            OperVoltage = operatingVoltsStr;
            DualPointPower = dualPointPower;
            MinCKTAmp = minCKTAmpStr;
            MinCKTAmp2 = minCKTAmpStr2;
            MfsMcb = mfsMCBStr;
            MfsMcb2 = mfsMCBStr2;
            MOP = rawMopStr;
            HeatingType = heatTypeStr;
            Voltage = voltageStr;
            TestPressureHigh = testPresHighStr;
            TestPressureLow = testPresLowStr;
            SecondaryHtgInput = secHtgInputStr;
            HeatingInputElectric = heatingInputElecStr;
            FuelType = fuelType;
            Comp1Qty = comp1QtyStr;
            Comp2Qty = comp2QtyStr;
            Comp3Qty = comp3QtyStr;
            Comp4Qty = comp4QtyStr;
            Comp5Qty = comp5QtyStr;
            Comp6Qty = comp6QtyStr;
            Comp1Phase = comp1PhaseStr;
            Comp2Phase = comp2PhaseStr;
            Comp3Phase = comp3PhaseStr;
            Comp4Phase = comp4PhaseStr;
            Comp5Phase = comp5PhaseStr;
            Comp6Phase = comp6PhaseStr;
            Comp1RLA_Volts = comp1RLA_VoltsStr;
            Comp2RLA_Volts = comp2RLA_VoltsStr;
            Comp3RLA_Volts = comp3RLA_VoltsStr;
            Comp4RLA_Volts = comp4RLA_VoltsStr;
            Comp5RLA_Volts = comp5RLA_VoltsStr;
            Comp6RLA_Volts = comp6RLA_VoltsStr;
            Comp1LRA = comp1LRAStr;
            Comp2LRA = comp2LRAStr;
            Comp3LRA = comp3LRAStr;
            Comp4LRA = comp4LRAStr;
            Comp5LRA = comp5LRAStr;
            Comp6LRA = comp6LRAStr;
            Circuit1Charge = circuit1ChargeStr;
            Circuit2Charge = circuit2ChargeStr;
            FanCondQty = fanCondQtyStr;
            FanEvapQty = fanEvapQtyStr;
            FanErvQty = fanERVQtyStr;
            FanPwrExhQty = fanPWRExhQtyStr;
            FanCondPhase = fanCondPHStr;
            FanEvapPhase = fanEvapPHStr;
            FanErvPhase = fanERVPHStr;
            FanPwrExhPhase = fanPWRExhPHStr;
            FanCondFLA = fanCondFLAVoltStr;
            FanEvapFLA = fanEvapFLAVoltStr;
            FanErvFLA = fanERVFLAVoltStr;
            FanPwrExhFLA = fanPWRExhFLAVoltStr;
            FanCondHP = fanCondHPStr;
            FanEvapHP = fanEvapHPStr;
            FanErvHP = fanERVHPStr;
            FanPwrExhHP = fanPWRExhHPStr;
            FlowRate = flowRateStr;
            EnteringTemp = enteringTempStr;
            OperatingPressure = operatingPressureStr;
            WaterGlycol = waterGlycol;
            DF_StaticPressure = df_StaticPressureStr;
            DF_MaxHtgInputBTUH = df_MaxHtgInputBTUHStr;
            DF_MinHeatingInput = df_MinHeatingInputStr;
            DF_MinPressureDrop = df_MinPressureDropStr;
            DF_MaxPressureDrop = df_MaxPressureDropStr;
            DF_ManifoldPressure = df_ManifoldPressureStr;
            DF_CutoutTemp = df_CutOutTempStr;
            DF_MinGasPressure = df_MinGasPressureStr;
            DF_MaxGasPressure = df_MaxGasPressureStr;
            DF_TempRise = df_TempRiseStr;
            IN_MaxHtgInputBTUH = if_MaxHtgInputBTUHStr;
            IN_HeatingOutputBTUH = if_HtgOutputBTUHStr;
            IN_MinInputBTU = if_MinInputBTUStr;
            IN_MaxExt = if_MaxEntStr;
            IN_TempRise = if_TempRiseStr;
            IN_MaxOutAirTemp = maxOutletAirTempStr;
            IN_MaxGasPressure = if_MaxGasPressureStr;
            IN_MinGasPressure = if_MinGasPressureStr;
            IN_ManifoldPressure = if_ManifoldPressureStr;
            ModelNoVerifiedBy = "OAU_Config";
            French = "French";

            if (revType.StartsWith("OA") == true)
            {
                WhseCode = "LWH5";
            }
            else
            {
                WhseCode = "LWH2";
            }

            try
            {
                InsertETL_Oau();
            }
            catch (Exception f)
            {
                MessageBox.Show("Error writing to the R6_OA_EtlOAU Table - " + f);
            }

        }

        private string GetMinInputBTU(string mbhStr, string unitType)
        {
            string minInputBTUStr = "";

            decimal mbhDec;

            if (Int32.Parse(mbhStr) <= 125)
            {
                mbhStr = mbhStr + "000";
                mbhDec = decimal.Parse(mbhStr) * .2m;  // Change from .25m to .2m - 7/28/15 per Dale - Reflects changes from 8:1 turndown to a 10:1 turndown
                minInputBTUStr = mbhDec.ToString("f0");
            }
            else if ((Int32.Parse(mbhStr) <= 400) || ((Int32.Parse(mbhStr) == 500) && (unitType == "N" || unitType == "G")) || ((Int32.Parse(mbhStr) == 600) && unitType == "G"))
            {
                mbhStr = mbhStr + "000";
                mbhDec = decimal.Parse(mbhStr) * .1m;  // Change from .125m to .1m - 7/28/15 per Dale - Reflects changes from 8:1 turndown to a 10:1 turndown
                minInputBTUStr = mbhDec.ToString("f0");
            }           
            else
            {
                mbhStr = mbhStr + "000";
                mbhDec = decimal.Parse(mbhStr) * .05m;  // Change from .0625m to .05m - 7/28/15 per Dale - Reflects changes from 8:1 turndown to a 10:1 turndown
                minInputBTUStr = mbhDec.ToString("f0");
            }

            return minInputBTUStr;

        }

        private int GetNextSeqNum(string assemblySeqStr, DataTable dtBOM)
        {
            int retSeqNumInt = 0;
            string seqNumStr = "";

            foreach (DataRow dr in dtBOM.Rows)
            {
                if (dr["AssemblySeq"].ToString() == assemblySeqStr)
                {
                    seqNumStr = dr["SeqNo"].ToString();
                }
            }

            if (seqNumStr != "")
            {
                retSeqNumInt = Int32.Parse(seqNumStr) + 10;
            }

            return retSeqNumInt;

        }

        public string CalculateAmpacityValues(DataTable dtBOM, string modelNo, out string mopStr, out string rawMopStr, string oaRev, string jobNumStr)
        {            
            string partNumStr = "";
            string parentPartNumStr = "";
            string compTypeStr = "";
            string compQtyStr = "0";
            string compRLA_VoltsStr = "0";
            string compLRAStr = "0";           
            string fanCondQtyStr = "0";
            string fanCondFLAStr = "0";
            string fanCondHPStr = "";
            string fanEvapQtyStr = "0";
            string fanEvapFLAStr = "0";
            string fanEvapHPStr = "";            
            string fanERVQtyStr = "0";
            string fanERVFLAStr = "0";
            string fanERVHPStr = "";
            string fanPwrExhQtyStr = "0";
            string fanPwrExhFLAStr = "0";
            string fanPwrExhHPStr = "";                                     
            string mcaStr = "0";
            string elecMcaStr = "0";
            string elecMopStr = "0";
            string elecRawMopStr = "0";   
            string coolingMcaStr = "0";
            string coolingMopStr = "0";
            string coolingRawMopStr = "0";
            string fansOnlyMcaStr = "0";
            string fansOnlyFLAStr = "0";
            string fansOnlyRawMopStr = "0";
            string nonElecPrimNoSecFlaStr = "0";
            string nonElecPrimNoSecMcaStr = "0";
            string nonElecPrimNoSecMopStr = "0";
            string nonElecPrimWithSecFlaStr = "0";
            string nonElecPrimWithSecMcaStr = "0";
            string nonElecPrimWithSecMopStr = "0";
            string heatPumpNoSecFlaStr = "0";
            string heatPumpNoSecMcaStr = "0";
            string heatPumpNoSecMopStr = "0";
            string heatPumpWithSecFlaStr = "0";
            string heatPumpWithSecMcaStr = "0";
            string heatPumpWithSecMopStr = "0";
            string qtyStr = "0";
            string heatingTypeStr = "";
            string heatDigitStr = "";
            string tmp_MinCKTAmpStr = "";
            string tmp_MFSMCBStr = "";
            string heatingInputElecStr = "0";
            string secHtgInputStr = "0";
            string primaryHtgInputStr = "0";
            string kwStr = "";
            string ruleHeadID = "0";
            string ruleBatchID = "";
            string unitTypeStr = "";
            string OAUTypeCode = "";
            string partCategory = "";
            string preHtgInputStr = "0";
            string mbhGasHeatStr = "0";
            string digit11Values = "A,B,C,D,E";
            string revisionNum = "";
          
            int voltageInt = 0;           
            int phaseInt = 0;
            int primKwInt = 0;
            int secKwInt = 0;

            double tempDbl;
            double primHtgInputElecDbl = 0;
            double secHtgInputElecDbl = 0;
            double preHtgInputElecDbl = 0;

            decimal hertzDec = 0;
            decimal tandAsmQty = 0;
            decimal compQtyPer = 0; ;

            bool calcElecHeatMCA = false;
            //bool calcCoolMCA = false;
            bool heatPumpExist = false;
            bool airSrcHeatPumpExist = false;
            bool secHeatExist = false;
            bool secElecHeatExist = false;
            bool nonElecPrimNoSecHeat = false;
            bool nonElecPrimWithSecHeat = false;            

            if (oaRev == "REV6" || oaRev == "VKG")
            {
                unitTypeStr = oaRev;
                switch (modelNo.Substring(8, 1))
                {
                    case "1":
                        voltageInt = 208;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                    case "2":
                        voltageInt = 230;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                    case "3":
                        voltageInt = 460;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                    case "4":
                        voltageInt = 575;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                    case "5":
                        voltageInt = 115;
                        hertzDec = 60;
                        phaseInt = 1;
                        break;
                    case "6":
                        voltageInt = 208;
                        hertzDec = 60;
                        phaseInt = 1;
                        break;
                    case "7":
                        voltageInt = 208;
                        hertzDec = 50;
                        phaseInt = 3;
                        break;
                    case "8":
                        voltageInt = 380;
                        hertzDec = 50;
                        phaseInt = 3;
                        break;
                    case "9":
                        voltageInt = 208;
                        hertzDec = 50;
                        phaseInt = 1;
                        break;
                    default:
                        voltageInt = 208;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                }

                heatDigitStr = modelNo.Substring(15, 1);  //Digit 16  
                if (heatDigitStr == "A" || heatDigitStr == "B" || heatDigitStr == "C" || 
                    heatDigitStr == "D" || heatDigitStr == "E" || heatDigitStr == "F" ||
                    heatDigitStr == "L" || heatDigitStr == "M")  // Indirect Fired Heat IF
                {
                    heatingTypeStr = "IF";
                    primaryHtgInputStr = "0";

                    if (voltageInt == 208 || voltageInt == 230 || voltageInt == 240)
                    {
                        primaryHtgInputStr = "6.25";
                    }                   
                    else if  (voltageInt == 460)
                    {
                        primaryHtgInputStr = "3.13";
                    }
                    else if (voltageInt == 575)
                    {
                        primaryHtgInputStr = "2.5";
                    }                   
                    //DataTable dtModel = GetModelNumDigitDesc(17, "IF", modelNo.Substring(16, 1));  // Commented out 08/09/19

                    //if (dtModel.Rows.Count > 0) // Calculate Heating electric input value.
                    //{
                    //    DataRow drModel;
                    //    drModel = dtModel.Rows[0];

                    //    mbhGasHeatStr = drModel["DigitDescription"].ToString();
                    //    mbhGasHeatStr = mbhGasHeatStr.Substring(0, mbhGasHeatStr.IndexOf(' '));
                    //}
                }
                else if (heatDigitStr == "G")
                {
                    heatingTypeStr = "HOTWATER";
                }
                else if (heatDigitStr == "H" || heatDigitStr == "J")
                {
                    heatingTypeStr = "ELEC";
                    calcElecHeatMCA = true;
                    try
                    {
                        DataTable dtModel = new DataTable();

                        if (unitTypeStr == "REV6") 
                        {
                            dtModel = GetModelNumDigitDesc(17, "ELEC", modelNo.Substring(16, 1), "REV6"); // Digit 17 for OA units                      
                        }
                        else
                        {
                            dtModel = GetModelNumDigitDesc(65, "ELEC", modelNo.Substring(64, 1), "VKG"); // Digit 65 for Viking units
                        }

                        if (dtModel.Rows.Count > 0) // Calculate Heating electric input value.
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            kwStr = drModel["DigitDescription"].ToString();
                            kwStr = stripKwFromDescription(kwStr, voltageInt, unitTypeStr);

                            if (IsNumeric(kwStr) == true)
                            {
                                primKwInt = Int32.Parse(kwStr);
                            }

                            kwStr = kwStr + "000";
                            tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                            tempDbl = tempDbl / Math.Sqrt(3);
                            primHtgInputElecDbl = tempDbl;
                            heatingInputElecStr = tempDbl.ToString("f2");
                        }
                    }
                    catch (Exception f)
                    {
                        MessageBox.Show("Error Reading ModelNumDesc Table for digit 19 - " + f);
                    }
                }

                if (heatingTypeStr != "ELEC")  // Potential bug - always assumes secondary heat type is electric
                {
                    if ( modelNo.Substring(17, 1) == "4" || modelNo.Substring(17, 1) == "5")  // If digit 18 equal 4 or 5
                    {
                        nonElecPrimWithSecHeat = true;
                    }
                    else if ( modelNo.Substring(17, 1) == "0")
                    {
                        nonElecPrimNoSecHeat = true;
                    }
                }       
        
                if (modelNo.Substring(13, 1) == "5" || modelNo.Substring(13, 1) == "6")
                {
                    airSrcHeatPumpExist = true;
                    heatPumpExist = true;
                }

                if (modelNo.Substring(13, 1) == "7" || modelNo.Substring(13, 1) == "8")
                {                  
                    heatPumpExist = true;
                }

                if (modelNo.Substring(17, 1) != "0")  //if Secondary heat exist - Digit 18 != 0
                {
                    secHeatExist = true;
                    if (modelNo.Substring(17, 1) == "4" || modelNo.Substring(17, 1) == "5")
                    {
                        secElecHeatExist = true;
                        DataTable dtModel = GetModelNumDigitDesc(19, "ELEC", modelNo.Substring(18, 1), unitTypeStr); //Retrieve data from Digit 19 for both Viking & OA
                        if (dtModel.Rows.Count > 0)
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            kwStr = drModel["DigitDescription"].ToString();
                            kwStr = stripKwFromDescription(kwStr, voltageInt, oaRev);

                            if (IsNumeric(kwStr) == true)
                            {
                                secKwInt = Int32.Parse(kwStr);
                            }

                            kwStr = kwStr + "000";
                           
                            tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                            tempDbl = tempDbl / Math.Sqrt(3);
                            secHtgInputElecDbl = tempDbl;
                            secHtgInputStr = secHtgInputElecDbl.ToString("f2");

                            if (voltageInt == 208 && modelNo.StartsWith("HA") == true)
                            {                               
                                tempDbl = double.Parse(kwStr) * double.Parse(voltageInt.ToString());
                                tempDbl = tempDbl / 52900 / Math.Sqrt(3);
                                secHtgInputElecDbl = tempDbl;
                                secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                            }                                                                                                                
                        }
                    }
                    heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                    calcElecHeatMCA = true;
                }
            }
            else
            {
                switch (modelNo.Substring(8, 1))
                {
                    case "1":
                        voltageInt = 115;
                        hertzDec = 60;
                        phaseInt = 1;
                        break;
                    case "2":
                        voltageInt = 208;
                        hertzDec = 60;
                        phaseInt = 1;
                        break;
                    case "3":
                        voltageInt = 208;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                    case "4":
                        voltageInt = 460;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                    case "5":
                        voltageInt = 575;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;                    
                    default:
                
                        voltageInt = 208;
                        hertzDec = 60;
                        phaseInt = 3;
                        break;
                }
                heatDigitStr = modelNo.Substring(19, 1);  //Digit 20
                unitTypeStr = modelNo.Substring(2, 1);

                if (unitTypeStr == "L" || unitTypeStr == "M" || unitTypeStr == "B" || unitTypeStr == "G")
                {
                    OAUTypeCode = "OALBG";
                }
                else
                {
                    OAUTypeCode = "OAU123DKN";
                }

                if (modelNo.Substring(22, 1) != "0")
                {
                    secHeatExist = true;
                }

                // If Heat Pump then account  for PreHeat in MCA & MOP
                if (modelNo.Substring(3, 1) == "E" || modelNo.Substring(3, 1) == "F")
                {
                    heatPumpExist = true;
                    preHtgInputStr = "0";
                    if ((modelNo.Substring(3, 1) == "E") && 
                        (modelNo.Substring(13, 1) == "1" || modelNo.Substring(3, 1) == "2" || modelNo.Substring(3, 1) == "4"))
                    {
                        airSrcHeatPumpExist = true;
                    }

                    if (modelNo.Substring(19, 1) == "G" || modelNo.Substring(19, 1) == "H" || modelNo.Substring(19, 1) == "L" ||
                        modelNo.Substring(19, 1) == "N" || modelNo.Substring(19, 1) == "Q" || modelNo.Substring(19, 1) == "S" ||
                        modelNo.Substring(19, 1) == "T" || modelNo.Substring(19, 1) == "U" || modelNo.Substring(19, 1) == "V" ||
                        modelNo.Substring(19, 1) == "W" || modelNo.Substring(19, 1) == "Y" || modelNo.Substring(19, 1) == "Z")     // Unit has secondary electric heat source
                    {

                        secHeatExist = true;
                        secElecHeatExist = true;
                        DataTable dtModel = GetRev5ModelNoDigitDesc(23, modelNo.Substring(22, 1), "ELEC", OAUTypeCode);

                        if (dtModel.Rows.Count > 0)
                        {
                            DataRow drModel = dtModel.Rows[0];
                            kwStr = drModel["DigitDescription"].ToString();

                            kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");

                            if (IsNumeric(kwStr) == true && modelNo.Substring(22, 1) != "X")
                            {
                                secKwInt = Int32.Parse(kwStr);
                            }

                            kwStr = kwStr + "000";
                            tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                            tempDbl = tempDbl / Math.Sqrt(3);
                            preHtgInputElecDbl = tempDbl;

                            preHtgInputStr = preHtgInputElecDbl.ToString("f2");
                            calcElecHeatMCA = true;
                        }
                    }
                }

                if (heatDigitStr == "A" || heatDigitStr == "E" ||
                    heatDigitStr == "G" || heatDigitStr == "T")   // Indirect Fired (IF) & DuelFuel IF/????
                {
                    heatingTypeStr = "IF";
                    primaryHtgInputStr = "0";

                    if (voltageInt == 208 || voltageInt == 230 || voltageInt == 240)
                    {
                        primaryHtgInputStr = "6.25";
                    }                  
                    else if (voltageInt == 460)
                    {
                        primaryHtgInputStr = "3.13";
                    }
                    else if (voltageInt == 575)
                    {
                        primaryHtgInputStr = "2.5";
                    }                   
                    //DataTable dtModel = GetRev5ModelNoDigitDesc(22, modelNo.Substring(21, 1), "IF", OAUTypeCode);

                    //if (dtModel.Rows.Count > 0)
                    //{
                    //    DataRow drModel = dtModel.Rows[0];
                    //    mbhGasHeatStr = drModel["DigitDescription"].ToString();
                    //    mbhGasHeatStr = mbhGasHeatStr.Substring(0, mbhGasHeatStr.IndexOf(' '));
                    //}
                }
                else if (heatDigitStr == "J" || heatDigitStr == "P" ||
                         heatDigitStr == "Q" || heatDigitStr == "Y")
                {
                    heatingTypeStr = "HOTWATER";
                    if (heatDigitStr == "Q" || heatDigitStr == "Y")
                    {
                        secHeatExist = true;
                        secElecHeatExist = true;
                    }
                }
                else if (heatDigitStr == "C" || heatDigitStr == "D" || heatDigitStr == "H" ||
                         heatDigitStr == "M" || heatDigitStr == "N" || heatDigitStr == "U" || 
                         heatDigitStr == "W")
                {
                    heatingTypeStr = "ELEC";

                    if (heatDigitStr == "H" || heatDigitStr == "N" || 
                        heatDigitStr == "U" || heatDigitStr == "W")
                    {
                        secHeatExist = true;
                        secElecHeatExist = true;
                    }

                    calcElecHeatMCA = true;
                    try
                    {
                        DataTable dtModel = GetRev5ModelNoDigitDesc(22, modelNo.Substring(21, 1), "ELEC", OAUTypeCode );

                        if (dtModel.Rows.Count > 0) // Calculate Heating electric input value.
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            kwStr = drModel["DigitDescription"].ToString();
                            kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");

                            if (IsNumeric(kwStr) == true)
                            {
                                primKwInt = Int32.Parse(kwStr);
                            }

                            kwStr = kwStr + "000";
                            tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                            tempDbl = tempDbl / Math.Sqrt(3);
                            primHtgInputElecDbl = tempDbl;
                            heatingInputElecStr = tempDbl.ToString("f2");                        
                        }

                        if (heatDigitStr == "H" || heatDigitStr == "N" || heatDigitStr == "U" || heatDigitStr == "W")
                        {
                            //Get secondary heat value
                            dtModel = GetRev5ModelNoDigitDesc(23, modelNo.Substring(22, 1), "ELEC", OAUTypeCode );
                            if (dtModel.Rows.Count > 0)
                            {
                                DataRow drModel;
                                drModel = dtModel.Rows[0];

                                kwStr = drModel["DigitDescription"].ToString();
                                kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");

                                if (IsNumeric(kwStr) == true && modelNo.Substring(22, 1) != "X")
                                {
                                    secKwInt = Int32.Parse(kwStr);
                                }

                                kwStr = kwStr + "000";
                                tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                                tempDbl = tempDbl / Math.Sqrt(3);
                                secHtgInputElecDbl = tempDbl;
                                secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                            }
                        }
                        //heatingInputElecStr = (primHtgInputElecDbl + secHtgInputElecDbl).ToString("f2");
                    }
                    catch (Exception f)
                    {
                        MessageBox.Show("Error Reading ModelNumDesc Table for digit 19 - " + f);
                    }
                }

                if (heatingTypeStr != "ELEC")  // Potential bug - always assumes secondary heat type is electric
                {
                    if (modelNo.Substring(22, 1) == "0")
                    {
                        nonElecPrimNoSecHeat = true;             
                    }
                    else
                    {
                        secHeatExist = true;
                        secElecHeatExist = true;
                        calcElecHeatMCA = true;

                        //Get secondary heat value
                        DataTable dtModel = GetRev5ModelNoDigitDesc(23, modelNo.Substring(22, 1), "ELEC", OAUTypeCode);

                        if (dtModel.Rows.Count > 0)
                        {
                            DataRow drModel;
                            drModel = dtModel.Rows[0];

                            kwStr = drModel["DigitDescription"].ToString();
                            kwStr = stripKwFromDescription(kwStr, voltageInt, "REV5");

                            if (IsNumeric(kwStr) == true)
                            {
                                secKwInt = Int32.Parse(kwStr);
                            }

                            kwStr = kwStr + "000";
                            tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                            tempDbl = tempDbl / Math.Sqrt(3);
                            secHtgInputElecDbl = tempDbl;
                            secHtgInputStr = secHtgInputElecDbl.ToString("f2");
                        }
                    }
                }
            }

            //if (digit11Values.Contains(modelNo.Substring(10, 1)))  //if Digit 11 equals A, B, C, D or E
            //{
            //    calcCoolMCA = true;
            //}         

            List<Compressor> CompList = new List<Compressor>();

            foreach(DataRow dr in dtBOM.Rows)
            {
                partNumStr = dr["PartNum"].ToString();
                revisionNum = dr["RevisionNum"].ToString();
                if (dr["ParentPartNum"] != null)
                {
                    parentPartNumStr = dr["ParentPartNum"].ToString();
                }
                else
                {
                    parentPartNumStr = "";
                }
                //ruleHeadID = dr["RuleHeadID"].ToString();
                partCategory = dr["PartCategory"].ToString();

                //if (ruleHeadID == "")
                //{
                //    ruleHeadID = "0";
                //}
               
                //if (partNumStr == "VCPRFG-0991")
                //{
                //    partCategory = dr["PartCategory"].ToString();
                //}

                if (partCategory == "Compressor" || partCategory.Contains("DigitalScroll") || partCategory.Contains("TandemComp"))
                {
                    if (partNumStr.Contains("RFGASM") == true)
                    {
                        string immedParent = partNumStr;

                        DataTable dtComp = GetPartBOM(partNumStr, revisionNum);

                        foreach(DataRow drComp in dtComp.Rows)
                        {
                            partNumStr = drComp["MtlPartNum"].ToString();
                            qtyStr = drComp["QtyPer"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                compQtyStr = qtyStr;
                            }
                            compTypeStr = dr["PartCategory"].ToString();
                            getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, oaRev, ruleHeadID);

                            if (decimal.Parse(compRLA_VoltsStr) > 0)
                            {
                                CompList.Add(new Compressor("", partNumStr, parentPartNumStr, immedParent, compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                            }
                        }
                    }
                    else
                    {
                        qtyStr = dr["ReqQty"].ToString();
                        if (qtyStr.IndexOf(".") > 0)
                        {
                            compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                        }
                        else
                        {
                            compQtyStr = qtyStr;
                        }
                        compTypeStr = dr["PartCategory"].ToString();
                        getCompRLA_Data(partNumStr, voltageInt.ToString(), out compRLA_VoltsStr, out compLRAStr, oaRev, ruleHeadID);

                        if (decimal.Parse(compRLA_VoltsStr) > 0)
                        {
                            CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
                        }
                    }
                }
                else if (dr["PartCategory"].ToString().Contains("Motor") || dr["PartCategory"].ToString().Contains("Mtr"))
                {
                    //string tempRuleHeadID = "0";
                    //if (dr["RuleHeadID"] != null)
                    //{
                    //    tempRuleHeadID = dr["RuleHeadID"].ToString();
                    //}
                    DataTable dtMotor = GetMotorDataDTL(voltageInt, hertzDec, phaseInt, oaRev, partNumStr);                                                        
                    if (dtMotor.Rows.Count > 0)
                    {
                        DataRow drMotor = dtMotor.Rows[0];
                        if (dr["PartType"].ToString() == "Condensor")
                        {
                            qtyStr = dr["ReqQty"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                fanCondQtyStr = qtyStr;
                            }
                            
                            fanCondFLAStr = drMotor["FLA"].ToString();                            
                            fanCondHPStr = drMotor["HP"].ToString();
                        }
                        else if (dr["PartType"].ToString() == "ERV")
                        {
                            qtyStr = dr["ReqQty"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                fanERVQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                fanERVQtyStr = qtyStr;
                            }
                                                        
                            fanERVFLAStr = drMotor["FLA"].ToString();                            
                            fanERVHPStr = drMotor["HP"].ToString();
                        }
                        else if (dr["PartType"].ToString() == "SupplyFan" || dr["PartType"].ToString() == "Indoor FAN")
                        {
                            qtyStr = dr["ReqQty"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                fanEvapQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                fanEvapQtyStr = qtyStr;
                            }
                            
                            fanEvapFLAStr = drMotor["FLA"].ToString();                            
                            fanEvapHPStr = drMotor["HP"].ToString();
                        }
                        else if (dr["PartType"].ToString() == "PoweredExhaust" || dr["PartType"].ToString() == "Powered Exhaust")
                        {
                            qtyStr = dr["ReqQty"].ToString();
                            if (qtyStr.IndexOf(".") > 0)
                            {
                                fanPwrExhQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                fanPwrExhQtyStr = qtyStr;
                            }
                            
                            fanPwrExhFLAStr = drMotor["FLA"].ToString();                            
                            fanPwrExhHPStr = drMotor["HP"].ToString();
                        }
                    }
                }
                else if (modelNo.StartsWith("HA") == true && partNumStr.StartsWith("VMEASM") == true && parentPartNumStr.StartsWith("VK-COND") == true)
                {
                    string immedParent = partNumStr;
                    DataTable dtAsm = GetPartBOM(partNumStr, revisionNum);

                    foreach (DataRow drAsm in dtAsm.Rows)
                    {
                        partNumStr = drAsm["MtlPartNum"].ToString();

                        DataTable dtMotor = GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);

                        if (dtMotor.Rows.Count > 0)
                        {
                            DataRow drMotor = dtMotor.Rows[0];
                            qtyStr = drAsm["QtyPer"].ToString();

                            if (qtyStr.IndexOf(".") > 0)
                            {
                                fanCondQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            }
                            else
                            {
                                fanCondQtyStr = qtyStr;
                            }

                            fanCondFLAStr = drMotor["FLA"].ToString();
                            fanCondHPStr = drMotor["HP"].ToString();                                                       
                        }
                    }
                }
                //else if (partNumStr.ToUpper().Contains("VELHTR") || partNumStr.ToUpper().Contains("VGSBRN"))
                //{
                //    if (heatingTypeStr != "ELEC" && secHeatExist == false)
                //    {
                //        DataTable dtFurnace = GetFurnaceDataDTL(partNumStr);
                //        if (dtFurnace.Rows.Count > 0)
                //        {
                //            DataRow drFurn = dtFurnace.Rows[0];
                //            primaryHtgInputStr = drFurn["Amps"].ToString();
                //        }
                //    }
                    
                //}
            }

            //DataTable dtTan = GetTandemAsmFromJobAsmbl(jobNumStr);  03/22/2021

            //foreach (DataRow row in dtTan.Rows)
            //{
            //    partNumStr = row["PartNum"].ToString();
            //    tandAsmQty = (decimal) row["QtyPer"];
            //    compTypeStr = "TandemCompAssembly";                                            
            //    ruleBatchID = "0";
            //    ruleHeadID = "0";    

            //    DataTable dtComp = GetTandemCompElecData(partNumStr);

            //    if (dtComp.Rows.Count > 0)
            //    {
            //        parentPartNumStr = partNumStr;
            //        foreach(DataRow dr in dtComp.Rows)
            //        {
            //            partNumStr = dr["PartNum"].ToString();
            //            qtyStr = dr["QtyPer"].ToString();
            //            if (qtyStr.IndexOf(".") > 0)
            //            {
            //                compQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
            //            }
            //            else
            //            {
            //                compQtyStr = qtyStr;
            //            }
                        
            //            compQtyStr = (Int32.Parse(compQtyStr) * tandAsmQty).ToString("f0");
            //            compRLA_VoltsStr = dr["RLA"].ToString();
            //            compLRAStr = dr["LRA"].ToString();
                       
            //            CompList.Add(new Compressor("", partNumStr, parentPartNumStr, "", compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
            //        }
            //    }
            //}

            //var results2 = dtBOM.AsEnumerable().Where(dr => (dr.Field<string>("ParentPartNum").Contains("RFG-COMP") == true));
            //foreach (DataRow row in results)
            //{
            //    partNumStr = row["PartNum"].ToString();
            //    tandAsmQty = (decimal)row["RequiredQty"];

            //    DataTable dtComp = GetTandemCompElecData(partNumStr);

            //    if (dtComp.Rows.Count > 0)
            //    {
            //        parentPartNumStr = partNumStr;
            //        foreach (DataRow dr in dtComp.Rows)
            //        {
            //            partNumStr = dr["PartNum"].ToString();
            //            compQtyPer = (decimal)dr["QtyPer"];
            //            compQtyStr = (compQtyPer * tandAsmQty).ToString();
            //            compRLA_VoltsStr = dr["Voltage"].ToString();
            //            compLRAStr = dr["LRA"].ToString();
            //            compTypeStr = "TandemCompAssembly";
            //            ruleHeadID = 0;
            //            ruleBatchID = 0;
            //            CompList.Add(new Compressor("", partNumStr, parentPartNumStr, compQtyStr, compRLA_VoltsStr, compLRAStr, compTypeStr, ruleHeadID, ruleBatchID));
            //        }
            //    }
            //}
           
            mcaStr = "0";

            // Calculate the different modes that the unit can operate in.
            // Fans are calculated for every unit. A. FansOnly Mode

            calcAmpacityValuesFansOnly(out fansOnlyFLAStr, out fansOnlyMcaStr, out fansOnlyRawMopStr, fanEvapFLAStr, fanEvapQtyStr, fanERVFLAStr,
                                       fanERVQtyStr, fanPwrExhFLAStr, fanPwrExhQtyStr, voltageInt.ToString());

            if (nonElecPrimNoSecHeat == true) // If the unit has non electric primary heat and no secondary heat then add the additional values to MCA & MOP
            {                                 // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                nonElecPrimNoSecMcaStr = (double.Parse(fansOnlyMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                nonElecPrimNoSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");                
            }
            else if (nonElecPrimWithSecHeat == true)  // If the unit has non electric primart heat and has secondary elec heat then add the additional values to MCA & MOP
            {                                         // D.	Primary Heat: None / Gas / Hot Water / Steam, or Secondary Heat: Gas (Viking and Horizon)
                if (secKwInt >= 50)
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + secHtgInputElecDbl).ToString("f1");      
                }
                else
                {
                    nonElecPrimWithSecMcaStr = ((1.25 * (double.Parse(fansOnlyFLAStr) + double.Parse(primaryHtgInputStr))) + (secHtgInputElecDbl * 1.25)).ToString("f1");
                }
                
                nonElecPrimWithSecMopStr = (double.Parse(fansOnlyRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");               
            }

            if (modelNo.Substring(4,3) != "000") // if the unit has cooling then calculate the cooling MCA & MOP
            {                                    // B. DX Cool (Horizon & Viking)
                calcAmpacityValues(out coolingMcaStr, out coolingMopStr, out coolingRawMopStr, CompList, fanCondFLAStr, fanCondQtyStr, fanEvapFLAStr, fanEvapQtyStr,
                                   fanERVFLAStr, fanERVQtyStr, fanPwrExhFLAStr, fanPwrExhQtyStr, voltageInt.ToString());
            }

            if (heatPumpExist == true) // if the unit has a heat pump and has secondary electric heating then add the additional values to MCA & MOP
            {                          // E. DX Heat (Air or Water Source Heat Pump) Normal Operation (Horizon)
                if (airSrcHeatPumpExist == true)  // F.	DX Heat (Air Source Heat Pump) Defrost Mode Operation (Horizon)
                {
                    if (nonElecPrimWithSecHeat == true) // If the unit has non electric primary heat and has secondary heat then add the additional values to MCA & MOP
                    {
                        if (secElecHeatExist == false) //Mode F-a
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr)).ToString("f1");
                        }
                        else // Secondary heat exist   Mode F-b
                        {
                            if (secKwInt >= 50)
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                            }
                            else
                            {
                                coolingMcaStr = (double.Parse(coolingMcaStr) + double.Parse(primaryHtgInputStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                            }
                            coolingRawMopStr = (double.Parse(coolingRawMopStr) + double.Parse(primaryHtgInputStr) + secHtgInputElecDbl).ToString("f1");
                        }
                    }
                    else if (heatingTypeStr == "ELEC") // Mode F-c Elec primary heat; no secondary elec heat
                    {
                        if (primKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + primHtgInputElecDbl).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * primHtgInputElecDbl)).ToString("f2");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + primHtgInputElecDbl).ToString("f2");
                    }
                }
                else
                {
                    if (secElecHeatExist == true) //Mode E-a
                    {
                        if (secKwInt >= 50)
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + secHtgInputElecDbl).ToString("f1");
                        }
                        else
                        {
                            coolingMcaStr = (double.Parse(coolingMcaStr) + (1.25 * secHtgInputElecDbl)).ToString("f1");
                        }
                        coolingRawMopStr = (double.Parse(coolingRawMopStr) + secHtgInputElecDbl).ToString("f1");
                    }
                    // If no secondary elec heat then just use the coolingMcaStr & coolingRawMopStr values
                    // Mode E-b
                }               
                // Else if no secondary elec heat the coolingMcaStr doesn't change from what was calculate earlier.
            }           
 
            if (calcElecHeatMCA == true) // if the unit contains some type of electric heating
            {                            // C.	Primary Heat: Electric (Viking and Horizon)
                calcElecAmpacityValues(out elecMcaStr, out elecRawMopStr, primKwInt, secKwInt, fanEvapFLAStr, fanEvapQtyStr,
                    fanERVFLAStr, fanERVQtyStr, fanPwrExhFLAStr, fanPwrExhQtyStr, voltageInt.ToString(), 
                    primHtgInputElecDbl, secHtgInputElecDbl);              
            }

            // determine which MCA value is the largest 
            if (double.Parse(fansOnlyMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(elecMcaStr) &&
                double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(fansOnlyMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {                
                mcaStr = fansOnlyMcaStr;               
            }
            else if (double.Parse(coolingMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr) && double.Parse(coolingMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = coolingMcaStr;               
            }
            else if (double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimNoSecMcaStr) >= double.Parse(nonElecPrimWithSecMcaStr))
            {
                mcaStr = nonElecPrimNoSecMcaStr;
            }
            else if (double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(fansOnlyMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(elecMcaStr) &&
                     double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(coolingMcaStr) && double.Parse(nonElecPrimWithSecMcaStr) >= double.Parse(nonElecPrimNoSecMcaStr))
            {
                mcaStr = nonElecPrimWithSecMcaStr;
            }
            else
            {
                mcaStr = elecMcaStr;               
            }

            // determine which MOP value is the largest 
            if (double.Parse(fansOnlyRawMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(elecRawMopStr) &&
               double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(fansOnlyRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {                             
                rawMopStr = fansOnlyRawMopStr;
            }
            else if (double.Parse(coolingRawMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimNoSecMopStr) && double.Parse(coolingRawMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {               
                rawMopStr = coolingRawMopStr;
            }
            else if (double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimNoSecMopStr) >= double.Parse(nonElecPrimWithSecMopStr))
            {
                rawMopStr = nonElecPrimNoSecMopStr;
            }
            else if (double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(fansOnlyRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(elecRawMopStr) &&
                     double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(coolingRawMopStr) && double.Parse(nonElecPrimWithSecMopStr) >= double.Parse(nonElecPrimNoSecMopStr))
            {
                rawMopStr = nonElecPrimWithSecMopStr;
            }
            else
            {
                rawMopStr = elecRawMopStr;                 
            }

            // Do the rounding for both electrical MCA & non electrical MCA
            if (elecMcaStr != "0")
            {
                elecMopStr = roundElecMOP(double.Parse(elecMcaStr)); // Electrical MCA rounding
            }
            else
            {
                elecMopStr = "0";
            }
            mopStr = roundMOP(double.Parse(rawMopStr)); // Non-Electrical MCA rounding

            if (double.Parse(elecMopStr) > double.Parse(mopStr)) // Use the larger of the 2 values
            {
                mopStr = elecMopStr;  
            }          

            return mcaStr.ToString();
        }

        public void calcAmpacityValuesFansOnly(out string minCKTAmpStr, out string mfsMCBStr, out string rawMopStr, string fanEvapFLAStr,
                                               string fanEvapQtyStr, string fanERVFLAStr, string fanERVQtyStr, string fanPWRExhFLAStr,
                                               string fanPWRExhQtyStr, string voltageStr)
        {
            string largestMotorLoadStr = "0";
            string fansOnlyFLAStr = "0";

            int fanEvapQtyInt = 0;
            int fanERVQtyInt = 0;
            int fanPWRExhQtyInt = 0;            

            double mcaAmpsDbl = 0;
            double mopDbl = 0;
            double controlAmpDbl;            

            largestMotorLoadStr = "0";

            if (fanEvapQtyStr != "")
            {
                fanEvapQtyInt = Int32.Parse(fanEvapQtyStr);

                if (fanEvapQtyStr == "")
                {
                    fanEvapQtyStr = "0";
                }
            }
            else
            {
                fanEvapFLAStr = "0";
            }

            if (fanERVQtyStr != "")
            {
                fanERVQtyInt = Int32.Parse(fanERVQtyStr);

                if (fanERVFLAStr == "")
                {
                    fanERVFLAStr = "0";
                }
            }
            else
            {
                fanERVFLAStr = "0";
            }

            if (fanPWRExhQtyStr != "")
            {
                fanPWRExhQtyInt = Int32.Parse(fanPWRExhQtyStr);

                if (fanPWRExhFLAStr == "")
                {
                    fanPWRExhFLAStr = "0";
                }
            }
            else
            {
                fanPWRExhFLAStr = "0";
            }

            if (fanEvapFLAStr != "")
            {
                if (double.Parse(fanEvapFLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = fanEvapFLAStr;
                }
            }
            else
            {
                fanEvapFLAStr = "0";
            }

            if (fanERVFLAStr != "")
            {
                if (double.Parse(fanERVFLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = fanERVFLAStr;
                }
            }
            else
            {
                fanERVFLAStr = "0";
            }

            if (fanPWRExhFLAStr != "")
            {
                if (double.Parse(fanPWRExhFLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = fanPWRExhFLAStr;
                }
            }
            else
            {
                fanPWRExhFLAStr = "0";
            }

            if (voltageStr == "208" || voltageStr == "230" || voltageStr == "240" )
            {
                controlAmpDbl = 2.4;
            }
            else
            {
                controlAmpDbl = 1.0;
            }

            minCKTAmpStr = ((fanEvapQtyInt * double.Parse(fanEvapFLAStr)) + (fanERVQtyInt * double.Parse(fanERVFLAStr)) + 
                             (fanPWRExhQtyInt * double.Parse(fanPWRExhFLAStr)) + controlAmpDbl).ToString("f2");

            mfsMCBStr = ((0.25 * double.Parse(largestMotorLoadStr)) + (fanEvapQtyInt * double.Parse(fanEvapFLAStr)) +
                         (fanERVQtyInt * double.Parse(fanERVFLAStr)) + (fanPWRExhQtyInt * double.Parse(fanPWRExhFLAStr)) +
                         controlAmpDbl).ToString("f2");

            mopDbl = (1.25 * double.Parse(largestMotorLoadStr)) + (fanEvapQtyInt * double.Parse(fanEvapFLAStr)) +
                     (fanERVQtyInt * double.Parse(fanERVFLAStr)) + (fanPWRExhQtyInt * double.Parse(fanPWRExhFLAStr)) +
                     controlAmpDbl;

            //minCKTAmpStr = mcaAmpsDbl.ToString("f2");
            
            //mfsMCBStr = roundMOP(mopDbl);
            
            rawMopStr = mopDbl.ToString("f2");
           
        }

        public void calcAmpacityValues(out string minCKTAmpStr, out string mfsMCBStr, out string rawMopStr, List<Compressor> CompList, string fanCondFLAStr, 
                                       string fanCondQtyStr, string fanEvapFLAStr, string fanEvapQtyStr, string fanERVFLAStr, string fanERVQtyStr,
                                       string fanPWRExhFLAStr, string fanPWRExhQtyStr, string voltageStr)
        {

            string largestMotorLoadStr = "0";
            string largestMotorTypeStr = "Compressor";
            string compQtyStr = "";
            string comp1RLAStr = "0";
            string comp2RLAStr = "0";
            string comp3RLAStr = "0";
            string comp4RLAStr = "0";
            string comp5RLAStr = "0";
            string comp6RLAStr = "0";

            int comp1QtyInt = 0;
            int comp2QtyInt = 0;
            int comp3QtyInt = 0;
            int comp4QtyInt = 0;
            int comp5QtyInt = 0;
            int comp6QtyInt = 0;
            int fanCondQtyInt = 0;
            int fanEvapQtyInt = 0;
            int fanERVQtyInt = 0;
            int fanPWRExhQtyInt = 0;
            int compNum = 1;

            double mcaAmpsDbl = 0;
            double mopDbl = 0;
            double controlAmpDbl;
            double preHeatInputDbl = 0;

            foreach (var comp in CompList)
            {
                if (comp.PartNum != "")
                {
                    switch (compNum)
                    {
                        case 1:
                            compQtyStr = comp.Qty;
                            comp1QtyInt = Int32.Parse(compQtyStr);
                            comp1RLAStr = comp.RLA;
                            break;

                        case 2:
                            compQtyStr = comp.Qty;
                            comp2QtyInt = Int32.Parse(compQtyStr);
                            comp2RLAStr = comp.RLA;
                            break;

                        case 3:
                            compQtyStr = comp.Qty;
                            comp3QtyInt = Int32.Parse(compQtyStr);
                            comp3RLAStr = comp.RLA;
                            break;

                        case 4:
                            compQtyStr = comp.Qty;
                            comp4QtyInt = Int32.Parse(compQtyStr);
                            comp4RLAStr = comp.RLA;
                            break;

                        case 5:
                            compQtyStr = comp.Qty;
                            comp5QtyInt = Int32.Parse(compQtyStr);
                            comp5RLAStr = comp.RLA;
                            break;

                        case 6:
                            compQtyStr = comp.Qty;
                            comp6QtyInt = Int32.Parse(compQtyStr);
                            comp6RLAStr = comp.RLA;
                            break;

                    }
                }
                ++compNum;
            }

            largestMotorLoadStr = comp1RLAStr;

            if (fanCondQtyStr != "")
            {
                fanCondQtyInt = Int32.Parse(fanCondQtyStr);

                if (fanCondFLAStr == "")
                {
                    fanCondFLAStr = "0";
                }
            }
            else
            {
                fanCondFLAStr = "0";
            }

            if (fanEvapQtyStr != "")
            {
                fanEvapQtyInt = Int32.Parse(fanEvapQtyStr);

                if (fanEvapQtyStr == "")
                {
                    fanEvapQtyStr = "0";
                }
            }
            else
            {
                fanEvapFLAStr = "0";
            }

            if (fanERVQtyStr != "")
            {
                fanERVQtyInt = Int32.Parse(fanERVQtyStr);

                if (fanERVFLAStr == "")
                {
                    fanERVFLAStr = "0";
                }
            }
            else
            {
                fanERVFLAStr = "0";
            }

            if (fanPWRExhQtyStr != "")
            {
                fanPWRExhQtyInt = Int32.Parse(fanPWRExhQtyStr);

                if (fanPWRExhFLAStr == "")
                {
                    fanPWRExhFLAStr = "0";
                }
            }
            else
            {
                fanPWRExhFLAStr = "0";
            }

            if (comp2RLAStr != "")
            {
                if (double.Parse(comp2RLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = comp2RLAStr;
                    largestMotorTypeStr = "Secondary Compressor";
                }
            }
            else
            {
                comp2RLAStr = "0";
            }

            if (comp3RLAStr != "")
            {
                if (double.Parse(comp3RLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = comp3RLAStr;
                    largestMotorTypeStr = "Third Compressor";
                }
            }
            else
            {
                comp3RLAStr = "0";
            }

            if (comp4RLAStr != "")
            {
                if (double.Parse(comp4RLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = comp4RLAStr;
                    largestMotorTypeStr = "Fourth Compressor";
                }
            }
            else
            {
                comp4RLAStr = "0";
            }

            if (comp5RLAStr != "")
            {
                if (double.Parse(comp5RLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = comp5RLAStr;
                    largestMotorTypeStr = "Fifth Compressor";
                }
            }
            else
            {
                comp5RLAStr = "0";
            }

            if (comp6RLAStr != "")
            {
                if (double.Parse(comp6RLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = comp6RLAStr;
                    largestMotorTypeStr = "Sixth Compressor";
                }
            }
            else
            {
                comp6RLAStr = "0";
            }

            if (fanCondFLAStr != "")
            {
                if (double.Parse(fanCondFLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = fanCondFLAStr;
                    largestMotorTypeStr = "Condenser Fan";
                }
            }
            else
            {
                fanCondFLAStr = "0";
            }

            if (fanEvapFLAStr != "")
            {
                if (double.Parse(fanEvapFLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = fanEvapFLAStr;
                    largestMotorTypeStr = "Evaporator Fan";
                }
            }
            else
            {
                fanEvapFLAStr = "0";
            }

            if (fanERVFLAStr != "")
            {
                if (double.Parse(fanERVFLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = fanERVFLAStr;
                    largestMotorTypeStr = "ERV Fan";
                }
            }
            else
            {
                fanERVFLAStr = "0";
            }

            if (fanPWRExhFLAStr != "")
            {
                if (double.Parse(fanPWRExhFLAStr) > double.Parse(largestMotorLoadStr))
                {
                    largestMotorLoadStr = fanPWRExhFLAStr;
                    largestMotorTypeStr = "Powered Exhaust";
                }
            }
            else
            {
                fanPWRExhFLAStr = "0";
            }

            if (largestMotorTypeStr == "Compressor")
            {
                --comp1QtyInt;
            }
            else if (largestMotorTypeStr == "Secondary Compressor")
            {
                --comp2QtyInt;
            }
            else if (largestMotorTypeStr == "Third Compressor")
            {
                --comp3QtyInt;
            }
            else if (largestMotorTypeStr == "Fourth Compressor")
            {
                --comp4QtyInt;
            }
            else if (largestMotorTypeStr == "Fifth Compressor")
            {
                --comp5QtyInt;
            }
            else if (largestMotorTypeStr == "Sixth Compressor")
            {
                --comp6QtyInt;
            }
            else if (largestMotorTypeStr == "Condenser Fan")
            {
                --fanCondQtyInt;
            }
            else if (largestMotorTypeStr == "Evaporator Fan")
            {
                --fanEvapQtyInt;
            }
            else if (largestMotorTypeStr == "ERV Fan")
            {
                --fanERVQtyInt;
            }
            else if (largestMotorTypeStr == "Powered Exhaust")
            {
                --fanPWRExhQtyInt;
            }

            if (voltageStr == "208" || voltageStr == "230" || voltageStr == "240")
            {
                controlAmpDbl = 2.4; 
            }
            else
            {
                controlAmpDbl = 1.0;
            }           

            mcaAmpsDbl = (1.25 * double.Parse(largestMotorLoadStr)) + (comp1QtyInt * double.Parse(comp1RLAStr)) +
                         (comp2QtyInt * double.Parse(comp2RLAStr)) + (comp3QtyInt * double.Parse(comp3RLAStr)) +
                         (comp4QtyInt * double.Parse(comp4RLAStr)) + (comp5QtyInt * double.Parse(comp5RLAStr)) +
                         (comp6QtyInt * double.Parse(comp6RLAStr)) + (fanCondQtyInt * double.Parse(fanCondFLAStr)) +
                         (fanEvapQtyInt * double.Parse(fanEvapFLAStr)) + (fanERVQtyInt * double.Parse(fanERVFLAStr)) +
                         (fanPWRExhQtyInt * double.Parse(fanPWRExhFLAStr)) + controlAmpDbl;

            mopDbl = (2.25 * double.Parse(largestMotorLoadStr)) + (comp1QtyInt * double.Parse(comp1RLAStr)) +
                         (comp2QtyInt * double.Parse(comp2RLAStr)) + (comp3QtyInt * double.Parse(comp3RLAStr)) +
                          (comp4QtyInt * double.Parse(comp4RLAStr)) + (comp5QtyInt * double.Parse(comp5RLAStr)) +
                         (comp6QtyInt * double.Parse(comp6RLAStr)) + (fanCondQtyInt * double.Parse(fanCondFLAStr)) +
                         (fanEvapQtyInt * double.Parse(fanEvapFLAStr)) + (fanERVQtyInt * double.Parse(fanERVFLAStr)) +
                         (fanPWRExhQtyInt * double.Parse(fanPWRExhFLAStr)) + controlAmpDbl;

            minCKTAmpStr = mcaAmpsDbl.ToString("f1");

            //mfsMCBStr = roundMOP(mopDbl);
            mfsMCBStr = mopDbl.ToString("f1");

            rawMopStr = mopDbl.ToString("f1");

        }

        public void calcElecAmpacityValues(out string minCKTAmpStr, out string mfsMCBStr, int primKwInt, int secKwInt,
                                           string fanEvapFLAStr, string fanEvapQtyStr, string fanERVFLAStr, string fanERVQtyStr, 
                                           string fanPWRExhFLAStr, string fanPWRExhQtyStr, string voltageStr, 
                                           double primaryInputDbl, double secHtgInputDbl)
        {
            int fanEvapQtyInt = 0;
            int fanERVQtyInt = 0;
            int fanPWRExhQtyInt = 0;

            double mcaAmpsDbl = 0;
            double rawMopDbl = 0;
            double largestLoadValueDbl = 0;
            double controlAmpDbl;
            double fanEvapFLA_Dbl = 0;
            double fanERVFLA_Dbl = 0;
            double fanPWRExhFLA_Dbl = 0;
            double primElecMultiplier = 1.25;
            double secElecMultiplier = 1.25;

            if (fanEvapQtyStr != "")
            {
                fanEvapQtyInt = Int32.Parse(fanEvapQtyStr);
                if (fanEvapFLAStr != "")
                {
                    fanEvapFLA_Dbl = double.Parse(fanEvapFLAStr);
                }               
            }               

            if (fanERVQtyStr != "")
            {
                fanERVQtyInt = Int32.Parse(fanERVQtyStr);
                if (fanERVFLAStr != "")
                {
                   fanERVFLA_Dbl = double.Parse(fanERVFLAStr);
                }
            }           

            if (fanPWRExhQtyStr != "")
            {
                fanPWRExhQtyInt = Int32.Parse(fanPWRExhQtyStr);
                if (fanPWRExhFLAStr != "")
                {
                    fanPWRExhFLA_Dbl = double.Parse(fanPWRExhFLAStr);
                }
            }          

            if (voltageStr == "208" || voltageStr == "230" || voltageStr == "240")
            {
                controlAmpDbl = 2.4;
            }
            else
            {
                controlAmpDbl = 1.0;
            }

            largestLoadValueDbl = fanEvapFLA_Dbl;

            if (fanERVFLA_Dbl > largestLoadValueDbl)
            {
                largestLoadValueDbl = fanERVFLA_Dbl;
            }

            if (fanPWRExhFLA_Dbl > largestLoadValueDbl)
            {
                largestLoadValueDbl = fanPWRExhFLA_Dbl;
            }

            if (primKwInt >= 50)
            {
                primElecMultiplier = 1.0;
            }

            if (secKwInt >= 50)
            {
                secElecMultiplier = 1.0;
            }

            mcaAmpsDbl = (primElecMultiplier * primaryInputDbl) + (secElecMultiplier * secHtgInputDbl) +
                         (1.25 * ((fanEvapQtyInt * fanEvapFLA_Dbl) + (fanERVQtyInt * fanERVFLA_Dbl) + 
                         (fanPWRExhQtyInt * fanPWRExhFLA_Dbl) + controlAmpDbl));
            //mcaAmpsDbl = 1.25 * ((fanEvapQtyInt * fanEvapFLA_Dbl) + (fanERVQtyInt * fanERVFLA_Dbl) +
            //                    (fanPWRExhQtyInt * fanPWRExhFLA_Dbl) + controlAmpDbl + primaryInputDbl + secHtgInputDbl);
            
            minCKTAmpStr = mcaAmpsDbl.ToString("f1");

            rawMopDbl = (1.25 * largestLoadValueDbl) + ((fanEvapQtyInt * fanEvapFLA_Dbl) + (fanERVQtyInt * fanERVFLA_Dbl) +
                                 (fanPWRExhQtyInt * fanPWRExhFLA_Dbl) + controlAmpDbl + primaryInputDbl + secHtgInputDbl);
           
            mfsMCBStr = rawMopDbl.ToString("f1");
        }
        
        public string roundElecMOP(double mopDbl)
        {
            string mopStr = "";

            if (mopDbl > 900)
            {
                mopStr = "1000";
            }
            else if (mopDbl > 800)
            {
                mopStr = "900";
            }
            else if (mopDbl > 700)
            {
                mopStr = "800";
            }
            else if (mopDbl > 600)
            {
                mopStr = "700";
            }
            else if (mopDbl > 500)
            {
                mopStr = "600";
            }
            else if (mopDbl > 450)
            {
                mopStr = "500";
            }
            else if (mopDbl > 400)
            {
                mopStr = "450";
            }           
            else if (mopDbl > 350)
            {
                mopStr = "400";
            }
            else if (mopDbl > 300)
            {
                mopStr = "350";
            }
            else if (mopDbl > 250)
            {
                mopStr = "300";
            }
            else if (mopDbl > 225)
            {
                mopStr = "250";
            }
            else if (mopDbl > 200)
            {
                mopStr = "225";
            }
            else if (mopDbl > 175)
            {
                mopStr = "200";
            }
            else if (mopDbl > 150)
            {
                mopStr = "175";
            }
            else if (mopDbl > 125)
            {
                mopStr = "150";
            }
            else if (mopDbl > 110)
            {
                mopStr = "125";
            }
            else if (mopDbl > 100)
            {
                mopStr = "110";
            }
            else if (mopDbl > 90)
            {
                mopStr = "100";
            }
            else if (mopDbl > 80)
            {
                mopStr = "90";
            }
            else if (mopDbl > 70)
            {
                mopStr = "80";
            }
            else if (mopDbl > 60)
            {
                mopStr = "70";
            }
            else if (mopDbl > 50)
            {
                mopStr = "60";
            }
            else if (mopDbl > 45)
            {
                mopStr = "50";
            }
            else if (mopDbl > 40)
            {
                mopStr = "45";
            }
            else if (mopDbl > 30)
            {
                mopStr = "40";
            }
            else if (mopDbl > 25)
            {
                mopStr = "30";
            }
            else if (mopDbl > 20)
            {
                mopStr = "25";
            }
            else if (mopDbl > 15)
            {
                mopStr = "20";
            }
            else
            {
                mopStr = "15";
            }

            return mopStr;
        }

        public string roundMOP(double mopDbl)
        {
            string mopStr = "";

            if (mopDbl < 20)
            {
                mopStr = "15";
            }
            else if (mopDbl < 25)
            {
                mopStr = "20";
            }
            else if (mopDbl < 30)
            {
                mopStr = "25";
            }
            else if (mopDbl < 35)
            {
                mopStr = "30";
            }
            else if (mopDbl < 40)
            {
                mopStr = "35";
            }
            else if (mopDbl < 45)
            {
                mopStr = "40";
            }
            else if (mopDbl < 50)
            {
                mopStr = "45";
            }
            else if (mopDbl < 60)
            {
                mopStr = "50";
            }
            else if (mopDbl < 70)
            {
                mopStr = "60";
            }
            else if (mopDbl < 80)
            {
                mopStr = "70";
            }
            else if (mopDbl < 90)
            {
                mopStr = "80";
            }
            else if (mopDbl < 100)
            {
                mopStr = "90";
            }
            else if (mopDbl < 110)
            {
                mopStr = "100";
            }
            else if (mopDbl < 125)
            {
                mopStr = "110";
            }
            else if (mopDbl < 150)
            {
                mopStr = "125";
            }
            else if (mopDbl < 175)
            {
                mopStr = "150";
            }
            else if (mopDbl < 200)
            {
                mopStr = "175";
            }
            else if (mopDbl < 225)
            {
                mopStr = "200";
            }
            else if (mopDbl < 250)
            {
                mopStr = "225";
            }
            else if (mopDbl < 300)
            {
                mopStr = "250";
            }
            else if (mopDbl < 350)
            {
                mopStr = "300";
            }
            else if (mopDbl < 400)
            {
                mopStr = "350";
            }
            else if (mopDbl < 450)
            {
                mopStr = "400";
            }
            else if (mopDbl < 500)
            {
                mopStr = "450";
            }
            else if (mopDbl < 600)
            {
                mopStr = "500";
            }
            else if (mopDbl < 700)
            {
                mopStr = "600";
            }
            else if (mopDbl < 800)
            {
                mopStr = "700";
            }
            else if (mopDbl < 900)
            {
                mopStr = "800";
            }
            else if (mopDbl < 1000)
            {
                mopStr = "900";
            }
            else
            {
                mopStr = "1000";
            }

            return mopStr;
        }

        private int ConvertStringDecimalToInt(string value)
        {
            string valStr = "";
            int retVal = 0;
            int decPos = 0;

            if (value != "" || value != null)
            {
                decPos = value.IndexOf(".");
                if (decPos > 0)
                {
                    valStr = value.Substring(0, decPos);                    
                }
                else
                {
                    valStr = value;
                }
                retVal = Int32.Parse(valStr);
            }

            return retVal;
        }

        public string stripKwFromDescription(string kwStr, int voltage, string unitType)
        {
            string retKW = "";

            int validCharsLen = 0;            
            int slashPos = kwStr.IndexOf("/");
            int parPos = -1;

            if (unitType == "VKG" && voltage == 208)
            {               
                parPos = kwStr.IndexOf("(");
                retKW = kwStr.Substring((slashPos + 1), (parPos - (slashPos + 1)));               
            }
            else
            {
                foreach (char c in kwStr)
                {
                    if (Char.IsNumber(c))
                    {
                        ++validCharsLen;
                    }
                    else
                    {
                        break;
                    }
                }

                if (validCharsLen > 0)
                {
                    retKW = kwStr.Substring(0, validCharsLen);
                }
            }

            return retKW;

        }

        public void getCompRLA_Data(string partNumStr, string voltageStr, out string compRLAStr, out string compLRAStr, string oaRev, string ruleHeadID)
        {
            DataTable dtComp = new DataTable();
            int voltageInt = Int32.Parse(voltageStr);
            int phaseInt = 3;

            compRLAStr = "0.0";
            compLRAStr = "0.0";            

            try
            {
                if (oaRev == "REV5")
                {
                    dtComp= GetCompDetailData(partNumStr, voltageInt, phaseInt);
                }
                else
                {
                    dtComp = GetCompressorDataDtl(partNumStr, voltageInt);
                }
                
                if (dtComp.Rows.Count > 0)
                {
                    DataRow dr = dtComp.Rows[0];
                    compRLAStr = dr["RLA"].ToString();
                    compLRAStr = dr["LRA"].ToString();
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error reading Compressor Data Detail for part# " + partNumStr + " - " + f);
            }

        }

        public void getFLA_Data(int ruleHeadID, string voltageStr, out string partFLAStr, out string partHPStr, string partNumStr)
        {
            string hpStr = "";
            
            int dashPosInt;
            
            partFLAStr = "0";
            partHPStr = "0";           

            try
            {
                DataTable dt = GetMotorDataDTL(Int32.Parse(voltageStr), 60, 3, "REV5", partNumStr);

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    partFLAStr = dr["FLA"].ToString();
                    dashPosInt = partFLAStr.IndexOf('-');

                    if (voltageStr == "208")
                    {
                        if (dashPosInt > 0)
                        {
                            partFLAStr = partFLAStr.Substring(0, dashPosInt);
                        }
                    }
                    else if (voltageStr == "460")
                    {
                        if (dashPosInt > 0)
                        {
                            partFLAStr = partFLAStr.Substring((dashPosInt + 1), (partFLAStr.Length - (dashPosInt + 1)));
                        }
                    }
                    hpStr = dr["HP"].ToString();
                    partHPStr = dr["HP"].ToString();                                                                                     
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error reading Motors for RuleHeadID# " + ruleHeadID.ToString() + " - " + f);
            }


        }

        //public void getOldCompRLA_Data(string partNumStr, string voltageStr, out string compRLAStr, out string compLRAStr)
        //{
        //    int voltageInt = Int32.Parse(voltageStr);

        //    compRLAStr = "0.0";
        //    compLRAStr = "0.0";

        //    try
        //    {
        //        DataTable dtComp = GetCompressorDataDtl(partNumStr, voltageInt);
        //        if (dtComp.Rows.Count > 0)
        //        {
        //            DataRow dr = dtComp.Rows[0];
        //            compRLAStr = dr["RLA"].ToString();
        //            compLRAStr = dr["LRA"].ToString();
        //        }
        //    }
        //    catch (Exception f)
        //    {
        //        MessageBox.Show("Error reading Compressor Data Detail for part# " + partNumStr + " - " + f);
        //    }

        //}

        public int GetRowIndex(DataTable dt, string digitVal)
        {
            int retRowIdx = 0;

            foreach (DataRow dr in dt.Rows)
            {
                if (digitVal == dr["DigitVal"].ToString())
                {
                    retRowIdx = Int32.Parse(dr["RowIndex"].ToString()) - 1;
                    break;
                }
            }

            if (retRowIdx == -1)
            {
                retRowIdx = 0;
            }

            return retRowIdx;
        }

        public void getEstimateProdCost(string unitTypeStr, string auxBoxStr, out decimal tleLaborCostDec,
            out decimal tleBurdenCostDec, out decimal tleProdHoursDec)
        {
            string laborRateStr = "";
            string burdenCostStr = "";
            string prodStdStr = "";

            DataTable dt = new DataTable();

            tleLaborCostDec = 0;
            tleBurdenCostDec = 0;
            tleProdHoursDec = 0;

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetOAU_AssemblyOperationsData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UnitType", unitTypeStr);
                        cmd.Parameters.AddWithValue("@AuxBox", auxBoxStr);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                                foreach(DataRow dr in dt.Rows)
                                {
                                    laborRateStr = dr["LaborRate"].ToString();
                                    burdenCostStr = dr["BurdenCost"].ToString();
                                    prodStdStr = dr["ProductionStd"].ToString();

                                    tleLaborCostDec += (decimal.Parse(laborRateStr) * decimal.Parse(prodStdStr));
                                    tleBurdenCostDec += (decimal.Parse(burdenCostStr) * decimal.Parse(prodStdStr));
                                    tleProdHoursDec += decimal.Parse(prodStdStr);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //todo: needs review to determine if we are going to continue using assembly table or use epicor                      
        }       
 

        public void CalculateMonitorAmpacityValues(out string minCKTAmpStr, out string mfsMCBStr, out string rawMopStr, string modelNo)
        {            
            string largestMotorTypeStr = "Compressor";           
            string comp1StdRLAStr = "0";
            string comp2StdRLAStr = "0";
            string comp3StdRLAStr = "0";
            string comp1HighRLAStr = "0";
            string comp2HighRLAStr = "0";
            string exhaustFLAStr = "0";
            string condFanStdFLAStr = "0";
            string condFanOvrFLAStr = "0";
            string airXchangeWheelFLAStr = "0";
            string semcoWheelFLAStr = "0";
            string ddSupplyFanStdFLAStr = "0";
            string ddSupplyFanOvrFLAStr = "0";
            string beltSupplyFanStdFLAStr = "0";
            string beltSupplyFanOvrFLAStr = "0";
            string digit37_AirXchangeValues = "A,B,C,D,E";
            string digit37_SemcoValues = "N,P,Q,R,S";
            string preHeatFLAStr = "0";
            string electricHeatFLAStr = "0";
            string coolingMcaStr = "";
            string coolingMopStr = "";
            string coolingRawMopStr = "";

            double largestMotorLoadDbl = 0;            
            double comp1StdRLADbl = 0;
            double comp2StdRLADbl = 0;
            double comp3StdRLADbl = 0;
            double comp1HighRLADbl = 0;
            double comp2HighRLADbl = 0;          
            double condFanStdFLADbl = 0;
            double condFanOvrFLADbl = 0;
            double ddSupplyFanStdFLADbl = 0;
            double ddSupplyFanOvrFLADbl = 0;
            double beltSupplyFanStdFLADbl = 0;
            double beltSupplyFanOvrFLADbl = 0;            
            double exhaustFLADbl = 0;           
            double airXchangeWheelFLADbl = 0;
            double semcoWheelFLADbl = 0;
            double airXchangeOrSemcoFLADbl = 0;
            double preHeatFLADbl = 0;
            double electricHeatFLADbl = 0;
            double mcaAmpsDbl = 0;
            double mopDbl = 0;
            double controlAmpDbl;
            
            double fansOnlyFlaDbl = 0;
            double fansOnlyMcaDbl = 0;
            double fansOnlyRawMopDbl = 0;
            double elecHeatMcaDbl = 0;
            double elecHeatMopDbl = 0;            

            Digit1 = modelNo.Substring(0,1);
            Digit2 = modelNo.Substring(1,1);
            Digit456 = modelNo.Substring(3,3);
            Digit7 = modelNo.Substring(6,1);
            Digit8 = modelNo.Substring(7,1);
            Digit10 = modelNo.Substring(9,1);
            Digit22 = modelNo.Substring(21, 1);
            Digit32 = modelNo.Substring(31, 1);
            Digit33 = modelNo.Substring(32, 1);
            Digit37 = modelNo.Substring(36, 1);

            DataTable dt = MonitorGetCompAndMotorData();

            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                ddSupplyFanStdFLAStr = dr["DD_SupplyFanStdFLA"].ToString();
                ddSupplyFanOvrFLAStr = dr["DD_SupplyFanOvrFLA"].ToString();

                ddSupplyFanStdFLADbl = double.Parse(ddSupplyFanStdFLAStr);
                ddSupplyFanOvrFLADbl = double.Parse(ddSupplyFanOvrFLAStr);

                if (Digit1 == "Y")
                {
                    if (Digit8 == "3")
                    {
                        electricHeatFLADbl = 6.25;
                    }
                    else if (Digit8 == "4")
                    {
                        electricHeatFLADbl = 3.13;
                    }
                    else
                    {
                        controlAmpDbl = 2.5;
                    }
                }
                else
                {
                    electricHeatFLAStr = dr["ElectricHeatFLA"].ToString();
                    electricHeatFLADbl = double.Parse(electricHeatFLAStr);
                }

                beltSupplyFanStdFLAStr = dr["Belt_SupplyFanStdFLA"].ToString();
                beltSupplyFanOvrFLAStr = dr["Belt_SupplyFanOvrFLA"].ToString();

                beltSupplyFanStdFLADbl = double.Parse(beltSupplyFanStdFLAStr);
                beltSupplyFanOvrFLADbl = double.Parse(beltSupplyFanOvrFLAStr);               

                condFanStdFLAStr = dr["CondFanStdFLA"].ToString();
                condFanOvrFLAStr = dr["CondFanHighFLA"].ToString();

                condFanStdFLADbl = double.Parse(condFanStdFLAStr);
                condFanOvrFLADbl = double.Parse(condFanOvrFLAStr); 

                comp1StdRLAStr = dr["Compr1StdRLA"].ToString();
                comp2StdRLAStr = dr["Compr2StdRLA"].ToString();
                comp3StdRLAStr = dr["Compr3StdRLA"].ToString();

                comp1StdRLADbl = double.Parse(comp1StdRLAStr);
                comp2StdRLADbl = double.Parse(comp2StdRLAStr);
                comp3StdRLADbl = double.Parse(comp3StdRLAStr); 

                comp1HighRLAStr = dr["Compr1HighRLA"].ToString();
                comp2HighRLAStr = dr["Compr2HighRLA"].ToString();

                comp1HighRLADbl = double.Parse(comp1HighRLAStr);
                comp2HighRLADbl = double.Parse(comp2HighRLAStr);

                exhaustFLAStr = dr["ExhaustFanFLA"].ToString();
                exhaustFLADbl = double.Parse(exhaustFLAStr);

                semcoWheelFLAStr = dr["SemcoWheelFLA"].ToString();
                semcoWheelFLADbl = double.Parse(semcoWheelFLAStr);

                airXchangeWheelFLAStr = dr["AirXchangeWheelFLA"].ToString();
                airXchangeWheelFLADbl = double.Parse(airXchangeWheelFLAStr);

                preHeatFLAStr = dr["PreHeatFLA"].ToString();
                preHeatFLADbl = double.Parse(preHeatFLAStr);
            }

            CalculateMonitorFansOnlyAmpacityValues(out fansOnlyFlaDbl, out fansOnlyMcaDbl, out fansOnlyRawMopDbl, modelNo, ddSupplyFanStdFLADbl, ddSupplyFanOvrFLADbl,
                                                   beltSupplyFanStdFLADbl, beltSupplyFanOvrFLADbl, exhaustFLADbl, semcoWheelFLADbl, airXchangeWheelFLADbl);

            CalculateMonitorElecHeatAmpacityValues(out elecHeatMcaDbl, out elecHeatMopDbl, modelNo, ddSupplyFanStdFLADbl, ddSupplyFanOvrFLADbl,
                                                   beltSupplyFanStdFLADbl, beltSupplyFanOvrFLADbl, exhaustFLADbl, semcoWheelFLADbl, airXchangeWheelFLADbl, preHeatFLADbl, electricHeatFLADbl);              

            largestMotorLoadDbl = ddSupplyFanStdFLADbl;
            largestMotorTypeStr = "compr1Std";

            if (comp2StdRLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = comp2StdRLADbl;
                largestMotorTypeStr = "comp2Std";
            }

            if (comp3StdRLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = comp3StdRLADbl;
                largestMotorTypeStr = "comp3Std";
            }

            if (comp1HighRLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = comp1HighRLADbl;
                largestMotorTypeStr = "comp1High";
            }

            if (comp2HighRLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = comp2HighRLADbl;
                largestMotorTypeStr = "comp2High";
            }

            if (exhaustFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = exhaustFLADbl;
                largestMotorTypeStr = "exhaustFan";
            }

            if (semcoWheelFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = semcoWheelFLADbl;
                largestMotorTypeStr = "semcoWheel";
            }

            if (airXchangeWheelFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = airXchangeWheelFLADbl;
                largestMotorTypeStr = "airXchangeWheel";
            }

            if (condFanStdFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = condFanStdFLADbl;
                largestMotorTypeStr = "condFanStd";
            }

            if (condFanOvrFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = condFanOvrFLADbl;
                largestMotorTypeStr = "condFanOvr";
            }

            if (ddSupplyFanStdFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = ddSupplyFanStdFLADbl;
                largestMotorTypeStr = "ddSupplyFanStd";
            }

            if (ddSupplyFanOvrFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = ddSupplyFanOvrFLADbl;
                largestMotorTypeStr = "ddSupplyFanOvr";
            }

            if (beltSupplyFanStdFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = beltSupplyFanStdFLADbl;
                largestMotorTypeStr = "beltSupplyFanStd";
            }

            if (beltSupplyFanOvrFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = beltSupplyFanOvrFLADbl;
                largestMotorTypeStr = "beltSupplyFanOvr";
            }          
            
            switch (largestMotorTypeStr)
            {
                case "compr1Std":
                    comp1StdRLADbl = 0;
                    break;

                case "compr2Std":
                    comp2StdRLADbl = 0;
                    break;

                case "compr3Std":
                    comp3StdRLADbl = 0;
                    break;

                case "comp1High":
                    comp1HighRLADbl = 0;
                    break;

                case "comp2High":
                    comp2HighRLADbl = 0;
                    break;

                case "exhaustFan":
                    exhaustFLADbl = 0;
                    break;

                case "semcoWheel":
                    semcoWheelFLADbl = 0;
                    break;

                case "airXchangeWheel":
                    airXchangeWheelFLADbl = 0;
                    break;

                case "condFanStd":
                    condFanStdFLADbl = 0;
                    break;

                case "condFanOvr":
                    condFanOvrFLADbl = 0;
                    break;

                case "ddSupplyFanStd":
                    ddSupplyFanStdFLADbl = 0;
                    break;

                case "ddSupplyFanOvr":
                    ddSupplyFanOvrFLADbl = 0;
                    break;

                case "beltSupplyFanStd":
                    beltSupplyFanStdFLADbl = 0;
                    break;

                case "beltSupplyFanOvr":
                    beltSupplyFanOvrFLADbl = 0;
                    break;

                default:
                    break;
            }

            if (Digit8 == "3")
            {
                controlAmpDbl = 2.4;
            }
            else
            {
                controlAmpDbl = 1.0;
            }

            if (digit37_AirXchangeValues.Contains(Digit37))
            {
                airXchangeOrSemcoFLADbl = airXchangeWheelFLADbl;
            }
            else if (digit37_SemcoValues.Contains(Digit37))
            {
                airXchangeOrSemcoFLADbl = semcoWheelFLADbl;
            }            

            mcaAmpsDbl = (1.25 * largestMotorLoadDbl) + comp1StdRLADbl + comp2StdRLADbl + comp3StdRLADbl
                          + comp1HighRLADbl + comp2HighRLADbl + exhaustFLADbl + airXchangeOrSemcoFLADbl + 
                          + condFanStdFLADbl + condFanOvrFLADbl + ddSupplyFanStdFLADbl + ddSupplyFanOvrFLADbl
                          + beltSupplyFanStdFLADbl + beltSupplyFanOvrFLADbl + controlAmpDbl;

            mopDbl = (2.25 * largestMotorLoadDbl) + comp1StdRLADbl + comp2StdRLADbl + comp3StdRLADbl + comp1HighRLADbl + comp2HighRLADbl 
                          + exhaustFLADbl + airXchangeOrSemcoFLADbl + condFanStdFLADbl + condFanOvrFLADbl + ddSupplyFanStdFLADbl + ddSupplyFanOvrFLADbl
                          + beltSupplyFanStdFLADbl + beltSupplyFanOvrFLADbl + controlAmpDbl;

            coolingMcaStr = mcaAmpsDbl.ToString("f2");

            coolingMopStr = roundMOP(mopDbl);

            coolingRawMopStr = mopDbl.ToString("f2");


            if (double.Parse(coolingMcaStr) >= fansOnlyMcaDbl && double.Parse(coolingMcaStr) >= elecHeatMcaDbl)
            {
                minCKTAmpStr = coolingMcaStr;
                mfsMCBStr = coolingMopStr;
                rawMopStr = coolingRawMopStr;
            }
            else if (fansOnlyMcaDbl > double.Parse(coolingMcaStr) && fansOnlyMcaDbl >= elecHeatMcaDbl)
            {
                minCKTAmpStr = fansOnlyMcaDbl.ToString("f2");
                mfsMCBStr = fansOnlyRawMopDbl.ToString("f2");
                rawMopStr = "";
            }
            else
            {
                minCKTAmpStr = elecHeatMcaDbl.ToString("f2");
                mfsMCBStr = elecHeatMopDbl.ToString("f2");
                rawMopStr = "";
            }                      
        }

        private void CalculateMonitorFansOnlyAmpacityValues(out double fansOnlyFlaDbl, out double fansOnlyMcaDbl, out double fansOnlyRawMopDbl, string  modelNo, double ddSupplyFanStdFLADbl, 
                                                            double ddSupplyFanOvrFLADbl, double beltSupplyFanStdFLADbl, double beltSupplyFanOvrFLADbl, double exhaustFLADbl, double semcoWheelFLADbl, 
                                                            double airXchangeWheelFLADbl)                                                                   
        {
            string largestMotorTypeStr = "";
            string digit37_AirXchangeValues = "A,B,C,D,E";
            string digit37_SemcoValues = "N,P,Q,R,S";            
            string mopStr = "";

            double largestMotorLoadDbl = 0;
            double controlAmpDbl = 0;
            double airXchangeOrSemcoFLADbl = 0;           
            double mopDbl = 0;

            if (exhaustFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = exhaustFLADbl;
                largestMotorTypeStr = "exhaustFan";
            }

            if (semcoWheelFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = semcoWheelFLADbl;
                largestMotorTypeStr = "semcoWheel";
            }

            if (airXchangeWheelFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = airXchangeWheelFLADbl;
                largestMotorTypeStr = "airXchangeWheel";
            }           

            if (ddSupplyFanStdFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = ddSupplyFanStdFLADbl;
                largestMotorTypeStr = "ddSupplyFanStd";
            }

            if (ddSupplyFanOvrFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = ddSupplyFanOvrFLADbl;
                largestMotorTypeStr = "ddSupplyFanOvr";
            }

            if (beltSupplyFanStdFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = beltSupplyFanStdFLADbl;
                largestMotorTypeStr = "beltSupplyFanStd";
            }

            if (beltSupplyFanOvrFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = beltSupplyFanOvrFLADbl;
                largestMotorTypeStr = "beltSupplyFanOvr";
            }          
            
            switch (largestMotorTypeStr)
            {                
                case "exhaustFan":
                    exhaustFLADbl = 0;
                    break;

                case "semcoWheel":
                    semcoWheelFLADbl = 0;
                    break;

                case "airXchangeWheel":
                    airXchangeWheelFLADbl = 0;
                    break;

                case "ddSupplyFanStd":
                    ddSupplyFanStdFLADbl = 0;
                    break;

                case "ddSupplyFanOvr":
                    ddSupplyFanOvrFLADbl = 0;
                    break;

                case "beltSupplyFanStd":
                    beltSupplyFanStdFLADbl = 0;
                    break;

                case "beltSupplyFanOvr":
                    beltSupplyFanOvrFLADbl = 0;
                    break;

                default:
                    break;
            }

            Digit8 = modelNo.Substring(7, 1);
            if (Digit8 == "1") 
            {
                controlAmpDbl = 2.4;
            }
            else
            {
                controlAmpDbl = 1.0;
            }

            Digit37 = modelNo.Substring(36, 1);
            if (digit37_AirXchangeValues.Contains(Digit37))
            {
                airXchangeOrSemcoFLADbl = airXchangeWheelFLADbl;
            }
            else if (digit37_SemcoValues.Contains(Digit37))
            {
                airXchangeOrSemcoFLADbl = semcoWheelFLADbl;
            }

            fansOnlyFlaDbl = largestMotorLoadDbl + exhaustFLADbl + airXchangeOrSemcoFLADbl + ddSupplyFanStdFLADbl + ddSupplyFanOvrFLADbl +
                             beltSupplyFanStdFLADbl + beltSupplyFanOvrFLADbl + controlAmpDbl;

            fansOnlyMcaDbl = (0.25 * largestMotorLoadDbl) + exhaustFLADbl + airXchangeOrSemcoFLADbl + ddSupplyFanStdFLADbl + ddSupplyFanOvrFLADbl +
                              beltSupplyFanStdFLADbl + beltSupplyFanOvrFLADbl + controlAmpDbl;

            fansOnlyRawMopDbl = (1.25 * largestMotorLoadDbl) + exhaustFLADbl + airXchangeOrSemcoFLADbl + ddSupplyFanStdFLADbl + ddSupplyFanOvrFLADbl +
                                 beltSupplyFanStdFLADbl + beltSupplyFanOvrFLADbl  + controlAmpDbl;
                     
        }

        private void CalculateMonitorElecHeatAmpacityValues(out double elecHeatMcaDbl, out double elecHeatMopDbl, string modelNo, double ddSupplyFanStdFLADbl,
                                                            double ddSupplyFanOvrFLADbl, double beltSupplyFanStdFLADbl, double beltSupplyFanOvrFLADbl, double exhaustFLADbl, double semcoWheelFLADbl,
                                                            double airXchangeWheelFLADbl, double preHeatFLADbl, double electricHeatFLADbl)
        {
            string largestMotorTypeStr = "";
            string digit37_AirXchangeValues = "A,B,C,D,E";
            string digit37_SemcoValues = "N,P,Q,R,S";
            string mcaStr = "";

            double largestMotorLoadDbl = 0;
            double controlAmpDbl = 0;
            double airXchangeOrSemcoFLADbl = 0;
            double mopDbl = 0;

            if (exhaustFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = exhaustFLADbl;
                largestMotorTypeStr = "exhaustFan";
            }

            if (semcoWheelFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = semcoWheelFLADbl;
                largestMotorTypeStr = "semcoWheel";
            }

            if (airXchangeWheelFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = airXchangeWheelFLADbl;
                largestMotorTypeStr = "airXchangeWheel";
            }

            if (ddSupplyFanStdFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = ddSupplyFanStdFLADbl;
                largestMotorTypeStr = "ddSupplyFanStd";
            }

            if (ddSupplyFanOvrFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = ddSupplyFanOvrFLADbl;
                largestMotorTypeStr = "ddSupplyFanOvr";
            }

            if (beltSupplyFanStdFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = beltSupplyFanStdFLADbl;
                largestMotorTypeStr = "beltSupplyFanStd";
            }

            if (beltSupplyFanOvrFLADbl > largestMotorLoadDbl)
            {
                largestMotorLoadDbl = beltSupplyFanOvrFLADbl;
                largestMotorTypeStr = "beltSupplyFanOvr";
            }

            switch (largestMotorTypeStr)
            {
                case "exhaustFan":
                    exhaustFLADbl = 0;
                    break;

                case "semcoWheel":
                    semcoWheelFLADbl = 0;
                    break;

                case "airXchangeWheel":
                    airXchangeWheelFLADbl = 0;
                    break;

                case "ddSupplyFanStd":
                    ddSupplyFanStdFLADbl = 0;
                    break;

                case "ddSupplyFanOvr":
                    ddSupplyFanOvrFLADbl = 0;
                    break;

                case "beltSupplyFanStd":
                    beltSupplyFanStdFLADbl = 0;
                    break;

                case "beltSupplyFanOvr":
                    beltSupplyFanOvrFLADbl = 0;
                    break;

                default:
                    break;
            }

            Digit8 = modelNo.Substring(7, 1);
            if (Digit8 == "3")
            {
                controlAmpDbl = 2.4;
            }
            else
            {
                controlAmpDbl = 1.0;
            }

            Digit37 = modelNo.Substring(36, 1);
            if (digit37_AirXchangeValues.Contains(Digit37))
            {
                airXchangeOrSemcoFLADbl = airXchangeWheelFLADbl;
            }
            else if (digit37_SemcoValues.Contains(Digit37))
            {
                airXchangeOrSemcoFLADbl = semcoWheelFLADbl;
            }

            elecHeatMcaDbl = 1.25 * (preHeatFLADbl + electricHeatFLADbl + exhaustFLADbl + airXchangeOrSemcoFLADbl + ddSupplyFanStdFLADbl + ddSupplyFanOvrFLADbl
                                   + beltSupplyFanStdFLADbl + beltSupplyFanOvrFLADbl + controlAmpDbl);
            mcaStr = roundElecMOP(elecHeatMcaDbl);

            elecHeatMcaDbl = double.Parse(mcaStr);

            elecHeatMopDbl = (1.25 * largestMotorLoadDbl) + preHeatFLADbl + electricHeatFLADbl + exhaustFLADbl + airXchangeOrSemcoFLADbl + ddSupplyFanStdFLADbl + ddSupplyFanOvrFLADbl
                                 + beltSupplyFanStdFLADbl + beltSupplyFanOvrFLADbl + controlAmpDbl;            
        }

        public string CalculateMSP_AmpacityValues(DataTable dtBOM, string modelNo, out string mfsMCBStr, out string rawMopStr )
        {
            string largestMotorTypeStr = "SupplyFan";           
           
            string supplyFanFLAStr = "0";
            string supplyFanQtyStr = "0";           
            string electricHeatFLAStr = "0";
            string minCKTAmpStr = "0";
            string partNumStr = "";
            string partCategoryStr = "";
            string qtyStr = "";
            string kwStr = "";
            string Digit17Values = "A,B,C";

            int voltageInt = 0;
            int phaseInt = 0;
            int supplyFanQtyInt = 0;

            decimal hertzDec = 0;

            double largestLoadDbl = 0;                        
            double supplyFanFLADbl = 0;           
            double electricHeatFLADbl = 0;
            double mcaAmpsDbl = 0;
            double mopDbl = 0;
            double controlAmpDbl;
            double tempDbl = 0;

            switch (modelNo.Substring(8, 1))
            {
                case "1":
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
                //case "2":
                //    voltageInt = 230;
                //    hertzDec = 60;
                //    phaseInt = 3;
                //    break;
                case "3":
                    voltageInt = 460;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
                case "4":
                    voltageInt = 575;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
                //case "5":
                //    voltageInt = 115;
                //    hertzDec = 60;
                //    phaseInt = 1;
                //    break;
                //case "6":
                //    voltageInt = 208;
                //    hertzDec = 60;
                //    phaseInt = 1;
                //    break;
                //case "7":
                //    voltageInt = 208;
                //    hertzDec = 50;
                //    phaseInt = 3;
                //    break;
                //case "8":
                //    voltageInt = 380;
                //    hertzDec = 50;
                //    phaseInt = 3;
                //    break;
                //case "9":
                //    voltageInt = 208;
                //    hertzDec = 50;
                //    phaseInt = 1;
                //    break;
                default:
                    voltageInt = 208;
                    hertzDec = 60;
                    phaseInt = 3;
                    break;
            }

            foreach (DataRow dr in dtBOM.Rows)
            {
                partNumStr = dr["PartNum"].ToString();
              
                partCategoryStr = dr["PartCategory"].ToString();              

                //if (partNumStr == "VCPRFG-0991")
                //{
                //    partCategoryStr = dr["PartCategory"].ToString();
                //}

                if (dr["PartCategory"].ToString().Contains("Motor") || dr["PartCategory"].ToString().Contains("Mtr"))
                {                    
                    DataTable dtMotor = GetMotorDataDTL(voltageInt, hertzDec, phaseInt, "REV6", partNumStr);

                    if (dtMotor.Rows.Count > 0)
                    {
                        DataRow drMotor = dtMotor.Rows[0];
                        if (dr["PartType"].ToString() == "SupplyFan")
                        {
                            qtyStr = dr["ReqQty"].ToString();
                            supplyFanQtyStr = qtyStr.Substring(0, qtyStr.IndexOf('.'));
                            supplyFanQtyInt = Int32.Parse(supplyFanQtyStr);
                            supplyFanFLAStr = drMotor["FLA"].ToString();
                            supplyFanFLADbl = double.Parse(supplyFanFLAStr);
                        }
                    }
                }                
            }                     

            try
            {
                if (Digit17Values.Contains(modelNo.Substring(16, 1)) == true)  // If Digit 17 equals A, B, or C
                {
                    DataTable dtModel = MSP_GetModelNumDesc(17, "NA", modelNo.Substring(16, 1));

                    if (dtModel.Rows.Count > 0) // Calculate Heating electric input value.
                    {
                        DataRow drModel;
                        drModel = dtModel.Rows[0];

                        kwStr = drModel["DigitDescription"].ToString();
                        kwStr = stripKwFromDescription(kwStr, voltageInt, "MSP");

                        kwStr = kwStr + "000";
                        tempDbl = double.Parse(kwStr) / double.Parse(voltageInt.ToString());
                        tempDbl = tempDbl / Math.Sqrt(3);
                        electricHeatFLADbl = tempDbl;                                         
                    } 
                }
                else
                {
                    electricHeatFLADbl = 0;                    
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error Reading MSP_ModelNumDesc Table for digit 17 - " + f);
            }           

            if (modelNo.Substring(6,1) == "1")
            {
                controlAmpDbl = 2.4;
            }
            else
            {
                controlAmpDbl = 1.0;
            }

            if (modelNo.Substring(15, 1) == "3")  //IF Digit16 equals 3 then unit has electric heat.
            {
                mcaAmpsDbl = 1.25 * (supplyFanFLADbl + electricHeatFLADbl + controlAmpDbl);
                mfsMCBStr = roundElecMOP(mcaAmpsDbl);
            }
            else
            {               
               largestLoadDbl = supplyFanFLADbl;
               --supplyFanQtyInt;
               
                mcaAmpsDbl = (1.25 * largestLoadDbl) + (electricHeatFLADbl + (supplyFanFLADbl * supplyFanQtyInt) + controlAmpDbl);

                mopDbl = (2.25 * largestLoadDbl) + (electricHeatFLADbl + (supplyFanFLADbl * supplyFanQtyInt) + controlAmpDbl);
                mfsMCBStr = roundMOP(mopDbl);
            }            
           
            minCKTAmpStr = mcaAmpsDbl.ToString("f2");
           
            rawMopStr = mopDbl.ToString("f2");

            return minCKTAmpStr;
        }

        public DataTable SortBOM_SetupSubAssemblys(DataTable dtBOM)
        {
            string parentPartNum = "";
            string tempParent = "";
            string tempGen = "";
            string curParentPartNum = "nothing";
            string subAsmPart = "";
            string asmSeqStr = "";
            string tempPartNum = "";

            int asmSeqInt = 0;

            int seqNoInt = 0;
            int eqPos = -1;
            int parLen = 0;
            int generationInt = -1;

            DataView dv = dtBOM.DefaultView;
            dv.Sort = "AssemblySeq,ParentPartNum";
            dtBOM = dv.ToTable();            

            foreach (DataRow dr in dtBOM.Rows)   // locate all of the parent part numbers in the BOM and record them to a list for future use.
            {
                parentPartNum = dr["ParentPartNum"].ToString();
                subAsmPart = dr["SubAssemblyPart"].ToString();
                asmSeqStr = dr["AssemblySeq"].ToString();
                tempPartNum = dr["PartNum"].ToString(); ;

                if (asmSeqStr != "0")
                {
                    eqPos = parentPartNum.IndexOf("=");
                    if (eqPos > 0)
                    {
                        parLen = parentPartNum.Length;
                        tempParent = parentPartNum.Substring(0, (eqPos + 2));
                        tempParent = tempParent + parentPartNum.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                    }
                    else
                    {
                        tempParent = parentPartNum;
                    }

                    if (parentPartNum.Contains("EXHFANASM") == true)
                    {
                        string taco = "bell";
                    }

                    if (parentPartNum != curParentPartNum)
                    {
                        curParentPartNum = parentPartNum;
                        seqNoInt = 0;
                        ++asmSeqInt;
                    }

                    if (asmSeqInt == 0)
                    {
                        if (dr["PartType"].ToString().StartsWith("VMEASM"))
                        {
                            dr["ParentPartNum"] = dr["PartType"];
                        }
                    }

                    dr["AssemblySeq"] = asmSeqInt;
                    seqNoInt += 10;
                    dr["SeqNo"] = seqNoInt;
                    //dr["ParentPartNum"] = tempParent;
                }
                else
                {
                    dr["AssemblySeq"] = 0;
                    seqNoInt += 10;
                    dr["SeqNo"] = seqNoInt;
                }
            }

            return dtBOM;
        }

        public DataTable ProcessSubAssemblies(DataTable dtBOM)
        {
            string generationStr = "";
            string partNumStr = "";
            string parentPartNumStr = "";
            string curParentPartNumStr = "";
            string secondGenParentPartNumStr = "";
            string revisionNumStr = "";
            string mtlRevNumStr = "";
            string ruleHeadIdStr = "";
            string ruleBatchIdStr = "";
            string mtlPartNumStr = "";
            string asmUOMStr = "";
            string asmQtyStr = "";
            string partQtyStr = "";
            string immedParentStr = "";
            string partCategory = "";
            string stationLoc = "";

            int generationInt = 0;
            int curGenerationInt = 0;
            int seqNumInt = 0;
            int asmSeqInt = 0;
            int curAsmSeqInt = 0;
            int eqPos = -1;
            int parentLen = 0;

            bool firstAsm = true;
            bool subAsmPart = false;
            bool subAsmMtlPart = false;
            bool preConfigAsm = false;

            decimal partQtyDec = 0;
            decimal asmQtyDec = 0;

            DataTable dtNewBOM = dtBOM.Clone();

            foreach (DataRow drPrt in dtBOM.Rows)
            {
                partCategory = drPrt["PartCategory"].ToString();
                partNumStr = drPrt["PartNum"].ToString();
                asmSeqInt = (int)drPrt["AssemblySeq"];
                partQtyDec = (decimal)drPrt["QtyPer"];
                parentPartNumStr = drPrt["ParentPartNum"].ToString();
                revisionNumStr = drPrt["RevisionNum"].ToString();
                ruleHeadIdStr = drPrt["RuleHeadID"].ToString();
                ruleBatchIdStr = drPrt["RuleBatchID"].ToString();
                generationInt = (int)drPrt["Generation"];
                immedParentStr = drPrt["ImmediateParent"].ToString();
                subAsmPart = (bool)drPrt["SubAssemblyPart"];
                subAsmMtlPart = (bool)drPrt["SubAsmMtlPart"];
                preConfigAsm = (bool)drPrt["PreConfigAsm"];
                stationLoc = drPrt["StationLoc"].ToString();
             
                if (drPrt["QtyPer"] == null)
                {
                    partQtyDec = 0;
                }
                else
                {
                    partQtyStr = drPrt["QtyPer"].ToString();
                    partQtyDec = decimal.Parse(partQtyStr);
                }

                if (asmSeqInt == 0)
                {
                    seqNumInt += 10;
                    dtNewBOM = AddNewRowToBOM(dtNewBOM, drPrt, partNumStr, curAsmSeqInt, seqNumInt, partQtyDec, revisionNumStr, parentPartNumStr, 
                                              immedParentStr, ruleBatchIdStr, subAsmPart, subAsmMtlPart, preConfigAsm, generationInt);                  
                }
                else
                {
                    if (preConfigAsm == true)
                    {
                        dtNewBOM = AddPreConfigAssemblyBOM(dtNewBOM, partNumStr, parentPartNumStr, immedParentStr, revisionNumStr, 
                                                           ruleBatchIdStr, generationInt.ToString(), curAsmSeqInt, out curAsmSeqInt, 
                                                           partCategory);
                    }
                    else if (subAsmPart == true && subAsmMtlPart == true)
                    {
                        if (parentPartNumStr != curParentPartNumStr)
                        {
                            seqNumInt = 10;
                            ++curAsmSeqInt;
                            curParentPartNumStr = parentPartNumStr;
                        }
                        else
                        {
                            seqNumInt += 10;
                        }
                        dtNewBOM = AddNewRowToBOM(dtNewBOM, drPrt, partNumStr, curAsmSeqInt, seqNumInt, partQtyDec, revisionNumStr, parentPartNumStr, immedParentStr, 
                                                  ruleBatchIdStr, subAsmPart, subAsmMtlPart, preConfigAsm, generationInt);                               
                    }
                    else if (subAsmPart == true)
                    {
                        DataTable dtPartBOM = GetPartMtlBOM(partNumStr, revisionNumStr, parentPartNumStr);
                        ++curAsmSeqInt;
                       
                        if (dtPartBOM.Rows.Count > 0)
                        {                            
                            eqPos = parentPartNumStr.IndexOf("=");
                            if (eqPos > 0)
                            {
                                //generationStr = parentPartNumStr.Substring((eqPos + 2), 1);
                                if (generationInt > 1)
                                {
                                    parentLen = parentPartNumStr.Length;
                                    secondGenParentPartNumStr = parentPartNumStr.Substring((eqPos + 3), (parentLen - (eqPos + 3)));
                                    if (partNumStr != secondGenParentPartNumStr)
                                    {
                                        parentPartNumStr = parentPartNumStr.Substring(0, (eqPos + 2)) + generationStr + partNumStr;
                                    }
                                }
                            }

                            seqNumInt = 0;
                            foreach (DataRow row in dtPartBOM.Rows)
                            {
                                if (row["RevisionNum"] == null)
                                {
                                    mtlRevNumStr = "";
                                }
                                else
                                {
                                    mtlRevNumStr = row["RevisionNum"].ToString();
                                }

                                mtlPartNumStr = row["MtlPartNum"].ToString();
                                //generationStr = row["Generation"].ToString();
                                //generationInt = Int32.Parse(generationStr);

                                if (row["QtyPer"] == null)
                                {
                                    partQtyDec = 0;
                                }
                                else
                                {
                                    partQtyStr = drPrt["QtyPer"].ToString();
                                    partQtyDec = decimal.Parse(partQtyStr);
                                }

                                seqNumInt += 10;
                                dtNewBOM = AddNewRowToBOM(dtNewBOM, drPrt, mtlPartNumStr, curAsmSeqInt, seqNumInt, partQtyDec, revisionNumStr, 
                                                          parentPartNumStr, immedParentStr, ruleBatchIdStr, subAsmPart, subAsmMtlPart, preConfigAsm, generationInt);                                                            
                            }
                        }
                        //else
                        //{

                        //    dtNewBOM = AddNewRowToBOM(dtNewBOM, drPrt, curAsmSeqInt, seqNumInt, 1, revisionNumStr, parentPartNumStr, immedParentStr, 
                        //                              ruleBatchIdStr, subAsmPart, subAsmMtlPart, preConfigAsm, 0);              
                        //}
                    }
                    else
                    {
                        if (parentPartNumStr != curParentPartNumStr)
                        {
                            seqNumInt = 0;
                            ++curAsmSeqInt;
                            curParentPartNumStr = parentPartNumStr;
                        }
                       
                        seqNumInt += 10;                     
                        dtNewBOM = AddNewRowToBOM(dtNewBOM, drPrt, partNumStr, curAsmSeqInt, seqNumInt, 1, revisionNumStr, parentPartNumStr, immedParentStr,
                                                  ruleBatchIdStr, subAsmPart, subAsmMtlPart, preConfigAsm, 0);              

                    }
                }
            }
                     
            return dtNewBOM;
        }

        public DataTable AddNewRowToBOM(DataTable dtBOM, DataRow drPrt, string partNumStr, int curAsmSeqInt, int seqNumInt, decimal asmQtyDec, string revNumStr, string parentPartNumStr,
                                        string immedParentStr, string ruleBatchIdStr, bool subAsmPart, bool subAsmMtlPart, bool preConfigAsm, int generationInt)
        {
            string partQtyStr = "";

            decimal partQtyDec = 0;

            //if (drPrt["QtyPer"] == null)
            //{
            //    partQtyDec = 0;
            //}
            //else
            //{
            //    partQtyStr = drPrt["QtyPer"].ToString();
            //    partQtyDec = decimal.Parse(partQtyStr);
            //}

            var dr = dtBOM.NewRow();            
            dr["PartCategory"] = drPrt["PartCategory"].ToString();
            dr["AssemblySeq"] = curAsmSeqInt;
            dr["SeqNo"] = seqNumInt;
            dr["PartNum"] = partNumStr;
            dr["Description"] = drPrt["Description"].ToString();
            dr["QtyPer"] = asmQtyDec;
            //partQtyDec = partQtyDec * asmQtyDec;
            dr["ReqQty"] = asmQtyDec;
            dr["UOM"] = drPrt["UOM"].ToString();
            dr["UnitCost"] = decimal.Parse(drPrt["UnitCost"].ToString());
            dr["WhseCode"] = "LWH5";
            dr["RelOp"] = (int)drPrt["RelOp"];
            dr["PartStatus"] = drPrt["PartStatus"].ToString();
            dr["RevisionNum"] = revNumStr;
            dr["CostMethod"] = drPrt["CostMethod"].ToString();
            dr["PartType"] = drPrt["PartType"].ToString();
            dr["Status"] = drPrt["Status"].ToString();
            dr["PROGRESS_RECID"] = 0;
            dr["RuleHeadID"] = 0;
            dr["RuleBatchID"] = Int32.Parse(ruleBatchIdStr);
            dr["ParentPartNum"] = parentPartNumStr;
            dr["ImmediateParent"] = immedParentStr;
            dr["StationLoc"] = drPrt["StationLoc"].ToString();
            dr["SubAssemblyPart"] = subAsmPart;
            dr["SubAsmMtlPart"] = subAsmMtlPart;
            dr["PreConfigAsm"] = preConfigAsm;
            dr["Generation"] = generationInt;

            dtBOM.Rows.Add(dr);
            return dtBOM;
        }

        public DataTable AddInnerSubAssemblyParts(DataTable dtBOM, int asmSeqInt, out int curAsmSeqInt)
        {
            string generationStr = "";
            string partNumStr = "";
            string parentPartNumStr = "";
            string secondGenParentPartNumStr = "";
            string revisionNumStr = "";
            string mtlRevNumStr = "";
            string ruleHeadIdStr = "";
            string ruleBatchIdStr = "";
            string mtlPartNumStr = "";
            string asmUOMStr = "";
            string asmQtyStr = "";
            string partQtyStr = "";
            string immedParentStr = "";

            List<string> PartNumList = new List<string>();
            List<string> RevNumList = new List<string>();
            List<string> RuleHeadList = new List<string>();
            List<string> RuleBatchList = new List<string>();
            List<DataRow> RowsToRemove = new List<DataRow>();

            int generationInt = 0;
            int curGenerationInt = 0;
            int seqNumInt = 10;
            //int asmSeqInt = 5;
            int eqPos = -1;
            int parentLen = 0;

            bool firstAsm = true;
            bool subAsmPart = false;
            bool subAsmMtlPart = false;
            bool preConfigAsm = false;

            decimal partQtyDec = 0;
            decimal asmQtyDec = 0;

            //DataView dv = dtBOM.DefaultView;
            //dv.Sort = "AssemblySeq,ParentPartNum";
            //dtBOM = dv.ToTable();

            var results = dtBOM.AsEnumerable().Where(dr => dr.Field<Boolean>("SubAssemblyPart") == true && dr.Field<Boolean>("PreConfigAsm") == false && dr.Field<String>("ImmediateParent").Length > 0); //&& dr.Field<Boolean>("SubAsmMtlPart") == true
            
            //var results = dtBOM.AsEnumerable().Where(dr => dr.Field<Boolean>("SubAssemblyPart") == true && dr.Field<String>("PartType").StartsWith("VMEASM")); //&& dr.Field<Boolean>("SubAsmMtlPart") == true

           
            foreach (DataRow row in results)
            {
                RowsToRemove.Add(row);
            }

            foreach (DataRow drtr in RowsToRemove)
            {
                partNumStr = drtr["PartNum"].ToString();
                parentPartNumStr = drtr["ParentPartNum"].ToString();
                revisionNumStr = drtr["RevisionNum"].ToString();
                ruleHeadIdStr = drtr["RuleHeadID"].ToString();
                ruleBatchIdStr = drtr["RuleBatchID"].ToString();
                generationStr = drtr["Generation"].ToString();
                immedParentStr = drtr["ImmediateParent"].ToString();
                subAsmPart = (bool)drtr["SubAssemblyPart"];
                subAsmMtlPart = (bool)drtr["SubAsmMtlPart"];
                preConfigAsm = (bool)drtr["PreConfigAsm"];

                //if (preConfigAsm == true)
                //{
                //    dtBOM = AddPreConfigAssemblyBOM(dtBOM, partNumStr, parentPartNumStr, revisionNumStr, ruleBatchIdStr, generationStr);
                //}
                //else
                //{

                    DataTable dtPartBOM = GetPartMtlBOM(partNumStr, revisionNumStr, parentPartNumStr);
                    ++asmSeqInt;

                    //DataTable dtPartBOM = GetPanelAsmBOM(partNumStr, revisionNumStr, 1);

                    if (dtPartBOM.Rows.Count > 0)
                    {
                        if (preConfigAsm == true)
                        {
                            eqPos = parentPartNumStr.IndexOf("=");
                            if (eqPos > 0)
                            {
                                //generationStr = parentPartNumStr.Substring((eqPos + 2), 1);
                                if (Int32.Parse(generationStr) > 1)
                                {
                                    parentLen = parentPartNumStr.Length;
                                    secondGenParentPartNumStr = parentPartNumStr.Substring((eqPos + 2), (parentLen - (eqPos + 2)));
                                    if (partNumStr != secondGenParentPartNumStr)
                                    {
                                        parentPartNumStr = parentPartNumStr.Substring(0, (eqPos + 2)) + generationStr + partNumStr;
                                    }
                                }
                            }
                        }

                        //asmSeqInt = GetAsmSeq(dtBOM, asmSeqInt);

                        foreach (DataRow drPrt in dtPartBOM.Rows)
                        {
                            if (drPrt["RevisionNum"] == null)
                            {
                                mtlRevNumStr = "";
                            }
                            else
                            {
                                mtlRevNumStr = drPrt["RevisionNum"].ToString();
                            }

                            mtlPartNumStr = drPrt["MtlPartNum"].ToString();
                            //generationStr = drPrt["Generation"].ToString();
                            generationInt = Int32.Parse(generationStr);

                            if (drPrt["QtyPer"] == null)
                            {
                                partQtyDec = 0;
                            }
                            else
                            {
                                partQtyStr = drPrt["QtyPer"].ToString();
                                partQtyDec = decimal.Parse(partQtyStr);
                            }

                            if (curGenerationInt != generationInt)
                            {
                                curGenerationInt = generationInt;

                                seqNumInt = 10;
                                if (firstAsm)
                                {
                                    firstAsm = false;
                                    asmQtyDec = 1;
                                }
                                else
                                {
                                    DataTable dtAsm = GetPanelAsmBOM(partNumStr, revisionNumStr, 0);
                                    foreach (DataRow row in dtAsm.Rows)
                                    {
                                        if (Int32.Parse(row["Generation"].ToString()) == (curGenerationInt - 1))
                                        {
                                            asmQtyStr = row["QtyPer"].ToString();
                                            asmQtyDec = decimal.Parse(asmQtyStr);
                                            asmUOMStr = row["UOM"].ToString();
                                            break;
                                        }
                                    }
                                    ++asmSeqInt;
                                }
                            }

                            var dr = dtBOM.NewRow();
                            dr["PartCategory"] = "Panel Assembly";
                            dr["AssemblySeq"] = asmSeqInt;
                            dr["SeqNo"] = seqNumInt;
                            seqNumInt += 10;
                            dr["PartNum"] = drPrt["MtlPartNum"].ToString();
                            dr["Description"] = drPrt["PartDescription"].ToString();
                            dr["QtyPer"] = partQtyDec;
                            partQtyDec = partQtyDec * asmQtyDec;
                            dr["ReqQty"] = partQtyDec;
                            dr["UOM"] = drPrt["UOM"].ToString();
                            dr["UnitCost"] = decimal.Parse(drPrt["UnitCost"].ToString());
                            dr["WhseCode"] = "LWH5";
                            dr["RelOp"] = 10;
                            dr["PartStatus"] = "Active";
                            dr["RevisionNum"] = mtlRevNumStr;
                            dr["CostMethod"] = "A";
                            dr["PartType"] = "";
                            dr["Status"] = "BackFlush";
                            dr["PROGRESS_RECID"] = 0;
                            dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                            dr["RuleBatchID"] = Int32.Parse(ruleBatchIdStr);
                            //dr["ParentPartNum"] = drPrt["ParentPartNum"].ToString(); // 02032020 - Old methos
                            dr["ParentPartNum"] = parentPartNumStr;
                            dr["SubAssemblyPart"] = subAsmPart;
                            dr["SubAsmMtlPart"] = subAsmMtlPart;
                            dr["PreConfigAsm"] = preConfigAsm;
                            dr["ImmediateParent"] = immedParentStr;
                            dr["Generation"] = generationStr;

                            dtBOM.Rows.Add(dr);
                        }
                    }
                //}
            }

            foreach (DataRow row in RowsToRemove)
            {
                dtBOM.Rows.Remove(row);
            }

            curAsmSeqInt = asmSeqInt;
            return dtBOM;
        }

        public DataTable AddPanelAssemblyBOM(DataTable dtBOM, int curAsmSeqInt)
        {
            string generationStr = "";
            string partNumStr = "";
            string parentPartNumStr = "";
            string immedParentPartNumStr = "";
            string curImmedParentPartNumStr = "";
            string revisionNumStr = "";
            string mtlRevNumStr = "";
            string ruleHeadIdStr = "";
            string ruleBatchIdStr = "";
            string mtlPartNumStr = "";
            string asmUOMStr = "";
            string asmQtyStr = "";
            string partQtyStr = "";

            List<string> PartNumList = new List<string>();
            List<string> RevNumList = new List<string>();
            List<string> RuleHeadList = new List<string>();
            List<string> RuleBatchList = new List<string>();
            List<DataRow> RowsToRemove = new List<DataRow>();

            int generationInt = 0;
            int curGenerationInt = 0;
            int seqNumInt = 10;
            int asmSeqInt = 5;
            int eqPos = -1;
            int parLen = 0;           

            bool firstAsm = true;
            bool preConfigAsm = false;

            decimal partQtyDec = 0;
            decimal asmQtyDec = 0;

            //var results = dtBOM.AsEnumerable().Where(dr => dr.Field<string>("PartNum").Contains("PNLASM") == true);
            var results = dtBOM.AsEnumerable().Where(dr => dr.Field<Boolean>("SubAssemblyPart") == true && dr.Field<String>("ImmediateParent").Length > 0 && dr.Field<Boolean>("PreConfigAsm") == true);            

            foreach (DataRow row in results)
            {                
                RowsToRemove.Add(row);                
            }

            foreach (DataRow drtr in RowsToRemove)
            {

                partNumStr = drtr["PartNum"].ToString();
                parentPartNumStr = drtr["ParentPartNum"].ToString();
                immedParentPartNumStr = drtr["ImmediateParent"].ToString();
                revisionNumStr = drtr["RevisionNum"].ToString();
                ruleHeadIdStr = drtr["RuleHeadID"].ToString();
                ruleBatchIdStr = drtr["RuleBatchID"].ToString();
                generationStr = drtr["Generation"].ToString();
                preConfigAsm = (bool)drtr["PreConfigAsm"];

                if (preConfigAsm == true)
                {
                    dtBOM = AddPreConfigAssemblyBOM(dtBOM, partNumStr, parentPartNumStr, immedParentPartNumStr,revisionNumStr, 
                                                    ruleBatchIdStr, generationStr, curAsmSeqInt, out curAsmSeqInt, "Panel Assembly");
                }
                else
                {                   
                    immedParentPartNumStr = drtr["ImmediateParent"].ToString();

                    DataTable dtPnl = GetPanelAsmBOM(partNumStr, revisionNumStr, 1);

                    if (dtPnl.Rows.Count > 0)
                    {

                        //asmSeqInt = GetAsmSeq(dtBOM, asmSeqInt);

                        foreach (DataRow drPnl in dtPnl.Rows)
                        {
                            if (drPnl["RevisionNum"] == null)
                            {
                                mtlRevNumStr = "";
                            }
                            else
                            {
                                mtlRevNumStr = drPnl["RevisionNum"].ToString();
                            }

                            mtlPartNumStr = drPnl["MtlPartNum"].ToString();
                            generationStr = drPnl["Generation"].ToString();
                            generationInt = Int32.Parse(generationStr);
                            parentPartNumStr = drPnl["ParentPartNum"].ToString();

                            if (drPnl["QtyPer"] == null)
                            {
                                partQtyDec = 0;
                            }
                            else
                            {
                                partQtyStr = drPnl["QtyPer"].ToString();
                                partQtyDec = decimal.Parse(partQtyStr);
                            }

                            if (curGenerationInt != generationInt)
                            {
                                curGenerationInt = generationInt;
                                eqPos = parentPartNumStr.IndexOf("=");
                                parLen = parentPartNumStr.Length;

                                if (eqPos > 0)
                                {
                                    if (generationInt == 1)
                                    {
                                        //immedParentPartNumStr = "";
                                    }
                                    else if (generationInt == 2)
                                    {
                                        immedParentPartNumStr = partNumStr;
                                        curImmedParentPartNumStr = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                                    }
                                    else
                                    {
                                        //immedParentPartNumStr = curImmedParentPartNumStr;
                                        curImmedParentPartNumStr = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                                    }
                                }
                                else
                                {
                                    //immedParentPartNumStr = partNumStr;
                                }

                                seqNumInt = 10;
                                if (firstAsm)
                                {
                                    firstAsm = false;
                                    asmQtyDec = 1;
                                }
                                else
                                {
                                    DataTable dtAsm = GetPanelAsmBOM(partNumStr, revisionNumStr, 0);
                                    foreach (DataRow row in dtAsm.Rows)
                                    {
                                        if (Int32.Parse(row["Generation"].ToString()) == (curGenerationInt - 1))
                                        {
                                            asmQtyStr = row["QtyPer"].ToString();
                                            asmQtyDec = decimal.Parse(asmQtyStr);
                                            asmUOMStr = row["UOM"].ToString();
                                            break;
                                        }
                                    }
                                    ++asmSeqInt;
                                }
                            }

                            var dr = dtBOM.NewRow();
                            dr["PartCategory"] = "Panel Assembly";
                            dr["AssemblySeq"] = asmSeqInt;
                            dr["SeqNo"] = seqNumInt;
                            seqNumInt += 10;
                            dr["PartNum"] = drPnl["MtlPartNum"].ToString();
                            dr["Description"] = drPnl["PartDescription"].ToString();
                            dr["QtyPer"] = partQtyDec;

                            //if (drPnl["QtyPer"] == null)
                            //{
                            //    partQtyDec = 0;
                            //}
                            //else
                            //{
                            //    partQtyDec = partQtyDec * asmQtyDec;
                            //}

                            partQtyDec = partQtyDec * asmQtyDec;
                            dr["ReqQty"] = partQtyDec;
                            dr["UOM"] = drPnl["UOM"].ToString();
                            dr["UnitCost"] = decimal.Parse(drPnl["UnitCost"].ToString());
                            dr["WhseCode"] = "LWH5";
                            dr["RelOp"] = 10;
                            dr["PartStatus"] = "Active";
                            dr["RevisionNum"] = mtlRevNumStr;
                            dr["CostMethod"] = "A";
                            dr["PartType"] = "";
                            dr["Status"] = "BackFlush";
                            dr["PROGRESS_RECID"] = 0;
                            dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                            dr["RuleBatchID"] = Int32.Parse(ruleBatchIdStr);
                            dr["ParentPartNum"] = drPnl["ParentPartNum"].ToString();
                            dr["ImmediateParent"] = immedParentPartNumStr;
                            dr["Generation"] = drPnl["Generation"].ToString();
                            dr["SubAssemblyPart"] = false;
                            dr["SubAsmMtlPart"] = false;
                            dtBOM.Rows.Add(dr);
                        }
                    }
                }
            }

            foreach (DataRow row in RowsToRemove)
            {
                dtBOM.Rows.Remove(row);
            }

            return dtBOM;
        }

        public DataTable AddPreConfigAssemblyBOM(DataTable dtBOM, string partNumStr, string parentPartNumStr, string immedParentStr,
                                                 string revisionNumStr, string ruleBatchIdStr, string generationStr, int asmSeqInt, 
                                                 out int curAsmSeqInt, string partCategory)
        {            
            string primParentPartNumStr = "";           
            string secondGenParentPartNumStr = "";
            string immedParentPartNumStr = "";
            string curImmedParentPartNumStr = "";
            string curAsmPartNum = "";
            string asmPartNum = ""; 
            string mtlRevNumStr = "";
            string ruleHeadIdStr = "";            
            string mtlPartNumStr = "";
            string asmUOMStr = "";
            string asmQtyStr = "";
            string partQtyStr = "";

            List<string> PartNumList = new List<string>();
            List<string> RevNumList = new List<string>();
            List<string> RuleHeadList = new List<string>();
            List<string> RuleBatchList = new List<string>();
            List<DataRow> RowsToRemove = new List<DataRow>();

            int generationInt = 0;
            int curGenerationInt = 0;
            int seqNumInt = 10;            
            int eqPos = -1;
            int parLen = 0;

            bool firstAsm = true;
            bool firstIncGen = true; 
            bool incrementGen = false;

            decimal partQtyDec = 0;
            decimal asmQtyDec = 0;           
            
            ruleHeadIdStr = "0";          

            DataTable dtPnl = GetPanelAsmBOM(partNumStr, revisionNumStr, 1);

            if (dtPnl.Rows.Count > 0)
            {
                eqPos = parentPartNumStr.IndexOf("=");
                if (eqPos > 0)
                {
                    primParentPartNumStr = parentPartNumStr.Substring(0, (eqPos - 1));
                    
                    if (Int32.Parse(generationStr) > 1)
                    {
                        incrementGen = true;
                        parLen = parentPartNumStr.Length;
                        secondGenParentPartNumStr = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                        if (partNumStr != secondGenParentPartNumStr)
                        {
                            parentPartNumStr = parentPartNumStr.Substring(0, (eqPos + 2)) + generationStr + partNumStr;
                        }
                    }
                }                      

                foreach (DataRow drPnl in dtPnl.Rows)
                {
                    if (drPnl["RevisionNum"] == null)
                    {
                        mtlRevNumStr = "";
                    }
                    else
                    {
                        mtlRevNumStr = drPnl["RevisionNum"].ToString();
                    }

                    asmPartNum = drPnl["ParentPartNum"].ToString();
                    parLen = asmPartNum.Length;
                    eqPos = asmPartNum.IndexOf("=");
                    if (eqPos > 0)
                    {
                        asmPartNum = asmPartNum.Substring((eqPos + 3), (parLen - (eqPos + 3)));                       
                    }             
                    
                    //parentPartNumStr = drPnl["ParentPartNum"].ToString();
                    mtlPartNumStr = drPnl["MtlPartNum"].ToString();
                    generationStr = drPnl["Generation"].ToString();
                    //generationInt = Int32.Parse(generationStr);

                    if (curAsmPartNum != asmPartNum)
                    {
                        curAsmPartNum = asmPartNum;
                        ++asmSeqInt;
                        seqNumInt = 10;
                        if (incrementGen == true)
                        {
                            generationInt = Int32.Parse(generationStr);
                            ++generationInt;
                        }
                        else
                        {
                            generationInt = Int32.Parse(generationStr);
                        }
                        parentPartNumStr = primParentPartNumStr + " = " + generationInt.ToString() + asmPartNum;                     
                    }                           

                    if (drPnl["QtyPer"] == null)
                    {
                        partQtyDec = 0;
                    }
                    else
                    {
                        partQtyStr = drPnl["QtyPer"].ToString();
                        partQtyDec = decimal.Parse(partQtyStr);
                    }

                    if (curGenerationInt != generationInt)
                    {
                        curGenerationInt = generationInt;
                        eqPos = parentPartNumStr.IndexOf("=");
                        parLen = parentPartNumStr.Length;

                        if (eqPos > 0)
                        {
                            if (generationInt == 1)
                            {
                                //immedParentPartNumStr = "";
                            }
                            else if (generationInt == 2)
                            {
                                //immedParentPartNumStr = partNumStr;
                                if (incrementGen == true)
                                {
                                    if (firstIncGen == true)
                                    {
                                        //immedParentPartNumStr = primParentPartNumStr;
                                        firstIncGen = false;
                                    }
                                }
                               
                                curImmedParentPartNumStr = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                            }
                            else
                            {
                                immedParentPartNumStr = curImmedParentPartNumStr;
                                curImmedParentPartNumStr = parentPartNumStr.Substring((eqPos + 3), (parLen - (eqPos + 3)));
                            }
                        }
                        else
                        {
                            immedParentPartNumStr = partNumStr;
                        }
                       
                        if (firstAsm)
                        {
                            firstAsm = false;
                            asmQtyDec = 1;
                        }
                        else
                        {
                            DataTable dtAsm = GetPanelAsmBOM(partNumStr, revisionNumStr, 0);
                            foreach (DataRow row in dtAsm.Rows)
                            {
                                if (Int32.Parse(row["Generation"].ToString()) == (curGenerationInt - 1))
                                {
                                    asmQtyStr = row["QtyPer"].ToString();
                                    asmQtyDec = decimal.Parse(asmQtyStr);
                                    asmUOMStr = row["UOM"].ToString();
                                    break;
                                }
                            }                          
                        }
                    }

                    var dr = dtBOM.NewRow();
                    dr["PartCategory"] = partCategory;
                    dr["AssemblySeq"] = asmSeqInt;
                    dr["SeqNo"] = seqNumInt;
                    seqNumInt += 10;
                    dr["PartNum"] = drPnl["MtlPartNum"].ToString();
                    dr["Description"] = drPnl["PartDescription"].ToString();
                    dr["QtyPer"] = partQtyDec;

                    //if (drPnl["QtyPer"] == null)
                    //{
                    //    partQtyDec = 0;
                    //}
                    //else
                    //{
                    //    partQtyDec = partQtyDec * asmQtyDec;
                    //}

                    partQtyDec = partQtyDec * asmQtyDec;
                    dr["ReqQty"] = partQtyDec;
                    dr["UOM"] = drPnl["UOM"].ToString();
                    dr["UnitCost"] = decimal.Parse(drPnl["UnitCost"].ToString());
                    dr["WhseCode"] = "LWH5";
                    dr["RelOp"] = 10;
                    dr["PartStatus"] = "Active";
                    dr["RevisionNum"] = mtlRevNumStr;
                    dr["CostMethod"] = "A";
                    dr["PartType"] = "";
                    dr["Status"] = "BackFlush";
                    dr["PROGRESS_RECID"] = 0;
                    dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
                    dr["RuleBatchID"] = Int32.Parse(ruleBatchIdStr);
                    dr["ParentPartNum"] = parentPartNumStr;
                    dr["ImmediateParent"] = immedParentStr;
                    dr["Generation"] = generationInt.ToString();
                    dr["SubAssemblyPart"] = false;
                    dr["SubAsmMtlPart"] = false;
                    dtBOM.Rows.Add(dr);
                }
            }
            
            //}
            //foreach (DataRow row in RowsToRemove)
            //{
            //    dtBOM.Rows.Remove(row);
            //}

            curAsmSeqInt = asmSeqInt;
            return dtBOM;
        }

        public int GetAsmSeq(DataTable dtBOM, int asmSeqInt)
        {
            string asmSeqStr = "";

            int retAsmSeqInt = 0;

            foreach (DataRow row in dtBOM.Rows)
            {
                if (row["AssemblySeq"].ToString() != asmSeqStr)
                {
                    asmSeqStr = row["AssemblySeq"].ToString();
                }
            }
            if (Int32.Parse(asmSeqStr) == asmSeqInt)
            {
                retAsmSeqInt = asmSeqInt;
            }
            else
            {
                retAsmSeqInt = asmSeqInt + 1;
            }

            return retAsmSeqInt;
        }

        public string getCurrentProdLine(DataTable dtJobOper, string tableType)
        {
            string prodLine = "";
            string oprSeq = "";

            if (tableType == "Job")
            {
                foreach (DataRow row in dtJobOper.Rows)
                {
                    if (row["AssemblySeq"].ToString() == "0" && row["OprSeq"].ToString() == "40")
                    {
                        if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "NoDtl")
                        {
                            prodLine = row["ResourceID"].ToString().Substring(0, 2);
                            switch (prodLine)
                            {
                                case "L1":
                                    prodLine = "Line 1";
                                    break;
                                case "L2":
                                    prodLine = "Line 2";
                                    break;
                                case "L3":
                                    prodLine = "Line 3";
                                    break;
                                case "L4":
                                    prodLine = "Line 4";
                                    break;
                                case "LA":
                                    prodLine = "LINEA";
                                    break;
                                case "LB":
                                    prodLine = "LINEB";
                                    break;
                                case "LC":
                                    prodLine = "LINEC";
                                    break;
                                case "LD":
                                    prodLine = "LINED";
                                    break;                               
                                default:
                                    prodLine = "LINEA";
                                    break;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (DataRow row in dtJobOper.Rows)
                {
                    if (row["OprSeq"].ToString() == "40")
                    {
                        if (row["ResourceGrpID"].ToString() != "" || row["ResourceID"].ToString() != "NoDtl")
                        {
                            prodLine = row["ResourceID"].ToString().Substring(0, 2);
                            switch (prodLine)
                            {
                                case "L1":
                                    prodLine = "Line 1";
                                    break;
                                case "L2":
                                    prodLine = "Line 2";
                                    break;
                                case "L3":
                                    prodLine = "Line 3";
                                    break;
                                case "L4":
                                    prodLine = "Line 4";
                                    break;
                                case "LA":
                                    prodLine = "LINEA";
                                    break;
                                case "LB":
                                    prodLine = "LINEB";
                                    break;
                                case "LC":
                                    prodLine = "LINEC";
                                    break;
                                case "LD":
                                    prodLine = "LINED";
                                    break;                     
                                default:
                                    prodLine = "LINEA";
                                    break;
                            }
                        }
                    }
                }
            }

            return prodLine;
        }

        public string findStationLoc(int oprSeq, string unitTypeStr, string prodLine)
        {
            string stationLoc = "ASY 1";

            if (unitTypeStr.StartsWith("HA"))
            {
                switch (oprSeq)
                {
                    case 10:
                        stationLoc = "ASY 1";
                        break;
                    case 20:
                        stationLoc = "ASY 2";
                        break;
                    case 30:
                        stationLoc = "ASY 3";
                        break;
                    case 40:
                        stationLoc = "ASY 4";
                        break;
                    case 50:
                        stationLoc = "ASY 5";
                        break;
                    case 60:
                        stationLoc = "ASY 6";
                        break;
                    case 70:
                        stationLoc = "ASY 7";
                        break;
                    case 80:
                        stationLoc = "ASY 8";
                        break;
                    case 90:
                        stationLoc = "ASY 9";
                        break;
                    case 100:
                        stationLoc = "ASY 10";
                        break;                    
                    default:
                        stationLoc = "ASY 1";
                        break;
                }
            }
            else if (unitTypeStr == "OANG" || prodLine.StartsWith("LTD") == true)
            {
                switch (oprSeq)
                {
                    case 30:
                        stationLoc = "WELD";
                        break;
                    //case 35:
                    //    stationLoc = "POLY";
                    //    break;
                    //case 36:
                    //    stationLoc = "FOAM";
                    //    break;
                    case 40:
                    case 41:
                        stationLoc = "ASY 1";
                        break;                                                                                                     
                    //case 45:
                    //    stationLoc = "SA OP";
                    //    break;
                    case 50:
                    case 51:
                        stationLoc = "ASY 2";
                        break;                    
                    case 60:
                        stationLoc = "ASY 3";
                        break;
                    case 70:
                        stationLoc = "ASY 4";
                        break;
                    case 71:                   
                        stationLoc = "ASY 5";
                        break;                                         
                         case 72:
                    case 80:
                    case 85:
                        stationLoc = "ASY 6";
                        break;
                    case 90:
                        stationLoc = "TEST";
                        break;
                    case 100:
                        stationLoc = "CLOSE";
                        break;      
                    default:
                        stationLoc = "ASY 1";
                        break;
                }
            }
            else
            {
                switch (oprSeq)
                {
                    //case 30:
                    //    stationLoc = "WELD";
                    //    break;
                    //case 35:
                    //    stationLoc = "POLY";
                    //    break;
                    //case 36:
                    //    stationLoc = "FOAM";
                    //    break;
                    case 40:
                        stationLoc = "ASY 1";
                        break;
                    case 41:
                        stationLoc = "ASY 2";
                        break;
                    case 45:
                        stationLoc = "SA OP";
                        break;
                    case 50:
                        stationLoc = "ASY 3";
                        break;
                    case 51:
                        stationLoc = "ASY 4";
                        break;
                    case 60:
                        stationLoc = "ASY 5";
                        break;
                    case 70:
                        stationLoc = "ASY 6";
                        break;
                    case 71:
                        stationLoc = "ASY 7";
                        break;
                    case 72:
                        stationLoc = "ASY 8";
                        break;
                    case 80:
                        stationLoc = "ASY 9";
                        break;
                    case 85:
                        stationLoc = "ASY 10";
                        break;
                    case 90:
                        stationLoc = "TEST";
                        break;
                    case 100:
                        stationLoc = "CLOSE";
                        break;              
                    default:
                        stationLoc = "ASY 1";
                        break;
                }
            }

            return stationLoc;
        }

        public int findRelatedOpByStatioLoc(string stationLoc, string unitTypeStr)
        {
            int relOp = 40;

            if (unitTypeStr == "VKG")
            {
                switch (stationLoc)
                {
                    case "ASY 1":
                        relOp = 10;
                        break;
                    case "ASY 2":
                        relOp = 20;
                        break;
                    case "ASY 3":
                        relOp = 30;
                        break;
                    case "ASY 4":
                        relOp = 40;
                        break;
                    case "ASY 5":
                        relOp = 50;
                        break;
                    case "ASY 6":
                        relOp = 60;
                        break;
                    case "ASY 7":
                        relOp = 70;
                        break;
                    case "ASY 8":
                        relOp = 80;
                        break;
                    case "ASY 9":
                        relOp = 90;
                        break;
                    case "ASY 10":
                        relOp = 100;
                        break;                                       
                    default:
                        relOp = 40;
                        break;
                }
            }
            else if (unitTypeStr == "OANG")
            {
                switch (stationLoc)
                {
                    case "ASY 1":
                        relOp = 40;
                        break;
                    case "ASY 2":
                        relOp = 50;
                        break;
                    case "ASY 3":
                        relOp = 60;
                        break;
                    case "ASY 4":
                        relOp = 65;
                        break;
                    case "ASY 5":
                        relOp = 70;
                        break;
                    case "ASY 6":
                        relOp = 80;
                        break;
                    case "TEST":
                        relOp = 90;
                        break;
                    case "CLOSE":
                        relOp = 100;
                        break;                   
                    default:
                        relOp = 40;
                        break;                                      
                }
            }
            else
            {
                switch (stationLoc)
                {

                    case "ASY 1":
                        relOp = 40;
                        break;
                    case "ASY 2":
                        relOp = 41;
                        break;
                    case "ASY 3":
                        relOp = 50;
                        break;
                    case "ASY 4":
                        relOp = 51;
                        break;
                    case "ASY 5":
                        relOp = 60;
                        break;
                    case "ASY 6":
                        relOp = 70;
                        break;
                    case "ASY 7":
                        relOp = 71;
                        break;
                    case "ASY 8":
                        relOp = 72;
                        break;
                    case "ASY 9":
                        relOp = 80;
                        break;
                    case "ASY 10":
                        relOp = 85;
                        break;
                    case "TEST":
                        relOp = 90;
                        break;
                    case "CLOSE":
                        relOp = 100;
                        break;
                    default:
                        relOp = 40;
                        break;                              
                }
            }

            return relOp;
        }

        public bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }

        public double CalcKilowatts(string  inBTU)
        {
            double retVal = 0;
            double tmpBTU;

            tmpBTU = double.Parse(inBTU);

            retVal = tmpBTU * 0.000293;

            return retVal;
        }
      
        #endregion


    }
}
