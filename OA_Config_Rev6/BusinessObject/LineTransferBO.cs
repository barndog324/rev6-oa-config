﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;


namespace OA_Config_Rev6
{
    class LineTransferBO
    {
        public DataTable GetOprDataByPlantLineAndOpr(string plantLine, int oprSeq)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.LineChange_GetOprDataByPlantLineAndOpr", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PlantLine", plantLine);
                        cmd.Parameters.AddWithValue("@OprSeq", oprSeq);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetProductionLineByJobNum(string jobNumStr, int oprSeq)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.LineChange_GetProductionLineByJobNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);
                        cmd.Parameters.AddWithValue("@OprSeq", oprSeq);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetJobSchedulingDataByJobNum(string jobNumStr)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.LineChange_GetJobSchedulingDataByJobNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", jobNumStr);                        
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable GetLineScheduleByDate(string lineStr, DateTime startDate)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.LineChange_GetLineScheduleByDate", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Line", lineStr);
                        cmd.Parameters.AddWithValue("@StartDate", startDate);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
    }
}
