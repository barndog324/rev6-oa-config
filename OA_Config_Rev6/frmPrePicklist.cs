﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Diagnostics;
using System.Configuration;

namespace OA_Config_Rev6
{
    public partial class frmPrePicklist : Form
    {
        MainBO objMain = new MainBO();

        public frmPrePicklist()
        {
            InitializeComponent();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            DateTime startDate = dtpStartDate.Value;

            ReportDocument cryRpt = new ReportDocument();
            frmPrintBOM frmPrint = new frmPrintBOM();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@StartDate";
            pdv1.Value = startDate;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\OAUPrePicklist.rpt";             
#else
            objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\OAUPrePicklist.rpt";
#endif

            cryRpt.Load(objMain.ReportString);
            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
