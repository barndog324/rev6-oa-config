﻿namespace OA_Config_Rev6
{
    partial class frmScheduleJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbLineSchd = new System.Windows.Forms.GroupBox();
            this.gbSlot5 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbSlot5StartHour = new System.Windows.Forms.Label();
            this.lbSlot5DueHour = new System.Windows.Forms.Label();
            this.rbSlot5 = new System.Windows.Forms.RadioButton();
            this.gbSlot4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lbSlot4StartHour = new System.Windows.Forms.Label();
            this.lbSlot4DueHour = new System.Windows.Forms.Label();
            this.rbSlot4 = new System.Windows.Forms.RadioButton();
            this.gbSlot3 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbSlot3StartHour = new System.Windows.Forms.Label();
            this.lbSlot3DueHour = new System.Windows.Forms.Label();
            this.rbSlot3 = new System.Windows.Forms.RadioButton();
            this.gbSlot2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbSlot2StartHour = new System.Windows.Forms.Label();
            this.lbSlot2DueHour = new System.Windows.Forms.Label();
            this.rbSlot2 = new System.Windows.Forms.RadioButton();
            this.gbSlot1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbSlot1StartHour = new System.Windows.Forms.Label();
            this.lbSlot1DueHour = new System.Windows.Forms.Label();
            this.rbSlot1 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpToLineDate = new System.Windows.Forms.DateTimePicker();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.gbToLine = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rbTechToLineD = new System.Windows.Forms.RadioButton();
            this.rbTechToLineC = new System.Windows.Forms.RadioButton();
            this.rbTechToLineB = new System.Windows.Forms.RadioButton();
            this.rbTechToLineA = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine4 = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine3 = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine2 = new System.Windows.Forms.RadioButton();
            this.rbGrassToLine1 = new System.Windows.Forms.RadioButton();
            this.lbCustName = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.lbMessage = new System.Windows.Forms.Label();
            this.gbLineSchd.SuspendLayout();
            this.gbSlot5.SuspendLayout();
            this.gbSlot4.SuspendLayout();
            this.gbSlot3.SuspendLayout();
            this.gbSlot2.SuspendLayout();
            this.gbSlot1.SuspendLayout();
            this.gbToLine.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbLineSchd
            // 
            this.gbLineSchd.Controls.Add(this.gbSlot5);
            this.gbLineSchd.Controls.Add(this.gbSlot4);
            this.gbLineSchd.Controls.Add(this.gbSlot3);
            this.gbLineSchd.Controls.Add(this.gbSlot2);
            this.gbLineSchd.Controls.Add(this.gbSlot1);
            this.gbLineSchd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbLineSchd.ForeColor = System.Drawing.Color.Blue;
            this.gbLineSchd.Location = new System.Drawing.Point(352, 143);
            this.gbLineSchd.Name = "gbLineSchd";
            this.gbLineSchd.Size = new System.Drawing.Size(229, 300);
            this.gbLineSchd.TabIndex = 310;
            this.gbLineSchd.TabStop = false;
            this.gbLineSchd.Text = "Line Schedule";
            // 
            // gbSlot5
            // 
            this.gbSlot5.Controls.Add(this.label15);
            this.gbSlot5.Controls.Add(this.label16);
            this.gbSlot5.Controls.Add(this.lbSlot5StartHour);
            this.gbSlot5.Controls.Add(this.lbSlot5DueHour);
            this.gbSlot5.Controls.Add(this.rbSlot5);
            this.gbSlot5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSlot5.Location = new System.Drawing.Point(11, 244);
            this.gbSlot5.Name = "gbSlot5";
            this.gbSlot5.Size = new System.Drawing.Size(200, 50);
            this.gbSlot5.TabIndex = 319;
            this.gbSlot5.TabStop = false;
            this.gbSlot5.Text = "Open Slot";
            this.gbSlot5.Visible = false;
            this.gbSlot5.Enter += new System.EventHandler(this.gbSlot5_Enter);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(78, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 15);
            this.label15.TabIndex = 294;
            this.label15.Text = "End Time:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(78, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 15);
            this.label16.TabIndex = 292;
            this.label16.Text = "StartTime:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSlot5StartHour
            // 
            this.lbSlot5StartHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot5StartHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot5StartHour.Location = new System.Drawing.Point(142, 13);
            this.lbSlot5StartHour.Name = "lbSlot5StartHour";
            this.lbSlot5StartHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot5StartHour.TabIndex = 293;
            this.lbSlot5StartHour.Text = "01:00";
            this.lbSlot5StartHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSlot5DueHour
            // 
            this.lbSlot5DueHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot5DueHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot5DueHour.Location = new System.Drawing.Point(142, 28);
            this.lbSlot5DueHour.Name = "lbSlot5DueHour";
            this.lbSlot5DueHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot5DueHour.TabIndex = 295;
            this.lbSlot5DueHour.Text = "??:??";
            this.lbSlot5DueHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbSlot5
            // 
            this.rbSlot5.AutoSize = true;
            this.rbSlot5.Location = new System.Drawing.Point(6, 19);
            this.rbSlot5.Name = "rbSlot5";
            this.rbSlot5.Size = new System.Drawing.Size(58, 17);
            this.rbSlot5.TabIndex = 10;
            this.rbSlot5.TabStop = true;
            this.rbSlot5.Text = "Slot 5";
            this.rbSlot5.UseVisualStyleBackColor = true;
            // 
            // gbSlot4
            // 
            this.gbSlot4.Controls.Add(this.label17);
            this.gbSlot4.Controls.Add(this.label18);
            this.gbSlot4.Controls.Add(this.lbSlot4StartHour);
            this.gbSlot4.Controls.Add(this.lbSlot4DueHour);
            this.gbSlot4.Controls.Add(this.rbSlot4);
            this.gbSlot4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSlot4.Location = new System.Drawing.Point(11, 188);
            this.gbSlot4.Name = "gbSlot4";
            this.gbSlot4.Size = new System.Drawing.Size(200, 50);
            this.gbSlot4.TabIndex = 318;
            this.gbSlot4.TabStop = false;
            this.gbSlot4.Text = "Open Slot";
            this.gbSlot4.Visible = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(78, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 15);
            this.label17.TabIndex = 294;
            this.label17.Text = "End Time:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(78, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 15);
            this.label18.TabIndex = 292;
            this.label18.Text = "StartTime:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSlot4StartHour
            // 
            this.lbSlot4StartHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot4StartHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot4StartHour.Location = new System.Drawing.Point(142, 13);
            this.lbSlot4StartHour.Name = "lbSlot4StartHour";
            this.lbSlot4StartHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot4StartHour.TabIndex = 293;
            this.lbSlot4StartHour.Text = "11:00";
            this.lbSlot4StartHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSlot4DueHour
            // 
            this.lbSlot4DueHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot4DueHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot4DueHour.Location = new System.Drawing.Point(142, 28);
            this.lbSlot4DueHour.Name = "lbSlot4DueHour";
            this.lbSlot4DueHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot4DueHour.TabIndex = 295;
            this.lbSlot4DueHour.Text = "??:??";
            this.lbSlot4DueHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbSlot4
            // 
            this.rbSlot4.AutoSize = true;
            this.rbSlot4.Location = new System.Drawing.Point(6, 19);
            this.rbSlot4.Name = "rbSlot4";
            this.rbSlot4.Size = new System.Drawing.Size(58, 17);
            this.rbSlot4.TabIndex = 10;
            this.rbSlot4.TabStop = true;
            this.rbSlot4.Text = "Slot 4";
            this.rbSlot4.UseVisualStyleBackColor = true;
            this.rbSlot4.CheckedChanged += new System.EventHandler(this.rbSlot4_CheckedChanged);
            // 
            // gbSlot3
            // 
            this.gbSlot3.Controls.Add(this.label12);
            this.gbSlot3.Controls.Add(this.label14);
            this.gbSlot3.Controls.Add(this.lbSlot3StartHour);
            this.gbSlot3.Controls.Add(this.lbSlot3DueHour);
            this.gbSlot3.Controls.Add(this.rbSlot3);
            this.gbSlot3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSlot3.Location = new System.Drawing.Point(11, 132);
            this.gbSlot3.Name = "gbSlot3";
            this.gbSlot3.Size = new System.Drawing.Size(200, 50);
            this.gbSlot3.TabIndex = 317;
            this.gbSlot3.TabStop = false;
            this.gbSlot3.Text = "Open Slot";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(78, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 15);
            this.label12.TabIndex = 294;
            this.label12.Text = "End Time:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(78, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 15);
            this.label14.TabIndex = 292;
            this.label14.Text = "StartTime:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSlot3StartHour
            // 
            this.lbSlot3StartHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot3StartHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot3StartHour.Location = new System.Drawing.Point(142, 13);
            this.lbSlot3StartHour.Name = "lbSlot3StartHour";
            this.lbSlot3StartHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot3StartHour.TabIndex = 293;
            this.lbSlot3StartHour.Text = "09:00";
            this.lbSlot3StartHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSlot3DueHour
            // 
            this.lbSlot3DueHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot3DueHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot3DueHour.Location = new System.Drawing.Point(142, 28);
            this.lbSlot3DueHour.Name = "lbSlot3DueHour";
            this.lbSlot3DueHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot3DueHour.TabIndex = 295;
            this.lbSlot3DueHour.Text = "??:??";
            this.lbSlot3DueHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbSlot3
            // 
            this.rbSlot3.AutoSize = true;
            this.rbSlot3.Location = new System.Drawing.Point(6, 19);
            this.rbSlot3.Name = "rbSlot3";
            this.rbSlot3.Size = new System.Drawing.Size(58, 17);
            this.rbSlot3.TabIndex = 10;
            this.rbSlot3.TabStop = true;
            this.rbSlot3.Text = "Slot 3";
            this.rbSlot3.UseVisualStyleBackColor = true;
            this.rbSlot3.CheckedChanged += new System.EventHandler(this.rbSlot3_CheckedChanged);
            // 
            // gbSlot2
            // 
            this.gbSlot2.Controls.Add(this.label10);
            this.gbSlot2.Controls.Add(this.label11);
            this.gbSlot2.Controls.Add(this.lbSlot2StartHour);
            this.gbSlot2.Controls.Add(this.lbSlot2DueHour);
            this.gbSlot2.Controls.Add(this.rbSlot2);
            this.gbSlot2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSlot2.Location = new System.Drawing.Point(11, 76);
            this.gbSlot2.Name = "gbSlot2";
            this.gbSlot2.Size = new System.Drawing.Size(200, 50);
            this.gbSlot2.TabIndex = 316;
            this.gbSlot2.TabStop = false;
            this.gbSlot2.Text = "Open Slot";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(78, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 15);
            this.label10.TabIndex = 294;
            this.label10.Text = "End Time:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(78, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 292;
            this.label11.Text = "StartTime:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSlot2StartHour
            // 
            this.lbSlot2StartHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot2StartHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot2StartHour.Location = new System.Drawing.Point(142, 13);
            this.lbSlot2StartHour.Name = "lbSlot2StartHour";
            this.lbSlot2StartHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot2StartHour.TabIndex = 293;
            this.lbSlot2StartHour.Text = "07:00";
            this.lbSlot2StartHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSlot2DueHour
            // 
            this.lbSlot2DueHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot2DueHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot2DueHour.Location = new System.Drawing.Point(142, 28);
            this.lbSlot2DueHour.Name = "lbSlot2DueHour";
            this.lbSlot2DueHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot2DueHour.TabIndex = 295;
            this.lbSlot2DueHour.Text = "??:??";
            this.lbSlot2DueHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbSlot2
            // 
            this.rbSlot2.AutoSize = true;
            this.rbSlot2.Location = new System.Drawing.Point(6, 19);
            this.rbSlot2.Name = "rbSlot2";
            this.rbSlot2.Size = new System.Drawing.Size(58, 17);
            this.rbSlot2.TabIndex = 10;
            this.rbSlot2.TabStop = true;
            this.rbSlot2.Text = "Slot 2";
            this.rbSlot2.UseVisualStyleBackColor = true;
            this.rbSlot2.CheckedChanged += new System.EventHandler(this.rbSlot2_CheckedChanged);
            // 
            // gbSlot1
            // 
            this.gbSlot1.Controls.Add(this.label20);
            this.gbSlot1.Controls.Add(this.label13);
            this.gbSlot1.Controls.Add(this.lbSlot1StartHour);
            this.gbSlot1.Controls.Add(this.lbSlot1DueHour);
            this.gbSlot1.Controls.Add(this.rbSlot1);
            this.gbSlot1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSlot1.Location = new System.Drawing.Point(11, 20);
            this.gbSlot1.Name = "gbSlot1";
            this.gbSlot1.Size = new System.Drawing.Size(200, 50);
            this.gbSlot1.TabIndex = 315;
            this.gbSlot1.TabStop = false;
            this.gbSlot1.Text = "Open Slot";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(78, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 15);
            this.label20.TabIndex = 294;
            this.label20.Text = "End Time:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(78, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 15);
            this.label13.TabIndex = 292;
            this.label13.Text = "StartTime:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSlot1StartHour
            // 
            this.lbSlot1StartHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot1StartHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot1StartHour.Location = new System.Drawing.Point(142, 13);
            this.lbSlot1StartHour.Name = "lbSlot1StartHour";
            this.lbSlot1StartHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot1StartHour.TabIndex = 293;
            this.lbSlot1StartHour.Text = "05:00";
            this.lbSlot1StartHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSlot1DueHour
            // 
            this.lbSlot1DueHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSlot1DueHour.ForeColor = System.Drawing.Color.Black;
            this.lbSlot1DueHour.Location = new System.Drawing.Point(142, 28);
            this.lbSlot1DueHour.Name = "lbSlot1DueHour";
            this.lbSlot1DueHour.Size = new System.Drawing.Size(40, 15);
            this.lbSlot1DueHour.TabIndex = 295;
            this.lbSlot1DueHour.Text = "??:??";
            this.lbSlot1DueHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbSlot1
            // 
            this.rbSlot1.AutoSize = true;
            this.rbSlot1.Location = new System.Drawing.Point(6, 19);
            this.rbSlot1.Name = "rbSlot1";
            this.rbSlot1.Size = new System.Drawing.Size(58, 17);
            this.rbSlot1.TabIndex = 10;
            this.rbSlot1.TabStop = true;
            this.rbSlot1.Text = "Slot 1";
            this.rbSlot1.UseVisualStyleBackColor = true;
            this.rbSlot1.CheckedChanged += new System.EventHandler(this.rbSlot1_CheckedChanged);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(85, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(180, 20);
            this.label9.TabIndex = 309;
            this.label9.Text = "Select To Line &&  Start Date:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpToLineDate
            // 
            this.dtpToLineDate.Location = new System.Drawing.Point(74, 163);
            this.dtpToLineDate.Name = "dtpToLineDate";
            this.dtpToLineDate.Size = new System.Drawing.Size(200, 20);
            this.dtpToLineDate.TabIndex = 308;
            this.dtpToLineDate.ValueChanged += new System.EventHandler(this.dtpToLineDate_ValueChanged);
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.ForeColor = System.Drawing.Color.Black;
            this.lbModelNo.Location = new System.Drawing.Point(14, 85);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(588, 20);
            this.lbModelNo.TabIndex = 299;
            this.lbModelNo.Text = "ModelNo";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCancel.Location = new System.Drawing.Point(466, 494);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 25);
            this.btnCancel.TabIndex = 298;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Enabled = false;
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.Green;
            this.btnSubmit.Location = new System.Drawing.Point(466, 458);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(115, 25);
            this.btnSubmit.TabIndex = 297;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // gbToLine
            // 
            this.gbToLine.Controls.Add(this.label3);
            this.gbToLine.Controls.Add(this.label4);
            this.gbToLine.Controls.Add(this.rbTechToLineD);
            this.gbToLine.Controls.Add(this.rbTechToLineC);
            this.gbToLine.Controls.Add(this.rbTechToLineB);
            this.gbToLine.Controls.Add(this.rbTechToLineA);
            this.gbToLine.Controls.Add(this.rbGrassToLine4);
            this.gbToLine.Controls.Add(this.rbGrassToLine3);
            this.gbToLine.Controls.Add(this.rbGrassToLine2);
            this.gbToLine.Controls.Add(this.rbGrassToLine1);
            this.gbToLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbToLine.ForeColor = System.Drawing.Color.Blue;
            this.gbToLine.Location = new System.Drawing.Point(32, 274);
            this.gbToLine.Name = "gbToLine";
            this.gbToLine.Size = new System.Drawing.Size(270, 169);
            this.gbToLine.TabIndex = 296;
            this.gbToLine.TabStop = false;
            this.gbToLine.Text = "To Line";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Green;
            this.label3.Location = new System.Drawing.Point(160, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Technology";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(47, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Grassland";
            // 
            // rbTechToLineD
            // 
            this.rbTechToLineD.AutoSize = true;
            this.rbTechToLineD.Enabled = false;
            this.rbTechToLineD.Location = new System.Drawing.Point(163, 130);
            this.rbTechToLineD.Name = "rbTechToLineD";
            this.rbTechToLineD.Size = new System.Drawing.Size(62, 17);
            this.rbTechToLineD.TabIndex = 17;
            this.rbTechToLineD.TabStop = true;
            this.rbTechToLineD.Text = "Line D";
            this.rbTechToLineD.UseVisualStyleBackColor = true;
            this.rbTechToLineD.CheckedChanged += new System.EventHandler(this.rbTechToLineD_CheckedChanged);
            // 
            // rbTechToLineC
            // 
            this.rbTechToLineC.AutoSize = true;
            this.rbTechToLineC.Enabled = false;
            this.rbTechToLineC.Location = new System.Drawing.Point(164, 105);
            this.rbTechToLineC.Name = "rbTechToLineC";
            this.rbTechToLineC.Size = new System.Drawing.Size(61, 17);
            this.rbTechToLineC.TabIndex = 16;
            this.rbTechToLineC.TabStop = true;
            this.rbTechToLineC.Text = "Line C";
            this.rbTechToLineC.UseVisualStyleBackColor = true;
            this.rbTechToLineC.CheckedChanged += new System.EventHandler(this.rbTechToLineC_CheckedChanged);
            // 
            // rbTechToLineB
            // 
            this.rbTechToLineB.AutoSize = true;
            this.rbTechToLineB.Location = new System.Drawing.Point(164, 80);
            this.rbTechToLineB.Name = "rbTechToLineB";
            this.rbTechToLineB.Size = new System.Drawing.Size(61, 17);
            this.rbTechToLineB.TabIndex = 15;
            this.rbTechToLineB.TabStop = true;
            this.rbTechToLineB.Text = "Line B";
            this.rbTechToLineB.UseVisualStyleBackColor = true;
            this.rbTechToLineB.CheckedChanged += new System.EventHandler(this.rbTechToLineB_CheckedChanged);
            // 
            // rbTechToLineA
            // 
            this.rbTechToLineA.AutoSize = true;
            this.rbTechToLineA.Location = new System.Drawing.Point(163, 55);
            this.rbTechToLineA.Name = "rbTechToLineA";
            this.rbTechToLineA.Size = new System.Drawing.Size(61, 17);
            this.rbTechToLineA.TabIndex = 14;
            this.rbTechToLineA.TabStop = true;
            this.rbTechToLineA.Text = "Line A";
            this.rbTechToLineA.UseVisualStyleBackColor = true;
            this.rbTechToLineA.CheckedChanged += new System.EventHandler(this.rbTechToLineA_CheckedChanged);
            // 
            // rbGrassToLine4
            // 
            this.rbGrassToLine4.AutoSize = true;
            this.rbGrassToLine4.Location = new System.Drawing.Point(49, 129);
            this.rbGrassToLine4.Name = "rbGrassToLine4";
            this.rbGrassToLine4.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine4.TabIndex = 13;
            this.rbGrassToLine4.TabStop = true;
            this.rbGrassToLine4.Text = "Line 4";
            this.rbGrassToLine4.UseVisualStyleBackColor = true;
            this.rbGrassToLine4.CheckedChanged += new System.EventHandler(this.rbGrassToLine4_CheckedChanged);
            // 
            // rbGrassToLine3
            // 
            this.rbGrassToLine3.AutoSize = true;
            this.rbGrassToLine3.Location = new System.Drawing.Point(49, 104);
            this.rbGrassToLine3.Name = "rbGrassToLine3";
            this.rbGrassToLine3.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine3.TabIndex = 12;
            this.rbGrassToLine3.TabStop = true;
            this.rbGrassToLine3.Text = "Line 3";
            this.rbGrassToLine3.UseVisualStyleBackColor = true;
            this.rbGrassToLine3.CheckedChanged += new System.EventHandler(this.rbGrassToLine3_CheckedChanged);
            // 
            // rbGrassToLine2
            // 
            this.rbGrassToLine2.AutoSize = true;
            this.rbGrassToLine2.Location = new System.Drawing.Point(49, 79);
            this.rbGrassToLine2.Name = "rbGrassToLine2";
            this.rbGrassToLine2.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine2.TabIndex = 11;
            this.rbGrassToLine2.TabStop = true;
            this.rbGrassToLine2.Text = "Line 2";
            this.rbGrassToLine2.UseVisualStyleBackColor = true;
            this.rbGrassToLine2.CheckedChanged += new System.EventHandler(this.rbGrassToLine2_CheckedChanged);
            // 
            // rbGrassToLine1
            // 
            this.rbGrassToLine1.AutoSize = true;
            this.rbGrassToLine1.Location = new System.Drawing.Point(49, 54);
            this.rbGrassToLine1.Name = "rbGrassToLine1";
            this.rbGrassToLine1.Size = new System.Drawing.Size(60, 17);
            this.rbGrassToLine1.TabIndex = 10;
            this.rbGrassToLine1.TabStop = true;
            this.rbGrassToLine1.Text = "Line 1";
            this.rbGrassToLine1.UseVisualStyleBackColor = true;
            this.rbGrassToLine1.CheckedChanged += new System.EventHandler(this.rbGrassToLine1_CheckedChanged);
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.ForeColor = System.Drawing.Color.Black;
            this.lbCustName.Location = new System.Drawing.Point(18, 56);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(588, 20);
            this.lbCustName.TabIndex = 294;
            this.lbCustName.Text = "Customer Name";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(215, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 20);
            this.label25.TabIndex = 293;
            this.label25.Text = "Job #:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Tahoma", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.Black;
            this.lbJobNum.Location = new System.Drawing.Point(294, 28);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(247, 20);
            this.lbJobNum.TabIndex = 292;
            this.lbJobNum.Text = "??????-??-??";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMessage
            // 
            this.lbMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lbMessage.ForeColor = System.Drawing.Color.Black;
            this.lbMessage.Location = new System.Drawing.Point(0, 539);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(613, 23);
            this.lbMessage.TabIndex = 311;
            this.lbMessage.Text = "Message Line";
            this.lbMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmScheduleJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 561);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.gbLineSchd);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dtpToLineDate);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.gbToLine);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lbJobNum);
            this.Name = "frmScheduleJob";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Schedule Job";
            this.gbLineSchd.ResumeLayout(false);
            this.gbSlot5.ResumeLayout(false);
            this.gbSlot5.PerformLayout();
            this.gbSlot4.ResumeLayout(false);
            this.gbSlot4.PerformLayout();
            this.gbSlot3.ResumeLayout(false);
            this.gbSlot3.PerformLayout();
            this.gbSlot2.ResumeLayout(false);
            this.gbSlot2.PerformLayout();
            this.gbSlot1.ResumeLayout(false);
            this.gbSlot1.PerformLayout();
            this.gbToLine.ResumeLayout(false);
            this.gbToLine.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbLineSchd;
        private System.Windows.Forms.GroupBox gbSlot5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label lbSlot5StartHour;
        public System.Windows.Forms.Label lbSlot5DueHour;
        private System.Windows.Forms.RadioButton rbSlot5;
        private System.Windows.Forms.GroupBox gbSlot4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label lbSlot4StartHour;
        public System.Windows.Forms.Label lbSlot4DueHour;
        private System.Windows.Forms.RadioButton rbSlot4;
        private System.Windows.Forms.GroupBox gbSlot3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label lbSlot3StartHour;
        public System.Windows.Forms.Label lbSlot3DueHour;
        private System.Windows.Forms.RadioButton rbSlot3;
        private System.Windows.Forms.GroupBox gbSlot2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label lbSlot2StartHour;
        public System.Windows.Forms.Label lbSlot2DueHour;
        private System.Windows.Forms.RadioButton rbSlot2;
        private System.Windows.Forms.GroupBox gbSlot1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label lbSlot1StartHour;
        public System.Windows.Forms.Label lbSlot1DueHour;
        private System.Windows.Forms.RadioButton rbSlot1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpToLineDate;
        public System.Windows.Forms.Label lbModelNo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.GroupBox gbToLine;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lbCustName;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.RadioButton rbTechToLineD;
        public System.Windows.Forms.RadioButton rbTechToLineC;
        public System.Windows.Forms.RadioButton rbTechToLineB;
        public System.Windows.Forms.RadioButton rbTechToLineA;
        public System.Windows.Forms.RadioButton rbGrassToLine4;
        public System.Windows.Forms.RadioButton rbGrassToLine3;
        public System.Windows.Forms.RadioButton rbGrassToLine2;
        public System.Windows.Forms.RadioButton rbGrassToLine1;
        private System.Windows.Forms.Label lbMessage;
    }
}