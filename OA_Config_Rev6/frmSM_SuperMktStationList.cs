﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmSM_SuperMktStationList : Form
    {
        public frmSM_SuperMktStationList()
        {
            InitializeComponent();
        }

        private void btnDisplayRpt_Click(object sender, EventArgs e)
        {
            bool firstJob = true; 
            bool stationChecked = false;
            bool allStations = false;

            string jobNumStr = "";
            string jobNumList = "";
            string stationLst = "";
            string stationNumLst = "";                  
  
            MainBO objMain = new MainBO();

            DataTable dt = new DataTable();
            dt.Columns.Add("JobNum", typeof(string)).MaxLength = 14;
            dt.Columns.Add("StationList", typeof(string)).MaxLength = 200;    
       
           

            foreach (DataGridViewRow row in dgvSuperMktJobs.Rows)
            {
                if (!firstJob)
                {
                    jobNumList += "*;";                    
                }
                else
                {
                    firstJob = false;
                }

                jobNumStr = row.Cells["JobNum"].Value.ToString();  // Job Number cell
                jobNumList += jobNumStr + ",";              

                var dr = dt.NewRow();
                dr["JobNum"] = jobNumStr;

                if (Convert.ToBoolean(row.Cells[0].Value) == true) // All Stations checkbox cell
                {
                    allStations = true;
                }
                else
                {
                    allStations = false;
                }

                if (allStations == false)
                {
                    if (Convert.ToBoolean(row.Cells[1].Value) == true) // Weld Station checkbox cell
                    {
                        jobNumList += "Welding";
                        stationChecked = true;
                    }

                    if (Convert.ToBoolean(row.Cells[2].Value) == true) // Station 1 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 1";
                        stationChecked = true;
                    }

                    if (Convert.ToBoolean(row.Cells[3].Value) == true)  // Station 2 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 2";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[4].Value) == true) // Station 3 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 3";
                        stationChecked = true;
                    }

                    if (Convert.ToBoolean(row.Cells[5].Value) == true) // Station 4 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 4";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[6].Value) == true)   // Station 5 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 5";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[7].Value) == true) // Station 6 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 6";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[8].Value) == true) // Station 6 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 9";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[9].Value) == true) // Station 6 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "ASY 10";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[10].Value) == true) //SubStation 1 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 1";
                        stationChecked = true;
                    }

                    if (Convert.ToBoolean(row.Cells[11].Value) == true) // SubStation 2 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 2";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[12].Value) == true) // SubStation 3 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 3";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[13].Value) == true) // SubStation 4 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 4";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[14].Value) == true) // SubStation 5-11 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 5";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[15].Value) == true) // SubStation 12 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 6";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[16].Value) == true) // SubStation 14 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 7";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[17].Value) == true) // SubStation 14 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 8";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[18].Value) == true) // SubStation 14 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 9";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[19].Value) == true) // SubStation 14 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 10";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[20].Value) == true) // SubStation 14 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 11";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[21].Value) == true) // SubStation 14 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 12";
                        stationChecked = true;

                    }

                    if (Convert.ToBoolean(row.Cells[22].Value) == true) // SubStation 14 checkbox cell
                    {
                        if (stationChecked == true)
                        {
                            jobNumList += ",";
                        }
                        jobNumList += "SA 13";
                        stationChecked = true;

                    }

                    if (stationChecked == false)
                    {
                        MessageBox.Show("ERROR - No Stations selected for Job # " + jobNumStr);
                        break;
                    }
                    else
                    {
                        stationChecked = false;
                        dr["StationList"] = stationLst.Substring(0, stationLst.Length);
                        dt.Rows.Add(dr);
                    }
                }
                else
                {                    
                    jobNumList += "Welding,ASY 1,ASY 2,ASY 3,ASY 4,ASY 5,ASY 6,ASY 9,ASY 10,SA 1,SA 2,SA 3,SA 4,SA 5,SA 6,SA 7,SA 8,SA 9,SA 10,SA 11,SA 12,SA 13";
                }
            }

            if (jobNumList.Length > 0)
            {
                
                //jobNumList = jobNumList.Substring(0, (jobNumList.Length - 2));   
                jobNumList += "*;";

                ReportDocument cryRpt = new ReportDocument();
                frmPrintBOM frmPrint = new frmPrintBOM();

                ParameterFields paramFields = new ParameterFields();

                ParameterField pf1 = new ParameterField();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pf1.Name = "@JobList";
                pdv1.Value = jobNumList;
                pf1.CurrentValues.Add(pdv1);

                paramFields.Add(pf1);

                //ParameterField pf2 = new ParameterField();
                //ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                //pf2.Name = "@StationList";
                //pdv2.Value = stationLst;
                //pf2.CurrentValues.Add(pdv2);

                //paramFields.Add(pf2);               

                frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG               
            objMain.EpicorServer = ConfigurationManager.AppSettings["Test2EpicorAppServer"];
            objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfigurator\\crystalreports\\SheetMetalSuperMarketPicklistByJobNum.rpt";             
#else
                objMain.EpicorServer = ConfigurationManager.AppSettings["ProdEpicorAppServer"];
                objMain.ReportString = @"\\" + objMain.EpicorServer + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\SheetMetalSuperMarketPicklistByJobNum.rpt";
#endif

                cryRpt.Load(objMain.ReportString);

                frmPrint.crystalReportViewer1.ReportSource = cryRpt;
                frmPrint.ShowDialog();
                frmPrint.crystalReportViewer1.Refresh();
            }
            else
            {
                MessageBox.Show("WARNING: No jobs have been selected from list.");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
