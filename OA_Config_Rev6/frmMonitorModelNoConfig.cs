﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OA_Config_Rev6
{
    public partial class frmMonitorModelNoConfig : Form
    {
        MainBO objMain = new MainBO();

        public frmMonitorModelNoConfig()
        {
            InitializeComponent();           
        }

        #region Events

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }       

        private void btnParseModelNo_Click(object sender, EventArgs e)
        {
            string modelNoStr;
            string digitValStr;
            string heatingTypeStr = "NA";                                            
           
            
            modelNoStr = this.txtModelNo.Text;
            if (modelNoStr.Length == 43)                
            {                                                                 
                // Retrieve Cabinet Size.                    
                digitValStr = modelNoStr.Substring(0, 1);

                if (digitValStr == "T")
                {
                    heatingTypeStr = "ELEC";
                }                    
                else 
                {
                    heatingTypeStr = "GE";
                }

                lbModelNo.Text = modelNoStr;                                          

                lbJobNum.Text = "";
                lbOrderDate.Text = "";
                lbShipDate.Text = "";
                lbBOMCreationDate.Text = "";
                lbLastUpdateDate.Text = "";
                lbProdStartDate.Text = "";
                lbCompleteDate.Text = "";
                lbEnvironment.Text = "";
                lbModelNo.Text = modelNoStr;
                this.Text = modelNoStr;      
                lbCustName.Text = "";

                // Digit 1 - Unit Type
                digitValStr = modelNoStr.Substring(0, 1);
                DataTable dt = objMain.MonitorGetModelNoValues(1, "NA");
                this.cbUnitType.DataSource = dt;
                this.cbUnitType.DisplayMember = "DigitDescription";
                this.cbUnitType.ValueMember = "DigitVal";
                this.cbUnitType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 2 - Efficiency
                digitValStr = modelNoStr.Substring(1, 1);
                dt = objMain.MonitorGetModelNoValues(2, "NA");
                this.cbEfficiency.DataSource = dt;
                this.cbEfficiency.DisplayMember = "DigitDescription";
                this.cbEfficiency.ValueMember = "DigitVal";
                this.cbEfficiency.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 3 - Airflow Configuration               
                digitValStr = modelNoStr.Substring(2, 1);
                dt = objMain.MonitorGetModelNoValues(3, "NA");
                this.cbAirflowConfiguration.DataSource = dt;
                this.cbAirflowConfiguration.DisplayMember = "DigitDescription";
                this.cbAirflowConfiguration.ValueMember = "DigitVal";
                this.cbAirflowConfiguration.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 456 - CoolingCapacity              
                digitValStr = modelNoStr.Substring(3, 3);
                dt = objMain.MonitorGetModelNoValues(456, "NA");
                this.cbCoolingCapacity.DataSource = dt;
                this.cbCoolingCapacity.DisplayMember = "DigitDescription";
                this.cbCoolingCapacity.ValueMember = "DigitVal";
                this.cbCoolingCapacity.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 7 - MajorDesignSequence
                digitValStr = modelNoStr.Substring(6, 1);
                dt = objMain.MonitorGetModelNoValues(7, "NA");
                this.cbMajorDesignSequence.DataSource = dt;
                this.cbMajorDesignSequence.DisplayMember = "DigitDescription";
                this.cbMajorDesignSequence.ValueMember = "DigitVal";
                this.cbMajorDesignSequence.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 8 - Voltage
                digitValStr = modelNoStr.Substring(7, 1);
                dt = objMain.MonitorGetModelNoValues(8, "NA");
                this.cbVoltage.DataSource = dt;
                this.cbVoltage.DisplayMember = "DigitDescription";
                this.cbVoltage.ValueMember = "DigitVal";
                this.cbVoltage.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 9 - UnitControls
                digitValStr = modelNoStr.Substring(8, 1);
                dt = objMain.MonitorGetModelNoValues(9, "NA");
                this.cbUnitControls.DataSource = dt;
                this.cbUnitControls.DisplayMember = "DigitDescription";
                this.cbUnitControls.ValueMember = "DigitVal";
                this.cbUnitControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 10 - HeatingCapacity
                digitValStr = modelNoStr.Substring(9, 1);
                dt = objMain.MonitorGetModelNoValues(10, heatingTypeStr);
                this.cbHeatingCapacity.DataSource = dt;
                this.cbHeatingCapacity.DisplayMember = "DigitDescription";
                this.cbHeatingCapacity.ValueMember = "DigitVal";
                this.cbHeatingCapacity.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 11 - cbMinorDesignSequence
                digitValStr = modelNoStr.Substring(10, 1);
                dt = objMain.MonitorGetModelNoValues(11, "NA");
                this.cbMinorDesignSequence.DataSource = dt;
                this.cbMinorDesignSequence.DisplayMember = "DigitDescription";
                this.cbMinorDesignSequence.ValueMember = "DigitVal";
                this.cbMinorDesignSequence.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 12,13 - ServiceSequence
                digitValStr = modelNoStr.Substring(11, 2);
                dt = objMain.MonitorGetModelNoValues(1213, "NA");
                this.cbServiceSequence.DataSource = dt;
                this.cbServiceSequence.DisplayMember = "DigitDescription";
                this.cbServiceSequence.ValueMember = "DigitVal";
                this.cbServiceSequence.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 14 - FreshAirSelection
                digitValStr = modelNoStr.Substring(13, 1);
                dt = objMain.MonitorGetModelNoValues(14, "NA");
                this.cbFreshAirSelection.DataSource = dt;
                this.cbFreshAirSelection.DisplayMember = "DigitDescription";
                this.cbFreshAirSelection.ValueMember = "DigitVal";
                this.cbFreshAirSelection.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 15 -SupplyFanMotor
                digitValStr = modelNoStr.Substring(14, 1);
                dt = objMain.MonitorGetModelNoValues(15, "NA");
                this.cbSupplyFanMotor.DataSource = dt;
                this.cbSupplyFanMotor.DisplayMember = "DigitDescription";
                this.cbSupplyFanMotor.ValueMember = "DigitVal";
                this.cbSupplyFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 16 - HingedServiceAccessFilters
                digitValStr = modelNoStr.Substring(15, 1);
                dt = objMain.MonitorGetModelNoValues(16, "NA");
                this.cbHingedServiceAccessFilters.DataSource = dt;
                this.cbHingedServiceAccessFilters.DisplayMember = "DigitDescription";
                this.cbHingedServiceAccessFilters.ValueMember = "DigitVal";
                this.cbHingedServiceAccessFilters.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);


                // Digit 17 - CondenserCoilProtection
                digitValStr = modelNoStr.Substring(16, 1);
                dt = objMain.MonitorGetModelNoValues(17, "NA");
                this.cbCondenserCoilProtection.DataSource = dt;
                this.cbCondenserCoilProtection.DisplayMember = "DigitDescription";
                this.cbCondenserCoilProtection.ValueMember = "DigitVal";
                this.cbCondenserCoilProtection.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 18 - ThruBaseProvisions
                digitValStr = modelNoStr.Substring(17, 1);
                dt = objMain.MonitorGetModelNoValues(18, heatingTypeStr);
                this.cbThruBaseProvisions.DataSource = dt;
                this.cbThruBaseProvisions.DisplayMember = "DigitDescription";
                this.cbThruBaseProvisions.ValueMember = "DigitVal";
                this.cbThruBaseProvisions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 19 - DisconnectSwitch
                digitValStr = modelNoStr.Substring(18, 1);
                dt = objMain.MonitorGetModelNoValues(19, "NA");
                this.cbDisconnectSwitch.DataSource = dt;
                this.cbDisconnectSwitch.DisplayMember = "DigitDescription";
                this.cbDisconnectSwitch.ValueMember = "DigitVal";
                this.cbDisconnectSwitch.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 20 - ConvenienceOutletOptions
                digitValStr = modelNoStr.Substring(19, 1);
                dt = objMain.MonitorGetModelNoValues(20, "NA");
                this.cbConvenienceOutletOptions.DataSource = dt;
                this.cbConvenienceOutletOptions.DisplayMember = "DigitDescription";
                this.cbConvenienceOutletOptions.ValueMember = "DigitVal";
                this.cbConvenienceOutletOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 21 - CommunicationsOptions
                digitValStr = modelNoStr.Substring(20, 1);
                dt = objMain.MonitorGetModelNoValues(21, "NA");
                this.cbCommunicationsOptions.DataSource = dt;
                this.cbCommunicationsOptions.DisplayMember = "DigitDescription";
                this.cbCommunicationsOptions.ValueMember = "DigitVal";
                this.cbCommunicationsOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                //Digit 22 - RefrigerationSystemOptions
                digitValStr = modelNoStr.Substring(21, 1);
                dt = objMain.MonitorGetModelNoValues(22, "NA");
                this.cbRefrigerationSystemOptions.DataSource = dt;
                this.cbRefrigerationSystemOptions.DisplayMember = "DigitDescription";
                this.cbRefrigerationSystemOptions.ValueMember = "DigitVal";
                this.cbRefrigerationSystemOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 23 - RefrigerationControls
                digitValStr = modelNoStr.Substring(22, 1);
                dt = objMain.MonitorGetModelNoValues(23, "NA");
                this.cbRefrigerationControls.DataSource = dt;
                this.cbRefrigerationControls.DisplayMember = "DigitDescription";
                this.cbRefrigerationControls.ValueMember = "DigitVal";
                this.cbRefrigerationControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 24 - SmokeDetector
                digitValStr = modelNoStr.Substring(23, 1);
                dt = objMain.MonitorGetModelNoValues(24, "NA");
                this.cbSmokeDetector.DataSource = dt;
                this.cbSmokeDetector.DisplayMember = "DigitDescription";
                this.cbSmokeDetector.ValueMember = "DigitVal";
                this.cbSmokeDetector.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 25- TraneSystemMonitoringControls
                digitValStr = modelNoStr.Substring(24, 1);
                dt = objMain.MonitorGetModelNoValues(25, "NA");
                this.cbTraneSystemMonitoringControls.DataSource = dt;
                this.cbTraneSystemMonitoringControls.DisplayMember = "DigitDescription";
                this.cbTraneSystemMonitoringControls.ValueMember = "DigitVal";
                this.cbTraneSystemMonitoringControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 26 - SystemMonitoringControls
                digitValStr = modelNoStr.Substring(25, 1);
                dt = objMain.MonitorGetModelNoValues(26, "NA");
                this.cbSystemMonitoringControls.DataSource = dt;
                this.cbSystemMonitoringControls.DisplayMember = "DigitDescription";
                this.cbSystemMonitoringControls.ValueMember = "DigitVal";
                this.cbSystemMonitoringControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 27 - UnitHardwareEnhancements
                digitValStr = modelNoStr.Substring(26, 1);
                dt = objMain.MonitorGetModelNoValues(27, "NA");
                this.cbUnitHardwareEnhancements.DataSource = dt;
                this.cbUnitHardwareEnhancements.DisplayMember = "DigitDescription";
                this.cbUnitHardwareEnhancements.ValueMember = "DigitVal";
                this.cbUnitHardwareEnhancements.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 28 - Powered Exhaust Fan Wheel
                digitValStr = modelNoStr.Substring(27, 1);
                dt = objMain.MonitorGetModelNoValues(28, "NA");
                this.cbShortCircuitCurrentRating.DataSource = dt;
                this.cbShortCircuitCurrentRating.DisplayMember = "DigitDescription";
                this.cbShortCircuitCurrentRating.ValueMember = "DigitVal";
                this.cbShortCircuitCurrentRating.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 29,30 - Powered Exhaust Fan Motor HP
                //digitValStr = modelNoStr.Substring(28, 1);
                //dt = objMain.MonitorGetModelNoValues(29, "NA", oauTypeCode);
                //this.cbPoweredExhaustFanMotorHP.DataSource = dt;
                //this.cbPoweredExhaustFanMotorHP.DisplayMember = "DigitDescription";
                //this.cbPoweredExhaustFanMotorHP.ValueMember = "DigitVal";
                //this.cbPoweredExhaustFanMotorHP.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 31 - AdvancedUnitControls
                digitValStr = modelNoStr.Substring(30, 1);
                dt = objMain.MonitorGetModelNoValues(31, "NA");
                this.cbAdvancedUnitControls.DataSource = dt;
                this.cbAdvancedUnitControls.DisplayMember = "DigitDescription";
                this.cbAdvancedUnitControls.ValueMember = "DigitVal";
                this.cbAdvancedUnitControls.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 32 - PreHeaterOption
                digitValStr = modelNoStr.Substring(31, 1);
                dt = objMain.MonitorGetModelNoValues(32, "NA");
                this.cbPreHeaterOption.DataSource = dt;
                this.cbPreHeaterOption.DisplayMember = "DigitDescription";
                this.cbPreHeaterOption.ValueMember = "DigitVal";
                this.cbPreHeaterOption.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 33 - ExhaustFanMotor
                digitValStr = modelNoStr.Substring(32, 1);
                dt = objMain.MonitorGetModelNoValues(33, "NA");
                this.cbExhaustFanMotor.DataSource = dt;
                this.cbExhaustFanMotor.DisplayMember = "DigitDescription";
                this.cbExhaustFanMotor.ValueMember = "DigitVal";
                this.cbExhaustFanMotor.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 34 - ExhaustFanMotorType
                digitValStr = modelNoStr.Substring(33, 1);
                dt = objMain.MonitorGetModelNoValues(34, "NA");
                this.cbExhaustFanMotorType.DataSource = dt;
                this.cbExhaustFanMotorType.DisplayMember = "DigitDescription";
                this.cbExhaustFanMotorType.ValueMember = "DigitVal";
                this.cbExhaustFanMotorType.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 35 - ExhaustFanSize
                digitValStr = modelNoStr.Substring(34, 1);
                dt = objMain.MonitorGetModelNoValues(35, "NA");
                this.cbExhaustFanSize.DataSource = dt;
                this.cbExhaustFanSize.DisplayMember = "DigitDescription";
                this.cbExhaustFanSize.ValueMember = "DigitVal";
                this.cbExhaustFanSize.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 36 - ExhaustDampers
                digitValStr = modelNoStr.Substring(35, 1);
                dt = objMain.MonitorGetModelNoValues(36, "NA");
                this.cbExhaustDampers.DataSource = dt;
                this.cbExhaustDampers.DisplayMember = "DigitDescription";
                this.cbExhaustDampers.ValueMember = "DigitVal";
                this.cbExhaustDampers.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 37 - EnergyRecoveryWheel
                digitValStr = modelNoStr.Substring(36, 1);
                dt = objMain.MonitorGetModelNoValues(37, "NA");
                this.cbEnergyRecoveryWheel.DataSource = dt;
                this.cbEnergyRecoveryWheel.DisplayMember = "DigitDescription";
                this.cbEnergyRecoveryWheel.ValueMember = "DigitVal";
                this.cbEnergyRecoveryWheel.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 38 - EnergyRecoveryOptions 
                digitValStr = modelNoStr.Substring(37, 1);
                dt = objMain.MonitorGetModelNoValues(38, "NA");
                this.cbEnergyRecoveryOptions.DataSource = dt;
                this.cbEnergyRecoveryOptions.DisplayMember = "DigitDescription";
                this.cbEnergyRecoveryOptions.ValueMember = "DigitVal";
                this.cbEnergyRecoveryOptions.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 39 - SupplyDampers 
                digitValStr = modelNoStr.Substring(38, 1);
                dt = objMain.MonitorGetModelNoValues(39, "NA");
                this.cbSupplyDampers.DataSource = dt;
                this.cbSupplyDampers.DisplayMember = "DigitDescription";
                this.cbSupplyDampers.ValueMember = "DigitVal";
                this.cbSupplyDampers.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 40 - SmokeDectectorOutdoorAirMon 
                digitValStr = modelNoStr.Substring(39, 1);
                dt = objMain.MonitorGetModelNoValues(40, "NA");
                this.cbSmokeDectectorOutdoorAirMon.DataSource = dt;
                this.cbSmokeDectectorOutdoorAirMon.DisplayMember = "DigitDescription";
                this.cbSmokeDectectorOutdoorAirMon.ValueMember = "DigitVal";
                this.cbSmokeDectectorOutdoorAirMon.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 41 - Pulley - Driver 
                digitValStr = modelNoStr.Substring(40, 1);
                dt = objMain.MonitorGetModelNoValues(41, "NA");
                this.cbPulleyDriver.DataSource = dt;
                this.cbPulleyDriver.DisplayMember = "DigitDescription";
                this.cbPulleyDriver.ValueMember = "DigitVal";
                this.cbPulleyDriver.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 42 - Belt 
                digitValStr = modelNoStr.Substring(41, 1);
                dt = objMain.MonitorGetModelNoValues(42, "NA");
                this.cbBelt.DataSource = dt;
                this.cbBelt.DisplayMember = "DigitDescription";
                this.cbBelt.ValueMember = "DigitVal";
                this.cbBelt.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                // Digit 43 - Pulley - Fan 
                digitValStr = modelNoStr.Substring(42, 1);
                dt = objMain.MonitorGetModelNoValues(43, "NA");
                this.cbPulleyFan.DataSource = dt;
                this.cbPulleyFan.DisplayMember = "DigitDescription";
                this.cbPulleyFan.ValueMember = "DigitVal";
                this.cbPulleyFan.SelectedIndex = objMain.GetRowIndex(dt, digitValStr);

                this.btnCreateSubmittal.Enabled = true;

                this.Refresh();
            }
            else if (modelNoStr.Length == 0)     
            {
                modelNoStr = cbUnitType.SelectedValue.ToString();
                modelNoStr += cbEfficiency.SelectedValue.ToString();
                modelNoStr += cbAirflowConfiguration.SelectedValue.ToString();
                modelNoStr += cbCoolingCapacity.SelectedValue.ToString();
                modelNoStr += cbMajorDesignSequence.SelectedValue.ToString();
                modelNoStr += cbVoltage.SelectedValue.ToString();
                modelNoStr += cbUnitControls.SelectedValue.ToString();
                modelNoStr += cbHeatingCapacity.SelectedValue.ToString();
                modelNoStr += cbMinorDesignSequence.SelectedValue.ToString();
                modelNoStr += cbServiceSequence.SelectedValue.ToString();
                modelNoStr += cbFreshAirSelection.SelectedValue.ToString();
                modelNoStr += cbSupplyFanMotor.SelectedValue.ToString();
                modelNoStr += cbHingedServiceAccessFilters.SelectedValue.ToString();
                modelNoStr += cbCondenserCoilProtection.SelectedValue.ToString();
                modelNoStr += cbThruBaseProvisions.SelectedValue.ToString();
                modelNoStr += cbDisconnectSwitch.SelectedValue.ToString();
                modelNoStr += cbConvenienceOutletOptions.SelectedValue.ToString();
                modelNoStr += cbCommunicationsOptions.SelectedValue.ToString();
                modelNoStr += cbRefrigerationSystemOptions.SelectedValue.ToString();
                modelNoStr += cbRefrigerationControls.SelectedValue.ToString();
                modelNoStr += cbSmokeDetector.SelectedValue.ToString();
                modelNoStr += cbTraneSystemMonitoringControls.SelectedValue.ToString();
                modelNoStr += cbSystemMonitoringControls.SelectedValue.ToString();
                modelNoStr += cbUnitHardwareEnhancements.SelectedValue.ToString();
                modelNoStr += cbShortCircuitCurrentRating.SelectedValue.ToString();
                modelNoStr += "00";
                modelNoStr += cbAdvancedUnitControls.SelectedValue.ToString();
                modelNoStr += cbPreHeaterOption.SelectedValue.ToString();
                modelNoStr += cbExhaustFanMotor.SelectedValue.ToString();
                modelNoStr += cbExhaustFanMotorType.SelectedValue.ToString();
                modelNoStr += cbExhaustFanSize.SelectedValue.ToString();
                modelNoStr += cbExhaustDampers.SelectedValue.ToString();
                modelNoStr += cbEnergyRecoveryWheel.SelectedValue.ToString();
                modelNoStr += cbEnergyRecoveryOptions.SelectedValue.ToString();
                modelNoStr += cbSupplyDampers.SelectedValue.ToString();

                modelNoStr += cbSmokeDectectorOutdoorAirMon.SelectedValue.ToString();
                modelNoStr += cbPulleyDriver.SelectedValue.ToString();
                modelNoStr += cbBelt.SelectedValue.ToString();
                modelNoStr += cbPulleyFan.SelectedValue.ToString();
                lbModelNo.Text = modelNoStr;
                txtModelNo.Text = modelNoStr;
            }   
            else
            {
                MessageBox.Show("ERROR - Invalid ModelNo, must be 43 characters long!");
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }

        private void btnCreateBOM_Click(object sender, EventArgs e)
        {
            CreateBOM();
        }

        private void deleteBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string jobNumStr;
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int deletedSuccessInt = 0;

            jobNumStr = this.lbJobNum.Text;

            DialogResult result1 = MessageBox.Show("Attempting to delete an existing BOM for Job# " + jobNumStr +
                                                   " do you wish to continue???",
                                                   "WARNING WARNING WARNING",
                                                   MessageBoxButtons.YesNo,
                                                   MessageBoxIcon.Exclamation);

            if (result1 == DialogResult.Yes)
            {
                deletedSuccessInt = objMain.deleteJobFromDatabase(jobNumStr, userName, "MON");
                if (deletedSuccessInt == 0)
                {
                    //this.lbBOMCreationDate.Text = "";
                    MessageBox.Show("You have successfully deleted the BOM for Job# " + jobNumStr + " from the database");
                }
                else if (deletedSuccessInt == 1)
                {
                    MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                        jobNumStr + " from the OAU_BOMs table! Please contact IT to resolve this issue.");
                }
                else if (deletedSuccessInt == 2)
                {
                    MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                        jobNumStr + " from the jobmtl table! Please contact IT to resolve this issue.");
                }
                else if (deletedSuccessInt == 3)
                {
                    MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                        jobNumStr + " from the jobopdtl table! Please contact IT to resolve this issue.");
                }
                else if (deletedSuccessInt == 4)
                {
                    MessageBox.Show("ERROR - A problem occurred while trying to delete the BOM for Job# " +
                        jobNumStr + " from the joboper table! Please contact IT to resolve this issue.");
                }
            }
        }

        private void btnGetOrderDetails_Click(object sender, EventArgs e)
        {
            string orderNumStr = txtOrderNum.Text;
            string orderLineStr = txtOrderLine.Text;
            string modelNoStr = "";

            int orderNumInt = 0;
            int orderLineInt = 0;

            try 
            {
                orderNumInt = Int32.Parse(orderNumStr);
            }
            catch
            {
                 MessageBox.Show("ERROR -- Order Num is required integer field!");
            }

            try 
            {
                orderLineInt = Int32.Parse(orderLineStr);
            }
            catch
            {
                 MessageBox.Show("ERROR -- Order Line is required integer field!");
            }

            try
            {
                DataTable dt = objMain.MonitorGetOrderDetails(orderNumInt, orderLineInt);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];

                    modelNoStr = dr["LineDesc"].ToString();
                    if (modelNoStr.Length == 40)
                    {
                        txtModelNo.Text = modelNoStr;
                    }
                    else
                    {
                        MessageBox.Show("ERROR -- Order Line Description field does not contain a valid Monitor ModelNo. Please enter a valid ModelNo!");
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR -- Reading the OrderDtl table = " + ex);
            }
        }

        private void btnCreateSubmittal_Click(object sender, EventArgs e)
        {
            string modelNoStr = txtModelNo.Text;
            string minCKTAmpStr = "";
            string mopStr = "";
            string rawMopStr = "";
            string voltageStr = "";
            string jobNameStr = "";
            string tonageStr = "";
            string serverName = ConfigurationManager.AppSettings["ProdEpicorAppServer"];

            switch (modelNoStr.Substring(7, 1)) //Voltage 
            {
                case "1":                    
                    voltageStr = "208-230/60/1";                    
                    break;
                case "3":                    
                    voltageStr = "208-230/60/3";                    
                    break;
                case "4":                   
                    voltageStr = "460/60/3";                   
                    break;
                case "W":                   
                    voltageStr = "575/60/3";                    
                    break;
                default:
                    voltageStr = "208-230/60/3"; 
                    break;
            }

            switch (modelNoStr.Substring(3, 3)) //Tonage
            {
                case "033":
                    tonageStr = "3 Tons ";
                    break;
                case "036":
                    tonageStr = "3 Tons ";
                    break;
                case "037":
                    tonageStr = "3 Tons ";
                    break;
                case "043":
                    tonageStr = "4 Tons ";
                    break;
                case "047":
                    tonageStr = "4 Tons ";
                    break;
                case "048":
                    tonageStr = "4 Tons ";
                    break;
                case "060":
                    tonageStr = "5 Tons ";
                    break;
                case "063":
                    tonageStr = "5 Tons ";
                    break;
                case "067":
                    tonageStr = "5 Tons ";
                    break;
                case "072":
                    tonageStr = "6 Tons ";
                    break;
                case "074":
                    tonageStr = "6 Tons ";
                    break;
                case "090":
                    tonageStr = "7 1/2 Tons ";
                    break;
                case "092":
                    tonageStr = "7 1/2 Tons ";
                    break;
                case "102":
                    tonageStr = "8 1/2 Tons ";
                    break;
                case "120":
                    tonageStr = "10 Tons ";
                    break;
                case "150":
                    tonageStr = "12 1/2 Tons ";
                    break;
                case "180":
                    tonageStr = "15 Tons ";
                    break;
                case "210":
                    tonageStr = "17 1/2 Tons ";
                    break;
                case "240":
                    tonageStr = "20 Tons ";
                    break;
                case "300":
                    tonageStr = "25 Tons ";
                    break;
                default:
                    tonageStr = "";
                    break;
            }

            objMain.CalculateMonitorAmpacityValues(out minCKTAmpStr, out mopStr, out rawMopStr, modelNoStr);

            ReportDocument cryRpt = new ReportDocument();
            frmPrintBOM frmPrint = new frmPrintBOM();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@ModelNo";
            pdv1.Value = modelNoStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@Voltage";
            pdv2.Value = voltageStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@MinCKTAmp";
            pdv3.Value = minCKTAmpStr;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@MOP";
            pdv4.Value = mopStr;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@JobName";
            pdv5.Value = jobNameStr;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@Tonage";
            pdv6.Value = tonageStr;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG
            //cryRpt.Load(@"C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OA_Config_Rev6\OA_Config_Rev6\Reports\MonitorSubmittalRpt.v1.rpt");
            cryRpt.Load(@"\\KCCWVTEPIC9APP2\Apps\OAU\OAUConfiguratorcrystalreports\MonitorSubmittalRpt.v1.rpt");
#else
            //cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\MonitorSubmittalRpt.v1.rpt");
            //cryRpt.Load(@"\\KCCWVPEPIC9APP\Apps\OAU\OAUConfiguratorcrystalreports\MonitorSubmittalRpt.v1.rpt");
            cryRpt.Load(@"\\" + serverName + "\\Apps\\OAU\\OAUConfiguratorcrystalreports\\JobNumBarCode.rpt");
#endif

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.ShowDialog();
            frmPrint.crystalReportViewer1.Refresh();            
        }

        #endregion

        #region Other Methods
        private void CreateBOM()
        {
            string jobNumStr = lbJobNum.Text;
            string modelNoStr = lbModelNo.Text;
            string minCKTAmpStr = "0";
            string mopStr = "0";
            string rawMopStr = "0";
            string circuit1Charge = "";
            string circuit2Charge = "";

            frmDisplayBOM frmBOM = new frmDisplayBOM();

            frmBOM.lbVoltage.Text = cbVoltage.Text;
            frmBOM.lbCir1Label.Visible = false;
            frmBOM.lbCir1Chrg.Visible = false;
            frmBOM.lbCir2Label.Visible = false;
            frmBOM.lbCir2Chrg.Visible = false;

            DataTable dtBOM = objMain.GetExistingBOMPartListByJobNum(jobNumStr, "MON");

            if (dtBOM.Rows.Count == 0) // No Existing BOM found.
            {
                dtBOM = objMain.MonitorGetPartListByModelNo(modelNoStr);

                frmBOM.btnAddPart.Enabled = false;
                string parentPartNum = "";
                string curParentPartNum = "";
                string curAsmSeq = "";
                int seqNo = 0;
                int asmSeq = 0;                

                if (dtBOM.Rows.Count > 0)
                {
                    objMain.CalculateMonitorAmpacityValues(out minCKTAmpStr, out mopStr, out rawMopStr, modelNoStr);
                    //dtBOM = addRefrigAndBreakerAndWarranty(dtBOM, minCKTAmpStr, mopStr, out circuit1Charge, out circuit2Charge);
                }
            }
            else
            {
                DataTable dtEtl = objMain.GetMonitorEtlData(jobNumStr);
                if (dtEtl.Rows.Count > 0)
                {
                    DataRow dr = dtEtl.Rows[0];
                    minCKTAmpStr = dr["MinCKTAmp"].ToString();
                    mopStr = dr["MOP"].ToString();
                }
                frmBOM.btnPrint.Enabled = true;
                frmBOM.btnAddPart.Enabled = true;               
            }

            if (jobNumStr == "")
            {
                frmBOM.btnSave.Enabled = false;
            }
            

            dtBOM.DefaultView.Sort = "AssemblySeq";
            dtBOM = dtBOM.DefaultView.ToTable();
            frmBOM.dgvBOM.DataSource = dtBOM;
            frmBOM.dgvBOM.Columns["PartCategory"].Width = 150;
            frmBOM.dgvBOM.Columns["PartCategory"].HeaderText = "Category";
            frmBOM.dgvBOM.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartCategory"].DisplayIndex = 0;
            frmBOM.dgvBOM.Columns["AssemblySeq"].Width = 50;
            frmBOM.dgvBOM.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            frmBOM.dgvBOM.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["AssemblySeq"].DisplayIndex = 1;
            frmBOM.dgvBOM.Columns["SeqNo"].Width = 50;
            frmBOM.dgvBOM.Columns["SeqNo"].HeaderText = "Seq No";
            frmBOM.dgvBOM.Columns["SeqNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["SeqNo"].DisplayIndex = 2;
            frmBOM.dgvBOM.Columns["PartNum"].Width = 150;
            frmBOM.dgvBOM.Columns["PartNum"].HeaderText = "Part Number";
            frmBOM.dgvBOM.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartNum"].DisplayIndex = 3;
            frmBOM.dgvBOM.Columns["Description"].Width = 300;
            frmBOM.dgvBOM.Columns["Description"].HeaderText = "Part Description";
            frmBOM.dgvBOM.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Description"].DisplayIndex = 4;
            frmBOM.dgvBOM.Columns["ReqQty"].Width = 75;
            frmBOM.dgvBOM.Columns["ReqQty"].HeaderText = "Req Qty";
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["ReqQty"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["ReqQty"].DisplayIndex = 5;
            frmBOM.dgvBOM.Columns["UOM"].Width = 50;
            frmBOM.dgvBOM.Columns["UOM"].HeaderText = "UOM";
            frmBOM.dgvBOM.Columns["UOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["UOM"].DisplayIndex = 6;
            frmBOM.dgvBOM.Columns["UnitCost"].Width = 75;
            frmBOM.dgvBOM.Columns["UnitCost"].HeaderText = "Unit Cost";
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            frmBOM.dgvBOM.Columns["UnitCost"].DefaultCellStyle.Format = "N2";
            frmBOM.dgvBOM.Columns["UnitCost"].DisplayIndex = 7;
            frmBOM.dgvBOM.Columns["WhseCode"].Width = 75;
            frmBOM.dgvBOM.Columns["WhseCode"].HeaderText = "Whse Code";
            frmBOM.dgvBOM.Columns["WhseCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["WhseCode"].DisplayIndex = 8;
            frmBOM.dgvBOM.Columns["RelOp"].Width = 70;
            frmBOM.dgvBOM.Columns["RelOp"].HeaderText = "Rel Op";
            frmBOM.dgvBOM.Columns["RelOp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            frmBOM.dgvBOM.Columns["RelOp"].DisplayIndex = 9;
            frmBOM.dgvBOM.Columns["Status"].Width = 75;
            frmBOM.dgvBOM.Columns["Status"].HeaderText = "Status";
            frmBOM.dgvBOM.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["Status"].DisplayIndex = 10;
            frmBOM.dgvBOM.Columns["PartStatus"].Width = 60;
            frmBOM.dgvBOM.Columns["PartStatus"].HeaderText = "Active Status";
            frmBOM.dgvBOM.Columns["PartStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["PartStatus"].DisplayIndex = 11;
            frmBOM.dgvBOM.Columns["ParentPartNum"].Width = 210;
            frmBOM.dgvBOM.Columns["ParentPartNum"].HeaderText = "ParentPartNum";
            frmBOM.dgvBOM.Columns["ParentPartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmBOM.dgvBOM.Columns["ParentPartNum"].DisplayIndex = 12;
            frmBOM.dgvBOM.Columns["QtyPer"].Visible = false; 
            frmBOM.dgvBOM.Columns["RevisionNum"].Visible = false;
            frmBOM.dgvBOM.Columns["CostMethod"].Visible = false;
            frmBOM.dgvBOM.Columns["PartType"].Visible = false;
            frmBOM.dgvBOM.Columns["PROGRESS_RECID"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleHeadID"].Visible = false;
            frmBOM.dgvBOM.Columns["RuleBatchID"].Visible = false;
            frmBOM.dgvBOM.Columns["Generation"].Visible = false;

            frmBOM.Text = "Monitor Bill of Materials";
            frmBOM.lbBOMCreationDate.Text = lbBOMCreationDate.Text;
            frmBOM.lbBOM_CreateBy.Text = lbBomCreateBy.Text;
            frmBOM.lbCompleteDate.Text = lbCompleteDate.Text;
            frmBOM.lbLastUpdateDate.Text = lbLastUpdateDate.Text;
            frmBOM.lbLastUpdateBy.Text = lbLastUpdateBy.Text;
            frmBOM.lbOrderDate.Text = lbOrderDate.Text;
            frmBOM.lbProdStartDate.Text = lbProdStartDate.Text;
            frmBOM.lbShipDate.Text = lbShipDate.Text;
            frmBOM.lbJobNum.Text = jobNumStr;
            frmBOM.lbEnvironment.Text = GlobalJob.EnvironmentStr;
            frmBOM.BackColor = GlobalJob.EnvColor;
            frmBOM.lbCustName.Text = lbCustName.Text;
            frmBOM.lbModelNo.Text = modelNoStr;
            frmBOM.lbMCA.Text = minCKTAmpStr;
            frmBOM.lbMOP.Text = mopStr;
            frmBOM.lbCir1Chrg.Text = "";
            frmBOM.lbCir2Chrg.Text = "";

            frmBOM.ShowDialog();
        }

        #endregion       

        private void txtModelNo_TextChanged(object sender, EventArgs e)
        {
            if (txtModelNo.Text.Length == 40)
            {
                btnCreateSubmittal.Enabled = true;
            }
            else
            {
                btnCreateSubmittal.Enabled = false;
            }
        }


        private DataTable addRefrigAndBreakerAndWarranty(DataTable dtBOM, string mcaStr, string mopStr, out string circuit1ChargeRetVal, out string circuit2ChargeRetVal)
        {
            string modelNoStr = lbModelNo.Text;
            string unitPriceStr = "";
            string ruleHeadIdStr = "";
            string digit3 = modelNoStr.Substring(2, 1);
            string digit4 = modelNoStr.Substring(3, 1);
            string digit567 = modelNoStr.Substring(4, 3);
            string digit9 = modelNoStr.Substring(8, 1);
            string digit11 = modelNoStr.Substring(10, 1);
            string digit12 = modelNoStr.Substring(11, 1);
            string digit13 = modelNoStr.Substring(12, 1);
            string digit14 = modelNoStr.Substring(13, 1);
            string circuit1ChargeStr = "0";
            string circuit2ChargeStr = "0";

            int seqNumInt = 0;

            DataTable dtCCD = objMain.GetCompCircuitChargeData(digit3, digit4, digit567, digit9, digit11, digit12, digit13, digit14);
            if (dtCCD.Rows.Count > 0)
            {
                DataRow drComp = dtCCD.Rows[0];

                if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                {
                    circuit1ChargeStr = drComp["Circuit1_Charge"].ToString();
                }
                else
                {
                    circuit1ChargeStr = (Decimal.Parse(drComp["Circuit1_Charge"].ToString()) + Decimal.Parse(drComp["Circuit1_ReheatCharge"].ToString())).ToString();
                }

                if (drComp["Circuit2_Charge"].ToString() != "")
                {
                    if ((modelNoStr.Substring(11, 1).ToString() == "0"))
                    {
                        circuit2ChargeStr = drComp["Circuit2_Charge"].ToString();
                    }
                    else
                    {
                        circuit2ChargeStr = (Decimal.Parse(drComp["Circuit2_Charge"].ToString()) + Decimal.Parse(drComp["Circuit2_ReheatCharge"].ToString())).ToString();
                    }
                }
            }

            DataTable dtPart = objMain.GetPartDetail("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                unitPriceStr = drPart["UnitPrice"].ToString();
            }

            dtPart = objMain.GetPartRulesHead("VCPRFG-0400");
            if (dtPart.Rows.Count > 0)
            {
                DataRow drPart = dtPart.Rows[0];
                ruleHeadIdStr = drPart["ID"].ToString();
            }

            var dr = dtBOM.NewRow();
            dr["PartCategory"] = "Refrigeration";
            dr["AssemblySeq"] = 0;
            seqNumInt = getNextSeqNum("0", dtBOM);
            dr["SeqNo"] = seqNumInt;
            dr["PartNum"] = "VCPRFG-0400";
            dr["Description"] = "Tank, 100lb, R-410a";

            circuit1ChargeRetVal = circuit1ChargeStr;
            circuit2ChargeRetVal = circuit2ChargeStr;
            string partQtyStr = (decimal.Parse(circuit1ChargeStr) + decimal.Parse(circuit2ChargeStr)).ToString();

            decimal partQtyDec = 0;

            if (partQtyStr == "")
            {
                partQtyDec = 0;
            }
            else
            {
                partQtyDec = decimal.Parse(partQtyStr);
            }

            dr["ReqQty"] = partQtyDec;
            dr["UOM"] = "LB";
            dr["UnitCost"] = decimal.Parse(unitPriceStr);
            dr["WhseCode"] = "LWH2";
            dr["RelOp"] = 60;
            dr["PartStatus"] = "Active";
            dr["RevisionNum"] = "";
            dr["CostMethod"] = "A";
            dr["PartType"] = "";
            dr["Status"] = "BackFlush";
            dr["PROGRESS_RECID"] = 0;
            dr["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            dr["ParentPartNum"] = "";

            dtBOM.Rows.Add(dr);

            //if (Int32.Parse(mopStr) < 100)
            //{
            //    string tempMopStr = "0" + mopStr;
            //    mopStr = tempMopStr;
            //}
            //dtPart = objMain.GetOAU_BreakerByModelNo(modelNoStr.Substring(0, 3), mopStr, modelNoStr.Substring(40, 1), "REV6");

            //if (dtPart.Rows.Count > 0)
            //{
            //    var dr1 = dtBOM.NewRow();

            //    DataRow drPart = dtPart.Rows[0];

            //    dr1["PartCategory"] = drPart["PartCategory"].ToString();
            //    dr1["AssemblySeq"] = 0;
            //    dr1["SeqNo"] = seqNumInt + 10;
            //    dr1["PartNum"] = drPart["PartNum"].ToString();
            //    dr1["Description"] = drPart["PartDescription"].ToString();

            //    partQtyStr = drPart["ReqQty"].ToString();

            //    if (partQtyStr == "")
            //    {
            //        partQtyDec = 0;
            //    }
            //    else
            //    {
            //        partQtyDec = decimal.Parse(partQtyStr);
            //    }

            //    dr1["ReqQty"] = partQtyDec;
            //    dr1["UOM"] = drPart["UOM"].ToString();
            //    unitPriceStr = drPart["UnitPrice"].ToString();
            //    dr1["UnitCost"] = decimal.Parse(unitPriceStr);
            //    dr1["WhseCode"] = "LWH2";
            //    dr1["RelOp"] = Int32.Parse(drPart["RelOp"].ToString());
            //    dr1["PartStatus"] = "Active";
            //    dr1["RevisionNum"] = drPart["RevisionNum"].ToString();
            //    dr1["CostMethod"] = drPart["CostMethod"].ToString();
            //    dr1["PartType"] = drPart["PartType"].ToString();
            //    dr1["Status"] = "BackFlush";
            //    dr1["PROGRESS_RECID"] = 0;
            //    ruleHeadIdStr = drPart["RuleHeadId"].ToString();
            //    dr1["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            //    dr1["ParentPartNum"] = "";

            //    dtBOM.Rows.Add(dr1);
            //}
            //var dr2 = dtBOM.NewRow();

            //dr2["PartCategory"] = "Warranty";
            //dr2["AssemblySeq"] = 0;
            //seqNumInt = 9998;
            //dr2["SeqNo"] = seqNumInt.ToString();
            //dr2["PartNum"] = "OAWARRANTYSTD";
            //dr2["Description"] = "Warranty Part Entry";

            //partQtyStr = "1.00";

            //if (partQtyStr == "")
            //{
            //    partQtyDec = 0;
            //}
            //else
            //{
            //    partQtyDec = decimal.Parse(partQtyStr);
            //}

            //dr2["ReqQty"] = partQtyDec;
            //dr2["UOM"] = "";
            ////unitPriceStr = drPart["UnitPrice"].ToString();
            ////dr2["UnitCost"] = decimal.Parse(unitPriceStr);
            //dr2["UnitCost"] = 0;
            //dr2["WhseCode"] = "LWH2";
            //dr2["RelOp"] = 10;
            //dr2["PartStatus"] = "Active";
            //dr2["RevisionNum"] = "";
            //dr2["CostMethod"] = "A";
            //dr2["PartType"] = "Warranty";
            //dr2["Status"] = "BackFlush";
            //dr2["PROGRESS_RECID"] = 0;
            //ruleHeadIdStr = "0";
            //dr2["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            //dr2["ParentPartNum"] = "";

            //dtBOM.Rows.Add(dr2);

            //var dr3 = dtBOM.NewRow();

            //dr3["PartCategory"] = "Warranty";
            //dr3["AssemblySeq"] = 0;
            //seqNumInt = 9999;
            //dr3["SeqNo"] = seqNumInt.ToString();
            //dr3["PartNum"] = "OAWARRANTYCOMP";
            //dr3["Description"] = "Warranty Part Entry";

            //partQtyStr = "1.00";

            //if (partQtyStr == "")
            //{
            //    partQtyDec = 0;
            //}
            //else
            //{
            //    partQtyDec = decimal.Parse(partQtyStr);
            //}

            //dr3["ReqQty"] = partQtyDec;
            //dr3["UOM"] = "";
            ////unitPriceStr = drPart["UnitPrice"].ToString();
            ////dr3["UnitCost"] = decimal.Parse(unitPriceStr);
            //dr3["UnitCost"] = 0;
            //dr3["WhseCode"] = "LWH2";
            //dr3["RelOp"] = 10;
            //dr3["PartStatus"] = "Active";
            //dr3["RevisionNum"] = "";
            //dr3["CostMethod"] = "A";
            //dr3["PartType"] = "Warranty";
            //dr3["Status"] = "BackFlush";
            //dr3["PROGRESS_RECID"] = 0;
            //ruleHeadIdStr = "0";
            //dr3["RuleHeadID"] = Int32.Parse(ruleHeadIdStr);
            //dr3["ParentPartNum"] = "";

            //dtBOM.Rows.Add(dr3);


            return dtBOM;
        }

        private int getNextSeqNum(string assemblySeqStr, DataTable dtBOM)
        {
            int retSeqNumInt = 0;
            string seqNumStr = "";

            foreach (DataRow dr in dtBOM.Rows)
            {
                if (dr["AssemblySeq"].ToString() == assemblySeqStr)
                {
                    seqNumStr = dr["SeqNo"].ToString();
                }
            }

            if (seqNumStr != "")
            {
                retSeqNumInt = Int32.Parse(seqNumStr) + 10;
            }

            return retSeqNumInt;

        }

        private void createBOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateBOM();
        }
    }
}
